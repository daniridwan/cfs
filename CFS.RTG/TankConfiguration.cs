﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace CFS.RTG
{
    public class TankConfiguration : ConfigurationSection
    {
        public static TankConfiguration GetConfiguration()
        {
            try
            {
                return (TankConfiguration)System.Configuration.ConfigurationManager.GetSection("DeviceConfiguration/TankCollectorConfiguration");
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [ConfigurationProperty("TankElementCollection")]
        public TankElementCollection ListOfTank
        {
            get
            {
                return (TankElementCollection)this["TankElementCollection"] ?? new TankElementCollection();
            }
        }
        public TankElement GetServiceElement(string name)
        {
            TankElement retval = null;
            foreach (TankElement element in ListOfTank)
            {
                if (element.ID == name)
                {
                    retval = element;
                    break;
                }
            }
            return retval;
        }
    }

    public class TankElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new TankElement();
        }
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((TankElement)element).ID;
        }
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }
        protected override string ElementName
        {
            get
            {
                return "TankElement";
            }
        }
    }

    public class TankElement : ConfigurationElement
    {
        [ConfigurationProperty("Id", DefaultValue = "Undefined", IsKey = true, IsRequired = true)]
        public string ID
        {
            get { return (string)this["Id"]; }
            set { this["Id"] = value; }
        }
        [ConfigurationProperty("IpAddress", IsRequired = true, DefaultValue = "127.0.0.1")]
        public string IPAddress
        {
            get { return (string)this["IpAddress"]; }
            set { this["IpAddress"] = value; }
        }
        [ConfigurationProperty("DeviceAddress", IsRequired = true, DefaultValue = 1)]
        public int DeviceAddress
        {
            get { return (int)this["DeviceAddress"]; }
            set { this["DeviceAddress"] = value; }
        }
        [ConfigurationProperty("Port", IsRequired = true, DefaultValue = 502)]
        public int Port
        {
            get { return (int)this["Port"]; }
            set { this["Port"] = value; }
        }
        [ConfigurationProperty("StartAddress", IsRequired = true, DefaultValue = 0)]
        public int StartAddress
        {
            get { return (int)this["StartAddress"]; }
            set { this["StartAddress"] = value; }
        }
        [ConfigurationProperty("Quantity", IsRequired = true, DefaultValue = 1)]
        public int Quantity
        {
            get { return (int)this["Quantity"]; }
            set { this["Quantity"] = value; }
        }
    }
}
