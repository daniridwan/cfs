﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EasyModbus;
using System.Configuration;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.RTG
{
    class ASTM54Calculation
    {
        public double CalculateVollCorrFactor(double temperature, double density15)
        {
            density15 = Math.Round(density15, 4);
            temperature = Math.Round(temperature, 1);
            double liquidVollCorrFactor = new double();
            try
            {
                HSSFWorkbook hssfwb54;
                using (FileStream file = new FileStream("ASTM/ASTM54.xls", FileMode.Open, FileAccess.Read))
                {
                    hssfwb54 = new HSSFWorkbook(file);
                }
                ISheet sheet54 = hssfwb54.GetSheet("Sheet1");

                int rowNumberTemperature = 0;
                bool temperatureDefinedIn54 = false;
                int rowNumberTemperatureLowerLimit = 0;
                double rowTemperatureLowerLimit = 0; //x1
                int rowNumberTemperatureUpperLimit = 0;
                double rowTemperatureUpperLimit = 0; //x2

                int colNumberDensity = 0;
                bool densityDefinedIn54 = false;
                int colNumberDensityLowerLimit = 0;
                double colDensityLowerLimit = 0; //y1
                int colNumberDensityUpperLimit = 0;
                double colDensityUpperLimit = 0; //y2

                for (int row = 1; row <= sheet54.LastRowNum; row++)
                {
                    if (sheet54.GetRow(row) != null)
                    {
                        double cellValue = sheet54.GetRow(row).GetCell(0).NumericCellValue;
                        if (temperature == cellValue)
                        {
                            rowNumberTemperature = row;
                            temperatureDefinedIn54 = true;
                        }

                        if (cellValue < temperature)
                        {
                            rowTemperatureLowerLimit = cellValue;
                            rowNumberTemperatureLowerLimit = row;
                        }
                        else if (cellValue > temperature)
                        {
                            rowTemperatureUpperLimit = cellValue;
                            rowNumberTemperatureUpperLimit = row;
                            break;
                        }
                    }
                }

                IRow col54 = sheet54.GetRow(0);
                for (int colIndex = 1; colIndex < col54.LastCellNum; colIndex++)
                {
                    double cellValue = col54.GetCell(colIndex).NumericCellValue;
                    if (density15 == cellValue)
                    {
                        colNumberDensity = colIndex;
                        densityDefinedIn54 = true;
                    }

                    if (cellValue < density15)
                    {
                        colDensityLowerLimit = cellValue;
                        colNumberDensityLowerLimit = colIndex;
                    }
                    else if (cellValue > density15)
                    {
                        colDensityUpperLimit = cellValue;
                        colNumberDensityUpperLimit = colIndex;
                        break;
                    }
                }

                if (temperatureDefinedIn54 && densityDefinedIn54)
                {
                    liquidVollCorrFactor = sheet54.GetRow(rowNumberTemperature).GetCell(colNumberDensity).NumericCellValue;
                    Console.WriteLine("ASTM54 Calculation Temp: {0} Density15: {1} -> LiquidVolCorrFactor: {2}", temperature, density15, liquidVollCorrFactor);
                }
                if (temperatureDefinedIn54 && !densityDefinedIn54)
                {
                    double densityLowerLimit = sheet54.GetRow(rowNumberTemperature).GetCell(colNumberDensityLowerLimit).NumericCellValue;
                    double densityUpperLimit = sheet54.GetRow(rowNumberTemperature).GetCell(colNumberDensityUpperLimit).NumericCellValue;
                    liquidVollCorrFactor = densityLowerLimit + ((density15 - colDensityLowerLimit) / (colDensityUpperLimit - colDensityLowerLimit)) * (densityUpperLimit - densityLowerLimit);
                    Console.WriteLine("ASTM54 Calculation Temp: {0} Density15Interpolation): {1} -> LiquidVolCorrFactor: {2}", temperature, density15, liquidVollCorrFactor);
                }
                if (!temperatureDefinedIn54 && densityDefinedIn54)
                {
                    double temperatureLowerLimit = sheet54.GetRow(rowNumberTemperatureLowerLimit).GetCell(colNumberDensity).NumericCellValue;
                    double temperatureUpperLimit = sheet54.GetRow(rowNumberTemperatureUpperLimit).GetCell(colNumberDensity).NumericCellValue;
                    liquidVollCorrFactor = temperatureLowerLimit + ((temperature - rowTemperatureLowerLimit) / (rowTemperatureUpperLimit - rowTemperatureLowerLimit)) * (temperatureUpperLimit - temperatureLowerLimit);
                    Console.WriteLine("ASTM54 Calculation Temp(Interpolation): {0} Density: {1} -> LiquidVolCorrFactor: {2}", temperature, density15, liquidVollCorrFactor);
                }
                if (!temperatureDefinedIn54 && !densityDefinedIn54)
                {
                    //interpolasi pertama
                    double density15XLowerYLower = sheet54.GetRow(rowNumberTemperatureLowerLimit).GetCell(colNumberDensityLowerLimit).NumericCellValue;
                    double density15XUpperYLower = sheet54.GetRow(rowNumberTemperatureLowerLimit).GetCell(colNumberDensityUpperLimit).NumericCellValue;
                    double density15Lower = density15XLowerYLower + ((density15 - colDensityLowerLimit) / (colDensityUpperLimit - colDensityLowerLimit)) * (density15XUpperYLower - density15XLowerYLower);
                    //interpolasi kedua
                    double density15XLowerYUpper = sheet54.GetRow(rowNumberTemperatureUpperLimit).GetCell(colNumberDensityLowerLimit).NumericCellValue;
                    double density15XUpperYUpper = sheet54.GetRow(rowNumberTemperatureUpperLimit).GetCell(colNumberDensityUpperLimit).NumericCellValue;
                    double density15Upper = density15XLowerYUpper + ((density15 - colDensityLowerLimit) / (colDensityUpperLimit - colDensityLowerLimit)) * (density15XUpperYLower - density15XLowerYLower);
                    //interpolasi ketiga
                    liquidVollCorrFactor = density15Lower + ((temperature - rowTemperatureLowerLimit) / (rowTemperatureUpperLimit - rowTemperatureLowerLimit)) * (density15Upper - density15Lower);
                    Console.WriteLine("ASTM54 Calculation Extrapolation Temp: {0} Density15: {1} -> LiquidVolCorrFactor: {2}", temperature, density15, liquidVollCorrFactor);
                }
                return liquidVollCorrFactor;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return liquidVollCorrFactor;
            }
            return liquidVollCorrFactor;
        }
    }
}
