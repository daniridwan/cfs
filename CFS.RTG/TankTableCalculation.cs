﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EasyModbus;
using System.Configuration;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.RTG
{
    class TankTableCalculation
    {
        public double CalculateVolume(string tankName, double level)
        {
            double volumeObserved = new double();
            try
            {
                HSSFWorkbook hssfwbTT;
                using (FileStream file = new FileStream(string.Format("TankTable/{0}.xls", tankName), FileMode.Open, FileAccess.Read))
                {
                    hssfwbTT = new HSSFWorkbook(file);
                }
                double lowerLevelLimit = 0; //x1
                double lowerVolumeLimit = 0; //y1

                double upperLevelLimit = 0; //x2
                double upperVolumeLimit = 0; //y2

                ISheet sheetTT = hssfwbTT.GetSheet("Sheet1");
                for (int row = 1; row <= sheetTT.LastRowNum; row++)
                {
                    if (sheetTT.GetRow(row) != null)
                    {
                        double cellValue = sheetTT.GetRow(row).GetCell(0).NumericCellValue;
                        if (level == cellValue)
                        {
                            volumeObserved = sheetTT.GetRow(row).GetCell(1).NumericCellValue;
                            Console.WriteLine(string.Format("Tank Table Volume Calculation {0} mm -> {1} L", level, volumeObserved));
                            return volumeObserved;
                        }

                        if (cellValue < level)
                        {
                            lowerLevelLimit = cellValue;
                            lowerVolumeLimit = sheetTT.GetRow(row).GetCell(1).NumericCellValue;
                        }
                        else if (cellValue > level)
                        {
                            upperLevelLimit = cellValue;
                            upperVolumeLimit = sheetTT.GetRow(row).GetCell(1).NumericCellValue;
                            break;
                        }
                    }
                }
                //calculate interpolation
                volumeObserved = lowerVolumeLimit + ((level - lowerLevelLimit) / (upperLevelLimit - lowerLevelLimit)) * (upperVolumeLimit - lowerVolumeLimit);
                Console.WriteLine(string.Format("Tank Table Volume Calculation {0} -> {1}", level, volumeObserved));

                return volumeObserved;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return volumeObserved;
            }
        }
    }
}
