﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EasyModbus;
using System.Configuration;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System.IO;
using System.Threading;

namespace CFS.RTG
{
    class RTGWorker
    {
        public void Run()
        {
            TankElementCollection tankcollection = null;
            tankcollection = TankConfiguration.GetConfiguration().ListOfTank;
            foreach (TankElement element in tankcollection)
            {
                string id = element.ID;
                ModbusClient modbus = new ModbusClient(element.IPAddress, element.Port);
                int startAddress = element.StartAddress;
                int quantity = element.Quantity;
                try
                {
                    modbus.Connect();
                    //update to Tank Live Data
                    Tank tank = Tank.FindByName(element.ID);
                    TankLiveData tld = TankLiveData.FindById(tank.TankId);
                    tld.TankHeight = tank.TankHeight;
                    tld.TankVolume = tank.TankVolume;

                    float responseLiquidLevel; //P_LEVEL
                    float responseTotalObservedVolume; //TOT_OBS_VOL
                    float responseAvailCapacityVolume; //AVAIL_VOL
                    float responseVapourVolume; //VAPOUR_ROOM_VOL
                    float responseLiquidTemperature; //P_TEMP
                    float responseVapourTemperature; //V_TEMP
                    float responseAverageTemperature; //A_TEMP
                    float responseLiquidPressure; //P_PRESS
                    float responseVapourPressure; // V_PRESS
                    float responseLiquidObservedDensity; //P_OBS_D
                    float responseLiquidReferenceDensity; //P_REF_D
                    float responseLiquidGrossVolume; //GROSS_OBS_VOL / ATG Liquid Vol Obs 
                    float responseLiquidStandardVol; //NET STD VOL / ATG Liquid Vol 15
                    float responseLiquidMass; //P_MASS / LiquidMass
                    float responseVapourMass; //V_MASS
                    float responseTotalMass; //TOT_MASS                                              
                    float responseTotalMassFlowRate; //TOTMASS_FLW_RATE

                    #region Retrieve data from ATG
                    //liquid level
                    responseLiquidLevel = ModbusClient.ConvertRegistersToFloat(modbus.ReadInputRegisters(startAddress, 2), ModbusClient.RegisterOrder.HighLow);
                    tld.LiquidLevel = responseLiquidLevel;
                    //liquid temp
                    responseLiquidTemperature = ModbusClient.ConvertRegistersToFloat(modbus.ReadInputRegisters(startAddress + 16, 2), ModbusClient.RegisterOrder.HighLow);
                    tld.LiquidTemperature = responseLiquidTemperature;
                    //vapor temperature
                    //responseVapourTemperature = ModbusClient.ConvertRegistersToFloat(modbus.ReadInputRegisters(startAddress + 20, 2), ModbusClient.RegisterOrder.HighLow);
                    //tld.VaporTemperature = responseVapourTemperature;
                    //vapor pressure
                    //responseVapourPressure = ModbusClient.ConvertRegistersToFloat(modbus.ReadInputRegisters(startAddress + 32, 2), ModbusClient.RegisterOrder.HighLow);
                    //tld.VaporPressure = responseVapourPressure;
                    //liquid density
                    responseLiquidObservedDensity = ModbusClient.ConvertRegistersToFloat(modbus.ReadInputRegisters(startAddress + 36, 2), ModbusClient.RegisterOrder.HighLow);
                    tld.LiquidDensity = responseLiquidObservedDensity;
                    //liquid pressure
                    responseLiquidPressure = ModbusClient.ConvertRegistersToFloat(modbus.ReadInputRegisters(startAddress + 28, 2), ModbusClient.RegisterOrder.HighLow);
                    tld.LiquidPressure = responseLiquidPressure;
                    //total observe volume
                    responseTotalObservedVolume = ModbusClient.ConvertRegistersToFloat(modbus.ReadInputRegisters(startAddress + 4, 2), ModbusClient.RegisterOrder.HighLow);
                    tld.TotalObservedVolume = responseTotalObservedVolume;
                    //default
                    tld.LiquidWater = 0;
                    Console.WriteLine(string.Format("Retrieve Data from ATG Tank {0}: Liquid Level: {1}mm. Liquid Temperature: {2}C. Liquid Density: {3}kg/cm3. Liquid Pressure: {4} Kg/Cm2. Liquid Vol Obs ATG: {5} L",
                    tank.Name, tld.LiquidLevel, tld.LiquidTemperature, tld.LiquidDensity, tld.LiquidPressure, tld.TotalObservedVolume));
                    modbus.Disconnect();
                    #endregion

                    //read excel from Tank Table to calculate Volume observed based on level(mm)
                    #region TankTable
                    TankTableCalculation tt = new TankTableCalculation();
                    tld.LiquidVolObs = (float)tt.CalculateVolume(tank.Name, (double)tld.LiquidLevel);
                    #endregion

                    //read excel from ASTM 53 to calculate density at 15C based on liquid temperature & density given
                    #region ASTM 53
                    ASTM53Calculation astm53 = new ASTM53Calculation();
                    tld.LiquidDensity15 = (float)astm53.CalculateDensity15(tld.LiquidTemperature, tld.LiquidDensity);
                    #endregion

                    //read excel from ASTM 54 to calculate volume correction factor at 15C based on density15 & temperature
                    #region ASTM 54
                    ASTM54Calculation astm54 = new ASTM54Calculation();
                    tld.LiquidVolumeCorrectionFactor = 0;
                    tld.LiquidVolumeCorrectionFactor = (float)astm54.CalculateVollCorrFactor(tld.LiquidTemperature, tld.LiquidDensity15);
                    tld.LiquidVol15 = tld.LiquidVolObs * tld.LiquidVolumeCorrectionFactor;
                    Console.WriteLine(string.Format("Liquid Vol15: {0}", tld.LiquidVol15));
                    #endregion

                    //read excel from ASTM 56 to calculate LiquidDensityMultiplier based on density15
                    #region ASTM56
                    tld.LiquidDensityMultiplier = 0;
                    ASTM56Calculation astm56 = new ASTM56Calculation();
                    tld.LiquidDensityMultiplier = (float)astm56.CalculateLiquidDensityMultiplier(tld.LiquidDensity15);
                    #endregion

                    #region Liquid Mass
                    tld.LiquidMass = tld.LiquidVol15 * tld.LiquidDensityMultiplier;
                    Console.WriteLine(string.Format("Liquid Mass: {0}", tld.LiquidMass));
                    #endregion

                    #region Vapor
                    //tld.VaporVolObs = tld.TankVolume - tld.LiquidVolObs;
                    //tld.VaporVol15 = tld.VaporVolObs;
                    //tld.VaporDensity = (float)((tank.LabMolecularWeight / 22.4) * 0.001);
                    //tld.VaporTemperatureFactor = 273 / (273 + tld.VaporTemperature);
                    //tld.VaporPressureFactor = (float)((1.033 + tld.VaporPressure) / 1.033);
                    double molFactor = (tank.LabMolecularWeight / 22.4) * 0.001;
                    double astm58;
                    if (tank.UseLabValue)
                        astm58 = ASTM58(tank.LabDensity);
                    else
                        astm58 = ASTM58(tld.LiquidDensity);
                    //tld.VaporMass = (float)(tld.VaporVolObs * tld.VaporTemperatureFactor * tld.VaporPressureFactor * molFactor * astm58);
                    #endregion
                    tld.Timestamp = DateTime.Now;
                    tld.UpdateAndFlush();

                    //write to console
                    Console.WriteLine(string.Format(
                    @"{0} Level: {1} Temp: {2} Density: {3} Density 15C: {4} VolObs: {5} 
                    LiquidVollCorrFactor: {6} LiquidVol15: {7} Liquid Mass: {8}  
                    LiquidDensityMultiplier: {9} 
                    Timestamp: {10}",
                    element.ID, tld.LiquidLevel.ToString(), tld.LiquidTemperature.ToString(), tld.LiquidDensity.ToString(), tld.LiquidDensity15.ToString(), tld.LiquidVolObs.ToString(), tld.LiquidVolumeCorrectionFactor.ToString(),
                    tld.LiquidVol15.ToString(), tld.LiquidMass.ToString(),
                    tld.LiquidDensityMultiplier, tld.Timestamp.ToString()));

                    Thread.Sleep(5000);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public double ASTM58(double density15)
        {
            double result = 0;
            if (density15 >= 0.5 && density15 <= 0.5191)
                result = 0.99775;
            if (density15 >= 0.5192 && density15 <= 0.5421)
                result = 0.99785;
            if (density15 >= 0.5422 && density15 <= 0.5673)
                result = 0.99795;
            if (density15 >= 0.5674 && density15 <= 0.5950)
                result = 0.99805;
            if (density15 >= 0.5951 && density15 <= 0.6255)
                result = 0.99815;
            if (density15 >= 0.6256 && density15 <= 0.6593)
                result = 0.99825;

            return result;
        }
    }
}

