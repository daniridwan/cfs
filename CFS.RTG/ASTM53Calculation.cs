﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EasyModbus;
using System.Configuration;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.RTG
{
    class ASTM53Calculation
    {
        public double CalculateDensity15(double temperature, double density)
        {
            density = Math.Round(density, 4);
            temperature = Math.Round(temperature, 1);
            double density15 = new double();
            try
            {
                HSSFWorkbook hssfwb53;
                using (FileStream file = new FileStream("ASTM/ASTM53.xls", FileMode.Open, FileAccess.Read))
                {
                    hssfwb53 = new HSSFWorkbook(file);
                }
                ISheet sheet53 = hssfwb53.GetSheet("Sheet1");

                int rowNumberTemperature = 0;
                bool temperatureDefinedIn53 = false;
                int rowNumberTemperatureLowerLimit = 0;
                double rowTemperatureLowerLimit = 0; //x1
                int rowNumberTemperatureUpperLimit = 0;
                double rowTemperatureUpperLimit = 0; //x2

                int colNumberDensity = 0;
                bool densityDefinedIn53 = false;
                int colNumberDensityLowerLimit = 0;
                double colDensityLowerLimit = 0; //y1
                int colNumberDensityUpperLimit = 0;
                double colDensityUpperLimit = 0; //y2

                for (int row = 1; row <= sheet53.LastRowNum; row++)
                {
                    if (sheet53.GetRow(row) != null)
                    {
                        double cellValue = sheet53.GetRow(row).GetCell(0).NumericCellValue;
                        if (temperature == cellValue)
                        {
                            rowNumberTemperature = row;
                            temperatureDefinedIn53 = true;
                        }

                        if (cellValue < temperature)
                        {
                            rowTemperatureLowerLimit = cellValue;
                            rowNumberTemperatureLowerLimit = row;
                        }
                        else if (cellValue > temperature)
                        {
                            rowTemperatureUpperLimit = cellValue;
                            rowNumberTemperatureUpperLimit = row;
                            break;
                        }
                    }
                }

                IRow col53 = sheet53.GetRow(0);
                for (int colIndex = 1; colIndex < col53.LastCellNum; colIndex++)
                {
                    double cellValue = col53.GetCell(colIndex).NumericCellValue;
                    if (density == cellValue)
                    {
                        colNumberDensity = colIndex;
                        densityDefinedIn53 = true;
                    }

                    if (cellValue < density)
                    {
                        colDensityLowerLimit = cellValue;
                        colNumberDensityLowerLimit = colIndex;
                    }
                    else if (cellValue > density)
                    {
                        colDensityUpperLimit = cellValue;
                        colNumberDensityUpperLimit = colIndex;
                        break;
                    }
                }

                if (temperatureDefinedIn53 && densityDefinedIn53)
                {
                    density15 = sheet53.GetRow(rowNumberTemperature).GetCell(colNumberDensity).NumericCellValue;
                    Console.WriteLine("ASTM53 Calculation Temp: {0} Density: {1} -> Density15: {2}", temperature, density, density15);
                }
                if (temperatureDefinedIn53 && !densityDefinedIn53)
                {
                    double densityLowerLimit = sheet53.GetRow(rowNumberTemperature).GetCell(colNumberDensityLowerLimit).NumericCellValue;
                    double densityUpperLimit = sheet53.GetRow(rowNumberTemperature).GetCell(colNumberDensityUpperLimit).NumericCellValue;
                    density15 = densityLowerLimit + ((density - colDensityLowerLimit) / (colDensityUpperLimit - colDensityLowerLimit)) * (densityUpperLimit - densityLowerLimit);
                    Console.WriteLine("ASTM53 Calculation Temp: {0} Density(Interpolation): {1} -> Density15: {2}", temperature, density, density15);
                }
                if (!temperatureDefinedIn53 && densityDefinedIn53)
                {
                    double temperatureLowerLimit = sheet53.GetRow(rowNumberTemperatureLowerLimit).GetCell(colNumberDensity).NumericCellValue;
                    double temperatureUpperLimit = sheet53.GetRow(rowNumberTemperatureUpperLimit).GetCell(colNumberDensity).NumericCellValue;
                    density15 = temperatureLowerLimit + ((temperature - rowTemperatureLowerLimit) / (rowTemperatureUpperLimit - rowTemperatureLowerLimit)) * (temperatureUpperLimit - temperatureLowerLimit);
                    Console.WriteLine("ASTM53 Calculation Temp(Interpolation): {0} Density: {1} -> Density15: {2}", temperature, density, density15);
                }
                if (!temperatureDefinedIn53 && !densityDefinedIn53)
                {
                    //interpolasi pertama
                    double density15XLowerYLower = sheet53.GetRow(rowNumberTemperatureLowerLimit).GetCell(colNumberDensityLowerLimit).NumericCellValue;
                    double density15XUpperYLower = sheet53.GetRow(rowNumberTemperatureLowerLimit).GetCell(colNumberDensityUpperLimit).NumericCellValue;
                    double density15Lower = density15XLowerYLower + ((density - colDensityLowerLimit) / (colDensityUpperLimit - colDensityLowerLimit)) * (density15XUpperYLower - density15XLowerYLower);
                    //interpolasi kedua
                    double density15XLowerYUpper = sheet53.GetRow(rowNumberTemperatureUpperLimit).GetCell(colNumberDensityLowerLimit).NumericCellValue;
                    double density15XUpperYUpper = sheet53.GetRow(rowNumberTemperatureUpperLimit).GetCell(colNumberDensityUpperLimit).NumericCellValue;
                    double density15Upper = density15XLowerYUpper + ((density - colDensityLowerLimit) / (colDensityUpperLimit - colDensityLowerLimit)) * (density15XUpperYLower - density15XLowerYLower);
                    //interpolasi ketiga
                    density15 = density15Lower + ((temperature - rowTemperatureLowerLimit) / (rowTemperatureUpperLimit - rowTemperatureLowerLimit)) * (density15Upper - density15Lower);
                    Console.WriteLine("ASTM53 Calculation Extrapolation Temp: {0} Density: {1} -> Density15: {2}", temperature, density, density15);
                }
                return density15;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return density15;
            }
        }
    }
}
