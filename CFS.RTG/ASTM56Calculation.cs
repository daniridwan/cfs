﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EasyModbus;
using System.Configuration;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.RTG
{
    class ASTM56Calculation
    {
        public double CalculateLiquidDensityMultiplier(double density15)
        {
            double liquidDensityMultiplier = new float();
            try
            {
                HSSFWorkbook hssfwb56;
                using (FileStream file = new FileStream("ASTM/ASTM56.xls", FileMode.Open, FileAccess.Read))
                {
                    hssfwb56 = new HSSFWorkbook(file);
                }

                double lowerDensity15Limit = 0; //x1
                double lowerMultiplierLimit = 0; //y1

                double upperDensity15Limit = 0; //x2
                double upperMultiplierLimit = 0; //y2

                ISheet sheet56 = hssfwb56.GetSheet("Sheet1");

                for (int row = 1; row <= sheet56.LastRowNum; row++)
                {
                    if (sheet56.GetRow(row) != null)
                    {
                        double cellValue = sheet56.GetRow(row).GetCell(0).NumericCellValue;
                        if (density15 == cellValue)
                        {
                            liquidDensityMultiplier = (float)sheet56.GetRow(row).GetCell(1).NumericCellValue;
                            Console.WriteLine(string.Format("ASTM 56 Calculation: Liquid Density Multiplier {0}  -> {1} L", density15, liquidDensityMultiplier));
                            return liquidDensityMultiplier;
                        }

                        if (cellValue < density15)
                        {
                            lowerDensity15Limit = cellValue;
                            lowerMultiplierLimit = (float)sheet56.GetRow(row).GetCell(1).NumericCellValue;
                        }
                        else if (cellValue > density15)
                        {
                            upperDensity15Limit = cellValue;
                            upperMultiplierLimit = (float)sheet56.GetRow(row).GetCell(1).NumericCellValue;
                            break;
                        }
                    }
                }
                liquidDensityMultiplier = lowerMultiplierLimit + ((density15 - lowerDensity15Limit) / (upperDensity15Limit - lowerDensity15Limit)) * (upperMultiplierLimit - lowerMultiplierLimit);
                Console.WriteLine(string.Format("ASTM56 Liquid Density Multiplier Calculation {0} -> {1}", density15, liquidDensityMultiplier));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return liquidDensityMultiplier;
            }
            return liquidDensityMultiplier;
        }
    }
}
