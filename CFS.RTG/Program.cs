﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Timers;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;
using EasyModbus;
using CFS.Models;
using log4net;
using System.Configuration;

namespace CFS.RTG
{
    class Program
    {
        private static Timer timer;

        public Program()
        {
        }

        static void Main(string[] args)
        {
            ActiveRecordStarter.Initialize(Assembly.Load("FDM.Models"), ActiveRecordSectionHandler.Instance);
            try
            {
                int interval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["RequestInterval"]);
                timer = new System.Timers.Timer(30000);
                // Hook up the Elapsed event for the timer.
                timer.Elapsed += new ElapsedEventHandler(Timer_Elapsed);

                // Set the Interval to 2 seconds (2000 milliseconds).
                timer.Interval = interval;
                timer.Enabled = false;

                RTGWorker rtgWorker = new RTGWorker();
                rtgWorker.Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void Timer_Elapsed(object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Pulling data from ATG at {0}", e.SignalTime);
            RTGWorker rtgWorker = new RTGWorker();
            rtgWorker.Run();
        }
    }
}
