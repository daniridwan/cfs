﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;
using log4net;
using log4net.Config;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;

namespace CFS.BarcodeScanner
{
    class Program
    {
        public Program()
        {
        }

        static void Main(String[] args)
        {
            XmlConfigurator.Configure();
            ActiveRecordStarter.Initialize(Assembly.Load("CFS.Models"), ActiveRecordSectionHandler.Instance);

            ScannerElementCollection scannerCollection = null;
            scannerCollection = ScannerConfiguration.GetConfiguration().ListOfScanner;
            foreach(ScannerElement element in scannerCollection)
            {
                AsynchronousClient.StartClient(element.ID, element.IPAddress, element.Port);
            }

        }
    }
}
