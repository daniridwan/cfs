﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Timers;
using System.Configuration;
using System.IO;
using CFS.Models;

namespace CFS.BarcodeScanner
{
    public class AsynchronousClient
    {
        // ManualResetEvent instances signal completion.  
        private static ManualResetEvent connectDone =
            new ManualResetEvent(false);
        private static ManualResetEvent sendDone =
            new ManualResetEvent(false);
        private static ManualResetEvent receiveDone =
            new ManualResetEvent(false);

        // The response from the remote device.  
        private static String response = String.Empty;
        private static String mode = String.Empty;

        public static void StartClient(string ID, string address, int port)
        {
            // Connect to a remote device.  
            try
            {
                IPAddress ipAddress = IPAddress.Parse(address);
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
                mode = ID; // weight in / out
                // Create a TCP/IP socket.  
                Socket client = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect to the remote endpoint.  
                client.BeginConnect(remoteEP,
                    new AsyncCallback(ConnectCallback), client);
                connectDone.WaitOne();
                Console.WriteLine("connected");

                // Send test data to the remote device.  
                //Send(client, "This is a test<EOF>");
                //sendDone.WaitOne();

                // Receive the response from the remote device.  
                Receive(client);
                receiveDone.WaitOne();

                // Write the response to the console.  
                //Console.WriteLine("Response received : {0}", response);

                // Release the socket.  
                client.Shutdown(SocketShutdown.Both);
                client.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket client = (Socket)ar.AsyncState;

                // Complete the connection.  
                client.EndConnect(ar);

                Console.WriteLine("Socket connected to {0}",
                    client.RemoteEndPoint.ToString());

                // Signal that the connection has been made.  
                connectDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static void Receive(Socket client)
        {
            try
            {
                // Create the state object.  
                StateObject state = new StateObject();
                state.workSocket = client;
                state.AutoReset = false;
                state.Elapsed += onTimer;

                // Begin receiving the data from the remote device.  
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static void onTimer(Object source, System.Timers.ElapsedEventArgs e)
        {
            Console.WriteLine(string.Format("Receiving Data from {0}", mode));
            StateObject state = (StateObject)source;
            Console.WriteLine(state.sb.ToString());
            response = state.sb.ToString().Replace("\r\n","");
            string message = "";
            TruckLoadingPlanning plan = TruckLoadingPlanning.FindByDeliveryOrderNumber(response);
            string cfsurl = ConfigurationManager.AppSettings["CFSServerUrl"];
            if (plan != null)
            {
                if (mode == "Weight In")
                {
                    if (plan.InitialWeight == 0)
                    {
                        message = string.Format("Weight In request for {0} with DO number {1}.", plan.ProductName, plan.DeliveryOrderNumber);
                        SendNotification(message, true, "Weight In");
                    }
                }
                else if (mode == "Weight Out")
                {
                    if (plan.InitialWeight > 0)
                    {
                        message = string.Format("Weight Out request for {0} with DO number {1}", plan.ProductName, plan.DeliveryOrderNumber);
                        SendNotification(message, true, "Weight Out");
                    }
                }
            }
            else
            {
                //throw exception, misconfiguration
                Console.WriteLine("Barcode / DO not found");
            }
            //clear string builder
            state.sb.Clear();
        }

        private static void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket
                // from the asynchronous state object.  
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.workSocket;

                // Read data from the remote device.  
                int bytesRead = client.EndReceive(ar);
                //Console.WriteLine(bytesRead);
                state.Stop();
                state.Interval = Convert.ToDouble(ConfigurationManager.AppSettings["RequestInterval"]);//100;
                state.Start();

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.  
                    state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                    // Get the rest of the data.  
                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                }
                else
                {
                    // All the data has arrived; put it in response.  
                    if (state.sb.Length > 1)
                    {
                        string message = "";
                        response = state.sb.ToString();
                    }
                    // Signal that all bytes have been received.  
                    receiveDone.Set();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static void SendNotification(string message, bool status, string mode)
        {
            string server = System.Configuration.ConfigurationManager.AppSettings["NotificationServerUrl"];
            string cfsurl = ConfigurationManager.AppSettings["CFSServerUrl"];
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(server);
            request.ContentType = "application/json";
            request.Method = "POST";
            //default fail
            string icon = "glyphicon glyphicon-remove-circle";
            string stringStatus = "FAIL";
            string title = "CFS Barcode Scanner";
            if (status)
            {
                icon = "glyphicon glyphicon-ok-circle";
                stringStatus = "SUCCESS";
            }
            if(mode == "Weight In")
            {
                title = "CFS Barcode Scanner - Weight In Request"; 
            }
            else if (mode == "Weight Out")
            {
                title = "CFS Barcode Scanner - Weight Out Request";
            }
            
            //send json
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = "{\"title\":\"" + title + "\"," +
                              "\"icon\":\"" + icon + "\"," +
                              "\"text\":\"" + String.Join(" ", message) + "\"," +
                              "\"url\":\"" + cfsurl + string.Format("/Weighting?donumber={0}&mode={1}", response, mode.Replace(" ","")) + "\"," +
                              "\"status\":\"" + stringStatus + "\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var httpResponse = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine(result);
            }
        }
    }
}
