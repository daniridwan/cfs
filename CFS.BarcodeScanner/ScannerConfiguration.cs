﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace CFS.BarcodeScanner
{
    public class ScannerConfiguration : ConfigurationSection
    {
        public static ScannerConfiguration GetConfiguration()
        {
            try
            {
                return (ScannerConfiguration)System.Configuration.ConfigurationManager.GetSection("DeviceConfiguration/ScannerCollectorConfiguration");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [ConfigurationProperty("ScannerElementCollection")]
        public ScannerElementCollection ListOfScanner
        {
            get
            {
                return (ScannerElementCollection)this["ScannerElementCollection"] ?? new ScannerElementCollection();
            }
        }

        public ScannerElement GetServiceElement(string name)
        {
            ScannerElement retval = null;
            foreach (ScannerElement element in ListOfScanner)
            {
                if (element.ID == name)
                {
                    retval = element;
                    break;
                }
            }
            return retval;
        }
    }

    public class ScannerElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ScannerElement();
        }
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ScannerElement)element).ID;
        }
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }
        protected override string ElementName
        {
            get
            {
                return "ScannerElement";
            }
        }
    }

    public class ScannerElement : ConfigurationElement
    {
        [ConfigurationProperty("Id", DefaultValue = "Undefined", IsKey = true, IsRequired = true)]
        public string ID
        {
            get { return (string)this["Id"]; }
            set { this["Id"] = value; }
        }
        [ConfigurationProperty("IpAddress", IsRequired = true, DefaultValue = "127.0.0.1")]
        public string IPAddress
        {
            get { return (string)this["IpAddress"]; }
            set { this["IpAddress"] = value; }
        }
        [ConfigurationProperty("Port", IsRequired = true, DefaultValue = 502)]
        public int Port
        {
            get { return (int)this["Port"]; }
            set { this["Port"] = value; }
        }
    }
}
