﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CFS.APIModels;

namespace CFS.APIWeb.Controllers
{
    public class VisitorController : ApiController
    {
        private bool lastOperationStatus;
        // GET api/values
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET api/values/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        // api/Visitor/InsertVisitor?registrationPlate= 
        public IHttpActionResult InsertVisitor(string deliveryOrderNumber)
        {
            CFS.APIModels.Visitor newVisitor = new Visitor();
            List<string> msg = new List<string>();
            try { 
                newVisitor.Kartu = deliveryOrderNumber;
                newVisitor.Nama = "";
                newVisitor.Asal = "";
                newVisitor.Keterangan = "";
                newVisitor.Tanggal = DateTime.Now;
                newVisitor.JamInput = DateTime.Now;
                newVisitor.IsAktif = true;
                newVisitor.Save();
                lastOperationStatus = true;
                return Ok(new { result = newVisitor });
            }
            catch (Exception ex)
            {
                //return HttpResponseMessage(HttpStatusCode.BadRequest);
                return Content(HttpStatusCode.BadRequest, string.Format("Bad Request : {0}", ex.Message));
            }
        }
        // api/Visitor/GetToGateOutTransaction
        [Route("api/Visitor/GetToGateOutTransaction")]
        [HttpGet]
        public IHttpActionResult GetToGateOutTransaction()
        {
            try { 
                CFS.APIModels.Visitor[] visitor = Visitor.FindAlreadyGateOut();
                return Ok(visitor);
            }
            catch(Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, string.Format("Bad Request : {0}", ex.Message)); return null;
            }
        }

        // api/Visitor/GetToGateInTransaction
        [Route("api/Visitor/GetToGateInTransaction")]
        [HttpGet]
        public IHttpActionResult GetToGateInTransaction()
        {
            try
            {
                CFS.APIModels.Visitor[] visitor = CFS.APIModels.Visitor.FindAlreadyGateIn();
                return Ok(visitor);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, string.Format("Bad Request : {0}", ex.Message)); return null;
            }
        }
    }
}
