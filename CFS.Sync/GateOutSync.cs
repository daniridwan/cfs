﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using CFS.Web;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using System.Web.Script.Serialization;

namespace CFS.Sync
{
    class GateOutSync
    {
        public void Run()
        {
            try
            {
                //parking system
                string url = GlobalClass.parkingRoutesApi("api/Visitor", "GetToGateOutTransaction", "");
                url = url.Replace("?", "");
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                request.Accept = "application/json";
                request.ContentType = "application/json";
                request.Method = "GET";

                //string parsedContent = Newtonsoft.Json.JsonConvert.SerializeObject("");
                //ASCIIEncoding encoding = new ASCIIEncoding();
                //Byte[] bytes = encoding.GetBytes(parsedContent);
                //request.ContentLength = bytes.Length;

                //Stream dataStream = request.GetRequestStream();
                //dataStream.Write(bytes, 0, bytes.Length);
                //dataStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string content = new StreamReader(response.GetResponseStream()).ReadToEnd();
                List<CFS.APIModels.Visitor> toGateOutVisitor = new List<APIModels.Visitor>();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    toGateOutVisitor = JsonConvert.DeserializeObject<List<CFS.APIModels.Visitor>>(content);
                }
                else
                {
                    Console.WriteLine(response.ResponseUri);
                }
                CFS.Models.TruckLoading[] listToGateOut = TruckLoading.FindByShipmentStatus(2); //find yg baru selesai adm
                foreach (TruckLoading t in listToGateOut)
                {
                    foreach (CFS.APIModels.Visitor v in toGateOutVisitor)
                    {
                        if (t.DeliveryOrderNumber == v.Kartu) //jika sama
                        {
                            if (v.Keluar != null) //sudah masuk (tapping)
                            {
                                t.GateOutMode = "Auto";
                                t.GateOutLocation = "Gate Barrier";
                                t.GateOutTimestamp = v.Keluar;
                                t.ShipmentStatus = 3;
                                t.UpdateAndFlush();

                                //update do dan upload ke COS
                                foreach (TruckLoadingPlanning tlp in t.TruckLoadingPlanning)
                                {
                                    DeliveryOrderDetail dod = DeliveryOrderDetail.FindByDeliveryOrderNumber(tlp.DeliveryOrderNumber);
                                    if (tlp.FillingPointGroup == "Chemical")
                                    {
                                        dod.ActualQuantity = Convert.ToInt32(tlp.FinalWeight - tlp.InitialWeight);
                                        dod.UpdateAndFlush();
                                    }
                                    if (tlp.FillingPointGroup == "Fuel")
                                    {
                                        dod.ActualQuantity = Convert.ToSingle(tlp.Actual);
                                        dod.UpdateAndFlush();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
    }
}
