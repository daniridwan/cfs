﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using CFS.Web;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using System.Web.Script.Serialization;

namespace CFS.Sync
{
    class GateInSync
    {
        public void Run()
        {
            try
            {
                //parking system
                string url = GlobalClass.parkingRoutesApi("api/Visitor", "GetToGateInTransaction", "");
                url = url.Replace("?", "");
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                request.Accept = "application/json";
                request.ContentType = "application/json";
                request.Method = "GET";

                //string parsedContent = Newtonsoft.Json.JsonConvert.SerializeObject("");
                //ASCIIEncoding encoding = new ASCIIEncoding();
                //Byte[] bytes = encoding.GetBytes(parsedContent);
                //request.ContentLength = bytes.Length;

                //Stream dataStream = request.GetRequestStream();
                //dataStream.Write(bytes, 0, bytes.Length);
                //dataStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string content = new StreamReader(response.GetResponseStream()).ReadToEnd();
                List<CFS.APIModels.Visitor> toGateInVisitor = new List<APIModels.Visitor>();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    toGateInVisitor = JsonConvert.DeserializeObject<List<CFS.APIModels.Visitor>>(content);
                }
                else
                {
                    Console.WriteLine(response.ResponseUri);
                }
                CFS.Models.TruckLoading[] listToGateIn = TruckLoading.FindByShipmentStatus(1); //find yg baru selesai adm
                foreach(TruckLoading t in listToGateIn)
                {
                    foreach(CFS.APIModels.Visitor v in toGateInVisitor)
                    {
                        if(t.DeliveryOrderNumber == v.Kartu) //jika sama
                        {
                            if(v.Masuk != null) //sudah masuk (tapping)
                            {
                                t.GateInMode = "Auto";
                                t.GateInLocation = "Gate Barrier";
                                t.GateInTimestamp = v.Masuk;
                                t.ShipmentStatus = 2;
                                t.UpdateAndFlush();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
    }
}
