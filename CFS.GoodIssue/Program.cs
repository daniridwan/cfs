﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using log4net;
using log4net.Config;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;

namespace CFS.GoodIssue
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            ActiveRecordStarter.Initialize(Assembly.Load("CFS.Models"), ActiveRecordSectionHandler.Instance);

            try
            {
                GoodIssue gi = new GoodIssue();
                gi.Run();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
    }
}
