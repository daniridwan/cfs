﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using CFS.Web;
using System.Net;
using Newtonsoft.Json;
using System.IO;
//using System.Web.Script.Serialization;

namespace CFS.GoodIssue
{
    class GoodIssue
    {
        public void Run()
        {
            //find completed DO but not GI yet
            TruckLoading[] listToGoodIssue = TruckLoading.FindToGoodIssue();

            foreach(TruckLoading t in listToGoodIssue)
            {
                string url = GlobalClass.routesApi("DeliveryOrderList", "Update", "deliveryOrderNumber=" + t.DeliveryOrderNumber);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                request.Accept = "application/json";
                request.ContentType = "application/json";
                request.Method = "POST";
                //remove updated by
                DeliveryOrder updatedDo = DeliveryOrder.FindByShipmentNumber(t.DeliveryOrderNumber);
                updatedDo.Status = "Completed";

                //actual
                string parsedContent = Newtonsoft.Json.JsonConvert.SerializeObject(updatedDo);
                ASCIIEncoding encoding = new ASCIIEncoding();
                Byte[] bytes = encoding.GetBytes(parsedContent);
                request.ContentLength = bytes.Length;

                Stream dataStream = request.GetRequestStream();
                dataStream.Write(bytes, 0, bytes.Length);
                dataStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string content = new StreamReader(response.GetResponseStream()).ReadToEnd();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    t.IsGoodIssueSucceed = 1;
                    t.GoodIssueTimestamp = DateTime.Now;
                    t.GoodIssueMessage = "Good Issue Success";
                    updatedDo.Update();
                    t.Update();
                    Console.WriteLine(string.Format("Good Issue Succeed for {0}", t.DeliveryOrderNumber));
                }
                else
                {
                    Console.WriteLine(string.Format("Failed to update DO : {0}", t.DeliveryOrderNumber));
                }
            } 
        }
    }
}
