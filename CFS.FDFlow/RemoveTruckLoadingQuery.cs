﻿using System;
using System.Collections.Generic;

namespace CFS.FDFlow
{
    public class RemoveTruckLoadingQuery : HttpNetFlowQuery
    {
        public string Loading_Order { get; set; }
        public int Batch { get; set; }
    }
}
