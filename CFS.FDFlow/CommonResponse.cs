﻿using System;
using System.Collections.Generic;

namespace CFS.FDFlow
{
    public class CommonResponse : HttpNetFlowResponse
    {
        public CommonResponse(bool iserror, int responsecode, string message)
        {
            Response_Type = iserror;
            Response_Code = responsecode;
            Message = message;
        }
        public Boolean Response_Type { get; set; }
        public int Response_Code { get; set; }
        public string Message { get; set; }

    }
}
