﻿using System;
using System.Collections.Generic;

namespace CFS.FDFlow
{
    public class HttpNetFlowResponse
    {
        public string IPAddress { get; set; }
        public int PortNumber { get; set; }
        public TimeSpan QueryTime { get; set; }
    }
}
