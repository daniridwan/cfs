﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CFS.FDFlow
{
    public class EmptyTruckLoadingQuery : HttpNetFlowQuery
    {
        public string Transporter_Name { get; set; }
        public string Transporter_SerialNumber { get; set; }
        public string Compartment_Name { get; set; }
        public string Forwarder_Name { get; set; }
        public string Destination_Name { get; set; }
        public string Driver_Name { get; set; }
        public string Kernet_Name { get; set; }
        public string Transporter_Code { get; set; }
        public string Driver_Code { get; set; }
        public string Kernet_Code { get; set; }
        public DateTime Gate_In_Time { get; set; }
        public List<EmptyTruckLoadingQueryDetail> Details { get; set; }
        public EmptyTruckLoadingQuery()
        {
            Details = new List<EmptyTruckLoadingQueryDetail>();
        }
    }
    public class EmptyTruckLoadingQueryDetail
    {
        public string Loading_Order { get; set; }
        public string Sales_Order { get; set; }
        public string Product_Name { get; set; }
        public string Product_Code { get; set; }
        public int Batch { get; set; }
        public int PIN { get; set; }
        public int Preset { get; set; }
        public int Safe_Capacity { get; set; }
        public string Filling_Point_Name { get; set; }

    }
}
