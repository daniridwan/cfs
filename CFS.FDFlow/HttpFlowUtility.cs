﻿using System;
using System.Collections.Generic;

namespace CFS.FDFlow
{
    public static class HttpFlowUtility
    {
        public static DateTime ToCSharpTime(double CDateTime)
        {
            DateTime unixStartTime = new DateTime(1899, 12, 30, 0, 0, 0, 0);
            return unixStartTime.AddDays(CDateTime);
        }
    }
}
