﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Net;
using System.Xml;
using log4net;

namespace CFS.FDFlow
{
    public class FLOWProxy
    {
        private static FLOWProxy instance = null;
        public static FLOWProxy Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new FLOWProxy();
                }
                return instance;
            }
        }
        private static ILog logger = LogManager.GetLogger(typeof(FLOWProxy));

        private string DefaultIPAddress;
        private int DefaultPort;
        private int DefaultTimeOut;

        private FLOWProxy()
        {
            LoadConfig();
        }

        public void LoadConfig()
        {
            //FDM.Models.SystemConfiguration host = FDM.Models.SystemConfiguration.FindByKey("fdflow.host");
            //FDM.Models.SystemConfiguration port = FDM.Models.SystemConfiguration.FindByKey("fdflow.port");

            string host = System.Web.Configuration.WebConfigurationManager.AppSettings["FDFlowHost"];
            string port = System.Web.Configuration.WebConfigurationManager.AppSettings["FDFlowPort"];

            // accommodate testing using command line
            if (host == null || host.Length == 0)
            {
                host = System.Configuration.ConfigurationManager.AppSettings["FDFlowHost"];
            }
            if (port == null || port.Length == 0)
            {
                port = System.Configuration.ConfigurationManager.AppSettings["FDFlowPort"];
            }

            if (host != null && host.Length != 0)
            {
                DefaultIPAddress = host;
            }
            else
            {
                DefaultIPAddress = "localhost";
            }

            if (port != null && port.Length != 0)
            {
                int iPort = 0;
                if (int.TryParse(port, out iPort))
                {
                    DefaultPort = iPort;
                }
                else
                {
                    DefaultPort = 8010;
                }
            }
            else
            {
                DefaultPort = 8010;
            }

            DefaultTimeOut = 10000;

            //LoggerFacade.Log(Mod + "Default Configuration : " + ipaddress + "," + port + "," + timeout, Category.Info, Priority.Low);
        }

        #region INetFlowConnector Members

        public HttpNetFlowResponse SendTransactionToNetFLOW(HttpNetFlowQuery query)
        {
            this.ValidateQuery(query);

            try
            {
                if (query.IPAddress == "" || query.IPAddress == null)
                    query.IPAddress = DefaultIPAddress;
                if (query.PortNumber == 0)
                    query.PortNumber = DefaultPort;
                if (query.TimeOut == 0)
                    query.TimeOut = DefaultTimeOut;

                DateTime BeginTime = DateTime.Now;

                EmptyTruckLoadingQuery QueryGet;
                if (query.GetType() == typeof(EmptyTruckLoadingQuery))
                {
                    QueryGet = (EmptyTruckLoadingQuery)query;
                }
                else
                {
                    return new CommonResponse(false, 1, "");
                }

                foreach (EmptyTruckLoadingQueryDetail detail in QueryGet.Details)
                {
                    string requesttext = SendTransactionFromNetFLOW_CreateRequest(QueryGet, detail);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requesttext);
                    request.Timeout = QueryGet.TimeOut;
                    request.Method = "GET";

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream respstream = response.GetResponseStream();
                    TextReader reader = new StreamReader(respstream);

                    string responsetext = reader.ReadToEnd();
                }
                CommonResponse Response = new CommonResponse(true, 4, "Success");
                Response.QueryTime = DateTime.Now.Subtract(BeginTime);

                return Response;
            }
            catch (Exception)
            {
                return new CommonResponse(false, 3, "Undefined Error");
            }
        }
        private string SendTransactionFromNetFLOW_CreateRequest(EmptyTruckLoadingQuery query, EmptyTruckLoadingQueryDetail detail)
        {

            int countbatch = 0;
            string loadingorder = "";
            StringBuilder builder = new StringBuilder();
            builder.Append("http://");
            builder.Append(query.IPAddress);
            builder.Append(":");
            builder.Append(query.PortNumber.ToString());

            builder.Append("/newtransaction?id=");
            builder.Append(detail.Loading_Order);
            builder.Append("&pin=");
            builder.Append(detail.PIN);
            builder.Append("&preset=");
            builder.Append(detail.Preset);
            builder.Append("&product=");
            builder.Append(detail.Product_Name);
            builder.Append("&transportertype=");
            builder.Append("truck");
            builder.Append("&transporterID=");
            builder.Append(query.Transporter_Name);
            builder.Append("&truckserialnumber=");
            builder.Append(query.Transporter_SerialNumber);
            builder.Append("&transporter=");
            builder.Append(query.Forwarder_Name);
            builder.Append("&destinationID=");
            builder.Append(query.Destination_Name);
            builder.Append("&destinationcompany=");
            builder.Append(query.Destination_Name);
            int batchcount = 0;
            foreach (EmptyTruckLoadingQueryDetail detail1 in query.Details)
            {
                if (detail1.Loading_Order == loadingorder)
                    batchcount++;
            }
            builder.Append("&batchcnt=");
            builder.Append(countbatch);
            builder.Append("&fpname=");
            builder.Append(detail.Filling_Point_Name);
            builder.Append("&multibatch=");
            builder.Append(query.Details);
            builder.Append("&batchnumber=");
            builder.Append(detail.Batch);
            loadingorder = detail.Loading_Order;


            // Isi disini

            #region Original Query from JobHandler
            /*
            ostr << "newtransaction?id=" << t->transactionID();
            ostr << "&pin=" << t->pin();
            ostr << "&preset=" << std::dec << t->presetValue();
            ostr << "&product=" << t->product();
            ostr << "&transportertype=" << t->transporterType();
            ostr << "&transporterID=" << t->transporterID();
            ostr << "&truckserialnumber=" << t->truckSerialNumber();
            ostr << "&transporter=" << t->companyName();
            ostr << "&destinationID=" << t->destinationID();
            ostr << "&destinationcompany=" << t->destinationCompany();
            ostr << "&batchcnt=" << t->batchCount();
            ostr << "&fpname=" << t->fpName();
            std::string value = t->multiBatch() ? "true" : "false";
            ostr << "&multibatch=" << value;
            ostr << "&batchnumber=" << t->batchNumber();
            */
            #endregion

            return builder.ToString();
        }

        public HttpNetFlowResponse GetTransactionFromNetFLOW(HttpNetFlowQuery query)
        {
            //LoggerFacade.Log(Mod + " Get Transaction, Begin", Category.Debug, Priority.Low);
            //LoggerFacade.Log(Mod + " Get Transaction, Validating Query", Category.Debug, Priority.Low);
            this.ValidateQuery(query);
            //LoggerFacade.Log(Mod + " Get Transaction, Query Valid", Category.Debug, Priority.Low);
            try
            {
                DateTime BeginTime = DateTime.Now;

                FilledTruckLoadingQuery QueryGet;
                if (query.GetType() == typeof(FilledTruckLoadingQuery))
                {
                    QueryGet = (FilledTruckLoadingQuery)query;
                }
                else
                {
                    return new CommonResponse(false, 1, "");
                }

                //LoggerFacade.Log(Mod + " Get Transaction, Creating Query", Category.Debug, Priority.Low);

                string requesttext = GetTransactionFromNetFLOW_CreateRequest(QueryGet);

                //LoggerFacade.Log(Mod + " Get Transaction, Query : " + requesttext, Category.Debug, Priority.Low);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requesttext);
                request.Timeout = QueryGet.TimeOut;
                request.Method = "GET";

                //LoggerFacade.Log(Mod + " Get Transaction, Send Request", Category.Debug, Priority.Low);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream respstream = response.GetResponseStream();
                TextReader reader = new StreamReader(respstream);
                //LoggerFacade.Log(Mod + " Get Transaction, Response Received", Category.Debug, Priority.Low);
                string responsetext = reader.ReadToEnd();
                //LoggerFacade.Log(Mod + " Get Transaction, Response : " + responsetext, Category.Debug, Priority.Low);

                //LoggerFacade.Log(Mod + " Get Transaction, Parsing Response", Category.Debug, Priority.Low);
                HttpNetFlowResponse Response = GetTransactionFromNetFLOW_ParseResponse(responsetext);

                //LoggerFacade.Log(Mod + " Get Transaction, Finished Parsing", Category.Debug, Priority.Low);
                Response.QueryTime = DateTime.Now.Subtract(BeginTime);

                return Response;
            }
            catch (Exception)
            {
                return new CommonResponse(true, 3, "Undefined Error");
            }
        }
        private string GetTransactionFromNetFLOW_CreateRequest(FilledTruckLoadingQuery query)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("http://");
            builder.Append(query.IPAddress);
            builder.Append(":");
            builder.Append(query.PortNumber.ToString());
            builder.Append("/gettransactiont?transporterid=");
            builder.Append(query.Transporter_Name);

            #region Original Query from JobHandler
            /*
            ostr << "gettransactiont?";
            ostr << "transporterid=" << idTransaction;
            */
            #endregion

            return builder.ToString();
        }
        private HttpNetFlowResponse GetTransactionFromNetFLOW_ParseResponse(string response)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(response);
            //LoggerFacade.Log(Mod + " Get Transaction, Begin Parsing Response", Category.Debug, Priority.Low);
            try
            {
                XmlNode transcationlist = doc.DocumentElement;
                XmlNode transaction = transcationlist.FirstChild;

                string loadingorder = "";
                int batch = 0;
                double temperature = 0;
                double density = 0;

                FilledTruckLoadingResponse ResponseData = new FilledTruckLoadingResponse();
                while (transaction != null)
                {
                    FilledTruckLoadingItem item = new FilledTruckLoadingItem();
                    foreach (XmlAttribute attr in transaction.Attributes)
                    {
                        if (attr.Name == "completed")
                        {
                            item.IsCompleted = Convert.ToBoolean(attr.Value);
                            //LoggerFacade.Log(Mod + " Get Transaction, Parsing Complete Status : " + attr.Value, Category.Debug, Priority.None);
                        }
                        if (attr.Name == "id")
                        {
                            loadingorder = attr.Value;
                            //LoggerFacade.Log(Mod + " Get Transaction, Parsing Loading Order : " + loadingorder, Category.Debug, Priority.None);
                        }
                        if (attr.Name == "batchnumber")
                        {
                            batch = Convert.ToInt16(attr.Value);
                            //LoggerFacade.Log(Mod + " Get Transaction, Parsing Batch Number : " + batch, Category.Debug, Priority.None);
                        }
                        if (attr.Name == "temp")
                        {
                            temperature = Convert.ToDouble(attr.Value);
                            //LoggerFacade.Log(Mod + " Get Transaction, Parsing temperature : " + temperature.ToString(), Category.Debug, Priority.None);
                        }
                        if (attr.Name == "dens")
                        {
                            density = Convert.ToDouble(attr.Value);
                            //LoggerFacade.Log(Mod + " Get Transaction, Parsing Density : " + density.ToString(), Category.Debug, Priority.None);
                        }
                        if (attr.Name == "transporterID")
                        {
                            //string depan = attr.Value.Substring(0, 2);
                            //string tengah = attr.Value.Substring(3, 4);
                            //string belakang = attr.Value.Substring(6, 2);
                            //item.Transporter_Name = string.Format("{0} {1} {2}", depan, tengah, belakang);
                            item.Transporter_Name = attr.Value;
                            //LoggerFacade.Log(Mod + " Get Transaction, Parsing Truck Code : " + ResponseData.Transporter_Name, Category.Debug, Priority.None);
                        }
                    }
                    // Query the filling details
                    foreach (XmlNode sectionarray in transaction.ChildNodes)
                    {
                        foreach (XmlNode section in sectionarray.ChildNodes)
                        {
                            FilledTruckLoadingResponseDetail ResponseDataDetail = new FilledTruckLoadingResponseDetail();

                            ResponseDataDetail.Loading_Order = loadingorder;
                            ResponseDataDetail.Batch = batch;
                            ResponseDataDetail.Temperature = temperature;
                            ResponseDataDetail.Density = density;

                            foreach (XmlNode attr2 in section.ChildNodes)
                            {
                                string temp = attr2.Attributes[0].Value;
                                if (attr2.Name == "fpname")
                                {
                                    if (temp == "")
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        ResponseDataDetail.Filling_Point_Name = temp;
                                        //LoggerFacade.Log(Mod + " Get Transaction, Parsing - fpname :" + temp, Category.Debug, Priority.None);
                                    }
                                }
                                if (attr2.Name == "loaded")
                                {
                                    ResponseDataDetail.Actual = Convert.ToInt16(temp);
                                    //LoggerFacade.Log(Mod + " Get Transaction, Parsing - loaded :" + temp, Category.Debug, Priority.None);
                                }
                                if (attr2.Name == "starttime")
                                {
                                    double temptime = 0;
                                    Double.TryParse(temp, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out temptime);
                                    ResponseDataDetail.Start_Filling = HttpFlowUtility.ToCSharpTime(temptime);
                                    //double temptime = Convert.ToDouble(temp);
                                    //ResponseDataDetail.Start_Filling = HttpFlowUtility.ToCSharpTime(temptime);
                                    //LoggerFacade.Log(Mod + " Get Transaction, Parsing - starttime :" + temptime.ToString(), Category.Debug, Priority.None);
                                    //LoggerFacade.Log(Mod + " Get Transaction, Parsing - starttime :" + HttpFlowUtility.ToCSharpTime(temptime).ToString(), Category.Debug, Priority.None);
                                }
                                if (attr2.Name == "starttotal")
                                {
                                    ResponseDataDetail.Start_Totalizer = Convert.ToDouble(temp);
                                    //LoggerFacade.Log(Mod + " Get Transaction, Parsing - starttotal :" + temp.ToString(), Category.Debug, Priority.None);
                                }
                                if (attr2.Name == "stoptime")
                                {
                                    double temptime = 0;
                                    Double.TryParse(temp, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out temptime);
                                    ResponseDataDetail.End_Filling = HttpFlowUtility.ToCSharpTime(temptime);
                                    //double temptime = Convert.ToDouble(temp);
                                    //ResponseDataDetail.End_Filling = HttpFlowUtility.ToCSharpTime(temptime);
                                    //LoggerFacade.Log(Mod + " Get Transaction, Parsing - stoptime :" + temptime.ToString(), Category.Debug, Priority.None);
                                    //LoggerFacade.Log(Mod + " Get Transaction, Parsing - stoptime :" + HttpFlowUtility.ToCSharpTime(temptime).ToString(), Category.Debug, Priority.None);
                                }
                                if (attr2.Name == "endtotal")
                                {
                                    ResponseDataDetail.Stop_Totalizer = Convert.ToDouble(temp);
                                    //LoggerFacade.Log(Mod + " Get Transaction, Parsing - endtotal :" + temp.ToString(), Category.Debug, Priority.None);
                                }
                                if (attr2.Name == "preset")
                                {
                                    ResponseDataDetail.Preset = Convert.ToInt16(temp);
                                    //LoggerFacade.Log(Mod + " Get Transaction, Parsing - preset :" + temp.ToString(), Category.Debug, Priority.None);
                                }
                                if (attr2.Name == "interrupted")
                                {
                                    ResponseDataDetail.Is_Interrupted = Convert.ToBoolean(temp);
                                    //LoggerFacade.Log(Mod + " Get Transaction, Parsing - interrupted :" + temp.ToString(), Category.Debug, Priority.None);
                                }
                            }
                            if (ResponseDataDetail.Filling_Point_Name != null)
                                item.Details.Add(ResponseDataDetail);
                        }
                    }
                    // Summarize Others

                    DateTime StartTime = DateTime.MaxValue;
                    DateTime StopTime = DateTime.MinValue;
                    double TotalActual = 0;
                    double TotalPreset = 0;
                    foreach (FilledTruckLoadingResponseDetail detail in item.Details)
                    {
                        if (StartTime > detail.Start_Filling)
                            StartTime = detail.Start_Filling;
                        if (StopTime < detail.End_Filling)
                            StopTime = detail.End_Filling;
                        TotalActual += detail.Actual;
                        TotalPreset += detail.Preset;
                    }

                    if (StartTime != DateTime.MinValue)
                        item.Start_Filling_Time = StartTime;

                    if (StopTime != DateTime.MinValue)
                        item.Stop_Filling_Time = StopTime;

                    item.Total_Actual = Convert.ToInt32(TotalActual);
                    item.Total_Preset = Convert.ToInt32(TotalPreset);

                    ResponseData.Items.Add(item);

                    transaction = transaction.NextSibling;
                }
                //LoggerFacade.Log(Mod + " Get Transaction, End Parsing Response", Category.Debug, Priority.Low);
                return ResponseData;
            }
            catch (Exception ex)
            {
                //LoggerFacade.Log(Mod + " Get Transaction, Exception :" + ex.Message, Category.Debug, Priority.Low);
                return new CommonResponse(true, 2, "Error Parsing");
            }
        }

        public HttpNetFlowResponse RemoveTransactionFromNetFLOW(HttpNetFlowQuery query)
        {
            this.ValidateQuery(query);
            try
            {
                DateTime BeginTime = DateTime.Now;

                RemoveTruckLoadingQuery QueryGet;
                if (query.GetType() == typeof(RemoveTruckLoadingQuery))
                {
                    QueryGet = (RemoveTruckLoadingQuery)query;
                }
                else
                {
                    return new CommonResponse(false, 1, "");
                }

                string requesttext = RemoveTransactionFromNetFLOW_CreateRequest(QueryGet);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requesttext);
                request.Timeout = QueryGet.TimeOut;
                request.Method = "GET";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream respstream = response.GetResponseStream();
                TextReader reader = new StreamReader(respstream);
                string responsetext = reader.ReadToEnd();

                HttpNetFlowResponse Response = GetTransactionFromNetFLOW_ParseResponse(responsetext);

                Response.QueryTime = DateTime.Now.Subtract(BeginTime);

                return Response;
            }
            catch (Exception)
            {
                return new CommonResponse(true, 3, "Undefined Error");
            }
        }
        private string RemoveTransactionFromNetFLOW_CreateRequest(RemoveTruckLoadingQuery query)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("http://");
            builder.Append(query.IPAddress);
            builder.Append(":");
            builder.Append(query.PortNumber.ToString());

            builder.Append("/deletetransaction?id=");
            builder.Append(query.Loading_Order);
            builder.Append("&batchnumber=");
            builder.Append(query.Batch);
            #region Original Query from JobHandler
            /*
            ostr << "deletetransaction?id=" << idTransaction;
            ostr << "&batchnumber=" << std::dec << _batchNumber;
            */
            #endregion
            return builder.ToString();
        }

        private void ValidateQuery(HttpNetFlowQuery Query)
        {
            if (Query.IPAddress == "" || Query.IPAddress == null)
                Query.IPAddress = DefaultIPAddress;
            if (Query.PortNumber == 0)
                Query.PortNumber = DefaultPort;
            if (Query.TimeOut == 0)
                Query.TimeOut = DefaultTimeOut;
        }
        #endregion
    }
}
