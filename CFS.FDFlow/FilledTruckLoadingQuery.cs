﻿using System;
using System.Collections.Generic;

namespace CFS.FDFlow
{
    public class FilledTruckLoadingQuery : HttpNetFlowQuery
    {
        public string Transporter_Name { get; set; }
    }
}
