﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CFS.FDFlow
{
    public class HttpNetFlowQuery
    {
        public string IPAddress { get; set; }
        public int PortNumber { get; set; }
        public int TimeOut { get; set; }
        public string RequestMethod { get; set; }
    }
}

