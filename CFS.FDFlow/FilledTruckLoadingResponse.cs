﻿using System;
using System.Collections.Generic;

namespace CFS.FDFlow
{
    public class FilledTruckLoadingResponse : HttpNetFlowResponse
    {
        public List<FilledTruckLoadingItem> Items { get; set; }
        public FilledTruckLoadingResponse()
        {
            Items = new List<FilledTruckLoadingItem>();
        }
    }


    public class FilledTruckLoadingItem
    {
        public string Transporter_Name { get; set; }
        public string Compartment_Name { get; set; }
        public DateTime? Gate_In_Time { get; set; }
        public DateTime? Start_Filling_Time { get; set; }
        public DateTime? Stop_Filling_Time { get; set; }
        public int Total_Preset { get; set; }
        public int Total_Actual { get; set; }
        public List<FilledTruckLoadingResponseDetail> Details { get; set; }
        public bool IsCompleted { get; set; }
        public FilledTruckLoadingItem()
        {
            Details = new List<FilledTruckLoadingResponseDetail>();
        }
    }
    /// <summary>
    /// Sub Class for details
    /// </summary>
    public class FilledTruckLoadingResponseDetail
    {
        public string Loading_Order { get; set; }
        public int Batch { get; set; }
        public int Preset { get; set; }
        public int Actual { get; set; }
        public double Start_Totalizer { get; set; }
        public double Stop_Totalizer { get; set; }
        public DateTime Start_Filling { get; set; }
        public DateTime End_Filling { get; set; }
        public string Filling_Point_Name { get; set; }
        public double Temperature { get; set; }
        public double Density { get; set; }
        public Boolean Is_Interrupted { get; set; }
    }
}
