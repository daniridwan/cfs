﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Net;
using System.IO;
using System.Diagnostics;


namespace CFS.Global
{
    class GlobalClass
    {
        public static bool onlyNumber(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                return true;
            else
                return false;
        }

        public static String routesApi(string controller, string action, string param)
        {
            string apiServer = ConfigurationManager.AppSettings["apiServer"];

            string url = string.Format("{0}/{1}/{2}?{3}", apiServer, controller, action, param);
            return url;
        }

        public static String RandomString(int length)
        {
            const string pool = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random rnd = new Random();
            var chars = Enumerable.Range(0, length)
                .Select(x => pool[rnd.Next(0, pool.Length)]);
            return new string(chars.ToArray());
        }

        public static void SendToPrinter(string printerPath, string fileName)
        {
            if (String.IsNullOrEmpty(fileName))
                return;

            var url = fileName;
            var filePath = String.Format(@"{0}\{1}.pdf", Path.GetTempPath(), Guid.NewGuid().ToString());
            using (var client = new WebClient())
            {
                client.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                client.DownloadFile(url, filePath);
            }

            ProcessStartInfo gsProcessInfo = new ProcessStartInfo();
            Process gsProcess = new Process();

            //if (printerPath == "Microsoft XPS Document Writer")
            //    gsProcessInfo.Verb = "Print";
            //else
            //    gsProcessInfo.Verb = "printto";
            //gsProcessInfo.FileName = filePath;
            //gsProcessInfo.Arguments = printerPath;

            string gsArguments = string.Format("-print-to \"{0}\" \"{1}\" -exit-on-print", printerPath, filePath);
            string gsLocation = Path.Combine(Environment.CurrentDirectory, "SumatraPDF.exe");
            gsProcessInfo.FileName = gsLocation;
            gsProcessInfo.Arguments = gsArguments;
            gsProcessInfo.CreateNoWindow = true;
            gsProcessInfo.WindowStyle = ProcessWindowStyle.Hidden;

            gsProcess = Process.Start(gsProcessInfo);
            gsProcess.WaitForExit(60);

        }

    }
}
