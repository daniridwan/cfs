﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using log4net.Config;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;
using System.Reflection;

namespace CFS.Gate
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //XmlConfigurator.Configure();
            ActiveRecordStarter.Initialize(Assembly.Load("FDM.Models"), ActiveRecordSectionHandler.Instance);

            Application.Run(new Gate());
        }
    }
}
