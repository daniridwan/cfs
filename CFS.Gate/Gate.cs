﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CFS.Models;
using CFS.Biostar;
using CFS.Global;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Runtime.InteropServices;
using System.Timers;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.SignalR.Client.Hubs;
using System.Collections.Specialized;
using System.Timers;

namespace CFS.Gate
{
    public partial class Gate : Form
    {
        #region variable
        private string flagSubmit = "";
        private API.OnLogReceived cbOnLogReceived = null;
        private BS2ErrorCode result;
        BS_Program bs = new BS_Program();
        private int counter;
        private int maxCounter = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["maxCounter"]);
        private System.Timers.Timer aTimer = new System.Timers.Timer();
        #endregion 

        public Gate()
        {
            InitializeComponent();
        }

        delegate void SetTextCallback(string text, Control target);
        private void SetText(string text, Control target)
        {
            if (this.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text, target });
            }
            else
            {
                target.Text = text;
            }
        }

        private void startScan()
        {
            if (bs.DeviceID == 0)
            {
                bs.init();
                bs.Msg.Clear();
                string deviceIpAddress = System.Configuration.ConfigurationManager.AppSettings["ScanerIpAddress"];
                if (!bs.ConnectToDevice(deviceIpAddress))
                {
                    addLogMsg(string.Join(Environment.NewLine, bs.Msg));
                    SetText("Device connection failed", lblResult);
                    lblResult.BackColor = Color.Red;
                    DialogResult dialogResult = MessageBox.Show("Failed to connect to device." + "\n" + "Please try to connect again.", "Connecting Device", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                        startScan();
                    else
                        //this.Close();
                    return;
                }
            }

            cbOnLogReceived = new API.OnLogReceived(RealtimeLogReceived);
            result = (BS2ErrorCode)API.BS2_StartMonitoringLog(bs.SdkContext, bs.DeviceID, cbOnLogReceived);
            addLogMsg(string.Join(Environment.NewLine, bs.Msg));
        }

        private void stopScan()
        {
            result = (BS2ErrorCode)API.BS2_StopMonitoringLog(bs.SdkContext, bs.DeviceID);
        }

        private void addLogMsg(string msg)
        {
            var tmpMsg = (string)richTxtLogMsg.Invoke(new Func<string>(() => richTxtLogMsg.Text));
            SetText(DateTime.Now + ": " + msg + "\n" + tmpMsg, richTxtLogMsg);
        }

        private void RealtimeLogReceived(UInt32 deviceID, IntPtr log)
        {
            try
            {
                if (log != IntPtr.Zero)
                {
                    BS2Event eventLog = (BS2Event)Marshal.PtrToStructure(log, typeof(BS2Event));
                    string userID = System.Text.Encoding.ASCII.GetString(eventLog.userID).TrimEnd('\0'); //id of a smart card
                    Truck truck = Truck.FindByIdentificationNumber(userID);
                    ClearViewCompartment(); //clear boxes
                    if (truck == null){
                        SetText("Verification Failed.", lblResult);
                        SetText("_______", lblTruckRegistrationPlate);
                        lblResult.BackColor = Color.Red;
                        addLogMsg("Unregistered card, please contact dispatch.");
                    }
                    else{ //truck registered, find ongoing transaction
                        TruckLoading currentLoading = TruckLoading.FindCurrentLoading(truck.TruckId);
                        if (currentLoading == null){
                            SetText("Verification Failed.", lblResult);
                            SetText(truck.RegistrationPlate, lblTruckRegistrationPlate);
                            lblResult.BackColor = Color.Red;
                            addLogMsg(string.Format("No shipment, please contact Dispatch.", truck.RegistrationPlate));
                        }
                        else{
                            if (currentLoading.GateInTimestamp == null){ // belum gate in, lanjut ke proses gate in
                                SetText("Processing Gate In.", lblResult);
                                lblResult.BackColor = Color.Yellow;
                                SetText(truck.RegistrationPlate, lblTruckRegistrationPlate);
                                addLogMsg(string.Format("Processing : Gate In for {0}", truck.RegistrationPlate));
                                flagSubmit = "Gate In";
                            }
                            else{//proses gate out
                                SetText("Processing Gate Out.", lblResult);
                                lblResult.BackColor = Color.Yellow;
                                SetText(truck.RegistrationPlate, lblTruckRegistrationPlate);
                                addLogMsg(string.Format("Processing : Gate Out for {0}", truck.RegistrationPlate));
                                flagSubmit = "Gate Out";
                            }
                        var msg = Save(flagSubmit, currentLoading);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                addLogMsg(string.Format("Failed : {0}", ex.Message.ToString()));
            }
        }

        private void Gate_Shown(object sender, EventArgs e)
        {
            startScan();
        }

        private List<string> Save(string flagSubmit, TruckLoading toSubmit)
        {
            bool lastOperationStatus = true;
            List<string> lastMessages = new List<string>();
            if (flagSubmit == "Gate In")
            {
                try
                {
                    toSubmit.GateInTimestamp = DateTime.Now;
                    toSubmit.GateInMode = "Auto";
                    toSubmit.GateInUser = "System";
                    toSubmit.GateInLocation = "Gate In";
                    //send to API
                    string url = GlobalClass.routesApi("DistributionMonitoring", "GateIn", "TruckLoadingId=" + toSubmit.TruckLoadingId);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                    request.Accept = "application/json";
                    request.ContentType = "application/json";
                    request.Method = "POST";

                    string parsedContent = Newtonsoft.Json.JsonConvert.SerializeObject(toSubmit);
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    Byte[] bytes = encoding.GetBytes(parsedContent);
                    request.ContentLength = bytes.Length;

                    Stream dataStream = request.GetRequestStream();
                    dataStream.Write(bytes, 0, bytes.Length);
                    dataStream.Close();

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    string content = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        JObject resultJson = JObject.Parse(content);
                        var success = (bool)resultJson["success"];

                        if (success == true)
                        {
                            JObject arrayRecord = (JObject)resultJson["record"];
                            lastOperationStatus = true;
                            lastMessages.Add("Submit Success");
                            addLogMsg(string.Format("Gate In Success {0}", toSubmit.TruckRegistrationPlate));
                            SetText("Gate In Success.", lblResult);
                            lblResult.BackColor = Color.Green;
                            UpdateViewCompartment(toSubmit);
                            Print(flagSubmit, toSubmit.TruckLoadingId);
                        }
                        else
                        {
                            JArray arrayMessage = (JArray)resultJson["messages"];
                            lastOperationStatus = false;
                            lastMessages.Add(string.Join(Environment.NewLine, arrayMessage));
                            SetText("Gate In Failed.", lblResult);
                            lblResult.BackColor = Color.Red;
                            addLogMsg(string.Format("Gate In Failed : {0}", String.Join(", ", lastMessages.ToArray())));
                        }
                    }
                    else
                    {
                        lastOperationStatus = false;
                        lastMessages.Add(response.StatusDescription);
                        lblResult.BackColor = Color.Red;
                        addLogMsg(string.Format("Gate In Failed : {0}", lastMessages.ToString()));
                    }
                }
                catch (Exception ex)
                {
                    lastOperationStatus = false;
                    lblResult.BackColor = Color.Red;
                    lastMessages.Add(ex.Message);
                }
            }
            else //gate out
            {
                try
                {
                    toSubmit.GateOutTimestamp = DateTime.Now;
                    toSubmit.GateOutMode = "Auto";
                    toSubmit.GateOutUser = "System";
                    toSubmit.GateOutLocation = "Gate Out";
                    //send to API
                    string url = GlobalClass.routesApi("DistributionMonitoring", "GateOut", "TruckLoadingId=" + toSubmit.TruckLoadingId);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                    request.Accept = "application/json";
                    request.ContentType = "application/json";
                    request.Method = "POST";

                    string parsedContent = Newtonsoft.Json.JsonConvert.SerializeObject(toSubmit);
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    Byte[] bytes = encoding.GetBytes(parsedContent);
                    request.ContentLength = bytes.Length;

                    Stream dataStream = request.GetRequestStream();
                    dataStream.Write(bytes, 0, bytes.Length);
                    dataStream.Close();

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    string content = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        JObject resultJson = JObject.Parse(content);
                        var success = (bool)resultJson["success"];

                        if (success == true)
                        {
                            JObject arrayRecord = (JObject)resultJson["record"];
                            lastOperationStatus = true;
                            lastMessages.Add("Submit Success");
                            addLogMsg(string.Format("Gate Out Success {0}", toSubmit.TruckRegistrationPlate));
                            SetText("Gate Out Success.", lblResult);
                            lblResult.BackColor = Color.Green;
                            Print(flagSubmit, toSubmit.TruckLoadingId);
                            UpdateViewCompartment(toSubmit);
                        }
                        else
                        {
                            JArray arrayMessage = (JArray)resultJson["messages"];
                            lastOperationStatus = false;
                            lastMessages.Add(string.Join(Environment.NewLine, arrayMessage));
                            SetText("Gate Out Failed.", lblResult);
                            lblResult.BackColor = Color.Red;
                            addLogMsg(string.Format("Gate Out Failed : {0}", String.Join(", ", lastMessages.ToArray())));
                        }
                    }
                    else
                    {
                        lastOperationStatus = false;
                        lastMessages.Add(response.StatusDescription);
                        lblResult.BackColor = Color.Red;
                        addLogMsg(string.Format("Gate Out Failed : {0}", lastMessages.ToString()));
                    }
                }
                catch (Exception ex)
                {
                    lastOperationStatus = false;
                    lblResult.BackColor = Color.Red;
                    lastMessages.Add(ex.Message);
                }
            }
            return lastMessages;
        }

        public void UpdateViewCompartment(TruckLoading currentLoading)
        {
            //find again, because fp value wasnt refreshed.
            IList<string> listDO = new List<string>();
            currentLoading = TruckLoading.FindById(currentLoading.TruckLoadingId);
            foreach (TruckLoadingPlanning plan in currentLoading.TruckLoadingPlanning)
            {
                if (!listDO.Contains(plan.DeliveryOrderNumber))
                {
                    listDO.Add(plan.DeliveryOrderNumber);
                }
            }
            int boxCount = 1;
            foreach(string deliveryOrder in listDO)
            {
                DeliveryOrder DO = DeliveryOrder.FindByDeliveryOrderNumber(deliveryOrder); //get do material
                Product product = Product.FindByProductCode(DO.Material); // for bg color
                TruckLoadingPlanning[] plan = TruckLoadingPlanning.FindByDeliveryOrderNumber(deliveryOrder); //for filling point list and preset
                if (boxCount == 1)
                {
                    Color color = System.Drawing.ColorTranslator.FromHtml(product.HexColor);
                    //richTextBox1.BackColor = color;
                    StringBuilder sb = new StringBuilder();
                    sb.Append(product.ProductName.Replace(",BULK","") + "\n");
                    sb.Append("---------------------\n");
                    foreach (TruckLoadingPlanning p in plan)
                    {
                        sb.Append(string.Format("{0} \n",p.FillingPointName));
                        sb.Append(string.Format("{0} - {1}L \n", p.PIN, p.Preset));
                        sb.Append("---------------------\n");
                    }
                    SetText(sb.ToString(), richTextBox1);
                }
                else if (boxCount == 2)
                {
                    Color color = System.Drawing.ColorTranslator.FromHtml(product.HexColor);
                    richTextBox2.BackColor = color;
                    StringBuilder sb = new StringBuilder();
                    sb.Append(product.ProductName.Replace(",BULK", "") + "\n");
                    sb.Append("---------------------\n");
                    foreach (TruckLoadingPlanning p in plan)
                    {
                        sb.Append(string.Format("{0} \n", p.FillingPointName));
                        sb.Append(string.Format("{0} - {1}L \n", p.PIN, p.Preset));
                        sb.Append("---------------------\n");
                    }
                    SetText(sb.ToString(), richTextBox2);
                }
                else if (boxCount == 3)
                {
                    Color color = System.Drawing.ColorTranslator.FromHtml(product.HexColor);
                    richTextBox3.BackColor = color;
                    StringBuilder sb = new StringBuilder();
                    sb.Append(product.ProductName.Replace(",BULK", "") + "\n");
                    sb.Append("---------------------\n");
                    foreach (TruckLoadingPlanning p in plan)
                    {
                        sb.Append(string.Format("{0} \n", p.FillingPointName));
                        sb.Append(string.Format("{0} - {1}L \n", p.PIN, p.Preset));
                        sb.Append("---------------------\n");
                    }
                    SetText(sb.ToString(), richTextBox3);
                }
                else if (boxCount == 4)
                {
                    Color color = System.Drawing.ColorTranslator.FromHtml(product.HexColor);
                    richTextBox4.BackColor = color;
                    StringBuilder sb = new StringBuilder();
                    sb.Append(product.ProductName.Replace(",BULK", "") + "\n");
                    sb.Append("---------------------\n");
                    foreach (TruckLoadingPlanning p in plan)
                    {
                        sb.Append(string.Format("{0} \n", p.FillingPointName));
                        sb.Append(string.Format("{0} - {1}L \n", p.PIN, p.Preset));
                        sb.Append("---------------------\n");
                    }
                    SetText(sb.ToString(), richTextBox4);
                }
                else if (boxCount == 5)
                {
                    Color color = System.Drawing.ColorTranslator.FromHtml(product.HexColor);
                    richTextBox5.BackColor = color;
                    StringBuilder sb = new StringBuilder();
                    sb.Append(product.ProductName.Replace(",BULK", "") + "\n");
                    sb.Append("---------------------\n");
                    foreach (TruckLoadingPlanning p in plan)
                    {
                        sb.Append(string.Format("{0} \n", p.FillingPointName));
                        sb.Append(string.Format("{0} - {1}L \n", p.PIN, p.Preset));
                        sb.Append("---------------------\n");
                    }
                    SetText(sb.ToString(), richTextBox5); 
                }
                else
                {
                }
                boxCount++;
            }
        }

        public void ClearViewCompartment()
        {
            SetText("", richTextBox1);
            SetText("", richTextBox2);
            SetText("", richTextBox3);
            SetText("", richTextBox4);            
            SetText("", richTextBox5);
            SetText("", lblResult);
            SetText("", lblTruckRegistrationPlate);
        }

        public void Print(string flagSubmit, int truckLoadingId)
        {
            string thermalPrinterPath = ConfigurationManager.AppSettings["ThermalPrinter"];
            string printerPath = ConfigurationManager.AppSettings["Printer"];
            string filePath;
            if (flagSubmit == "Gate In")
            {
                try
                {
                    //print loading info
                    filePath = GlobalClass.routesApi("HttpHandlers", "LoadingInfoPdfExporter.ashx", "id=" + truckLoadingId);
                    //addLogMsg(filePath);
                    GlobalClass.SendToPrinter(thermalPrinterPath, filePath);
                    SetText("Printing Loading Info", lblResult);
                    addLogMsg("Printing Loading Info");
                }
                catch (Exception ex)
                {
                    SetText(ex.Message, lblResult);
                    addLogMsg(ex.Message);
                }
            }
            if (flagSubmit == "Gate Out")
            {
                try
                {
                    //print loading info
                    filePath = GlobalClass.routesApi("HttpHandlers", "DeliveryReportPdfExporter.ashx", "id=" + truckLoadingId);
                    //addLogMsg(filePath);
                    GlobalClass.SendToPrinter(printerPath, filePath);
                    SetText("Printing Delivery Report", lblResult);
                    addLogMsg("Printing Delivery Report");
                }
                catch (Exception ex)
                {
                    SetText(ex.Message, lblResult);
                    addLogMsg(ex.Message);
                }
            }
        }
    }
}
