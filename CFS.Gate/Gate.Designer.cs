﻿namespace CFS.Gate
{
    partial class Gate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Gate));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.richTxtLogMsg = new System.Windows.Forms.RichTextBox();
            this.truckLoadingGroupBox1 = new System.Windows.Forms.GroupBox();
            this.lblTruckRegistrationPlate = new System.Windows.Forms.Label();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.lblResult = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.truckLoadingGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox2.Controls.Add(this.richTxtLogMsg);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox2.Location = new System.Drawing.Point(1279, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(293, 999);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Log Message";
            // 
            // richTxtLogMsg
            // 
            this.richTxtLogMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTxtLogMsg.Location = new System.Drawing.Point(6, 29);
            this.richTxtLogMsg.Name = "richTxtLogMsg";
            this.richTxtLogMsg.Size = new System.Drawing.Size(281, 964);
            this.richTxtLogMsg.TabIndex = 0;
            this.richTxtLogMsg.Text = "";
            // 
            // truckLoadingGroupBox1
            // 
            this.truckLoadingGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.truckLoadingGroupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.truckLoadingGroupBox1.BackColor = System.Drawing.Color.Navy;
            this.truckLoadingGroupBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("truckLoadingGroupBox1.BackgroundImage")));
            this.truckLoadingGroupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.truckLoadingGroupBox1.Controls.Add(this.lblTruckRegistrationPlate);
            this.truckLoadingGroupBox1.Controls.Add(this.richTextBox5);
            this.truckLoadingGroupBox1.Controls.Add(this.richTextBox4);
            this.truckLoadingGroupBox1.Controls.Add(this.richTextBox3);
            this.truckLoadingGroupBox1.Controls.Add(this.richTextBox2);
            this.truckLoadingGroupBox1.Controls.Add(this.richTextBox1);
            this.truckLoadingGroupBox1.Controls.Add(this.lblResult);
            this.truckLoadingGroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.truckLoadingGroupBox1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.truckLoadingGroupBox1.Location = new System.Drawing.Point(12, 6);
            this.truckLoadingGroupBox1.Name = "truckLoadingGroupBox1";
            this.truckLoadingGroupBox1.Size = new System.Drawing.Size(1261, 999);
            this.truckLoadingGroupBox1.TabIndex = 0;
            this.truckLoadingGroupBox1.TabStop = false;
            this.truckLoadingGroupBox1.Text = "Truck Loading Information";
            // 
            // lblTruckRegistrationPlate
            // 
            this.lblTruckRegistrationPlate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTruckRegistrationPlate.AutoSize = true;
            this.lblTruckRegistrationPlate.BackColor = System.Drawing.Color.Transparent;
            this.lblTruckRegistrationPlate.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTruckRegistrationPlate.ForeColor = System.Drawing.Color.White;
            this.lblTruckRegistrationPlate.Location = new System.Drawing.Point(466, 702);
            this.lblTruckRegistrationPlate.Name = "lblTruckRegistrationPlate";
            this.lblTruckRegistrationPlate.Size = new System.Drawing.Size(299, 76);
            this.lblTruckRegistrationPlate.TabIndex = 1;
            this.lblTruckRegistrationPlate.Text = "_______";
            this.lblTruckRegistrationPlate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // richTextBox5
            // 
            this.richTextBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox5.ForeColor = System.Drawing.Color.Black;
            this.richTextBox5.Location = new System.Drawing.Point(1033, 330);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.ReadOnly = true;
            this.richTextBox5.Size = new System.Drawing.Size(140, 180);
            this.richTextBox5.TabIndex = 10;
            this.richTextBox5.Text = "";
            // 
            // richTextBox4
            // 
            this.richTextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox4.ForeColor = System.Drawing.Color.Black;
            this.richTextBox4.Location = new System.Drawing.Point(887, 330);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.ReadOnly = true;
            this.richTextBox4.Size = new System.Drawing.Size(140, 180);
            this.richTextBox4.TabIndex = 9;
            this.richTextBox4.Text = "";
            // 
            // richTextBox3
            // 
            this.richTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.ForeColor = System.Drawing.Color.Black;
            this.richTextBox3.Location = new System.Drawing.Point(741, 330);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ReadOnly = true;
            this.richTextBox3.Size = new System.Drawing.Size(140, 180);
            this.richTextBox3.TabIndex = 8;
            this.richTextBox3.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.ForeColor = System.Drawing.Color.Black;
            this.richTextBox2.Location = new System.Drawing.Point(595, 330);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(140, 180);
            this.richTextBox2.TabIndex = 7;
            this.richTextBox2.Text = "";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.ForeColor = System.Drawing.Color.Black;
            this.richTextBox1.Location = new System.Drawing.Point(449, 330);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(140, 180);
            this.richTextBox1.TabIndex = 6;
            this.richTextBox1.Text = "";
            // 
            // lblResult
            // 
            this.lblResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResult.AutoSize = true;
            this.lblResult.BackColor = System.Drawing.Color.Green;
            this.lblResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResult.ForeColor = System.Drawing.Color.White;
            this.lblResult.Location = new System.Drawing.Point(152, 99);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(140, 46);
            this.lblResult.TabIndex = 0;
            this.lblResult.Text = "Ready";
            this.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Gate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Navy;
            this.ClientSize = new System.Drawing.Size(1584, 1011);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.truckLoadingGroupBox1);
            this.Name = "Gate";
            this.Text = "FDM - Gate";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.Gate_Shown);
            this.groupBox2.ResumeLayout(false);
            this.truckLoadingGroupBox1.ResumeLayout(false);
            this.truckLoadingGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox truckLoadingGroupBox1;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.RichTextBox richTxtLogMsg;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label lblTruckRegistrationPlate;
    }
}