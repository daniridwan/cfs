﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //console.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-role').jqGrid(
        {
            pager: 'tbl-role-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },
            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', 'Role Id', '', 'Role Name'],
            colModel: // {name: nama_didatabase}
		[
		        { name: 'No', sortable: false, width: 40, hidden: false },
				{ name: 'RoleId', sortable: false, width: 1, hidden: true },
				{ name: 'view', width: 18, sortable: false, hidden: false },
//                { name: 'del', width: 18, sortable: false, hidden: false },
				{ name: 'Name', sortable: true, index: 'Name', width: 200, hidden: false }
   		],
            viewrecords: true,
            sortorder: "asc",
            sortname: "RoleId",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
            altclass: 'even',
            altRows: true
        }).navGrid('#tbl-roles-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {
            $('#tbl-role')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    // reset filter js
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    //refresh js
    refresh: function () {
        try {
            this.gridObj.trigger('reloadGrid');
        } catch (er) {
            var $thisgridObj = $('#tbl-role').jqGrid();
            $thisgridObj.trigger('reloadGrid');
        }
    },
    OnWindowResize: function () {
        $('#tbl-role').setGridWidth($('#pnl-frame').width(), false).triggerHandler('resize');
    }
});
