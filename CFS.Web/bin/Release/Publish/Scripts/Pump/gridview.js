﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-pump').jqGrid(
        {
            pager: 'tbl-pump-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', 'Pump Name', 'Flowrate (m<sup>3</sup>/h)', 'Motor (kW)', 'Merk', 'Tipe', 'Jenis', 'Explosion Code', 'Kelas'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 18, sortable: false, hidden: false },
                { name: 'Name', width: 120, sortable: true },
                { name: 'Flowrate', width: 135, sortable: true },
                { name: 'Motor', width: 110, sortable: true },
                { name: 'Merk', width: 120, sortable: true },
                { name: 'Tipe', width: 100, sortable: true },
                { name: 'Jenis', width: 100, sortable: true },
                { name: 'ExplosionCode', width: 125, sortable: true },
                { name: 'Kelas', width: 90, sortable: true },
		],
            viewrecords: true,
            sortorder: "asc",
            sortname: "PumpId",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-pump-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {            
            $('#tbl-pump')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-pump').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

