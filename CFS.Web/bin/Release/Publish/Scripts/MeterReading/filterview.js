﻿FDM.View.FilterView = Backbone.View.extend({
    events: {
        "change select": "onDropDownChanged",
        "keypress input[type='text']": "onTextKeypress",
        "click .cmd-reset-filter": "onFilterReset",
		"click .cmd-refresh": "onRefresh",
		"click .cmd-export": "onExport",
        "change #flt-timestamp-to":"onTimestampToChange",
    },
    render: function () {
        // don't forget to call ancestor 's render()
        C.FormView.prototype.render.call(this);
        var dateFrom = { altField:"#hdn-timestamp-from", altFormat: "yy-mm-ddT00:00:00",
            dateFormat: 'dd-MM-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true, maxDate: new Date
        };
        var dateTo = { altField:"#hdn-timestamp-to", altFormat: "yy-mm-ddT00:00:00",
            dateFormat: 'dd-MM-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true, maxDate: new Date
        };

        $(".datepicker").datepicker({
        beforeShow: function (input, inst) {
            setTimeout(function () {
                inst.dpDiv.css({
                    top: $(".datepicker").offset().top + 35,
                    left: $(".datepicker").offset().left
                });
            }, 0);
        }
        });

        $('#flt-timestamp-from').datepicker(_.extend(dateFrom, {}));
        $('#flt-timestamp-to').datepicker(_.extend(dateTo, {}));
    },
    onDropDownChanged:function(ev)
    {
        this.trigger('FilterView.Changed ',{param:this.getSearchParam()});
        return false;
    },
    onTextKeypress:function(ev)
    {
        if(ev.keyCode === 13)
        {
            this.trigger('FilterView.Changed',{param:this.getSearchParam()});
            return false;
        }
    },
    onFilterReset:function(ev)
    {
        C.clearField(this.$el);
		 this.$('select').trigger('liszt:updated');
        this.trigger('FilterView.Changed ',{param:this.getSearchParam()});
        return false;
    },
	onRefresh:function(ev)
	{
        this.trigger('FilterView.Changed ',{param:this.getSearchParam()});
        return false;
	},
	onExport: function(ev)
    {
        this.trigger('FilterView.Export',{param:this.getSearchParam()});
        return false;
    },
   
    /** General Function **/
    getSearchParam:function() {

        var value = this.$('form').formHash();
        for(var key in value)
        {
            value[key] = encodeURIComponent(value[key]);
        }
        return value;
    },

    requestSumVolume: function(arg) //render total volume, actual, average flowrate disini semua
    {
        $this = this;
        Backbone.ajaxMan.add(
            {
                url: arg,
                success: function(arg) {
                    if (arg.success) {
                        $this.renderThruput(arg);
                    }
                    else {
                        C.Util.log(arg);
                    }
                }

            });
    },

    renderThruput:function(arg)
    {
        $this=this;
        if(arg.records.records[0] !=null)
         {
            $('#volume-total').html('Total Preset : ' + $this.formatDollar(arg.records.records[0]) + ' Kg');
            $('#actual-total').html('Total Actual : ' + $this.formatDollar(arg.records1.records[0]) + ' Kg');
            var selisih = $this.formatDollar((arg.records.records[0])-(arg.records1.records[0]));
            selisihABS=Math.abs(selisih);
            $('#selisih-total').html('Mass Deviation : ' + selisih + ' Kg');
            $('#flowrate').html('Average Flowrate : ' + arg.records2.records[0].toFixed(4) + ' Kg/minute');
            
        }
        else{
            $('#volume-total').html('Total Volume : 0 Kg');
            $('#actual-total').html('Total Actual: 0 Kg');
            $('#selisih-total').html('Volume Deviation: 0 Kg');
            $('#flowrate').html('Flowrate: 0 Kg/minute');
        }
    },

    formatDollar:function(num)
    {
        var p = parseFloat(num).toFixed(2).split(".");
        return p[0].split("").reverse().reduce(function(acc, num, i, orig)
        {
            return num + (i && !(i % 3) ? "," : "") + acc;
        }, "");
    },
});