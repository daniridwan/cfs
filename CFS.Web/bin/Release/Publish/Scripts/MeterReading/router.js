﻿FDM.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.meterReadingColl = new FDM.Model.MeterReadingList();

        // create views
        this.tabView = new FDM.View.TabView();
        this.gridView = new FDM.View.GridView();
        this.formView = new FDM.View.FormView({ el: $('#pnl-form') });
        this.filterView = new FDM.View.FilterView({ el: $('#pnl-search') });

        // setup event routes
        this.gridView.bind('GridView.PageRequest', this.onPageRequested, this);
        this.meterReadingColl.bind('MeterReading.PageReady', this.onPageReady, this);
        this.formView.bind('FormView.Created', this.onCreateNew, this);
        this.formView.bind('FormView.Updated', this.onUpdated, this);
        this.filterView.bind('FilterView.Changed', this.onFilterChanged, this);
        this.filterView.bind('FilterView.Export', this.onExportClick, this);
        
    },
    routes: {
        "": "grid",
        "pnl-grid": "grid",
        "view/:id": "view",
        "new": "openForm",
    },
    start: function () {
        this.tabView.render();
        this.filterView.render();
        this.gridView.render();
        this.formView.render();
        Backbone.history.start();
    },
    view: function (id) {
        var user = this.meterReadingColl.get(id);
        if (user) {
            this.formView.prepareForUpdate(user);
            this.formView.makeReadOnly();
            this.tabView.tabObj.click(1);
            $(window).scrollTop($('#pnl-form').offset().top);
        }
        else
            this.navigate('');
    },
    onCreateNew: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    grid: function () {
        this.tabView.tabObj.click(0);
    },
    openForm: function () {
        this.formView.prepareForInsert();
        this.formView.clearAuditTrail();
        this.formView.validator.resetForm();
        this.tabView.tabObj.click(1);
    },
    onUpdated: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    onFilterChanged: function (filter) {
        this.gridView.setFilter(filter.param);
        this.gridView.refresh();
    },
    onPageRequested: function (arg) {
        Msg.show('Loading...');
        this.meterReadingColl.fetchPage(arg);
        var url = this.meterReadingColl.url();
        url = url.replace('MeterReading/Page','MeterReading/GetTotalThruput');
        this.filterView.requestSumVolume(url);
    },
    onPageReady: function (arg) {
        Msg.hide();
        this.gridView.dataBind(arg);
    },
    onExportClick: function (arg) {
        var url = this.meterReadingColl.url();
        url = url.replace('MeterReading/Page','HttpHandlers/MeterReadingExporter.ashx');
        window.open(url);
    },
});

$(document).ready(function()
{
    FDM.AppRouter = new FDM.Router.AppRouter();
    FDM.AppRouter.start();
});
