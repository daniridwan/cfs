﻿FDM.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-tank-historical').jqGrid(
        {
            pager: 'tbl-tank-historical-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', 'Tank Name', 'Timestamp', 'Liquid Level (mm)', 'Liquid Water (mm)', 'Liquid Temperature (°C)', 'Liquid Density (Kg/cm3)', 'Liquid Density 15 (Kg/cm3)', 'Liquid VolObs (L)', 'Liquid Vol15 (L)', 'Liquid Mass (Kg)',
            'Vapor Temperature (°C)', 'Vapor Density (Kg/cm3)', 'Vapor VolObs (L)', 'Vapor Vol 15 (L)', 'Vapor Mass (Kg)', 'Vapor Pressure (Kg/cm3)', 'Tank Height (mm)', 'Tank Volume (L)'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 18, sortable: false, hidden: false },
                { name: 'TankName', index: 'TankId', width: 90, sortable: false },
                { name: 'TimestampText', index: 'Timestamp', width: 110, sortable: false },
                { name: 'LiquidLevelText', index: 'LiquidLevel', width: 140, sortable: true },
                { name: 'LiquidWaterText', index: 'LiquidWater', width: 140, sortable: true },
                { name: 'LiquidTemperatureText', index: 'LiquidTemperature', width: 175, sortable: true },
                { name: 'LiquidDensityText', index: 'LiquidDensity', width: 140, sortable: true },
                { name: 'LiquidDensity15Text', index: 'LiquidDensity15', width: 140, sortable: true },
                { name: 'LiquidVolObsText', index: 'LiquidVolObs', width: 140, sortable: true },
                { name: 'LiquidVol15Text', index: 'LiquidVol15', width: 135, sortable: true },
                { name: 'LiquidMassText', index: 'LiquidMass', width: 125, sortable: true },
                { name: 'VaporTemperatureText', index: 'VaporTemperature', width: 165, sortable: true },
                { name: 'VaporDensityText', index: 'VaporDensity', width: 130, sortable: true },
                { name: 'VaporVolObsText', index: 'VaporVolObs', width: 115, sortable: true },
                { name: 'VaporVol15Text', index: 'VaporVol15', width: 115, sortable: true },
                { name: 'VaporMassText', index: 'VaporMass', width: 125, sortable: true },
                { name: 'VaporPressureText', index: 'VaporPressure', width: 125, sortable: true },
                { name: 'TankHeightText', index: 'TankHeight', width: 125, sortable: true },
                { name: 'TankVolumeText', index: 'TankVolume', width: 125, sortable: true },
		],
            viewrecords: true,
            sortorder: "asc",
            sortname: "Timestamp",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-tank-historical-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {    
            this.renderDailyHistoricalChart(toInject);
            $('#tbl-tank-historical')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-tank-historical').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    },
    renderDailyHistoricalChart: function (arg) {
        var max = arg.rows.length;
        var dateLabel = [];
        var historicalT1 = [];
        var historicalT2 = [];
        var historicalT4 = [];
        var historicalT5 = [];
        var historicalT6 = [];
        var historicalT7 = [];
        var historicalT9 = [];
        var historicalT10 = [];
        var i = 0;
        for (i = 0; i < max; i++) {
            if (arg.rows[i] != null) {
                dateLabel.push(this.date2text(arg.rows[i].Timestamp, 'dd-mmm-yyyy HH:MM:ss'));
                if(arg.rows[i].TankName == 'T-1')
                {
                    historicalT1.push(arg.rows[i].LiquidLevel);
                }
                if(arg.rows[i].TankName == 'T-2')
                {
                    historicalT2.push(arg.rows[i].LiquidLevel);
                }
                if(arg.rows[i].TankName == 'T-4')
                {
                    historicalT4.push(arg.rows[i].LiquidLevel);
                }
                if(arg.rows[i].TankName == 'T-5')
                {
                    historicalT5.push(arg.rows[i].LiquidLevel);
                }
                if(arg.rows[i].TankName == 'T-6')
                {
                    historicalT6.push(arg.rows[i].LiquidLevel);
                }
                if(arg.rows[i].TankName == 'T-7')
                {
                    historicalT7.push(arg.rows[i].LiquidLevel);
                }
                if(arg.rows[i].TankName == 'T-9')
                {
                    historicalT9.push(arg.rows[i].LiquidLevel);
                }
                if(arg.rows[i].TankName == 'T-10')
                {
                    historicalT10.push(arg.rows[i].LiquidLevel);
                }
            }
        }
        //remove duplicate time
        dateLabel = dateLabel
        .map(e => e)
        .filter((e, i, a) => a.indexOf(e) === i);
        
        var config = {
            type: 'line',
            data: {
                labels: dateLabel,
                datasets: [{
                    label: "T-1",
                    borderColor: window.chartColors.premium,
                    fill: false,
                    backgroundColor: window.chartColors.premium,
                    data: historicalT1
                },
                {
                    label: "T-2",
                    borderColor: window.chartColors.premium,
                    fill: false,
                    backgroundColor: window.chartColors.premium,
                    data: historicalT2
                },
                {
                    label: "T-4",
                    borderColor: window.chartColors.pertamax,
                    fill: false,
                    backgroundColor: window.chartColors.pertamax,
                    data: historicalT4
                },
                {
                    label: "T-5",
                    borderColor: window.chartColors.pertamax,
                    fill: false,
                    backgroundColor: window.chartColors.pertamax,
                    data: historicalT5
                },
                {
                    label: "T-6",
                    borderColor: window.chartColors.solar,
                    fill: false,
                    backgroundColor: window.chartColors.solar,
                    data: historicalT6
                },
                {
                    label: "T-7",
                    borderColor: window.chartColors.solar,
                    fill: false,
                    backgroundColor: window.chartColors.solar,
                    data: historicalT7
                },
                {
                    label: "T-9",
                    borderColor: window.chartColors.pertamax,
                    fill: false,
                    backgroundColor: window.chartColors.pertamax,
                    data: historicalT9
                },
                {
                    label: "T-10",
                    borderColor: window.chartColors.fame,
                    fill: false,
                    backgroundColor: window.chartColors.fame,
                    data: historicalT10
                }]
			},
            options: {
				responsive: true,
                bezierCurve: true,
				title: {
					display: true,
					text: ''
				},
                legend: {
                    display: true,
                },
				tooltips: {
					mode: 'index',
					intersect: false,
                    callbacks: {
                        label: function(tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label;
                        var val = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        return label + ' : ' + val + ' Kg';
                        }
                    }
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Timestamp'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Liquid Level (mm)'
						},
                        ticks:{
                            beginAtZero: true
                        }
					}]
				}
			}
        }
        //call chart
        $("canvas#daily-historical-chart").remove();
        $("div.chart-historical-div").append('<canvas id="daily-historical-chart" class="animated fadeIn"></canvas>');
        var ctx = document.getElementById('daily-historical-chart').getContext('2d');
        ctx.canvas.height = 80;
        var chart = new Chart(ctx, config);
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'dd-mm-yyyy';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    }
});        //close app gridview

