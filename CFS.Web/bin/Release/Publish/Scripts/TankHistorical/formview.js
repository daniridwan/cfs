﻿FDM.View.FormView = C.FormView.extend(
{
initialize: function() {

    },
    events: function() {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
        /* put additional event here */
        });
    },
render: function() {

    // don't forget to call ancestor's render()
    C.FormView.prototype.render.call(this);

    var dateopt = { altFormat: "dd-mm-yy",
            dateFormat: 'd-M-yy', showOn: 'button', duration: 'fast', buttonImage: g_baseUrl + '/Images/calendar.png'
        };
    this.validator = this.$("form").validate({ ignore: null });
    //ini mustinya tergantung permission
    this.enableEdit(false);
},
onEditClick: function(ev){
        C.FormView.prototype.onEditClick.call(this, ev);  
    },
onSaveClick: function(ev) {
    // ini override onSaveClick di ancestor
    var $this = this;
        if(this.validator.form()==false)
        {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-form').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else
        {
            this.$('.client-msg').hide();
        }


    // di sini mustinya ada validate
    // ...
    var toSave = this.$('.the-form').formHash();
    if (this.model) //update
    {
        this.showThrobber(true);
        this.model.save(toSave,
            {
                success: function(model, response) {
                    if (response.success) {
                        $this.showThrobber(false);
                        $this.model.set(response.record);
                        $this.dataBind($this.model);
                        $this.makeReadOnly();
                        $this.renderServerMessage(response);
                        $this.trigger('FormView.Updated', $this.model);
                    }
                    else {
                        $this.renderServerMessage(response);
                    }
                },
                error: function(model, response) {
                    $this.showThrobber(false);
                    C.Util.log(response);
                    if (window.console) console.log(response);
                }
            });
    }
    else //insert
    {
        var objSave = new FDM.Model.TankHistoricalData();
        delete (toSave.TankId);
        Msg.show('saving..');
        objSave.save(toSave,
    	    {
    	        success: function(model, response) {
    	            if (response.success) {
    	                $this.showThrobber(false);
    	                $this.model = objSave;
    	                $this.model.set(response.record);
    	                $this.dataBind($this.model);
    	                $this.makeReadOnly();
    	                $this.renderServerMessage(response);
    	                $this.trigger('FormView.Updated', $this.model);
    	            }
    	            else {
    	                $this.renderServerMessage(response);
    	            }
    	        },
    	        error: function(model, response) {
    	            $this.showThrobber(false);
    	            if (window.console) console.log(response);
    	        }
    	    });
    }
},
prepareForInsert: function() {
    C.FormView.prototype.prepareForInsert.apply(this);
    this.validator.resetForm();
    this.clearAuditTrail();
    this.enableEditControl(true);
},
dataBind: function(arg) {
    C.FormView.prototype.dataBind.apply(this, arguments);
    $('#txt-timestamp').val(arg.get('TimestampText'));
    $('#txt-liquid-level').val(arg.get('LiquidLevelText'));
    $('#txt-liquid-temperature').val(arg.get('LiquidTemperatureText'));
    $('#txt-liquid-density').val(arg.get('LiquidDensityText'));
    $('#txt-liquid-density15').val(arg.get('LiquidDensity15Text'));
    $('#txt-liquid-vol-obs').val(arg.get('LiquidVolObsText'));
    $('#txt-liquid-vol-15').val(arg.get('LiquidVol15Text'));
    $('#txt-liquid-mass').val(arg.get('LiquidMassText'));
    $('#txt-vapor-temperature').val(arg.get('VaporTemperatureText'));
    $('#txt-vapor-density').val(arg.get('VaporDensityText'));
    $('#txt-vapor-vol-obs').val(arg.get('VaporVolObsText'));
    $('#txt-vapor-vol-15').val(arg.get('VaporVol15Text'));
    $('#txt-vapor-mass').val(arg.get('VaporMassText'));
    $('#txt-vapor-pressure').val(arg.get('VaporPressureText'));
    $('#txt-tank-height').val(arg.get('TankHeightText'));
    $('#txt-tank-volume').val(arg.get('TankVolumeText'));
    if(arg.get('CreatedTimestamp') != null)
        {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else
        {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
    if(arg.get('UpdatedTimestamp') != null)
        {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else
        {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
},
prepareForUpdate: function(arg) {
        this.dataBind(arg);
        this.validator.resetForm();
        this.makeReadOnly();
        this.enableEditControl(false);
},
clearAuditTrail: function() {
    $('#lbl-insert-date').text('-');
    $('#lbl-insert-by').text('-');
    $('#lbl-last-updated-date').text('-');
    $('#lbl-last-updated-by').text('-');
},
enableEditControl: function(en) {
        if (en) {
            this.$('button.ui-datepicker-trigger').show();
        }
        else {
            this.$('button.ui-datepicker-trigger').hide();
        }
    },
});