﻿CFS = { Model: {}, View: {}, Router: {} };
$.metadata.setType("attr", "validate");
//Delivery Order Realization
CFS.Model.StockMonitoring = Backbone.Model.extend(
{
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    defaults:
    {
        InsertedDate: null,
        LastUpdatedDate: null,
        Visible: 1
    },
    url: function () {
        return g_baseUrl + 'StockMonitoring/Save?id=' + (this.id ? this.id : '0');
    },
    idAttribute: 'Row',
    onChange: function () {
        var val = this.attributes;
        try {
            val.view = '<a href="#view/' + val.PlanningId + '"><div class="ui-silk ui-silk-zoom" style="height:22px"></div></a>';
            val.SoundingDateText = this.date2text(val.SoundingDate, 'd-mmm-yyyy');//this.date2text(val.SoundingDate, 'd-mmm-yyyy HH:MM:ss');
            val.ArrivalDateText = this.date2text(val.ArrivalDate, 'd-mmm-yyyy');//this.date2text(val.SoundingDate, 'd-mmm-yyyy HH:MM:ss');
            val.DeliveryDateText = this.date2text(val.DeliveryDate, 'd-mmm-yyyy');//this.date2text(val.SoundingDate, 'd-mmm-yyyy HH:MM:ss');
        }
        catch (er) {
            C.Util.log(er);
        }
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'd-mmm-yyyy';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
    formatDollar: function (num) {
        return (
                parseFloat(num)
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            )
    },
});

CFS.Model.StockMonitoringList = Backbone.Collection.extend(
{
    initialize: function () {
        this.on('reset', this.onResetOrChange, this);
    },
    postData: {}, //from jqgrid
    totalRecords: 0,
    pageIndex: 0,
    pageSize: 15,
    model: CFS.Model.StockMonitoring,
    url: function () {
        var xurl = g_baseUrl + 'StockMonitoring/Page?pageIndex=' + this.pageIndex + '&pageSize=' + this.pageSize;
        var query = [];
        for (var key in this.postData) {
            if (this.postData.hasOwnProperty(key) && key.indexOf('flt') != -1) {
                query.push("&" + key + "=" + this.postData[key]);
            }
        }
        if (this.postData.sidx && this.postData.sidx.length) { query.push("&sidx=" + this.postData.sidx); }
        if (this.postData.sord && this.postData.sord.length) { query.push("&sord=" + this.postData.sord); }
        query.push('&t=' + (new Date()).valueOf());
        return xurl + query.join('');
    },
    parse: function (arg) {
        if (!arg.success) {
            alert(arg.message);
            return;
        }

        var pageSize = parseInt(this.pageSize, 10);
        var pageIndex = parseInt(this.pageIndex, 10);
        var id = (pageSize * pageIndex) + 1;
        for (var i = 0, ii = arg.records.length; i < ii; i++) {
            arg.records[i].No = id + i;
        }

        this.totalRecords = arg.totalRecords;
        return arg.records;
    },
    date2text: function (dt) {
        if (dt) { return C.parseIsoDateTime(dt).format('d-mmm-yyyy'); }
        else { return ''; }
    },
    fetchPage: function (postData) {
        this.pageIndex = postData.pageIndex;
        this.pageSize = postData.pageSize;
        this.postData = postData;
        this.fetch({ reset: true });
    },
    onResetOrChange: function () {
        var idx = this.pageIndex * this.pageSize + 1;
        for (var i = 0, ii = this.length; i < ii; i++) {
            this.at(i).onChange();
        }
        var toInject = {
            total: Math.ceil(this.totalRecords / this.pageSize),
            page: this.pageIndex + 1,
            records: this.totalRecords,
            rows: this.toJSON()
        };
        try {
            this.trigger('StockMonitoring.PageReady', toInject);
        } catch (ex) { }
    }
});