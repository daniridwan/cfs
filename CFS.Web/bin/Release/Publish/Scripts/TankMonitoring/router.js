﻿CFS.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.stockMonitoringColl = new CFS.Model.StockMonitoringList();

        // create views
        this.tabView = new CFS.View.TabView();
        this.gridView = new CFS.View.GridView();
        this.formView = new CFS.View.FormView({ el: $('#pnl-form') });
        this.filterView = new CFS.View.FilterView({ el: $('#pnl-search') });
        this.dlgStockReport = new CFS.View.DlgStockReport({ el: $('#dlg-lo-report') });

        // setup event routes
        this.gridView.bind('GridView.PageRequest', this.onPageRequested, this);
        this.stockMonitoringColl.bind('StockMonitoring.PageReady', this.onPageReady, this);
        this.formView.bind('FormView.Created', this.onCreateNew, this);
        this.formView.bind('FormView.Updated', this.onUpdated, this);
        this.filterView.bind('FilterView.Changed', this.onFilterChanged, this);
        this.filterView.bind('FilterView.Export', this.onExportClick, this);
        this.filterView.bind('FilterView.Report', this.onReportClick, this);
        
    },
    routes: {
        "": "grid",
        "pnl-grid": "grid",
        "view/:id": "view",
        "new": "openForm",
    },
    start: function () {
        this.tabView.render();
        this.filterView.render();
        this.gridView.render();
        this.formView.render();
        this.dlgStockReport.render();
        Backbone.history.start();
    },
    view: function (id) {
        var user = this.stockMonitoringColl.get(id);
        if (user) {
            this.formView.prepareForUpdate(user);
            this.formView.makeReadOnly();
            this.tabView.tabObj.click(1);
            $(window).scrollTop($('#pnl-form').offset().top);
        }
        else
            this.navigate('');
    },
    onCreateNew: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    grid: function () {
        this.tabView.tabObj.click(0);
    },
    openForm: function () {
        this.formView.prepareForInsert();
        this.formView.clearAuditTrail();
        this.formView.validator.resetForm();
        this.tabView.tabObj.click(1);
    },
    onUpdated: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    onFilterChanged: function (filter) {
        this.gridView.setFilter(filter.param);
        this.gridView.refresh();
    },
    onPageRequested: function (arg) {
        Msg.show('Loading...');
        this.stockMonitoringColl.fetchPage(arg);
    },
    onPageReady: function (arg) {
        Msg.hide();
        this.gridView.dataBind(arg);
    },
    onExportClick: function (arg) {
        var url = this.deliveryOrderRealizationColl.url();
        url = url.replace('StockMonitoring/Page','HttpHandlers/DeliveryOrderRealizationExporter.ashx');
        window.open(url);
    },
    onReportClick: function (arg) {
        this.dlgStockReport.clear();
        this.dlgStockReport.show(true);
    }
});

$(document).ready(function()
{
    CFS.AppRouter = new CFS.Router.AppRouter();
    CFS.AppRouter.start();
});
