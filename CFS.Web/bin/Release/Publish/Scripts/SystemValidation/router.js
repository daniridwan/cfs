﻿CFS.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.systemValidationColl = new CFS.Model.SystemValidationList();
        // create views
        this.tabView = new CFS.View.TabView();
        this.gridView = new CFS.View.GridView();
        this.formView = new CFS.View.FormView({ el: $('#pnl-form') });
        this.filterView = new CFS.View.FilterView({ el: $('#pnl-search') });
        this.systemValidationColl.bind('SystemValidation.PageReady', this.onPageReady, this);
        //setup event routes
        this.gridView.bind('GridView.PageRequest', this.onPageRequested, this);
        this.systemValidationColl.bind('SystemValidation.PageReady', this.onPageReady, this);
        this.formView.bind('FormView.Created', this.onCreateNew, this);
        this.formView.bind('FormView.Updated', this.onUpdated, this);
        this.filterView.bind('FilterView.Changed', this.onFilterChanged, this);
    },
    routes: {
        "": "grid",
        "pnl-grid": "grid",
        "view/:id": "view",
        "upload/:id": "upload",
        "new": "openForm"
    },
    start: function () {
        this.tabView.render();
        this.gridView.render();
        this.formView.render();
        Backbone.history.start();
    },
    view: function (id) {
        var user = this.systemValidationColl.get(id);
        console.log(this.systemValidationColl);
        if (user) {
            C.Util.log(user.attributes);
            this.formView.validator.resetForm();
            this.formView.dataBind(user);
            this.formView.makeReadOnly();
            this.tabView.tabObj.click(1);
            $(window).scrollTop($('#pnl-form').offset().top);
        }
        else
            this.navigate('');
    },
    grid: function () {
        this.tabView.tabObj.click(0);
    },
    openForm: function () {
        this.formView.prepareForInsert();
        this.formView.validator.resetForm();
        this.tabView.tabObj.click(1);
    },
    onCreateNew: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    onUpdated: function (arg) {
        this.navigate('view/' + arg.id, { replace: true });
    },
    onFilterChanged: function (filter) {
        this.gridView.setFilter(filter.param, true);
        this.gridView.refresh();
    },
    onPageRequested: function (arg) {
        Msg.show('Loading...');
        this.systemValidationColl.fetchPage(arg);
    },
    onPageReady: function (arg) {
        Msg.hide();
        this.gridView.dataBind(arg);
    }
});

$(document).ready(function () {
    CFS.AppRouter = new CFS.Router.AppRouter();
    CFS.AppRouter.start();
});
