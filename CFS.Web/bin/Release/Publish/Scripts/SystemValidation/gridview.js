﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //console.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-system-validation').jqGrid(
        {
            pager: 'tbl-system-validation-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },
            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', 'Key', 'Value', 'Is Active?', 'Description'],
            colModel: // {name: nama_didatabase}
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 18, sortable: false, hidden: false },
                { name: 'Name', width: 200, sortable: true },
                { name: 'Value', width: 80, sortable: true },
                { name: 'IsActiveText', index: 'IsActive', width: 120, sortable: true },
                { name: 'Description', width: 600, sortable: false },
   		],
            viewrecords: true,
            sortorder: "asc",
            sortname: "Id",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
            altclass: 'even',
            altRows: true
        }).navGrid('#tbl-system-validation-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {
            $('#tbl-system-validation')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    // reset filter js
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    //refresh js
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize: function () {
        $('#tbl-system-validation').setGridWidth($('#pnl-frame').width(), false).triggerHandler('resize');
    }
});
