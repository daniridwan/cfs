﻿FDM.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //console.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-user').jqGrid(
        {
            pager: 'tbl-user-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },
            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', 'Username', 'Role', 'Phone', 'Email', 'Status'],
            colModel: // {name: nama_didatabase}
		[
		        { name: 'No', sortable: false, width: 40, hidden: false },
				{ name: 'view', width: 18, sortable: false, hidden: false },
                //{ name: 'delete', width: 18, sortable: false, hidden: false },
                { name: 'Username', sortable: false, width: 120, hidden: false },
                { name: 'RoleName', sortable: false, width: 100, hidden: false },
                { name: 'PhoneNumber', sortable: false, width: 150, hidden: false },
                { name: 'Email', sortable: false, width: 150, hidden: false },
                { name: 'IsActiveText', index: 'IsActive', width: 100, sortable: true },
   		],
            viewrecords: true,
            sortorder: "asc",
            sortname: "Username",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
            altclass: 'even',
            altRows: true
        }).navGrid('#tbl-user-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {
            $('#tbl-user')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    // reset filter js
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    //refresh js
    refresh: function () {
        try {
            this.gridObj.trigger('reloadGrid');
        } catch (er) {
            var $thisgridObj = $('#tbl-user').jqGrid();
            $thisgridObj.trigger('reloadGrid');
        }
    },
    OnWindowResize: function () {
        $('#tbl-user').setGridWidth($('#pnl-frame').width(), false).triggerHandler('resize');
    }
});
