﻿CFS = { Model: {}, View: {}, Router: {} };

CFS.Model.Users = Backbone.Model.extend(
{
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    defaults:
    {
    },
    url: function () {
        return g_baseUrl + 'User/UpdatePassword?username=' + (g_user ? g_user : '0');
    },
    idAttribute: 'Username',
    onChange: function () {
        var val = this.attributes;
        val.cid = this.cid;

        try {
            val.view = '<a href="#view/' + val.UserId + '"><div class="ui-silk ui-silk-zoom"></div></a>';
        }
        catch (er) {
            C.Util.log(er);
        }
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'yyyy-mmm-d';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
});

CFS.Model.UsersList = Backbone.Collection.extend(
{
    initialize: function () {
        this.on('reset', this.onResetOrChange, this);
        this.on('change', this.onResetOrChange, this);
    },
    postData: {}, //from jqgrid
    totalRecords: 0,
    pageIndex: 0,
    pageSize: 15,
    model: CFS.Model.Users,
    url: function () {
        var xurl = g_baseUrl + 'User/Page?pageIndex=' + this.pageIndex + '&pageSize=' + this.pageSize;
        var query = [];
        for (var key in this.postData) {
            if (this.postData.hasOwnProperty(key) && key.indexOf('flt') != -1) {
                query.push("&" + key + "=" + this.postData[key]);
            }
        }
        if (this.postData.sidx && this.postData.sidx.length) { query.push("&sidx=" + this.postData.sidx); }
        if (this.postData.sord && this.postData.sord.length) { query.push("&sord=" + this.postData.sord); }
        return xurl + query.join('');
    },
    parse: function (arg) {
        if (!arg.success) {
            alert(arg.message);
            return;
        }

        var pageSize = parseInt(this.pageSize, 10);
        var pageIndex = parseInt(this.pageIndex, 10);
        var id = (pageSize * pageIndex) + 1;
        for (var i = 0, ii = arg.records.length; i < ii; i++) {
            arg.records[i].No = id + i;
        }
        this.totalRecords = arg.totalRecords;
        return arg.records;
    },
    date2text: function (dt) {
        if (dt) { return C.parseIsoDateTime(dt).format('d-mmm-yyyy'); }
        else { return ''; }
    },
    fetchPage: function (postData) {
        this.pageIndex = postData.pageIndex;
        this.pageSize = postData.pageSize;
        this.postData = postData;
        this.fetch({ reset: true });
    },
    onResetOrChange: function () {
        var idx = this.pageIndex * this.pageSize + 1;
        for (var i = 0, ii = this.length; i < ii; i++) {
            this.at(i).onChange();
        }
        var toInject = {
            total: Math.ceil(this.totalRecords / this.pageSize),
            page: this.pageIndex + 1,
            records: this.totalRecords,
            rows: this.toJSON()
        };
        try {
            this.trigger('Users.PageReady', toInject);
        } catch (ex) { }
    }
});