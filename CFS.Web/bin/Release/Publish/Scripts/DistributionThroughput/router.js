﻿NetLPG.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models

        // create views
        this.tabView = new NetLPG.View.TabView();
        this.chartView = new NetLPG.View.ChartView();
        this.filterView = new NetLPG.View.FilterView({ el: $('#pnl-search') });

        // setup event routes
        NetLPG.Model.TheDistributionThroughputList.on('DistributionThroughput.PageReady', this.onPageReady, this);
        this.filterView.bind('FilterView.Changed', this.onFilterChanged, this);
        this.filterView.bind('FilterView.Export', this.onExportClick, this);
    },
    routes: {
    },
    start: function () {
        this.tabView.render();
        this.filterView.render();
        this.chartView.render();
        Backbone.history.start();
        this.filterView.onFilterReset();
    },
    onFilterChanged: function (arg) {
        arg.param.pageSize = 0;
		arg.param.pageIndex = 0;
        this.fltTimestampFrom = arg.param.fltTimestampFrom;
		this.fltTimestampTo = arg.param.fltTimestampTo;
        NetLPG.Model.TheDistributionThroughputList.fetchPage(arg.param);
    },
    onPageReady: function (arg) {
        this.chartView.refresh(this.fltTimestampFrom, this.fltTimestampTo);
    },
});

$(document).ready(function()
{
    NetLPG.AppRouter = new NetLPG.Router.AppRouter();
    NetLPG.AppRouter.start();
});
