﻿NetLPG.View.TabView = Backbone.View.extend(
{
    initialize: function (el) {
        this.el = el;
    },
    render: function () {
        this.tabObj = $('#pnl-frame').tabs('.panes', { api: true, tabs: '.tabs a' });
        this.tabObj.click(0);
    }
});