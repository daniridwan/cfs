﻿FDM.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.tankTicketColl = new FDM.Model.TankTicketList();

        // create views
        this.tabView = new FDM.View.TabView();
        this.gridView = new FDM.View.GridView();
        this.formView = new FDM.View.FormView({ el: $('#pnl-form') });
        this.filterView = new FDM.View.FilterView({ el: $('#pnl-search') });
        this.dlgNewForm = new FDM.View.DlgNewForm({ el: $('#dlg-new-form') });

        // setup event routes
        this.gridView.bind('GridView.PageRequest', this.onPageRequested, this);
        this.tankTicketColl.bind('TankTicket.PageReady', this.onPageReady, this);
        this.formView.bind('FormView.Created', this.onCreateNew, this);
        this.formView.bind('FormView.Updated', this.onUpdated, this);
        this.filterView.bind('FilterView.Changed', this.onFilterChanged, this);
        this.filterView.bind('FilterView.Export', this.onExportClick, this);
        this.dlgNewForm.bind('DlgNewForm.Cancel', this.grid, this);
        this.dlgNewForm.bind('DlgNewForm.Selected', this.openForm, this);
        
    },
    routes: {
        "": "grid",
        "pnl-grid": "grid",
        "view/:id": "view",
        "new": "openNewDialog",
        "pdf/:id": "print",
    },
    start: function () {
        this.tabView.render();
        this.filterView.render();
        this.gridView.render();
        this.formView.render();
        this.dlgNewForm.render();
        Backbone.history.start();
    },
    view: function (id) {
        var user = this.tankTicketColl.get(id);
        if (user) {
            this.formView.prepareForUpdate(user);
            this.formView.makeReadOnly();
            this.tabView.tabObj.click(1);
            $(window).scrollTop($('#pnl-form').offset().top);
        }
        else
            this.navigate('');
    },
    print: function(id) {
        // redirect to another page
        var url = this.tankTicketColl.url();
        url = url.replace(url, g_baseUrl + 'HttpHandlers/TankTicketPdfExporter.ashx?id=' + id);
        window.open(url);
        this.navigate('');
    },
    onCreateNew: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    grid: function () {
        this.navigate('', { replace: true });
        this.tabView.tabObj.click(0);
    },
    openNewDialog: function () {
        this.dlgNewForm.clear();
        this.dlgNewForm.show(true);
    },
    openForm: function (arg){
        this.formView.prepareForInsert(arg);
        //this.formView.validator.resetForm();
        this.tabView.tabObj.click(1);
    },
    onUpdated: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    onFilterChanged: function (filter) {
        this.gridView.setFilter(filter.param);
        this.gridView.refresh();
    },
    onPageRequested: function (arg) {
        Msg.show('Loading...');
        this.tankTicketColl.fetchPage(arg);
    },
    onPageReady: function (arg) {
        Msg.hide();
        this.gridView.dataBind(arg);
    },
    onExportClick: function (arg) {
        var url = this.tankTicketColl.url();
        url = url.replace('TankTicket/Page','HttpHandlers/TankTicketExporter.ashx');
        window.open(url);
    },
});

$(document).ready(function()
{
    FDM.AppRouter = new FDM.Router.AppRouter();
    FDM.AppRouter.start();
});
