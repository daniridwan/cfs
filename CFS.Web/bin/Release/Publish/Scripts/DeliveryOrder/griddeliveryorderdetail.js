﻿CFS.View.pGridDeliveryOrderDetail =
    {
        initialize: function (el) {
        },
        events: {
            "click .cmd-view": "onViewClick",
            "click .cmd-del": "onDeleteClick",
        },
        showControl: function (en) {
            if (en) {
                this.gridObj.showCol('view');
                this.gridObj.showCol('del');
                this.gridObj.showCol('cb');
            }
            else {
                this.gridObj.hideCol('view');
                this.gridObj.hideCol('del');
                this.gridObj.hideCol('cb');
            }
        },
        render: function () {
            var $this = this;
            this.gridObj = this.$el.jqGrid({
                datatype: function (postData) {
                    if ($this.newFilter) {
                        $this.newFilter = false;
                        postData.pageIndex = 1;
                    }
                    postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                    var param = $.extend({}, postData);
                    if ($this.filter) {
                        $.extend(param, $this.filter);
                    }
                    $this.trigger('GridView.PageRequest', param);
                },
                jsonReader: {
                    id: 0,
                    repeatitems: false
                },
                colNames: ['Id','', 'No', 'DO Detail No.', 'Customer Ref. No.', 'Product', 'Tank', 'Quantity', 'Actual Quantity', 'UOM', 'Ship to Address'],
                colModel: [
                    { name: 'cid', width: 60, hidden: true },
                    { name: 'idx', width: 30, hidden: false },
                    { name: 'view', width: 24, sortable: false, hidden: false },
                    { name: 'DeliveryOrderDetailNumber', width: 150, sortable: true, hidden: true },
                    { name: 'CustomerReferenceNumber', width: 150, sortable: true },
                    { name: 'ProductName', index: 'ProductId', width: 110, sortable: true },
                    { name: 'TankName', width: 70, sortable: true },
                    { name: 'Quantity', width: 80, sortable: true },
                    { name: 'ActualQuantity', width: 120, sortable: true },
                    { name: 'UOM', width: 50, sortable: true },
                    { name: 'ShipToAddress', width: 300, sortable: true },
                ],
                viewrecords: true,
                rowNum: 300,
                sortorder: "asc",
                height: 'auto',
                multiselect: false,
                shrinkToFit: false,
                width: $('#pnl-frame').width() * 0.95,
                pginput: true,
                altRows: true,
                prmNames: {
                    rows: 'pageSize',
                    page: 'pageIndex'
                }

            });
        },
        dataBind: function (toInject) {
            try {
                this.$el[0].addJSONData(toInject);
                $('.soft-delete', this.$el).each(function (key, val) {
                    $(val).parent().parent().addClass('cancel');
                });
                this.$('.warnrest').each(function (key, val) {
                    $(val).parent().parent().addClass('warnrest');
                });
            }
            catch (err) {
                C.Util.log(err);
            }
        },
        setFilter: function (filter) {
            this.filter = filter;
            this.newFilter = true;
        },
        refresh: function () {
            this.gridObj.trigger('reloadGrid');
        },
        onViewClick: function (ev) {
            var id = $(ev.target).parent().parent().attr('id');
            this.trigger('GridView.RequestView', id);
        },
        onActualClick: function (ev) {
            var id = $(ev.target).parent().parent().attr('id');
            this.trigger('GridView.RequestActual', id);
        },
    };
CFS.View.GridDeliveryOrderDetail = Backbone.View.extend(CFS.View.pGridDeliveryOrderDetail);