﻿CFS.View.FormView = C.FormView.extend(
{
initialize: function() {
        this.gridDeliveryOrderDetail = new CFS.View.GridDeliveryOrderDetail({ el: $('#tbl-delivery-order-detail') });
        this.dlgDeliveryOrderDetail = new CFS.View.DlgDeliveryOrderDetail({ el: $('#dlg-delivery-order-detail') });
    },
    events: function() {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
        /* put additional event here */
            "click #btn-add-delivery-order": "onAddDeliveryOrder",
        });
    },
render: function() {

    // don't forget to call ancestor's render()
    C.FormView.prototype.render.call(this);

    var dateopt = { altFormat: "dd-mm-yy",
            dateFormat: 'd-M-yy', showOn: 'button', duration: 'fast', buttonImage: g_baseUrl + '/Images/calendar.png'
        };
    $(".datepicker").datepicker({
    beforeShow: function (input, inst) {
        setTimeout(function () {
            inst.dpDiv.css({
                top: $(".datepicker").offset().top + 35,
                left: $(".datepicker").offset().left
            });
        }, 0);
        }
    }); 

    this.validator = this.$("form").validate({ ignore: null });
    //ini mustinya tergantung permission
    this.gridDeliveryOrderDetail.render();
    this.dlgDeliveryOrderDetail.render();

    this.gridDeliveryOrderDetail.on('GridView.RequestDelete', this.onDeleteDeliveryOrderDetailRequested, this);
    this.gridDeliveryOrderDetail.on('GridView.RequestView', this.onViewDeliveryOrderDetailRequested, this);
    this.gridDeliveryOrderDetail.on('GridView.PageRequest', this.onPageDeliveryOrderDetailRequested, this);
    this.dlgDeliveryOrderDetail.on('updated', this.onDeliveryOrderDetailUpdated, this);

    this.enableEdit(true);
},
onEditClick: function(ev){
    C.FormView.prototype.onEditClick.call(this, ev);
    this.enableEditControl(true);
    },
onSaveClick: function(ev) {
    // ini override onSaveClick di ancestor
    var $this = this;
        if(this.validator.form()==false)
        {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-form').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else
        {
            this.$('.client-msg').hide();
        }


    // di sini mustinya ada validate
    // ...
    var toSave = this.$('.the-form').formHash();
    toSave.DeliveryOrderDetail = CFS.Model.DeliveryOrderDetailList.toJSON();
    if (this.model) //update
    {
        this.showThrobber(true);
        this.model.save(toSave,
            {
                success: function(model, response) {
                    if (response.success) {
                        $this.showThrobber(false);
                        $this.model.set(response.record);
                        $this.dataBind($this.model);
                        $this.makeReadOnly();
                        $this.renderServerMessage(response);
                        $this.enableEditControl(false);
                        $this.trigger('FormView.Updated', $this.model);
                        $('.cmd-edit').hide();
                    }
                    else {
                        $this.renderServerMessage(response);
                    }
                },
                error: function(model, response) {
                    $this.showThrobber(false);
                    C.Util.log(response);
                    if (window.console) console.log(response);
                }
            });
    }
    //else //insert
    //{
    //    var objSave = new CFS.Model.DeliveryOrder();
    //    delete (toSave.Id);
    //    Msg.show('Saving..');
    //    objSave.save(toSave,
    //	    {
    //	        success: function(model, response) {
    //	            if (response.success) {
    //	                $this.showThrobber(false);
    //	                $this.model = objSave;
    //	                $this.model.set(response.record);
    //	                $this.dataBind($this.model);
    //	                $this.makeReadOnly();
    //	                $this.renderServerMessage(response);
    //	                $this.trigger('FormView.Updated', $this.model);
    //	            }
    //	            else {
    //	                $this.renderServerMessage(response);
    //	            }
    //	        },
    //	        error: function(model, response) {
    //	            $this.showThrobber(false);
    //	            if (window.console) console.log(response);
    //	        }
    //	    });
    //}
},
prepareForInsert: function() {
    C.FormView.prototype.prepareForInsert.apply(this);
    this.validator.resetForm();
    this.clearAuditTrail();
    this.enableEditControl(true);
    //reset child
    CFS.Model.DeliveryOrderDetailList.reset();
    this.gridDeliveryOrderDetail.dataBind(CFS.Model.DeliveryOrderDetailList.getFormattedArray());
},
dataBind: function(arg) {
    C.FormView.prototype.dataBind.apply(this, arguments);
    if (arg.get('DeliveryDate') != null) {
        var DeliveryDate = C.parseIsoDateTime(arg.get('DeliveryDate'));
        $('#txt-delivery-date').val(DeliveryDate.format('d-mmm-yyyy'));
    }
},
prepareForUpdate: function(arg) {
    this.dataBind(arg);
    this.validator.resetForm();
    this.makeReadOnly();
    this.enableEditControl(false);

    CFS.Model.DeliveryOrderDetailList.reset(arg.attributes.DeliveryOrderDetail);
    this.gridDeliveryOrderDetail.dataBind(CFS.Model.DeliveryOrderDetailList.getFormattedArray());
},
clearAuditTrail: function() {
    $('#lbl-insert-date').text('-');
    $('#lbl-insert-by').text('-');
    $('#lbl-last-updated-date').text('-');
    $('#lbl-last-updated-by').text('-');
},
enableEditControl: function(en) {
        if (en) {
            this.$('button.ui-datepicker-trigger').show();
            this.gridDeliveryOrderDetail.showControl(true);
        }
        else {
            this.$('button.ui-datepicker-trigger').hide();
            this.gridDeliveryOrderDetail.showControl(false);
        }
},
onViewDeliveryOrderDetailRequested: function (id) {
    var record = CFS.Model.DeliveryOrderDetailList.get(id);
    // deprecated in current backbone version
    //var record = FDM.Model.TruckCompartmentList.getByCid(id);
    this.dlgDeliveryOrderDetail.prepareForUpdate(record);
    this.dlgDeliveryOrderDetail.show(true);
},
onDeliveryOrderDetailUpdated: function (arg) {
    var toInject = CFS.Model.DeliveryOrderDetailList.getFormattedArray();
    this.gridDeliveryOrderDetail.dataBind(toInject);
    this.dlgDeliveryOrderDetail.show(false);
    //this.dlgRejectReason.show(false);
},
});