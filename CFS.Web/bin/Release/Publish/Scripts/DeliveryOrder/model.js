﻿CFS = { Model: {}, View: {}, Router: {} };

CFS.Model.DeliveryOrderDetail = Backbone.Model.extend({
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    idAttribute: 'DeliveryOrderDetailId',
    defaults:
    {
    },
    parse: function (arg) {
        return arg;
    },
    onChange: function () {
        var m = this.attributes;
        m.cid = this.cid;
        m.view = '<div class="ui-silk ui-silk-page-white-edit cmd-view" style="cursor:pointer;" title="Edit"></div>';
        m.del = '<div class="ui-silk ui-silk-cancel cmd-del" style="cursor:pointer;" title="Remove"></div>';
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'yyyy-d-mmm';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
});

CFS.Model.DeliveryOrderDetailList = Backbone.Collection.extend({
    initialize: function () {
        this.on('reset', this.onReset, this);
    },
    url: 'dummy.aspx',
    model: CFS.Model.DeliveryOrderDetail,
    getFormattedArray: function () {
        for (var i = 0, ii = this.length; i < ii; i++) {
            var m = this.at(i);
            m.set('idx', i + 1);
        }
        var toInject = {
            total: 1,
            page: 1,
            records: this.models.length,
            rows: this.toJSON()
        };
        return toInject;
    },
    onReset: function (arg) {
        for (var i = 0, ii = this.length; i < ii; i++) {
            var m = this.at(i);
            m.set('idx', i + 1);
            m.onChange();
        }
    }
});
CFS.Model.DeliveryOrderDetailList = new CFS.Model.DeliveryOrderDetailList();

CFS.Model.DeliveryOrder = Backbone.Model.extend(
    {
        initialize: function () {
            this.on('change', this.onChange, this);
        },
        defaults:
        {
        },
        url: function () {
            return g_baseUrl + 'DeliveryOrder/Save?id=' + (this.id ? this.id : '0');
        },
        idAttribute: 'DeliveryOrderId',
        onChange: function () {
            var val = this.attributes;
            val.cid = this.cid;

            try {
                val.view = '<a href="#view/' + val.DeliveryOrderId + '"><div class="ui-silk ui-silk-zoom"></div></a>';
                val.DeliveryDateText = this.date2text(val.DeliveryDate, 'd-mmm-yyyy');
            }
            catch (er) {
                C.Util.log(er);
            }
        },
        date2text: function (dt, fmt) {
            if (!fmt) fmt = 'yyyy-mmm-d';
            if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
            else { return ''; }
        },
    });

CFS.Model.DeliveryOrderList = Backbone.Collection.extend(
    {
        initialize: function () {
            this.on('reset', this.onResetOrChange, this);
            this.on('change', this.onResetOrChange, this);
        },
        postData: {}, //from jqgrid
        totalRecords: 0,
        pageIndex: 0,
        pageSize: 15,
        model: CFS.Model.DeliveryOrder,
        url: function () {
            var xurl = g_baseUrl + 'DeliveryOrder/Page?pageIndex=' + this.pageIndex + '&pageSize=' + this.pageSize;
            var query = [];
            for (var key in this.postData) {
                if (this.postData.hasOwnProperty(key) && key.indexOf('flt') != -1) {
                    query.push("&" + key + "=" + this.postData[key]);
                }
            }
            if (this.postData.sidx && this.postData.sidx.length) { query.push("&sidx=" + this.postData.sidx); }
            if (this.postData.sord && this.postData.sord.length) { query.push("&sord=" + this.postData.sord); }
            return xurl + query.join('');
        },
        parse: function (arg) {
            if (!arg.success) {
                alert(arg.message);
                return;
            }

            var pageSize = parseInt(this.pageSize, 10);
            var pageIndex = parseInt(this.pageIndex, 10);
            var id = (pageSize * pageIndex) + 1;
            for (var i = 0, ii = arg.records.length; i < ii; i++) {
                arg.records[i].No = id + i;
            }
            this.totalRecords = arg.totalRecords;
            return arg.records;
        },
        date2text: function (dt) {
            if (dt) { return C.parseIsoDateTime(dt).format('d-mmm-yyyy'); }
            else { return ''; }
        },
        fetchPage: function (postData) {
            this.pageIndex = postData.pageIndex;
            this.pageSize = postData.pageSize;
            this.postData = postData;
            this.fetch({ reset: true });
        },
        onResetOrChange: function () {
            var idx = this.pageIndex * this.pageSize + 1;
            for (var i = 0, ii = this.length; i < ii; i++) {
                this.at(i).onChange();
            }
            var toInject = {
                total: Math.ceil(this.totalRecords / this.pageSize),
                page: this.pageIndex + 1,
                records: this.totalRecords,
                rows: this.toJSON()
            };
            try {
                this.trigger('DeliveryOrder.PageReady', toInject);
            } catch (ex) { }
        }
    });