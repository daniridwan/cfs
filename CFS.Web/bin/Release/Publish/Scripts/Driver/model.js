﻿CFS = { Model: {}, View: {}, Router: {} };
$.metadata.setType("attr", "validate");
//driver notes
CFS.Model.DriverNotes = Backbone.Model.extend({
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    idAttribute: 'DriverNotesId',
    defaults:
    {
    },
    parse: function (arg) {
        return arg;
    },
    onChange: function () {
        var m = this.attributes;

        m.cid = this.cid;
        m.view = '<div class="ui-silk ui-silk-page-white-edit cmd-view" style="cursor:pointer;" title="Edit"></div>';
        m.del = '<div class="ui-silk ui-silk-cancel cmd-del" style="cursor:pointer;" title="Remove"></div>';
        m.DateText = this.date2text(m.Date, 'd-mmm-yyyy');
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'yyyy-d-mmm';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
    formatDollar: function (num) {
        return (
            parseFloat(num)
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        )
    },
});

CFS.Model.DriverNotesList = Backbone.Collection.extend({
    initialize: function () {
        this.on('reset', this.onReset, this);
    },
    url: 'dummy.aspx',
    model: CFS.Model.DriverNotes,
    getFormattedArray: function () {
        for (var i = 0, ii = this.length; i < ii; i++) {
            var m = this.at(i);
            m.set('idx', i + 1);
        }
        var toInject = {
            total: 1,
            page: 1,
            records: this.models.length,
            rows: this.toJSON()
        };
        return toInject;
    },
    onReset: function (arg) {
        for (var i = 0, ii = this.length; i < ii; i++) {
            var m = this.at(i);
            m.set('idx', i + 1);
            m.onChange();
        }
    }
});
CFS.Model.DriverNotesList = new CFS.Model.DriverNotesList();
//Driver
CFS.Model.Driver = Backbone.Model.extend(
{
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    defaults:
    {
        InsertedDate: null,
        LastUpdatedDate: null,
        Visible: 1
    },
    url: function () {
        return g_baseUrl + 'Driver/Save?id=' + (this.id ? this.id : '0');
    },
    idAttribute: 'DriverId',
    onChange: function () {
        var val = this.attributes;
        try {
            val.view = '<a href="#view/' + val.DriverId + '"><div class="ui-silk ui-silk-zoom"></div></a>';
            val.print = '<a href="#print/' + val.DriverId + '"><div class="ui-silk ui-silk-printer"></div></a>';
            val.NameText = '<a href="#view/' + val.DriverId + '">' + val.Name + '</a>';
            val.DateOfBirthText = this.date2text(val.DateOfBirth, 'd-mmm-yyyy');
            val.DriverSimExpiryDateText = this.date2text(val.DriverSimExpiryDate, 'd-mmm-yyyy');
            val.DriverKimExpiryDateText = this.date2text(val.DriverKimExpiryDate, 'd-mmm-yyyy');
            if (val.HasPhoto == 1) {
                val.HasPhotoText = '<a style="color:#000"; target="_blank"; href="/images/DriverPhoto/' + val.BadgeNumber + '.jpg">'+'Photo'+'</a>';
            }
            else {
                val.HasPhotoText = '';
            }
            if (val.IsActive == 1) {
                val.IsActiveText = 'Active';
            }
            else {
                val.IsActiveText = 'Inactive';
            }
            val.AgeText = this.calculateAge(this.date2text2(val.DateOfBirth, 'yyyy/mm/dd'));
        }
        catch (er) {
            C.Util.log(er);
        }
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'dd-mm-yyyy';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
    date2text2: function (dt, fmt) { //for age calculation
        if (!fmt) fmt = 'dd/mm/yyyy';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
    calculateAge: function (birthday) { // crew birthdate
        var today = new Date();
        var birthDate = new Date(birthday);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

});

CFS.Model.DriverList = Backbone.Collection.extend(
{
    initialize: function () {
        this.on('reset', this.onResetOrChange, this);
    },
    postData: {}, //from jqgrid
    totalRecords: 0,
    pageIndex: 0,
    pageSize: 15,
    model: CFS.Model.Driver,
    url: function () {
        var xurl = g_baseUrl + 'Driver/Page?pageIndex=' + this.pageIndex + '&pageSize=' + this.pageSize;
        var query = [];
        for (var key in this.postData) {
            if (this.postData.hasOwnProperty(key) && key.indexOf('flt') != -1) {
                query.push("&" + key + "=" + this.postData[key]);
            }
        }
        if (this.postData.sidx && this.postData.sidx.length) { query.push("&sidx=" + this.postData.sidx); }
        if (this.postData.sord && this.postData.sord.length) { query.push("&sord=" + this.postData.sord); }
        query.push('&t=' + (new Date()).valueOf());
        return xurl + query.join('');
    },
    parse: function (arg) {
        if (!arg.success) {
            alert(arg.message);
            return;
        }

        var pageSize = parseInt(this.pageSize, 10);
        var pageIndex = parseInt(this.pageIndex, 10);
        var id = (pageSize * pageIndex) + 1;
        for (var i = 0, ii = arg.records.length; i < ii; i++) {
            arg.records[i].No = id + i;
        }

        this.totalRecords = arg.totalRecords;
        return arg.records;
    },
    date2text: function (dt) {
        if (dt) { return C.parseIsoDateTime(dt).format('d-mmm-yyyy'); }
        else { return ''; }
    },
    fetchPage: function (postData) {
        this.pageIndex = postData.pageIndex;
        this.pageSize = postData.pageSize;
        this.postData = postData;
        this.fetch({ reset: true });
    },
    onResetOrChange: function () {
        var idx = this.pageIndex * this.pageSize + 1;
        for (var i = 0, ii = this.length; i < ii; i++) {
            this.at(i).onChange();
        }
        var toInject = {
            total: Math.ceil(this.totalRecords / this.pageSize),
            page: this.pageIndex + 1,
            records: this.totalRecords,
            rows: this.toJSON()
        };
        try {
            this.trigger('Driver.PageReady', toInject);
        } catch (ex) { }
    }
});