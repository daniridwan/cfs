﻿CFS.View.FormView = C.FormView.extend(
{
initialize: function() {
        this.gridDriverNotes = new CFS.View.GridDriverNotes({ el: $('#tbl-driver-notes') });
        this.dlgDriverNotes = new CFS.View.DlgDriverNotes({ el: $('#dlg-driver-notes') });
    },
    events: function() {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
        /* put additional event here */
            //"click #btn-scan": "onScanClick",
            "click #btn-add-driver-notes": "onAddDriverNotesClick",
            "click #btn-snap": "onSnapClick",
        });
    },
render: function() {

    // don't forget to call ancestor's render()
    C.FormView.prototype.render.call(this);

    var dateopt = { altFormat: "d-mmm-yyyy",
            dateFormat: 'dd-MM-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true,yearRange: "-100:+100"
        };
         
    $(".datepicker").datepicker({
       beforeShow: function (input, inst) {
        setTimeout(function () {
            inst.dpDiv.css({
                top: $(".datepicker").offset().top + 35,
                left: $(".datepicker").offset().left,
             
            });
        }, 0);
        }
    }); 

    this.validator = this.$("form").validate({ ignore: null });
    //ini mustinya tergantung permission

    $('#txt-date-of-birth').datepicker(_.extend(dateopt, {}));
    $('#txt-driver-ktp-timestamp').datepicker(_.extend(dateopt, {}));
    $('#txt-driver-sim-expiry-date').datepicker(_.extend(dateopt, {}));

    this.gridDriverNotes.render();
    this.dlgDriverNotes.render();

    this.dlgDriverNotes.on('created', this.onDriverNotesCreated, this);
    this.dlgDriverNotes.on('updated', this.onDriverNotesUpdated, this);
    this.gridDriverNotes.on('GridView.RequestDelete', this.onDeleteDriverNotesRequested, this);
    this.gridDriverNotes.on('GridView.RequestView', this.onViewDriverNotesRequested, this);
    this.gridDriverNotes.on('GridView.PageRequest', this.onPageDriverNotesRequested, this); // masih blm dipake
    this.enableEdit(true);

    //webcam
    this.video = document.getElementById('video');
    this.context = canvas.getContext('2d');
    this.video = document.getElementById('video');
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
            //video.src = window.URL.createObjectURL(stream);
            video.srcObject = stream;
            video.play();
        });
    }
},
onScanClick: function(ev){
    Msg.show('Please place the finger on the sensor, twice.');
    var $this = this;
    var att = $this.model.attributes;
    console.log(att);
    $.ajax({
        type: "POST",
        url: g_baseUrl + 'Crew/UserDevice',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(att),
        success: function(arg) {
            if(arg.success){
                Msg.hide();
                $('#txt-identification-number').val(arg.records);
            }
            else{
                Msg.hide();
                alert(arg.message);
            }
        }
    });
},
onSnapClick: function (ev) {
    this.context.drawImage(video, 0, 0, 300, 300);
    const canvas = document.querySelector("#canvas");
    const dataURI = canvas.toDataURL();
    document.getElementById("snapshot-link").href = dataURI; 
    //console.log(dataURI);
},
onEditClick: function(ev){
        //$("#btn-scan").show();
        $("#btn-snap").show();
        this.enableEditControl(true);
        C.FormView.prototype.onEditClick.call(this, ev);  
    },
onSaveClick: function(ev) {
    // ini override onSaveClick di ancestor
    var $this = this;
        if(this.validator.form()==false)
        {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-form').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else
        {
            this.$('.client-msg').hide();
        }
        

    // di sini mustinya ada validate
    // ...
    var toSave = this.$('.the-form').formHash();
    toSave.DriverNotes = CFS.Model.DriverNotesList.toJSON();
    if (this.model) //update
    {
        toSave.DriverPhoto = document.getElementById("snapshot-link").href;
        console.log(toSave.DriverPhoto);
        this.showThrobber(true);
        this.model.save(toSave,
            {
                success: function(model, response) {
                    if (response.success) {
                        $this.showThrobber(false);
                        $this.model.set(response.record);
                        $this.dataBind($this.model);
                        $this.makeReadOnly();
                        $this.renderServerMessage(response);
                        $this.trigger('FormView.Updated', $this.model);
                        $this.enableEditControl(false);
                        //disable cam
                    }
                    else {
                        $this.renderServerMessage(response);
                    }
                },
                error: function(model, response) {
                    $this.showThrobber(false);
                    C.Util.log(response);
                    if (window.console) console.log(response);
                }
            });
    }
    else //insert
    {
        var objSave = new CFS.Model.Driver();
        toSave.DriverPhoto = document.getElementById("snapshot-link").href;
        delete (toSave.DriverId);
        Msg.show('saving..');
        objSave.save(toSave,
    	    {
    	        success: function(model, response) {
    	            if (response.success) {
    	                $this.showThrobber(false);
    	                $this.model = objSave;
    	                $this.model.set(response.record);
    	                $this.dataBind($this.model);
    	                $this.makeReadOnly();
    	                $this.renderServerMessage(response);
                        $this.trigger('FormView.Updated', $this.model);
                        $this.enableEditControl(false);
    	            }
    	            else {
    	                $this.renderServerMessage(response);
    	            }
    	        },
    	        error: function(model, response) {
    	            $this.showThrobber(false);
    	            if (window.console) console.log(response);
    	        }
    	    });
    }
    //$("#btn-scan").hide();
    $("#btn-snap").hide();
},
prepareForInsert: function() {
    C.FormView.prototype.prepareForInsert.apply(this);
    this.validator.resetForm();
    this.clearAuditTrail();
    this.enableEditControl(true);
    //$("#btn-scan").hide();
    const context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    $("#btn-snap").show();
    document.getElementById("snapshot-link").href = "#";
    CFS.Model.DriverNotesList.reset();
    this.gridDriverNotes.dataBind(CFS.Model.DriverNotesList.getFormattedArray());
},
dataBind: function(arg) {
    C.FormView.prototype.dataBind.apply(this, arguments);
    $('#txt-date-of-birth').val(arg.get('DateOfBirthText'));
    $('#txt-driver-sim-expiry-date').val(arg.get('DriverSimExpiryDateText'));
    $('#txt-driver-kim-expiry-date').val(arg.get('DriverKimExpiryDateText'));
    document.getElementById("snapshot-link").href = arg.get('DriverPhoto');
    if(arg.get('CreatedTimestamp') != null)
        {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else
        {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
    if(arg.get('UpdatedTimestamp') != null)
        {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else
        {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
},
prepareForUpdate: function(arg) {
    this.dataBind(arg);
    this.validator.resetForm();
    this.makeReadOnly();
    this.enableEditControl(false);

    const context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);

    CFS.Model.DriverNotesList.reset(arg.attributes.DriverNotes);
    this.gridDriverNotes.dataBind(CFS.Model.DriverNotesList.getFormattedArray());
},
clearAuditTrail: function() {
    $('#lbl-insert-date').text('-');
    $('#lbl-insert-by').text('-');
    $('#lbl-last-updated-date').text('-');
    $('#lbl-last-updated-by').text('-');
},
enableEditControl: function(en) {
        if (en) {
            this.$('button.ui-datepicker-trigger').show();
            //$("#btn-scan").show();
            $("#btn-snap").show();
            $('#btn-add-driver-notes').show();
            this.gridDriverNotes.showControl(true);
        }
        else {
            this.$('button.ui-datepicker-trigger').hide();
            //$("#btn-scan").hide();
            $("#btn-snap").hide();
            $('#btn-add-driver-notes').hide();
            this.gridDriverNotes.showControl(false);
        }
    },
onAddDriverNotesClick: function (ev) {
    this.dlgDriverNotes.prepareForInsert();
    this.dlgDriverNotes.show(true);
},
onDriverNotesCreated: function (arg) {
    CFS.Model.DriverNotesList.push(arg, {
        silent: true
    });
    var i = 1;
    var toInject = CFS.Model.DriverNotesList.getFormattedArray();
    this.gridDriverNotes.dataBind(toInject);
    this.dlgDriverNotes.show(false);
},
onDriverNotesUpdated: function (arg) {
    var toInject = CFS.Model.DriverNotesList.getFormattedArray();
    //alert(toInject);
    this.gridDriverNotes.dataBind(toInject);
    this.dlgDriverNotes.show(false);
},
onDeleteTruckCompartmentRequested: function (id) {
    var record = CFS.Model.DriverNotesList.get(id);
    // deprecated in current backbone version
    //var record = FDM.Model.TruckCompartmentList.getByCid(id);
    //var Info = '' + record.get('MaxCapacity') + ', ';
    if (confirm('Are you sure want to delete notes no  ' + record.get('idx'))) {
        CFS.Model.DriverNotesList.remove(record);
        var toInject = CFS.Model.DriverNotesList.getFormattedArray();
        this.gridDriverNotes.dataBind(toInject);
    }
},
onViewDriverNotesRequested: function (id) {
    var record = CFS.Model.DriverNotesList.get(id);
    // deprecated in current backbone version
    //var record = FDM.Model.TruckCompartmentList.getByCid(id);
    //this.dlgTruckCompartment.prepareForUpdate(record);
    //this.dlgTruckCompartment.show(true);
}
});