﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-driver').jqGrid(
        {
            pager: 'tbl-driver-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },
            
            
            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 20, 25, 30, 35, 40, 45],
            colNames: ['No', '', '', 'Badge Number', 'Name', 'Place Of Birth', 'Date Of Birth','Age', 'SIM Expiry Date', 'KIM Expiry Date', 'Status', 'Has Photo'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 20, sortable: false, hidden: false },
                { name: 'print', width: 20, sortable: false, hidden: false },
                { name: 'BadgeNumber', index: 'BadgeNumber', width: 120, sortable: true },
                { name: 'Name', index: 'Name', width: 100, sortable: true },
                { name: 'PlaceOfBirth', index: 'PlaceOfBirth', width: 120, sortable: true },
                { name: 'DateOfBirthText', index: 'DateOfBirth', width: 120, sortable: true },
                { name: 'AgeText', width: 50, sortable: false },
                { name: 'DriverSimExpiryDateText', index: 'DriverSimExpiryDate', width: 135, sortable: true },
                { name: 'DriverKimExpiryDateText', index: 'DriverKimExpiryDate', width: 135, sortable: true },
                { name: 'IsActiveText', index: 'IsActive', width: 80, sortable: true },   
                { name: 'HasPhotoText', width: 80, sortable: false },   
        ],
            viewrecords: true,
            sortorder: "asc",
            sortname: "DriverId",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-driver-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {    
            $('#tbl-driver')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-driver').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    },
    
});

