﻿CFS.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.driverColl = new CFS.Model.DriverList();

        // create views
        this.tabView = new CFS.View.TabView();
        this.gridView = new CFS.View.GridView();
        this.formView = new CFS.View.FormView({ el: $('#pnl-form') });
        this.filterView = new CFS.View.FilterView({ el: $('#pnl-search') });

        // setup event routes
        this.gridView.bind('GridView.PageRequest', this.onPageRequested, this);
        this.driverColl.bind('Driver.PageReady', this.onPageReady, this);
        this.formView.bind('FormView.Created', this.onCreateNew, this);
        this.formView.bind('FormView.Updated', this.onUpdated, this);
        this.filterView.bind('FilterView.Changed', this.onFilterChanged, this);
        this.filterView.bind('FilterView.Export', this.onExportClick, this);
        
    },
    routes: {
        "": "grid",
        "pnl-grid": "grid",
        "view/:id": "view",
        "new": "openForm",
        "print/:id" : "onPrintClick",
    },
    start: function () {
        this.tabView.render();
        this.filterView.render();
        this.gridView.render();
        this.formView.render();
        Backbone.history.start();
    },
    view: function (id) {
        var user = this.driverColl.get(id);
        if (user) {
            this.formView.prepareForUpdate(user);
            this.formView.makeReadOnly();
            this.tabView.tabObj.click(1);
            $(window).scrollTop($('#pnl-form').offset().top);
        }
        else
            this.navigate('');
    },
    onCreateNew: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    grid: function () {
        this.tabView.tabObj.click(0);
    },
    openForm: function () {
        this.formView.prepareForInsert();
        this.formView.clearAuditTrail();
        this.formView.validator.resetForm();
        this.tabView.tabObj.click(1);
    },
    onUpdated: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    onFilterChanged: function (filter) {
        this.gridView.setFilter(filter.param);
        this.gridView.refresh();
    },
    onPageRequested: function (arg) {
        Msg.show('Loading...');
        this.driverColl.fetchPage(arg);
    },
    onPageReady: function (arg) {
        Msg.hide();
        this.gridView.dataBind(arg);
    },
    onExportClick: function (arg) {
        var url = this.driverColl.url();
        url = url.replace('Driver/Page', 'HttpHandlers/DriverExporter.ashx');
        this.navigate('');
        window.open(url);
    },
    onPrintClick: function (id) {
        // redirect to another page
        var url = this.driverColl.url();
        console.log(url);
        url = url.replace(url, g_baseUrl + 'HttpHandlers/KIMPdfExporter.ashx?id=' + id);
        window.open(url);
        this.navigate('');
    }
});

$(document).ready(function()
{
    CFS.AppRouter = new CFS.Router.AppRouter();
    CFS.AppRouter.start();
});
