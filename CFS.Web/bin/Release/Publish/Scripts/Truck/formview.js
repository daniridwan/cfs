﻿FDM.View.FormView = C.FormView.extend(
{
initialize: function() {
        this.gridTruckCompartment = new FDM.View.GridTruckCompartment({ el: $('#tbl-truck-compartment') });
        this.dlgTruckCompartment = new FDM.View.DlgTruckCompartment({ el: $('#dlg-truck-compartment') });
    },
    events: function() {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
        /* put additional event here */
        "click #btn-add-truck-compartment": "onAddTruckCompartmentClick",
        "click #btn-scan": "onScanClick",
        });
    },
render: function() {

    // don't forget to call ancestor's render()
    C.FormView.prototype.render.call(this);

    var dateopt = { altFormat: "d-mmm-yyyy",
            dateFormat: 'dd-MM-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true,yearRange: "-100:+100"
        };

    $(".datepicker").datepicker({
    beforeShow: function (input, inst) {
        setTimeout(function () {
            inst.dpDiv.css({
                top: $(".datepicker").offset().top + 35,
                left: $(".datepicker").offset().left
            });
        }, 0);
        }
    }); 

    $('#txt-truck-keur-timestamp').datepicker(_.extend(dateopt, {}));
    $('#txt-truck-kip-timestamp').datepicker(_.extend(dateopt, {}));
    $('#txt-truck-stnk-timestamp').datepicker(_.extend(dateopt, {}));
    $('#txt-tank-tera-timestamp').datepicker(_.extend(dateopt, {}));
    $('#txt-tank-keur-timestamp').datepicker(_.extend(dateopt, {}));

    this.validator = this.$("form").validate({ ignore: null });

    this.gridTruckCompartment.render();
    this.dlgTruckCompartment.render();

    this.dlgTruckCompartment.on('created', this.onTruckCompartmentCreated, this);
    this.dlgTruckCompartment.on('updated', this.onTruckCompartmentUpdated, this);
    this.gridTruckCompartment.on('GridView.RequestDelete', this.onDeleteTruckCompartmentRequested, this);
    this.gridTruckCompartment.on('GridView.RequestView', this.onViewTruckCompartmentRequested, this);
    this.gridTruckCompartment.on('GridView.PageRequest', this.onPageTruckCompartmentRequested, this); // masih blm dipake

    //ini mustinya tergantung permission
    this.enableEdit(true);
},
onScanClick: function(ev){
        Msg.show('Please place the card near the card reader.');
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'Biostar/ScanCard',
            contentType: "application/json; charset=utf-8",
            success: function(arg) {
                if(arg.success){
                    Msg.hide();
                    $('#txt-identification-number').val(arg.records);
                }
                else{
                    Msg.hide();
                    alert(arg.message);
                }
            }
        });
    },
onEditClick: function(ev){
        C.FormView.prototype.onEditClick.call(this, ev);  
        this.enableEditControl(true);
    },
onSaveClick: function(ev) {
    // ini override onSaveClick di ancestor
    var $this = this;
        if(this.validator.form()==false)
        {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-form').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else
        {
            this.$('.client-msg').hide();
        }

    // di sini mustinya ada validate
    // ...
    var toSave = this.$('.the-form').formHash();
    toSave.TruckCompartment = FDM.Model.TruckCompartmentList.toJSON();
    if (this.model) //update
    {
        this.showThrobber(true);
        Msg.show('Updating..');
        this.model.save(toSave,
            {
                success: function(model, response) {
                    if (response.success) {
                        $this.showThrobber(false);
                        $this.model.set(response.record);
                        $this.dataBind($this.model);
                        $this.makeReadOnly();
                        $this.renderServerMessage(response);
                        $this.trigger('FormView.Updated', $this.model);
                        $this.enableEditControl(false);
                    }
                    else {
                        $this.renderServerMessage(response);
                    }
                },
                error: function(model, response) {
                    $this.showThrobber(false);
                    C.Util.log(response);
                    if (window.console) console.log(response);
                }
            });
    }
    else //insert
    {
        var objSave = new FDM.Model.Truck();
        delete (toSave.TruckId);
        Msg.show('Saving..');
        objSave.save(toSave,
    	    {
    	        success: function(model, response) {
    	            if (response.success) {
    	                $this.showThrobber(false);
    	                $this.model = objSave;
    	                $this.model.set(response.record);
    	                $this.dataBind($this.model);
    	                $this.makeReadOnly();
    	                $this.renderServerMessage(response);
    	                $this.trigger('FormView.Updated', $this.model);
                        $this.enableEditControl(false);
    	            }
    	            else {
    	                $this.renderServerMessage(response);
    	            }
    	        },
    	        error: function(model, response) {
    	            $this.showThrobber(false);
    	            if (window.console) console.log(response);
    	        }
    	    });
    }
    $("#btn-scan").hide();
},
prepareForInsert: function() {
    C.FormView.prototype.prepareForInsert.apply(this);
    this.validator.resetForm();
    this.clearAuditTrail();
    this.enableEditControl(true);
    //reset child
    FDM.Model.TruckCompartmentList.reset();
    this.gridTruckCompartment.dataBind(FDM.Model.TruckCompartmentList.getFormattedArray());
},
dataBind: function(arg) {
    C.FormView.prototype.dataBind.apply(this, arguments);
    $('#txt-truck-keur-weight').val(arg.get('TruckKeurWeightText'));
    $('#txt-tank-max-capacity').val(arg.get('TankMaxCapacityText'));
    $('#txt-tank-safe-capacity').val(arg.get('TankSafeCapacityText'));
    $('#txt-truck-keur-timestamp').val(arg.get('TruckKeurTimestampText'));
    $('#txt-truck-kip-timestamp').val(arg.get('TruckKipTimestampText'));
    $('#txt-truck-stnk-timestamp').val(arg.get('TruckStnkTimestampText'));
    $('#txt-tank-tera-timestamp').val(arg.get('TankTeraTimestampText'));
    $('#txt-tank-keur-timestamp').val(arg.get('TankKeurTimestampText'));
    if(arg.get('CreatedTimestamp') != null)
        {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else
        {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
    if(arg.get('UpdatedTimestamp') != null)
        {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else
        {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
},
prepareForUpdate: function(arg) {
        this.dataBind(arg);
        this.validator.resetForm();
        this.makeReadOnly();
        this.enableEditControl(false);

        FDM.Model.TruckCompartmentList.reset(arg.attributes.TruckCompartment);
        this.gridTruckCompartment.dataBind(FDM.Model.TruckCompartmentList.getFormattedArray());
},
clearAuditTrail: function() {
    $('#lbl-insert-date').text('-');
    $('#lbl-insert-by').text('-');
    $('#lbl-last-updated-date').text('-');
    $('#lbl-last-updated-by').text('-');
},
enableEditControl: function(en) {
        if (en) {
            this.$('button.ui-datepicker-trigger').show();
            $("#btn-scan").show();
            $('#btn-add-truck-compartment').show();
            this.gridTruckCompartment.showControl(true);
        }
        else {
            this.$('button.ui-datepicker-trigger').hide();
            $("#btn-scan").hide();
            $('#btn-add-truck-compartment').hide();
            this.gridTruckCompartment.showControl(false);
        }
    },
//truck compartment
onAddTruckCompartmentClick  : function(ev) {
        this.dlgTruckCompartment.prepareForInsert();
        this.dlgTruckCompartment.show(true);
    },
onTruckCompartmentCreated:function(arg)
    {
		FDM.Model.TruckCompartmentList.push(arg, {
				silent: true
			});
		var i=1;
        var toInject = FDM.Model.TruckCompartmentList.getFormattedArray();
        this.gridTruckCompartment.dataBind(toInject);
        this.dlgTruckCompartment.show(false);
    },
onTruckCompartmentUpdated:function(arg)
    {
        var toInject = FDM.Model.TruckCompartmentList.getFormattedArray();
        //alert(toInject);
        this.gridTruckCompartment.dataBind(toInject);
        this.dlgTruckCompartment.show(false);
    },
onDeleteTruckCompartmentRequested:function(id)
    {
        var record = FDM.Model.TruckCompartmentList.get(id);
        // deprecated in current backbone version
        //var record = FDM.Model.TruckCompartmentList.getByCid(id);
        //var Info = '' + record.get('MaxCapacity') + ', ';
        if (confirm('Are you sure want to delete compartment no  '+ record.get('idx'))) {
            FDM.Model.TruckCompartmentList.remove(record);
            var toInject = FDM.Model.TruckCompartmentList.getFormattedArray();
            this.gridTruckCompartment.dataBind(toInject);
        }
    },
onViewTruckCompartmentRequested:function(id)
    {
        var record = FDM.Model.TruckCompartmentList.get(id);
        // deprecated in current backbone version
        //var record = FDM.Model.TruckCompartmentList.getByCid(id);
        //this.dlgTruckCompartment.prepareForUpdate(record);
        //this.dlgTruckCompartment.show(true);
    }
});