﻿CFS.View.tGridPlbDocument =
{
    initialize: function (el) {
    },
    events: {
        "click .cmd-view": "onViewClick",
        "click .cmd-del": "onDeleteClick",
        "click .cmd-approve": "onApproveClick"
    },
    showControl: function (en) {
        if (en) {
            this.gridObj.showCol('view');
            this.gridObj.showCol('del');
            this.gridObj.showCol('cb');
        }
        else {
            this.gridObj.hideCol('view');
            this.gridObj.hideCol('del');
            this.gridObj.hideCol('cb');
        }
    },
    showApprovalControl: function (en) {
        if (en) {
            this.gridObj.showCol('appr');
        }
        else {
            this.gridObj.hideCol('appr');
        }
    },
    render: function () {
        var $this = this;
        this.gridObj = this.$el.jqGrid({
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },
            jsonReader: {
                id: 0,
                repeatitems: false
            },
            colNames: ['Id', 'No', '', '', 'Sppb No', 'Sppb Date', 'Quantity', 'Approve Timestamp', 'Approved By'],
            colModel: [
                       { name: 'cid', width: 60, hidden: true },
                       { name: 'idx', width: 30, hidden: false },
                       { name: 'view', width: 24, sortable: false, hidden: false },
                       { name: 'appr', width: 24, sortable: false, hidden: false },
                       //{ name: 'del', width: 24, sortable: false, hidden: false },
                       { name: 'PlbSppbNo', width: 140, sortable: true },
                       { name: 'PlbSppbDateText', index:'PlbSppbDate', width: 140, sortable: true },
                       { name: 'PlbQuantity', width: 140, sortable: true },
                       { name: 'ApprovedTimestampText', width: 170, sortable: true },
                       { name: 'ApprovedByName', index: 'ApprovedBy', width: 170, sortable: true },
                      ],
            viewrecords: true,
            rowNum: 50,
            sortorder: "asc",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() * 0.95,
            pginput: true,
            altRows: true,
            prmNames: {
                rows: 'pageSize',
                page: 'pageIndex'
            }

        });
    },
    dataBind: function (toInject) {
        try {
            this.$el[0].addJSONData(toInject);
            $('.soft-delete', this.$el).each(function (key, val) {
                $(val).parent().parent().addClass('cancel');
            });
            this.$('.warnrest').each(function (key, val) {
                $(val).parent().parent().addClass('warnrest');
            });
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    onViewClick: function (ev) {
        var id = $(ev.target).parent().parent().attr('id');
        this.trigger('GridView.RequestView', id);
    },
    onActualClick: function (ev) {
        var id = $(ev.target).parent().parent().attr('id');
        this.trigger('GridView.RequestActual', id);
    },
    onApproveClick: function (ev) {
        var id = $(ev.target).parent().parent().attr('id');
        this.trigger('GridView.RequestApprove', id);
    },
    onDeleteClick: function (ev) {
        var id = $(ev.target).parent().parent().attr('id');
        this.trigger('GridView.RequestDelete', id);
    },
};
CFS.View.GridPlbDocument = Backbone.View.extend(CFS.View.tGridPlbDocument);