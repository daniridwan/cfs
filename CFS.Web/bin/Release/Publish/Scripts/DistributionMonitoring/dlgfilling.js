﻿CFS.View.tDlgFilling =
    {
        initialize: function () {
        },
        events: {
            "click .dlg-tool-cmd-save": "onOkClick",
            "click .dlg-tool-cmd-cancel": "onCancelClick",
            "click .btn-calculate": "onCalculateClick",
        },
        render: function () {
            this.$el.jqm({
                modal: true
            });
            //
            this.$el.jqDrag('.dlg-title-block');
            this.validator = this.$el.validate({ onsubmit: false });
            this.type = '';

        },
        clear: function () {
            C.clearField(this.$el);
            this.validator.resetForm();
        },
        show: function (en, type) {
            this.$el[en ? 'jqmShow' : 'jqmHide']();
            if (type === 'Fuel') {
                this.type = type;
                $('.li-fuel').show();
                $('.li-chemical').hide();
            }
            else {
                this.type = type;
                $('.li-fuel').hide();
                $('.li-chemical').show();
                this.setupTimer('Chemical');
            }
        },
        setupTimer: function (type) {
            var $this = this;
            if (this.type === 'Chemical') {
                this.timerId = setInterval(function () {
                    $this.onTimer();
                }, 2000);
            }
        },
        onGetWeight: function () {
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'DistributionMonitoring/GetWeight',
                contentType: "application/json; charset=utf-8",
                //data: JSON.stringify(param),
                success: function (arg) {
                    if (arg.success) {
                        $('#txt-scan-weight').val(arg.records);
                    }
                    else {
                        $('#txt-scan-weight').val(arg.message);
                    }
                }
            });
        },
        onCancelClick: function (ev) {
            this.type = '';
            this.show(false);
        },
        onOkClick: function (ev) {
            // musti validate selection
            if (!this.validator.form())
                return;

            var val = this.$el.formHash();
            if (this.type == 'Chemical') {
                val.Actual = 0;
                val.InitialWeight = $("#txt-filling-initial-weight").val();
                val.FinalWeight = $("#txt-filling-final-weight").val();
            }
            else {
                val.Actual2 = $("#txt-filling-actual").val();
                val.Actual = Math.round(val.Actual2) / 1000;
                val.InitialWeight = 0;
                val.FinalWeight = 0;
            }
            
            if (this.currentRecord) // update
            {
                this.currentRecord.set(val, { silent: true });
                this.trigger('updated', this.currentRecord);
                this.show(false);
            }
        },
        prepareForUpdate: function (/*model*/arg) {
            this.currentMode = 'update';
            this.currentRecord = arg;
            this.dataBind(arg.attributes);
            //reset val
            $("#txt-flow-meter").val('');
            $("#txt-flow-meter").text('');
            $("#txt-filling-actual").val(arg.attributes.Actual);
            $("#txt-filling-initial-weight").val(arg.attributes.InitialWeight);
            $("#txt-filling-final-weight").val(arg.attributes.FinalWeight);
            var toSend =
            {
                fillingPointName: arg.attributes.FillingPointName
            };
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'FillingPoint/GetFillingPoint',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(toSend),
                success: function (arg) {
                    if (arg.success) {
                        $("#txt-correction-factor").text(arg.records.CorrectionFactor);
                        $("#txt-correction-factor").val(arg.records.CorrectionFactor);
                    }
                    else {
                        alert(arg.message);
                    }
                }
            });
        },
        dataBind: function (val) {
            this.$el.formHash(val);
        },
        prepareForInsert: function () {
            this.clear();
            this.currentMode = 'insert';
            this.currentRecord = null;
        },
        onTimer: function (type) {
            if (this.type == 'Chemical') {
                this.onGetWeight();
            }
        },
        onCalculateClick: function (ev) {
            var correction = $("#txt-correction-factor").val();
            var flowmeter = $("#txt-flow-meter").val();
            var actual = correction * flowmeter;
            //$("#txt-filling-actual").val(Math.round(actual));
            $("#txt-filling-actual").val(actual);
        }
    };
CFS.View.DlgFilling = Backbone.View.extend(CFS.View.tDlgFilling);