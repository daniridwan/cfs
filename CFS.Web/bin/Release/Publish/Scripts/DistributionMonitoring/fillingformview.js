﻿CFS.View.FillingFormView = C.FormView.extend(
{
    initialize: function () {
        
    },
    events: function () {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
            /* put additional event here */
            "click .cmd-back": "onBackClick",
            "change #ddl-filling-delivery-order": "onChangeTruck"
        });
    },
    render: function () {

        // don't forget to call ancestor's render()
        C.FormView.prototype.render.call(this);

        var dateopt = { altFormat: "dd-mm-yy",
            dateFormat: 'd-M-yy', showOn: 'button', duration: 'fast', buttonImage: g_baseUrl + '/Images/calendar.png'
        };
        this.validator = this.$("form").validate({ ignore: ":hidden:not(.chosen-select)" });

        //ini mustinya tergantung permission
        this.enableEdit(false);

        this.gridFilling = new CFS.View.GridFilling({ el: $('#tbl-filling') });
        this.dlgFilling = new CFS.View.DlgFilling({ el: $('#dlg-filling') });

        this.gridFilling.render();
        this.dlgFilling.render();
        
        this.gridFilling.on('GridView.RequestView', this.onViewFillingRequested, this);

        this.dlgFilling.on('updated', this.onFillingUpdated, this);
        //unused
        //this.gridFilling.on('GridView.PageRequest', this.onPageDeliveryOrderDetailRequested, this);
        //this.gridFilling.on('GridView.RequestDelete', this.onDeleteFillingRequested, this);
        //always show control
        this.gridFilling.showControl(true);
    },
    onEditClick: function (ev) {
        C.FormView.prototype.onEditClick.call(this, ev);
    },
    onRenderTruck: function(arg){
        $('#ddl-filling-delivery-order').val('').trigger("chosen:updated").find('option').remove().end();
        var param = { shipmentStatus: '2' };
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'DistributionMonitoring/GetInProcessTruckList',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(param),
            success: function (arg) {
                if (arg.success) {
                    var max = arg.records.length;
                    if(max > 0){
                        var ddlTruck = $("#ddl-filling-delivery-order");
                        ddlTruck.append($("<option>", { value : '', text: ''}));
                        for (i = 0; i < max; i++) {
                            ddlTruck.append($("<option>", { value: arg.records[i].TruckLoadingId, text: arg.records[i].DeliveryOrderNumber}));
                        }
                        ddlTruck.trigger("chosen:updated");
                    }
                }
                else {
                    console.log(arg.message);
                }
            }
        });
    },
    onSaveClick: function (ev) {
        // ini override onSaveClick di ancestor
        var $this = this;
        if (this.validator.form() == false) {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-form').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else {
            this.$('.client-msg').hide();
        }
        // di sini mustinya ada validate
        // ...
        var toSave = this.$('.the-form').formHash();
        toSave.TruckLoadingPlanning = CFS.Model.TruckLoadingPlanningList.toJSON();

        var objSave = new CFS.Model.TruckLoading();
        delete (toSave.TruckLoadingId);

        var TruckLoadingId = $("#ddl-filling-delivery-order option:selected").val();
        toSave.TruckLoadingId = TruckLoadingId;
        Msg.show('Saving..');
        //modify default url
        objSave.url = g_baseUrl + 'DistributionMonitoring/Filling?id=' + (TruckLoadingId ? TruckLoadingId : '0');
        objSave.save(toSave,
    	    {
                success: function (model, response) {
                if (response.success) {
                    $this.showThrobber(false);
    	            $this.model = objSave;
    	            $this.model.set(response.record);
    	            $this.dataBind($this.model);
    	            $this.makeReadOnly();
                    $this.renderServerMessage(response);
                    $this.trigger('FillingFormView.Updated', $this.model);
                    $(".chosen-select").chosen("destroy");
                    $(".chosen-select").hide();
                    $this.enableEditControl(false);
    	        }
    	        else {
    	            $this.renderServerMessage(response);
    	            Msg.hide();
    	        }
            },
            error: function (model, response) {
    	        $this.showThrobber(false);
    	        if (window.console) console.log(response);
    	    }
    	 });
    },
    prepareForInsert: function () {
        C.FormView.prototype.prepareForInsert.apply(this);
        $(".chosen-select").chosen({ width: "250px", search_contains: true, no_results_text: "Oops, nothing found!, ", allow_single_deselect: true });
        this.validator.resetForm();
        this.clearAuditTrail();
        this.enableEditControl(true);

        CFS.Model.TruckLoadingPlanningList.reset();
        this.gridFilling.dataBind(CFS.Model.TruckLoadingPlanningList.getFormattedArray());
    },
    dataBind: function (arg) {
        C.FormView.prototype.dataBind.apply(this, arguments);
        $('#txt-total-preset').val(arg.get('TotalPresetText'));
        if (arg.get('CreatedTimestamp') != null) {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
        if (arg.get('UpdatedTimestamp') != null) {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
    },
    //edit moved to edit form view
    //prepareForUpdate: function (arg) {
    //    this.dataBind(arg);
    //    this.validator.resetForm();
    //    this.makeReadOnly();
    //    this.enableEditControl(false);
    //},
    clearAuditTrail: function () {
        $('#lbl-insert-date').text('-');
        $('#lbl-insert-by').text('-');
        $('#lbl-last-updated-date').text('-');
        $('#lbl-last-updated-by').text('-');
    },
    enableEditControl: function (en) {
        if (en) {
            this.$('button.ui-datepicker-trigger').show();
            this.gridFilling.showControl(true);
        }
        else {
            this.$('button.ui-datepicker-trigger').hide();
            this.gridFilling.showControl(false);
        }
    },
    onBackClick: function (ev) {
        $(".chosen-select").chosen("destroy");
        window.location.hash = '#';
    },
    onChangeTruck: function (ev) {
        var $this = this;
        var truckLoadingId = $('#ddl-filling-delivery-order').val();
        //reset current model
        var record = [];
        CFS.Model.TruckLoadingPlanningList.reset();
        var toInject = CFS.Model.TruckLoadingPlanningList.getFormattedArray();
        this.gridFilling.dataBind(toInject);
        //add new model
        if (truckLoadingId != '') {
            var param = { id: truckLoadingId };
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'DistributionMonitoring/GetTruckLoadingPlanning',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(param),
                success: function (arg) {
                    if (arg.success) {
                        var max = arg.records.length;
                        if (max > 0) {
                            $this.onGridBinding(arg);
                        }
                    }
                    else {
                        console.log(arg.message);
                    }
                }
            });
        }
    },
    onGridBinding: function (arg) {
        for (i = 0; i < arg.records.length; i++) {
            var val = new CFS.Model.TruckLoadingPlanning();
            val.set(arg.records[i]);
            //delete val.cid;
            CFS.Model.TruckLoadingPlanningList.push(val, { silent: true });
        }
        var toInject = CFS.Model.TruckLoadingPlanningList.getFormattedArray();
        this.gridFilling.dataBind(toInject);
    },
    onViewFillingRequested: function (id) {
        var record = CFS.Model.TruckLoadingPlanningList.get(id);
        // deprecated in current backbone version
        //var record = CFS.Model.TruckCompartmentList.getByCid(id);
        this.dlgFilling.prepareForUpdate(record);
        if (record.attributes.FillingPointGroup === 'Fuel') {
            this.dlgFilling.show(true, record.attributes.FillingPointGroup);
        }
        else {
            alert('Chemical product, please use scanner on weight bridge.');
        }
    },
    onFillingUpdated: function (arg) {
        var toInject = CFS.Model.TruckLoadingPlanningList.getFormattedArray();
        //alert(toInject);
        this.gridFilling.dataBind(toInject);
        this.dlgFilling.show(false);
    },
});