﻿CFS.View.pGridFilling =
    {
        initialize: function (el) {
        },
        events: {
            "click .cmd-view": "onViewClick",
            "click .cmd-del": "onDeleteClick",
        },
        showControl: function (en) {
            if (en) {
                this.gridObj.showCol('view');
                this.gridObj.showCol('del');
                this.gridObj.showCol('cb');
            }
            else {
                this.gridObj.hideCol('view');
                this.gridObj.hideCol('del');
                this.gridObj.hideCol('cb');
            }
        },
        render: function () {
            var $this = this;
            this.gridObj = this.$el.jqGrid({
                datatype: function (postData) {
                    if ($this.newFilter) {
                        $this.newFilter = false;
                        postData.pageIndex = 1;
                    }
                    postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                    var param = $.extend({}, postData);
                    if ($this.filter) {
                        $.extend(param, $this.filter);
                    }
                    $this.trigger('GridView.PageRequest', param);
                },
                jsonReader: {
                    id: 0,
                    repeatitems: false
                },
                colNames: ['Id', 'No', '', 'DO Number', 'Product', 'Product Type', 'Filling Point Name', 'Filling Point Group', 'Tank Source', 'Preset', 'Actual','Init. Weight', 'Final Weight', 'UOM'],
                colModel: [
                    { name: 'cid', width: 60, hidden: true },
                    { name: 'idx', width: 30, hidden: false },
                    { name: 'view', width: 24, sortable: false, hidden: true },
                    { name: 'DeliveryOrderNumber', width: 170, sortable: true },
                    { name: 'ProductName', width: 120, sortable: true },
                    { name: 'ProductType', width: 120, sortable: false },
                    { name: 'FillingPointName', width: 145, sortable: true },
                    { name: 'FillingPointGroup', width: 145, sortable: true },
                    { name: 'TankName', width: 120, sortable: true },
                    { name: 'Preset', index: 'Preset', width: 80, sortable: false },
                    { name: 'Actual', index: 'Actual', width: 80, sortable: false },
                    { name: 'InitialWeight', index: 'InitialWeight', width: 90, sortable: false },
                    { name: 'FinalWeight', index: 'FinalWeight', width: 90, sortable: false },
                    { name: 'UOM', width: 80, sortable: true },
                ],
                viewrecords: true,
                rowNum: 300,
                sortorder: "asc",
                height: 'auto',
                multiselect: false,
                shrinkToFit: false,
                width: $('#pnl-frame').width() * 0.95,
                pginput: true,
                altRows: true,
                prmNames: {
                    rows: 'pageSize',
                    page: 'pageIndex'
                }

            });
        },
        dataBind: function (toInject) {
            try {
                this.$el[0].addJSONData(toInject);
                $('.soft-delete', this.$el).each(function (key, val) {
                    $(val).parent().parent().addClass('cancel');
                });
                this.$('.warnrest').each(function (key, val) {
                    $(val).parent().parent().addClass('warnrest');
                });
            }
            catch (err) {
                C.Util.log(err);
            }
        },
        setFilter: function (filter) {
            this.filter = filter;
            this.newFilter = true;
        },
        refresh: function () {
            this.gridObj.trigger('reloadGrid');
        },
        onViewClick: function (ev) {
            var id = $(ev.target).parent().parent().attr('id');
            this.trigger('GridView.RequestView', id);
        },
        onActualClick: function (ev) {
            var id = $(ev.target).parent().parent().attr('id');
            this.trigger('GridView.RequestActual', id);
        },
    };
CFS.View.GridFilling = Backbone.View.extend(CFS.View.pGridFilling);