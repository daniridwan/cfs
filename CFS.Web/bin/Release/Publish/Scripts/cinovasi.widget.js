/*jslint passfail: false, white: false, onevar: false, browser: true, nomen: false*/

/*global jQuery: false, $j: true, Cinovasi: false, C: false, window: false,
    alert: false, console: false, Class: false, $p: false */

C.FormView = Backbone.View.extend(
{
    canEdit: false,
    render: function () {
        this.$('.cmd-edit')[this.canEdit ? 'show' : 'hide']();
    },
    events:
    {
        "click .cmd-edit": "onEditClick",
        "click .cmd-cancel": "onCancelClick",
        "click .cmd-save": "onSaveClick",
        "click .cmd-cancel-edit": "onCancelEditClick",
        "click .cmd-back": "onBackClick"
    },
    dataBind: function (model) {
        this.model = model;
        this.$('.the-form').formHash(model.attributes);
        this.$('.client-msg').hide();
        this.$('.server-msg').hide();
    },
    onBackClick: function (ev) {
        window.location.hash = '#';
    },
    onEditClick: function (event) {
        this.$('.client-msg').hide();
        this.$('.server-msg').hide();
        this.makeEditable();
        this.trigger('evSingleView_StatusEditable', {});
    },
    onCancelClick: function (event) {
        this.trigger('evSingleView_RequestClose', {});
    },
    onCancelEditClick: function (event) {
        if (typeof this.hookBeforeCancelEdit == 'function') {
            var result = this.hookBeforeCancelEdit();
            if (!result) {
                return false;
            }
        }
        this.trigger('evSingleView_RequestRollback', {});
    },
    onSaveClick: function (event) {
        alert('C.FormView.onSaveClick is an abstract function');
    },
    showThrobber: function (enable) {
        if (enable) { this.$('.form-tool-throbber').css('visibility', 'visible'); }
        else { this.$('.form-tool-throbber').css('visibility', 'hidden'); }
    },
    showNotification: function (enable) {
        if (enable) { this.$('#pnl-notification').css('visibility', 'visible'); }
        else { this.$('#pnl-notification').css('visibility', 'hidden'); }
    },
    makeReadOnly: function () {
        /*Read Only*/
        Cinovasi.makeStatic(this.$el);
        this.$('.cmd-save').hide();
        this.$('.cmd-cancel-edit').hide();
        this.$('.cmd-edit')[this.canEdit ? 'show' : 'hide']();
        this.trigger('evSingleView_StatusReadOnly', {});
        this.editable = false;
    },
    enableEdit: function (en) {
        this.canEdit = en;
        this.$('.cmd-edit')[this.canEdit ? 'show' : 'hide']();
    },
    makeEditable: function () {
        Cinovasi.makeUnStatic(this.$el);
        this.$('.cmd-cancel-edit').show();
        this.$('.cmd-edit').hide();
        this.$('.cmd-save').show();
        this.trigger('evSingleView_StatusEditable', {});
        this.editable = true;
    },
    clear: function () {
        C.clearField(this.$el);
    },
    prepareForInsert: function () {
        this.model = null;
        this.makeEditable();
        C.clearField(this.$el);
        this.$('.server-msg').hide();
        this.$('.client-msg').hide();
        this.$('.cmd-cancel-edit').hide();
        this.$('select').each(function (i, el) {
            var $el = $j(el);
            if ($el.hasClass('chzn-done')) {
                var id = $el.attr('id');
                if (id && id.length) {
                    $j("#" + id).trigger("liszt:updated");
                }
            }
        });

    },
    renderHttpError: function (arg) {
        var msg = [];
        msg.push(arg.status + ' ' + arg.statusText);
        msg.push(arg.responseText);
        msg = msg.join('<br/>');
        var oMsg = this.$('.server-msg');
        oMsg.addClass('pnl-errmsg');
        oMsg.removeClass('pnl-okmsg');
        oMsg.html(msg);
        oMsg.show();
    },
    renderServerMessage: function (arg) {
        if (arg.success) {
            this.$('.server-msg').addClass('pnl-okmsg');
            this.$('.server-msg').removeClass('pnl-errmsg');
            this.$('.server-msg').html('Record saved successfully');
        }
        else {
            this.$('.server-msg').addClass('pnl-errmsg');
            this.$('.server-msg').removeClass('pnl-okmsg');
            var msg = ['<ul>'];
            for (var i = 0, ii = arg.messages.length; i < ii; i++) {
                msg.push('<li>' + arg.messages[i] + '</li>');
            }
            msg.push('</ul>');
            this.$('.server-msg').html(msg.join(''));
        }
        this.$('.server-msg').show();
    },
    renderClientMessage: function (toSave) {
        var msg = ['<ul class="msglist">'];
        var val = this.validator.form();
        for (var i = 0, ii = this.validator.numberOfInvalids(); i < ii; i++) {
            msg.push('<li>' + this.validator.errorList[i].message + '</li>');
        }
        msg.push('</ul>');
        if (!val) {
            this.$('.client-msg').addClass('pnl-errmsg');
            this.$('.client-msg').removeClass('pnl-okmsg');
            this.$('.client-msg').html(msg.join(''));
            this.$('.server-msg').hide();
        }
        else {
            this.$('.client-msg').html('');
        }
        return val;
    }
});

C.FilterView = Backbone.View.extend({
    events: {
        "change select": "onDropDownChanged",
        "keypress input[type='text']": "onTextKeypress",
        "click .cmd-reset-filter": "onFilterReset",
        "click .cmd-refresh": "onRefresh"
    },
    onDropDownChanged:function(ev)
    {
        this.trigger('FilterView.Changed ',{param:this.getSearchParam()});
        return false;
    },
    onTextKeypress:function(ev)
    {
        if(ev.keyCode === 13)
        {
            if($(ev.target).parent().hasClass('chzn-search')) return;
            this.trigger('FilterView.Changed',{param:this.getSearchParam()});
            return false;
        }
    },
    onFilterReset:function(ev)
    {
        C.clearField(this.$el);
        this.trigger('FilterView.Changed ',{param:this.getSearchParam()});
        return false;
    },
    onRefresh:function(ev)
    {
        this.trigger('FilterView.Changed ',{param:this.getSearchParam()});
        return false;
    },
    /** General Function **/
    getSearchParam:function()
    {
        var value = this.$('form').formHash();
        return value;
    }
});
