﻿CFS.View.FormView = C.FormView.extend(
{
    initialize: function () {
        this.uploader = new C.Uploader({
            selector: '#trg-upload',
            id: 'rev',
            allowedExtensions: ['pdf','doc','docx'],
            sizeLimit: 5242880
        });
    },
    events: function() {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
        /* put additional event here */
            "change #ddl-type": "onChangeType",
            "click  #btn-estimate-date": "onClickEstimateDate",
            "click .delete-trigger": "onDeleteAttachment"
        });
    },

    render: function() {

    // don't forget to call ancestor's render()
    C.FormView.prototype.render.call(this);

    var dateopt = { altFormat: "d-mmm-yyyy",
            dateFormat: 'dd-MM-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true,
        };

    this.validator = this.$("form").validate({ ignore: null, onkeyup: false });

    $(".datepicker").datepicker({
        beforeShow: function (input, inst) {
            setTimeout(function () {
                inst.dpDiv.css({
                    top: $(".datepicker").offset().top + 35,
                    left: $(".datepicker").offset().left
                });
            }, 0);
        }
    }); 

    //$('#txt-estimate-date').datepicker(_.extend(dateopt, {}));
    $('#txt-estimate-date').datetimepicker({
        controlType: 'select',
        dateFormat: "dd-MM-yy",
        timeFormat: "HH:mm",
    });
    this.uploader.on('evUploader_StatusUploadOK', this.onUploadOK);
    this.uploader.on('evUploader_StatusUploadError', this.onUploadError);
    this.uploader.setup();

    //ini mustinya tergantung permission
    this.enableEdit(true);
},
onEditClick: function(ev){
    C.FormView.prototype.onEditClick.call(this, ev);  
    this.enableEditControl(true);
},
onClickEstimateDate: function (ev) {            
    $('#txt-estimate-date').datetimepicker('show');
    ev.preventDefault();
},
onSaveClick: function(ev) {
    // ini override onSaveClick di ancestor
    var $this = this;
        if(this.validator.form()==false)
        {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-form').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else
        {
            this.$('.client-msg').hide();
        }

    // di sini mustinya ada validate
    // ...
    var toSave = this.$('.the-form').formHash();
    console.log(toSave);
    if (this.model) //update
    {
        this.showThrobber(true);
        this.model.save(toSave,
            {
                success: function(model, response) {
                    if (response.success) {
                        $this.showThrobber(false);
                        $this.model.set(response.record);
                        $this.dataBind($this.model);
                        $this.makeReadOnly();
                        $this.renderServerMessage(response);
                        $this.trigger('FormView.Updated', $this.model);
                        $this.enableEditControl(false);
                    }
                    else {
                        $this.renderServerMessage(response);
                    }
                },
                error: function(model, response) {
                    $this.showThrobber(false);
                    C.Util.log(response);
                    if (window.console) console.log(response);
                }
            });
    }
    else //insert
    {
        var objSave = new CFS.Model.VesselDocument();
        delete (toSave.VesselDocumentId);
        Msg.show('Saving..');
        objSave.save(toSave,
    	    {
    	        success: function(model, response) {
    	            if (response.success) {
    	                $this.showThrobber(false);
    	                $this.model = objSave;
    	                $this.model.set(response.record);
    	                $this.dataBind($this.model);
    	                $this.makeReadOnly();
    	                $this.renderServerMessage(response);
    	                $this.trigger('FormView.Updated', $this.model);
                        $this.enableEditControl(false);
    	            }
    	            else {
    	                $this.renderServerMessage(response);
    	            }
    	        },
    	        error: function(model, response) {
    	            $this.showThrobber(false);
    	            if (window.console) console.log(response);
    	        }
    	    });
    }
},
prepareForInsert: function() {
    C.FormView.prototype.prepareForInsert.apply(this);
    this.validator.resetForm();
    this.clearAuditTrail();
    this.enableEditControl(true);
    $('#lbl-file-name').text('');
    $('#lbl-file-name').removeAttr('href');
    $('#trg-upload').show();
    $('#trg-delete').hide();
},
dataBind: function(arg) {
    C.FormView.prototype.dataBind.apply(this, arguments);
    $('#txt-estimate-date').val(arg.get('EstimateDateText'));
    $('#lbl-file-name').text(arg.get('Filename'));
    $('#lbl-file-name').attr('href', g_baseUrl + 'HttpHandlers/Download.ashx?id=' + arg.get('VesselDocumentId'));
    if(arg.get('CreatedTimestamp') != null)
        {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else
        {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
    if(arg.get('UpdatedTimestamp') != null)
        {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else
        {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
},
prepareForUpdate: function(arg) {
    this.dataBind(arg);
    console.log(arg);
    this.validator.resetForm();
    this.makeReadOnly();
    this.enableEditControl(false);
    if (arg.attributes.Status === 'Scheduled') {
        this.enableEdit(true);
    }
    else {
        this.enableEdit(false);
    }
},
clearAuditTrail: function() {
    $('#lbl-insert-date').text('-');
    $('#lbl-insert-by').text('-');
    $('#lbl-last-updated-date').text('-');
    $('#lbl-last-updated-by').text('-');
},
enableEditControl: function(en) {
    if (en) {
        this.$('button.ui-datepicker-trigger').show();
        this.$('.btn-datetimepicker').show();
        $('#trg-upload').show();
        $('#trg-delete').show();
    }
    else {
        this.$('button.ui-datepicker-trigger').hide();
        this.$('.btn-datetimepicker').hide();
        $('#trg-delete').hide();
        $('#trg-upload').hide();
    }
},
onUploadOK: function (arg) {
    var fileinfo = arg.register[0];
    if (arg.id == 'rev') {
        $('#lbl-file-name').text(fileinfo.filename);
        $('#lbl-file-name').attr('href', g_baseUrl + 'HttpHandlers/Download.ashx?ticket=' + fileinfo.ticket + '&filename=' + fileinfo.filename);
        $('#hdn-file-name2').val(fileinfo.filename);
        $('#hdn-ticket').val(fileinfo.ticket);
        $('#trg-upload').hide();
        $('#trg-delete').show();
    }
},
onUploadError: function (arg) {
    if(arg.message == undefined)
    {
        alert("File melebihi kapasitas maksimum");
    }
    else
    {
        alert(arg.message);
    }
},
onDeleteAttachment: function (ev) {
    var id = $(ev.target).attr('id');
    if (id == 'trg-delete') {
        var filename = $('#hdn-file-name2').val();
        if (confirm('Apakah Anda ingin menghapus ' + filename + ' ?')) {
            $('#hdn-ticket').val('delete');
            $('#lbl-file-name').text('');
            $('#lbl-file-name').removeAttr('href');
            $('#hdn-file-name2').val('');
            $('#trg-upload').show();
            $('#trg-delete').hide();
        }
    }
}
});