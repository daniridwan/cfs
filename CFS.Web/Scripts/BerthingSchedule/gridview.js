﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-berthing-schedule').jqGrid(
        {
            pager: 'tbl-berthing-schedule-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', 'Estimate Date', 'Vessel Name','Local Agent', 'Product', 'Jetty', 'Quantity', 'UOM', 'Status', 'Is Active'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 18, sortable: false, hidden: false },
                { name: 'EstimateDateText', index:'EstimateDate', width: 120, sortable: true },
                { name: 'VesselName', index: 'VesselId', width: 150, sortable: true },
                { name: 'LocalAgent', width: 110, sortable: true },
                { name: 'ProductName', index: 'ProductId', width: 150, sortable: true },
                { name: 'Jetty', width: 100, sortable: true },
                { name: 'BlQuantity', width: 100, sortable: true },
                { name: 'BlUom', width: 90, sortable: true },
                { name: 'Status', index: 'IsActive', width: 110, sortable: true },
                { name: 'IsActiveText', index: 'IsActive', width: 100, sortable: true },
		],
            viewrecords: true,
            sortorder: "desc",
            sortname: "EstimateDate",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-berthing-schedule-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {            
            $('#tbl-berthing-schedule')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-berthing-schedule').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

