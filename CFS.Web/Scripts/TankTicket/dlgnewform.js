﻿FDM.View.tDlgNewForm = 
{
    initialize: function(){
        var activeCount;
    },
    events: {
        "click .dlg-tool-cmd-save": "onOkClick",
        "click .dlg-tool-cmd-cancel": "onCancelClick",
        "change #ddl-tank-name": "onChangeTankName",
    },
    render: function(){
        this.$el.jqm({
            modal: true
        });
        
        this.$el.jqDrag('.dlg-title-block');
        var activeCount = 0;
        this.validator = this.$el.validate({onsubmit:false});
    },
    clear:function()
    {
        //digunakan untuk clear seluruh element
        //C.clearField(this.$el);
        $('#ddl-tank-name').val('');
        var strStatus = '<option value="">--</option>';
        var strType = '<option value="">--</option>';
        $('#ddl-operation-type').html(strType);
        $('#ddl-operation-status').html(strStatus);
        this.validator.resetForm();
    },
    show: function(en){
        this.$el[en ? 'jqmShow' : 'jqmHide']();
        if(en){
            activeCount = 0;
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'Dashboard/GetTankLatestOperation',
                contentType: "application/json; charset=utf-8",
                success: function (arg) {
                    var max = arg.records.length;
                    if (arg.success && max > 0) {
                        var strTank = "Tank In Operation : <br>" +
                                      "<table class='table table-condensed table-hover'><tr class='info'><td>Tank</td><td>Current Operation</td><td>Liquid Level (mm)</td><td>Is Active ?</td></tr>";
                        for (i = 0; i < max; i++) {
                            strTank += "<tr><td>"+ arg.records[i].TankName +"</td><td>"+ arg.records[i].OperationType + " "+arg.records[i].OperationStatus +"</td><td>"+ arg.records[i].LiquidLevel +"</td><td>"+ arg.records[i].IsActiveText +"</td></tr>";  
                            if(arg.records[i].IsActive){
                                activeCount ++;
                            }
                        }
                        strTank += "</table>";
                        $("#tank-in-operation").html(strTank);
                        //count max 
                    }
                }
            });
        } // end if(en)
    },

    onCancelClick: function(ev){
        $('#dlg-new-form').css({"top":"5%", "left":"30%"}); //reset to original position
        this.trigger('DlgNewForm.Cancel', {});
        this.show(false);
    },
    onOkClick: function(ev){
        if(!this.validator.form())
            return;
        var val = this.$el.formHash();
        this.trigger('DlgNewForm.Selected', val);
        this.show(false);
    },
    prepareForUpdate: function(/*model*/arg){
        this.currentMode = 'update';
        this.currentRecord = arg;
        this.dataBind(arg.attributes);
    },
    dataBind: function(val)
    {
        this.$el.formHash(val);
    },
    prepareForInsert:function()
    {
        this.clear();
        this.currentMode = 'insert';
        this.currentRecord = null;
    },
    onChangeTankName: function() {
        var $this = this;
        var val = this.$el.formHash();
        //clear field dibawah tank name
        $('#ddl-operation-type').html('');
        $('#ddl-operation-status').html('');
        //cari tank ticket terakhir, jika terakhir close, bisa pilih mode yg lain, jika open hanya operasi nya & close
        if(val.TankName != '')
        {
            Msg.show('Loading...');
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'TankTicket/GetLatestTankTicket',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(val),
                success: function(arg) {
                    $('#ddl-operation-type').html('');
                    $('#ddl-operation-status').html('');
                    if(arg.success)
                    {
                        //cek status tank ticket terakhir, open / close?
                        if(arg.record[0] != null)
                        {
                            Msg.hide();
                            if(arg.record[0].OperationStatus == 'Open')
                            {
                                //buat close
                                var strType = '<option value="'+ arg.record[0].OperationType +'">'+ arg.record[0].OperationType +'</option>';
                                var strStatus = '<option value="Close">Close</option>';
                                $('#ddl-operation-type').html(strType);
                                $('#ddl-operation-status').html(strStatus);
                            }
                            else
                            {
                                var strStatus = '<option value="Open">Open</option>';
                                var strType = ['<option value="Intertank">Intertank</option>'];
                                strType.push('<option value="Receiving">Receiving</option>');
                                strType.push('<option value="Sales">Sales</option>');
                                if(activeCount == 1){
                                    strType.push('<option value="Receiving Sales">Receiving Sales</option>');
                                }
                                $('#ddl-operation-type').html(strType);
                                $('#ddl-operation-status').html(strStatus);
                            }
                        }
                        else
                        {
                            Msg.hide();
                            //jika latest tank ticket null, force create receiving open 
                            var strStatus = '<option value="Open">Open</option>';
                            var strType = '<option value="Receiving">Receiving</option>';
                            $('#ddl-operation-type').html(strType);
                            $('#ddl-operation-status').html(strStatus);
                        }
                    }
                }
            });
        }
        else
        {
            var strStatus = '<option value="">--</option>';
            var strType = '<option value="">--</option>';
            $('#ddl-operation-type').html(strType);
            $('#ddl-operation-status').html(strStatus);
        }
    }
};
FDM.View.DlgNewForm = Backbone.View.extend(FDM.View.tDlgNewForm);