// cinovasi.foundation.js
/*jslint passfail: false, white: false, onevar: false, browser: true, nomen: false*/

/*global jQuery: false, $j: true, Cinovasi: true, C: true, window: false,
    alert: false, console: false, Class: false, jsonPath: false, PersistenceApi: false */
(function () {
if(typeof Cinovasi !== 'undefined'){ return; }

$j = jQuery;
C = {};
Cinovasi = C;

C.Util =
{
    makeStatic : function(container)
    {
        var getType = function (el)
        {
            var t = el.type;
            switch( t )
            {
                case "select": case "select-one": case "select-multiple":t = "select";  break;
                case "text": case "textarea": case "password":t = "text";               break;
                case "checkbox": case "radio": t = t;                                   break;
            }
            return t;
        };

        var ctl = $j(container).find('[name]');
        var processed = {};
        var elementValue;
        var fieldElementDiv;

        function radioHandler()
        {
            $j(this).hide().attr('freeze', '1');
            $j(this).parent().hide().attr('freeze', '1');
        }

        function checkboxHandler()
        {
            $j(this).hide().attr('freeze', '1');
            $j(this).parent().hide().attr('freeze', '1');
        }

        function valueHandler()
        {
            elementValue += $j(this).parent().text() + ', ';
        }
        
        $j('.ui-datepicker-trigger', container).hide();

        for(var i = 0; i < ctl.length; i+=1)
        {
            try
            {
                var el = ctl[i];
                var n = el.name;
                var elid = '#' + el.id;
                if( processed[n]) {continue;}

                var itsType = getType(el);
                if(itsType === "text")
                {
                    $j(elid).hide().attr('freeze', '1');
                    elementValue = $j(elid).val().replace('\n', '<br />');
                    fieldElementDiv = $j(elid).parent();
                    $j('.fieldValue', fieldElementDiv).html(elementValue).show().attr('unfreeze', '1');
                }
                else if (itsType === "select")
                {
                    // handle chosenized select
                    if($j(elid).hasClass('chzn-done'))
                    {
                        var id = elid.replace(/-/g,'_') + '_chzn';
                        $j(id).hide();
                    }
                    
                    $j(elid).hide().attr('freeze', '1');
                    elementValue = $j(elid).find('option:selected').text();
                    fieldElementDiv = $j(elid).parent();
                    $j('.fieldValue', fieldElementDiv).text(elementValue).show().attr('unfreeze', '1');
                }
                else if (itsType === "checkbox")
                {
                    $j(container).find('[type="checkbox"][name="' + n + '"]')
                    .each(checkboxHandler);
                    elementValue = '';
                    $j(container).find('[type="checkbox"][name="' + n + '"]:checked')
                    .each(valueHandler);
                    fieldElementDiv = $j(elid).parent().parent();

                    $j('.fieldValue', fieldElementDiv).text(elementValue.slice(0,-2)).show().attr('unfreeze', '1');


                }
                else if (itsType === "radio")
                {
                    $j(container).find('[type="radio"][name="' + n + '"]')
                    .each(radioHandler);

                    elementValue = '';
                    $j(container).find('[type="radio"][name="' + n + '"]:checked')
                    .each(valueHandler);
                    fieldElementDiv = $j(elid).parent().parent();
                    $j('.fieldValue', fieldElementDiv).text(elementValue.slice(0,-2)).show().attr('unfreeze', '1');
                }

                processed[n] = true;
            }
            catch(err)
            {
                C.Util.log(err+ ' id=' + elid);
            }
        }
    },

    makeUnStatic : function(container)
    {
        $j('.ui-datepicker-trigger', container).show();
        $j('[unfreeze="1"]',container).each(function()
        {
            $j(this).hide().text('').removeAttr('unfreeze');
        });

        $j('[freeze="1"]',container).each(function()
        {
            $j(this).show().removeAttr('freeze');
        });
        
        $j(container).find('select').each(function()
        {
            var $el = $j(this);
            if($el.hasClass('chzn-done'))
            {
                var id = $el.attr('id');
                if(id && id.length)
                {
                    $j("#"+id).trigger("liszt:updated");            
                    var cid = '#'+id.replace(/-/g,'_')+'_chzn';
                    $j(cid).show();
                    $el.hide();
                }
            }
        });
    },

    clearField : function(container)
    {
        var getType = function (el)
        {
            var t = el.type;
            switch( t )
            {
                case "select": case "select-one": case "select-multiple":t = "select";    break;
                case "text": case "textarea": case "password": case "hidden": t = "text"; break;
                case "checkbox": case "radio": t = t;                                     break;
            }
            return t;
        };

        var ctl = $j(container).find('[name]');

        for(var i = 0; i < ctl.length; i+=1)
        {
            var el = ctl[i];
            var n = el.name;
            var $el = $j(el);

            var itsType = getType(el);
            if(itsType === "text")
            {
                $el.val('');
            }
            else if (itsType === "select")
            {
                $el.val('');
            }
            else if (itsType === "checkbox")
            {
                $el[0].checked = '';
            }
            else if (itsType === "radio")
            {
                $el[0].checked = '';
            }
        }
    },

    getQueryString : function()
    {
        var searchArray = window.location.search.substr(1).split('&');
        var q = {};
        for(var i = 0, ii = searchArray.length; i < ii; i+=1)
        {
            var item = searchArray[i].split('=');
            try
            {
                q[item[0]] = item[1];
            }
            catch(exc)
            {
                q[item[0]] = '';
            }
        }
        return q;
    },
    newId:function()
    {
        if(typeof C.gen === 'undefined') {C.gen = 0;}
        return 'gen-' + (C.gen = C.get + 1).toString();
    },
    log:function(msg)
    {
        if(window.console && window.console.log){ console.log(msg);}
    },
    logError:function(msg)
    {
        if(window.console && window.console.error){ console.error(msg);}
    }
};

C.makeStatic = C.Util.makeStatic;
C.makeUnStatic = C.Util.makeUnStatic;
C.clearField = C.Util.clearField;
C.getQueryString = C.Util.getQueryString;

//copied from extjs
C.namespace = function()
{
    var o, d;
    $j.each(arguments, function(idx, v) {
        d = v.split(".");
        o = window[d[0]] = window[d[0]] || {};
        $j.each(d.slice(1), function(idx, v2){
            o = o[v2] = o[v2] || {};
        });
    });
    return o;
};

C.ObjectManager = function()
{
    this.cache = {};
    $j(window).bind('unload.objmanager',{sender:this}, this.cleanUp);
};

C.ObjectManager.prototype =
{
    cleanUp:function(ev)
    {
        var $this = ev.data.sender;
        $j(window).unbind('unload.objmanager');
        for(var key in $this.cache)
        {
            if($this.cache.hasOwnProperty(key))
            {
                try
                {
                    var obj = $this.cache[key];
                    obj.fini();
                    $this.cache[key] = null;
                }
                catch(ex){}
            }
        }
    },
    addObject: function(key, object)
    {
        this.cache[key] = object; //overwritten
    },
    getObject:function(key)
    {
        return this.cache[key]; //will return undefined
    }
};

C.SObjMgr = new C.ObjectManager();


C.FileDescriptor = function(name, desc, ticket)
{
    this.filename = name || '';
    this.description = desc || '';
    this.url = '';
    this.ticket = ticket || '';
    this.docId = 0;
};

C.RecordDescriptor = function(/*$j selector*/ container)
{
    this.value = {};
    this.details = [];
    this.domContainer = container;
};

C.RecordDescriptor.prototype =
{
    toJSON : function()
    {
        return JSON.stringify(this);
    },

    clear : function()
    {
        this.value = {};
        this.details = [];
    },

    fieldHash : function(id, inHash)
    {
        var bGetHash = (arguments.length === 1);

        // create a hash to return
        var stHash = {};

        // get all the form elements
        var n, stProcessed = {};

        $j(id + ' *[name]').each(function()
        {
                n = this.name;

                // if the element doesn't have a name, then skip it
                if( !n || stProcessed[n] ) { return; }

                // create a jquery object to the current named form elements
                //var jel = $j(id + ' ' + this.tagName.toLowerCase() + "[name='"+n+"']");
                var jel = $j(id + ' ' + /*this.tagName.toLowerCase() +*/ "[name='"+n+"']");

                // if we're getting the values, get them now

                try
                {
                    if( bGetHash )
                    {
                        stHash[n] = jel.getValue();
                    }
                    else if( typeof inHash[n] !== "undefined" )
                    {
                        jel.setValue(inHash[n]);
                    }
                }catch(ex){}

                stProcessed[n] = true;
        });

        // if getting a hash map return it, otherwise return the $j object
        return bGetHash ? stHash : null;
    },


    syncFromUI : function()
    {
        if(this.domContainer.length === 0) {return false;}
        if($j(this.domContainer).length === 0) {return false;}
        this.value = this.fieldHash(this.domContainer);
    },

    syncToUI : function()
    {
        this.fieldHash(this.domContainer, this.value);
    },

    set : function(/* another instance */ rhs)
    {
        this.domContainer = rhs.domContainer || this.domContainer;
        this.value = rhs.value || {};
        this.details = rhs.details || [];
    }
};


C.AjaxQueue = function()
{
    this.ajaxManager = $j.manageAjax.create(Math.random(),
    {
        queue: true,
        cacheResponse: false
    });
};

C.AjaxQueue.prototype =
{
    fini : function()
    {
        this.ajaxManager._unload();
        this.ajaxManager = null;
    },
    rpc:function(call)
    {
        if (!call.callback)
        {
            throw new Error('Synchronous calls not supported.');
        }

        var packet = JSON.stringify(call.request).replace(/&/g,'%26');
        var $this = this;
        this.ajaxManager.add(
        {
            type: "POST",
            url: call.url,
            data: packet,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-JSON-RPC", call.request.method);
            },
            success: function(s) {
                //jquery 1.4.2, krn header 'application/json',
                //response udh automatically parsed
                //call.callback(JSON.parse(s));
                call.callback(s);
            },
            error:function(x)
            {
                var ev = {data:x};
            }
        });
    }
};

C.SAjaxQueue = new C.AjaxQueue();
C.SObjMgr.addObject('ajaxQueue',C.SAjaxQueue);
C.channelHelper = function(arg)
{
    C.SAjaxQueue.rpc(arg);
};


C.Message = function(moduleId, action)
{
    this.scenario = [];
    this.messageBody = '{}';
    this.moduleId = moduleId || 0;
    this.action = action || '';
};

//2012-05-22T00:00:00
C.parseIsoDateTime = function(input)
{
    if (!input)
    return new Date();

    if(input.length < 19)
        return new Date();

    var y = parseInt(input.substr(0,4),10),
        m = parseInt(input.substr(5,2),10) - 1,
        d = parseInt(input.substr(8,2),10),
        h = parseInt(input.substr(11,2),10),
        mi = parseInt(input.substr(14,2),10),
        s = parseInt(input.substr(17,2),10);

    // bulletproofing attempt
    if(y < 100)
    { 
        y += 2000;
        if(y > 2030) y -= 100;
    }
        
    var r = null;
    if(input[input.length-1] == 'Z')
        r = new Date(Date.UTC(y,m,d,h,mi,s));
    else
        r = new Date(y,m,d,h,mi,s);
    r.setFullYear(y);
    return r;
};

C.autoTypeDateValidator = function(val, el) 
{
    var $el = $(el);
    var $hdn = $('#' + $(el).data('related-field'));
	
    if(!val.length)
    {
        $hdn.val('');
        return true;
    }
	var months = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var toParse = val.split('/');
    if(toParse.length)
    {
        if(toParse.length < 2)
        {
            $hdn.val('');
            return false;
        }

        if(toParse.length == 2)
        {
            toParse.push((new Date()).getFullYear());
        }
        
        var month = parseInt(toParse[0],10);
        if(isNaN(month))
        {
            month = _.indexOf(months, toParse[0]);
        }
        
        var day = parseInt(toParse[1],10);
        var year = parseInt(toParse[2],10);
        var ok = true;
        if(typeof(day) == 'undefined') return false;
        if(month < 1 || month > 12) ok = false;
        if(day < 1 || day > 31) ok = false;
        if(year < 1 ) ok = false;
        if(year < 100)
        { 
            year += 2000;
            if(year > 2030) year -= 100;
        }

        if(ok)
        {
            var date = new Date(year, month-1, day);
            $el.val(date.format('mmm/d/yyyy'));
            $hdn.val(date.format('isoDateTime'));
            $el.trigger('change');
            return true;
        }
        else
        {
            $hdn.val('');
            return false;
        }
    }
    else
    {
        $hdn.val('');
        return false;
    }
};

C.dateInputFocusHandler = function(ev)
{
    // parse as expected format (mon/d/yyyy)
    // indirect through remote 
    var $el = $(ev.target);
    var $hdn = $('#' + $el.data('related-field'));
    var val = $hdn.val();
    
    // convert to date
    if(val)
    {
        var dt = C.parseIsoDateTime(val);
        var convert = dt.format('m/d/yyyy');
        $el.val(convert);
    }
    $el.select();
};

C.dateInputBlankHandler = function(ev)
{
    if($(ev.target).val() == '')
    {
        $('#' + $(ev.target).data('related-field')).val('');
    }
};


}()); //end of inline function


