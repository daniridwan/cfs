﻿FDM.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.customerColl = new FDM.Model.CustomerList();

        // create views
        this.tabView = new FDM.View.TabView();
        this.gridView = new FDM.View.GridView();
        this.formView = new FDM.View.FormView({ el: $('#pnl-form') });
        this.filterView = new FDM.View.FilterView({ el: $('#pnl-search') });
        this.dlgDownloadCustomer = new FDM.View.DlgDownloadCustomer({ el: $('#dlg-download-customer') });

        // setup event routes
        this.gridView.bind('GridView.PageRequest', this.onPageRequested, this);
        this.customerColl.bind('Customer.PageReady', this.onPageReady, this);
        this.formView.bind('FormView.Created', this.onCreateNew, this);
        this.formView.bind('FormView.Updated', this.onUpdated, this);
        this.filterView.bind('FilterView.Changed', this.onFilterChanged, this);
        this.filterView.bind('FilterView.Export', this.onExportClick, this);
        this.filterView.bind('FilterView.Download', this.onDownloadClick, this);

    },
    routes: {
        "": "grid",
        "pnl-grid": "grid",
        "view/:id": "view",
        "new": "openForm",
    },
    start: function () {
        this.tabView.render();
        this.filterView.render();
        this.gridView.render();
        this.formView.render();
        this.dlgDownloadCustomer.render();
        Backbone.history.start();
    },
    view: function (id) {
        var user = this.customerColl.get(id);
        if (user) {
            this.formView.prepareForUpdate(user);
            this.formView.makeReadOnly();
            this.tabView.tabObj.click(1);
            $(window).scrollTop($('#pnl-form').offset().top);
        }
        else
            this.navigate('');
    },
    onCreateNew: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    grid: function () {
        this.tabView.tabObj.click(0);
    },
    openForm: function () {
        this.formView.prepareForInsert();
        this.formView.clearAuditTrail();
        this.formView.validator.resetForm();
        this.tabView.tabObj.click(1);
    },
    onUpdated: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    onFilterChanged: function (filter) {
        this.gridView.setFilter(filter.param);
        this.gridView.refresh();
    },
    onPageRequested: function (arg) {
        Msg.show('Loading...');
        this.customerColl.fetchPage(arg);
    },
    onPageReady: function (arg) {
        Msg.hide();
        this.gridView.dataBind(arg);
    },
    onExportClick: function (arg) {
        var url = this.customerColl.url();
        url = url.replace('Customer/Page','HttpHandlers/CustomerExporter.ashx');
        window.open(url);
    },
    onDownloadClick: function (arg){
        this.dlgDownloadCustomer.clear();
        this.dlgDownloadCustomer.show(true);
    },
});

$(document).ready(function()
{
    FDM.AppRouter = new FDM.Router.AppRouter();
    FDM.AppRouter.start();
});
