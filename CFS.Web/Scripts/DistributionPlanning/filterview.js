﻿FDM.View.FilterView = Backbone.View.extend({
    events: {
        "change select": "onDropDownChanged",
        "keypress input[type='text']": "onTextKeypress",
        "click .cmd-reset-filter": "onFilterReset",
        "click .cmd-refresh": "onRefresh",
        "click .cmd-export": "onExport"
    },
    render: function () {
        // don't forget to call ancestor 's render()
        C.FormView.prototype.render.call(this);
        var date = new Date();
        date.setDate(date.getDate() + 7);
        var date = { altField:"#hdn-date", altFormat: "yy-mm-ddT00:00:00",
            dateFormat: 'dd-MM-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast',
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true, maxDate: date
        };

        $(".datepicker").datepicker({
            beforeShow: function (input, inst) {
            setTimeout(function () {
                inst.dpDiv.css({
                    top: $(".datepicker").offset().top + 35,
                    left: $(".datepicker").offset().left
                });
            }, 0);
        }
        });

        $('#flt-date').datepicker(_.extend(date, {}));
    },
    onDropDownChanged: function (ev) {
        this.trigger('FilterView.Changed ', { param: this.getSearchParam() });
        return false;
    },
    onTextKeypress: function (ev) {
        if (ev.keyCode === 13) {
            this.trigger('FilterView.Changed', { param: this.getSearchParam() });
            return false;
        }
    },
    onFilterReset: function (ev) {
        C.clearField(this.$el);
        this.$('select').trigger('liszt:updated');
        this.trigger('FilterView.Changed ', { param: this.getSearchParam() });
        return false;
    },
    onRefresh: function (ev) {
        this.trigger('FilterView.Changed ', { param: this.getSearchParam() });
        return false;
    },
    onExport: function (ev) {
        this.trigger('FilterView.Export', { param: this.getSearchParam() });
        return false;
    },
    /** General Function **/
    getSearchParam: function () {

        var value = this.$('form').formHash();
        for (var key in value) {
            value[key] = encodeURIComponent(value[key]);
        }
        return value;
    }
});