﻿FDM.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;
        this.gridObj = $('#tbl-distribution-planning').jqGrid(
        {
            pager: 'tbl-distribution-planning-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 20, 25, 30, 35, 40, 45, 50, 55, 60],
            colNames: ['No', '', 'Date', 'Planning (Kg)'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 18, sortable: false, hidden: false },
                { name: 'DateText', index: 'Date', width: 100, sortable: true },
                { name: 'PlanningText', index: 'Planning', width: 120, sortable: true },
		],
            viewrecords: true,
            sortorder: "desc",
            sortname: "Date",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-distribution-planning-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {            
            $('#tbl-distribution-planning')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-distribution-planning').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

