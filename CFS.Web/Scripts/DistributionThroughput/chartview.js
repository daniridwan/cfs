﻿NetLPG.View.ChartView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
    },
    render: function () {
        this.refresh();
    },
    refresh:function(fltTimestampFrom, fltTimestampTo)
    {
    	this.renderDailyThroughputChart(fltTimestampFrom, fltTimestampTo);
    },
    renderDailyThroughputChart: function (fltTimestampFrom, fltTimestampTo) {
        var dataSet = this.prepareData();   
        var i = 0;
        var config = {
            type: 'line',
            data: {
                labels: dataSet.dateLabel,
                datasets: [{
                    label: 'Throughput',
			        borderColor: window.chartColors.red,
                    fill: false,
                    backgroundColor: window.chartColors.red,
                    data: dataSet.throughput
				},{
                    label: 'Planning',
			        borderColor: window.chartColors.blue,
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    data: dataSet.planning,
                }]
			},
            options: {
				responsive: true,
                bezierCurve: true,
				title: {
					display: true,
					text: ''
				},
                legend: {
                    display: true,
                },
				tooltips: {
					mode: 'index',
					intersect: false,
                    callbacks: {
                        label: function(tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label;
                        var val = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        return val + ' MT';
                        }
                    }
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Throughput Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Metric Ton (MT)'
						}
					}]
				}
			}
        }
        //call chart
        $("canvas#daily-throughput-chart").remove();
        $("div.chart-div").append('<canvas id="daily-throughput-chart" class="animated fadeIn"></canvas>');
        var ctx = document.getElementById('daily-throughput-chart').getContext('2d');
        ctx.canvas.height = 80;
        var chart = new Chart(ctx, config);
    },
    prepareData: function(){
        var toReturn = { dateLabel: [], throughput: [], planning: [] };
        for(var i = 0, ii = NetLPG.Model.TheDistributionThroughputList.totalRecords; i < ii; i++)
    	{
            var o = NetLPG.Model.TheDistributionThroughputList.at(i).attributes;
            toReturn.dateLabel.push(this.date2text(o.ThroughputDate, 'd-mmm-yyyy'));
            toReturn.throughput.push(o.Throughput/1000);
            toReturn.planning.push(o.Planning/1000);
        }
        return toReturn
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'dd-mm-yyyy';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
});

