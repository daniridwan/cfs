﻿CFS.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.truckTrackingColl = new CFS.Model.TruckTrackingList();
        // create views
        this.gridView = new CFS.View.GridView();
        // setup event routes
        this.gridView.bind('GridView.PageRequest', this.onPageRequested, this);
        this.truckTrackingColl.bind('TruckTracking.PageReady', this.onPageReady, this);
        //timer for auto refresh
        this.timerId = 0;
        this.setupTimer(true);
    },
    routes: {
        "speak/:regPlate": "voice"
    },
    voice: function (number) {
        //responsiveVoice.speak("Truk dengan nomor antrian " + regPlate + " silahkan masuk ke loket", "Indonesian Female");
        var $this = this;
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'Dashboard/Speak?number=' + number,
            contentType: "application/json; charset=utf-8",
            success: function (arg) {
                var sounds = new Array();
                var counter = -1;
                for (i = 0; i <= arg.records.length; i++) {
                    sounds.push(new Audio(g_baseUrl + 'Content/Sound/' + arg.records[i]));
                }
                playSnd();

                function playSnd() {
                    counter++;
                    if (counter == sounds.length) return;
                    sounds[counter].addEventListener('ended', playSnd);
                    sounds[counter].play();
                }
            }
        });
        window.location.hash = '#';
    },
    setupTimer: function (arg) {
        if (arg) {
            var $this = this;
            this.timerId = setInterval(function () {
                $this.onTimer();
            }, 60000); // in miliseconds, 1 sec = 1000 ms
        }
    },
    onTimer: function () {
        //all function to auto refresh;
        //this.onRenderTankInOperation();
        //this.onRenderPlanningVsThroughput();
        //this.onRenderDailyThroughput();
        //this.onRenderStockGauge();
        //this.onRenderNotification();
        this.gridView.refresh();
    },
    start: function () {
        //this.onRenderTankInOperation();
        //this.onRenderPlanningVsThroughput();
        //this.onRenderDailyThroughput();
        //this.onRenderStockGauge();
        //this.onRenderNotification();
        this.gridView.render();
        Backbone.history.start();
    },
    onPageRequested: function (arg) {
        Msg.show('Loading...');
        this.truckTrackingColl.fetchPage(arg);
    },
    onPageReady: function (arg) {
        Msg.hide();
        this.gridView.dataBind(arg);
    },
    onRenderNotification: function (arg){
      $.ajax({
            type: "POST",
            url: g_baseUrl + 'Dashboard/GetLatestNotification',
            contentType: "application/json; charset=utf-8",
            success: function (arg) {
                var max = arg.records.length;
                if (arg.success && max > 0) {
                    for (i = 0; i < max; i++) {
                        //remove all
                        $("#list-notification").html('');
                    }
                    for (i = 0; i < max; i++) {
                        //53 = max length
                        var trimmedString = arg.records[i].Text.length > 53 ? 
                                            arg.records[i].Text.substring(0, 53 - 3) + "..." : 
                                            arg.records[i].Text;
                        $("#list-notification").append('<li class="list-group-item"><span class="'+ arg.records[i].Icon +'"></span> <b style="color:#0092DD;font-family: Arial;">'+ arg.records[i].Title +'</b><br>'+ trimmedString +'<br><font size="0.5em">'+ C.parseIsoDateTime(arg.records[i].Timestamp) +'</font></li>');
                    }
                }
                else if (max == 0){
                    $("#list-notification").html('<li class="list-group-item"><span class="glyphicon glyphicon-remove-sign"></span> No New Notification</li>');
                }
                else {
                    $("#list-notification").html('GetLatestNotification failed / not available, please contact Administrator');
                }
            }
        });  
    },
    onRenderTankInOperation: function () {
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'Dashboard/GetTankLatestOperation',
            contentType: "application/json; charset=utf-8",
            success: function (arg) {
                var max = arg.records.length;
                if (arg.success && max > 0) {
                    for (i = 0; i < max; i++) {
                        //remove all
                        $('#section-slider-tank-operation').slick('removeSlide', null, null, true);
                    }
                    for (i = 0; i < max; i++) {
                        var strTank = '<div class="row"> <div class="col-xs-12 col-xs-offset-4"> ' +
                                      '<img src="' + g_baseUrl + 'Images/tank-bg.png" alt="LPG Tank" class="img-rounded img-responsive" width="170px" /> ' +
                                      '</div> <div class="col-xs-12 col-xs-offset-2" style="text-align:left" data-sizes="50vw"><font face = "Arial"> ' +
                                      '<table> <tr> <td rowspan="3" style="padding-right:10px;"><b><font size=18px;">' + arg.records[i].TankName + '</font></b></td>' +
                                      '<td><b>Operation Type</b>   : ' + arg.records[i].OperationType + '</td></tr>' +
                                      '<tr><td><b>Operation Status</b> : ' + arg.records[i].OperationStatus + '</td></tr>' +
                                      '<tr><td><b>Product</b> : ' + arg.records[i].ProductName + '</td></tr>' +
                                      '</table>' +
                                      '</font></div> </div>';
                        $('#section-slider-tank-operation').slick('slickAdd', strTank);
                    }
                }
                else {
                    $("#section-slider-tank-operation").html('GetTankLatestOperation failed / not available, please contact Administrator');
                }
            }
        });
    }, //close onRenderTankInOperation
    onRenderPlanningVsThroughput: function () {
        $('[data-toggle="tooltip"]').tooltip(); 
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'Dashboard/GetPlanningVsThroughput',
            contentType: "application/json; charset=utf-8",
            success: function (arg) {
                var max = arg.records.length;
                if (arg.success && max > 0) {
                    if (arg.records[0].Percentage < 30){
                        var color = 'progress-bar-danger'; //red
                    }
                    else if (arg.records[0].Percentage >= 30 && arg.records[0].Percentage < 70)
                    {
                        var color = 'progress-bar-warning'; //yellow
                    }
                    else{
                        var color = 'progress-bar-success'; //green
                    }

                    $('.odo-planning').html(0);
                    $('.odo-throughput').html(0);
                    var planning = document.querySelector('.odo-planning');
                    var throughput = document.querySelector('.odo-throughput');
                    pl = new Odometer({
                        el: planning,
                        // Any option (other than auto and selector) can be passed in here
                        format: '(,ddd).dd',
                        theme: 'default',
                        duration: 1000,
                        animation: 'count'
                    });
                    th = new Odometer({
                        el: throughput,
                        // Any option (other than auto and selector) can be passed in here
                        format: '(,ddd).dd',
                        theme: 'default',
                        duration: 1000,
                        animation: 'count'
                    });
                    $('.odo-planning').html(arg.records[0].Planning);
                    $('.odo-throughput').html(arg.records[0].Throughput);

                    var strProgress = '<div class="progress-bar '+ color +' progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ' + arg.records[0].Percentage + '%" data-placement="bottom" data-toggle="tooltip-left" title="' + arg.records[0].Percentage + '%">' +
                                      '<span class="sr-only">' + arg.records[0].Percentage + '% Complete</span><div class="info-text">' + arg.records[0].Percentage + '%</div>' +
                                      '</div>';
                    $(".progress").html(strProgress);
                }
                else {
                    $("#body-planning-vs-throughput").html('<br><br><br>GetPlanningVsThroughput failed / not available, please contact Administrator.');
                }
            }
        });
    }, //close onRenderPlanningVsThroughput
    onRenderDailyThroughput: function () {
        $this = this;
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'Dashboard/GetDailyThroughput',
            contentType: "application/json; charset=utf-8",
            success: function (arg) {
                var max = arg.records.length;
                if (arg.success && max > 0) {
                    $this.renderDailyThroughputChart(arg);
                }
                else{
                    console.log(arg);
                    $("div.chart-div").html('<br>Data not available, please contact Administrator.');
                }
            }
        });
    }, //close onRenderPlanningVsThroughput
    renderDailyThroughputChart: function (arg) {
        var max = arg.records.length;
        console.log(arg.records);
        var dateLabel = [];
        var throughput = [];
        var i = 0;
        for (i = 0; i < max; i++) {
            if (arg.records[i] != null) {
                dateLabel.push(this.date2text(arg.records[i].ThroughputDate, 'd-mmm-yyyy'));
                throughput.push(arg.records[i].Throughput/1000);
            }
        }
        var config = {
            type: 'bar',
            data: {
                    labels: dateLabel,
                    datasets: [{
                    label: 'Throughput',
			        borderColor: window.chartColors.red,
                    data: throughput,
                    fill: false,
                    backgroundColor: window.chartColors.red
				}]
			},
            options: {
				responsive: true,
                bezierCurve: true,
				title: {
					display: true,
					text: ''
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Throughput ( KL )'
						},
                        ticks:{
                            beginAtZero: true
                        }
					}]
				}
			}
        }
        //call chart
        $("canvas#daily-throughput-chart").remove();
        $("div.chart-div").append('<canvas id="daily-throughput-chart" class="animated fadeIn"></canvas>');
        var ctx = document.getElementById('daily-throughput-chart').getContext('2d');
        ctx.canvas.height = 100;
        var chart = new Chart(ctx, config);
    },
    onRenderStockGauge: function () {
        var objectiveThruput = 0;
        var dayofmonth = new Date();
        var month = dayofmonth.getMonth();
        var year = dayofmonth.getFullYear();
        var d = new Date(year,month+1,0);
        var numberofday = d.getDate();
        $this = this;
        //get current month objective throughput
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'Dashboard/GetCurrentObjectiveThroughput',
            contentType: "application/json; charset=utf-8",
            success: function (arg) {
                var max = arg.records.length;
                if (arg.success && max > 0) {
                    this.objectiveThruput = arg.records[0].ObjectiveThroughput;
                    //daily average
                    this.objectiveThruput = this.objectiveThruput / numberofday;
                    $this.getTankLiveData(this.objectiveThruput);
                }
                else{
                    $("#body-stock-inventory").html('Objective Throughput for current month failed / not available, please contact Administrator.');
                }
            }
        });
    },
    getTankLiveData: function(arg) {
        //arg = average throughput
        
        var averageThroughput = arg;
        var dayCoverageT5;
        var dayCoverageT6;
        $this = this;
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'Dashboard/GetTankLiveData',
            contentType: "application/json; charset=utf-8",
            success: function (arg) {
                var max = arg.records.length;
                if (arg.success && max > 0) {
                    for (i = 0; i < max; i++) {
                        if(arg.records[i].TankName == "T-5")
                        {
                            var dayCoverageT5 = arg.records[i].LiquidMass / averageThroughput;
                        }
                        if(arg.records[i].TankName == "T-6")
                        {
                            var dayCoverageT6 = arg.records[i].LiquidMass / averageThroughput;
                        }
                    }
                    $this.renderStockGaugeT5(dayCoverageT5);
                    $this.renderStockGaugeT6(dayCoverageT6);
                }
                else{
                    $("#body-stock-inventory").html('Failed to retrieve tank live data, please contact Administrator.');
                }
              }
        });
    },
    renderStockGaugeT5: function(arg) {
        var gaugeT5 = new RadialGauge({
            renderTo: 'circularGaugeContainerT5',
            width: 175,
            height: 175,
            units: 'Days',
            minValue: 0,
            maxValue: 30,
            title: 'T-5',
            majorTicks: ['0','5','10','15','20','25','30'],
            minorTicks: 5,
            highlights  : [
                { from : 0,  to : 5, color : 'rgba(200, 50, 50, .8)' },
                { from : 5,  to : 10, color : 'rgba(244, 252, 0, .8)' },
                { from : 10, to : 30, color : 'rgba(9, 244, 9, .8)' }
            ],
            valueInt: 1,
            valueDec: 2,
            colorPlate: "#fff",
            colorMajorTicks: "#000",
            colorMinorTicks: "#000",
            colorTitle: "#000",
            colorUnits: "#000",
            colorNumbers: "#000",
            valueBox: true,
            colorValueText: "#000",
            colorValueBoxRect: "#fff",
            colorValueBoxRectEnd: "#fff",
            colorValueBoxBackground: "#fff",
            colorValueBoxShadow: false,
            colorValueTextShadow: false,
            colorNeedleShadowUp: true,
            colorNeedleShadowDown: false,
            colorNeedle: "rgba(200, 50, 50, .75)",
            colorNeedleEnd: "rgba(200, 50, 50, .75)",
            colorNeedleCircleOuter: "rgba(200, 200, 200, 1)",
            colorNeedleCircleOuterEnd: "rgba(200, 200, 200, 1)",
            borderShadowWidth: 2,
            borders: true,
            borderInnerWidth: 0,
            borderMiddleWidth: 0,
            borderOuterWidth: 3,
            colorBorderOuter: "#fafafa",
            colorBorderOuterEnd: "#b5b5b5",
            needleType: "arrow",
            needleWidth: 2,
            needleCircleSize: 7,
            needleCircleOuter: true,
            needleCircleInner: false,
            animationDuration: 1500,
            animationRule: "dequint",
            fontNumbers: "Open Sans",
            fontTitle: "Open Sans",
            fontUnits: "Open Sans",
            fontValue: "Russo One",
            fontValueStyle: 'bold',
            fontNumbersSize: 20,
            fontNumbersStyle: 'bold',
            fontNumbersWeight: 'bold',
            fontTitleSize: 24,
            fontUnitsSize: 24,
            fontValueSize: 36,
            animatedValue: true
        });
        gaugeT5.draw();
        gaugeT5.value = arg;
    },
    renderStockGaugeT6: function(arg) {
        var gaugeT6 = new RadialGauge({
            renderTo: 'circularGaugeContainerT6',
            width: 175,
            height: 175,
            units: 'Days',
            minValue: 0,
            maxValue: 30,
            title: 'T-6',
            majorTicks: ['0','5','10','15','20','25','30'],
            minorTicks: 5,
            highlights  : [
                { from : 0,  to : 5, color : 'rgba(200, 50, 50, .8)' },
                { from : 5,  to : 10, color : 'rgba(244, 252, 0, .8)' },
                { from : 10, to : 30, color : 'rgba(9, 244, 9, .8)' }
            ],
            valueInt: 1,
            valueDec: 2,
            colorPlate: "#fff",
            colorMajorTicks: "#000",
            colorMinorTicks: "#000",
            colorTitle: "#000",
            colorUnits: "#000",
            colorNumbers: "#000",
            valueBox: true,
            colorValueText: "#000",
            colorValueBoxRect: "#fff",
            colorValueBoxRectEnd: "#fff",
            colorValueBoxBackground: "#fff",
            colorValueBoxShadow: false,
            colorValueTextShadow: false,
            colorNeedleShadowUp: true,
            colorNeedleShadowDown: false,
            colorNeedle: "rgba(200, 50, 50, .75)",
            colorNeedleEnd: "rgba(200, 50, 50, .75)",
            colorNeedleCircleOuter: "rgba(200, 200, 200, 1)",
            colorNeedleCircleOuterEnd: "rgba(200, 200, 200, 1)",
            borderShadowWidth: 2,
            borders: true,
            borderInnerWidth: 0,
            borderMiddleWidth: 0,
            borderOuterWidth: 3,
            colorBorderOuter: "#fafafa",
            colorBorderOuterEnd: "#b5b5b5",
            needleType: "arrow",
            needleWidth: 2,
            needleCircleSize: 7,
            needleCircleOuter: true,
            needleCircleInner: false,
            animationDuration: 1500,
            animationRule: "dequint",
            fontNumbers: "Open Sans",
            fontTitle: "Open Sans",
            fontUnits: "Open Sans",
            fontValue: "Russo One",
            fontValueStyle: 'bold',
            fontNumbersSize: 20,
            fontNumbersStyle: 'bold',
            fontNumbersWeight: 'bold',
            fontTitleSize: 24,
            fontUnitsSize: 24,
            fontValueSize: 36,
            animatedValue: true
        });
        gaugeT6.draw();
        gaugeT6.value = arg;
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'dd-mm-yyyy';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    }
});        //close app router


$(document).ready(function()
{
    CFS.AppRouter = new CFS.Router.AppRouter();
    CFS.AppRouter.start();
});
