﻿CFS.View.tDlgDeliveryOrderDetail = 
{
    initialize: function(){
    },
    events: {
        "click .dlg-tool-cmd-save": "onOkClick",
        "click .dlg-tool-cmd-cancel": "onCancelClick",
        "change #ddl-tank": "onChangeTankName",
    },
    render: function(){
        this.$el.jqm({
            modal: true
        });
        //
        this.$el.jqDrag('.dlg-title-block');
        this.validator = this.$el.validate({ onsubmit: false });
    },
    clear:function()
    {
        //digunakan untuk clear seluruh element
        C.clearField(this.$el);
        this.validator.resetForm();
    },
    show: function (en) {
        this.$el[en ? 'jqmShow' : 'jqmHide']();
    },

    onCancelClick: function(ev){
        this.show(false);
    },
    onOkClick: function(ev){
        // musti validate selection
        if(!this.validator.form())
            return;
        var val = this.$el.formHash();
        val.ProductName = $('#ddl-product').text();
        if (this.currentRecord) // update
        {
            this.currentRecord.set(val, { silent: true });
            this.trigger('updated', this.currentRecord);
            this.show(false);
        }
        else // insert
        {
            // di sini musti create AssetLog record, lalu push ke collection
            var c = new CFS.Model.DeliveryOrderDetail();
            c.set(val, { silent: true });
            //alert(c);
            this.trigger('created', c); //jump to trigger defined in formview.js
            this.show(false);
        }             
    },
    prepareForUpdate: function(/*model*/arg){
        this.currentMode = 'update';
        this.currentRecord = arg;
        $('#ddl-tank').html('<option value="">--</option>');
        var companyId = $('#hdn-company-id').val();
        var $this = this;
        var toSend =
        {
            companyId: companyId
        };
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'DeliveryOrder/GetTankByCompany',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(toSend),
            success: function (arg) {
                var max = arg.records.length;
                if (arg.success && max > 0) {
                    for (i = 0; i < max; i++) {
                        var strName = '<option value="' + arg.records[i].Name + '">' + arg.records[i].Name + '</option>';
                        $('#ddl-tank').append(strName);
                    }
                }
                $("#ddl-tank").val($this.currentRecord.attributes.TankName);
                $this.onChangeTankName($this.currentRecord.attributes.TankName);
            }
        });
        this.dataBind(arg.attributes);
    },
    dataBind: function(val)
    {
        this.$el.formHash(val);
    },
    onChangeTankName: function () {
        var toSend =
        {
            tankName: $("#ddl-tank").val()
        };
        $('#ddl-product').html('');
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'DeliveryOrder/GetProductByTank',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(toSend),
            success: function (arg) {
                var strName = '<option value="' + arg.records.ProductId + '">' + arg.records.ProductName + '</option>';
                $('#ddl-product').append(strName);
                $("#ddl-product").val(arg.records.ProductId);
            }
        });
    },
    prepareForInsert:function()
    {
        this.clear();
        this.currentMode = 'insert';
        this.currentRecord = null;
    }
    };
CFS.View.DlgDeliveryOrderDetail = Backbone.View.extend(CFS.View.tDlgDeliveryOrderDetail);