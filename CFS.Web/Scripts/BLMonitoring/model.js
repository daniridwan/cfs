﻿CFS = { Model: {}, View: {}, Router: {} };
$.metadata.setType("attr", "validate");
//Vessel Document
CFS.Model.VesselDocument = Backbone.Model.extend(
{
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    defaults:
    {
        InsertedDate: null,
        LastUpdatedDate: null,
        Visible: 1
    },
    url: function () {
        return g_baseUrl + 'BLMonitoring/Save?id=' + (this.id ? this.id : '0');
    },
    idAttribute: 'VesselDocumentId',
    onChange: function () {
        var val = this.attributes;
        try {
            val.view = '<a href="#view/' + val.VesselDocumentId + '"><div class="ui-silk ui-silk-zoom" style="height:22px"></div></a>';
            val.SppbDateText = this.date2text(val.SppbDate, 'd-mmm-yyyy');
            val.PibDateText = this.date2text(val.PibDate, 'd-mmm-yyyy');
            val.ArrivalDateText = this.date2text(val.ArrivalDate, 'd-mmm-yyyy');
            val.BlQuantityText = this.formatDollar(val.BlQuantity);
            val.BlCostText = this.formatDollar(val.BlCost);
        }
        catch (er) {
            C.Util.log(er);
        }
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'd-mmm-yyyy';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
    formatDollar: function (num) {
        return (
                parseFloat(num)
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            )
    },
});

CFS.Model.VesselDocumentList = Backbone.Collection.extend(
{
    initialize: function () {
        this.on('reset', this.onResetOrChange, this);
    },
    postData: {}, //from jqgrid
    totalRecords: 0,
    pageIndex: 0,
    pageSize: 15,
    model: CFS.Model.VesselDocument,
    url: function () {
        var xurl = g_baseUrl + 'BLMonitoring/PageReceiving?pageIndex=' + this.pageIndex + '&pageSize=' + this.pageSize;
        var query = [];
        for (var key in this.postData) {
            if (this.postData.hasOwnProperty(key) && key.indexOf('flt') != -1) {
                query.push("&" + key + "=" + this.postData[key]);
            }
        }
        if (this.postData.sidx && this.postData.sidx.length) { query.push("&sidx=" + this.postData.sidx); }
        if (this.postData.sord && this.postData.sord.length) { query.push("&sord=" + this.postData.sord); }
        query.push('&t=' + (new Date()).valueOf());
        return xurl + query.join('');
    },
    parse: function (arg) {
        if (!arg.success) {
            alert(arg.message);
            return;
        }

        var pageSize = parseInt(this.pageSize, 10);
        var pageIndex = parseInt(this.pageIndex, 10);
        var id = (pageSize * pageIndex) + 1;
        for (var i = 0, ii = arg.records.length; i < ii; i++) {
            arg.records[i].No = id + i;
        }

        this.totalRecords = arg.totalRecords;
        return arg.records;
    },
    date2text: function (dt) {
        if (dt) { return C.parseIsoDateTime(dt).format('d-mmm-yyyy'); }
        else { return ''; }
    },
    fetchPage: function (postData) {
        this.pageIndex = postData.pageIndex;
        this.pageSize = postData.pageSize;
        this.postData = postData;
        this.fetch({ reset: true });
    },
    onResetOrChange: function () {
        var idx = this.pageIndex * this.pageSize + 1;
        for (var i = 0, ii = this.length; i < ii; i++) {
            this.at(i).onChange();
        }
        var toInject = {
            total: Math.ceil(this.totalRecords / this.pageSize),
            page: this.pageIndex + 1,
            records: this.totalRecords,
            rows: this.toJSON()
        };
        try {
            this.trigger('VesselDocument.PageReady', toInject);
        } catch (ex) { }
    }
});
//delivery
//Delivery Order Realization
CFS.Model.DeliveryOrderRealization = Backbone.Model.extend(
    {
        initialize: function () {
            this.on('change', this.onChange, this);
        },
        defaults:
        {
            InsertedDate: null,
            LastUpdatedDate: null,
            Visible: 1
        },
        url: function () {
            return g_baseUrl + 'BLMonitoring/Save?id=' + (this.id ? this.id : '0');
        },
        idAttribute: 'TruckLoadingId',
        onChange: function () {
            var val = this.attributes;
            try {
                val.view = '<a href="#view/' + val.PlanningId + '"><div class="ui-silk ui-silk-zoom" style="height:22px"></div></a>';
                val.GateOutTimestampText = this.date2text(val.GateOutTimestamp, 'd-mmm-yyyy HH:MM:ss');
                val.QuantityText = this.formatDollar(val.Quantity);
                val.UnitPriceText = this.formatDollar(val.UnitPrice);
            }
            catch (er) {
                C.Util.log(er);
            }
        },
        date2text: function (dt, fmt) {
            if (!fmt) fmt = 'd-mmm-yyyy';
            if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
            else { return ''; }
        },
        formatDollar: function (num) {
            return (
                parseFloat(num)
                    .toFixed(2) // always two decimal digits
                    .replace('.', ',') // replace decimal point character with ,
                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            )
        },
    });

CFS.Model.DeliveryOrderRealizationList = Backbone.Collection.extend(
    {
        initialize: function () {
            this.on('reset', this.onResetOrChange, this);
        },
        postData: {}, //from jqgrid
        totalRecords: 0,
        pageIndex: 0,
        pageSize: 15,
        model: CFS.Model.DeliveryOrderRealization,
        url: function () {
            var xurl = g_baseUrl + 'BLMonitoring/PageDelivery?pageIndex=' + this.pageIndex + '&pageSize=' + this.pageSize;
            var query = [];
            for (var key in this.postData) {
                if (this.postData.hasOwnProperty(key) && key.indexOf('flt') != -1) {
                    query.push("&" + key + "=" + this.postData[key]);
                }
            }
            if (this.postData.sidx && this.postData.sidx.length) { query.push("&sidx=" + this.postData.sidx); }
            if (this.postData.sord && this.postData.sord.length) { query.push("&sord=" + this.postData.sord); }
            query.push('&t=' + (new Date()).valueOf());
            return xurl + query.join('');
        },
        parse: function (arg) {
            if (!arg.success) {
                alert(arg.message);
                return;
            }

            var pageSize = parseInt(this.pageSize, 10);
            var pageIndex = parseInt(this.pageIndex, 10);
            var id = (pageSize * pageIndex) + 1;
            for (var i = 0, ii = arg.records.length; i < ii; i++) {
                arg.records[i].No = id + i;
            }

            this.totalRecords = arg.totalRecords;
            return arg.records;
        },
        date2text: function (dt) {
            if (dt) { return C.parseIsoDateTime(dt).format('d-mmm-yyyy'); }
            else { return ''; }
        },
        fetchPage: function (postData) {
            this.pageIndex = postData.pageIndex;
            this.pageSize = postData.pageSize;
            this.postData = postData;
            this.fetch({ reset: true });
        },
        onResetOrChange: function () {
            var idx = this.pageIndex * this.pageSize + 1;
            for (var i = 0, ii = this.length; i < ii; i++) {
                this.at(i).onChange();
            }
            var toInject = {
                total: Math.ceil(this.totalRecords / this.pageSize),
                page: this.pageIndex + 1,
                records: this.totalRecords,
                rows: this.toJSON()
            };
            try {
                this.trigger('DeliveryOrderRealization.PageReady', toInject);
            } catch (ex) { }
        }
    });
