﻿CFS.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.vesselDocumentColl = new CFS.Model.VesselDocumentList();
        this.deliveryOrderRealizationColl = new CFS.Model.DeliveryOrderRealizationList();

        // create views
        this.tabView = new CFS.View.TabView();
        this.gridView = new CFS.View.GridView();
        this.gridViewDelivery = new CFS.View.GridViewDelivery();
        //this.formView = new CFS.View.FormView({ el: $('#pnl-form') });
        this.filterView = new CFS.View.FilterView({ el: $('#pnl-search') });

        // setup event routes
        this.gridView.bind('GridView.PageRequest', this.onPageRequested, this);
        this.vesselDocumentColl.bind('VesselDocument.PageReady', this.onPageReady, this);
        this.deliveryOrderRealizationColl.bind('DeliveryOrderRealization.PageReady', this.onPageReadyDelivery, this);
        //this.formView.bind('FormView.Created', this.onCreateNew, this);
        //this.formView.bind('FormView.Updated', this.onUpdated, this);
        this.filterView.bind('FilterView.Changed', this.onFilterChanged, this);
        this.filterView.bind('FilterView.Export', this.onExportClick, this);
        this.filterView.bind('FilterView.Report', this.onReportClick, this);  
    },
    routes: {
        "": "grid",
        "pnl-grid": "grid",
        "view/:id": "view",
        "new": "openForm",
    },
    start: function () {
        this.tabView.render();
        this.filterView.render();
        this.gridView.render();
        this.gridViewDelivery.render();
        Backbone.history.start();
    },
    view: function (id) {
        var user = this.vesselDocumentColl.get(id);
        if (user) {
            this.formView.prepareForUpdate(user);
            this.formView.makeReadOnly();
            this.tabView.tabObj.click(1);
            $(window).scrollTop($('#pnl-form').offset().top);
        }
        else
            this.navigate('');
    },
    onCreateNew: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    grid: function () {
        this.tabView.tabObj.click(0);
    },
    openForm: function () {
        this.formView.prepareForInsert();
        this.formView.clearAuditTrail();
        this.formView.validator.resetForm();
        this.tabView.tabObj.click(1);
    },
    onUpdated: function (arg) {
        this.gridView.refresh();
        this.gridViewDelivery.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    onFilterChanged: function (filter) {
        if (filter.param.fltBlNo == '') {
            Msg.hide();
            this.gridView.clear();
            this.gridViewDelivery.clear();
        }
        this.gridView.setFilter(filter.param);
        this.gridViewDelivery.setFilter(filter.param);
        this.gridView.refresh();
        this.gridViewDelivery.refresh();
    },
    onPageRequested: function (arg) {
        Msg.show('Loading...');
        if (arg.fltBlNo == null || arg.fltBlNo == '') {
            Msg.hide();
        }
        else {
            this.vesselDocumentColl.fetchPage(arg);
            this.deliveryOrderRealizationColl.fetchPage(arg);
        }
    },
    onPageReady: function (arg) {
        Msg.hide();        
        this.gridView.dataBind(arg);
    },
    onPageReadyDelivery: function (arg) {
        Msg.hide();        
        this.gridViewDelivery.dataBind(arg);
    },
    onExportClick: function (arg) {
        var url = this.vesselDocumentColl.url();
        if (arg.param.fltBlNo == '' || arg.param.fltBlNo == null) {
            alert('Please select BL to Export.');
        }
        else {
            url = url.replace('BLMonitoring/PageReceiving', 'HttpHandlers/BLMonitoringExporter.ashx');
            window.open(url);
        }
    }
});

$(document).ready(function()
{
    CFS.AppRouter = new CFS.Router.AppRouter();
    CFS.AppRouter.start();
});
