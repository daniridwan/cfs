﻿CFS = { Model: {}, View: {}, Router: {} };

//vessel tank
CFS.Model.VesselTank = Backbone.Model.extend({
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    idAttribute: 'VesselTankId',
    defaults:
    {
    },
    parse: function (arg) {
        return arg;
    },
    onChange: function () {
        var m = this.attributes;

        m.cid = this.cid;
        m.view = '<div class="ui-silk ui-silk-page-white-edit cmd-view" style="cursor:pointer;" title="Edit"></div>';
        m.del = '<div class="ui-silk ui-silk-cancel cmd-del" style="cursor:pointer;" title="Remove"></div>';
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'yyyy-d-mmm';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
    formatDollar: function (num) {
        return (
            parseFloat(num)
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        )
    },
});

CFS.Model.VesselTankList = Backbone.Collection.extend({
    initialize: function () {
        this.on('reset', this.onReset, this);
    },
    url: 'dummy.aspx',
    model: CFS.Model.VesselTank,
    getFormattedArray: function () {
        for (var i = 0, ii = this.length; i < ii; i++) {
            var m = this.at(i);
            m.set('idx', i + 1);
        }
        var toInject = {
            total: 1,
            page: 1,
            records: this.models.length,
            rows: this.toJSON()
        };
        return toInject;
    },
    onReset: function (arg) {
        for (var i = 0, ii = this.length; i < ii; i++) {
            var m = this.at(i);
            m.set('idx', i + 1);
            m.onChange();
        }
    }
});
CFS.Model.VesselTankList = new CFS.Model.VesselTankList();

//plb document
CFS.Model.PlbDocument = Backbone.Model.extend({
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    idAttribute: 'Id',
    defaults:
    {
    },
    parse: function (arg) {
        return arg;
    },
    onChange: function () {
        var m = this.attributes;

        m.cid = this.cid;
        if (m.ApprovedBy == null) {
            m.view = '<div class="ui-silk ui-silk-page-white-edit cmd-view" style="cursor:pointer;" title="Edit"></div>';
        }
        m.del = '<div class="ui-silk ui-silk-cancel cmd-del" style="cursor:pointer;" title="Remove"></div>';
        if (m.ApprovedBy == null) {
            m.appr = '<div class="ui-silk input.ui-silk cmd-approve" style="cursor:pointer;" title="Approve"></div>';
        }
        m.PlbSppbDateText = this.date2text(m.PlbSppbDate, 'd-mmm-yyyy');
        m.ApprovedTimestampText = this.date2text(m.ApprovedTimestamp, 'd-mmm-yyyy HH:MM:ss');
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'yyyy-d-mmm';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
    formatDollar: function (num) {
        return (
            parseFloat(num)
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        )
    },
});

CFS.Model.PlbDocumentList = Backbone.Collection.extend({
    initialize: function () {
        this.on('reset', this.onReset, this);
    },
    url: 'dummy.aspx',
    model: CFS.Model.PlbDocument,
    getFormattedArray: function () {
        for (var i = 0, ii = this.length; i < ii; i++) {
            var m = this.at(i);
            m.set('idx', i + 1);
        }
        var toInject = {
            total: 1,
            page: 1,
            records: this.models.length,
            rows: this.toJSON()
        };
        return toInject;
    },
    onReset: function (arg) {
        for (var i = 0, ii = this.length; i < ii; i++) {
            var m = this.at(i);
            m.set('idx', i + 1);
            m.onChange();
        }
    }
});
CFS.Model.PlbDocumentList = new CFS.Model.PlbDocumentList();

CFS.Model.VesselDocument = Backbone.Model.extend(
{
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    defaults:
    {
    },
    url: function () {
        return g_baseUrl + 'PlbDocument/Save?id=' + (this.id ? this.id : '0');
    },
    idAttribute: 'VesselDocumentId',
    onChange: function () {
        var val = this.attributes;
        val.cid = this.cid;
        var i = 0;
        try {
            val.view = '<a href="#view/' + val.VesselDocumentId + '"><div class="ui-silk ui-silk-zoom"></div></a>';
            val.SppbDateText = this.date2text(val.SppbDate, 'd-mmm-yyyy');
            val.PibDateText = this.date2text(val.PibDate, 'd-mmm-yyyy');
            val.ArrivalDateText = this.date2text(val.ArrivalDate, 'd-mmm-yyyy HH:MM:ss');
            val.EstimateDateText = this.date2text(val.EstimateDate, 'd-mmm-yyyy HH:MM:ss');
            if (val.IsActive == 1) {
                val.IsActiveText = 'Active';
            }
            else {
                val.IsActiveText = 'Inactive';
            }
        }
        catch (er) {
            C.Util.log(er);
        }
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'yyyy-mmm-d';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
});

CFS.Model.VesselDocumentList = Backbone.Collection.extend(
{
    initialize: function () {
        this.on('reset', this.onResetOrChange, this);
        this.on('change', this.onResetOrChange, this);
    },
    postData: {}, //from jqgrid
    totalRecords: 0,
    pageIndex: 0,
    pageSize: 15,
    model: CFS.Model.VesselDocument,
    url: function () {
        var xurl = g_baseUrl + 'PlbDocument/Page?pageIndex=' + this.pageIndex + '&pageSize=' + this.pageSize;
        var query = [];
        for (var key in this.postData) {
            if (this.postData.hasOwnProperty(key) && key.indexOf('flt') != -1) {
                query.push("&" + key + "=" + this.postData[key]);
            }
        }
        if (this.postData.sidx && this.postData.sidx.length) { query.push("&sidx=" + this.postData.sidx); }
        if (this.postData.sord && this.postData.sord.length) { query.push("&sord=" + this.postData.sord); }
        return xurl + query.join('');
        },
        parse: function (arg) {
        if (!arg.success) {
            alert(arg.message);
            return;
        }

        var pageSize = parseInt(this.pageSize, 10);
        var pageIndex = parseInt(this.pageIndex, 10);
        var id = (pageSize * pageIndex) + 1;
        for (var i = 0, ii = arg.records.length; i < ii; i++) {
            arg.records[i].No = id + i;
        }
        this.totalRecords = arg.totalRecords;
        return arg.records;
    },
    date2text: function (dt) {
        if (dt) { return C.parseIsoDateTime(dt).format('d-mmm-yyyy'); }
        else { return ''; }
    },
    fetchPage: function (postData) {
        this.pageIndex = postData.pageIndex;
        this.pageSize = postData.pageSize;
        this.postData = postData;
        this.fetch({ reset: true });
    },
    onResetOrChange: function () {
        var idx = this.pageIndex * this.pageSize + 1;
        for (var i = 0, ii = this.length; i < ii; i++) {
            this.at(i).onChange();
        }
        var toInject = {
            total: Math.ceil(this.totalRecords / this.pageSize),
            page: this.pageIndex + 1,
            records: this.totalRecords,
            rows: this.toJSON()
        };
        try {
            this.trigger('VesselDocument.PageReady', toInject);
        } catch (ex) { }
    }
});