﻿CFS.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.truckLoadingColl = new CFS.Model.TruckLoadingList();

        // create views
        this.tabView = new CFS.View.TabView();
        this.formView = new CFS.View.FormView({ el: $('#pnl-form') });

        // setup event routes
        this.truckLoadingColl.bind('TruckLoading.PageReady', this.onPageReady, this);
        this.formView.bind('FormView.Created', this.onCreateNew, this);
        this.formView.bind('FormView.Updated', this.onUpdated, this);
        
    },
    routes: {
        "": "form",
    },
    start: function () {
        this.tabView.render();
        this.formView.render();
        Backbone.history.start();
    },
    form: function () {
        this.formView.prepareForInsert();
        this.formView.getWeight();
        this.formView.setupTimer();
        this.formView.enableEditControl(false);
        this.formView.validator.resetForm();
        this.tabView.tabObj.click(0);
    },
    openForm: function () {
        this.tabView.tabObj.click(1);
    },
    onUpdated: function (arg) {
        //this.gridView.refresh();
        //this.navigate('view/' + arg.id, { replace: true });
    },
});

$(document).ready(function()
{
    CFS.AppRouter = new CFS.Router.AppRouter();
    CFS.AppRouter.start();
});
