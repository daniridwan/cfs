﻿CFS.View.FormView = C.FormView.extend(
{
initialize: function () {
        var count = 0;
        var weight = 0;
        var prevweight = 0;
        var finished = false;
    },
    events: function() {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
        /* put additional event here */
        });
    },

    render: function() {

    // don't forget to call ancestor's render()
    C.FormView.prototype.render.call(this);

    var dateopt = { altFormat: "d-mmm-yyyy",
            dateFormat: 'dd-MM-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true,
        };

    this.validator = this.$("form").validate({ ignore: null });

    //ini mustinya tergantung permission
    this.enableEdit(true);
},
onEditClick: function(ev){
        C.FormView.prototype.onEditClick.call(this, ev);  
        this.enableEditControl(true);
    },
onSaveClick: function(ev) {
    // ini override onSaveClick di ancestor
    var $this = this;
        if(this.validator.form()==false)
        {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-form').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else
        {
            this.$('.client-msg').hide();
        }

    var toSave = this.$('.the-form').formHash();
    var objSave = new CFS.Model.TruckLoadingPlanning();
    var urlParams = new URLSearchParams(window.location.search);
    var number = urlParams.get('donumber');
    var mode = urlParams.get('mode');
    toSave.DeliveryOrderNumber = number;
    Msg.show('Saving..');
    objSave.url = g_baseUrl + 'Weighting/Weighting?number=' + number;
    objSave.save(toSave,
        {
    	    success: function(model, response) {
    	        if (response.success) {

    	            $this.showThrobber(false);
    	            $this.model = objSave;
    	            $this.model.set(response.record);
    	            $this.dataBind($this.model);
    	            $this.makeReadOnly();
    	            $this.renderServerMessage(response);
    	            //$this.trigger('FormView.Updated', $this.model);
                    $this.enableEditControl(false);
                    Msg.hide();
                    $this.finished = true;
    	        }
    	        else {
    	            $this.renderServerMessage(response);
    	        }
    	    },
    	    error: function(model, response) {
    	        $this.showThrobber(false);
    	        if (window.console) console.log(response);
    	    }
        }); 
},
prepareForInsert: function() {
    C.FormView.prototype.prepareForInsert.apply(this);
    this.validator.resetForm();
    this.enableEditControl(true);
    var urlParams = new URLSearchParams(window.location.search);
    var number = urlParams.get('donumber');
    var mode = urlParams.get('mode');
    var param = { doNumber: number }; 
    this.count = 1;
    this.finished = false;
    //get do details
    $.ajax({
        type: "POST",
        url: g_baseUrl + 'Weighting/GetDODetail',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(param),
        success: function (arg) {
            if (arg.success) {
                if (mode.toLowerCase() === 'weightin') {
                    $('.weight-in').show();
                    $('.weight-out').hide();
                }
                if (mode.toLowerCase() === 'weightout') {
                    $('.weight-in').hide();
                    $('.weight-out').show();
                }
            }
            else {
                console.log(arg.message);
            }
        }
    });
},
setupTimer: function (type) {
    var $this = this;
    this.timerId = setInterval(function () {
        $this.onTimer();
    }, 2000);
},
getWeight: function () {
    var urlParams = new URLSearchParams(window.location.search);
    var mode = urlParams.get('mode');
    var $this = this;
    $.ajax({
        type: "POST",
        url: g_baseUrl + 'DistributionMonitoring/GetWeight',
        contentType: "application/json; charset=utf-8",
        //data: JSON.stringify(param),
        success: function (arg) {
            if (arg.success) {
                $this.weight = arg.records;
                if (mode.toLowerCase() === 'weightin') {
                    $('#txt-initial-weight').val(arg.records);
                }
                if (mode.toLowerCase() === 'weightout') {
                    $('#txt-final-weight').val(arg.records);
                }
                if ($this.weight === $this.prevweight) {
                    $this.count = $this.count + 1;
                }
                else {
                    //reset
                    $this.count = 1;
                }
            }
            else {
                if (mode.toLowerCase() === 'weightin') {
                    $('#txt-initial-weight').val(arg.message);
                }
                else {
                    $('#txt-final-weight').val(arg.message);
                }
            }
        }
    });
},
onTimer: function (type) {
    this.getWeight();
    this.prevweight = this.weight;
    if (this.count >= 5 && this.finished == false && this.weight != 0) {
        this.enableEditControl(true);
    }
    else {
        this.enableEditControl(false);
    }
},
dataBind: function(arg) {
    C.FormView.prototype.dataBind.apply(this, arguments);
    if(arg.get('CreatedTimestamp') != null)
        {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else
        {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
    if(arg.get('UpdatedTimestamp') != null)
        {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else
        {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
},
prepareForUpdate: function(arg) {
        this.dataBind(arg);
        this.validator.resetForm();
        this.makeReadOnly();
        this.enableEditControl(false);
},
enableEditControl: function(en) {
        if (en) {
            $('#btn-save').show();
        }
        else {
            $('#btn-save').hide();
        }
    },
});