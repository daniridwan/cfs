﻿CFS.View.tDlgVesselTank = 
{
    initialize: function(){
    },
    events: {
        "click .dlg-tool-cmd-save": "onOkClick",
        "click .dlg-tool-cmd-cancel": "onCancelClick",
    },
    render: function(){
        this.$el.jqm({
            modal: true
        });
        //
        this.$el.jqDrag('.dlg-title-block');
        this.validator = this.$el.validate({onsubmit:false});
        
    },
    clear:function()
    {
        C.clearField(this.$el);
        this.validator.resetForm();
    },
    show: function(en){
        this.$el[en ? 'jqmShow' : 'jqmHide']();
    },

    onCancelClick: function(ev){
        this.show(false);
    },
    onOkClick: function(ev){
        // musti validate selection
        if(!this.validator.form())
            return;
        
        var val = this.$el.formHash();
        
        if(this.currentRecord) // update
        {
            this.currentRecord.attributes.TankName = $("#ddl-vessel-tank-id option:selected").text();
            this.currentRecord.set(val,{silent:true});
            this.trigger('updated', this.currentRecord);
            this.show(false);
        
        }
        else // insert
        {
            // di sini musti create AssetLog record, lalu push ke collection
            var c = new CFS.Model.VesselTank();
            c.attributes.TankName = $("#ddl-vessel-tank-id option:selected").text();
            c.set(val,{silent:true});
            //alert(c);
            this.trigger('created', c);
            this.show(false);
        }
        
    },
    prepareForUpdate: function(/*model*/arg){
        this.currentMode = 'update';
        this.currentRecord = arg;
        this.dataBind(arg.attributes);
    },
    dataBind: function(val)
    {
        this.$el.formHash(val);
    },
    prepareForInsert:function()
    {
        this.clear();
        this.currentMode = 'insert';
        this.currentRecord = null;
    }
};
CFS.View.DlgVesselTank = Backbone.View.extend(CFS.View.tDlgVesselTank);