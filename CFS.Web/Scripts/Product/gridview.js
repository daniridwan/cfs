﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-product').jqGrid(
        {
            pager: 'tbl-product-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', 'Product Code', 'Product Name', 'Type', 'Density (gr/m<sup>3</sup>)', 'UOM'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 18, sortable: false, hidden: false },
                { name: 'ProductCode', width: 120, sortable: true },
                { name: 'ProductName', width: 120, sortable: true },
                { name: 'ProductType', width: 100, sortable: true },
                { name: 'ProductDensity', width: 120, sortable: true },
                { name: 'UOM', width: 50, sortable: true },
		],
            viewrecords: true,
            sortorder: "asc",
            sortname: "ProductId",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-product-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {            
            $('#tbl-product')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-product').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

