﻿FDM.View.tDlgDownload = 
{
    initialize: function(){
    },
    events: {
        "click .dlg-tool-cmd-save": "onOkClick",
        "click .dlg-tool-cmd-cancel": "onCancelClick",
    },
    render: function(){
        this.$el.jqm({
            modal: true
        });
        //
        this.$el.jqDrag('.dlg-title-block');

        var dateopt = { altFormat: "dd-mm-yy",
            dateFormat: 'dd-M-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true,
        };

        this.validator = this.$el.validate({onsubmit:false});
    },
    clear:function()
    {
        //digunakan untuk clear seluruh element
        //C.clearField(this.$el);
        $('#txt-transporter-code').val('');
        this.validator.resetForm();
    },
    show: function(en){
        this.$el[en ? 'jqmShow' : 'jqmHide']();
    },

    onCancelClick: function(ev){
        this.show(false);
    },
    onOkClick: function(ev){
        // musti validate selection
        if(!this.validator.form())
            return;
        var val = this.$el.formHash();
        Msg.show('Downloading...');
        var $this = this;
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'Transporter/Download',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(val),
            success: function(arg) {
                if(arg.success){
                    Msg.hide();
                    alert(arg.message);
                }
                else{
                    Msg.hide();
                    alert(arg.message);
                }
            }
        });  
        this.show(false);              
    },
    prepareForUpdate: function(/*model*/arg){
        this.currentMode = 'update';
        this.currentRecord = arg;
        this.dataBind(arg.attributes);
    },
    dataBind: function(val)
    {
        this.$el.formHash(val);
    },
    prepareForInsert:function()
    {
        this.clear();
        this.currentMode = 'insert';
        this.currentRecord = null;
    }
};
FDM.View.DlgDownload = Backbone.View.extend(FDM.View.tDlgDownload);