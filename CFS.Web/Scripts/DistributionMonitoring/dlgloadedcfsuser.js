﻿CFS.View.tDlgLoadedCFSUser =
    {
        initialize: function () {
            this.dlgLoadedCFSUser = null;
        },
        events: {
            "click .dlg-tool-cmd-save": "onOkClick",
            "click .dlg-tool-cmd-cancel": "onCancelClick",
            "keypress input[type='text']": "onTextKeypress",
        },
        render: function () {
            this.$el.jqm({
                modal: true
            });
            //
            this.$el.jqDrag('.dlg-title-block');
            this.validator = this.$el.validate({ onsubmit: false });
        },
        clear: function () {
            C.clearField(this.$el);
            this.validator.resetForm();
        },
        show: function (en, parent) {
            this.$el[en ? 'jqmShow' : 'jqmHide']();
            this.dlgLoadedCFSUser = parent;
        },
        onCancelClick: function (ev) {
            this.dlgLoadedCFSUser.trigger('DlgLoadedCFSUser.ResetNavigation', {});  
            this.show(false);
        },
        onOkClick: function (ev) {
            // musti validate selection
            if (!this.validator.form())
                return;

            var val = this.$el.formHash();
            
            if (this.currentRecord) // update
            {
                var $this = this;
                this.currentRecord.set(val, { silent: true });
                $.ajax({
                    type: "POST",
                    url: g_baseUrl + 'DistributionMonitoring/Update?id=' + this.currentRecord.id,
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(this.currentRecord.attributes),
                    success: function (arg) {
                        if (arg.success) {
                            $this.dlgLoadedCFSUser.trigger('DlgLoadedCFSUser.ReprintDO', { id: $this.currentRecord.id });  
                            $this.show(false);
                        }
                        else {
                            alert(arg.message);
                        }
                    }
                });
                //this.trigger('updated', this.currentRecord);
            }
        },
        onRenderLoadedUser: function () {
            $('#ddl-update-loaded-cfs-user').val('').trigger("chosen:updated").find('option').remove().end();
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'LoadingUser/GetAllPersonnel',
                contentType: "application/json; charset=utf-8",
                //data: JSON.stringify(param),
                success: function (arg) {
                    if (arg.success) {
                        var max = arg.records.length;
                        if (max > 0) {
                            var ddlPersonnel = $("#ddl-update-loaded-cfs-user");
                            ddlPersonnel.append($("<option>", { value: '', text: '' }));
                            for (i = 0; i < max; i++) {
                                ddlPersonnel.append($("<option>", { value: arg.records[i].Name, text: arg.records[i].Name }));
                            }
                            ddlPersonnel.trigger("chosen:updated");
                        }
                    }
                    else {
                        console.log(arg.message);
                    }
                }
            });
        },
        prepareForUpdate: function (/*model*/arg) {
            this.currentMode = 'update';
            this.currentRecord = arg;
            this.dataBind(arg.attributes);
        },
        dataBind: function (val) {
            this.$el.formHash(val);
        },
        prepareForInsert: function () {
            this.clear();
            this.currentMode = 'insert';
            this.currentRecord = null;
        },
        onTextKeypress: function (ev) {
            if (ev.keyCode === 13) {
                return false;
            }
        },
    };
CFS.View.DlgLoadedCFSUser = Backbone.View.extend(CFS.View.tDlgLoadedCFSUser);