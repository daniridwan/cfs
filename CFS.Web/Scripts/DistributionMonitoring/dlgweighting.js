﻿CFS.View.tDlgWeighting =
    {
        initialize: function () {
            var count = 0;
            var weight = 0;
            var prevweight = 0;
            var finished = false;
        },
        events: {
            "click .dlg-tool-cmd-save": "onOkClick",
            "click .dlg-tool-cmd-cancel": "onCancelClick",
        },
        render: function () {
            this.$el.jqm({
                modal: true
            });
            //
            this.$el.jqDrag('.dlg-title-block');
            this.validator = this.$el.validate({ onsubmit: false });
            this.type = '';

        },
        clear: function () {
            C.clearField(this.$el);
            this.validator.resetForm();
        },
        show: function (en, type) {
            this.$el[en ? 'jqmShow' : 'jqmHide']();
            if (type == 'Chemical') {
                this.type = type;
                $('.li-fuel').hide();
                $('.li-chemical').show();
                this.setupTimer('Chemical');
            }
            else {
                this.type = type;
                $('.li-fuel').show();
                $('.li-chemical').hide();
            }
        },
        setupTimer: function (type) {
            var $this = this;
            if (this.type == 'Chemical') {
                this.timerId = setInterval(function () {
                    $this.onTimer();
                }, 2000);
            }
        },
        onGetWeight: function () {
            var $this = this;
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'DistributionMonitoring/GetWeight',
                contentType: "application/json; charset=utf-8",
                //data: JSON.stringify(param),
                success: function (arg) {
                    if (arg.success) {
                        $this.weight = arg.records;
                        $('#txt-scan-weight-2').val(arg.records);
                        $('#txt-weighting-final-weight').val(arg.records);

                        if ($this.weight === $this.prevweight) {
                            $this.count = $this.count + 1;
                        }
                        else {
                            //reset
                            $this.count = 1;
                        }

                    }
                    else {
                        $('#txt-scan-weight-2').val(arg.message);
                        $('#txt-weighting-final-weight').val(arg.records);
                    }
                }
            });
        },
        onCancelClick: function (ev) {
            this.type = '';
            this.show(false);
        },
        onOkClick: function (ev) {
            // musti validate selection
            if (!this.validator.form())
                return;

            var val = this.$el.formHash();
            if (this.type == 'Chemical') {
                val.Actual = 0;
                //val.InitialWeight = $("#txt-weighting-initial-weight").val();
                val.FinalWeight = $("#txt-weighting-final-weight").val();
            }
            else {
                val.Actual = $("#txt-weighting-actual").val();
                //val.InitialWeight = 0;
                val.FinalWeight = 0;
            }
            
            if (this.currentRecord) // update
            {
                this.currentRecord.set(val, { silent: true });
                this.trigger('updated', this.currentRecord);
                this.show(false);
                this.finished = true;
            }
        },
        prepareForUpdate: function (/*model*/arg) {
            this.currentMode = 'update';
            this.currentRecord = arg;
            this.dataBind(arg.attributes);
            $("#btn-ok-weighting").hide();
            $("#txt-weighting-actual").val(arg.attributes.Actual);
            //$("#txt-weighting-initial-weight").val(arg.attributes.InitialWeight);
            $("#txt-weighting-final-weight").val(arg.attributes.FinalWeight);
        },
        dataBind: function (val) {
            this.$el.formHash(val);
        },
        prepareForInsert: function () {
            this.clear();
            this.currentMode = 'insert';
            this.currentRecord = null;
        },
        onTimer: function (type) {
            if (this.type == 'Chemical') {
                this.onGetWeight();
                this.prevweight = this.weight;
                console.log(this.count);
                if (this.count >= 10 && this.weight != 0) {
                    $("#btn-ok-weighting").show();
                }
                else {
                    $("#btn-ok-weighting").hide();
                }
            }
        }
    };
CFS.View.DlgWeighting = Backbone.View.extend(CFS.View.tDlgWeighting);