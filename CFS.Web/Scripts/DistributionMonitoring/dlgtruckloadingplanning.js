﻿CFS.View.tDlgTruckLoadingPlanning = 
{
    initialize: function(){
    },
    events: {
        "click .dlg-tool-cmd-save": "onOkClick",
        "click .dlg-tool-cmd-cancel": "onCancelClick",
        "change #txt-administration-vessel-document-id": "onChangeDocument"
    },
    render: function(){
        this.$el.jqm({
            modal: true
        });
        //
        this.$el.jqDrag('.dlg-title-block');
        this.validator = this.$el.validate({ onsubmit: false });
    },
    clear:function()
    {
        C.clearField(this.$el);
        this.validator.resetForm();
    },
    show: function(en){
        this.$el[en ? 'jqmShow' : 'jqmHide']();
    },

    onCancelClick: function(ev){
        $("#txt-administration-vessel-document-id").val('').trigger("chosen:updated");
        $('#txt-administration-vessel-document-id').empty();
        this.show(false);
    },
    onOkClick: function(ev){
        // musti validate selection
        if(!this.validator.form())
            return;

        var val = this.$el.formHash();
        var stringCqty = $("#txt-administration-confirmed-quantity").val();
        var stringFp = $("#txt-administration-filling-point-name option:selected").text();
        var stringVd = $("#txt-administration-vessel-document-id option:selected").text();
        var stringPlb = $("#txt-administration-plb-document-id option:selected").text();
        if(this.currentRecord) // update
        {
            var splitFp = stringFp.split("|");
            var splitVd = stringVd.split("|");
            if (stringCqty <= 0)
            {
                alert('Please input Confirmed Quantity');
                return;
            }
            if (stringFp === '') {
                alert('Please Select Filling Point');
                return;
            }
            if (stringVd === '') {
                alert('Please Select Vessel Document');
                return;
            }
            if (stringPlb === '' && splitVd[3].trim() === 'Import PLB') {
                alert('Please Select Plb Document');
                return;
            }
            this.currentRecord.attributes.TankName = splitFp[1].trim();
            this.currentRecord.attributes.PumpName = splitFp[2].trim();
            this.currentRecord.attributes.Flowrate = splitFp[3].trim();
            
            var tankName = splitVd[2].trim();
            if (tankName != this.currentRecord.attributes.TankName.trim()) {
                alert('Mismatch data between Tank Source and Vessel Document');
                return;
            }
            this.currentRecord.attributes.VesselNo = $("#txt-administration-vessel-document-id option:selected").text();
            this.currentRecord.attributes.PlbSppbNo = $("#txt-administration-plb-document-id option:selected").text();
            this.currentRecord.set(val, { silent: true });
            this.trigger('updated', this.currentRecord);
            this.show(false);
        }
        else if (this.currentRecord == null)// insert
        {
            var deliveryNumber = $("#txt-administration-delivery-order-number").val(); //
            var toSend =
            {
                number: deliveryNumber
            };
            var $this = this;
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'DeliveryOrder/ValidateDeliveryOrder',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(toSend),
                success: function (arg) {
                    if (arg.success) {
                        $this.trigger('created', arg); // jump to trigger defined in administrationformview.js
                        $("#txt-delivery-order-number").val(deliveryNumber);
                        $("#txt-delivery-order-number").text(deliveryNumber);
                        $("#hdn-administration-company-id").val(arg.deliveryOrder.CompanyId);
                        $("#hdn-administration-company-id").text(arg.deliveryOrder.CompanyId);
                    }
                    else {
                        alert(arg.message);
                    }
                }
            });
            this.show(false);
        }
    },
    prepareForUpdate: function(/*model*/arg){
        this.currentMode = 'update';
        this.currentRecord = arg;
        this.dataBind(arg.attributes);
        this.$('.insert-plan').hide();
        this.$('.update-plan').show();
        this.$('.plb-plan').hide();
        $('#txt-administration-vessel-document-id').empty();
        $('#txt-administration-vessel-document-id').append('<option></option>');
        $('#txt-administration-filling-point-name').empty();
        $('#txt-administration-filling-point-name').append('<option></option>');
        $('#txt-administration-plb-document-id').empty();
        $('#txt-administration-plb-document-id').append('<option></option>');
        //ajax get vessel document based on product and company
        var $this = this;
        var toSend =
        {
            companyId : $("#hdn-administration-company-id").val(),
            productId: arg.attributes.ProductId.ProductId
        };
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'VesselDocument/GetVesselDocument',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(toSend),
            success: function (arg) {
                if (arg.success) {
                    var max = arg.record.length;
                    for (i = 0; i < max; i++) {
                        $('#txt-administration-vessel-document-id').append('<option value=' + arg.record[i].VesselDocumentId + '>' + arg.record[i].VesselNo + ' | ' + arg.record[i].BlNo + ' | ' + arg.record[i].Name + ' | ' + arg.record[i].Type + '</option>');
                    }
                    $('#txt-administration-vessel-document-id').trigger("chosen:updated");
                }
                else {
                    alert(arg.message);
                }
            }
        });
        this.prepareFillingPoint(arg.attributes.ProductId.ProductId);
    },
    dataBind: function(val)
    {
        this.$el.formHash(val);
    },
    onChangeDocument: function () {
        var stringVd = $("#txt-administration-vessel-document-id option:selected").text();
        var splitVd = stringVd.split("|");
        if (splitVd[3] === ' Import PLB') {
            //show kolom plb
            this.$('.plb-plan').show();
            //find sppb approved based on vessel document id
            var vdId = $('#txt-administration-vessel-document-id').val();
            //clear plb drop down
            $('#txt-administration-plb-document-id').empty();
            $('#txt-administration-plb-document-id').append('<option></option>');
            var toSend =
            {
                vesselDocumentId: vdId
            };
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'PlbDocument/GetPlbDocument',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(toSend),
                success: function (arg) {
                    if (arg.success) {
                        var max = arg.record.length;
                        for (i = 0; i < max; i++) {
                            $('#txt-administration-plb-document-id').append('<option value=' + arg.record[i].Id + '>' + arg.record[i].PlbSppbNo +'</option>');
                        }
                        $('#txt-administration-plb-document-id').trigger("chosen:updated");
                    }
                    else {
                        alert(arg.message);
                    }
                }
            });
        }
        else{
            //hide kolom plb
            this.$('.plb-plan').hide();
        }
    },
    prepareForInsert:function()
    {   
        this.clear();
        this.currentMode = 'insert';
        this.currentRecord = null;
        this.$('.insert-plan').show();
        this.$('.update-plan').hide();
        this.$('.plb-plan').hide();
    },
    prepareFillingPoint: function (prodId) {
        var $this = this;
        var toSend =
        {
            companyId: $("#hdn-administration-company-id").val(),
            productId: prodId
        };
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'DeliveryOrder/FillingPointSelection',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(toSend),
            success: function (arg) {
                if (arg.success) {
                    var max = arg.records.length;
                    for (i = 0; i < max; i++) {
                        $('#txt-administration-filling-point-name').append('<option value=' + arg.records[i].FillingPointName + '>' + arg.records[i].FillingPointName + ' | ' + arg.records[i].TankName + ' | ' + arg.records[i].PumpName + ' | ' + arg.records[i].Flowrate + ' </option>');
                    }
                    $('#txt-administration-filling-point-name').trigger("chosen:updated");
                }
                else {
                    alert(arg.message);
                }
            }
        });
    },
    prepareVesselDocument: function () {

    }
};
CFS.View.DlgTruckLoadingPlanning = Backbone.View.extend(CFS.View.tDlgTruckLoadingPlanning);