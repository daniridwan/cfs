﻿CFS.View.tGridTruckLoadingPlanning =
{
    initialize: function (el) {
    },
    events: {
        "click .cmd-view": "onViewClick",
        "click .cmd-del": "onDeleteClick"
    },
    showControl: function (en) {
        if (en) {
            this.gridObj.showCol('view');
            //this.gridObj.showCol('del');
            this.gridObj.showCol('cb');
        }
        else {
            this.gridObj.hideCol('view');
            //this.gridObj.hideCol('del');
            this.gridObj.hideCol('cb');
        }
    },
    render: function () {
        var $this = this;

        this.gridObj = this.$el.jqGrid({
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },
            jsonReader: {
                id: 0,
                repeatitems: false
            },
            colNames: ['Id', 'No', '', 'DO Number', 'Product', 'Density (gr/m<sup>3</sup>)', 'Product Type', 'FP Name', 'FP Group','Tank Source', 'Preset', 'Confirmed Qty', 'Actual', 'Initial Weight', 'Final Weight', 'UOM', 'Vessel Document', 'PLB Document', 'Pump Name', 'Flowrate (m<sup>3</sup>/h)'],
            colModel: [
                       { name: 'cid', width: 60, hidden: true },
                       { name: 'idx', width: 30, hidden: false },
                       { name: 'view', width: 24, sortable: false, hidden: true },
                       //{ name: 'del', width: 24, sortable: false, hidden: true },
                       { name: 'DeliveryOrderNumber', width: 80, sortable: true },
                       { name: 'ProductName', width: 130, sortable: true },
                       { name: 'ProductDensity', width: 115, sortable: true },
                       { name: 'ProductType', width: 95, sortable: false },
                       { name: 'FillingPointName', width: 135, sortable: true },
                       { name: 'FillingPointGroup', width: 80, sortable: true },
                       { name: 'TankName', width: 95, sortable: true },
                       { name: 'Preset', index: 'Preset', width: 60, sortable: false },
                       { name: 'ConfirmedQuantity', index: 'ConfirmedQuantity', width: 95, sortable: false },
                       { name: 'Actual', index: 'Actual', width: 60, sortable: false },
                       { name: 'InitialWeight', index: 'InitialWeight', width: 95, sortable: false },
                       { name: 'FinalWeight', index: 'FinalWeight', width: 95, sortable: false },
                       { name: 'UOM', width: 50, sortable: true },
                       { name: 'VesselNo', width: 165, sortable: true },
                       { name: 'PlbSppbNo', width: 130, sortable: true },
                       { name: 'PumpName', width: 85, sortable: true },
                       { name: 'Flowrate', width: 120, sortable: true },
                      ],
            viewrecords: true,
            rowNum: 300,
            sortorder: "asc",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() * 0.95,
            pginput: true,
            altRows: true,
            prmNames: {
                rows: 'pageSize',
                page: 'pageIndex'
            }

        });
    },
    dataBind: function (toInject) {
        try {
            this.$el[0].addJSONData(toInject);
            $('.soft-delete', this.$el).each(function (key, val) {
                $(val).parent().parent().addClass('cancel');
            });
            this.$('.warnrest').each(function (key, val) {
                $(val).parent().parent().addClass('warnrest');
            });
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    onViewClick: function (ev) {
        var id = $(ev.target).parent().parent().attr('id');
        this.trigger('GridPlanningView.RequestView', id);
    },
    onActualClick: function (ev) {
        var id = $(ev.target).parent().parent().attr('id');
        this.trigger('GridPlanningView.RequestActual', id);
    },
    onDeleteClick: function (ev) {
        var id = $(ev.target).parent().parent().attr('id');
        this.trigger('GridPlanningView.RequestDelete', id);
    },
};
CFS.View.GridTruckLoadingPlanning = Backbone.View.extend(CFS.View.tGridTruckLoadingPlanning);