﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-distribution-monitoring').jqGrid(
        {
            pager: 'tbl-distribution-monitoring-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', 'SP', 'FI', 'DS', 'PS','DO', 'Registration Plate', 'DO Number', 'Total Preset (Kg / KL)', 'Shipment Status', 'Adm. Timestamp', 'Gate In Timestamp', 'Gate Out Timestamp', 'Good Issue Timestamp', 'Good Issue Message'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 20, sortable: false, hidden: false },
                //{ name: 'del', width: 20, sortable: false, hidden: false },
                //{ name: 'queueTruck', width: 25, sortable: false, hidden: false },
                { name: 'loadingInfo', width: 25, sortable: false, hidden: false },
                //{ name: 'weighingTicket', width: 25, sortable: false, hidden: true },
                { name: 'inspectionForm', width: 25, sortable: false, hidden: false },
                { name: 'sealList', width: 25, sortable: false, hidden: false },
                { name: 'productSample', width: 25, sortable: false, hidden: false },
                { name: 'deliveryReport', width: 25, sortable: false, hidden: false },
                { name: 'TruckRegistrationPlate', width: 135, sortable: true },
                { name: 'DeliveryOrderNumber', width: 135, sortable: true },
                { name: 'TotalPresetText', index: 'TotalPreset', width: 120, sortable: false },
                { name: 'ShipmentStatusText', index: 'ShipmentStatus', width: 125, sortable: true, hidden: false},
                { name: 'AdministrationTimestampText', index: 'AdministrationTimestamp', width: 160, sortable: true },
                { name: 'GateInTimestampText', index: 'GateInTimestamp', width: 150, sortable: true },
                { name: 'GateOutTimestampText', index: 'GateOutTimestamp', width: 160, sortable: true },
                { name: 'GoodIssueTimestampText', index: 'GoodIssueTimestamp', width: 180, sortable: true },
                { name: 'GoodIssueMessage', width: 170, sortable: true },
		],
            viewrecords: true,
            sortorder: "desc",
            sortname: "AdministrationTimestamp",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-distribution-monitoring-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {            
            $('#tbl-distribution-monitoring')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-distribution-monitoring').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

