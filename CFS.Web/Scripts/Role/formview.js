﻿CFS.View.FormView = C.FormView.extend(
{
    initialize: function () {

    },
    events: function () {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
            /* put additional event here */
        });
    },
    render: function () {
        // don't forget to call ancestor's render()
        C.FormView.prototype.render.call(this);
        this.validator = this.$("form").validate({ ignore: null });
        this.enableEdit(true);
    },
    onEditClick: function (ev) {
        C.FormView.prototype.onEditClick.call(this, ev);
    },
    onSaveClick: function (ev) {
        // ini override onSaveClick di ancestor
        var $this = this;
        if (this.validator.form() == false) {
            this.renderClientMessage();
            this.$('.client-msg').show();
            return;
        }
        else {
            this.$('.client-msg').hide();
        }


        // di sini mustinya ada validate
        // ...
        var toSave = this.$('.the-form').formHash();
        delete (toSave.RoleId);
        if (this.model) //update
        {
            this.showThrobber(true);
            this.model.save(toSave,
            {
                success: function (model, response) {
                    if (response.success) {
                        $this.showThrobber(false);
                        $this.model.set(response.record);
                        $this.dataBind($this.model);
                        $this.makeReadOnly();
                        $this.prepareForUpdate($this.model);
                        $this.renderServerMessage(response);
                        $this.trigger('FormView.Updated', $this.model);
                    }
                    else {
                        $this.renderServerMessage(response);
                    }
                },
                error: function (model, response) {
                    $this.showThrobber(false);
                    if (window.console) console.log(response);
                }
            });
        }
        else //insert
        {
            var objSave = new CFS.Model.Role();
            delete (toSave.RoleId);
            objSave.save(toSave,
    	    {
    	        success: function (model, response) {
    	            if (response.success) {
    	                $this.showThrobber(false);
    	                $this.model = objSave;
    	                $this.model.set(response.record);
    	                $this.dataBind($this.model);
    	                $this.makeReadOnly();
    	                $this.renderServerMessage(response);
    	                $this.trigger('FormView.Created', $this.model);
    	            }
    	            else {
    	                $this.renderServerMessage(response);
    	            }
    	        },
    	        error: function (model, response) {
    	            $this.showThrobber(false);
    	            if (window.console) console.log(response);
    	        }
    	    });
        }
    },
    prepareForInsert: function () {
        C.FormView.prototype.prepareForInsert.apply(this);
        //$('#txt-role-name').removeAttr('readonly');
    },
    dataBind: function (arg) {
        C.FormView.prototype.dataBind.apply(this, arguments);
    },
    prepareForUpdate: function (arg) {
        this.dataBind(arg);
        this.validator.resetForm();
        this.makeReadOnly();
    }
});