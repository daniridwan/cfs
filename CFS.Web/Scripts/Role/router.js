﻿CFS.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.roleColl = new CFS.Model.RoleList();
        // create views
        this.tabView = new CFS.View.TabView();
        this.gridView = new CFS.View.GridView();
        this.formView = new CFS.View.FormView({ el: $('#pnl-form') });
        this.filterView = new CFS.View.FilterView({ el: $('#pnl-search') });
        this.roleColl.bind('Role.PageReady', this.onPageReady, this);
        // delete 
        this.dlgDelete = new CFS.View.DlgDelete({ el: $('#dlg-delete') });
        //setup event routes
        this.gridView.bind('GridView.PageRequest', this.onPageRequested, this);
        this.roleColl.bind('Roles.PageReady', this.onPageReady, this);
        this.formView.bind('FormView.Created', this.onCreateNew, this);
        this.formView.bind('FormView.Updated', this.onUpdated, this);
        this.filterView.bind('FilterView.Changed', this.onFilterChanged, this);
        this.filterView.bind('FilterView.Export', this.onExportClick, this);
        this.dlgDelete.bind('DlgDelete.Deleted', this.onDeleted);
    },
    routes: {
        "": "grid",
        "pnl-grid": "grid",
        "view/:id": "view",
        "upload/:id": "upload",
        "new": "openForm",
        "delete/:id": "delete"
    },
    start: function () {
        this.tabView.render();
        this.gridView.render();
        this.formView.render();
        Backbone.history.start();
    },
    view: function (id) {
        var user = this.roleColl.get(id);
        if (user) {
            //C.Util.log(user.attributes);
            this.formView.validator.resetForm();
            this.formView.dataBind(user);
            this.formView.makeReadOnly();
            this.tabView.tabObj.click(1);
            $(window).scrollTop($('#pnl-form').offset().top);
        }
        else
            this.navigate('');
    },
    delete: function(id) {
        var roleid = this.roleColl.get(id);
        this.dlgDelete.detectDefault(roleid.id);
        this.dlgDelete.show(true);
        this.navigate('', { replace: true });
    },
    grid: function () {
        this.tabView.tabObj.click(0);
    },
    openForm: function () {
        this.formView.prepareForInsert();
        this.formView.validator.resetForm();
        this.tabView.tabObj.click(1);
    },
    onCreateNew: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    onUpdated: function (arg) {
        this.navigate('view/' + arg.id, { replace: true });
    },
    onDeleted: function () {
        try{
            this.gridView.refresh();
        }catch (er) {
            var $thisgridView = new FDM.View.GridView();
            $thisgridView.refresh();
        }
    },
    onFilterChanged: function (filter) {
        this.gridView.setFilter(filter.param);
        this.gridView.refresh();
    },
    onPageRequested: function (arg) {
        Msg.show('loading...');
        this.roleColl.fetchPage(arg);
    },
    onPageReady: function (arg) {
        Msg.hide();
        this.gridView.dataBind(arg);
    }
});

$(document).ready(function () {
    CFS.AppRouter = new CFS.Router.AppRouter();
    CFS.AppRouter.start();
});
