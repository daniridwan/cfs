﻿var userid = null;
FDM.View.DlgDelete = Backbone.View.extend(
{
    initialize: function () {
    },
    events: {
        "click .dlg-tool-cmd-save": "onOkClick",
        "click .dlg-tool-cmd-cancel": "onCancelClick"
    },
    render: function () {
        this.$el.jqm({
            modal: true
        });
        this.$el.jqDrag('.dlg-title-block');
        this.validator = this.$el.validate({ onsubmit: false });

    },
    detectDefault: function (id) {
        userid = id;
    },
    show: function (en) {
        en ? this.$el.show() : this.$el.hide();
    },

    onCancelClick: function (ev) {
        this.show(false);
    },
    onOkClick: function (ev) {
        $this = this;

        Backbone.ajaxMan.add(
            {
                url: 'User/Delete?Id=' + userid,
                success: function (arg) {
                    if (arg.success) {
                        alert(arg.messages);
                        $this.trigger('DlgDelete.Deleted');
                    }
                    else {
                        alert(arg.messages);
                        C.Util.log(arg);
                    }
                }
            });
        this.show(false);
    }
});
