﻿CFS.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.usersColl = new CFS.Model.UsersList();

        // create views
        this.tabView = new CFS.View.TabView();
        this.formView = new CFS.View.FormView({ el: $('#pnl-form') })        

        // setup event routes
        this.usersColl.bind('Users.PageReady', this.onPageReady, this);
        this.formView.bind('FormView.Updated', this.onUpdated, this);
        
    },
    routes: {
        "": "grid",
    },
    start: function () {
        this.tabView.render();
        this.formView.render();
        Backbone.history.start();
    },
    grid: function () {
        this.tabView.tabObj.click(0);
    },
    onUpdated: function (arg) {
        $('#btn-change-password').hide();
        $('#orderedList').hide();
    }
});

$(document).ready(function()
{
    CFS.AppRouter = new CFS.Router.AppRouter();
    CFS.AppRouter.start();
});
