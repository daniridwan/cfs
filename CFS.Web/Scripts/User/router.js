﻿FDM.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.userColl = new FDM.Model.UserList();
        // create views
        this.tabView = new FDM.View.TabView();
        this.gridView = new FDM.View.GridView();
        this.formView = new FDM.View.FormView({ el: $('#pnl-form') });
        this.filterView = new FDM.View.FilterView({ el: $('#pnl-search') });
        this.userColl.bind('Users.PageReady', this.onPageReady, this);
        // delete 
        this.dlgDelete = new FDM.View.DlgDelete({ el: $('#dlg-delete') });
        //setup event routes
        this.gridView.bind('GridView.PageRequest', this.onPageRequested, this);
        this.userColl.bind('Users.PageReady', this.onPageReady, this);
        this.formView.bind('FormView.Created', this.onCreateNew, this);
        this.formView.bind('FormView.Updated', this.onUpdated, this);
        this.filterView.bind('FilterView.Changed', this.onFilterChanged, this);
        this.filterView.bind('FilterView.Export', this.onExportClick, this);
        this.dlgDelete.bind('DlgDelete.Deleted', this.onDeleted);
    },
    routes: {
        "": "grid",
        "pnl-grid": "grid",
        "view/:id": "view",
        "upload/:id": "upload",
        "new": "openForm",
        "delete/:id": "delete"
    },
    start: function () {
        this.tabView.render();
        this.gridView.render();
        this.formView.render();
        Backbone.history.start();
    },
    view: function (id) {
        var user = this.userColl.get(id);
        if (user) {
            this.formView.validator.resetForm();
            this.formView.dataBind(user);
            this.formView.prepareForUpdate(user);
            this.formView.makeReadOnly();
            this.tabView.tabObj.click(1);
            $(window).scrollTop($('#pnl-form').offset().top);
        }
        else
            this.navigate('');
    },
    delete: function(id) {
        var userid = this.userColl.get(id);
        this.dlgDelete.detectDefault(userid.id);
        this.dlgDelete.show(true);
        this.navigate('', { replace: true });
    },
    grid: function () {
        this.tabView.tabObj.click(0);
    },
    openForm: function () {
        this.formView.prepareForInsert();
        this.formView.validator.resetForm();
        this.tabView.tabObj.click(1);
    },
    onCreateNew: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    onUpdated: function (arg) {
        this.navigate('view/' + arg.id, { replace: true });
    },
    onDeleted: function () {
        try{
            this.gridView.refresh();
        }catch (er) {
            var $thisgridView = new FDM.View.GridView();
            $thisgridView.refresh();
        }
    },
    onFilterChanged: function (filter) {
        this.gridView.setFilter(filter.param);
        this.gridView.refresh();
    },
    onPageRequested: function (arg) {
        Msg.show('loading...');
        this.userColl.fetchPage(arg);
    },
    onPageReady: function (arg) {
        Msg.hide();
        this.gridView.dataBind(arg);
    },
    onExportClick: function (arg) {
        var url = this.userColl.url();
        url = url.replace('User/Page','HttpHandlers/UserExporter.ashx');
        window.open(url);
    },
});

$(document).ready(function () {
    FDM.AppRouter = new FDM.Router.AppRouter();
    FDM.AppRouter.start();

});