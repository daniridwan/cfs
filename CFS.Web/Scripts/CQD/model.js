﻿FDM = { Model: {}, View: {}, Router: {} };
$.metadata.setType("attr", "validate");
//Crew
FDM.Model.ViewCertificateQuantityDischarge = Backbone.Model.extend(
{
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    defaults:
    {
        InsertedDate: null,
        LastUpdatedDate: null,
        Visible: 1
    },
    url: function () {
        return g_baseUrl + 'CQD/Save?id=' + (this.id ? this.id : '0');
    },
    idAttribute: 'TankTicketOpenId',
    onChange: function () {
        var val = this.attributes;
        try {
            val.view = '<a href="#view/' + val.TankTicketOpenId + '"><div class="ui-silk ui-silk-zoom"></div></a>';
            val.NameText = '<a href="#view/' + val.TankTicketOpenId + '">' + val.TankTicketOpenId + '</a>';
            val.StartTimestampText = this.date2text(val.StartTimestamp, 'd-mmm-yyyy HH:MM:ss');
            val.StopTimestampText = this.date2text2(val.StopTimestamp, 'd-mmm-yyyy HH:MM:ss');
            val.LiquidLevelBefore = this.formatDollar(val.LiquidLevelBefore);
            val.LiquidLevelAfter = this.formatDollar(val.LiquidLevelAfter);
            val.LiquidVolObsBefore = this.formatDollar(val.LiquidVolObsBefore);
            val.LiquidVolObsAfter = this.formatDollar(val.LiquidVolObsAfter);
            val.LiquidVolumeCorrectionFactorBefore = this.formatDollar(val.LiquidVolumeCorrectionFactorBefore);
            val.LiquidVolumeCorrectionFactorAfter = this.formatDollar(val.LiquidVolumeCorrectionFactorAfter);
            val.LiquidVol15Before = this.formatDollar(val.LiquidVol15Before);
            val.LiquidVol15After = this.formatDollar(val.LiquidVol15After);
            val.LiquidMassBefore = this.formatDollar(val.LiquidMassBefore);
            val.LiquidMassAfter = this.formatDollar(val.LiquidMassAfter);
            val.VaporPressureBefore = this.formatDollar(val.VaporPressureBefore);
            val.VaporPressureAfter = this.formatDollar(val.VaporPressureAfter);
            val.VaporPressureFactorBefore = this.formatDollar(val.VaporPressureFactorBefore);
            val.VaporPressureFactorAfter = this.formatDollar(val.VaporPressureFactorAfter);
            val.VaporMassBefore = this.formatDollar(val.VaporMassBefore);
            val.VaporMassAfter = this.formatDollar(val.VaporMassAfter);
        }
        catch (er) {
            C.Util.log(er);
        }
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'dd-mm-yyyy';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
    date2text2: function (dt, fmt) { //for age calculation
        if (!fmt) fmt = 'dd/mm/yyyy';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
    formatDollar: function (num) {
        return (
                parseFloat(num)
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            )
    },
});

FDM.Model.CQDList = Backbone.Collection.extend(
{
    initialize: function () {
        this.on('reset', this.onResetOrChange, this);
    },
    postData: {}, //from jqgrid
    totalRecords: 0,
    pageIndex: 0,
    pageSize: 15,
    model: FDM.Model.ViewCertificateQuantityDischarge,
    url: function () {
        var xurl = g_baseUrl + 'CQD/Page?pageIndex=' + this.pageIndex + '&pageSize=' + this.pageSize;
        var query = [];
        for (var key in this.postData) {
            if (this.postData.hasOwnProperty(key) && key.indexOf('flt') != -1) {
                query.push("&" + key + "=" + this.postData[key]);
            }
        }
        if (this.postData.sidx && this.postData.sidx.length) { query.push("&sidx=" + this.postData.sidx); }
        if (this.postData.sord && this.postData.sord.length) { query.push("&sord=" + this.postData.sord); }
        query.push('&t=' + (new Date()).valueOf());
        return xurl + query.join('');
    },
    parse: function (arg) {
        if (!arg.success) {
            alert(arg.message);
            return;
        }

        var pageSize = parseInt(this.pageSize, 10);
        var pageIndex = parseInt(this.pageIndex, 10);
        var id = (pageSize * pageIndex) + 1;
        for (var i = 0, ii = arg.records.length; i < ii; i++) {
            arg.records[i].No = id + i;
        }

        this.totalRecords = arg.totalRecords;
        return arg.records;
    },
    date2text: function (dt) {
        if (dt) { return C.parseIsoDateTime(dt).format('d-mmm-yyyy'); }
        else { return ''; }
    },
    fetchPage: function (postData) {
        this.pageIndex = postData.pageIndex;
        this.pageSize = postData.pageSize;
        this.postData = postData;
        this.fetch({ reset: true });
    },
    onResetOrChange: function () {
        var idx = this.pageIndex * this.pageSize + 1;
        for (var i = 0, ii = this.length; i < ii; i++) {
            this.at(i).onChange();
        }
        var toInject = {
            total: Math.ceil(this.totalRecords / this.pageSize),
            page: this.pageIndex + 1,
            records: this.totalRecords,
            rows: this.toJSON()
        };
        try {
            this.trigger('CQD.PageReady', toInject);
        } catch (ex) { }
    }
});