﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace CFS.Web
{
    public class GlobalClass
    {
        public static String routesApi(string controller, string action, string param)
        {
            string apiServer = ConfigurationManager.AppSettings["cosServer"];

            string url = string.Format("{0}/{1}/{2}?{3}", apiServer, controller, action, param);
            return url;
        }

        public static String parkingRoutesApi(string controller, string action, string param)
        {
            string parkingServer = ConfigurationManager.AppSettings["parkingApiServer"];

            string url = string.Format("{0}/{1}/{2}?{3}", parkingServer, controller, action, param);
            return url;
        }
    }
}