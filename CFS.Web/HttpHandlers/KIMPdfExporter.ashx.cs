﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.Formula;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.Web.HttpHandlers
{
    /// <summary>
    /// Summary description for KIMPdfExporter
    /// </summary>
    public class KIMPdfExporter : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int id = int.Parse(context.Request.QueryString["id"]);
            CFS.Models.Driver toRender = CFS.Models.Driver.FindById(id);
            CFS.Models.PDF.KIMPdfRenderer tDriver = new CFS.Models.PDF.KIMPdfRenderer();

            byte[] pdf = tDriver.Render(toRender);

            context.Response.ContentType = "application/pdf";
            context.Response.AddHeader("Content-Length", pdf.Length.ToString());
            context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"KIM_{0}.pdf\"", toRender.BadgeNumber));
            context.Response.BinaryWrite(pdf);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}