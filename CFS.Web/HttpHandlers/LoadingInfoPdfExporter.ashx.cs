﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.Formula;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.Web.HttpHandlers
{
    /// <summary>
    /// Summary description for LoadingInfoPdfExporter
    /// </summary>
    public class LoadingInfoPdfExporter : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int id = int.Parse(context.Request.QueryString["id"]);
            CFS.Models.TruckLoading toRender = CFS.Models.TruckLoading.FindById(id);
            CFS.Models.PDF.LoadingInfoPdfRenderer tTruckLoading = new CFS.Models.PDF.LoadingInfoPdfRenderer();

            byte[] pdf = tTruckLoading.Render(toRender);

            context.Response.ContentType = "application/pdf";
            context.Response.AddHeader("Content-Length", pdf.Length.ToString());
            context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"Sticker_Pendaftaran_{0}.pdf\"", toRender.DeliveryOrderNumber));
            context.Response.BinaryWrite(pdf);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}