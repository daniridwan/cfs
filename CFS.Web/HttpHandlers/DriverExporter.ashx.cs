﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.Formula;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.Web.HttpHandlers
{
    public class DriverExporter : IHttpHandler
    {
        private Dictionary<int, ICellStyle> cellStyleLookup;
        private HSSFWorkbook hssfworkbook;

        public void ProcessRequest(HttpContext context)
        {
            PagingQuery pq = ExtractQuery();
            pq.PageSize = 0;
            PagingResult pr = CFS.Models.Driver.SelectPaging(pq);

            byte[] result = RenderExcel(pr.Records, pq);
            context.Response.ContentType = "application/vnd.ms-excel";
            context.Response.AddHeader("Content-Length", result.Length.ToString());
            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"Driver.xls\"");
            context.Response.BinaryWrite(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private PagingQuery ExtractQuery()
        {
            HttpContext Context = HttpContext.Current;
            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in Context.Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Context.Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Context.Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Context.Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Context.Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Context.Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;
            return pq;
        }

        private HSSFWorkbook LoadTemplate(string path)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            hssfworkbook = new HSSFWorkbook(file);
            return hssfworkbook;
        }

        private ICell CreateCell(IRow row, int cellIndex)
        {
            ICell cell = row.GetCell(cellIndex);
            if (cell == null)
            {
                cell = row.CreateCell(cellIndex);
            }
            if (cellStyleLookup.ContainsKey(cellIndex))
            {
                ICellStyle originalstyle = cellStyleLookup[cellIndex];
                cell.CellStyle = originalstyle;
            }
            return cell;
        }

        private Dictionary<int, ICellStyle> BuildRowStyleLookup(IRow row, int startCellIndex, int count)
        {
            Dictionary<int, ICellStyle> colStyleMap = new Dictionary<int, ICellStyle>();

            for (int i = 0, cellIndex = startCellIndex; i < count; i++, cellIndex++)
            {
                ICell cell = row.GetCell(cellIndex);
                if (cell != null)
                {
                    ICellStyle style = cell.CellStyle;
                    if (style != null)
                    {
                        colStyleMap.Add(cellIndex, style);
                    }
                }
            }
            return colStyleMap;
        }

        public byte[] RenderExcel(IList records, PagingQuery pq)
        {
            HSSFWorkbook hssfworkbook = LoadTemplate(HttpContext.Current.Server.MapPath("~/Content/Template/Driver.xls"));
            ISheet sheet = hssfworkbook.GetSheet("Driver");

            cellStyleLookup = BuildRowStyleLookup(sheet.GetRow(9), 0, 20); // 9 start isi data dari query

            int current_row = 9;
            int index_row = 1;
            string now = DateTime.Now.ToString();

            //export timestamp
            sheet.GetRow(5).GetCell(2).SetCellValue(DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss"));

            //filter
            string fltKIMNumber = "";
            if (pq.SearchParam["fltKIMNumber"] != null && pq.SearchParam["fltKIMNumber"].Length > 0)
            {
                fltKIMNumber = (pq.SearchParam["fltKIMNumber"]);
            }

            string fltName = "";
            if (pq.SearchParam["fltName"] != null && pq.SearchParam["fltName"].Length > 0)
            {
                fltName = (pq.SearchParam["fltName"]);
            }

            string fltKTPNumber = "";
            if (pq.SearchParam["fltKTPNumber"] != null && pq.SearchParam["fltKTPNumber"].Length > 0)
            {
                fltKTPNumber = (pq.SearchParam["fltKTPNumber"]);
            }

            string fltCompanyName = "";
            if (pq.SearchParam["fltCompanyName"] != null && pq.SearchParam["fltCompanyName"].Length > 0)
            {
                fltCompanyName = (pq.SearchParam["fltCompanyName"]);
            }

            sheet.GetRow(5).GetCell(5).SetCellValue(fltKIMNumber);
            sheet.GetRow(6).GetCell(5).SetCellValue(fltName);
            sheet.GetRow(5).GetCell(8).SetCellValue(fltKTPNumber);

            if (records.Count != 0)
            {
                sheet.ShiftRows(9, sheet.LastRowNum, records.Count);
            }

            foreach (CFS.Models.Driver record in records)
            {
                IRow row = sheet.GetRow(current_row);
                if (row == null)
                {
                    row = sheet.CreateRow(current_row);
                }

                string dateOfBirth = record.DateOfBirth.HasValue ? record.DateOfBirth.Value.ToString("dd MMMM yyyy") : "";
                string driverSimExpiryDate = record.DriverSimExpiryDate.HasValue ? record.DriverSimExpiryDate.Value.ToString("dd MMMM yyyy") : "";
                string driverKimExpiryDate = record.DriverKimExpiryDate.HasValue ? record.DriverKimExpiryDate.Value.ToString("dd MMMM yyyy") : "";

                CreateCell(row, 0).SetCellValue(index_row);
                CreateCell(row, 1).SetCellValue(record.BadgeNumber);
                CreateCell(row, 2).SetCellValue(record.Name);
                CreateCell(row, 3).SetCellValue(record.PlaceOfBirth);
                CreateCell(row, 4).SetCellValue(dateOfBirth);
                CreateCell(row, 5).SetCellValue(record.DriverKtpNumber);
                CreateCell(row, 6).SetCellValue(record.DriverSimNumber);
                CreateCell(row, 7).SetCellValue(record.DriverSimType);
                CreateCell(row, 8).SetCellValue(driverSimExpiryDate);
                CreateCell(row, 9).SetCellValue(driverKimExpiryDate);
                CreateCell(row, 10).SetCellValue(record.DriverCompany);
                CreateCell(row, 11).SetCellValue(record.DriverAddress);
                CreateCell(row, 12).SetCellValue(record.DriverPhoneNumber);
                CreateCell(row, 13).SetCellValue(record.DriverMobilePhoneNumber);
                CreateCell(row, 14).SetCellValue(record.HasPhoto.ToString());

                current_row = current_row + 1;
                index_row = index_row + 1;
            }

            sheet.ForceFormulaRecalculation = true;
            MemoryStream ms = new MemoryStream();
            hssfworkbook.Write(ms);
            return ms.ToArray();
        }
    }
}