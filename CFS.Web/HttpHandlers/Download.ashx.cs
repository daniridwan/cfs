﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using CFS.Models;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;

namespace CFS.Web.HttpHandlers
{
    public class Download : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string filename = null;
            string mimeType = null;
            byte[] toStream = null;
            
            if(FileId != string.Empty)
            {
                toStream = LoadById(FileId, out filename, out mimeType);
            }
            else if(FileTicket != string.Empty)
            {
                toStream = LoadByTicket(FileTicket, out filename, out mimeType);
            }

            //filename = context.Server.HtmlEncode(filename);

            if (toStream != null)
            {
                context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                context.Response.ContentType = mimeType;
                context.Response.AddHeader("Content-Length", toStream.Length.ToString());
                context.Response.BinaryWrite(toStream);
            }
            else
            {
                context.Response.StatusCode = 404;
                context.Response.Write("Not Found");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private string FileId
        {
            get
            {
                if (HttpContext.Current.Request.QueryString["id"] != null)
                    return HttpContext.Current.Request.QueryString["id"];
                else
                    return string.Empty;
            }
        }

        private string FileTicket
        {
            get
            {
                if (HttpContext.Current.Request.QueryString["ticket"] != null)
                {
                    return HttpContext.Current.Request.QueryString["ticket"];
                }
                else
                    return string.Empty;
            }
        }

        private string FileName
        {
            get
            {
                if (HttpContext.Current.Request.QueryString["filename"] != null)
                {
                    return HttpContext.Current.Request.QueryString["filename"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        private void SendResponse(UploaderResponse ur)
        {
            HttpContext context = HttpContext.Current;
            context.Response.Clear();
            context.Response.ContentType = "text/html";

            string toSend = "<html><body>" + JsonConvert.SerializeObject(ur) + "</body></html>";
            context.Response.Write(toSend);
            context.Response.Flush();
        }

        private byte[] LoadById(string id, out string filename, out string mimeType)
        {
            filename = "";
            mimeType = "application/octet-stream";
            try
            {
                CFS.Models.VesselDocument f = CFS.Models.VesselDocument.FindById(Convert.ToInt64(id));

                if (f != null)
                {
                    filename = f.Filename;
                    
                    string repositoryPath = System.Web.Configuration.WebConfigurationManager.AppSettings["RepositoryPath"];
                    //failsafe
                    //repositoryPath = repositoryPath + "\\" + channelcode + "\\" + descType;

                    if (repositoryPath == null || repositoryPath.Length == 0)
                    {
                        repositoryPath = HttpContext.Current.Server.MapPath("~/__tmpUpload/");
                    }

                    string filePath = Path.Combine(repositoryPath, f.Filename);
                    if (File.Exists(filePath))
                    {
                        return File.ReadAllBytes(filePath);
                    }
                    else
                    {
                        return null;
                    }
                }
                else return null;
            }
            catch (Exception exc)
            {
                return null;
            }
        }

        private byte[] LoadByTicket(string ticket, out string filename, out string mimeType)
        {
            filename = FileName;
            mimeType = "application/octet-stream";
            try
            {
                string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/"), "__tmpUpload/");
                filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/"), "__tmpUpload\\");
                filePath = Path.Combine(filePath, ticket);
                if (File.Exists(filePath))
                {
                    return File.ReadAllBytes(filePath);
                }
                else
                {
                    return null;
                }
            }
            catch(Exception exc)
            {
                return null;
            }
        }

    }
}