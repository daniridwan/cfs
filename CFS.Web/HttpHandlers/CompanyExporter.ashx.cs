﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.Formula;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.Web.HttpHandlers
{
    public class CompanyExporter : IHttpHandler
    {
        private Dictionary<int, ICellStyle> cellStyleLookup;
        private HSSFWorkbook hssfworkbook;

        public void ProcessRequest(HttpContext context)
        {
            PagingQuery pq = ExtractQuery();
            pq.PageSize = 0;
            PagingResult pr = CFS.Models.Company.SelectPaging(pq);

            byte[] result = RenderExcel(pr.Records, pq);
            context.Response.ContentType = "application/vnd.ms-excel";
            context.Response.AddHeader("Content-Length", result.Length.ToString());
            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"Company.xls\"");
            context.Response.BinaryWrite(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private PagingQuery ExtractQuery()
        {
            HttpContext Context = HttpContext.Current;
            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in Context.Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Context.Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Context.Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Context.Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Context.Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Context.Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;
            return pq;
        }

        private HSSFWorkbook LoadTemplate(string path)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            hssfworkbook = new HSSFWorkbook(file);
            return hssfworkbook;
        }

        private ICell CreateCell(IRow row, int cellIndex)
        {
            ICell cell = row.GetCell(cellIndex);
            if (cell == null)
            {
                cell = row.CreateCell(cellIndex);
            }
            if (cellStyleLookup.ContainsKey(cellIndex))
            {
                ICellStyle originalstyle = cellStyleLookup[cellIndex];
                cell.CellStyle = originalstyle;
            }
            return cell;
        }

        private Dictionary<int, ICellStyle> BuildRowStyleLookup(IRow row, int startCellIndex, int count)
        {
            Dictionary<int, ICellStyle> colStyleMap = new Dictionary<int, ICellStyle>();

            for (int i = 0, cellIndex = startCellIndex; i < count; i++, cellIndex++)
            {
                ICell cell = row.GetCell(cellIndex);
                if (cell != null)
                {
                    ICellStyle style = cell.CellStyle;
                    if (style != null)
                    {
                        colStyleMap.Add(cellIndex, style);
                    }
                }
            }
            return colStyleMap;
        }

        public byte[] RenderExcel(IList records, PagingQuery pq)
        {
            HSSFWorkbook hssfworkbook = LoadTemplate(HttpContext.Current.Server.MapPath("~/Content/Template/Company.xls"));
            ISheet sheet = hssfworkbook.GetSheet("Company");

            cellStyleLookup = BuildRowStyleLookup(sheet.GetRow(9), 0, 20); // 9 start isi data dari query

            int current_row = 9;
            int index_row = 1;
            string now = DateTime.Now.ToString();

            //export timestamp
            sheet.GetRow(5).GetCell(2).SetCellValue(DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss"));

            //filter
            string fltCompanyName = "";
            if (pq.SearchParam["fltCompanyName"] != null && pq.SearchParam["fltCompanyName"].Length > 0)
            {
                fltCompanyName = (pq.SearchParam["fltCompanyName"]);
            }

            sheet.GetRow(5).GetCell(5).SetCellValue(fltCompanyName);

            if (records.Count != 0)
            {
                sheet.ShiftRows(9, sheet.LastRowNum, records.Count);
            }

            foreach (CFS.Models.Company record in records)
            {
                IRow row = sheet.GetRow(current_row);
                if (row == null)
                {
                    row = sheet.CreateRow(current_row);
                }

                CreateCell(row, 0).SetCellValue(index_row);
                CreateCell(row, 1).SetCellValue(record.CompanyName);
                CreateCell(row, 2).SetCellValue(record.Address);
                CreateCell(row, 3).SetCellValue(record.Email);
                CreateCell(row, 4).SetCellValue(record.PhoneNumber);
                
                current_row = current_row + 1;
                index_row = index_row + 1;
            }

            sheet.ForceFormulaRecalculation = true;
            MemoryStream ms = new MemoryStream();
            hssfworkbook.Write(ms);
            return ms.ToArray();
        }
    }
}