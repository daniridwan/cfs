﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.Formula;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.Web.HttpHandlers
{
    public class DeliveryOrderRealizationExporter : IHttpHandler
    {
        private Dictionary<int, ICellStyle> cellStyleLookup;
        private HSSFWorkbook hssfworkbook;

        public void ProcessRequest(HttpContext context)
        {
            PagingQuery pq = ExtractQuery();
            pq.PageSize = 0;
            PagingResult pr = CFS.Models.ViewDeliveryOrderRealization.SelectPaging(pq);

            byte[] result = RenderExcel(pr.Records, pq);
            context.Response.ContentType = "application/vnd.ms-excel";
            context.Response.AddHeader("Content-Length", result.Length.ToString());
            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"DORealization.xls\"");
            context.Response.BinaryWrite(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private PagingQuery ExtractQuery()
        {
            HttpContext Context = HttpContext.Current;
            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in Context.Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Context.Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Context.Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Context.Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Context.Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Context.Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;
            return pq;
        }

        private HSSFWorkbook LoadTemplate(string path)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            hssfworkbook = new HSSFWorkbook(file);
            return hssfworkbook;
        }

        private ICell CreateCell(IRow row, int cellIndex)
        {
            ICell cell = row.GetCell(cellIndex);
            if (cell == null)
            {
                cell = row.CreateCell(cellIndex);
            }
            if (cellStyleLookup.ContainsKey(cellIndex))
            {
                ICellStyle originalstyle = cellStyleLookup[cellIndex];
                cell.CellStyle = originalstyle;
            }
            return cell;
        }

        private Dictionary<int, ICellStyle> BuildRowStyleLookup(IRow row, int startCellIndex, int count)
        {
            Dictionary<int, ICellStyle> colStyleMap = new Dictionary<int, ICellStyle>();

            for (int i = 0, cellIndex = startCellIndex; i < count; i++, cellIndex++)
            {
                ICell cell = row.GetCell(cellIndex);
                if (cell != null)
                {
                    ICellStyle style = cell.CellStyle;
                    if (style != null)
                    {
                        colStyleMap.Add(cellIndex, style);
                    }
                }
            }
            return colStyleMap;
        }

        public byte[] RenderExcel(IList records, PagingQuery pq)
        {
            HSSFWorkbook hssfworkbook = LoadTemplate(HttpContext.Current.Server.MapPath("~/Content/Template/DORealization.xls"));
            ISheet sheet = hssfworkbook.GetSheet("Delivery Order Realization");

            cellStyleLookup = BuildRowStyleLookup(sheet.GetRow(9), 0, 20); // 9 start isi data dari query

            int current_row = 9;
            int index_row = 1;
            string now = DateTime.Now.ToString();

            //export timestamp
            sheet.GetRow(5).GetCell(2).SetCellValue(DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss"));

            //filter
            string fltRegistrationPlate = "";
            if (pq.SearchParam["fltRegistrationPlate"] != null && pq.SearchParam["fltRegistrationPlate"].Length > 0)
            {
                fltRegistrationPlate = (pq.SearchParam["fltRegistrationPlate"]);
            }

            string fltDeliveryOrderNumber = "";
            if (pq.SearchParam["fltDeliveryOrderNumber"] != null && pq.SearchParam["fltDeliveryOrderNumber"].Length > 0)
            {
                fltDeliveryOrderNumber = (pq.SearchParam["fltDeliveryOrderNumber"]);
            }

            string fltProductName = "";
            if (pq.SearchParam["fltProductName"] != null && pq.SearchParam["fltProductName"].Length > 0)
            {
                fltProductName = (pq.SearchParam["fltProductName"]);
            }

            string fltCompanyName = "";
            if (pq.SearchParam["fltCompanyName"] != null && pq.SearchParam["fltCompanyName"].Length > 0)
            {
                fltCompanyName = (pq.SearchParam["fltCompanyName"]);
            }

            string fltTimestampFrom = "";
            if (pq.SearchParam["fltTimestampFrom"] != null && pq.SearchParam["fltTimestampFrom"].Length > 0)
            {
                fltTimestampFrom = (pq.SearchParam["fltTimestampFrom"]);
            }

            string fltTimestampTo = "";
            if (pq.SearchParam["fltTimestampTo"] != null && pq.SearchParam["fltTimestampTo"].Length > 0)
            {
                fltTimestampTo = (pq.SearchParam["fltTimestampTo"]);
            }

            string fltFillingPointGroup = "";
            if (pq.SearchParam["fltFillingPointGroup"] != null && pq.SearchParam["fltFillingPointGroup"].Length > 0)
            {
                fltFillingPointGroup = (pq.SearchParam["fltFillingPointGroup"]);
            }

            string fltBlNo = "";
            if (pq.SearchParam["fltBlNo"] != null && pq.SearchParam["fltBlNo"].Length > 0)
            {
                fltBlNo = (pq.SearchParam["fltBlNo"]);
            }

            sheet.GetRow(5).GetCell(5).SetCellValue(fltRegistrationPlate);
            sheet.GetRow(6).GetCell(5).SetCellValue(fltDeliveryOrderNumber);
            sheet.GetRow(5).GetCell(8).SetCellValue(fltProductName);
            sheet.GetRow(6).GetCell(8).SetCellValue(fltCompanyName);
            sheet.GetRow(5).GetCell(12).SetCellValue(fltFillingPointGroup);
            sheet.GetRow(6).GetCell(12).SetCellValue(fltBlNo);
            string fltTimestampFrom1 = fltTimestampFrom.Replace("T", " ");
            string fltTimestampTo1 = fltTimestampTo.Replace("T", " ");
            sheet.GetRow(5).GetCell(15).SetCellValue(fltTimestampFrom1);
            sheet.GetRow(6).GetCell(15).SetCellValue(fltTimestampTo1);

            if (records.Count != 0)
            {
                sheet.ShiftRows(9, sheet.LastRowNum, records.Count);
            }

            foreach (CFS.Models.ViewDeliveryOrderRealization record in records)
            {
                IRow row = sheet.GetRow(current_row);
                if (row == null)
                {
                    row = sheet.CreateRow(current_row);
                }
                string admTimestamp = record.AdministrationTimestamp.HasValue ? record.AdministrationTimestamp.Value.ToString("dd MMMM yyyy HH:mm:ss") : "";
                string gateInTimestamp = record.GateInTimestamp.HasValue ? record.GateInTimestamp.Value.ToString("dd MMMM yyyy HH:mm:ss") : "";
                string gateOutTimestamp = record.GateOutTimestamp.HasValue ? record.GateOutTimestamp.Value.ToString("dd MMMM yyyy HH:mm:ss") : "";
                string goodIssueTimestamp = record.GoodIssueTimestamp.HasValue ? record.GoodIssueTimestamp.Value.ToString("dd MMMM yyyy HH:mm:ss") : "";
                TimeSpan? dwellingTime = record.GateOutTimestamp - record.GateInTimestamp;
                TimeSpan? queueTime = record.GateInTimestamp - record.AdministrationTimestamp;
                double pumpingTime = 0;
                TimeSpan pumpingTimeSpan = new TimeSpan(0, 0, 0);
                if(record.FillingPointGroup == "Chemical")
                {
                    //pumpingTime = (record.NetWeight/1000) * 1.2; //1.2 didapat dari 60/50 ,50 default pompa
                    pumpingTime = ((record.NetWeight / 1000)/record.ProductDensity) * (60 / record.Flowrate);
                    string s = pumpingTime.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
                    string[] parts = s.Split('.');
                    int i1 = int.Parse(parts[0]);
                    int i2 = int.Parse(parts[1]);
                    pumpingTimeSpan = new TimeSpan(0, i1, (i2*60)/100);
                }
                else
                {
                    //pumpingTime = record.Actual * 1.2; 
                    pumpingTime = (record.Actual/record.ProductDensity) * (60 / record.Flowrate);
                    string s = pumpingTime.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
                    string[] parts = s.Split('.');
                    int i1 = int.Parse(parts[0]); //9
                    int i2 = int.Parse(parts[1]); //60
                    pumpingTimeSpan = new TimeSpan(0, i1, (i2*60)/100);
                }

                CreateCell(row, 0).SetCellValue(index_row);
                CreateCell(row, 1).SetCellValue(record.RegistrationPlate);
                CreateCell(row, 2).SetCellValue(record.TankSafeCapacity);
                CreateCell(row, 3).SetCellValue(record.DeliveryOrderNumber);
                CreateCell(row, 4).SetCellValue(record.TransactionNumber);
                CreateCell(row, 5).SetCellValue(record.CustomerReferenceNumber);
                CreateCell(row, 6).SetCellValue(record.ProductName);
                CreateCell(row, 7).SetCellValue(record.CompanyName);
                CreateCell(row, 8).SetCellValue(record.Actual);
                CreateCell(row, 9).SetCellValue(record.UOM);
                CreateCell(row, 10).SetCellValue(record.FlowmeterQuantity);
                CreateCell(row, 11).SetCellValue(record.CorrectionFactor);
                CreateCell(row, 12).SetCellValue(record.Actual2);
                CreateCell(row, 13).SetCellValue(record.InitialWeight);
                CreateCell(row, 14).SetCellValue(record.FinalWeight);
                CreateCell(row, 15).SetCellValue(record.NetWeight);
                CreateCell(row, 16).SetCellValue(record.TankName);
                CreateCell(row, 17).SetCellValue(record.FillingPointName);
                CreateCell(row, 18).SetCellValue(record.FillingPointGroup);
                CreateCell(row, 19).SetCellValue(record.VesselNo.ToString());
                CreateCell(row, 20).SetCellValue(record.BlNo.ToString());
                CreateCell(row, 21).SetCellValue(admTimestamp);
                CreateCell(row, 22).SetCellValue(gateInTimestamp);
                CreateCell(row, 23).SetCellValue(gateOutTimestamp);
                CreateCell(row, 24).SetCellValue(goodIssueTimestamp);
                CreateCell(row, 25).SetCellValue(dwellingTime.ToString());
                CreateCell(row, 26).SetCellValue(queueTime.ToString());
                CreateCell(row, 27).SetCellValue(pumpingTimeSpan.ToString());
                CreateCell(row, 28).SetCellValue((dwellingTime - pumpingTimeSpan).ToString());
                CreateCell(row, 29).SetCellValue(record.AdministrationUser);
                CreateCell(row, 30).SetCellValue(record.LoadedUser);
                CreateCell(row, 31).SetCellValue(record.LoadedCFSUser);
                CreateCell(row, 32).SetCellValue(record.PumpName);
                CreateCell(row, 33).SetCellValue(record.Flowrate);
                CreateCell(row, 34).SetCellValue(record.Remarks);
                current_row = current_row + 1;
                index_row = index_row + 1;
            }

            sheet.ForceFormulaRecalculation = true;
            MemoryStream ms = new MemoryStream();
            hssfworkbook.Write(ms);
            return ms.ToArray();
        }
    }
}