﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.IO;
using Newtonsoft.Json;
using log4net;

namespace CFS.Web.HttpHandlers
{
    [JsonObject]
    public class UploadInfo
    {
        [JsonProperty("filename")]
        public string Filename { get; set; }
        [JsonProperty("ticket")]
        public string Ticket { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("size")]
        public long Size { get; set; }
    }

    [JsonObject]
    public class UploaderResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("register")]
        public ArrayList Register { get; set; }

        public UploaderResponse()
        {
            Success = false;
            Message = "";
            Register = new ArrayList();
        }
    }

    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    public class UploadHandler : IHttpHandler, IRequiresSessionState
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UploadHandler));

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            string response = string.Empty;
            UploaderResponse ur = new UploaderResponse();

            if (!context.Request.IsAuthenticated)
            {
                //ur.Success = false;
                //ur.Message = "Not Authenticated";
                //SendResponse(ur);
                //return;
            }

            try
            {
                //check di current registration
                Hashtable fileReg = (Hashtable)context.Session["DMSBufferedUploader"];
                if (fileReg == null)
                {
                    fileReg = new Hashtable();
                    context.Session["DMSBufferedUploader"] = fileReg;
                }

                string tempFolder = Path.Combine(HttpContext.Current.Server.MapPath("~"), "__tmpUpload");
                log.Debug(string.Format("define upload folder : {0}", tempFolder));
                if (!Directory.Exists(tempFolder))
                    Directory.CreateDirectory(tempFolder);

                //deteksi file ada apa aja (bisa multiple) di request
                //save to temporary folder
                HttpFileCollection uploads = context.Request.Files;
                log.Debug(string.Format("got {0} files", uploads.Count));
                for (int i = 0; i < uploads.Count; i++)
                {
                    HttpPostedFile theFile = uploads[i];
                    if (theFile.ContentLength == 0)
                    {
                        log.Debug("empty posted file");
                        ur.Message = "Cannot receive empty file";
                        continue;
                    }

                    //string description = FindDescription(theFile.FileName);
                    string description = string.Empty;

                    //apakah ada file bernama sama persis (with all path)
                    //no detection, kalo ada multiple same name akan diupload lagi
                    //(dalam satu session upload, user bisa mengupload bbrp file, dan bisa jadi namanya sama)
                    //UploadInfo prev = CheckExisting(fileReg, theFile.FileName);
                    
                    // seems unreliable, bypass CheckExisting

                    UploadInfo prev = null;
                    if (prev != null)
                    {
                        //otomatis lsg tumpuk, guard dilakukan di client side
                        string dest = Path.Combine(tempFolder, prev.Ticket);
                        theFile.SaveAs(dest);
                        ur.Message = "File uploaded successfully";
                        ur.Success = true;
                        ur.Register.Add(prev);
                    }
                    else
                    {
                        string ticket = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                        string dest = Path.Combine(tempFolder, ticket);

                        log.Debug(string.Format("saving {0}", theFile.FileName));
                        theFile.SaveAs(dest);

                        //register GUID name to original file name
                        UploadInfo ui = new UploadInfo();
                        ui.Filename = Path.GetFileName(theFile.FileName);
                        ui.Ticket = ticket;
                        ui.Description = description;
                        ui.Size = theFile.InputStream.Length;
                        
                        fileReg[ticket] = ui;
                        ur.Register.Add(ui);
                        ur.Message = "File uploaded successfully";
                        ur.Success = true;
                    }
                }
            }
            catch (Exception exc)
            {
                ur.Success = false;
                ur.Message = exc.Message;
            }
            SendResponse(ur);

        }

        public UploadInfo CheckExisting(Hashtable coll, string val)
        {
            foreach (DictionaryEntry de in coll)
            {
                UploadInfo ui = (UploadInfo)de.Value;
                if (ui != null)
                {
                    if (ui.Filename == val)
                        return ui;
                }
            }
            return null;
        }

        private void SendResponse(UploaderResponse ur)
        {
            HttpContext context = HttpContext.Current;
            context.Response.Clear();
            context.Response.ContentType = "text/html";

            string toSend = "<html><body>" + JsonConvert.SerializeObject(ur) + "</body></html>";
            context.Response.Write(toSend);
            context.Response.Flush();

        }



    }
}
