﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.Formula;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.Web.HttpHandlers
{
    public class DeliveryOrderRealizationSummaryExporter : IHttpHandler
    {
        private Dictionary<int, ICellStyle> cellStyleLookup;
        private HSSFWorkbook hssfworkbook;

        public void ProcessRequest(HttpContext context)
        {
            PagingQuery pq = ExtractQuery();
            pq.PageSize = 0;
            PagingResult pr = CFS.Models.ViewDeliveryOrderRealizationSummary.SelectPaging(pq);

            byte[] result = RenderExcel(pr.Records, pq);
            context.Response.ContentType = "application/vnd.ms-excel";
            context.Response.AddHeader("Content-Length", result.Length.ToString());
            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"DORealization.xls\"");
            context.Response.BinaryWrite(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private PagingQuery ExtractQuery()
        {
            HttpContext Context = HttpContext.Current;
            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in Context.Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Context.Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Context.Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Context.Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Context.Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Context.Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;
            return pq;
        }

        private HSSFWorkbook LoadTemplate(string path)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            hssfworkbook = new HSSFWorkbook(file);
            return hssfworkbook;
        }

        private ICell CreateCell(IRow row, int cellIndex)
        {
            ICell cell = row.GetCell(cellIndex);
            if (cell == null)
            {
                cell = row.CreateCell(cellIndex);
            }
            if (cellStyleLookup.ContainsKey(cellIndex))
            {
                ICellStyle originalstyle = cellStyleLookup[cellIndex];
                cell.CellStyle = originalstyle;
            }
            return cell;
        }

        private Dictionary<int, ICellStyle> BuildRowStyleLookup(IRow row, int startCellIndex, int count)
        {
            Dictionary<int, ICellStyle> colStyleMap = new Dictionary<int, ICellStyle>();

            for (int i = 0, cellIndex = startCellIndex; i < count; i++, cellIndex++)
            {
                ICell cell = row.GetCell(cellIndex);
                if (cell != null)
                {
                    ICellStyle style = cell.CellStyle;
                    if (style != null)
                    {
                        colStyleMap.Add(cellIndex, style);
                    }
                }
            }
            return colStyleMap;
        }

        public byte[] RenderExcel(IList records, PagingQuery pq)
        {
            HSSFWorkbook hssfworkbook = LoadTemplate(HttpContext.Current.Server.MapPath("~/Content/Template/DORealizationSummary.xls"));
            ISheet sheet = hssfworkbook.GetSheet("Delivery Order Realization");

            cellStyleLookup = BuildRowStyleLookup(sheet.GetRow(11), 0, 20); // 9 start isi data dari query

            int current_row = 11;
            int index_row = 1;
            string now = DateTime.Now.ToString();

            //export timestamp
            sheet.GetRow(5).GetCell(2).SetCellValue(DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss"));

            //filter
            string fltRegistrationPlate = "";
            if (pq.SearchParam["fltRegistrationPlate"] != null && pq.SearchParam["fltRegistrationPlate"].Length > 0)
            {
                fltRegistrationPlate = (pq.SearchParam["fltRegistrationPlate"]);
            }

            string fltDeliveryOrderNumber = "";
            if (pq.SearchParam["fltDeliveryOrderNumber"] != null && pq.SearchParam["fltDeliveryOrderNumber"].Length > 0)
            {
                fltDeliveryOrderNumber = (pq.SearchParam["fltDeliveryOrderNumber"]);
            }

            string fltProductName = "";
            if (pq.SearchParam["fltProductName"] != null && pq.SearchParam["fltProductName"].Length > 0)
            {
                fltProductName = (pq.SearchParam["fltProductName"]);
            }

            string fltCompanyName = "";
            if (pq.SearchParam["fltCompanyName"] != null && pq.SearchParam["fltCompanyName"].Length > 0)
            {
                fltCompanyName = (pq.SearchParam["fltCompanyName"]);
            }

            string fltTimestampFrom = "";
            if (pq.SearchParam["fltTimestampFrom"] != null && pq.SearchParam["fltTimestampFrom"].Length > 0)
            {
                fltTimestampFrom = (pq.SearchParam["fltTimestampFrom"]);
            }

            string fltTimestampTo = "";
            if (pq.SearchParam["fltTimestampTo"] != null && pq.SearchParam["fltTimestampTo"].Length > 0)
            {
                fltTimestampTo = (pq.SearchParam["fltTimestampTo"]);
            }

            string fltFillingPointGroup = "";
            if (pq.SearchParam["fltFillingPointGroup"] != null && pq.SearchParam["fltFillingPointGroup"].Length > 0)
            {
                fltFillingPointGroup = (pq.SearchParam["fltFillingPointGroup"]);
            }

            string fltBlNo = "";
            if (pq.SearchParam["fltBlNo"] != null && pq.SearchParam["fltBlNo"].Length > 0)
            {
                fltBlNo = (pq.SearchParam["fltBlNo"]);
            }

            sheet.GetRow(5).GetCell(5).SetCellValue(fltRegistrationPlate);
            sheet.GetRow(6).GetCell(5).SetCellValue(fltDeliveryOrderNumber);
            sheet.GetRow(5).GetCell(8).SetCellValue(fltProductName);
            sheet.GetRow(6).GetCell(8).SetCellValue(fltCompanyName);
            sheet.GetRow(5).GetCell(12).SetCellValue(fltFillingPointGroup);
            sheet.GetRow(6).GetCell(12).SetCellValue(fltBlNo);
            string fltTimestampFrom1 = fltTimestampFrom.Replace("T", " ");
            string fltTimestampTo1 = fltTimestampTo.Replace("T", " ");
            sheet.GetRow(5).GetCell(15).SetCellValue(fltTimestampFrom1);
            sheet.GetRow(6).GetCell(15).SetCellValue(fltTimestampTo1);

            if (records.Count != 0)
            {
                sheet.ShiftRows(11, sheet.LastRowNum, records.Count);
            }

            foreach (CFS.Models.ViewDeliveryOrderRealizationSummary record in records)
            {
                IRow row = sheet.GetRow(current_row);
                if (row == null)
                {
                    row = sheet.CreateRow(current_row);
                }
                string admTimestamp = record.AdministrationTimestamp.HasValue ? record.AdministrationTimestamp.Value.ToString("dd MMMM yyyy HH:mm:ss") : "";
                string gateInTimestamp = record.GateInTimestamp.HasValue ? record.GateInTimestamp.Value.ToString("dd MMMM yyyy HH:mm:ss") : "";
                string gateOutTimestamp = record.GateOutTimestamp.HasValue ? record.GateOutTimestamp.Value.ToString("dd MMMM yyyy HH:mm:ss") : "";
                string transactionDate = record.GateOutTimestamp.HasValue ? record.GateOutTimestamp.Value.ToString("dd MMMM yyyy") : "";
                //string goodIssueTimestamp = record.GoodIssueTimestamp.HasValue ? record.GoodIssueTimestamp.Value.ToString("dd MMMM yyyy HH:mm:ss") : "";
                TimeSpan? cfsTime = record.GateOutTimestamp - record.GateInTimestamp;
                TimeSpan? queueTime = record.GateInTimestamp - record.AdministrationTimestamp;
                TimeSpan? dwellingTime = record.GateOutTimestamp - record.AdministrationTimestamp;
                double pumpingTime = 0;
                TimeSpan pumpingTimeSpan = new TimeSpan(0, 0, 0);
                if(record.Unit == "Kg")
                {
                    TruckLoading tl = TruckLoading.FindByDeliveryOrdeNumber(record.DeliveryOrderNumber);
                    TruckLoadingPlanning[] tlp = TruckLoadingPlanning.FindByTruckLoadingId(tl.TruckLoadingId);
                    pumpingTimeSpan = new TimeSpan();
                    foreach (TruckLoadingPlanning x in tlp)
                    {
                        pumpingTime = (((x.FinalWeight-x.InitialWeight) / 1000) / x.ProductDensity) * (60 / x.Flowrate);
                        string s = pumpingTime.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
                        string[] parts = s.Split('.');
                        int i1 = int.Parse(parts[0]); //9
                        int i2 = int.Parse(parts[1]); //60
                        pumpingTimeSpan = pumpingTimeSpan + new TimeSpan(0, i1, (i2 * 60) / 100);
                    }
                }
                else
                {
                    TruckLoading tl = TruckLoading.FindByDeliveryOrdeNumber(record.DeliveryOrderNumber);
                    TruckLoadingPlanning[] tlp = TruckLoadingPlanning.FindByTruckLoadingId(tl.TruckLoadingId);
                    pumpingTimeSpan = new TimeSpan();
                    foreach (TruckLoadingPlanning x in tlp)
                    {
                        pumpingTime = ((x.Actual2 / 1000) / x.ProductDensity) * (60 / x.Flowrate);
                        string s = pumpingTime.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
                        string[] parts = s.Split('.');
                        int i1 = int.Parse(parts[0]); //9
                        int i2 = int.Parse(parts[1]); //60
                        pumpingTimeSpan = pumpingTimeSpan + new TimeSpan(0, i1, (i2 * 60) / 100);
                    }
                }

                CreateCell(row, 0).SetCellValue(index_row);
                CreateCell(row, 1).SetCellValue(transactionDate);
                CreateCell(row, 2).SetCellValue(record.RegistrationPlate);
                CreateCell(row, 3).SetCellValue(record.TankSafeCapacity);
                CreateCell(row, 4).SetCellValue(record.Unit);
                CreateCell(row, 5).SetCellValue(record.Product);
                CreateCell(row, 6).SetCellValue(record.Preset);
                CreateCell(row, 7).SetCellValue(record.ConfirmedQuantity);
                CreateCell(row, 8).SetCellValue(record.ConfirmedQuantity/record.Preset-1);
                CreateCell(row, 9).SetCellValue(record.Actual);
                CreateCell(row, 10).SetCellValue((record.Actual/record.ConfirmedQuantity-1)*100);
                CreateCell(row, 13).SetCellValue(record.PumpName);
                CreateCell(row, 14).SetCellValue(record.Flowrate);
                CreateCell(row, 15).SetCellValue(admTimestamp);
                CreateCell(row, 16).SetCellValue(gateInTimestamp);
                CreateCell(row, 17).SetCellValue(gateOutTimestamp);
                CreateCell(row, 18).SetCellValue(queueTime.ToString());
                CreateCell(row, 19).SetCellValue(cfsTime.ToString());
                CreateCell(row, 20).SetCellValue(pumpingTimeSpan.ToString()); //pumping
                CreateCell(row, 21).SetCellValue((cfsTime-pumpingTimeSpan).ToString()); //others
                CreateCell(row, 22).SetCellValue(dwellingTime.ToString()); //dwelling
                CreateCell(row, 23).SetCellValue(record.AdministrationUser);
                CreateCell(row, 24).SetCellValue(record.LoadedUser);
                CreateCell(row, 25).SetCellValue(record.LoadedCFSUser);
                CreateCell(row, 26).SetCellValue(record.DriverName);

                current_row = current_row + 1;
                index_row = index_row + 1;
            }

            sheet.ForceFormulaRecalculation = true;
            MemoryStream ms = new MemoryStream();
            hssfworkbook.Write(ms);
            return ms.ToArray();
        }
    }
}