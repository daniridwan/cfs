﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.Formula;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.Web.HttpHandlers
{
    public class BLMonitoringExporter : IHttpHandler
    {
        private Dictionary<int, ICellStyle> cellStyleLookup;
        private HSSFWorkbook hssfworkbook;

        public void ProcessRequest(HttpContext context)
        {
            PagingQuery pqReceiving = ExtractQuery();
            pqReceiving.PageSize = 0;
            PagingResult prReceiving = CFS.Models.VesselDocument.SelectPaging(pqReceiving);
            PagingResult prDelivery = CFS.Models.ViewDeliveryOrderRealization.SelectPaging(pqReceiving);

            byte[] result = RenderExcel(prReceiving.Records, pqReceiving, prDelivery.Records);
            context.Response.ContentType = "application/vnd.ms-excel";
            context.Response.AddHeader("Content-Length", result.Length.ToString());
            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"BL.xls\"");
            context.Response.BinaryWrite(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private PagingQuery ExtractQuery()
        {
            HttpContext Context = HttpContext.Current;
            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in Context.Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Context.Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Context.Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Context.Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Context.Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Context.Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;
            return pq;
        }

        private HSSFWorkbook LoadTemplate(string path)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            hssfworkbook = new HSSFWorkbook(file);
            return hssfworkbook;
        }

        private ICell CreateCell(IRow row, int cellIndex)
        {
            ICell cell = row.GetCell(cellIndex);
            if (cell == null)
            {
                cell = row.CreateCell(cellIndex);
            }
            if (cellStyleLookup.ContainsKey(cellIndex))
            {
                ICellStyle originalstyle = cellStyleLookup[cellIndex];
                cell.CellStyle = originalstyle;
            }
            return cell;
        }

        private Dictionary<int, ICellStyle> BuildRowStyleLookup(IRow row, int startCellIndex, int count)
        {
            Dictionary<int, ICellStyle> colStyleMap = new Dictionary<int, ICellStyle>();

            for (int i = 0, cellIndex = startCellIndex; i < count; i++, cellIndex++)
            {
                ICell cell = row.GetCell(cellIndex);
                if (cell != null)
                {
                    ICellStyle style = cell.CellStyle;
                    if (style != null)
                    {
                        colStyleMap.Add(cellIndex, style);
                    }
                }
            }
            return colStyleMap;
        }

        public byte[] RenderExcel(IList recordsReceiving, PagingQuery pq, IList recordsDelivery)
        {
            HSSFWorkbook hssfworkbook = LoadTemplate(HttpContext.Current.Server.MapPath("~/Content/Template/BL.xls"));
            ISheet sheet = hssfworkbook.GetSheet("Dok Penerimaan dan Pengeluaran");
            sheet.ProtectSheet("Redeco@)@)");

            cellStyleLookup = BuildRowStyleLookup(sheet.GetRow(9), 0, 20); // 9 start isi data dari query

            int current_row = 9;
            int index_row = 1;
            string now = DateTime.Now.ToString();

            //export timestamp
            sheet.GetRow(5).GetCell(6).SetCellValue(DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss"));

            //filter
            string fltBlNo = "";
            if (pq.SearchParam["fltBlNo"] != null && pq.SearchParam["fltBlNo"].Length > 0)
            {
                fltBlNo = (pq.SearchParam["fltBlNo"]);
            }

            sheet.GetRow(5).GetCell(3).SetCellValue(fltBlNo);

            if (recordsReceiving.Count != 0)
            {
                sheet.ShiftRows(9, sheet.LastRowNum, recordsReceiving.Count);
            }

            foreach (CFS.Models.VesselDocument record in recordsReceiving)
            {
                IRow row = sheet.GetRow(current_row);
                if (row == null)
                {
                    row = sheet.CreateRow(current_row);
                }

                CreateCell(row, 0).SetCellValue(index_row);
                CreateCell(row, 1).SetCellValue(record.Type);
                CreateCell(row, 2).SetCellValue(record.BlNo);
                CreateCell(row, 3).SetCellValue(record.PibNo);
                string pibDate = record.PibDate.HasValue ? record.PibDate.Value.ToString("dd MMMM yyyy") : "";
                string arrivalDate = record.ArrivalDate.HasValue ? record.ArrivalDate.Value.ToString("dd MMMM yyyy") : "";
                CreateCell(row, 4).SetCellValue(pibDate);
                CreateCell(row, 5).SetCellValue(arrivalDate);
                CreateCell(row, 6).SetCellValue(record.CompanyName);
                CreateCell(row, 7).SetCellValue(record.ProductName);
                CreateCell(row, 8).SetCellValue(record.BlQuantity);
                CreateCell(row, 9).SetCellValue(record.BlUom);

                current_row = current_row + 1;
                index_row = index_row + 1;
            }

            current_row = 13;
            index_row = 1;
            foreach (CFS.Models.ViewDeliveryOrderRealization record in recordsDelivery)
            {
                IRow row = sheet.GetRow(current_row);
                if (row == null)
                {
                    row = sheet.CreateRow(current_row);
                }

                CreateCell(row, 0).SetCellValue(index_row);
                CreateCell(row, 1).SetCellValue(record.TransactionNumber);
                CreateCell(row, 2).SetCellValue(record.RegistrationPlate);
                CreateCell(row, 3).SetCellValue(record.TankName);
                CreateCell(row, 4).SetCellValue(record.Quantity);
                CreateCell(row, 5).SetCellValue(record.UOM);
                string gateOutTimestamp = record.GateOutTimestamp.HasValue ? record.GateOutTimestamp.Value.ToString("dd MMMM yyyy HH:mm:ss") : "";
                CreateCell(row, 6).SetCellValue(gateOutTimestamp);

                current_row = current_row + 1;
                index_row = index_row + 1;
            }

            sheet.ForceFormulaRecalculation = true;
            MemoryStream ms = new MemoryStream();
            hssfworkbook.Write(ms);
            return ms.ToArray();
        }
    }
}