﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.Formula;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.Web.HttpHandlers
{
    public class StockMonitoringExporter : IHttpHandler
    {
        private Dictionary<int, ICellStyle> cellStyleLookup;
        private HSSFWorkbook hssfworkbook;

        public void ProcessRequest(HttpContext context)
        {
            string paramTank = context.Request["Tank"].ToString();
            string paramPeriod = context.Request["Period"].ToString();
            char[] delimiter = new char[] { '-' };
            string[] split = paramPeriod.Split(delimiter);
            int year = Convert.ToInt32(split[1]);
            int month = Convert.ToInt32(split[0]);
            long tankId = Convert.ToInt64(paramTank);
            byte[] result = RenderExcel(year, month, tankId);
            Tank t = Tank.FindById(tankId);
            context.Response.ContentType = "application/vnd.ms-excel";
            context.Response.AddHeader("Content-Length", result.Length.ToString());
            string period = month.ToString() + "-" + year.ToString();
            string filename = string.Format("Stock_Monitoring_{0}_{1}", t.Name, period);
            context.Response.AddHeader("Content-Disposition", "attachment; filename=\""+ filename +".xls\"");
            context.Response.BinaryWrite(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private PagingQuery ExtractQuery()
        {
            HttpContext Context = HttpContext.Current;
            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in Context.Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Context.Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Context.Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Context.Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Context.Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Context.Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;
            return pq;
        }

        private HSSFWorkbook LoadTemplate(string path)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            hssfworkbook = new HSSFWorkbook(file);
            return hssfworkbook;
        }

        private ICell CreateCell(IRow row, int cellIndex)
        {
            ICell cell = row.GetCell(cellIndex);
            if (cell == null)
            {
                cell = row.CreateCell(cellIndex);
            }
            if (cellStyleLookup.ContainsKey(cellIndex))
            {
                ICellStyle originalstyle = cellStyleLookup[cellIndex];
                cell.CellStyle = originalstyle;
            }
            return cell;
        }

        private Dictionary<int, ICellStyle> BuildRowStyleLookup(IRow row, int startCellIndex, int count)
        {
            Dictionary<int, ICellStyle> colStyleMap = new Dictionary<int, ICellStyle>();

            for (int i = 0, cellIndex = startCellIndex; i < count; i++, cellIndex++)
            {
                ICell cell = row.GetCell(cellIndex);
                if (cell != null)
                {
                    ICellStyle style = cell.CellStyle;
                    if (style != null)
                    {
                        colStyleMap.Add(cellIndex, style);
                    }
                }
            }
            return colStyleMap;
        }

        public byte[] RenderExcel(int year, int month, long tankId)
        {
            HSSFWorkbook hssfworkbook = LoadTemplate(HttpContext.Current.Server.MapPath("~/Content/Template/StockMonitoring.xls"));
            ISheet sheet = hssfworkbook.GetSheet("Stock Monitoring");
            sheet.ProtectSheet("Redeco@)@)");

            cellStyleLookup = BuildRowStyleLookup(sheet.GetRow(10), 0, 20); // 9 start isi data dari query

            int current_row = 10;
            int index_row = 1;
            string now = DateTime.Now.ToString();

            //export timestamp
            sheet.GetRow(5).GetCell(1).SetCellValue(DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"));

            //filter
            sheet.GetRow(5).GetCell(4).SetCellValue(month.ToString() + "-" + year.ToString());
            Tank t = Tank.FindById(tankId);
            sheet.GetRow(5).GetCell(6).SetCellValue(t.Name);

            int lastDate = DateTime.DaysInMonth(year, month);

            //if (records.Count != 0)
            //{
            //sheet.ShiftRows(9, sheet.LastRowNum, lastDate);
            //}

            for(int i = 1; i <= lastDate; i++)
            {
                IRow row = sheet.GetRow(current_row);
                if (row == null)
                {
                    row = sheet.CreateRow(current_row);
                }
                DateTime currentDate = new DateTime(year, month, i);
                CreateCell(row, 0).SetCellValue(currentDate.ToString("dd-MMM-yyyy"));

                //variable
                //meja ukur / measurement level
                CreateCell(row, 7).SetCellValue(t.MeasurementLevel);

                //calculate last month opening stock, find nearest sounding 
                if(i == 1)
                {
                    float lastOpeningStock = CalculatePreviousStock(year, month, tankId);
                    CreateCell(row, 1).SetCellValue(lastOpeningStock);
                }
                else
                {
                    //calculate from prev actual
                }
                //receiving and sounding
                ViewStockMonitoring receiving = ViewStockMonitoring.FindByTankAndReceivingDate(tankId, currentDate);
                if(receiving != null)
                {
                    CreateCell(row, 2).SetCellValue(receiving.Quantity);
                    CreateCell(row, 3).SetCellValue(receiving.Spillage);
                    CreateCell(row, 16).SetCellValue(receiving.BlNo);

                }
                //sounding
                ViewStockMonitoring sounding = ViewStockMonitoring.FindByTankAndSoundingDate(tankId, currentDate);
                if (sounding != null)
                {
                    CreateCell(row, 8).SetCellValue(sounding.SoundingLevel);
                    CreateCell(row, 10).SetCellValue(sounding.Temperature);
                    CreateCell(row, 11).SetCellValue(sounding.ProductTankVolume);
                    CreateCell(row, 12).SetCellValue(sounding.ProductPipeVolume);
                    CreateCell(row, 13).SetCellValue(sounding.ProductSlopeVolume);
                }

                //delivery
                ViewStockMonitoring delivery = ViewStockMonitoring.FindByTankAndDeliveryDate(tankId, currentDate);
                if(delivery != null) {
                    CreateCell(row, 4).SetCellValue(delivery.TotalDelivery);
                }

                current_row = current_row + 1;
                index_row = index_row + 1;
            }
            //foreach (CFS.Models.Truck record in records)
            //{
            //    IRow row = sheet.GetRow(current_row);
            //    if (row == null)
            //    {
            //        row = sheet.CreateRow(current_row);
            //    }

            //    string truckStnkTimestamp = record.TruckStnkTimestamp.HasValue ? record.TruckStnkTimestamp.Value.ToString("dd MMMM yyyy") : "";

            //    CreateCell(row, 0).SetCellValue(index_row);
            //    CreateCell(row, 1).SetCellValue(record.RegistrationPlate);
            //    CreateCell(row, 2).SetCellValue(record.TruckType);
            //    CreateCell(row, 3).SetCellValue(record.TankType);
            //    CreateCell(row, 4).SetCellValue(record.TruckTransporter);
            //    CreateCell(row, 5).SetCellValue(record.ChassisNumber);
            //    CreateCell(row, 6).SetCellValue(record.TruckTareWeight);
            //    CreateCell(row, 7).SetCellValue(truckStnkTimestamp);
            //    CreateCell(row, 8).SetCellValue(record.TruckStnkNumber);
            //    CreateCell(row, 9).SetCellValue(record.TankMaxCapacity);
            //    CreateCell(row, 10).SetCellValue(record.TankSafeCapacity);
            //    CreateCell(row, 11).SetCellValue(record.UOM);
            //    CreateCell(row, 12).SetCellValue(record.TruckBrand);
            //    CreateCell(row, 13).SetCellValue(record.EmergencyContact);

            //    current_row = current_row + 1;
            //    index_row = index_row + 1;
            //}

            sheet.ForceFormulaRecalculation = true;
            MemoryStream ms = new MemoryStream();
            hssfworkbook.Write(ms);
            return ms.ToArray();
        }

        public float CalculatePreviousStock (int year, int month, long tankId)
        {
            float lastStock = 0;
            DateTime firstDate = new DateTime(year, month, 1);
            ViewStockMonitoring latestSounding = ViewStockMonitoring.FindLatestSounding(tankId, firstDate);
            if(latestSounding == null)
            {
                return lastStock;
            }    
            else
            {
                //calculate latest stock for
                int lastDate = DateTime.DaysInMonth(year, month-1);
                DateTime? endMonth = new DateTime(year, month - 1, lastDate);
                int diff = Convert.ToInt32((endMonth - latestSounding.SoundingDate).Value.TotalDays);
                float latestStock = latestSounding.ProductTankVolume + latestSounding.ProductPipeVolume + latestSounding.ProductSlopeVolume;
                for(int i = 0; i <= diff; i++)
                {
                    //calculate plus minus
                    ViewStockMonitoring dailyDelivery = ViewStockMonitoring.FindByTankAndDeliveryDate(tankId, latestSounding.SoundingDate.Value.AddDays(i));
                    if(dailyDelivery != null)
                    {
                        latestStock = latestStock - dailyDelivery.TotalDelivery;
                    }
                }    
                return latestStock;
            }
            
        }
    }
}