﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using CFS.Models;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.Formula;
using NPOI.SS.UserModel;
using System.IO;

namespace CFS.Web.HttpHandlers
{
    /// <summary>
    /// Summary description for InspectionFormPdfExporter
    /// </summary>
    public class InspectionFormPdfExporter : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            int id = int.Parse(context.Request.QueryString["id"]);
            CFS.Models.TruckLoading toRender = CFS.Models.TruckLoading.FindById(id);
            CFS.Models.PDF.InspectionFormPdfRenderer tTruckLoading = new CFS.Models.PDF.InspectionFormPdfRenderer();

            byte[] pdf = tTruckLoading.Render(toRender);

            context.Response.ContentType = "application/pdf";
            context.Response.AddHeader("Content-Length", pdf.Length.ToString());
            context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"Form_Inspeksi_{0}_{1}.pdf\"",toRender.TruckRegistrationPlate  ,toRender.DeliveryOrderNumber));
            context.Response.BinaryWrite(pdf);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}