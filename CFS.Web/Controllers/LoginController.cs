﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CFS.Models;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Net;
using log4net;

namespace CFS.Web.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        private static ILog log = LogManager.GetLogger(typeof(LoginController));

        public ActionResult Index()
        {
            ViewBag.IsAuth = HttpContext.User.Identity.IsAuthenticated;
            ViewBag.DepotName = "";
            ViewBag.Location = "";
            try
            {
                ViewBag.DepotName = System.Configuration.ConfigurationManager.AppSettings["DepotName"];
                ViewBag.Location = System.Configuration.ConfigurationManager.AppSettings["Location"];
            }
            catch
            {
                ViewBag.DepotName = "";
                ViewBag.Location = "";
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(CFS.Models.Users user)
        {
            if (ModelState.IsValid)
            {
                string userid = Request.Form["userid"];
                string password = Request.Form["password"];
                string username = Request.Form["username"];

                if (DoLogin(user.Username, user.Password))
                {
                    FormsAuthentication.SetAuthCookie(user.Username, false);
                    CFS.Models.Users userdata = CFS.Models.Users.FindByUsername(username);
                    Session["Username"] = userdata.Username;
                    foreach (CFS.Models.Role r in userdata.User_Roles)
                    {
                        Session["Rolename"] = r.Name;
                        var sessionval = Session["Rolename"].ToString();
                    }
                    ViewBag.username = userdata.Username;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Incorrect username or password, or Disabled Account !");
                    View(user);
                }
            }
            return View(user);
        }

        private bool DoLogin(string username, string password)
        {
            return CFS.Models.Users.Login(username, password);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ajaxLogin(string username, string password)
        {
            Hashtable toReturn = new Hashtable();
            toReturn["success"] = false;
            try
            {
                bool result = DoLogin(username, password);
                toReturn["success"] = result;

                if (result)
                {
                    log.Debug("Do Login Result True");
                    System.Web.Security.FormsAuthentication.SetAuthCookie(username, false);
                    CFS.Models.Users userdata = CFS.Models.Users.FindByUsername(username);

                    Session["Username"] = userdata.Username;
                    foreach (CFS.Models.Role r in userdata.User_Roles)
                    {
                        Session["Rolename"] = r.Name;
                    }
                    //return RedirectToAction("Index", "Home");
                }
                else
                {
                    log.Debug("Do Login Result False");
                    toReturn["message"] = "Login failed.";

                }
                return new JsonNetResult() { Data = toReturn };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    log.DebugFormat("{0} : " + exc, "Failed Login");
                    exc = exc.InnerException;
                }
                toReturn["message"] = exc.Message;
                log.DebugFormat("{0} : " + exc, "Failed Login");
                return new JsonResult() { Data = toReturn };
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login");
        }
    }
}
