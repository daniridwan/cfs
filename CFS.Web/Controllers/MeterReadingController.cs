﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using Newtonsoft.Json;

namespace CFS.Web.Controllers
{
    public class MeterReadingController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            FillingPoint[] fillingpoint = FillingPoint.FindAll();
            ViewBag.FillingPoint = fillingpoint;
            return View();
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.ViewMeterReading.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;

            return pq;
        }

        [HttpGet]
        public ActionResult GetTotalThruput()
        {
            PagingQuery param = ExtractQuery();
            Hashtable toReturn = new Hashtable();
            toReturn["success"] = false;
            try
            {
                toReturn["records"] = ViewMeterReading.getThruputByPeriodandProduct(param);
                toReturn["records1"] = ViewMeterReading.getActualByPeriodandProduct(param);
                toReturn["records2"] = ViewMeterReading.getAverageFlowrate(param);
                toReturn["success"] = true;
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }
                toReturn["message"] = exc.Message;
                toReturn["stacktrace"] = exc.StackTrace;
            }
            return new JsonNetResult() { Data = toReturn };
        }


    }
}
