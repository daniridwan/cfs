﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using Newtonsoft.Json;
using Microsoft.AspNet.SignalR;
using System.Drawing;
using System.IO;

namespace CFS.Web.Controllers
{
    public class DriverController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [System.Web.Mvc.Authorize]
        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu Driver"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                Driver[] driver = Driver.FindAll();
                ViewBag.Driver = driver;
                ViewBag.New = false;
                ViewBag.Update = false;
                if (Users.GetCurrentUser().HasPermission("Create Driver"))
                {
                    ViewBag.New = true;
                }
                if (Users.GetCurrentUser().HasPermission("Update Driver"))
                {
                    ViewBag.Update = true;
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
            
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.Driver.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;

            return pq;
        }

        public ActionResult Save(CFS.Models.Driver toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.DriverId == 0)
                    {
                        CFS.Models.Driver inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.Driver updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.Driver InsertImpl(CFS.Models.Driver toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                PagingQuery pq = ExtractQuery();
                Hashtable toReturn = new Hashtable();
                toReturn["counter"] = Driver.GetTotalRegistered(pq);
                PagingResult pr = (PagingResult)toReturn["counter"];
                int count = Convert.ToInt32(pr.Records[0]);
                toSave.BadgeNumber = DateTime.Now.ToString("yy") + DateTime.Now.ToString("MM") + (count + 1).ToString("000");
                toSave.DriverKimExpiryDate = DateTime.Now.AddYears(1);
                toSave.CreatedTimestamp = DateTime.Now;

                string host = System.Configuration.ConfigurationManager.AppSettings["photoDirectory"];
                string filePath = string.Format("{0}/{1}/{2}.jpg", host, "images/DriverPhoto", toSave.BadgeNumber);
                //string filePath = string.Format("C:/Users/danim/Documents/Work/RPU/CFS/CFS.Web/Images/DriverPhoto/{0}.jpg", orig.BadgeNumber);
                if (!toSave.DriverPhoto.Contains("null") && toSave.DriverPhoto.Contains("data:image/png;base64,") && !toSave.DriverPhoto.Contains("Driver#"))
                {
                    System.IO.File.WriteAllBytes(filePath, Convert.FromBase64String(toSave.DriverPhoto.Replace("data:image/png;base64,", string.Empty)));
                    toSave.DriverPhoto = filePath;
                }
                else
                {
                    toSave.DriverPhoto = null;
                }

                toSave.CreatedBy = user;
                toSave.Save();

                //save notes
                DriverNotes toSaveNotes = new DriverNotes();
                IList<DriverNotes> LtoSaveNotes = new List<DriverNotes>();
                if(toSave.DriverNotes != null) { 
                    foreach (DriverNotes dn in toSave.DriverNotes)
                    {
                        dn.DriverId = toSave.DriverId;
                        dn.Save();
                    }
                }

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.Driver UpdateImpl(CFS.Models.Driver toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.Driver orig = CFS.Models.Driver.FindById(toSave.DriverId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                orig.BadgeNumber = toSave.BadgeNumber;
                orig.IdentificationNumber = toSave.IdentificationNumber;
                orig.Name = toSave.Name;
                orig.PlaceOfBirth = toSave.PlaceOfBirth;
                orig.DateOfBirth = toSave.DateOfBirth;
                orig.DriverSimType = toSave.DriverSimType;
                orig.DriverKtpNumber = toSave.DriverKtpNumber;
                orig.DriverSimNumber = toSave.DriverSimNumber;
                orig.DriverSimExpiryDate = toSave.DriverSimExpiryDate;
                orig.DriverKimExpiryDate = toSave.DriverKimExpiryDate;
                orig.DriverCompany = toSave.DriverCompany;
                orig.DriverAddress = toSave.DriverAddress;
                orig.DriverPhoneNumber = toSave.DriverPhoneNumber;
                orig.DriverMobilePhoneNumber = toSave.DriverMobilePhoneNumber;
                orig.IsActive = toSave.IsActive;

                //driver photo, shouldve move to config
                string host = System.Configuration.ConfigurationManager.AppSettings["photoDirectory"];
                string filePath = string.Format("{0}/{1}/{2}.jpg", host, "images/DriverPhoto", orig.BadgeNumber);
                //string filePath = string.Format("C:/Users/danim/Documents/Work/RPU/CFS/CFS.Web/Images/DriverPhoto/{0}.jpg", orig.BadgeNumber);
                if (!toSave.DriverPhoto.Contains("null") && toSave.DriverPhoto.Contains("data:image/png;base64,"))
                {
                    System.IO.File.WriteAllBytes(filePath, Convert.FromBase64String(toSave.DriverPhoto.Replace("data:image/png;base64,", string.Empty)));
                    orig.DriverPhoto = filePath;
                }
                
                orig.UpdatedBy = user;
                orig.UpdatedTimestamp = DateTime.Now;

                if (toSave.DriverNotes != null) {
                    //child table
                    foreach (DriverNotes dnNew in toSave.DriverNotes)
                    {
                        if (dnNew.DriverId == null || dnNew.DriverId == 0)
                        {
                            //insert new compartment
                            dnNew.DriverId = toSave.DriverId;
                            dnNew.Save();
                            orig.DriverNotes.Add(dnNew);
                        }
                        else
                        {
                            foreach (DriverNotes dnOrig in orig.DriverNotes)
                            {
                                if (dnNew.DriverNotesId == dnOrig.DriverNotesId)
                                {
                                    //update existing compartment
                                    dnOrig.Date = dnNew.Date;
                                    dnOrig.Notes = dnNew.Notes;
                                    dnOrig.Update();
                                    //isDeleted = false;
                                }
                            }
                        }
                    }

                    //find deleted notes
                    List<DriverNotes> deleted = new List<DriverNotes>();
                    foreach (DriverNotes dn in orig.DriverNotes)
                    {
                        bool found = false;

                        foreach (DriverNotes dnNew in toSave.DriverNotes)
                        {
                            if (dn.DriverNotesId == dnNew.DriverNotesId)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            deleted.Add(dn);
                        }
                    }

                    foreach (DriverNotes dnd in deleted)
                    {
                        dnd.Delete();
                    }
                }
                
                try
                {
                    orig.Update();
                }
                catch (Exception exc)
                {
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);
                }

                lastOperationStatus = true;

                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }
        //[HttpPost]
        //public ActionResult UserDevice(Driver param)
        //{
        //    var bs = new BiostarController();
        //    var toReturn = bs.addUser(param.DriverId.ToString(), param.Name.ToString(), 0, 1);
        //    if ((bool)toReturn["success"])
        //        UserFinger(Int32.Parse(param.DriverId.ToString()), "Sync");
        //    return new JsonNetResult() { Data = toReturn };
        //}

        public void UserFinger(int userid, string message)
        {
            try
            {
                var context = GlobalHost.ConnectionManager.GetHubContext<Hubs.UserFingerHub>();
                context.Clients.All.broadcastMessage(userid, message);
            }
            catch (Exception ex)
            {
                var toReturn = ex.Message.ToString();
            }
        }

        public Hashtable GetTotal()
        {
            PagingQuery param = ExtractQuery();
            Hashtable toReturn = new Hashtable();
            toReturn["success"] = false;
            try
            {
                toReturn["records"] = Driver.GetTotalRegistered(param);
                toReturn["success"] = true;
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }
                toReturn["message"] = exc.Message;
                toReturn["stacktrace"] = exc.StackTrace;
            }
            return toReturn;
        }

    }
}
