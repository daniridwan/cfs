﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using Newtonsoft.Json;
using System.Net;
using System.IO;

namespace CFS.Web.Controllers
{
    public class WeightingController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index(string donumber, string mode)
        {
            if (Users.GetCurrentUser().HasPermission("View Menu Weighting"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                DeliveryOrderDetail doDetail = DeliveryOrderDetail.FindByDeliveryOrderNumber(donumber);
                DeliveryOrder dO = DeliveryOrder.FindById(doDetail.DeliveryOrderId);
                TruckLoading tl = TruckLoading.FindByDeliveryOrdeNumber(dO.DeliveryOrderNumber);
                ViewBag.Truck = tl.TruckRegistrationPlate;
                ViewBag.DO = doDetail.DeliveryOrderNumber;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        [HttpPost]
        public ActionResult GetDODetail(string doNumber)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                DeliveryOrderDetail record = DeliveryOrderDetail.FindByDeliveryOrderNumber(doNumber);
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        public ActionResult Weighting(CFS.Models.TruckLoadingPlanning toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.DeliveryOrderNumber != "")
                    {
                        CFS.Models.TruckLoadingPlanning inserted = WeightingImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        toReturn["success"] = false;
                    }
                    if (lastOperationStatus)
                    {
                        ss.Flush();
                    }
                    else
                    {
                        ts.VoteRollBack();
                    }
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                return new JsonNetResult() { Data = toReturn };
            }
        }

        private CFS.Models.TruckLoadingPlanning WeightingImpl(CFS.Models.TruckLoadingPlanning toSave)
        {
            TruckLoadingPlanning orig = TruckLoadingPlanning.FindByDeliveryOrderNumber(toSave.DeliveryOrderNumber);
            //query string,
            string mode = Request.QueryString["mode"];
            if(toSave.InitialWeight != 0) { 
                orig.InitialWeight = toSave.InitialWeight;
            }
            //if(toSave.FinalWeight != 0) { 
            //    orig.FinalWeight = toSave.FinalWeight;
            //}
            orig.Update();
            lastOperationStatus = true;
            return orig;
        }
    }
}
