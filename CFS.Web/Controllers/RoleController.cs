﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using System.Net;
using Newtonsoft.Json;

namespace CFS.Web.Controllers
{
    public class RoleController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu Role"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                ViewBag.New = false;
                ViewBag.Update = false;
                if (Users.GetCurrentUser().HasPermission("Create Role"))
                {
                    ViewBag.New = true;
                }
                if (Users.GetCurrentUser().HasPermission("Update Role"))
                {
                    ViewBag.Update = true;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.Role.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;

            return pq;
        }
        public ActionResult Save(CFS.Models.Role toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.RoleId == 0)
                    {
                        CFS.Models.Role inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.Role updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.Role InsertImpl(CFS.Models.Role toSave)
        {
            if (toSave.Validate("insert"))
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());

                toSave.Save();

                //child table
                char[] separator = { ',' };
                string[] arrayPermission = toSave.PermissionNameStr.Split(separator);
                foreach (var rn in arrayPermission)
                {
                    CFS.Models.RolePermission origRolesPer = new CFS.Models.RolePermission();
                    origRolesPer.RoleId = toSave.RoleId;
                    origRolesPer.PermissionId = Permission.FindByName(rn).PermissionId;
                    origRolesPer.Save();
                }

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.Role UpdateImpl(CFS.Models.Role toSave)
        {

            if (toSave.Validate("update"))
            {
                CFS.Models.Role orig = CFS.Models.Role.FindById(toSave.RoleId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                orig.Name = toSave.Name;

                CFS.Models.RolePermission[] origRolePermission = CFS.Models.RolePermission.FindByRoleId(toSave.RoleId);
                foreach (var p in origRolePermission)
                {
                    p.DeleteAndFlush();
                }
                var rn = toSave.PermissionNameStr.Split(',');
                for (int i = 0; i < rn.Length; i++)
                {
                    CFS.Models.RolePermission origRolesPermission = new CFS.Models.RolePermission();
                    origRolesPermission.RoleId = toSave.RoleId;
                    origRolesPermission.PermissionId = Permission.FindByName(rn[i]).PermissionId;
                    origRolesPermission.Save();
                }
                orig.Update();
                lastOperationStatus = true;
                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }


        public ActionResult Delete(Int32 Id)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    CFS.Models.Role toDelete = CFS.Models.Role.FindById(Id);
                    CFS.Models.Role Deleted = DeleteImpl(toDelete);
                    ss.Flush();
                    lastErrorMessages.Add("Record deleted successfully");
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = Deleted;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.Role DeleteImpl(CFS.Models.Role toSave)
        {

            if (toSave.Validate("delete"))
            {
                CFS.Models.Role orig = CFS.Models.Role.FindById(toSave.RoleId);

                orig.DeleteAndFlush();
                lastOperationStatus = true;

                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }
    }
}
