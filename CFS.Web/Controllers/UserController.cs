﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using System.Net;
using Newtonsoft.Json;
using Microsoft.AspNet.SignalR;

namespace CFS.Web.Controllers
{
    public class UserController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [System.Web.Mvc.Authorize]
        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu Users"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                ViewBag.New = false;
                ViewBag.Update = false;
                if (Users.GetCurrentUser().HasPermission("Create User"))
                {
                    ViewBag.New = true;
                }
                if (Users.GetCurrentUser().HasPermission("Update User"))
                {
                    ViewBag.Update = true;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.Users.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        [System.Web.Mvc.Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        private CFS.Models.Users ChangePasswordImpl(CFS.Models.Users toSave)
        {
            if (toSave.Validate("changePassword"))
            {
                CFS.Models.Users orig = CFS.Models.Users.FindById(toSave.UserId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                orig.Password = toSave.NewPassword;
                orig.IsAlreadyChangePassword = 1;
                orig.UpdatedTimestamp = DateTime.Now;
                orig.UpdatedBy = user;
                orig.UpdateAndFlush();
                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;

            return pq;
        }
        public ActionResult Save(CFS.Models.Users toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.UserId == 0)
                    {
                        CFS.Models.Users inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.Users updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.Users InsertImpl(CFS.Models.Users toSave)
        {
            if (toSave.Validate("insert"))
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());

                toSave.Password = CFS.Models.Users.StringToMD5Hash(toSave.Username);
                toSave.IsAlreadyChangePassword = 0;
                toSave.CreatedTimestamp = DateTime.Now;
                toSave.CreatedBy = user;

                toSave.Save();

                //child table
                char[] separator = { ',' };
                string[] arrayRole = toSave.RoleNameStr.Split(separator);
                foreach (var rn in arrayRole)
                {
                    CFS.Models.UserRole origRoles = new CFS.Models.UserRole();
                    origRoles.UserId = toSave.UserId;
                    origRoles.RoleId = Role.FindByName(rn).RoleId;
                    origRoles.Save();
                }

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        public ActionResult UpdatePassword(CFS.Models.Users toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.Username != null)
                    {
                        string oldPassword = toSave.OldPassword;
                        string newPassword = toSave.NewPassword;
                        string confirmPassword = toSave.ConfirmPassword;
                        Users toChange = Users.FindByUsername(toSave.Username);
                        toChange.OldPassword = oldPassword;
                        toChange.NewPassword = newPassword;
                        toChange.ConfirmPassword = confirmPassword;
                        CFS.Models.Users changed = ChangePasswordImpl(toChange);
                        toReturn["record"] = changed;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.Users UpdateImpl(CFS.Models.Users toSave)
        {

            if (toSave.Validate("update"))
            {
                CFS.Models.Users orig = CFS.Models.Users.FindById(toSave.UserId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                orig.Username = toSave.Username;
                orig.Email = toSave.Email;
                orig.PhoneNumber = toSave.PhoneNumber;
                orig.IsActive = toSave.IsActive;

                orig.UpdatedTimestamp = DateTime.Now;
                orig.UpdatedBy = user;

                //child table
                CFS.Models.UserRole[] origUserRoles = CFS.Models.UserRole.FindByUserId(toSave.UserId);
                foreach (var r in origUserRoles)
                {
                    r.DeleteAndFlush();
                }
                var rn = toSave.RoleNameStr.Split(',');
                for (int i = 0; i < rn.Length; i++)
                {
                    CFS.Models.UserRole origRoles = new CFS.Models.UserRole();
                    origRoles.UserId = toSave.UserId;
                    origRoles.RoleId = Role.FindByName(rn[i]).RoleId;
                    origRoles.Save();
                }

                orig.Update();

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }


        public ActionResult Delete(Int32 Id)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    CFS.Models.Users toDelete = CFS.Models.Users.FindById(Id);
                    CFS.Models.Users Deleted = DeleteImpl(toDelete);
                    ss.Flush();
                    lastErrorMessages.Add("Record deleted successfully");
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = Deleted;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.Users DeleteImpl(CFS.Models.Users toSave)
        {

            if (toSave.Validate("delete"))
            {
                CFS.Models.Users orig = CFS.Models.Users.FindById(toSave.UserId);

                orig.DeleteAndFlush();
                lastOperationStatus = true;

                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        public ActionResult ResetPassword(int id, string password)
        {
            PagingResult toReturn = new PagingResult();
            toReturn.Success = false;

            string passwordRegex = @"^.*(?=.{8,16})(?=.*\d)(?=.*[a-zA-Z]).*$";
            if (!Regex.IsMatch(password, passwordRegex))
            {
                toReturn.Message = "Password must be 8-16 characters and must contain at least 1 uppercase and 1 number";
                toReturn.Success = false;
            }
            else
            {
                CFS.Models.Users u = CFS.Models.Users.FindById(id);
                ResetPasswordImpl(u, password);
                toReturn.Message = "Reset Password Succeed";
                toReturn.Success = true;
            }

            return new JsonResult() { Data = toReturn };
        }

        private void ResetPasswordImpl(Users user, string password)
        {
            user.ResetPassword(password);
        }

        //[HttpPost]
        //public ActionResult UserDevice(Users param)
        //{
        //    var bs = new BiostarController();
        //    //have to separate with crew id, spare 10k counter
        //    long generateUserId = 10000 + param.UserId;
        //    var toReturn = bs.addUser(generateUserId.ToString(), param.Username.ToString(), 0, 1);
        //    if ((bool)toReturn["success"])
        //        UserFinger(Int32.Parse(generateUserId.ToString()), "Sync");
        //    return new JsonNetResult() { Data = toReturn };
        //}

        public void UserFinger(int userid, string message)
        {
            try
            {
                var context = GlobalHost.ConnectionManager.GetHubContext<Hubs.UserFingerHub>();
                context.Clients.All.broadcastMessage(userid, message);
            }
            catch
            {
            }
        }

        [HttpPost]
        public ActionResult GetCurrentUserPermission(string permission)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                bool result = Users.GetCurrentUser().HasPermission(permission);
                toReturn["records"] = result;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetCurrentUser()
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                Users u = Users.GetCurrentUser();
                toReturn["records"] = u;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetCurrentUserRoles()
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                string roles = Users.GetCurrentUser().GetRoleNames().ToString();
                toReturn["records"] = roles;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }
    }
}

