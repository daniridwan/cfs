﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using System.Net;
using Newtonsoft.Json;
using CFS.Biostar;
using System.Runtime.InteropServices;

namespace CFS.Web.Controllers
{
    public class BiostarController : Controller
    {
        //
        // GET: /Biostar/
        string deviceIpAddress = System.Configuration.ConfigurationManager.AppSettings["ScanerIpAddress"];

        public ActionResult Index()
        {
            return View();
        }

        public Hashtable addUser(string userid, string username, int useCard, int useFinger)
        {
            Hashtable toReturn = new Hashtable();
            toReturn["success"] = false;

            BS_Program bs = new BS_Program();
            API.OnReadyToScan cbCardOnReadyToScan = null;
            API.OnReadyToScan cbFingerOnReadyToScan = null;

            BS2ErrorCode result = BS2ErrorCode.BS_SDK_SUCCESS;
            BS2FingerprintTemplateFormatEnum templateFormat = BS2FingerprintTemplateFormatEnum.FORMAT_SUPREMA;
            SortedSet<BS2CardAuthModeEnum> privateCardAuthMode = new SortedSet<BS2CardAuthModeEnum>();
            SortedSet<BS2FingerAuthModeEnum> privateFingerAuthMode = new SortedSet<BS2FingerAuthModeEnum>();
            SortedSet<BS2IDAuthModeEnum> privateIDAuthMode = new SortedSet<BS2IDAuthModeEnum>();
            SortedSet<BS2FaceAuthModeEnum> privateFaceAuthMode = new SortedSet<BS2FaceAuthModeEnum>();

            if (bs.DeviceID == 0)
            {
                bs.init();
                bs.Msg.Clear();
                if (!bs.ConnectToDevice(deviceIpAddress))
                {
                    toReturn["message"] = "Failed to connect to device." + "\n" + "Please try to connect again.";
                    return toReturn;
                }
                else
                {

                    bool cardSupported = Convert.ToBoolean(bs.DeviceInfo.cardSupported);
                    bool fingerSupported = Convert.ToBoolean(bs.DeviceInfo.fingerSupported);
                    if (useCard != 1)
                        cardSupported = false;
                    if (useFinger != 1)
                        fingerSupported = false;

                    privateIDAuthMode.Add(BS2IDAuthModeEnum.PROHIBITED);

                    if (cardSupported)
                    {
                        privateCardAuthMode.Add(BS2CardAuthModeEnum.PROHIBITED);
                        privateCardAuthMode.Add(BS2CardAuthModeEnum.CARD_ONLY);
                    }
                    else if (fingerSupported)
                    {
                        privateFingerAuthMode.Add(BS2FingerAuthModeEnum.BIOMETRIC_ONLY);

                        privateIDAuthMode.Add(BS2IDAuthModeEnum.ID_BIOMETRIC);
                    }

                    BS2UserBlob userBlob = Util.AllocateStructure<BS2UserBlob>();
                    userBlob.user.version = 0;
                    userBlob.user.formatVersion = 0;
                    userBlob.user.faceChecksum = 0;
                    userBlob.user.fingerChecksum = 0;
                    userBlob.user.numCards = 0;
                    userBlob.user.numFingers = 0;
                    userBlob.user.numFaces = 0;
                    userBlob.user.flag = 0;

                    userBlob.cardObjs = IntPtr.Zero;
                    userBlob.fingerObjs = IntPtr.Zero;
                    userBlob.faceObjs = IntPtr.Zero;

                    string userID = userid;
                    if (userID.Length == 0)
                    {
                        toReturn["message"] = "The user id can not be empty.";
                        goto dcDevice;
                    }
                    else if (userID.Length > BS2Envirionment.BS2_USER_ID_SIZE)
                    {
                        toReturn["message"] = string.Format("The user id should less than {0} words.", BS2Envirionment.BS2_USER_ID_SIZE);
                        goto dcDevice;
                    }
                    else
                    {
                        //TODO Alphabet user id is not implemented yet.
                        UInt32 uid;
                        if (!UInt32.TryParse(userID, out uid))
                        {
                            toReturn["message"] = "The user id should be a numeric.";
                            goto dcDevice;
                        }

                        byte[] userIDArray = Encoding.UTF8.GetBytes(userID);
                        Array.Clear(userBlob.user.userID, 0, BS2Envirionment.BS2_USER_ID_SIZE);
                        Array.Copy(userIDArray, userBlob.user.userID, userIDArray.Length);
                    }

                    userBlob.setting.startTime = 0;
                    userBlob.setting.endTime = 0;

                    if (cardSupported)
                    {
                        userBlob.setting.cardAuthMode = (byte)BS2CardAuthModeEnum.CARD_ONLY;
                    }

                    if (fingerSupported)
                    {
                        userBlob.setting.securityLevel = (byte)BS2UserSecurityLevelEnum.DEFAULT;
                        //userBlob.setting.fingerAuthMode = (byte)BS2FingerAuthModeEnum.BIOMETRIC_ONLY;
                        userBlob.setting.fingerAuthMode = (byte)BS2FingerAuthModeEnum.NONE;
                    }

                    userBlob.setting.idAuthMode = (byte)BS2IDAuthModeEnum.NONE;

                    Array.Clear(userBlob.name, 0, BS2Envirionment.BS2_USER_NAME_LEN);

                    string userName = username;

                    if (userName.Length == 0)
                    {
                        toReturn["message"] = "[Warning] user name will be displayed as empty.";
                        goto dcDevice;
                    }
                    else if (userName.Length > BS2Envirionment.BS2_USER_NAME_LEN)
                    {
                        toReturn["message"] = string.Format("The user name should less than {0} words.", BS2Envirionment.BS2_USER_NAME_LEN);
                        goto dcDevice;
                    }
                    else
                    {
                        byte[] userNameArray = Encoding.UTF8.GetBytes(userName);
                        Array.Copy(userNameArray, userBlob.name, userNameArray.Length);
                    }

                    userBlob.photo.size = 0;
                    Array.Clear(userBlob.photo.data, 0, BS2Envirionment.BS2_USER_PHOTO_SIZE);

                    if (cardSupported)
                    {
                        userBlob.user.numCards = (byte)1;

                        if (userBlob.user.numCards > 0)
                        {
                            int structSize = Marshal.SizeOf(typeof(BS2CSNCard));
                            BS2Card card = Util.AllocateStructure<BS2Card>();
                            userBlob.cardObjs = Marshal.AllocHGlobal(structSize * userBlob.user.numCards);

                            IntPtr curCardObjs = userBlob.cardObjs;
                            cbCardOnReadyToScan = new API.OnReadyToScan(ReadyToScanForCard);

                            for (byte idx = 0; idx < userBlob.user.numCards; )
                            {
                                result = (BS2ErrorCode)API.BS2_ScanCard(bs.SdkContext, bs.DeviceID, out card, cbCardOnReadyToScan);
                                if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                                {
                                    toReturn["message"] = string.Format("Got error({0}).", result);
                                    goto dcDevice;
                                }
                                else if (Convert.ToBoolean(card.isSmartCard))
                                {
                                    toReturn["message"] = "CSN card is only available. Try again";
                                    goto dcDevice;
                                }
                                else
                                {
                                    Marshal.Copy(card.cardUnion, 0, curCardObjs, structSize);
                                    curCardObjs += structSize;
                                    ++idx;
                                }
                            }

                            cbCardOnReadyToScan = null;
                        }
                    }

                    if (fingerSupported)
                    {
                        userBlob.user.numFingers = (byte)1;

                        if (userBlob.user.numFingers > 0)
                        {
                            BS2FingerprintConfig fingerprintConfig;

                            result = (BS2ErrorCode)API.BS2_GetFingerprintConfig(bs.SdkContext, bs.DeviceID, out fingerprintConfig);
                            if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            {
                                toReturn["message"] = string.Format("Got error({0}).", result);
                                goto dcDevice;
                            }
                            else
                            {
                                templateFormat = (BS2FingerprintTemplateFormatEnum)fingerprintConfig.templateFormat;
                            }

                            int structSize = Marshal.SizeOf(typeof(BS2Fingerprint));
                            BS2Fingerprint fingerprint = Util.AllocateStructure<BS2Fingerprint>();
                            userBlob.fingerObjs = Marshal.AllocHGlobal(structSize * userBlob.user.numFingers);
                            IntPtr curFingerObjs = userBlob.fingerObjs;
                            cbFingerOnReadyToScan = new API.OnReadyToScan(ReadyToScanForFinger);

                            UInt32 outquality;
                            for (int idx = 0; idx < userBlob.user.numFingers; ++idx)
                            {
                                for (UInt32 templateIndex = 0; templateIndex < BS2Envirionment.BS2_TEMPLATE_PER_FINGER; )
                                {
                                    result = (BS2ErrorCode)API.BS2_ScanFingerprint(bs.SdkContext, bs.DeviceID, ref fingerprint, templateIndex, (UInt32)BS2FingerprintQualityEnum.QUALITY_STANDARD, (byte)templateFormat, out outquality, cbFingerOnReadyToScan);
                                    if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                                    {
                                        if (result == BS2ErrorCode.BS_SDK_ERROR_EXTRACTION_LOW_QUALITY ||
                                            result == BS2ErrorCode.BS_SDK_ERROR_CAPTURE_LOW_QUALITY)
                                        {
                                            toReturn["message"] = "Bad fingerprint quality. Try again";
                                            goto dcDevice;
                                        }
                                        else
                                        {
                                            toReturn["message"] = string.Format("Got error({0}).", result);
                                            goto dcDevice;
                                        }
                                    }
                                    else
                                    {
                                        ++templateIndex;
                                    }
                                }

                                //Verify the fingerprints
                                result = (BS2ErrorCode)API.BS2_VerifyFingerprint(bs.SdkContext, bs.DeviceID, ref fingerprint);
                                if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                                {
                                    if (result == BS2ErrorCode.BS_SDK_ERROR_NOT_SAME_FINGERPRINT)
                                    {
                                        toReturn["message"] = "Verify the fingerprint does not match. Try again";
                                        goto dcDevice;
                                    }
                                    else
                                    {
                                        toReturn["message"] = string.Format("Got error({0}).", result);
                                        goto dcDevice;
                                    }
                                }

                                fingerprint.index = (byte)idx;
                                fingerprint.flag = (byte)BS2FingerprintFlagEnum.NORMAL;

                                Marshal.StructureToPtr(fingerprint, curFingerObjs, false);
                                curFingerObjs += structSize;
                            }

                            cbFingerOnReadyToScan = null;
                        }
                    }

                    Array.Clear(userBlob.accessGroupId, 0, BS2Envirionment.BS2_MAX_ACCESS_GROUP_PER_USER);

                    #region groupid
                    //int accessGroupIdIndex = 0;
                    //char[] delimiterChars = { ' ', ',', '.', ':', '\t' };
                    //string[] accessGroupIDs = "ID_1".Split(delimiterChars);

                    //foreach (string accessGroupID in accessGroupIDs)
                    //{
                    //    if (accessGroupID.Length > 0)
                    //    {
                    //        UInt32 item;
                    //        if (UInt32.TryParse(accessGroupID, out item))
                    //        {
                    //            userBlob.accessGroupId[accessGroupIdIndex++] = item;
                    //        }
                    //    }
                    //}
                    #endregion

                    BS2UserBlob[] userBlobs = new BS2UserBlob[] { userBlob };
                    Console.WriteLine("Trying to enroll user.");
                    result = (BS2ErrorCode)API.BS2_EnrolUser(bs.SdkContext, bs.DeviceID, userBlobs, 1, 1);

                    //Release memory
                    if (userBlob.cardObjs != IntPtr.Zero)
                    {
                        Marshal.FreeHGlobal(userBlob.cardObjs);
                    }

                    if (userBlob.fingerObjs != IntPtr.Zero)
                    {
                        Marshal.FreeHGlobal(userBlob.fingerObjs);
                    }
                    toReturn["success"] = true;
                    toReturn["records"] = userid;

                dcDevice:
                    bs.DisconnectToDevice();
                    bs.release();
                }
            }
            return toReturn;
        }

        //public Hashtable ScanCard()
        public ActionResult ScanCard()
        {
            Hashtable toReturn = new Hashtable();
            toReturn["success"] = true;
            string cardId = "";

            BS_Program bs = new BS_Program();
            API.OnReadyToScan cbCardOnReadyToScan = null;
            BS2ErrorCode result = BS2ErrorCode.BS_SDK_SUCCESS;

            if (bs.DeviceID == 0)
            {
                bs.init();
                bs.Msg.Clear();
                if (!bs.ConnectToDevice(deviceIpAddress))
                {
                    toReturn["success"] = false;
                    toReturn["message"] = "Failed to connect to device." + "\n" + "Please try to connect again.";
                }
                else
                {
                    BS2Card card;

                    cbCardOnReadyToScan = new API.OnReadyToScan(ReadyToScanForCard);
                    result = (BS2ErrorCode)API.BS2_ScanCard(bs.SdkContext, bs.DeviceID, out card, cbCardOnReadyToScan);
                    if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                    {
                        toReturn["success"] = false;
                        toReturn["message"] = string.Format("Got error({0}).", result);
                    }
                    else
                    {
                        if (Convert.ToBoolean(card.isSmartCard))
                        {
                            UInt16 hdrCRC;
                            UInt16 cardCRC;
                            BS2SmartCardData smartCard = Util.ConvertTo<BS2SmartCardData>(card.cardUnion);

                            result = bs.computeCRC(smartCard, out hdrCRC, out cardCRC);
                            if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            {
                                toReturn["success"] = false;
                                toReturn["message"] = string.Format("Can't compute CRC16({0})", result);
                            }
                            else if (smartCard.header.hdrCRC != hdrCRC)
                            {
                                toReturn["success"] = false;
                                toReturn["message"] = string.Format("Get a header crc mismatch(expected[{0}] computed[{1}])", smartCard.header.hdrCRC, hdrCRC);
                            }
                            else if (smartCard.header.cardCRC != cardCRC)
                            {
                                toReturn["success"] = false;
                                toReturn["message"] = string.Format("Get a card crc mismatch(expected[{0}] computed[{1}])", smartCard.header.cardCRC, cardCRC);
                            }
                            else
                            {
                                string hexVal = BitConverter.ToString(smartCard.cardID).Replace("-", string.Empty);
                                cardId = Convert.ToInt64(hexVal, 16).ToString();
                            }
                        }
                        else
                        {
                            BS2CSNCard csnCard = Util.ConvertTo<BS2CSNCard>(card.cardUnion);
                            string hexVal = BitConverter.ToString(csnCard.data).Replace("-", string.Empty);
                            cardId = Convert.ToInt64(hexVal, 16).ToString();
                        }
                    }

                    //cbCardOnReadyToScan = null;
                    bs.DisconnectToDevice();
                    bs.release();
                }
            }

            toReturn["records"] = cardId;
            return new JsonNetResult() { Data = toReturn };
            //return toReturn;
        }

        void ReadyToScanForCard(UInt32 deviceID, UInt32 sequence)
        {
            //Console.WriteLine("Place your card on the device.");
        }

        void ReadyToScanForFinger(UInt32 deviceID, UInt32 sequence)
        {
            //Console.WriteLine("Place your card on the device.");
        }

    }
}
