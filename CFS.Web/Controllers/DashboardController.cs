﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using Newtonsoft.Json;

namespace CFS.Web.Controllers
{
    public class DashboardController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetTankLatestOperation()
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                ViewTankLatestOperation[] record = ViewTankLatestOperation.FindAll();
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetPlanningVsThroughput()
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                ViewPlanningVsThroughput[] record = ViewPlanningVsThroughput.FindAll();
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetLatestNotification()
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                Notification[] record = Notification.Find3Latest();
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetDailyThroughput()
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                ViewDailyThroughput[] record = ViewDailyThroughput.FindCurrentMonthThroughput();
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        public ActionResult TruckTrackingPage()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.ViewQueueTruck.SelectPagingGateIn(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }
            pq.SearchParam = nvc;
            return pq;
        }

        [HttpPost]
        public ActionResult Speak(string number)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                if (number.Length == 0)
                {
                    toReturn["success"] = false;
                }

                List<string> soundList = new List<string>();
                soundList.Add("bell.wav");
                soundList.Add("NOMOR ANTRIAN2.wav");
                number = number.Replace(" ", "");
                char[] words = number.ToCharArray();
                if (number == "10")
                {
                    soundList.Add("10.wav");
                }
                else if (number == "11")
                {
                    soundList.Add("11.wav");
                }
                else { 
                    foreach (char word in words)
                    {
                        soundList.Add(string.Format("{0}.wav", word));
                    }
                }
                soundList.Add("DIPERSILAHKANMENUJU.wav");
                soundList.Add("loket2.wav");
                soundList.Add("2.wav");
                toReturn["records"] = soundList;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }
    }
}
