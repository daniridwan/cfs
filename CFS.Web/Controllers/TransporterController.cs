﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using System.Net;
using Newtonsoft.Json;

namespace CFS.Web.Controllers
{
    public class TransporterController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            ViewBag.New = false;
            ViewBag.Download = false;
            ViewBag.Update = false;
            if (Users.GetCurrentUser().HasPermission("Transporter_CREATE"))
            {
                ViewBag.New = true;
            }
            if (Users.GetCurrentUser().HasPermission("Transporter_DOWNLOAD"))
            {
                ViewBag.Download = true;
            }
            if (Users.GetCurrentUser().HasPermission("Transporter_UPDATE"))
            {
                ViewBag.Update = true;
            }
            return View();
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.Transporter.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;

            return pq;
        }

        public ActionResult Save(CFS.Models.Transporter toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.Id == 0)
                    {
                        CFS.Models.Transporter inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.Transporter updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.Transporter InsertImpl(CFS.Models.Transporter toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                toSave.CreatedTimestamp = DateTime.Now;
                toSave.CreatedBy = user;
                toSave.Save();

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.Transporter UpdateImpl(CFS.Models.Transporter toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.Transporter orig = CFS.Models.Transporter.FindById(toSave.Id);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                orig.Name = toSave.Name;
                orig.TransporterCode = toSave.TransporterCode;
                orig.Name = toSave.Name;
                orig.Address = toSave.Address;
                orig.City = toSave.City;
                orig.PostalCode = toSave.PostalCode;

                orig.UpdatedBy = user;
                orig.UpdatedTimestamp = DateTime.Now;

                orig.Update();
                lastOperationStatus = true;

                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        //[HttpPost]
        //public ActionResult Download(Transporter param)
        //{
        //    Hashtable toReturn = new Hashtable();

        //    //request param
        //    FDM.SAP.GetTransporter.dt_getTransporterRequest request = new FDM.SAP.GetTransporter.dt_getTransporterRequest();
        //    request.Transporter = param.TransporterCode;

        //    //do work
        //    FDM.SAP.GetTransporter.mi_osgetTransporterService service = new FDM.SAP.GetTransporter.mi_osgetTransporterService();
        //    FDM.SAP.GetTransporter.dt_getTransporterResponse response = null;

        //    try
        //    {
        //        //create proxy & credential
        //        WebServiceConfiguration config = FDM.Models.WebServiceConfiguration.FindByName("GetTransporter");
        //        service.Proxy = WebProxy.GetDefaultProxy();
        //        service.Url = config.Url;
        //        service.Credentials = new System.Net.NetworkCredential(config.Username, config.Password);
        //        service.Timeout = config.Timeout;
        //        //process
        //        response = service.mi_osgetTransporter(request);

        //        foreach (FDM.SAP.GetTransporter.dt_getTransporterResponseMessage msg in response.Message)
        //        {
        //            if (msg.Type == "S")
        //            {
        //                //success
        //                //check if already exist
        //                Transporter findTransporter = Transporter.FindByCode(param.TransporterCode);
        //                FDM.Models.SimpleUser user = FDM.Models.SimpleUser.CreateFromUser(FDM.Models.Users.GetCurrentUser());
        //                if (findTransporter == null)
        //                {
        //                    //new
        //                    Transporter transporter = new Transporter();
        //                    transporter.TransporterCode = response.TransporterDetails[0].Transporter_Code;
        //                    transporter.Name = response.TransporterDetails[0].Transporter_Name;
        //                    transporter.Address = response.TransporterDetails[0].Transporter_Address;
        //                    transporter.City = response.TransporterDetails[0].Transporter_City;
        //                    transporter.PostalCode = response.TransporterDetails[0].Postal_Code;
        //                    transporter.CreatedTimestamp = DateTime.Now;
        //                    transporter.CreatedBy = user;
        //                    transporter.SaveAndFlush();
        //                    toReturn["message"] = string.Format("Transporter code {0} berhasil ditambahkan", param.TransporterCode);
        //                }
        //                else
        //                {
        //                    //update
        //                    findTransporter.TransporterCode = response.TransporterDetails[0].Transporter_Code;
        //                    findTransporter.Name = response.TransporterDetails[0].Transporter_Name;
        //                    findTransporter.Address = response.TransporterDetails[0].Transporter_Address;
        //                    findTransporter.City = response.TransporterDetails[0].Transporter_City;
        //                    findTransporter.PostalCode = response.TransporterDetails[0].Postal_Code;
        //                    findTransporter.UpdatedTimestamp = DateTime.Now;
        //                    findTransporter.UpdatedBy = user;
        //                    findTransporter.UpdateAndFlush();
        //                    toReturn["message"] = string.Format("Transporter code {0} berhasil diupdate", param.TransporterCode);
        //                }
        //                toReturn["records"] = response.TransporterDetails;
        //                toReturn["success"] = true;
        //            }
        //            else
        //            {
        //                toReturn["success"] = false;
        //                toReturn["message"] = msg.Desc_Msg;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string exception = "";
        //        while (ex.InnerException != null)
        //        {
        //            exception = ex.InnerException.ToString();
        //        }
        //        if (ex.InnerException == null)
        //        {
        //            exception = ex.Message;
        //        }

        //        toReturn["success"] = false;
        //        toReturn["message"] = exception;
        //    }

        //    return new JsonNetResult() { Data = toReturn };
        //}
    }
}
