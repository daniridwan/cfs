﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using System.Net;
using Newtonsoft.Json;
using EasyModbus;
using System.Net;
using System.IO;

namespace CFS.Web.Controllers
{
    public class DistributionMonitoringController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu Distribution Monitoring"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                Truck[] truck = Truck.FindAll();
                ViewBag.Truck = truck;
                Driver[] driver = Driver.FindAllActive();
                ViewBag.Driver = driver;
                Company[] company = Company.FindAll();
                ViewBag.Company = company;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.TruckLoading.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {
            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;
            return pq;
        }

        public ActionResult Administration(CFS.Models.TruckLoading toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.TruckLoadingId == 0)
                    {
                        CFS.Models.TruckLoading inserted = AdministrationImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        toReturn["success"] = false;
                    }
                    if (lastOperationStatus)
                    {
                        ss.Flush();
                    }
                    else
                    {
                        ts.VoteRollBack();
                    }
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                return new JsonNetResult() { Data = toReturn };
            }
        }

        private CFS.Models.TruckLoading AdministrationImpl (CFS.Models.TruckLoading toSave)
        {
            if (toSave.Validate("Administration"))
            {
                lastOperationStatus = false;
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                toSave.AdministrationLocation= "CFS Web";
                toSave.AdministrationMode = "Manual";
                toSave.AdministrationUser = user.Username;
                toSave.AdministrationTimestamp = DateTime.Now;
                toSave.ShipmentStatus = 1;
                toSave.CreatedTimestamp = DateTime.Now;
                toSave.CreatedBy = user;
                toSave.Save();
                
                foreach (TruckLoadingPlanning tp in toSave.TruckLoadingPlanning)
                {
                    tp.TruckLoadingId = toSave.TruckLoadingId;
                    tp.Save();
                }

                foreach(TruckLoadingSecuritySeal tls in toSave.TruckLoadingSecuritySeal)
                {
                    tls.TruckLoadingId = toSave.TruckLoadingId;
                    tls.Save();
                }

                DeliveryOrder dO = DeliveryOrder.FindByShipmentNumber(toSave.DeliveryOrderNumber);
                dO.Status = "In Process";
                dO.UpdateAndFlush();

                SystemValidation sysParking = SystemValidation.FindByKey("Parking_Sync");
                if (sysParking.IsActive)
                {
                    try
                    {
                        //send to parking system
                        string url = GlobalClass.parkingRoutesApi("api/Visitor", "InsertVisitor", "deliveryOrderNumber=" + toSave.DeliveryOrderNumber);
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                        request.Accept = "application/json";
                        request.ContentType = "application/json";
                        request.Method = "POST";
                        //remove updated by
                        toSave.UpdatedBy = null;
                        string parsedContent = Newtonsoft.Json.JsonConvert.SerializeObject(toSave);
                        ASCIIEncoding encoding = new ASCIIEncoding();
                        Byte[] bytes = encoding.GetBytes(parsedContent);
                        request.ContentLength = bytes.Length;

                        Stream dataStream = request.GetRequestStream();
                        dataStream.Write(bytes, 0, bytes.Length);
                        dataStream.Close();

                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        string content = new StreamReader(response.GetResponseStream()).ReadToEnd();
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            lastOperationStatus = true;
                        }
                        else
                        {
                            lastOperationStatus = false;
                            lastErrorMessages.Add(string.Format("Error Parking API : {0}", response.StatusDescription));
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        lastOperationStatus = false;
                        lastErrorMessages.Add(string.Format("Error Parking API : {0}", ex.Message));
                        return null;
                    }
                    
                }
                else
                {
                    lastOperationStatus = true;
                }
                //insert gate in
                SystemValidation sysManualGateIn = SystemValidation.FindByKey("Manual_Gate_In");
                if (sysManualGateIn.IsActive)
                {
                    TruckLoading toGateIn = TruckLoading.FindByDeliveryOrdeNumber(toSave.DeliveryOrderNumber);
                    toGateIn.GateInTimestamp = DateTime.Now;
                    toGateIn.GateInMode = "Manual";
                    toGateIn.GateInLocation = "Loket-2";
                    toGateIn.ShipmentStatus = 2;
                    toGateIn.Update();
                }
                //send to COS
                SystemValidation sysCOSSync = SystemValidation.FindByKey("COS_Sync");
                if (sysCOSSync.IsActive) { 
                    try 
                    {
                        string url = GlobalClass.routesApi("DeliveryOrderList", "Update", "deliveryOrderNumber=" + toSave.DeliveryOrderNumber);
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                        request.Accept = "application/json";
                        request.ContentType = "application/json";
                        request.Method = "POST";
                        //remove updated by
                        DeliveryOrder updatedDo = DeliveryOrder.FindByShipmentNumber(toSave.DeliveryOrderNumber);
                        updatedDo.Status = "In Process";
                        string parsedContent = Newtonsoft.Json.JsonConvert.SerializeObject(updatedDo);
                        ASCIIEncoding encoding = new ASCIIEncoding();
                        Byte[] bytes = encoding.GetBytes(parsedContent);
                        request.ContentLength = bytes.Length;

                        Stream dataStream = request.GetRequestStream();
                        dataStream.Write(bytes, 0, bytes.Length);
                        dataStream.Close();

                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        string content = new StreamReader(response.GetResponseStream()).ReadToEnd();
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            lastOperationStatus = true;
                        }
                        else
                        {
                            lastOperationStatus = false;
                            lastErrorMessages.Add(string.Format("Error COS API : {0}", response.StatusDescription));
                            return null;
                        }
                    }
                    catch (Exception ex) {
                        lastOperationStatus = false;
                        lastErrorMessages.Add(string.Format("Error COS API : {0}", ex.Message));
                        return null;
                    }
                }

                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        public ActionResult GateIn(CFS.Models.TruckLoading toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.TruckLoadingId != 0)
                    {
                        CFS.Models.TruckLoading inserted = GateInImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        toReturn["success"] = false;
                    }
                    if (lastOperationStatus)
                    {
                        ss.Flush();
                    }
                    else
                    {
                        ts.VoteRollBack();
                    }
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                return new JsonNetResult() { Data = toReturn };
            }
        }

        private CFS.Models.TruckLoading GateInImpl(CFS.Models.TruckLoading toSave)
        {
            if (toSave.Validate("Gate In"))
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                CFS.Models.TruckLoading orig = CFS.Models.TruckLoading.FindById(toSave.TruckLoadingId);
                orig.GateInLocation = "CFS Web";
                orig.GateInMode = "Manual";
                orig.GateInUser = user.Username;
                orig.GateInTimestamp = DateTime.Now;
                orig.ShipmentStatus = 2;
                orig.UpdatedTimestamp = DateTime.Now;
                orig.UpdatedBy = user;
                orig.Update();

                lastOperationStatus = true;
                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        public ActionResult Filling(CFS.Models.TruckLoading toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.TruckLoadingId != 0)
                    {
                        CFS.Models.TruckLoading inserted = FillingImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        toReturn["success"] = false;
                    }
                    if (lastOperationStatus)
                    {
                        ss.Flush();
                    }
                    else
                    {
                        ts.VoteRollBack();
                    }
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                return new JsonNetResult() { Data = toReturn };
            }
        }

        public ActionResult Weighting(CFS.Models.TruckLoading toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.TruckLoadingId != 0)
                    {
                        CFS.Models.TruckLoading inserted = WeightingImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        toReturn["success"] = false;
                    }
                    if (lastOperationStatus)
                    {
                        ss.Flush();
                    }
                    else
                    {
                        ts.VoteRollBack();
                    }
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                return new JsonNetResult() { Data = toReturn };
            }
        }

        private CFS.Models.TruckLoading FillingImpl(CFS.Models.TruckLoading toSave)
        {
            if (toSave.Validate("Filling"))
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                CFS.Models.TruckLoading orig = CFS.Models.TruckLoading.FindById(toSave.TruckLoadingId);
                foreach (TruckLoadingPlanning tpNew in toSave.TruckLoadingPlanning)
                {
                    foreach (TruckLoadingPlanning tpOrig in orig.TruckLoadingPlanning)
                    {
                        if (tpNew.PlanningId == tpOrig.PlanningId)
                        {
                            tpOrig.Actual = tpNew.Actual;
                            //tpOrig.Actual2 = tpNew.Actual2;
                            tpOrig.Actual2 = tpNew.FlowmeterQuantity * tpNew.CorrectionFactor;
                            tpOrig.FlowmeterQuantity = tpNew.FlowmeterQuantity;
                            tpOrig.CorrectionFactor = tpNew.CorrectionFactor;
                            tpOrig.InitialWeight = tpNew.InitialWeight;
                            tpOrig.FinalWeight = tpNew.FinalWeight;
                            tpOrig.Update();

                            DeliveryOrderDetail dod = DeliveryOrderDetail.FindByDeliveryOrderNumber(tpOrig.DeliveryOrderNumber);
                            if (tpOrig.FillingPointGroup == "Chemical")
                            {
                                dod.ActualQuantity = Convert.ToInt32(tpOrig.FinalWeight - tpOrig.InitialWeight);
                                dod.UpdateAndFlush();
                            }
                            if (tpOrig.FillingPointGroup == "Fuel")
                            {
                                dod.ActualQuantity = Convert.ToSingle(tpOrig.Actual);
                                dod.UpdateAndFlush();
                            }
                        }
                    }
                }
                //insert gate out
                SystemValidation sysManualGateOut = SystemValidation.FindByKey("Manual_Gate_Out");
                if (sysManualGateOut.IsActive)
                {
                    orig.GateOutTimestamp = DateTime.Now;
                    orig.GateOutMode = "Manual";
                    orig.GateOutLocation = "Loket-3";
                    orig.ShipmentStatus = 3;
                }
                orig.LoadedTimestamp = DateTime.Now;
                orig.LoadedMode = "Manual";
                orig.LoadedLocation = "CFS Web";
                orig.LoadedUser = user.Username;
                orig.Update();

                lastOperationStatus = true;
                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.TruckLoading WeightingImpl(CFS.Models.TruckLoading toSave) //timbang isi only
        {
            if (toSave.Validate("Weighting"))
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                CFS.Models.TruckLoading orig = CFS.Models.TruckLoading.FindById(toSave.TruckLoadingId);
                foreach (TruckLoadingPlanning tpNew in toSave.TruckLoadingPlanning)
                {
                    //timbang isi via manual entry
                    foreach (TruckLoadingPlanning tpOrig in orig.TruckLoadingPlanning)
                    {
                        if (tpNew.PlanningId == tpOrig.PlanningId)
                        {
                            //tpOrig.Actual = tpNew.Actual;
                            //tpOrig.InitialWeight = tpNew.InitialWeight; //final weight only
                            tpOrig.FinalWeight = tpNew.FinalWeight;
                            tpOrig.Update();
                            if (tpOrig.FillingPointGroup == "Chemical")
                            {
                                DeliveryOrderDetail dod = DeliveryOrderDetail.FindByDeliveryOrderNumber(tpOrig.DeliveryOrderNumber);
                                dod.ActualQuantity = Convert.ToInt32(tpOrig.FinalWeight - tpOrig.InitialWeight);
                                dod.UpdateAndFlush();
                            }

                        }
                    }
                }
                orig.LoadedTimestamp = DateTime.Now;
                orig.LoadedMode = "Manual";
                orig.LoadedLocation = "CFS Web";
                orig.LoadedUser = user.Username;
                orig.Update();
                lastOperationStatus = true;
                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        public ActionResult GateOut(CFS.Models.TruckLoading toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.TruckLoadingId != 0)
                    {
                        CFS.Models.TruckLoading inserted = GateOutImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        toReturn["success"] = false;
                    }
                    if (lastOperationStatus)
                    {
                        ss.Flush();
                    }
                    else
                    {
                        ts.VoteRollBack();
                    }
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                return new JsonNetResult() { Data = toReturn };
            }
        }

        private CFS.Models.TruckLoading GateOutImpl(CFS.Models.TruckLoading toSave)
        {
            toSave = TruckLoading.FindById(toSave.TruckLoadingId);
            if (toSave.Validate("Gate Out"))
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                CFS.Models.TruckLoading orig = CFS.Models.TruckLoading.FindById(toSave.TruckLoadingId);
                orig.GateOutLocation = "CFS Web";
                orig.GateOutMode = "Manual";
                orig.GateOutUser = user.Username;
                orig.GateOutTimestamp = DateTime.Now;
                orig.ShipmentStatus = 3;
                orig.UpdatedTimestamp = DateTime.Now;
                orig.UpdatedBy = user;

                //update DO to completed
                DeliveryOrder dO = DeliveryOrder.FindByShipmentNumber(toSave.DeliveryOrderNumber);
                dO.Status = "Completed";
                dO.UpdatedTimestamp = DateTime.Now;

                //update DO status to Customer Order
                try
                {
                    //api to customer order
                    string url = GlobalClass.routesApi("DeliveryOrder", "Update", "DeliveryOrderNumber=" + dO.DeliveryOrderNumber);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                    request.Accept = "application/json";
                    request.ContentType = "application/json";
                    request.Method = "POST";
                    //remove updated by
                    toSave.UpdatedBy = null;
                    string parsedContent = Newtonsoft.Json.JsonConvert.SerializeObject(toSave);
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    Byte[] bytes = encoding.GetBytes(parsedContent);
                    request.ContentLength = bytes.Length;

                    Stream dataStream = request.GetRequestStream();
                    dataStream.Write(bytes, 0, bytes.Length);
                    dataStream.Close();

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    string content = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        orig.Update();
                        dO.Update();
                        lastOperationStatus = true;
                        return orig;
                    }
                    else
                    {
                        lastOperationStatus = false;
                        lastErrorMessages.Add(response.StatusDescription);
                        return null;
                    }
                }
                catch (Exception exc)
                {
                    lastOperationStatus = false;
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);
                    return null;
                }
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        [HttpPost]
        public ActionResult GetDeliveryOrderByStatus(string status)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                DeliveryOrder[] record = DeliveryOrder.FindByStatus(status);
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GenerateSeal(int number, int quantity)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                List<TruckLoadingSecuritySeal> record = new List<TruckLoadingSecuritySeal>();
                for (int i = 0; i < quantity; i++)
                {
                    TruckLoadingSecuritySeal tlss = new TruckLoadingSecuritySeal();
                    tlss.SealNumber = number + i;
                    tlss.SealTimestamp = DateTime.Now;
                    record.Add(tlss);
                }
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetInProcessTruckList(string shipmentStatus)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                TruckLoading[] record = TruckLoading.FindByShipmentStatus(Convert.ToInt32(shipmentStatus));
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetInProcessPlanningList(string shipmentStatus, string fillingPointGroup)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                ViewProcessedTruck[] record = ViewProcessedTruck.FindByShipmentStatus(Convert.ToInt32(shipmentStatus), fillingPointGroup);

                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetTruckLoadingPlanning(int id)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                TruckLoadingPlanning[] record = TruckLoadingPlanning.FindByTruckLoadingId(id);
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetTruckLoadingPlanningId(int id)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                TruckLoadingPlanning record = TruckLoadingPlanning.FindById(id);
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetWeight()
        {
            string ipAddress = System.Configuration.ConfigurationManager.AppSettings["WeightBridge"];
            int startAddress = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["WBStartAddress"]);
            int quantity = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["WBQuantityAddress"]);
            int port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["WBPort"]);
            ModbusClient modbus = new ModbusClient(ipAddress, port);
            Hashtable toReturn = new Hashtable();
            try
            {
                modbus.Connect();
                int[] response = modbus.ReadHoldingRegisters(startAddress, quantity);
                int low = (response[0] & 0xffff);
                int high = (int)((response[1] << 16) & 0xffff0000);
                int value = high | low;
                modbus.Disconnect();
                toReturn["records"] = value;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        public ActionResult Update(CFS.Models.TruckLoading toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.TruckLoadingId != 0)
                    {
                        CFS.Models.TruckLoading updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.TruckLoading UpdateImpl(CFS.Models.TruckLoading toSave)
        {
            if (toSave.Validate("Update"))
            {
                CFS.Models.TruckLoading orig = CFS.Models.TruckLoading.FindById(toSave.TruckLoadingId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                orig.LoadedCFSUser = toSave.LoadedCFSUser;
                orig.UpdatedBy = user;
                orig.UpdatedTimestamp = DateTime.Now;
                orig.Update();
                lastOperationStatus = true;
                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }
    }
}
