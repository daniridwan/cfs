﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using Newtonsoft.Json;

namespace CFS.Web.Controllers
{
    public class TruckController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu Truck"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                ViewBag.New = false;
                ViewBag.Update = false;
                if (Users.GetCurrentUser().HasPermission("Create Truck"))
                {
                    ViewBag.New = true;
                }
                if (Users.GetCurrentUser().HasPermission("Update Truck"))
                {
                    ViewBag.Update = true;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.Truck.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;

            return pq;
        }

        public ActionResult Save(CFS.Models.Truck toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.TruckId == 0)
                    {
                        CFS.Models.Truck inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.Truck updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.Truck InsertImpl(CFS.Models.Truck toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                toSave.CreatedTimestamp = DateTime.Now;
                toSave.CreatedBy = user;
                toSave.Save();
                //save truck compartment
                TruckCompartment toSaveCompartment = new TruckCompartment();
                IList<TruckCompartment> LtoSaveCompartment = new List<TruckCompartment>();
                foreach (TruckCompartment tc in toSave.TruckCompartment)
                {
                    tc.TruckId = toSave.TruckId;
                    tc.Save();
                }

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.Truck UpdateImpl(CFS.Models.Truck toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.Truck orig = CFS.Models.Truck.FindById(toSave.TruckId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                orig.RegistrationPlate = toSave.RegistrationPlate;
                orig.TruckType = toSave.TruckType;
                orig.TankType = toSave.TankType;
                orig.ChassisNumber = toSave.ChassisNumber;
                orig.TruckTransporter = toSave.TruckTransporter;
                orig.TruckTareWeight = toSave.TruckTareWeight;
                orig.TruckStnkNumber = toSave.TruckStnkNumber;
                orig.TruckStnkTimestamp = toSave.TruckStnkTimestamp;
                orig.TankMaxCapacity = toSave.TankMaxCapacity;
                orig.TankSafeCapacity = toSave.TankSafeCapacity;
                orig.UOM = toSave.UOM;
                orig.TruckBrand = toSave.TruckBrand;
                orig.EmergencyContact = toSave.EmergencyContact;

                orig.UpdatedBy = user;
                orig.UpdatedTimestamp = DateTime.Now;

                //child table
                foreach (TruckCompartment tcNew in toSave.TruckCompartment)
                {
                    if (tcNew.TruckId == null || tcNew.TruckId == 0)
                    {
                        //insert new compartment
                        tcNew.TruckId = toSave.TruckId;
                        tcNew.Save();
                        orig.TruckCompartment.Add(tcNew);
                    }
                    else
                    {
                        foreach (TruckCompartment tcOrig in orig.TruckCompartment)
                        {
                            if (tcNew.CompartmentId == tcOrig.CompartmentId)
                            {
                                //update existing compartment
                                tcOrig.MaxCapacity = tcNew.MaxCapacity;
                                tcOrig.SafeCapacity = tcNew.SafeCapacity;
                                tcOrig.Update();
                                //isDeleted = false;
                            }
                        }
                    }
                }

                //find deleted compartment
                List<TruckCompartment> deleted = new List<TruckCompartment>();
                foreach (TruckCompartment tc in orig.TruckCompartment)
                {
                    bool found = false;

                    foreach (TruckCompartment tcNew in toSave.TruckCompartment)
                    {
                        if (tc.CompartmentId == tcNew.CompartmentId)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        deleted.Add(tc);
                    }
                }

                try
                {
                    orig.Update();
                }
                catch (Exception exc)
                {
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);
                }

                foreach (TruckCompartment tcd in deleted)
                {
                    tcd.Delete();
                }

                lastOperationStatus = true;
                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        [HttpPost]
        public ActionResult GetTruck(string registrationPlate)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                Truck record = Truck.FindByRegistrationPlate(registrationPlate);
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetTruckById(int id)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                Truck record = Truck.FindById(id);
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }
    }
}
