﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using CFS.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;

namespace CFS.Web.Controllers
{
    public class NotificationController : Controller
    {
        //
        // GET: /Notification/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.Notification.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;

            return pq;
        }

        [HttpPost]
        public ActionResult ShowNotification(Telemetry telemetry)
        {
            try
            {
                var context = GlobalHost.ConnectionManager.GetHubContext<Hubs.UserFingerHub>();
                context.Clients.All.broadcastMessage(telemetry);
                try
                {
                    //save to notification table
                    Notification newNotification = new Notification();
                    newNotification.Title = telemetry.title;
                    newNotification.Icon = telemetry.icon;
                    newNotification.Text = telemetry.text;
                    newNotification.Status = telemetry.status;
                    newNotification.Url = telemetry.url;
                    newNotification.Timestamp = DateTime.Now;
                    newNotification.SaveAndFlush();
                    return Json(string.Format("Success: {0} ", telemetry));
                }
                catch (Exception ex)
                {
                    return Json(string.Format("Fail: {0} ", ex.Message));
                }

            }
            catch (Exception ex)
            {
                return Json(string.Format("Error: {0} ", ex.InnerException));
            }
        }
    }
}
