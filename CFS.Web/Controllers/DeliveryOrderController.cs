﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using System.Net;
using Newtonsoft.Json;

namespace CFS.Web.Controllers
{
    public class DeliveryOrderController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu Vessel Document"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                var now = DateTimeOffset.Now;
                ViewBag.Years = Enumerable.Range(0, 5).Select(i => now.AddYears(-i).ToString("yyyy"));
                Company[] company = Company.FindAll();
                ViewBag.Company = company;
                //ViewBag.New = false;
                ViewBag.Update = false;
                //if (Users.GetCurrentUser().HasPermission("DeliveryOrder_CREATE"))
                //{
                //    ViewBag.New = true;
                //}
                if (Users.GetCurrentUser().HasPermission("Update Delivery Order"))
                {
                    ViewBag.Update = true;
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.DeliveryOrder.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;

            return pq;
        }

        public ActionResult Save(CFS.Models.DeliveryOrder toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.DeliveryOrderId == 0)
                    {
                        CFS.Models.DeliveryOrder inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.DeliveryOrder updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.DeliveryOrder InsertImpl(CFS.Models.DeliveryOrder toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                toSave.CreatedTimestamp = DateTime.Now;
                toSave.CreatedBy = user;
                toSave.Save();

                foreach (DeliveryOrderDetail d in toSave.DeliveryOrderDetail)
                {
                    d.DeliveryOrderId = toSave.DeliveryOrderId;
                    d.Save();
                }

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.DeliveryOrder UpdateImpl(CFS.Models.DeliveryOrder toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.DeliveryOrder orig = CFS.Models.DeliveryOrder.FindById(toSave.DeliveryOrderId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());

                orig.UpdatedBy = user;
                orig.UpdatedTimestamp = DateTime.Now;

                //child table
                foreach (DeliveryOrderDetail ddNew in toSave.DeliveryOrderDetail)
                {
                    if (ddNew.DeliveryOrderId == null || ddNew.DeliveryOrderId == 0)
                    {
                        //insert new do
                        ddNew.DeliveryOrderDetailId = toSave.DeliveryOrderId;
                        ddNew.Save();
                        orig.DeliveryOrderDetail.Add(ddNew);
                    }
                    else
                    {
                        foreach (DeliveryOrderDetail ddOrig in orig.DeliveryOrderDetail)
                        {
                            if (ddNew.DeliveryOrderDetailId == ddOrig.DeliveryOrderDetailId)
                            {
                                //update existing compartment
                                ddOrig.CustomerReferenceNumber = ddNew.CustomerReferenceNumber;
                                ddOrig.ProductId = ddNew.ProductId;
                                ddOrig.TankName = ddNew.TankName;
                                ddOrig.Quantity = ddNew.Quantity;
                                ddOrig.UOM = ddNew.UOM;
                                ddOrig.ShipToAddress = ddNew.ShipToAddress;
                                ddOrig.Update();
                                //isDeleted = false;
                            }
                        }
                    }
                }

                //find deleted compartment
                List<DeliveryOrderDetail> deleted = new List<DeliveryOrderDetail>();
                foreach (DeliveryOrderDetail dd in orig.DeliveryOrderDetail)
                {
                    bool found = false;

                    foreach (DeliveryOrderDetail ddNew in toSave.DeliveryOrderDetail)
                    {
                        if (dd.DeliveryOrderDetailId == ddNew.DeliveryOrderDetailId)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        deleted.Add(dd);
                    }
                }

                orig.Update();
                lastOperationStatus = true;

                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        [HttpPost]
        public ActionResult GetTankByCompany(string companyId)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                int cId = Convert.ToInt32(companyId);
                Tank[] record = Tank.FindByCompany(cId);
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetProductByTank(string tankName)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                Tank tank = Tank.FindByName(tankName);
                Product record = Product.FindById(tank.ProductId);
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult GetDeliveryOrderByStatus(string status)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                DeliveryOrder[] record = DeliveryOrder.FindByStatus(status);
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        //validasi ketika administrasi
        [HttpPost]
        public ActionResult ValidateDeliveryOrder(string number)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                DeliveryOrder DO = DeliveryOrder.FindByShipmentNumber(number);
                bool result = true;
                //validasi status DO
                if (DO.Status != "Approved")
                {
                    result = false;
                    toReturn["success"] = false;
                    toReturn["message"] = string.Format("This delivery order already {0}", DO.Status);
                }
                //foreach (DeliveryOrderDetail d in DO.DeliveryOrderDetail)
                //{
                //    ViewCurrentFillingPoint[] fp = ViewCurrentFillingPoint.FindFillingPointByTankAndProduct(d.ProductId, d.TankName);
                //    if(fp.Length == 0)
                //    {
                //        result = false;
                //        toReturn["success"] = false;
                //        toReturn["message"] = string.Format("Filling Point for {0} for {1} not found.", d.ProductName, d.TankName);
                //    }

                //}
                if (result)
                {
                    DeliveryOrderDetail[] detail = DeliveryOrderDetail.FindByDeliveryOrderId(DO.DeliveryOrderId);
                    List<TruckLoadingPlanning> record = new List<TruckLoadingPlanning>();
                    foreach (DeliveryOrderDetail d in detail)
                    {
                        //ViewCurrentFillingPoint[] fp = ViewCurrentFillingPoint.FindFillingPointByTankAndProduct(d.ProductId, d.TankName);
                        TruckLoadingPlanning p = new TruckLoadingPlanning();
                        Product product = Product.FindById(d.ProductId);
                        p.DeliveryOrderNumber = d.DeliveryOrderNumber;
                        p.Preset = d.Quantity;
                        p.UOM = d.UOM;
                        p.ProductId = Product.FindById(d.ProductId);
                        p.ProductDensity = p.ProductId.ProductDensity;
                        //p.FillingPointName = fp[0].FillingPointName;
                        p.FillingPointGroup = product.ProductType;
                        p.TankName = d.TankName;
                        record.Add(p);
                    }
                    toReturn["deliveryOrder"] = DO;
                    toReturn["records"] = record;
                    toReturn["success"] = true;
                }
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPost]
        public ActionResult FillingPointSelection(long companyId, long productId)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                ViewCurrentFillingPoint[] fp = ViewCurrentFillingPoint.FindFillingPointByCompanyAndProduct(companyId, productId);
                toReturn["records"] = fp;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }
    }
}
