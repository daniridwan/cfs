﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using Newtonsoft.Json;
using System.Net;
using System.IO;

namespace CFS.Web.Controllers
{
    public class TankController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu Tank"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                Product[] product = Product.FindAll();
                ViewBag.Product = product;
                Company[] company = Company.FindAll();
                ViewBag.Company = company;
                ViewBag.New = false;
                ViewBag.Update = false;
                if (Users.GetCurrentUser().HasPermission("Create Tank"))
                {
                    ViewBag.New = true;
                }
                if (Users.GetCurrentUser().HasPermission("Update Tank"))
                {
                    ViewBag.Update = true;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.Tank.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;

            return pq;
        }

        public ActionResult Save(CFS.Models.Tank toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.TankId == 0)
                    {
                        CFS.Models.Tank inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.Tank updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    if (lastOperationStatus)
                    {
                        ss.Flush();
                        toReturn["success"] = lastOperationStatus;
                        toReturn["messages"] = lastErrorMessages;
                    }
                    else //fail
                    {
                        ts.VoteRollBack();
                        toReturn["success"] = false;
                        toReturn["messages"] = lastErrorMessages;
                    }
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.Tank InsertImpl(CFS.Models.Tank toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                toSave.CreatedTimestamp = DateTime.Now;
                toSave.Create();
                try
                {
                    //api to customer order
                    string url = GlobalClass.routesApi("Tank", "SaveInsert", "TankId=" + toSave.TankId);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                    request.Accept = "application/json";
                    request.ContentType = "application/json";
                    request.Method = "POST";
                    string parsedContent = Newtonsoft.Json.JsonConvert.SerializeObject(toSave);
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    Byte[] bytes = encoding.GetBytes(parsedContent);
                    request.ContentLength = bytes.Length;

                    Stream dataStream = request.GetRequestStream();
                    dataStream.Write(bytes, 0, bytes.Length);
                    dataStream.Close();

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    string content = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        toSave.CreatedBy = user;
                        toSave.Save();
                        lastOperationStatus = true;
                        return toSave;
                    }
                    else
                    {
                        lastOperationStatus = false;
                        lastErrorMessages.Add(response.StatusDescription);
                        return null;
                    }
                }
                catch (Exception exc)
                {
                    lastOperationStatus = false;
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);
                    return null;
                }
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.Tank UpdateImpl(CFS.Models.Tank toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.Tank orig = CFS.Models.Tank.FindById(toSave.TankId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                DateTime now = DateTime.Now;
                //update tank history first
                CFS.Models.TankHistory tankHistory = new CFS.Models.TankHistory();
                tankHistory.TankId = orig.TankId;
                tankHistory.Name = orig.Name;
                tankHistory.ProductId = orig.ProductId;
                tankHistory.CompanyId = orig.CompanyId;
                tankHistory.StartDate = orig.StartDate;
                tankHistory.ExpirationDate = orig.ExpirationDate;
                tankHistory.MeasurementLevel = orig.MeasurementLevel;
                tankHistory.IsPlb = toSave.IsPlb;
                tankHistory.Timestamp = now;

                //update table tank
                orig.Name = toSave.Name;
                orig.ProductId = toSave.ProductId;
                orig.CompanyId = toSave.CompanyId;
                orig.StartDate = toSave.StartDate;
                orig.ExpirationDate = toSave.ExpirationDate;
                orig.MeasurementLevel = toSave.MeasurementLevel;
                orig.IsPlb = toSave.IsPlb;
                orig.UpdatedBy = user;
                orig.UpdatedTimestamp = now;

                try
                {
                    //api to customer order
                    string url = GlobalClass.routesApi("Tank", "Save", "Tank=" + toSave.ProductId);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                    request.Accept = "application/json";
                    request.ContentType = "application/json";
                    request.Method = "POST";
                    //remove updated by
                    toSave.UpdatedBy = null;
                    string parsedContent = Newtonsoft.Json.JsonConvert.SerializeObject(toSave);
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    Byte[] bytes = encoding.GetBytes(parsedContent);
                    request.ContentLength = bytes.Length;

                    Stream dataStream = request.GetRequestStream();
                    dataStream.Write(bytes, 0, bytes.Length);
                    dataStream.Close();

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    string content = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        tankHistory.Save();
                        orig.Update();
                        lastOperationStatus = true;
                        return orig;
                    }
                    else
                    {
                        lastOperationStatus = false;
                        lastErrorMessages.Add(response.StatusDescription);
                        return null;
                    }
                }
                catch (Exception exc)
                {
                    lastOperationStatus = false;
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);
                    return null;
                }
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        [HttpPost]
        public ActionResult GetTank(long tankId)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                Tank record = Tank.FindById(tankId);
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }
    }
}
