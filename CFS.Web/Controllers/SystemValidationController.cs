﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using System.Net;
using Newtonsoft.Json;

namespace CFS.Web.Controllers
{
    public class SystemValidationController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu System Validation"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                ViewBag.Update = false;
                if (Users.GetCurrentUser().HasPermission("Update System Validation"))
                {
                    ViewBag.Update = true;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.SystemValidation.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;

            return pq;
        }
        public ActionResult Save(CFS.Models.SystemValidation toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.Id == 0)
                    {
                        CFS.Models.SystemValidation inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.SystemValidation updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.SystemValidation InsertImpl(CFS.Models.SystemValidation toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());

                toSave.Save();

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.SystemValidation UpdateImpl(CFS.Models.SystemValidation toSave)
        {

            if (toSave.Validate())
            {
                CFS.Models.SystemValidation orig = CFS.Models.SystemValidation.FindById(toSave.Id);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                orig.Name = toSave.Name;
                orig.Value = toSave.Value;
                orig.IsActive = toSave.IsActive;

                orig.Update();
                lastOperationStatus = true;

                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }
    }
}