﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using Newtonsoft.Json;
using System.Net;
using System.IO;

namespace CFS.Web.Controllers
{
    public class BerthingScheduleController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();
        private List<string> filesToDelete = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu Berthing Schedule"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                ViewBag.New = false;
                ViewBag.Update = false;
                if (Users.GetCurrentUser().HasPermission("Create Berthing Schedule"))
                {
                    ViewBag.New = true;
                }
                if (Users.GetCurrentUser().HasPermission("Update Berthing Schedule"))
                {
                    ViewBag.Update = true;
                }
                Vessel[] vessel = Vessel.FindAll();
                ViewBag.Vessel = vessel;
                Product[] product = Product.FindAll();
                ViewBag.Product = product;
                Company[] company = Company.FindAll();
                ViewBag.Company = company;
                LocalAgent[] localAgent = LocalAgent.FindAll();
                ViewBag.LocalAgent = localAgent;
                Port[] port = Port.FindAll();
                ViewBag.Port = port;

                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.VesselDocument.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }
                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {
            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }
                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }
                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }
                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }
                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }
            pq.SearchParam = nvc;
            return pq;
        }

        public ActionResult Save(CFS.Models.VesselDocument toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.VesselDocumentId == 0)
                    {
                        CFS.Models.VesselDocument inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.VesselDocument updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.VesselDocument InsertImpl(CFS.Models.VesselDocument toSave)
        {
            if (toSave.Validate("Scheduled"))
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                toSave.Status = "Scheduled";
                toSave.IsActive = 0;
                if (toSave.Filename != null)
                { 
                    UploadFile(toSave.Ticket, toSave.Filename);
                    toSave.Filename = toSave.Ticket + "_" + toSave.Filename;
                }
                toSave.CreatedTimestamp = DateTime.Now;
                toSave.CreatedBy = user;
                toSave.Save();

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.VesselDocument UpdateImpl(CFS.Models.VesselDocument toSave)
        {
            if (toSave.Validate("Scheduled"))
            {
                CFS.Models.VesselDocument orig = CFS.Models.VesselDocument.FindById(toSave.VesselDocumentId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                orig.VesselId = toSave.VesselId;
                orig.ProductId = toSave.ProductId;
                orig.BlQuantity = toSave.BlQuantity;
                orig.BlUom = toSave.BlUom;
                orig.CompanyId = toSave.CompanyId;
                orig.FromPort = toSave.FromPort;
                orig.ToPort = toSave.ToPort;
                orig.LocalAgent = toSave.LocalAgent;
                orig.IsActive = toSave.IsActive;
                orig.UpdatedBy = user;
                orig.UpdatedTimestamp = DateTime.Now;
                if (orig.Filename != toSave.Filename) {
                    UploadFile(toSave.Ticket, toSave.Filename);
                    orig.Filename = toSave.Ticket + "_" + toSave.Filename;
                }
                orig.Update();

                lastOperationStatus = true;
                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private void UploadFile(string ticket, string filename)
        {
            string tmpFolder = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), "__tmpUpload\\");
            string repositoryPath = System.Web.Configuration.WebConfigurationManager.AppSettings["RepositoryPath"];
            string filePath = Path.Combine(tmpFolder, ticket);
            filesToDelete.Add(filePath);

            if (Directory.Exists(repositoryPath))
            {
                string destFile = Path.Combine(repositoryPath, ticket);
                System.IO.File.Copy(filePath, destFile + "_" + filename, true);
            }
            else
            {
                Directory.CreateDirectory(repositoryPath);
                throw new Exception(string.Format("Repository path \"{0}\" not exists", repositoryPath));
            }

        }

    }
}
