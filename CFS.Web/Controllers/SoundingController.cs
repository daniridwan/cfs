﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using Newtonsoft.Json;
using System.Net;
using System.IO;

namespace CFS.Web.Controllers
{
    public class SoundingController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu Sounding"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                ViewBag.New = false;
                ViewBag.Update = false;
                if (Users.GetCurrentUser().HasPermission("Create Sounding"))
                {
                    ViewBag.New = true;
                }
                if (Users.GetCurrentUser().HasPermission("Update Sounding"))
                {
                    ViewBag.Update = true;
                }
                Product[] product = Product.FindAll();
                ViewBag.Product = product;
                Company[] company = Company.FindAll();
                ViewBag.Company = company;
                Tank[] tank = Tank.FindAll();
                ViewBag.Tank = tank;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.Sounding.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }
                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {
            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }
                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }
                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }
                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }
                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }
            pq.SearchParam = nvc;
            return pq;
        }

        public ActionResult Save(CFS.Models.Sounding toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.Id == 0)
                    {
                        CFS.Models.Sounding inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.Sounding updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    if (lastOperationStatus)
                    {
                        ss.Flush();
                        toReturn["success"] = lastOperationStatus;
                        toReturn["messages"] = lastErrorMessages;
                    }
                    else //fail
                    {
                        ts.VoteRollBack();
                        toReturn["success"] = false;
                        toReturn["messages"] = lastErrorMessages;
                    }
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.Sounding InsertImpl(CFS.Models.Sounding toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                toSave.CreatedTimestamp = DateTime.Now;
                toSave.CreatedBy = user;
                toSave.SaveAndFlush();

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.Sounding UpdateImpl(CFS.Models.Sounding toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.Sounding orig = CFS.Models.Sounding.FindById(toSave.Id);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                orig.TankId = toSave.TankId;
                orig.ProductId = toSave.ProductId;
                orig.CompanyId  = toSave.CompanyId;
                orig.SoundingDate = toSave.SoundingDate;
                orig.SoundingLevel = toSave.SoundingLevel;
                orig.Temperature = toSave.Temperature;
                orig.ProductTankVolume = toSave.ProductTankVolume;
                orig.ProductPipeVolume = toSave.ProductPipeVolume;
                orig.ProductSlopeVolume = toSave.ProductSlopeVolume;
                orig.Type = toSave.Type;
                orig.Remarks = toSave.Remarks;

                orig.UpdatedBy = user;
                orig.UpdatedTimestamp = DateTime.Now;

                orig.UpdateAndFlush();
                lastOperationStatus = true;

                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

    }
}
