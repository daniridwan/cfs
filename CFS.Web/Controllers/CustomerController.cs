﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using System.Net;
using Newtonsoft.Json;
using System.IO;

namespace CFS.Web.Controllers
{
    public class CustomerController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            ViewBag.New = false;
            ViewBag.Update = false;
            ViewBag.Download = false;
            if (Users.GetCurrentUser().HasPermission("Customer_CREATE"))
            {
                ViewBag.New = true;
            }
            if (Users.GetCurrentUser().HasPermission("Customer_UPDATE"))
            {
                ViewBag.Update = true;
            }
            if (Users.GetCurrentUser().HasPermission("Customer_DOWNLOAD"))
            {
                ViewBag.Download = true;
            }
            return View();
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.Customer.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;

            return pq;
        }

        public ActionResult Save(CFS.Models.Customer toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.CustomerId == 0)
                    {
                        CFS.Models.Customer inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.Customer updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.Customer InsertImpl(CFS.Models.Customer toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                toSave.CreatedTimestamp = DateTime.Now;
                toSave.CreatedBy = user;
                toSave.Save();

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.Customer UpdateImpl(CFS.Models.Customer toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.Customer orig = CFS.Models.Customer.FindById(toSave.CustomerId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                orig.CustomerCode = toSave.CustomerCode;
                orig.CustomerName = toSave.CustomerName;
                orig.CustomerAddress = toSave.CustomerAddress;
                orig.CustomerPhone = toSave.CustomerPhone;
                orig.CustomerCity = toSave.CustomerCity;
                orig.PrimaryContactNumber = toSave.PrimaryContactNumber;
                orig.SecondaryContactNumber = toSave.SecondaryContactNumber;

                orig.UpdatedBy = user;
                orig.UpdatedTimestamp = DateTime.Now;

                orig.Update();
                lastOperationStatus = true;

                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        //[HttpPost]
        //public ActionResult Download(Customer param)
        //{
        //    Hashtable toReturn = new Hashtable();

        //    //request param
        //    FDM.SAP.GetCustomer.dt_getCustomerRequest request = new FDM.SAP.GetCustomer.dt_getCustomerRequest();
        //    request.Ship_to_Code = param.ShipToCode;
        //    request.Sales_Organization = param.SalesOrganization;
        //    request.Distribution_Channel = param.DistributionChannel;

        //    //do work
        //    FDM.SAP.GetCustomer.mi_osgetCustomerService service = new FDM.SAP.GetCustomer.mi_osgetCustomerService();
        //    FDM.SAP.GetCustomer.dt_getCustomerResponse response = null;
        //    //Get Customer
        //    try
        //    {
        //        //create proxy & credential
        //        WebServiceConfiguration config = FDM.Models.WebServiceConfiguration.FindByName("GetCustomer");
        //        service.Proxy = WebProxy.GetDefaultProxy();
        //        service.Url = config.Url;
        //        service.Credentials = new System.Net.NetworkCredential(config.Username, config.Password);
        //        service.Timeout = config.Timeout;
        //        //process
        //        response = service.mi_osgetCustomer(request);

        //        foreach (FDM.SAP.GetCustomer.dt_getCustomerResponseMessage msg in response.Message)
        //        {
        //            if (msg.Type == "S")
        //            {
        //                //success
        //                //check if already exist
        //                Customer findCustomer = Customer.FindByShipToCode(param.ShipToCode);
        //                FDM.Models.SimpleUser user = FDM.Models.SimpleUser.CreateFromUser(FDM.Models.Users.GetCurrentUser());
        //                if (findCustomer == null) //new
        //                {
        //                    Customer customer = new Customer();
        //                    customer.ShipToName = response.Details[0].Ship_to_Description;
        //                    customer.ShipToCode = response.Details[0].Ship_to_Code;
        //                    customer.ShipToAddress = response.Details[0].Ship_to_Address;
        //                    customer.SoldToCode = response.Details[0].Sold_to_code;
        //                    customer.SoldToName = response.Details[0].Sold_to_description;
        //                    customer.CustomerGroup = response.Details[0].Customer_Group;
        //                    customer.CreatedTimestamp = DateTime.Now;
        //                    customer.CreatedBy = user;
        //                    customer.SaveAndFlush();
        //                    toReturn["message"] = string.Format("Customer {0} berhasil ditambahkan", param.ShipToCode);
        //                }
        //                else //update
        //                {
        //                    findCustomer.ShipToName = response.Details[0].Ship_to_Description;
        //                    findCustomer.ShipToCode = response.Details[0].Ship_to_Code;
        //                    findCustomer.ShipToAddress = response.Details[0].Ship_to_Address;
        //                    findCustomer.SoldToCode = response.Details[0].Sold_to_code;
        //                    findCustomer.SoldToName = response.Details[0].Sold_to_description;
        //                    findCustomer.CustomerGroup = response.Details[0].Customer_Group;
        //                    findCustomer.UpdatedTimestamp = DateTime.Now;
        //                    findCustomer.UpdatedBy = user;
        //                    findCustomer.UpdateAndFlush();
        //                    toReturn["message"] = string.Format("Customer {0} berhasil diupdate", param.ShipToCode);
        //                }
        //                toReturn["records"] = response.Details;
        //                toReturn["success"] = true;
        //            }
        //            else
        //            {
        //                toReturn["success"] = false;
        //                toReturn["message"] = msg.Desc_Msg;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string exception = "";
        //        if (ex.InnerException == null)
        //        {
        //            exception = ex.Message;
        //        }
        //        else
        //        {
        //            exception = ex.InnerException.ToString();
        //        }

        //        toReturn["success"] = false;
        //        toReturn["message"] = ex.ToString();
        //    }
        //    return new JsonNetResult() { Data = toReturn };
        //}

    }
}
