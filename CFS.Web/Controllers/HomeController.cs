﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CFS.Models;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;

namespace CFS.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                string loggedUser = HttpContext.User.Identity.Name.ToString();
                CFS.Models.Users getUser = CFS.Models.Users.GetCurrentUser();
                ViewBag.IsAuth = HttpContext.User.Identity.IsAuthenticated;
                return View();
            }
            else
            {
                Session["Username"] = "";
                Session["Rolename"] = "";
                return RedirectToAction("Index", "Login");
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        private bool DoLogin(string username, string password)
        {
            return CFS.Models.Users.Login(username, password);
        }
    }
}
