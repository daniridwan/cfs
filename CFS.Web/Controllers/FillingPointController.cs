﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using Newtonsoft.Json;

namespace CFS.Web.Controllers
{
    public class FillingPointController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu Filling Point"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                Tank[] tank = Tank.FindAll();
                ViewBag.Tank = tank;
                Pump[] pump = Pump.FindAll();
                ViewBag.Pump = pump;
                ViewBag.New = false;
                ViewBag.Update = false;
                if (Users.GetCurrentUser().HasPermission("Create Filling Point"))
                {
                    ViewBag.New = true;
                }
                if (Users.GetCurrentUser().HasPermission("Update Filling Point"))
                {
                    ViewBag.Update = true;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.FillingPoint.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }

                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {

            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }

                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }

                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }

                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }

                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }

            pq.SearchParam = nvc;

            return pq;
        }

        [HttpPost]
        public ActionResult GetFillingPoint(string fillingPointName)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                FillingPoint record = FillingPoint.FindByName(fillingPointName);
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        public ActionResult Save(CFS.Models.FillingPoint toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.FillingPointId == 0)
                    {
                        CFS.Models.FillingPoint inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.FillingPoint updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.FillingPoint InsertImpl(CFS.Models.FillingPoint toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                CFS.Models.Tank tank = CFS.Models.Tank.FindById(toSave.IdHelper);
                toSave.TankId = tank;
                toSave.CreatedTimestamp = DateTime.Now;
                toSave.CreatedBy = user;
                toSave.SaveAndFlush();

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.FillingPoint UpdateImpl(CFS.Models.FillingPoint toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.FillingPoint orig = CFS.Models.FillingPoint.FindById(toSave.FillingPointId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                CFS.Models.Tank tank = CFS.Models.Tank.FindById(toSave.IdHelper);
                orig.Name = toSave.Name;
                orig.TankId = tank;
                orig.PumpId = toSave.PumpId;
                orig.CorrectionFactor = toSave.CorrectionFactor;
                orig.ExpiredDate = toSave.ExpiredDate;

                orig.UpdatedBy = user;
                orig.UpdatedTimestamp = DateTime.Now;

                orig.UpdateAndFlush();
                lastOperationStatus = true;

                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        [HttpPost]
        public ActionResult GetFillingPointByCode(string productCode)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                FillingPoint[] record = FillingPoint.FindActiveByCode(productCode);
                toReturn["records"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

    }
}
