﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using Newtonsoft.Json;
using System.Net;
using System.IO;

namespace CFS.Web.Controllers
{
    public class PlbDocumentController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu PLB Document"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                ViewBag.New = false;
                ViewBag.Update = false;
                if (Users.GetCurrentUser().HasPermission("Create PLB Document"))
                {
                    ViewBag.New = true;
                }
                if (Users.GetCurrentUser().HasPermission("Update PLB Document") || Users.GetCurrentUser().HasPermission("Approve PLB Document"))
                {
                    ViewBag.Update = true;
                }
                Company[] company = Company.FindAll();
                ViewBag.Company = company;
                Vessel[] vessel = Vessel.FindAll();
                ViewBag.Vessel = vessel;
                Product[] product = Product.FindAll();
                ViewBag.Product = product;
                Tank[] tank = Tank.FindAll();
                ViewBag.Tank = tank;

                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.VesselDocument.SelectPagingPLB(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }
                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {
            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }
                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }
                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }
                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }
                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }
            pq.SearchParam = nvc;
            return pq;
        }

        public ActionResult Save(CFS.Models.VesselDocument toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.VesselDocumentId == 0)
                    {
                        CFS.Models.VesselDocument inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.VesselDocument updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.VesselDocument InsertImpl(CFS.Models.VesselDocument toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                
                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.VesselDocument UpdateImpl(CFS.Models.VesselDocument toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.VesselDocument orig = CFS.Models.VesselDocument.FindById(toSave.VesselDocumentId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());

                //child table
                foreach (PlbDocument plbNew in toSave.PlbDocument)
                {
                    if (plbNew.VesselDocumentId == null || plbNew.VesselDocumentId == 0)
                    {
                        //insert new compartment
                        plbNew.VesselDocumentId = toSave.VesselDocumentId;
                        if(plbNew.ApprovedTimestamp != null)
                        {
                            plbNew.ApprovedBy = user;
                        }
                        plbNew.Save();
                        orig.PlbDocument.Add(plbNew);
                    }
                    else
                    {
                        foreach (PlbDocument plbOrig in orig.PlbDocument)
                        {
                            if (plbNew.Id == plbOrig.Id)
                            {
                                //update existing compartment
                                plbOrig.PlbSppbDate = plbNew.PlbSppbDate;
                                plbOrig.PlbSppbNo = plbNew.PlbSppbNo;
                                plbOrig.PlbQuantity = plbNew.PlbQuantity;
                                if(plbOrig.ApprovedTimestamp != plbNew.ApprovedTimestamp)
                                {
                                    plbOrig.ApprovedTimestamp = plbNew.ApprovedTimestamp;
                                    plbOrig.ApprovedBy = user;
                                }
                                plbOrig.Update();
                                //isDeleted = false;
                            }
                        }
                    }
                }

                //find deleted compartment
                List<PlbDocument> deleted = new List<PlbDocument>();
                foreach (PlbDocument vt in orig.PlbDocument)
                {
                    bool found = false;

                    foreach (PlbDocument vtNew in toSave.PlbDocument)
                    {
                        if (vt.Id == vtNew.Id)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        deleted.Add(vt);
                    }
                }

                orig.Update();

                lastOperationStatus = true;
                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        [HttpPost]
        public ActionResult GetPlbDocument(string vesselDocumentId)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                PlbDocument[] record = PlbDocument.FindByVesselDocumentId(Convert.ToInt64(vesselDocumentId));
                toReturn["record"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }
    }
}
