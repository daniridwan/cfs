﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using Castle.ActiveRecord;
using System.Text.RegularExpressions;
using CFS.Models;
using Newtonsoft.Json;
using System.Net;
using System.IO;

namespace CFS.Web.Controllers
{
    public class VesselDocumentController : Controller
    {
        private bool lastOperationStatus;
        private List<string> lastErrorMessages = new List<string>();

        [Authorize]
        public ActionResult Index()
        {
            if (Users.GetCurrentUser().HasPermission("View Menu Vessel Document"))
            {
                if (Users.GetCurrentUser().IsAlreadyChangePassword == 0)
                {
                    return RedirectToAction("ChangePassword", "User");
                }
                ViewBag.New = false;
                ViewBag.Update = false;
                ViewBag.Approve = false;
                if (Users.GetCurrentUser().HasPermission("Create Vessel Document"))
                {
                    ViewBag.New = true;
                }
                if (Users.GetCurrentUser().HasPermission("Update Vessel Document"))
                {
                    ViewBag.Update = true;
                }
                if (Users.GetCurrentUser().HasPermission("Approve Vessel Document"))
                {
                    ViewBag.Approve = true;
                }
                Company[] company = Company.FindAll();
                ViewBag.Company = company;
                Vessel[] vessel = Vessel.FindAll();
                ViewBag.Vessel = vessel;
                Product[] product = Product.FindAll();
                ViewBag.Product = product;
                Tank[] tank = Tank.FindAll();
                ViewBag.Tank = tank;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "NotAuthorized");
            }
        }

        public ActionResult Page()
        {
            try
            {
                PagingQuery pq = ExtractQuery();
                PagingResult pr = CFS.Models.VesselDocument.SelectPaging(pq);
                return new JsonNetResult() { Data = pr };
            }
            catch (Exception exc)
            {
                while (exc.InnerException != null)
                {
                    exc = exc.InnerException;
                }
                PagingResult pr = new PagingResult();
                pr.Success = false;
                pr.Message = exc.Message;
                return new JsonNetResult() { Data = pr };
            }
        }

        private PagingQuery ExtractQuery()
        {
            PagingQuery pq = new PagingQuery();
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in Request.QueryString)
            {
                if (key.Contains("flt"))
                {
                    nvc.Add(key, Request.QueryString[key]);
                }
                if (key == "pageIndex")
                {
                    pq.PageIndex = int.Parse(Request.QueryString[key]);
                }
                if (key == "pageSize")
                {
                    pq.PageSize = int.Parse(Request.QueryString[key]);
                }
                if (key == "sord")
                {
                    pq.SortDirection = Request.QueryString[key].ToLower() == "asc" ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc;
                }
                if (key == "sidx")
                {
                    pq.SortColumn = Request.QueryString[key];
                }
            }
            pq.SearchParam = nvc;
            return pq;
        }

        public ActionResult Save(CFS.Models.VesselDocument toSave)
        {
            Hashtable toReturn = new Hashtable();
            using (SessionScope ss = new SessionScope(FlushAction.Never))
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (toSave.VesselDocumentId == 0)
                    {
                        CFS.Models.VesselDocument inserted = InsertImpl(toSave);
                        toReturn["record"] = inserted;
                    }
                    else
                    {
                        CFS.Models.VesselDocument updated = UpdateImpl(toSave);
                        toReturn["record"] = updated;
                    }
                    ss.Flush();
                    toReturn["success"] = lastOperationStatus;
                    toReturn["messages"] = lastErrorMessages;
                }
                catch (System.Security.SecurityException)
                {
                    ts.VoteRollBack();
                    lastErrorMessages.Add("Not Authenticated, please login. This may happen if too long inactivity.");

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
                catch (Exception exc)
                {
                    ts.VoteRollBack();
                    while (exc.InnerException != null)
                    {
                        exc = exc.InnerException;
                    }
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);

                    toReturn["success"] = false;
                    toReturn["messages"] = lastErrorMessages;
                    toReturn["record"] = null;
                }
            }
            return new JsonNetResult() { Data = toReturn };
        }

        private CFS.Models.VesselDocument InsertImpl(CFS.Models.VesselDocument toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                toSave.CreatedTimestamp = DateTime.Now;
                toSave.CreatedBy = user;
                toSave.Save();
                //save vessel tank
                VesselTank toSaveVesselTank = new VesselTank();
                IList<VesselTank> LtoSaveVesselTank = new List<VesselTank>();
                foreach (VesselTank vt in toSave.VesselTank)
                {
                    vt.VesselDocumentId = toSave.VesselDocumentId;
                    vt.Save();
                }
                if(toSave.Type == "Import PLB")
                {
                    //save vessel tank
                    PlbDocument toSavePlbDocument = new PlbDocument();
                    IList<PlbDocument> LtoSavePlbDocument = new List<PlbDocument>();
                    foreach (PlbDocument plb in toSave.PlbDocument)
                    {
                        plb.VesselDocumentId = toSave.VesselDocumentId;
                        plb.Save();
                    }
                }

                lastOperationStatus = true;
                return toSave;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        private CFS.Models.VesselDocument UpdateImpl(CFS.Models.VesselDocument toSave)
        {
            if (toSave.Validate())
            {
                CFS.Models.VesselDocument orig = CFS.Models.VesselDocument.FindById(toSave.VesselDocumentId);
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                orig.VesselNo = toSave.VesselNo;
                orig.Type = toSave.Type;
                orig.ArrivalDate = toSave.ArrivalDate;
                orig.BlNo = toSave.BlNo;
                orig.BlQuantity = toSave.BlQuantity;
                orig.BlUom = toSave.BlUom;
                orig.BlCost = toSave.BlCost;
                if(toSave.Type != "Import PLB")
                {
                    orig.SppbNo = toSave.SppbNo;
                    orig.SppbDate = toSave.SppbDate;
                }
                orig.PibNo = toSave.PibNo;
                orig.PibDate = toSave.PibDate;
                orig.Remarks = toSave.Remarks;
                orig.IsActive = toSave.IsActive;
                orig.UpdatedBy = user;
                if(orig.Status != "Approved")
                {
                    orig.Status = "Arrived";
                }
                orig.UpdatedTimestamp = DateTime.Now;

                //child table
                foreach (VesselTank vtNew in toSave.VesselTank)
                {
                    if (vtNew.VesselDocumentId == null || vtNew.VesselDocumentId == 0)
                    {
                        //insert new compartment
                        vtNew.VesselDocumentId = toSave.VesselDocumentId;
                        vtNew.Save();
                        orig.VesselTank.Add(vtNew);
                    }
                    else
                    {
                        foreach (VesselTank vtOrig in orig.VesselTank)
                        {
                            if (vtNew.VesselTankId == vtOrig.VesselTankId)
                            {
                                //update existing compartment
                                vtOrig.TankId = vtNew.TankId;
                                vtOrig.Quantity = vtNew.Quantity;
                                vtOrig.Spillage = vtNew.Spillage;
                                vtOrig.Update();
                                //isDeleted = false;
                            }
                        }
                    }
                }

                //find deleted compartment
                List<VesselTank> deleted = new List<VesselTank>();
                foreach (VesselTank vt in orig.VesselTank)
                {
                    bool found = false;

                    foreach (VesselTank vtNew in toSave.VesselTank)
                    {
                        if (vt.VesselTankId == vtNew.VesselTankId)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        deleted.Add(vt);
                    }
                }

                try
                {
                    orig.Update();
                }
                catch (Exception exc)
                {
                    lastErrorMessages.Add(exc.Message);
                    lastErrorMessages.Add(exc.StackTrace);
                }

                foreach (VesselTank vtd in deleted)
                {
                    vtd.Delete();
                }

                lastOperationStatus = true;
                return orig;
            }
            else
            {
                lastOperationStatus = false;
                lastErrorMessages = toSave.GetErrorMessages();
                return null;
            }
        }

        [HttpPost]
        public ActionResult GetVesselDocument(int companyId, int productId)
        {
            Hashtable toReturn = new Hashtable();
            try
            {
                ViewVesselDocumentTank[] record = ViewVesselDocumentTank.FindByCompanyAndProduct(Convert.ToInt64(companyId), Convert.ToInt64(productId));
                toReturn["record"] = record;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                while (ex.InnerException != null)
                {
                    exception = ex.InnerException.ToString();
                }
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }

        [HttpPut]
        public ActionResult Approval(long id, string notes, string response)
        {
            VesselDocument vd = VesselDocument.FindById(id);
            Hashtable toReturn = new Hashtable();
            try
            {
                CFS.Models.SimpleUser user = CFS.Models.SimpleUser.CreateFromUser(CFS.Models.Users.GetCurrentUser());
                vd.Approver = user;
                vd.ApprovalTimestamp = DateTime.Now;
                vd.Status = "Approved";
                vd.IsActive = 1;
                vd.Update();

                toReturn["records"] = vd;
                toReturn["success"] = true;
            }
            catch (Exception ex)
            {
                string exception = "";
                if (ex.InnerException == null)
                {
                    exception = ex.Message;
                }
                else
                {
                    exception = ex.InnerException.ToString();
                }
                toReturn["success"] = false;
                toReturn["message"] = exception;
            }
            return new JsonNetResult() { Data = toReturn };
        }
    }
}
