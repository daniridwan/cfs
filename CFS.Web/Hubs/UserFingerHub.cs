﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin;
using Owin;
using CFS.Models;

namespace CFS.Web.Hubs
{
    [HubName("userFingerHub")]
    public class UserFingerHub : Hub
    {
        public void Send(int userid, string message)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastMessage(userid, message);
        }

        public void SendNotification(Telemetry telemetry)
        {
            Clients.All.broadcastMessage(telemetry);
        }
    }
}