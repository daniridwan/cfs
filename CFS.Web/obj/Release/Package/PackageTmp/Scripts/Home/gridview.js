﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-truck-tracking').jqGrid(
        {
            pager: 'tbl-truck-tracking-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 10,
            rowList: [10, 15, 20],
            colNames: ['Queue No', 'Play', 'Registration Plate', 'DO Number', 'Status'],
            colModel:
		    [
                { name: 'QueueNumber', sortable: false, width: 80, hidden: false },
                { name: 'Speak', sortable: false, width: 40, hidden: false },
                { name: 'RegistrationPlate', width: 140, sortable: true },
                { name: 'DeliveryOrderNumber', width: 145, sortable: true },
                { name: 'Status', width: 100, sortable: true },
		    ],
            viewrecords: true,
            sortorder: "asc",
            sortname: "QueueNumber",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-truck-tracking').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
            emptyrecords: "No trucks in area",
        }).navGrid('#tbl-truck-tracking-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {    
            $('#tbl-truck-tracking')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-truck-tracking').setGridWidth($('#pnl-truck-tracking').width(),false).triggerHandler('resize');
    }
});

