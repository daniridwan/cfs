﻿CFS.View.GridViewDelivery = Backbone.View.extend(
    {
        initialize: function (el) {
            //C.Util.log(el);
            $(window).on("resize", this.OnWindowResize);
        },
        render: function () {
            var $this = this;

            this.gridObj = $('#tbl-delivery-order-realization').jqGrid(
                {
                    //pager: 'tbl-delivery-order-realization-pager',
                    datatype: function (postData) {
                        if ($this.newFilter) {
                            $this.newFilter = false;
                            postData.pageIndex = 1;
                        }
                        postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                        var param = $.extend({}, postData);
                        if ($this.filter) {
                            $.extend(param, $this.filter);
                        }
                        $this.trigger('GridView.PageRequest', param);
                    },

                    jsonReader: { id: 0, repeatitems: false },
                    rowNum: -1,
                    rowList: [],
                    colNames: ['No.', 'No Transaksi', 'No Polisi', 'Jam Keluar', 'Tangki', 'No SPPB', 'Kuantitas', 'Satuan', 'Total Harga (IDR)'],
                    colModel:
                        [
                            { name: 'No', sortable: false, width: 40, hidden: false },
                            { name: 'TransactionNumber', width: 105, sortable: false },
                            { name: 'RegistrationPlate', width: 105, sortable: false },
                            { name: 'GateOutTimestampText', index: 'GateOutTimestamp', width: 125, sortable: false },
                            { name: 'TankName', width: 90, sortable: false },
                            { name: 'Sppb', width: 80, sortable: false },
                            { name: 'QuantityText', index: 'Quantity', width: 100, sortable: false },
                            { name: 'UOM', width: 80, sortable: false },
                            { name: 'UnitPriceText', index: 'UnitPrice', width: 130, align:'right', sortable: false },
                            
                        ],
                    viewrecords: true,
                    sortorder: "asc",
                    sortname: "GateOutTimestamp",
                    height: 'auto',
                    multiselect: false,
                    shrinkToFit: false,
                    width: $('#pnl-frame').width() - 2,
                    altclass: 'even',
                    altRows: true,
                    pginput: false,
                    prmNames: { rows: 'pageSize', page: 'pageIndex' },
                }).navGrid('#tbl-delivery-order-realization-pager', {
                    edit: false,
                    add: false,
                    del: false,
                    search: false,
                    refresh: false,
                    pgbuttons: false,
                    pginput: false,
                    viewrecords: false
                }
                );
        },
        dataBind: function (toInject) {
            try {
                $('#tbl-delivery-order-realization')[0].addJSONData(toInject);
            }
            catch (err) {
                C.Util.log(err);
            }
        },
        setFilter: function (filter) {
            this.filter = filter;
            this.newFilter = true;
        },
        refresh: function () {
            this.gridObj.trigger('reloadGrid');
        },
        clear: function () {
            this.gridObj.clearGridData(true).trigger("reloadGrid");
        },
        OnWindowResize: function () {
            $('#tbl-delivery-order-realization').setGridWidth($('#pnl-frame').width(), false).triggerHandler('resize');
        }
    });

