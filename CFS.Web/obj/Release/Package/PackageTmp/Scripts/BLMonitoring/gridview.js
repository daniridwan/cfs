﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-vessel-document').jqGrid(
        {
            //pager: 'tbl-vessel-document-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: -1,
            rowList: [],
            colNames: ['No.', 'Tipe', 'No. BL', 'No. PIB', 'Tgl. PIB', 'Tgl. Masuk','Pemilik Barang', 'Produk', 'Kuantitas', 'Satuan', 'Total Harga (IDR)' ],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'Type', width: 75, sortable: false },
                { name: 'BlNo', width: 155, sortable: false },
                { name: 'PibNo', width: 100, sortable: false },
                { name: 'PibDateText', index: 'PibDate', width: 100, sortable: false },
                { name: 'ArrivalDateText', index: 'ArrivalDate', width: 100, sortable: false },
                { name: 'CompanyName', index: 'CompanyId', width: 150, sortable: false },
                { name: 'ProductName', index: 'ProductId', width: 150, sortable: false },
                { name: 'BlQuantityText', index:'BlQuantity', width: 100, sortable: false },
                { name: 'BlUom', width: 80, sortable: false },
                { name: 'BlCostText', index:'BlCost', width: 130, align:'right', sortable: false },
		],
            viewrecords: true,
            sortorder: "desc",
            sortname: "VesselNo",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: false,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-vessel-document-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pgbuttons: false,
            pginput: false,
            viewrecords: false
        }
            );
    },
    dataBind: function (toInject) {
        try {            
            $('#tbl-vessel-document')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    clear: function () {
        this.gridObj.clearGridData(true).trigger("reloadGrid");
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-vessel-document').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

