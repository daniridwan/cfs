﻿NetLPG.View.FilterView = Backbone.View.extend({
    events: {
        "change select": "onDropDownChanged",
        "keypress input[type='text']": "onTextKeypress",
        "click .cmd-reset-filter": "onFilterReset",
        "click .cmd-refresh": "onRefresh",
        "click .cmd-export": "onExport",
        "change #flt-timestamp-to":"onTimestampToChange",
        "change #flt-timestamp-from":"onTimestampFromChange",
    },
    render: function () {
        // don't forget to call ancestor 's render()
        C.FormView.prototype.render.call(this);
        var dateFrom = { altField:"#hdn-timestamp-from", altFormat: "yy-mm-ddT00:00:00",
            dateFormat: 'dd-MM-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true, maxDate: new Date
        };
        var dateTo = { altField:"#hdn-timestamp-to", altFormat: "yy-mm-ddT23:59:59",
            dateFormat: 'dd-MM-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true, maxDate: new Date
        };

        $(".datepicker").datepicker({
        beforeShow: function (input, inst) {
            setTimeout(function () {
                inst.dpDiv.css({
                    top: $(".datepicker").offset().top + 35,
                    left: $(".datepicker").offset().left
                });
            }, 0);
        }
        });

        $('#flt-timestamp-from').datepicker(_.extend(dateFrom, {}));
        $('#flt-timestamp-to').datepicker(_.extend(dateTo, {}));
    },
    onDropDownChanged: function (ev) {
        this.trigger('FilterView.Changed ', { param: this.getSearchParam() });
        return false;
    },
    onTextKeypress: function (ev) {
        if (ev.keyCode === 13) {
            this.trigger('FilterView.Changed', { param: this.getSearchParam() });
            return false;
        }
    },
    onFilterReset: function (ev) {
        C.clearField(this.$el);
        this.$('select').trigger('liszt:updated');
        this.trigger('FilterView.Changed ', { param: this.getSearchParam() });
        return false;
    },
    onRefresh: function (ev) {
        this.trigger('FilterView.Changed ', { param: this.getSearchParam() });
        return false;
    },
    onExport: function (ev) {
        this.trigger('FilterView.Export', { param: this.getSearchParam() });
        return false;
    },
    onTimestampFromChange: function (ev){
        var from = $('#hdn-timestamp-from').val();
        var to = $('#hdn-timestamp-to').val();
        if(from > to && to != '')
        {
            alert('Invalid date range, filter timestamp to cannot be greater than filter timestamp from.zzzz');
            $('#hdn-timestamp-to').val('');
            $('#flt-timestamp-to').val('');
        }
    },
    onTimestampToChange: function (ev){
        var from = $('#hdn-timestamp-from').val();
        var to = $('#hdn-timestamp-to').val();
        if(from > to)
        {
            alert('Invalid date range, filter timestamp to cannot be greater than filter timestamp from.');
            $('#hdn-timestamp-to').val('');
            $('#flt-timestamp-to').val('');
        }
    },
    /** General Function **/
    getSearchParam: function () {

        var value = this.$('form').formHash();
        for (var key in value) {
            value[key] = encodeURIComponent(value[key]);
        }
        return value;
    }
});