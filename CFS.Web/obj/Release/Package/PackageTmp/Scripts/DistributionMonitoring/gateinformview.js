﻿CFS.View.GateInFormView = C.FormView.extend(
{
    initialize: function () {
    },
    events: function () {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
            /* put additional event here */
            "click .cmd-back":"onBackClick",
        });
    },
    render: function () {

        // don't forget to call ancestor's render()
        C.FormView.prototype.render.call(this);

        var dateopt = { altFormat: "dd-mm-yy",
            dateFormat: 'd-M-yy', showOn: 'button', duration: 'fast', buttonImage: g_baseUrl + '/Images/calendar.png'
        };
        this.validator = this.$("form").validate({ ignore: ":hidden:not(.chosen-select)" });

        //ini mustinya tergantung permission
        this.enableEdit(false);
    },
    onEditClick: function (ev) {
        C.FormView.prototype.onEditClick.call(this, ev);
    },
    onRenderGateInTruck: function(arg){
        $('#ddl-truck-gate-in').val('').trigger("chosen:updated").find('option').remove().end();
        var param = { shipmentStatus: '1' };
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'DistributionMonitoring/GetInProcessTruckList',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(param),
            success: function (arg) {
                if (arg.success) {
                    var max = arg.records.length;                    
                    if(max > 0){
                        var ddlTruck = $("#ddl-truck-gate-in");
                        ddlTruck.append($("<option>", { value : '', text: ''}));
                        for (i = 0; i < max; i++) {
                            ddlTruck.append($("<option>", { value: arg.records[i].TruckLoadingId, text: arg.records[i].TruckRegistrationPlate }));
                        }
                        ddlTruck.trigger("chosen:updated");
                    }
                }
                else {
                    console.log(arg.message);
                }
            }
        });
    },
    onSaveClick: function (ev) {
        // ini override onSaveClick di ancestor
        var $this = this;
        if (this.validator.form() == false) {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-form').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else {
            this.$('.client-msg').hide();
        }
        // di sini mustinya ada validate
        // ...
        var toSave = this.$('.the-form').formHash();

        var objSave = new CFS.Model.TruckLoading();
        delete (toSave.TruckLoadingId);
        var TruckLoadingId = $("#ddl-truck-gate-in option:selected").val();
        toSave.TruckLoadingId = TruckLoadingId;
        Msg.show('Saving..');
        //modify default url
        objSave.url = g_baseUrl + 'DistributionMonitoring/GateIn?id=' + toSave.TruckLoadingId;
        objSave.save(toSave,
    	    {
                success: function (model, response) {
                if (response.success) {
                    $this.showThrobber(false);
    	            $this.model = objSave;
    	            $this.model.set(response.record);
    	            $this.dataBind($this.model);
    	            $this.makeReadOnly();
                    $this.renderServerMessage(response);
                    $this.trigger('GateInFormView.Updated', $this.model);
                    $(".chosen-select").chosen("destroy");
                    $(".chosen-select").hide();
                    $this.enableEditControl(false);
    	        }
    	        else {
    	            $this.renderServerMessage(response);
    	            Msg.hide();
    	        }
            },
            error: function (model, response) {
    	        $this.showThrobber(false);
    	        if (window.console) console.log(response);
    	    }
    	 });
    },
    prepareForInsert: function () {
        C.FormView.prototype.prepareForInsert.apply(this);
        $(".chosen-select").chosen({ width: "250px", search_contains: true, no_results_text: "Oops, nothing found!, ", allow_single_deselect: true });
        $('#lbl-truck-keur-weight').text('');
        $("#lbl-truck-safe-capacity").text('');
        $("#lbl-tank-tera-timestamp").text('');
        this.validator.resetForm();
        this.clearAuditTrail();
        this.enableEditControl(true);
    },
    dataBind: function (arg) {
        C.FormView.prototype.dataBind.apply(this, arguments);
        $('#txt-total-preset').val(arg.get('TotalPresetText'));
        if (arg.get('CreatedTimestamp') != null) {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
        if (arg.get('UpdatedTimestamp') != null) {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
    },
    //edit moved to edit form view
    //prepareForUpdate: function (arg) {
    //    this.dataBind(arg);
    //    this.validator.resetForm();
    //    this.makeReadOnly();
    //    this.enableEditControl(false);
    //},
    clearAuditTrail: function () {
        $('#lbl-insert-date').text('-');
        $('#lbl-insert-by').text('-');
        $('#lbl-last-updated-date').text('-');
        $('#lbl-last-updated-by').text('-');
    },
    enableEditControl: function (en) {
        if (en) {
            this.$('button.ui-datepicker-trigger').show();
        }
        else {
            this.$('button.ui-datepicker-trigger').hide();
        }
    },
    onBackClick: function (ev) {
        $(".chosen-select").chosen("destroy");
        window.location.hash = '#';
    },
});