﻿CFS.View.tDlgTruckLoadingSecuritySeal = 
{
    initialize: function(){
    },
    events: {
        "click .dlg-tool-cmd-save": "onOkClick",
        "click .dlg-tool-cmd-cancel": "onCancelClick",
    },
    render: function(){
        this.$el.jqm({
            modal: true
        });
        //
        this.$el.jqDrag('.dlg-title-block');
        this.validator = this.$el.validate({onsubmit:false});
        
    },
    clear:function()
    {
        C.clearField(this.$el);
        this.validator.resetForm();
    },
    show: function(en){
        this.$el[en ? 'jqmShow' : 'jqmHide']();
    },

    onCancelClick: function(ev){
        this.show(false);
    },
    onOkClick: function(ev){
        // musti validate selection
        if(!this.validator.form())
            return;
        
        var val = this.$el.formHash();
        
        if(this.currentRecord) // update
        {
            this.currentRecord.set(val,{silent:true});
            this.trigger('updated', this.currentRecord);
            this.show(false);
        
        }
        else // insert
        {
            var sealNumber = $("#txt-administration-security-seal").val(); //
            var total = $("#txt-administration-seal-quantity").val();
            var toSend =
            {
                number: sealNumber,
                quantity: total
            };
            var $this = this;
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'DistributionMonitoring/GenerateSeal',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(toSend),
                success: function (arg) {
                    if (arg.success) {
                        $this.trigger('created', arg); // jump to trigger defined in administrationformview.js
                    }
                    else {
                        alert('Error : ' +arg.message);
                    }
                }
            });
            this.show(false);
        }
        
    },
    prepareForUpdate: function(/*model*/arg){
        this.currentMode = 'update';
        this.currentRecord = arg;
        this.dataBind(arg.attributes);
    },
    dataBind: function(val)
    {
        this.$el.formHash(val);
    },
    prepareForInsert:function()
    {
        this.clear();
        this.currentMode = 'insert';
        this.currentRecord = null;
    }
};
CFS.View.DlgTruckLoadingSecuritySeal = Backbone.View.extend(CFS.View.tDlgTruckLoadingSecuritySeal);