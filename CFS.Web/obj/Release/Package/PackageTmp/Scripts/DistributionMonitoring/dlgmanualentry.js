﻿CFS.View.tDlgManualEntry = 
{
    initialize: function(){
        this.dlgManualEntry = null;
    },
    events: {
        "click .dlg-tool-cmd-save": "onOkClick",
        "click .dlg-tool-cmd-cancel": "onCancelClick",
    },
    render: function(){
        this.$el.jqm({
            modal: true
        });
        //
        this.$el.jqDrag('.dlg-title-block');

        var dateopt = { altFormat: "dd-mm-yy",
            dateFormat: 'dd-M-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true,
        };

        this.validator = this.$el.validate({onsubmit:false});
    },
    clear:function()
    {
        //digunakan untuk clear seluruh element
        //C.clearField(this.$el);
        $('#txt-mode').val('');
        this.validator.resetForm();
    },
    show: function(en, parent){
        this.$el[en ? 'jqmShow' : 'jqmHide']();
        this.dlgManualEntry = parent;
    },

    onCancelClick: function(ev){
        this.show(false);
    },
    onOkClick: function(ev){
        // musti validate selection
        if(!this.validator.form())
            return;
        var val = this.$el.formHash();
        if(val.Mode == "Administration")
        {
            this.dlgManualEntry.trigger('FilterView.Administration', {});
        }
        else if(val.Mode == "GateIn")
        {
            this.dlgManualEntry.trigger('FilterView.GateIn', {});  
        }
        else if(val.Mode == "Filling")
        {
            this.dlgManualEntry.trigger('FilterView.Filling', {});  
        }
        else if (val.Mode == "Weighting") {
            this.dlgManualEntry.trigger('FilterView.Weighting', {});
        }
        else if(val.Mode == "GateOut")
        {
            this.dlgManualEntry.trigger('FilterView.GateOut', {});
        }
        this.show(false);              
    },
    prepareForUpdate: function(/*model*/arg){
        this.currentMode = 'update';
        this.currentRecord = arg;
        this.dataBind(arg.attributes);
    },
    dataBind: function(val)
    {
        this.$el.formHash(val);
    },
    prepareForInsert:function()
    {
        this.clear();
        this.currentMode = 'insert';
        this.currentRecord = null;
    }
};
CFS.View.DlgManualEntry = Backbone.View.extend(CFS.View.tDlgManualEntry);