﻿CFS.Router.AppRouter = Backbone.Router.extend({
    initialize: function () {

        // create models
        this.truckLoadingColl = new CFS.Model.TruckLoadingList();

        // create views
        this.tabView = new CFS.View.TabView();
        this.gridView = new CFS.View.GridView();
        this.administrationformView = new CFS.View.AdministrationFormView({ el: $('#pnl-administration') });
        this.gateinformView = new CFS.View.GateInFormView({ el: $('#pnl-gate-in') });
        this.gateoutformView = new CFS.View.GateOutFormView({ el: $('#pnl-gate-out') });
        this.fillingformView = new CFS.View.FillingFormView({ el: $('#pnl-filling') });
        this.weightingformView = new CFS.View.WeightingFormView({ el: $('#pnl-weighting') });
        this.detailformView = new CFS.View.DetailFormView({ el: $('#pnl-detail') });
        this.filterView = new CFS.View.FilterView({ el: $('#pnl-search') });
        this.dlgManualEntry = new CFS.View.DlgManualEntry({ el: $('#dlg-manual-entry') });
        this.dlgLoadedCFSUser = new CFS.View.DlgLoadedCFSUser({ el: $('#dlg-loaded-cfs-user') });

        // setup event routes
        this.gridView.bind('GridView.PageRequest', this.onPageRequested, this);
        this.truckLoadingColl.bind('TruckLoading.PageReady', this.onPageReady, this);
        this.administrationformView.bind('FormView.Created', this.onCreateNew, this);
        this.administrationformView.bind('FormView.Updated', this.onUpdated, this);
        this.gateinformView.bind('GateInFormView.Created', this.onCreateNew, this);
        this.gateinformView.bind('GateInFormView.Updated', this.onUpdated, this);
        this.fillingformView.bind('FillingFormView.Created', this.onCreateNew, this);
        this.fillingformView.bind('FillingFormView.Updated', this.onUpdated, this);
        this.weightingformView.bind('WeightingFormView.Created', this.onCreateNew, this);
        this.weightingformView.bind('WeightingFormView.Updated', this.onUpdated, this);
        this.gateoutformView.bind('GateOutFormView.Created', this.onCreateNew, this);
        this.gateoutformView.bind('GateOutFormView.Updated', this.onUpdated, this);
        this.detailformView.bind('DetailFormView.Created', this.onCreateNew, this);
        this.detailformView.bind('DetailFormView.Updated', this.onUpdated, this);
        this.filterView.bind('FilterView.Changed', this.onFilterChanged, this);
        this.filterView.bind('FilterView.Export', this.onExportClick, this);
        this.filterView.bind('FilterView.ManualEntry', this.onManualEntryClick, this);
        this.on('FilterView.Administration', this.onAdministrationClick);
        this.on('FilterView.GateIn', this.onGateInClick);
        this.on('FilterView.Filling', this.onFillingClick);
        this.on('FilterView.Weighting', this.onWeightingClick);
        this.on('FilterView.GateOut', this.onGateOutClick);
        this.on('DlgLoadedCFSUser.ResetNavigation', this.onResetURL);
        this.on('DlgLoadedCFSUser.ReprintDO', this.reprintDeliveryReport, this);
    },

    routes: {
        "": "grid",
        "pnl-grid": "grid",
        "view/:id": "view",
        "administration": "administrationForm",
        "gatein": "gateinForm",
        "gateout": "gateoutForm",
        "filling": "fillingForm",
        "weighting": "weightingForm",
        //"queueTruck/:id": "printQueueTruck",
        //"weighingTicket/:id": "printWeighingTicket",
        "loadingInfo/:id": "printLoadingInfo",
        "productSample/:id": "printProductSample",
        "inspectionForm/:id": "printInspectionForm",
        "sealList/:id": "printSealList",
        "deliveryReport/:id": "printDeliveryReport",
        //"fillingReport/:id": "printFillingReport",
    },
    start: function () {
        this.tabView.render();
        this.filterView.render();
        this.gridView.render();
        this.administrationformView.render();
        this.gateinformView.render();
        this.fillingformView.render();
        this.weightingformView.render();
        this.gateoutformView.render();
        this.detailformView.render();
        this.dlgManualEntry.render();
        this.dlgLoadedCFSUser.render();
        Backbone.history.start();
    },
    view: function (id) {
        var user = this.truckLoadingColl.get(id);
        if (user) {
            this.detailformView.prepareForUpdate(user);
            this.detailformView.makeReadOnly();
            this.tabView.tabObj.click(6);
            $(window).scrollTop($('#pnl-detail').offset().top);
        }
        else
            this.navigate('');
    },
    onCreateNew: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    grid: function () {
        this.tabView.tabObj.click(0);
    },
    administrationForm: function () {
        this.administrationformView.prepareForInsert();
        this.administrationformView.clearAuditTrail();
        this.administrationformView.validator.resetForm();
        this.tabView.tabObj.click(1);
    },
    gateinForm: function (){
        this.gateinformView.prepareForInsert();
        this.gateinformView.onRenderGateInTruck();
        this.gateinformView.clearAuditTrail();
        this.gateinformView.validator.resetForm();
        this.tabView.tabObj.click(2);
    },
    fillingForm: function () {
        this.fillingformView.prepareForInsert();
        this.fillingformView.onRenderTruck();
        this.fillingformView.clearAuditTrail();
        this.fillingformView.validator.resetForm();
        this.tabView.tabObj.click(4);
    },
    weightingForm: function () {
        this.weightingformView.prepareForInsert();
        this.weightingformView.onRenderDO();
        this.weightingformView.clearAuditTrail();
        this.weightingformView.validator.resetForm();
        this.tabView.tabObj.click(5);
    },
    gateoutForm: function (){
        this.gateoutformView.prepareForInsert();
        this.gateoutformView.onRenderGateOutTruck();
        this.gateoutformView.clearAuditTrail();
        this.gateoutformView.validator.resetForm();
        this.tabView.tabObj.click(3);
    },
    onUpdated: function (arg) {
        this.gridView.refresh();
        this.navigate('view/' + arg.id, { replace: true });
    },
    onFilterChanged: function (filter) {
        this.gridView.setFilter(filter.param);
        this.gridView.refresh();
    },
    onPageRequested: function (arg) {
        Msg.show('Loading...');
        this.truckLoadingColl.fetchPage(arg);
    },
    onPageReady: function (arg) {
        Msg.hide();
        this.gridView.dataBind(arg);
    },
    printQueueTruck: function(id) {
        // redirect to another page
        var url = this.truckLoadingColl.url();
        url = url.replace(url, g_baseUrl + 'HttpHandlers/QueueTruckPdfExporter.ashx?id=' + id);
        window.open(url);
        this.navigate('');
    },
    printWeighingTicket: function(id) {
        // redirect to another page
        var url = this.truckLoadingColl.url();
        url = url.replace(url, g_baseUrl + 'HttpHandlers/WeighingTicketPdfExporter.ashx?id=' + id);
        window.open(url);
        this.navigate('');
    },
    printLoadingInfo: function(id) {
        // redirect to another page
        var url = this.truckLoadingColl.url();
        url = url.replace(url, g_baseUrl + 'HttpHandlers/LoadingInfoPdfExporter.ashx?id=' + id);
        window.open(url);
        this.navigate('');
    },
    printInspectionForm: function (id) {
        // redirect to another page
        var url = this.truckLoadingColl.url();
        url = url.replace(url, g_baseUrl + 'HttpHandlers/InspectionFormPdfExporter.ashx?id=' + id);
        window.open(url);
        this.navigate('');
    },
    printSealList: function (id) {
        // redirect to another page
        var url = this.truckLoadingColl.url();
        url = url.replace(url, g_baseUrl + 'HttpHandlers/SealListPdfExporter.ashx?id=' + id);
        window.open(url);
        this.navigate('');
    },
    printDeliveryReport: function(id) {
        // redirect to another page
        var user = this.truckLoadingColl.get(id);
        if (user.attributes.LoadedCFSUser != null) {
            var url = this.truckLoadingColl.url();
            url = url.replace(url, g_baseUrl + 'HttpHandlers/DeliveryReportPdfExporter.ashx?id=' + id);
            window.open(url);
            this.navigate('');
        }
        else {
            //popup to insert CFS Loading User
            this.dlgLoadedCFSUser.prepareForUpdate(user);
            this.dlgLoadedCFSUser.onRenderLoadedUser();
            this.dlgLoadedCFSUser.show(true, this);
        }
    },
    reprintDeliveryReport: function (object) {
        // redirect to another page
        var user = this.truckLoadingColl.get(object.id);
        var url = this.truckLoadingColl.url();
        url = url.replace(url, g_baseUrl + 'HttpHandlers/DeliveryReportPdfExporter.ashx?id=' + object.id);
        window.open(url);
        this.navigate('');
    },
    printProductSample: function (id) {
        // redirect to another page
        var url = this.truckLoadingColl.url();
        url = url.replace(url, g_baseUrl + 'HttpHandlers/SampleProductPdfExporter.ashx?id=' + id);
        window.open(url);
        this.navigate('');
    },
    printFillingReport: function(id) {
        // redirect to another page
        var url = this.truckLoadingColl.url();
        url = url.replace(url, g_baseUrl + 'HttpHandlers/FillingReportPdfExporter.ashx?id=' + id);
        window.open(url);
        this.navigate('');
    },
    onManualEntryClick: function (arg) {
        this.dlgManualEntry.clear();
        this.dlgManualEntry.show(true, this);
    },
    onAdministrationClick: function (){
        window.location.hash = '#administration';
    },
    onGateInClick: function (){
        window.location.hash = '#gatein';
    },
    onFillingClick: function (){
        window.location.hash = '#filling';
    },
    onWeightingClick: function () {
        window.location.hash = '#weighting';
    },
    onGateOutClick: function (){
        window.location.hash = '#gateout';
    },
    onExportClick: function (arg) {
        var url = this.truckLoadingColl.url();
        url = url.replace('DistributionMonitoring/Page','HttpHandlers/DistributionMonitoringExporter.ashx');
        window.open(url);
    },
    onResetURL: function () {
        this.navigate('');
    },
});

$(document).ready(function()
{
    CFS.AppRouter = new CFS.Router.AppRouter();
    CFS.AppRouter.start();
});
