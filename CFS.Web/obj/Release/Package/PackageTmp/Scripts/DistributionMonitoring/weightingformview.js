﻿CFS.View.WeightingFormView = C.FormView.extend(
{
    initialize: function () {
        
    },
    events: function () {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
            /* put additional event here */
            "click .cmd-back": "onBackClick",
            "change #ddl-weighting-delivery-order-number": "onChangeDO"
        });
    },
    render: function () {

        // don't forget to call ancestor's render()
        C.FormView.prototype.render.call(this);

        var dateopt = { altFormat: "dd-mm-yy",
            dateFormat: 'd-M-yy', showOn: 'button', duration: 'fast', buttonImage: g_baseUrl + '/Images/calendar.png'
        };
        this.validator = this.$("form").validate({ ignore: ":hidden:not(.chosen-select)" });

        //ini mustinya tergantung permission
        this.enableEdit(false);

        this.gridWeighting= new CFS.View.GridWeighting({ el: $('#tbl-weighting') });
        this.dlgWeighting = new CFS.View.DlgWeighting({ el: $('#dlg-weighting') });

        this.gridWeighting.render();
        this.dlgWeighting.render();
        
        this.gridWeighting.on('GridView.RequestView', this.onViewWeightingRequested, this);

        this.dlgWeighting.on('updated', this.onWeightingUpdated, this);
        //unused
        //this.gridFilling.on('GridView.PageRequest', this.onPageDeliveryOrderDetailRequested, this);
        //this.gridFilling.on('GridView.RequestDelete', this.onDeleteFillingRequested, this);
        //always show control
        this.gridWeighting.showControl(true);
    },
    onEditClick: function (ev) {
        C.FormView.prototype.onEditClick.call(this, ev);
    },
    onRenderDO: function(arg){
        $('#ddl-weighting-delivery-order-number').val('').trigger("chosen:updated").find('option').remove().end();
        var param = { shipmentStatus: '2', fillingPointGroup: 'Chemical' };
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'DistributionMonitoring/GetInProcessPlanningList',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(param),
            success: function (arg) {
                if (arg.success) {
                    var max = arg.records.length;
                    if(max > 0){
                        var ddlDO = $("#ddl-weighting-delivery-order-number");
                        ddlDO.append($("<option>", { value : '', text: ''}));
                        for (i = 0; i < max; i++) {
                            ddlDO.append($("<option>", { value : arg.records[i].PlanningId, text: arg.records[i].DeliveryOrderNumber}));
                        }
                        ddlDO.trigger("chosen:updated");
                    }
                }
                else {
                    console.log(arg.message);
                }
            }
        });
    },
    onSaveClick: function (ev) {
        // ini override onSaveClick di ancestor
        var $this = this;
        if (this.validator.form() == false) {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-form').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else {
            this.$('.client-msg').hide();
        }
        // di sini mustinya ada validate
        // ...
        var toSave = this.$('.the-form').formHash();
        toSave.TruckLoadingPlanning = CFS.Model.TruckLoadingPlanningList.toJSON();

        var objSave = new CFS.Model.TruckLoading();
        delete (toSave.TruckLoadingId);

        var TruckLoadingId = $('#hdn-truck-loading-id-weighting').val();
        toSave.TruckLoadingId = TruckLoadingId;
        Msg.show('Saving..');
        //modify default url
        objSave.url = g_baseUrl + 'DistributionMonitoring/Weighting?id=' + (TruckLoadingId ? TruckLoadingId : '0');
        objSave.save(toSave,
    	    {
                success: function (model, response) {
                if (response.success) {
                    $this.showThrobber(false);
    	            $this.model = objSave;
    	            $this.model.set(response.record);
    	            $this.dataBind($this.model);
    	            $this.makeReadOnly();
                    $this.renderServerMessage(response);
                    $this.trigger('WeightingFormView.Updated', $this.model);
                    $(".chosen-select").chosen("destroy");
                    $(".chosen-select").hide();
                    $this.enableEditControl(false);
    	        }
    	        else {
    	            $this.renderServerMessage(response);
    	            Msg.hide();
    	        }
            },
            error: function (model, response) {
    	        $this.showThrobber(false);
    	        if (window.console) console.log(response);
    	    }
    	 });
    },
    prepareForInsert: function () {
        C.FormView.prototype.prepareForInsert.apply(this);
        $(".chosen-select").chosen({ width: "250px", search_contains: true, no_results_text: "Oops, nothing found!, ", allow_single_deselect: true });
        this.validator.resetForm();
        this.clearAuditTrail();
        this.enableEditControl(true);

        CFS.Model.TruckLoadingPlanningList.reset();
        this.gridWeighting.dataBind(CFS.Model.TruckLoadingPlanningList.getFormattedArray());
    },
    dataBind: function (arg) {
        C.FormView.prototype.dataBind.apply(this, arguments);
        $('#txt-total-preset').val(arg.get('TotalPresetText'));
        if (arg.get('CreatedTimestamp') != null) {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
        if (arg.get('UpdatedTimestamp') != null) {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
    },
    //edit moved to edit form view
    //prepareForUpdate: function (arg) {
    //    this.dataBind(arg);
    //    this.validator.resetForm();
    //    this.makeReadOnly();
    //    this.enableEditControl(false);
    //},
    clearAuditTrail: function () {
        $('#lbl-insert-date').text('-');
        $('#lbl-insert-by').text('-');
        $('#lbl-last-updated-date').text('-');
        $('#lbl-last-updated-by').text('-');
    },
    enableEditControl: function (en) {
        if (en) {
            this.$('button.ui-datepicker-trigger').show();
            this.gridWeighting.showControl(true);
        }
        else {
            this.$('button.ui-datepicker-trigger').hide();
            this.gridWeighting.showControl(false);
        }
    },
    onBackClick: function (ev) {
        $(".chosen-select").chosen("destroy");
        window.location.hash = '#';
    },
    onChangeDO: function (ev) {
        var $this = this;
        var truckLoadingPlanningId = $('#ddl-weighting-delivery-order-number').val();
        //reset current model
        var record = [];
        CFS.Model.TruckLoadingPlanningList.reset();
        var toInject = CFS.Model.TruckLoadingPlanningList.getFormattedArray();
        this.gridWeighting.dataBind(toInject);
        //add new model
        if (truckLoadingPlanningId != '') {
            var param = { id: truckLoadingPlanningId };
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'DistributionMonitoring/GetTruckLoadingPlanningId',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(param),
                success: function (arg) {
                    if (arg.success) {
                        if (arg.records.InitialWeight === 0) {
                            alert('Transaksi ini belum melakukan timbang isi, silahkan menggunakan scanner yg tersedia di timbangan.');
                        }
                        else {
                            $this.onGridBinding(arg);
                            $('#hdn-truck-loading-id-weighting').val(arg.records.TruckLoadingId);
                        }
                    }
                    else {
                        console.log(arg.message);
                    }
                }
            });
        }
    },
    onGridBinding: function (arg) {
        //for (i = 0; i < arg.records.length; i++) {
            var val = new CFS.Model.TruckLoadingPlanning();
            //val.set(arg.records[i]);
            val.set(arg.records);
            //delete val.cid;
            CFS.Model.TruckLoadingPlanningList.push(val, { silent: true });
        //}
        var toInject = CFS.Model.TruckLoadingPlanningList.getFormattedArray();
        this.gridWeighting.dataBind(toInject);
    },
    onViewWeightingRequested: function (id) {
        var record = CFS.Model.TruckLoadingPlanningList.get(id);
        // deprecated in current backbone version
        //var record = CFS.Model.TruckCompartmentList.getByCid(id);
        this.dlgWeighting.prepareForUpdate(record);
        this.dlgWeighting.show(true, record.attributes.FillingPointGroup);
    },
    onWeightingUpdated: function (arg) {
        var toInject = CFS.Model.TruckLoadingPlanningList.getFormattedArray();
        //alert(toInject);
        this.gridWeighting.dataBind(toInject);
        this.dlgWeighting.show(false);
    },
});