﻿CFS.View.AdministrationFormView = C.FormView.extend(
{
    initialize: function () {
    },
    events: function () {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
            /* put additional event here */
            "change #txt-truck-registration-plate": "onChangeTruck",
            "click .cmd-back":"onBackClick",
            "click #btn-add-delivery-order": "onAddTruckLoadingPlanningClick",
            "click #btn-reset-delivery-order": "onResetTruckLoadingPlanningClick",
            "click #btn-add-truck-loading-security-seal": "onAddTruckSealClick",
        });
    },
    render: function () {

        // don't forget to call ancestor's render()
        C.FormView.prototype.render.call(this);

        this.gridTruckLoadingPlanning = new CFS.View.GridTruckLoadingPlanning({ el: $('#tbl-delivery-order') });
        this.dlgTruckLoadingPlanning = new CFS.View.DlgTruckLoadingPlanning({ el: $('#dlg-truck-loading-planning') });

        this.gridTruckLoadingSecuritySeal = new CFS.View.GridTruckLoadingSecuritySeal({ el: $('#tbl-truck-loading-security-seal') });
        this.dlgTruckLoadingSecuritySeal = new CFS.View.DlgTruckLoadingSecuritySeal({ el: $('#dlg-truck-loading-security-seal') });

        this.gridTruckLoadingPlanning.render();
        this.dlgTruckLoadingPlanning.render();
        this.gridTruckLoadingSecuritySeal.render();
        this.dlgTruckLoadingSecuritySeal.render();

        this.dlgTruckLoadingPlanning.on('created', this.onTruckLoadingPlanningCreated, this);
        this.dlgTruckLoadingPlanning.on('updated', this.onTruckLoadingPlanningUpdated, this);
        this.dlgTruckLoadingSecuritySeal.on('created', this.onTruckLoadingSecuritySealCreated, this);

        this.gridTruckLoadingPlanning.on('GridPlanningView.RequestDelete', this.onDeleteTruckLoadingPlanningRequested, this);
        this.gridTruckLoadingPlanning.on('GridPlanningView.RequestView', this.onViewTruckLoadingPlanningRequested, this);
        this.gridTruckLoadingPlanning.on('GridPlanningView.PageRequest', this.onPageDetailTruckLoadingPlanningRequested, this); // masih blm dipake

        this.gridTruckLoadingSecuritySeal.on('GridSealView.RequestDelete', this.onDeleteTruckLoadingSecuritySealRequested, this);
        this.gridTruckLoadingSecuritySeal.on('GridSealView.RequestView', this.onViewTruckLoadingSecuritySealRequested, this);
        this.gridTruckLoadingSecuritySeal.on('GridSealView.PageRequest', this.onPageDetailTruckLoadingSecuritySealRequested, this); // masih blm dipake

        var dateopt = { altFormat: "dd-mm-yy",
            dateFormat: 'd-M-yy', showOn: 'button', duration: 'fast', buttonImage: g_baseUrl + '/Images/calendar.png'
        };
        this.validator = this.$("form").validate({ ignore: ":hidden:not(.chosen-select)" });

        //ini mustinya tergantung permission
        this.enableEdit(false);
    },
    onEditClick: function (ev) {
        C.FormView.prototype.onEditClick.call(this, ev);
    },
    onSaveClick: function (ev) {
        // ini override onSaveClick di ancestor
        var $this = this;
        if (this.validator.form() == false) {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-administration').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else {
            this.$('.client-msg').hide();
        }
        // di sini mustinya ada validate
        // ...
        var toSave = this.$('.the-form').formHash();

        toSave.TruckLoadingPlanning = CFS.Model.TruckLoadingPlanningList.toJSON();
        toSave.TruckLoadingSecuritySeal = CFS.Model.TruckLoadingSecuritySealList.toJSON();

        var objSave = new CFS.Model.TruckLoading();
        delete (toSave.TruckLoadingId);
        Msg.show('Saving..');
        //modify default url
        objSave.url = g_baseUrl + 'DistributionMonitoring/Administration?id=' + (this.id ? this.id : '0');
        objSave.save(toSave,
    	    {
                success: function (model, response) {
                if (response.success) {
                    $this.showThrobber(false);
    	            $this.model = objSave;
    	            $this.model.set(response.record);
    	            $this.dataBind($this.model);
    	            $this.makeReadOnly();
                    $this.renderServerMessage(response);
                    $this.trigger('FormView.Updated', $this.model);
                    $(".chosen-select").chosen("destroy");
                    $(".chosen-select").hide();
                    $('#btn-reset-delivery-order').hide();
                    $this.enableEditControl(false);
    	        }
                else {
    	            $this.renderServerMessage(response);
    	            Msg.hide();
    	        }
            },
            error: function (model, response) {
    	        $this.showThrobber(false);
    	        if (window.console) console.log(response);
    	    }
    	 });
    },
    prepareForInsert: function () {
        C.FormView.prototype.prepareForInsert.apply(this);
        $(".chosen-select").chosen({ width: "350px", search_contains: true, no_results_text: "Oops, nothing found!, ", allow_single_deselect: true });
        $('#lbl-truck-tare-weight').text('');
        $("#lbl-truck-safe-capacity").text('');
        $("#lbl-tank-tera-timestamp").text('');
        $('#btn-reset-delivery-order').hide();
        this.validator.resetForm();
        this.clearAuditTrail();
        this.enableEditControl(true);

        CFS.Model.TruckLoadingPlanningList.reset();
        this.gridTruckLoadingPlanning.dataBind(CFS.Model.TruckLoadingPlanningList.getFormattedArray());

        CFS.Model.TruckLoadingSecuritySealList.reset();
        this.gridTruckLoadingSecuritySeal.dataBind(CFS.Model.TruckLoadingSecuritySealList.getFormattedArray());

    },
    dataBind: function (arg) {
        C.FormView.prototype.dataBind.apply(this, arguments);
        $('#txt-total-preset').val(arg.get('TotalPresetText'));
        if (arg.get('CreatedTimestamp') != null) {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
        if (arg.get('UpdatedTimestamp') != null) {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
    },
    //edit moved to edit form view
    //prepareForUpdate: function (arg) {
    //    this.dataBind(arg);
    //    this.validator.resetForm();
    //    this.makeReadOnly();
    //    this.enableEditControl(false);
    //},
    clearAuditTrail: function () {
        $('#lbl-insert-date').text('-');
        $('#lbl-insert-by').text('-');
        $('#lbl-last-updated-date').text('-');
        $('#lbl-last-updated-by').text('-');
    },
    enableEditControl: function (en) {
        if (en) {
            this.$('button.ui-datepicker-trigger').show();
            $('#btn-add-truck-loading-planning').show();
            $('#btn-add-truck-loading-security-seal').show();
            this.gridTruckLoadingPlanning.showControl(true);
            this.gridTruckLoadingSecuritySeal.showControl(true);
        }
        else {
            this.$('button.ui-datepicker-trigger').hide();
            $('#btn-add-truck-loading-planning').hide();
            $('#btn-add-truck-loading-security-seal').hide();
            this.gridTruckLoadingPlanning.showControl(false);
            this.gridTruckLoadingSecuritySeal.showControl(false);
        }
    },
    onChangeTruck: function (arg) {
        var regPlate = $("#txt-truck-registration-plate option:selected").text();
        if (regPlate != "") {
        var toSend =
        {
            registrationPlate: regPlate
        };
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'Truck/GetTruck',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(toSend),
                success: function (arg) {
                    if (arg.success) {
                        $("#lbl-truck-tare-weight").text(arg.records.TruckTareWeight);
                        $("#lbl-truck-safe-capacity").text(arg.records.TankSafeCapacity);
                        var TankTeraTimestamp = C.parseIsoDateTime(arg.records.TankTeraTimestamp);
                        $("#lbl-tank-tera-timestamp").text(TankTeraTimestamp.format('dd-mmm-yyyy'));
                    }
                    else {
                        alert(arg.message);
                    }
                }
            });
        }
        else {
            $("#lbl-truck-tare-weight").text("");
            $("#lbl-tank-tera-timestamp").text("");
            $("#lbl-truck-safe-capacity").text("");
        }
    },
    onBackClick: function (ev) {
        $(".chosen-select").chosen("destroy");
        $('#btn-add-delivery-order').show();
        window.location.hash = '#';
    },
    onAddTruckLoadingPlanningClick  : function(ev) {
        this.dlgTruckLoadingPlanning.prepareForInsert();
        this.dlgTruckLoadingPlanning.show(true);
    },
    onResetTruckLoadingPlanningClick: function (ev) {
        var record = [];
        CFS.Model.TruckLoadingPlanningList.reset();
        var toInject = CFS.Model.TruckLoadingPlanningList.getFormattedArray();
        this.gridTruckLoadingPlanning.dataBind(toInject);
        $('#btn-reset-delivery-order').hide();
        $('#btn-add-delivery-order').show();
        $("#txt-delivery-order-number").val('');
        $("#txt-delivery-order-number").text('');
        $("#hdn-administration-company-id").val('');
        $("#hdn-administration-company-id").text('');

    },
    onAddTruckSealClick : function(ev){
        this.dlgTruckLoadingSecuritySeal.prepareForInsert();
        this.dlgTruckLoadingSecuritySeal.show(true);
    },
    onTruckLoadingPlanningCreated:function(arg)
    {
        for (i = 0; i < arg.records.length; i++) {   
            var val = new CFS.Model.TruckLoadingPlanning();
            val.set(arg.records[i]);
            //delete val.cid;
            val.attributes.Preset = val.attributes.Preset.toFixed(1); 
            CFS.Model.TruckLoadingPlanningList.push(val, { silent: true });
        }
        var toInject = CFS.Model.TruckLoadingPlanningList.getFormattedArray();
        this.gridTruckLoadingPlanning.dataBind(toInject);
        this.dlgTruckLoadingPlanning.show(false);
        //hide add button, show reset button
        $('#btn-add-delivery-order').hide();
        $('#btn-reset-delivery-order').show();

    },
    onTruckLoadingPlanningUpdated:function(arg)
    {
        var toInject = CFS.Model.TruckLoadingPlanningList.getFormattedArray();
        //alert(toInject);
        this.gridTruckLoadingPlanning.dataBind(toInject);
        this.dlgTruckLoadingPlanning.show(false);
    },
    onDeleteTruckLoadingPlanningRequested:function(id)
    {
        var record = CFS.Model.TruckLoadingPlanningList.get(id);
        if (confirm('Are you sure want to delete planning no '+ record.get('idx'))) {
            CFS.Model.TruckLoadingPlanningList.remove(record);
            var toInject = CFS.Model.TruckLoadingPlanningList.getFormattedArray();
            this.gridTruckLoadingPlanning.dataBind(toInject);
        }
    },
    onViewTruckLoadingPlanningRequested:function(id)
    {
        var record = CFS.Model.TruckLoadingPlanningList.get(id);
        //change it to update vessel
        this.dlgTruckLoadingPlanning.prepareForUpdate(record);
        this.dlgTruckLoadingPlanning.show(true);
    },
    onTruckLoadingSecuritySealCreated:function(arg)
    {
        for (i = 0; i < arg.records.length; i++) {   
            var val = new CFS.Model.TruckLoadingSecuritySeal();
            val.set(arg.records[i]);
            //delete val.cid;
            CFS.Model.TruckLoadingSecuritySealList.push(val, { silent: true });
        }
        var toInject = CFS.Model.TruckLoadingSecuritySealList.getFormattedArray();
        this.gridTruckLoadingSecuritySeal.dataBind(toInject);
        this.dlgTruckLoadingSecuritySeal.show(false);
    },
    onDeleteTruckLoadingSecuritySealRequested:function(id)
    {
        var record = CFS.Model.TruckLoadingSecuritySealList.get(id);
        if (confirm('Are you sure want to delete seal no  '+ record.get('idx'))) {
            CFS.Model.TruckLoadingSecuritySealList.remove(record);
            var toInject = CFS.Model.TruckLoadingSecuritySealList.getFormattedArray();
            this.gridTruckLoadingSecuritySeal.dataBind(toInject);
        }
    },
    onViewTruckLoadingSecuritySealRequested:function(id)
    {
        var record = CFS.Model.TruckLoadingSecuritySealList.get(id);
        // deprecated in current backbone version
        //var record = CFS.Model.TruckCompartmentList.getByCid(id);
        this.dlgTruckLoadingSecuritySeal.prepareForUpdate(record);
        this.dlgTruckLoadingSecuritySeal.show(true);
    }
});