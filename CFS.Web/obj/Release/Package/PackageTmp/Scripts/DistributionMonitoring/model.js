﻿CFS = { Model: {}, View: {}, Router: {} };
//truck loading planning
CFS.Model.TruckLoadingPlanning = Backbone.Model.extend({
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    //idAttribute: 'PlanningId',
    idAttribute: 'cid',
    defaults:
    {
    },
    parse: function (arg) {
        return arg;
    },
    onChange: function () {
        var m = this.attributes;
        m.cid = this.cid;
        m.view = '<div class="ui-silk ui-silk-page-white-edit cmd-view" style="cursor:pointer;" title="Edit"></div>';
        m.del = '<div class="ui-silk ui-silk-cancel cmd-del" style="cursor:pointer;" title="Remove"></div>';
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'yyyy-d-mmm';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
    formatDollar: function (num) {
        return (
                parseFloat(num)
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            )
    },
});

CFS.Model.TruckLoadingPlanningList = Backbone.Collection.extend({
    initialize: function () {
        this.on('reset', this.onReset, this);
    },
    url: 'dummy.aspx',
    model: CFS.Model.TruckLoadingPlanning,
    getFormattedArray: function () {
        for (var i = 0, ii = this.length; i < ii; i++) {
            var m = this.at(i);
            m.set('idx', i + 1);
        }
        var toInject = {
            total: 1,
            page: 1,
            records: this.models.length,
            rows: this.toJSON()
        };
        return toInject;
    },
    onReset: function (arg) {
        for (var i = 0, ii = this.length; i < ii; i++) {
            var m = this.at(i);
            m.set('idx', i + 1);
            m.onChange();
        }
    }
});
CFS.Model.TruckLoadingPlanningList = new CFS.Model.TruckLoadingPlanningList();

//truck loading security seal
CFS.Model.TruckLoadingSecuritySeal = Backbone.Model.extend({
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    //idAttribute: 'SecuritySealId',
    idAttribute: 'cid',
    defaults:
    {
    },
    parse: function (arg) {
        return arg;
    },
    onChange: function () {
        var m = this.attributes;

        m.cid = this.cid;
        m.view = '<div class="ui-silk ui-silk-page-white-edit cmd-view" style="cursor:pointer;" title="Edit"></div>';
        m.del = '<div class="ui-silk ui-silk-cancel cmd-del" style="cursor:pointer;" title="Remove"></div>';
        m.SealTimestampText = this.date2text(m.SealTimestamp, 'd-mmm-yyyy HH:MM:ss');
        m.TotalPresetText = this.formatDollar(m.TotalPreset);
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'yyyy-d-mmm';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
    formatDollar: function (num) {
        return (
                parseFloat(num)
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            )
    },
});

CFS.Model.TruckLoadingSecuritySealList = Backbone.Collection.extend({
    initialize: function () {
        this.on('reset', this.onReset, this);
    },
    url: 'dummy.aspx',
    model: CFS.Model.TruckLoadingSecuritySeal,
    getFormattedArray: function () {
        for (var i = 0, ii = this.length; i < ii; i++) {
            var m = this.at(i);
            m.set('idx', i + 1);
        }
        var toInject = {
            total: 1,
            page: 1,
            records: this.models.length,
            rows: this.toJSON()
        };
        return toInject;
    },
    onReset: function (arg) {
        for (var i = 0, ii = this.length; i < ii; i++) {
            var m = this.at(i);
            m.set('idx', i + 1);
            m.onChange();
        }
    }
});
CFS.Model.TruckLoadingSecuritySealList = new CFS.Model.TruckLoadingSecuritySealList();

//truck loading
CFS.Model.TruckLoading = Backbone.Model.extend(
{
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    defaults:
    {
    },
    url: function () {
        return g_baseUrl + 'DistributionMonitoring/Save?id=' + (this.id ? this.id : '0');
    },
    idAttribute: 'TruckLoadingId',
    onChange: function () {
        var val = this.attributes;
        val.cid = this.cid;

        try {
            val.view = '<a href="#view/' + val.TruckLoadingId + '"><div class="ui-silk ui-silk-zoom" title="View"></div></a>';
            if (val.WeightInTimestamp == null && val.WeightOutTimestamp == null) {
                val.del = '<a href="#del/' + val.TruckLoadingId + '"><div class="ui-silk ui-silk-cancel cmd-del" style="cursor:pointer;" title="Remove"></div>';
            }
            if (val.TruckLoadingPlanning.length > 0) { //baru selesai administrasi
                val.loadingInfo = '<a href="#loadingInfo/' + val.TruckLoadingId + '"><div class="ui-silk ui-silk-printer" title="Sticker Pendaftaran"></div></a>';
                val.inspectionForm = '<a href="#inspectionForm/' + val.TruckLoadingId + '"><div class="ui-silk ui-silk-printer" title="Form Inspeksi"></div></a>';
                val.sealList = '<a href="#sealList/' + val.TruckLoadingId + '"><div class="ui-silk ui-silk-printer" title="Daftar Segel"></div></a>';
                val.productSample = '<a href="#productSample/' + val.TruckLoadingId + '"><div class="ui-silk ui-silk-printer" title="Sample Produk"></div></a>';
            }
            if (val.TruckLoadingPlanning.length > 0 && val.IsFinished) { //selesai pengisian atau gate out
                val.deliveryReport = '<a href="#deliveryReport/' + val.TruckLoadingId + '"><div class="ui-silk ui-silk-printer" title="Delivery Report"></div></a>';
            }
            if (val.ShipmentStatus == 1) {
                val.ShipmentStatusText = "Administration";
            }
            if (val.ShipmentStatus == 2) {
                val.ShipmentStatusText = "In Progress";
            }
            if (val.ShipmentStatus == 3) {
                val.ShipmentStatusText = "Completed";
            }
            if (val.IsGoodIssueSucceed == 0) {
                val.IsGoodIssueSucceedText = "No";
            }
            if (val.IsGoodIssueSucceed == 1) {
                val.IsGoodIssueSucceedText = "Yes";
            }
            
            val.TotalPresetText = this.formatDollar(val.TotalPreset);
            val.TruckKeurWeightText = this.formatDollar(val.TruckKeurWeight);
            val.AdministrationTimestampText = this.date2text(val.AdministrationTimestamp, 'd-mmm-yyyy HH:MM:ss');
            val.GateInTimestampText = this.date2text(val.GateInTimestamp, 'd-mmm-yyyy HH:MM:ss');
            val.WeightInTimestampText = this.date2text(val.WeightInTimestamp, 'd-mmm-yyyy HH:MM:ss');
            val.LoadedTimestampText = this.date2text(val.LoadedTimestamp, 'd-mmm-yyyy HH:MM:ss');
            val.WeightOutTimestampText = this.date2text(val.WeightOutTimestamp, 'd-mmm-yyyy HH:MM:ss');
            val.GateOutTimestampText = this.date2text(val.GateOutTimestamp, 'd-mmm-yyyy HH:MM:ss');
            val.GoodIssueTimestampText = this.date2text(val.GoodIssueTimestamp, 'd-mmm-yyyy HH:MM:ss');
        }
        catch (er) {
            C.Util.log(er);
        }
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'yyyy-mmm-d';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    },
    formatDollar: function (num) {
        return (
                parseFloat(num)
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            )
    },
});

CFS.Model.TruckLoadingList = Backbone.Collection.extend(
{
    initialize: function () {
        this.on('reset', this.onResetOrChange, this);
        this.on('change', this.onResetOrChange, this);
    },
    postData: {}, //from jqgrid
    totalRecords: 0,
    pageIndex: 0,
    pageSize: 15,
    model: CFS.Model.TruckLoading,
    url: function () {
        var xurl = g_baseUrl + 'DistributionMonitoring/Page?pageIndex=' + this.pageIndex + '&pageSize=' + this.pageSize;
        var query = [];
        for (var key in this.postData) {
            if (this.postData.hasOwnProperty(key) && key.indexOf('flt') != -1) {
                query.push("&" + key + "=" + this.postData[key]);
            }
        }
        if (this.postData.sidx && this.postData.sidx.length) { query.push("&sidx=" + this.postData.sidx); }
        if (this.postData.sord && this.postData.sord.length) { query.push("&sord=" + this.postData.sord); }
        return xurl + query.join('');
    },
    parse: function (arg) {
        if (!arg.success) {
            alert(arg.message);
            return;
        }

        var pageSize = parseInt(this.pageSize, 10);
        var pageIndex = parseInt(this.pageIndex, 10);
        var id = (pageSize * pageIndex) + 1;
        for (var i = 0, ii = arg.records.length; i < ii; i++) {
            arg.records[i].No = id + i;
        }
        this.totalRecords = arg.totalRecords;
        return arg.records;
    },
    date2text: function (dt) {
        if (dt) { return C.parseIsoDateTime(dt).format('d-mmm-yyyy'); }
        else { return ''; }
    },
    fetchPage: function (postData) {
        this.pageIndex = postData.pageIndex;
        this.pageSize = postData.pageSize;
        this.postData = postData;
        this.fetch({ reset: true });
    },
    onResetOrChange: function () {
        var idx = this.pageIndex * this.pageSize + 1;
        for (var i = 0, ii = this.length; i < ii; i++) {
            this.at(i).onChange();
        }
        var toInject = {
            total: Math.ceil(this.totalRecords / this.pageSize),
            page: this.pageIndex + 1,
            records: this.totalRecords,
            rows: this.toJSON()
        };
        try {
            this.trigger('TruckLoading.PageReady', toInject);
        } catch (ex) { }
    }
});