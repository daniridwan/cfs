﻿CFS.View.DetailFormView = C.FormView.extend(
{
    initialize: function () {
    },
    events: function () {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
            /* put additional event here */
            "click .cmd-back":"onBackClick",
        });
    },
    render: function () {

        // don't forget to call ancestor's render()
        C.FormView.prototype.render.call(this);

        this.gridTruckLoadingPlanning = new CFS.View.GridTruckLoadingPlanning({ el: $('#tbl-truck-loading-planning-detail') });
        this.gridTruckLoadingSecuritySeal = new CFS.View.GridTruckLoadingSecuritySeal({ el: $('#tbl-truck-loading-security-seal-detail') });
        //this.gridTruckLoadingRecord = new CFS.View.GridTruckLoadingRecord({ el: $('#tbl-truck-loading-record-detail') });

        this.gridTruckLoadingPlanning.render();
        this.gridTruckLoadingSecuritySeal.render();
        //this.gridTruckLoadingRecord.render();

        var dateopt = { altFormat: "dd-mm-yy",
            dateFormat: 'd-M-yy', showOn: 'button', duration: 'fast', buttonImage: g_baseUrl + '/Images/calendar.png'
        };
        this.validator = this.$("form").validate({ ignore: ":hidden:not(.chosen-select)" });

        //ini mustinya tergantung permission
        this.enableEdit(false);
    },
    onEditClick: function (ev) {
        C.FormView.prototype.onEditClick.call(this, ev);
    },
    onSaveClick: function (ev) {
        // ini override onSaveClick di ancestor
        var $this = this;
        if (this.validator.form() == false) {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-form').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else {
            this.$('.client-msg').hide();
        }
        // di sini mustinya ada validate
        // ...
        var toSave = this.$('.the-form').formHash();

        toSave.TruckLoadingPlanning = CFS.Model.TruckLoadingPlanningList.toJSON();
        toSave.TruckLoadingSecuritySeal = CFS.Model.TruckLoadingSecuritySealList.toJSON();

        var objSave = new CFS.Model.TruckLoading();
        delete (toSave.TruckLoadingId);
        Msg.show('Saving..');
        //modify default url
        objSave.url = g_baseUrl + 'DistributionMonitoring/Administration?id=' + (this.id ? this.id : '0');
        objSave.save(toSave,
    	    {
                success: function (model, response) {
                if (response.success) {
                    $this.showThrobber(false);
    	            $this.model = objSave;
    	            $this.model.set(response.record);
    	            $this.dataBind($this.model);
    	            $this.makeReadOnly();
                    $this.renderServerMessage(response);
                    $this.trigger('FormView.Updated', $this.model);
                    $(".chosen-select").chosen("destroy");
                    $(".chosen-select").hide();
                    $this.enableEditControl(false);
    	        }
    	        else {
    	            $this.renderServerMessage(response);
    	            Msg.hide();
    	        }
            },
            error: function (model, response) {
    	        $this.showThrobber(false);
    	        if (window.console) console.log(response);
    	    }
    	 });
    },
    prepareForInsert: function () {
        C.FormView.prototype.prepareForInsert.apply(this);
        $(".chosen-select").chosen({ width: "250px", search_contains: true, no_results_text: "Oops, nothing found!, ", allow_single_deselect: true });
        $('#lbl-truck-keur-weight').text('');
        $("#lbl-truck-safe-capacity").text('');
        $("#lbl-tank-tera-timestamp").text('');
        this.validator.resetForm();
        this.clearAuditTrail();
        this.enableEditControl(true);

        CFS.Model.TruckLoadingPlanningList.reset();
        this.gridTruckLoadingPlanning.dataBind(CFS.Model.TruckLoadingPlanningList.getFormattedArray());

        CFS.Model.TruckLoadingSecuritySealList.reset();
        this.gridTruckLoadingSecuritySeal.dataBind(CFS.Model.TruckLoadingSecuritySealList.getFormattedArray());

    },
    dataBind: function (arg) {
        C.FormView.prototype.dataBind.apply(this, arguments);
        $('#txt-total-preset').val(arg.get('TotalPresetText'));
        if (arg.get('CreatedTimestamp') != null) {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
        if (arg.get('UpdatedTimestamp') != null) {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
    },
    prepareForUpdate: function (arg) {
        this.dataBind(arg);
        this.validator.resetForm();
        this.makeReadOnly();
        this.enableEditControl(false);

        CFS.Model.TruckLoadingPlanningList.reset(arg.attributes.TruckLoadingPlanning);
        this.gridTruckLoadingPlanning.dataBind(CFS.Model.TruckLoadingPlanningList.getFormattedArray());
        CFS.Model.TruckLoadingSecuritySealList.reset(arg.attributes.TruckLoadingSecuritySeal);
        this.gridTruckLoadingSecuritySeal.dataBind(CFS.Model.TruckLoadingSecuritySealList.getFormattedArray());
        //CFS.Model.TruckLoadingRecordList.reset(arg.attributes.TruckLoadingRecord);
        //this.gridTruckLoadingRecord.dataBind(CFS.Model.TruckLoadingRecordList.getFormattedArray());
    },
    clearAuditTrail: function () {
        $('#lbl-insert-date').text('-');
        $('#lbl-insert-by').text('-');
        $('#lbl-last-updated-date').text('-');
        $('#lbl-last-updated-by').text('-');
    },
    enableEditControl: function (en) {
        if (en) {
            this.$('button.ui-datepicker-trigger').show();
            $('#btn-add-truck-loading-planning').show();
            $('#btn-add-truck-loading-security-seal').show();
            this.gridTruckLoadingPlanning.showControl(true);
            this.gridTruckLoadingSecuritySeal.showControl(true);
            //this.gridTruckLoadingRecord.showControl(true);
        }
        else {
            this.$('button.ui-datepicker-trigger').hide();
            $('#btn-add-truck-loading-planning').hide();
            $('#btn-add-truck-loading-security-seal').hide();
            this.gridTruckLoadingPlanning.showControl(false);
            this.gridTruckLoadingSecuritySeal.showControl(false);
            //this.gridTruckLoadingRecord.showControl(false);
        }
    },
    onBackClick: function (ev) {
        $(".chosen-select").chosen("destroy");
        window.location.hash = '#';
    },
    onTruckLoadingPlanningCreated:function(arg)
    {
        for (i = 0; i < arg.records.length; i++) {   
            var val = new CFS.Model.TruckLoadingPlanning();
            val.set(arg.records[i]);
            delete val.cid;
            CFS.Model.TruckLoadingPlanningList.push(val, { silent: true });
        }
        var toInject = CFS.Model.TruckLoadingPlanningList.getFormattedArray();
        this.gridTruckLoadingPlanning.dataBind(toInject);
        this.dlgTruckLoadingPlanning.show(false);
    },
    onTruckLoadingPlanningUpdated:function(arg)
    {
        var toInject = CFS.Model.TruckLoadingPlanningList.getFormattedArray();
        //alert(toInject);
        this.gridTruckLoadingPlanning.dataBind(toInject);
        this.dlgTruckLoadingPlanning.show(false);
    },
    onTruckLoadingSecuritySealCreated:function(arg)
    {
        for (i = 0; i < arg.records.length; i++) {   
            var val = new CFS.Model.TruckLoadingSecuritySeal();
            val.set(arg.records[i]);
            delete val.cid;
            CFS.Model.TruckLoadingSecuritySealList.push(val, { silent: true });
        }
        var toInject = CFS.Model.TruckLoadingSecuritySealList.getFormattedArray();
        this.gridTruckLoadingSecuritySeal.dataBind(toInject);
        this.dlgTruckLoadingSecuritySeal.show(false);
    }
});