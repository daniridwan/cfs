﻿CFS = { Model: {}, View: {}, Router: {} };
$.metadata.setType("attr", "validate");
//Role
CFS.Model.Role = Backbone.Model.extend(
{
    initialize: function () {
        this.on('change', this.onChange, this);
    },
    defaults:
    {
        InsertedDate: null,
        LastUpdatedDate: null,
        Visible: 1
    },
    url: function () {
        return g_baseUrl + 'Role/Save?RoleId=' + (this.id ? this.id : '0');
    },
    idAttribute: 'RoleId',
    onChange: function () {
        var val = this.attributes;
        try {
            val.view = '<a href="#view/' + val.RoleId + '" title="View"><div class="ui-silk ui-silk-zoom"></div></a>';
            val.RoleNameText = '<a href="#view/' + val.RoleId + '" title="View">' + val.Name + '</div></a>';
            val.del = '<a href="#delete/' + val.RoleId + '" title="Delete"><div class="ui-silk ui-silk-cross"></div></a>';

            if (typeof val.PermissionName == "undefined" || !(val.PermissionName instanceof Array)) {
                val.PermissionNameStr = val.PermissionName;
                var x = val.PermissionName.split(",");
                var arr = [];
                for (i = 0; i < x.length; i++) {
                    arr.push(x[i]);
                };
                val.PermissionName = arr
            } else if (val.PermissionName.length == 0) {
                var x = val.PermissionNameStr.split(",");
                var arr = [];
                for (i = 0; i < x.length; i++) {
                    arr.push(x[i]);
                };
                val.PermissionName = arr
            };
        }
        catch (er) {
            if (window.console) console.log(er);
        }
    },
    date2text: function (dt, fmt) {
        if (!fmt) fmt = 'd-mmm-yyyy';
        if (dt) { return C.parseIsoDateTime(dt).format(fmt); }
        else { return ''; }
    }
});

CFS.Model.RoleList = Backbone.Collection.extend(
{
    initialize: function () {
        this.on('reset', this.onResetOrChange, this);
        this.on('change', this.onResetOrChange, this);
    },
    postData: {}, //from jqgrid
    totalRecords: 0,
    pageIndex: 0,
    pageSize: 15,
    model: CFS.Model.Role,
    url: function () {
        var xurl = g_baseUrl + 'Role/Page?pageIndex=' + this.pageIndex + '&pageSize=' + this.pageSize;
        var query = [];
        for (var key in this.postData) {
            if (this.postData.hasOwnProperty(key) && key.indexOf('flt') != -1) {
                query.push("&" + key + "=" + this.postData[key]);
            }
        }
        if (this.postData.sidx && this.postData.sidx.length) { query.push("&sidx=" + this.postData.sidx); }
        if (this.postData.sord && this.postData.sord.length) { query.push("&sord=" + this.postData.sord); }
        query.push('&t=' + (new Date()).valueOf());
        return xurl + query.join('');
    },
    parse: function (arg) {
        if (!arg.success) {
            alert(arg.message);
            return;
        }

        var pageSize = parseInt(this.pageSize, 10);
        var pageIndex = parseInt(this.pageIndex, 10);
        var id = (pageSize * pageIndex) + 1;
        for (var i = 0, ii = arg.records.length; i < ii; i++) {
            arg.records[i].No = id + i;
        }
        this.totalRecords = arg.totalRecords;
        return arg.records;
    },
    date2text: function (dt) {
        if (dt) { return C.parseIsoDateTime(dt).format('d-mmm-yyyy'); }
        else { return ''; }
    },
    fetchPage: function (postData) {
        this.pageIndex = postData.pageIndex;
        this.pageSize = postData.pageSize;
        this.postData = postData;
        this.fetch({ reset: true });
    },
    onResetOrChange: function () {
        for (var i = 0, ii = this.length; i < ii; i++) {
            this.at(i).onChange();
        }
        var toInject = {
            total: Math.ceil(this.totalRecords / this.pageSize),
            page: this.pageIndex + 1,
            records: this.totalRecords,
            rows: this.toJSON()
        };
        try {
            this.trigger('Role.PageReady', toInject);
        } catch (ex) { }
    }
});