﻿CFS.View.FilterView = Backbone.View.extend({
    events: {
        "change select": "onDropDownChanged",
        "keypress input[type='text']": "onTextKeypress",
        "click .cmd-reset-filter": "onFilterReset",
        "click .cmd-refresh": "onRefresh",
        "click .cmd-export": "onExport",
        "click .cmd-exportpdf": "onExportPdf"
    },
    render: function () {
        // don't forget to call ancestor 's render()
        //C.FormView.prototype.render.call(this);
    },
    onDropDownChanged: function (ev) {
        this.trigger('FilterView.Changed', { param: this.getSearchParam() });
        return false;
    },
    onTextKeypress: function (ev) {
        if (ev.keyCode === 13) {
            this.trigger('FilterView.Changed', { param: this.getSearchParam() });
            return false;
        }
    },
    onFilterReset: function (ev) {
        C.clearField(this.$el);
        this.trigger('FilterView.Changed ', { param: this.getSearchParam() });
        return false;
    },
    onRefresh: function (ev) {
        this.trigger('FilterView.Changed', { param: this.getSearchParam() });
        return false;
    },
    /** General Function **/
    getSearchParam: function () {
        var value = this.$('form').formHash();
        for (var key in value) {
            value[key] = encodeURIComponent(value[key]);
        }
        return value;
    }
});