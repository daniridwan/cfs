﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-sounding').jqGrid(
        {
            pager: 'tbl-sounding-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', 'Sounding Date', 'Tank', 'Company', 'Product', 'Type', 'Sounding Level (mm)', 'Temperature', 'Product Tank Volume', 'Product Pipe Volume', 'Product Slope Volume', 'Remarks'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 18, sortable: false, hidden: false },
                { name: 'SoundingDateText', index: 'SoundingDate', width: 120, sortable: true },
                { name: 'TankName', width: 105, sortable: false },
                { name: 'CompanyName', width: 130, sortable: false },
                { name: 'ProductName', width: 120, sortable: false },
                { name: 'Type', width: 120, sortable: false },
                { name: 'SoundingLevel', width: 150, sortable: true },
                { name: 'Temperature', width: 100, sortable: true },
                { name: 'ProductTankVolume', width: 155, sortable: true },
                { name: 'ProductPipeVolume', width: 155, sortable: true },
                { name: 'ProductSlopeVolume', width: 155, sortable: true },
                { name: 'Remarks', width: 125, sortable: true },
		],
            viewrecords: true,
            sortorder: "desc",
            sortname: "SoundingDate",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-sounding-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {            
            $('#tbl-sounding')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-sounding').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

