/*jslint passfail: false, white: false, onevar: false, browser: true, nomen: false*/

/*global jQuery: false, $j: true, Cinovasi: false, C: false, window: false,
    alert: false, console: false, Class: false, $p: false, confirm: false,
    DmsAPI:false, jsonPath:false */

C.Uploader = Backbone.View.extend(
{
    initialize: function (opt) {
        this.opt = $j.extend({}, this.defaultOpt, opt);
    },
    defaultOpt:
    {
        id: '',
        selector: 'body',
        folderId: 0,
        name: Math.random().toString().substr(2),
        allowedExtensions: ['doc', 'xls','csv', 'ppt', 'docx', 'xlsx', 'pptx', 'jpg', 'gif', 'png', 'bmp', 'pdf', 'txt', 'zip','rar'],
        //sizeLimit:3145728
        sizeLimit: 100000
    },
    setup: function () {
        this.opt.context = this;
        this.opt.onSubmit = this.onSubmit;
        this.opt.onComplete = this.onComplete;
        this.opt.button = $j(this.opt.selector)[0];
        this.opt.action = g_baseUrl + 'HttpHandlers/UploadHandler.ashx';
        this.valumsUploader = new qq.FileUploaderBasic(this.opt);
    },
    onSubmit: function (id, filename) {
        //fungsi ini dijalankan di context uploader option
        var $this = this.context;
        this.params.folderId = $this.opt.folderId;
        $this.trigger('evUploader_StatusStartUpload', { id: $this.opt.id });
    },
    onComplete: function (id, filename, response) {
        //fungsi ini dijalankan di context uploader option
        var $this = this.context;
        response.id = $this.opt.id
        if (response.success) {
            $this.trigger('evUploader_StatusUploadOK', response);
        }
        else {
            $this.trigger('evUploader_StatusUploadError', response);
        }
    },
    show: function (en) {
        $j(this.opt.selector)[en ? 'show' : 'hide']();
    }
});

C.UploadCommitter = Backbone.View.extend(
{
    init:function()
    {
        this._super();
        this.records = {};
    },
    fini:function()
    {
        this._super();
    },
    addRecord: function(record)
    {
        var ticket = record.ticket;
        if(ticket && ticket.length === 36)
        {
            this.records[record.ticket] = record;
        }
    },
    removeRecord: function(ticket)
    {
        if(ticket && ticket.length === 36)
        {
            delete this.records[ticket];
        }
    },
    commitUpload:function(invoker)
    {
        var $this = this;
        var tickets = [];
        for(var ticket in this.records)
        {
            tickets.push(ticket);
        }

        if(tickets.length)
        {
            DmsAPI.rpc.commitUpload(tickets,
            function(arg)
            {
                if(!arg.error)
                {
                    for(var i = 0, ii = arg.result.length; i < ii; i+=1)
                    {
                        var ticket = arg.result[i].ticket;
                        var record = $this.records[ticket];
                        if(record)
                        {
                            record.IdDms = arg.result[i].documentId;
                        }
                    }
                    $this.records = {};
                    $this.trigger('evUploader_StatusCommitted',{invoker:invoker});
                }
                else
                {
                    alert(arg.error);
                }
            },
            function(arg)
            {
                alert(JSON.stringify(arg));
            }).call(C.channelHelper);
        }
        else
        {
            this.trigger('evUploader_StatusCommitted',{invoker:invoker});
        }
    }
});


