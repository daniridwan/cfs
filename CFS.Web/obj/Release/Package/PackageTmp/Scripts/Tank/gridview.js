﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-tank-configuration').jqGrid(
        {
            pager: 'tbl-tank-configuration-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', 'Tank Name', 'Product Name', 'Company', 'Start Date', 'Expiration Date' ],
            colModel:
		    [
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 18, sortable: false, hidden: false },
                { name: 'Name', width: 120, sortable: true },
                { name: 'ProductName', index: 'ProductId', width: 120, sortable: true },
                { name: 'CompanyName', index: 'CompanyId', width: 140, sortable: true },
                { name: 'StartDateText', index: 'StartDate', width: 140, sortable: true },
                { name: 'ExpirationDateText', index: 'ExpirationDate', width: 140, sortable: true },
		    ],
            viewrecords: true,
            sortorder: "asc",
            sortname: "TankId",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-tank-configuration-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {    
            $('#tbl-tank-configuration')[0].addJSONData(toInject);        
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
        var val = this.Attributes;
    },
    OnWindowResize : function()
    {
        $('#tbl-tank-configuration').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

