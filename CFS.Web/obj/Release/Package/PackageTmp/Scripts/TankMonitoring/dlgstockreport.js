﻿CFS.View.tDlgStockReport = 
{
    initialize: function(){
    },
    events: {
        "click .dlg-tool-cmd-save": "onOkClick",
        "click .dlg-tool-cmd-cancel": "onCancelClick",
    },
    render: function(){
        this.$el.jqm({
            modal: true
        });
        //
        this.$el.jqDrag('.dlg-title-block');

        var dateopt = { altFormat: "dd-mm-yy",
            dateFormat: 'dd-MM-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true, maxDate: new Date
        };

        $(".datepicker").datepicker({
            beforeShow: function (input, inst) {
            setTimeout(function () {
                inst.dpDiv.css({
                top: $(".datepicker").offset().top + 35,
                left: $(".datepicker").offset().left
                });
            }, 0);
        }
        }); 

        this.validator = this.$el.validate({onsubmit:false});
    },
    clear:function()
    {
        //digunakan untuk clear seluruh element
        //C.clearField(this.$el);
        $('#txt-dlg-tank').val('');
        $('#txt-dlg-month').val('');
        $('#txt-dlg-year').val('');
        this.validator.resetForm();
    },
    show: function(en){
        this.$el[en ? 'jqmShow' : 'jqmHide']();
    },

    onCancelClick: function(ev){
        this.show(false);
    },
    onOkClick: function(ev){
        // musti validate selection
        if(!this.validator.form())
            return;
        var val = this.$el.formHash();
        Msg.show('Generating...');
        var url = g_baseUrl + 'HttpHandlers/StockMonitoringExporter.ashx?Tank='+ val.Tank +'&Period=' + val.Month + '-' +val.Year;
        window.open(url);
        Msg.hide();
        this.show(false);              
    },
    prepareForUpdate: function(/*model*/arg){
        this.currentMode = 'update';
        this.currentRecord = arg;
        this.dataBind(arg.attributes);
    },
    dataBind: function(val)
    {
        this.$el.formHash(val);
    },
    prepareForInsert:function()
    {
        this.clear();
        this.currentMode = 'insert';
        this.currentRecord = null;
    },
    date2text: function (dt, fmt) {
        if (dt) { return C.parseIsoDateTime(dt).format('d-mmm-yyyy'); }
        else { return ''; }
    }
};
CFS.View.DlgStockReport = Backbone.View.extend(CFS.View.tDlgStockReport);