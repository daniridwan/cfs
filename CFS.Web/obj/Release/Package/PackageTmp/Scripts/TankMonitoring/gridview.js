﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-stock-monitoring').jqGrid(
            {
                pager: 'tbl-stock-monitoring-pager',
                datatype: function (postData) {
                    if ($this.newFilter) {
                        $this.newFilter = false;
                        postData.pageIndex = 1;
                    }
                    postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                    var param = $.extend({}, postData);
                    if ($this.filter) {
                        $.extend(param, $this.filter);
                    }
                    $this.trigger('GridView.PageRequest', param);
                },

                jsonReader: { id: 0, repeatitems: false },
                rowNum: 15,
                rowList: [15, 30, 45],
                colNames: ['No', 'Sounding Date', 'Tank Name', 'Company Name', 'Liquid Level', 'Temperature', 'Product Tank Vol.', 'Product Pipe Vol.', 'Product Slope Vol.', 'Arrival Date', 'Receiving Qty. (Kg/KL)', 'BL No.', 'Delivery Date', 'Total Delivery (Kg/KL)'],
            colModel:
		    [
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'SoundingDateText', index: 'SoundingDate', width: 115, sortable: true },
                { name: 'Name', width: 95, sortable: true },
                { name: 'CompanyName', width: 135, sortable: true },
                { name: 'LiquidLevel', width: 95, sortable: true },
                { name: 'Temperature', width: 95, sortable: true },
                { name: 'ProductTankVolume', width: 125, sortable: true },
                { name: 'ProductPipeVolume', width: 125, sortable: true },
                { name: 'ProductSlopeVolume', width: 125, sortable: true },
                { name: 'ArrivalDateText', index: 'ArrivalDate', width: 115, sortable: true },
                { name: 'Quantity', width: 155, sortable: true },
                { name: 'BlNo', width: 95, sortable: true },
                { name: 'DeliveryDateText', index: 'DeliveryDate', width: 155, sortable: true },    
                { name: 'TotalDelivery', width: 155, sortable: true },
		    ],
            viewrecords: true,
            sortorder: "desc",
            sortname: "SoundingDate",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-stock-monitoring-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {            
            $('#tbl-stock-monitoring')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-stock-monitoring').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

