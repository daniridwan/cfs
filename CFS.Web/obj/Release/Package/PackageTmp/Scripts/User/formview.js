﻿FDM.View.FormView = C.FormView.extend(
{
    initialize: function () {
        this.dlgResetPassword = new FDM.View.DlgResetPassword({ el: $('#dlg-reset-password') });
    },
    events: function () {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
            /* put additional event here */
            "click .cmd-reset-password":"onResetPasswordClick",
            "click #btn-scan": "onScanClick",
        });
    },
    render: function () {
        // don't forget to call ancestor's render()
        C.FormView.prototype.render.call(this);

        this.dlgResetPassword.render();

        this.validator = this.$("form").validate({ ignore: null });
        this.enableEdit(true);
        $('#password-field').hide();
    },
    onScanClick: function(ev){
        Msg.show('Please place the finger on the sensor, twice.');
        var $this = this;
        var att = $this.model.attributes;
        console.log(att);
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'User/UserDevice',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(att),
            success: function(arg) {
                if(arg.success){
                    Msg.hide();
                    $('#txt-identification-number').val(arg.records);
                }
                else{
                    Msg.hide();
                    alert(arg.message);
                }
            }
        });
    },
    onEditClick: function (ev) {
        C.FormView.prototype.onEditClick.call(this, ev);
        $('#btn-reset-password').hide();
        $("#btn-scan").show();
    },
    onSaveClick: function (ev) {
        // ini override onSaveClick di ancestor
        var $this = this;
        if (this.validator.form() == false) {
            this.renderClientMessage();
            this.$('.client-msg').show();
            return;
        }
        else {
            this.$('.client-msg').hide();
        }


        // di sini mustinya ada validate
        // ...
        var toSave = this.$('.the-form').formHash();
        delete (toSave.UserId);
        if (this.model) //update
        {
            this.showThrobber(true);
            this.model.save(toSave,
            {
                success: function (model, response) {
                    if (response.success) {
                        $this.showThrobber(false);
                        $this.model.set(response.record);
                        $this.dataBind($this.model);
                        $this.makeReadOnly();
                        $this.prepareForUpdate($this.model);
                        $this.renderServerMessage(response);
                        $this.trigger('FormView.Updated', $this.model);
                    }
                    else {
                        $this.renderServerMessage(response);
                    }
                },
                error: function (model, response) {
                    $this.showThrobber(false);
                    if (window.console) console.log(response);
                }
            });
        }
        else //insert
        {
            var objSave = new FDM.Model.Users();
            delete (toSave.UserId);
            objSave.save(toSave,
    	    {
    	        success: function (model, response) {
    	            if (response.success) {
    	                $this.showThrobber(false);
    	                $this.model = objSave;
    	                $this.model.set(response.record);
    	                $this.dataBind($this.model);
    	                $this.makeReadOnly();
    	                $this.renderServerMessage(response);
    	                $this.trigger('FormView.Created', $this.model);
    	            }
    	            else {
    	                $this.renderServerMessage(response);
    	            }
    	        },
    	        error: function (model, response) {
    	            $this.showThrobber(false);
    	            if (window.console) console.log(response);
    	        }
    	    });
        }
        $("#btn-scan").hide();
    },
    prepareForInsert: function () {
        C.FormView.prototype.prepareForInsert.apply(this);
        this.enableEditControl(true);
        $('#btn-reset-password').hide();
        $("#btn-scan").hide();
    },
    onResetPasswordClick:function(ev)
    {
        var username = this.model.get('Username');
        if( username != null && username.length != 0)
        {
        this.dlgResetPassword.prepareForInsert(this.model);
        this.dlgResetPassword.show(true);
        }
        else
        {
            alert('Personel ini belum memiliki Username');
        }
    },
    dataBind: function (arg) {
        C.FormView.prototype.dataBind.apply(this, arguments);
        if (arg.get('CreatedTimestamp') != null) {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
        if (arg.get('UpdatedTimestamp') != null) {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
    },
    prepareForUpdate: function (arg) {
        this.dataBind(arg);
        this.validator.resetForm();
        this.makeReadOnly();
        $('#btn-reset-password').show();
        this.enableEditControl(false);
    },
    enableEditControl: function(en) {
        if (en) {
            this.$('button.ui-datepicker-trigger').show();
            $("#btn-scan").show();
        }
        else {
            this.$('button.ui-datepicker-trigger').hide();
            $("#btn-scan").hide();
        }
    }
});
