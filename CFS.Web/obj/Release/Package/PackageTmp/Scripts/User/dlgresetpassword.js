﻿FDM.View.tDlgResetPassword = 
{
    initialize: function(){
        this.currentRecord = null;
        this.currentMode = null;
    },
    events: {
        "click .dlg-tool-cmd-save": "onSaveClick",
        "click .dlg-tool-cmd-cancel": "onCancelClick",
    },
    render: function(){
		
        this.validator = $('#dlg-reset-password').validate({
            debug:true,
            ignore: null,
            rules:
            {
                NewPassword:{required:true},
                ConfirmPassword:{required:true,equalTo:'#txt-new-password'}
            }
        });
        var $this = this;
        this.$el.jqm({modal: true});
        this.$el.jqDrag('.dlg-title-block');
    },
    show: function(en){
        this.$el[en ? 'jqmShow' : 'jqmHide']();
    },
    onSaveClick:function(arg)
    {
        if(!this.validator.form())
            return; 
        // di sini mustinya ada validate
        // ...
        var toSave = this.$el.formHash();

        if(this.model) //update
        {
            var toPost = 
            {
                id : this.model.attributes.UserId,
                password : toSave.NewPassword
            };
        
			Msg.show('Saving..');
            $.ajax({
                type: "POST",
                url: g_baseUrl + 'User/ResetPassword',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(toPost),
                success: function(arg) {
                    if(arg.success){
                        Msg.hide();
                        this.show(false);
                        alert(arg.Message);
                    }
                    else{
                        Msg.hide();
                        alert(arg.Message);
                    }
                }
            });
        }
    },
    renderServerMessage:function(arg)
    {
        if(!arg.success)
        {
            var msg = [];
            msg.push('<ul>');
            for(var i = 0, ii = arg.messages.length; i < ii; i++)
            {
                msg.push('<li>' + arg.messages[i] + '</li>');
            }
            msg.push('</ul>');
            $('#pnl-msg').addClass('pnl-errmsg');
            $('#pnl-msg').html(msg.join(''));
            $('#pnl-msg').show();
        }
        else
        {
            $('#pnl-msg').hide();
        }
    },
    onCancelClick: function(arg){
        this.show(false);
    },
    prepareForInsert: function(/*model*/arg){
        this.currentMode = 'insert';
        C.Util.clearField(this.$el);
        this.model = arg;
        $('#lbl-username').text(this.model.get('Username'));
    }
};
FDM.View.DlgResetPassword = Backbone.View.extend(FDM.View.tDlgResetPassword);
