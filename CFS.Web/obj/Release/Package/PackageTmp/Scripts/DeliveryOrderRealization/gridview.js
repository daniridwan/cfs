﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-delivery-order-realization').jqGrid(
        {
            pager: 'tbl-delivery-order-realization-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', 'Reg. Plate', 'DO Number',
            'Tr. Number', 'Product','Company', 'Preset', 'Actual', 'UOM', 'Init. Weight','Final Weight','Net Weight','FP Name', 'FP Group', 'Vessel No', 'BL No', 'Adm. Timestamp' ,'Gate In Timestamp', 'Gate Out Timestamp', 'Good Issue Timestamp'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'RegistrationPlate', width: 95, sortable: true },
                { name: 'DeliveryOrderNumber', width: 100, sortable: true },
                { name: 'TransactionNumber', width: 100, sortable: true },
                { name: 'ProductName', width: 135, sortable: true },
                { name: 'CompanyName', width: 150, sortable: true },
                { name: 'Preset', width: 80, sortable: true },
                { name: 'Actual', width: 80, sortable: true },
                { name: 'UOM', width: 60, sortable: true },
                { name: 'InitialWeight', width: 105, sortable: true },
                { name: 'FinalWeight', width: 105, sortable: true },
                { name: 'NetWeight', width: 105, sortable: true },
                { name: 'FillingPointName', width: 90, sortable: true },
                { name: 'FillingPointGroup', width: 90, sortable: true },
                { name: 'VesselNo', width: 95, sortable: true },
                { name: 'BlNo', width: 120, sortable: true },
                { name: 'AdministrationTimestampText', index: 'AdministrationTimestamp', width: 135, sortable: true },
                { name: 'GateInTimestampText', index: 'GateInTimestamp', width: 150, sortable: true },
                { name: 'GateOutTimestampText', index: 'GateOutTimestamp', width: 160, sortable: true },
                { name: 'GoodIssueTimestampText', index: 'GoodIssueTimestamp', width: 175, sortable: true },
		],
            viewrecords: true,
            sortorder: "asc",
            sortname: "AdministrationTimestamp",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-delivery-order-realization-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {            
            $('#tbl-delivery-order-realization')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-delivery-order-realization').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

