﻿FDM.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;
        this.gridObj = $('#tbl-tank-ticket').jqGrid(
        {
            pager: 'tbl-tank-ticket-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', '', 'Tank Ticket Number', 'Tank', 'Timestamp', 'Operation Type', 'Operation Status', 'Measurement Type', 'Product'
                        ,'Liquid Level (mm)', 'Liquid Temperature (°C)', 'Liquid Density', 'Liquid Density 15', 'Liquid Vol Obs (L)', 'Liquid Vol 15 (L)', 'Liquid Mass (Kg)', 'Vapor Temperature (°C)'
                        ,'Vapor Density','Vapor Vol Obs', 'Vapor Vol 15', 'Vapor Mass (Kg)', 'Vapor Pressure', 
                        'Liquid Volume Correction Factor', 'Liquid Density Multiplier', 'Vapor Pressure Factor', 'Vapor Temperature Factor'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 20, sortable: false, hidden: false },
                { name: 'print', width: 20, sortable: false, hidden: false },
                { name: 'TankTicketNumber', index: 'TankTicketNumber', width: 150, sortable: true },
                { name: 'TankName', index: 'TankName', width: 70, sortable: true },
                { name: 'TimestampText', index: 'Timestamp', width: 120, sortable: true },
                { name: 'OperationType', index: 'OperationType', width: 125, sortable: true },
                { name: 'OperationStatus', index: 'OperationStatus', width: 135, sortable: true },
                { name: 'MeasurementType', index: 'MeasurementType', width: 150, sortable: true },
                { name: 'ProductName', index: 'ProductCode', width: 100, sortable: true },
                { name: 'LiquidLevelText', index: 'LiquidLevel', width: 140, sortable: true },
                { name: 'LiquidTemperatureText', index: 'LiquidTemperature', width: 175, sortable: true },
                { name: 'LiquidDensityText', index: 'LiquidDensity', width: 140, sortable: true },
                { name: 'LiquidDensity15Text', index: 'LiquidDensity15', width: 140, sortable: true },
                { name: 'LiquidVolObsText', index: 'LiquidVolObs', width: 140, sortable: true },
                { name: 'LiquidVol15Text', index: 'LiquidVol15', width: 135, sortable: true },
                { name: 'LiquidMassText', index: 'LiquidMass', width: 125, sortable: true },
                { name: 'VaporTemperatureText', index: 'VaporTemperature', width: 165, sortable: true },
                { name: 'VaporDensityText', index: 'VaporDensity', width: 130, sortable: true },
                { name: 'VaporVolObsText', index: 'VaporVolObs', width: 115, sortable: true },
                { name: 'VaporVol15Text', index: 'VaporVol15', width: 115, sortable: true },
                { name: 'VaporMassText', index: 'VaporMass', width: 125, sortable: true },
                { name: 'VaporPressureText', index: 'VaporPressure', width: 125, sortable: true },
                { name: 'LiquidVolumeCorrectionFactorText', index: 'LiquidVolumeCorrectionFactor', width: 220, sortable: true },
                { name: 'LiquidDensityMultiplierText', index: 'LiquidDensityMultiplier', width: 170, sortable: true },
                { name: 'VaporPressureFactorText', index: 'VaporPressureFactor', width: 150, sortable: true },
                { name: 'VaporTemperatureFactorText', index: 'VaporTemperatureFactor', width: 170, sortable: true },
		],
            viewrecords: true,
            sortorder: "desc",
            sortname: "Timestamp",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-tank-ticket-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {    
            $('#tbl-tank-ticket')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-tank-ticket').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

