﻿FDM.View.FormView = C.FormView.extend(
{
initialize: function() {

    },
    events: function() {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
        /* put additional event here */
        });
    },
render: function() {

    // don't forget to call ancestor's render()
    C.FormView.prototype.render.call(this);

   var dateopt = { altFormat: "dd-mm-yy",
            dateFormat: 'dd-M-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true,
        };

   $(".datepicker").datepicker({
        beforeShow: function (input, inst) {
        setTimeout(function () {
            inst.dpDiv.css({
                top: $(".datepicker").offset().top + 35,
                left: $(".datepicker").offset().left
            });
        }, 0);
        }
    }); 

    $('#txt-timestamp').datepicker(_.extend(dateopt, {}));

    this.validator = this.$("form").validate({ ignore: null });
    //ini mustinya tergantung permission
    this.enableEdit(false);
},
onEditClick: function(ev){
        C.FormView.prototype.onEditClick.call(this, ev);  
    },
onSaveClick: function(ev) {
    // ini override onSaveClick di ancestor
    var $this = this;
        if(this.validator.form()==false)
        {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-form').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else
        {
            this.$('.client-msg').hide();
        }


    // di sini mustinya ada validate
    // ...
    var toSave = this.$('.the-form').formHash();
    if (this.model) //update
    {
        this.showThrobber(true);
        Msg.show('Updating..');
        this.model.save(toSave,
            {
                success: function(model, response) {
                    if (response.success) {
                        $this.showThrobber(false);
                        $this.model.set(response.record);
                        $this.dataBind($this.model);
                        $this.makeReadOnly();
                        $this.renderServerMessage(response);
                        $this.trigger('FormView.Updated', $this.model);
                    }
                    else {
                        $this.renderServerMessage(response);
                    }
                },
                error: function(model, response) {
                    $this.showThrobber(false);
                    C.Util.log(response);
                    if (window.console) console.log(response);
                }
            });
    }
    else //insert
    {
        var objSave = new FDM.Model.TankTicket();
        delete (toSave.TankTicketId);
        Msg.show('Saving..');
        objSave.save(toSave,
    	    {
    	        success: function(model, response) {
    	            if (response.success) {
    	                $this.showThrobber(false);
    	                $this.model = objSave;
    	                $this.model.set(response.record);
    	                $this.dataBind($this.model);
    	                $this.makeReadOnly();
    	                $this.renderServerMessage(response);
    	                $this.trigger('FormView.Updated', $this.model);
                        $('#txt-tank-ticket-number-extension').hide();
    	            }
    	            else {
    	                $this.renderServerMessage(response);
    	            }
    	        },
    	        error: function(model, response) {
    	            $this.showThrobber(false);
    	            if (window.console) console.log(response);
    	        }
    	    });
    }
},
prepareForInsert: function() {
    C.FormView.prototype.prepareForInsert.apply(this);
    this.validator.resetForm();
    this.clearAuditTrail();
    this.enableEditControl(true);
},
prepareForInsert: function(arg) { //masuk ke sini
    C.FormView.prototype.prepareForInsert.apply(this);
    this.validator.resetForm();
    this.clearAuditTrail();
    this.enableEditControl(true);
    var timestamps = new Date();
    var year = timestamps.getFullYear();
    var month = timestamps.getMonth()+1;
    var day = timestamps.getDate();
    if (day < 10) {
        day = '0' + day;
    }
    if (month < 10) {
            month = '0' + month;
    }
    $('#txt-tank-ticket-number-extension').show();
    $('#txt-tank-ticket-number-extension').val('/'+ g_abbreviation +'/'+ arg.TankName +'/'+ year + month + day);
    $('#txt-tank-name').val(arg.TankName);
    $('#txt-tank-name').attr('readonly', 'readonly');
    $('#txt-timestamp').val(timestamps.format("d-mmm-yyyy HH:MM:ss"));
    $('#txt-timestamp').attr('readonly', 'readonly');
    $('#txt-operation-type').val(arg.OperationType);
    $('#txt-operation-type').attr('readonly', 'readonly');
    $('#txt-operation-status').val(arg.OperationStatus);
    $('#txt-operation-status').attr('readonly', 'readonly');
    $('#txt-measurement-type').val(arg.MeasurementType);
    $('#txt-measurement-type').attr('readonly', 'readonly');
    if(arg.MeasurementType == 'Auto')
    {
        Msg.show('Loading...');
        //get data from tank live data
        $.ajax({
            type: "POST",
            url: g_baseUrl + 'TankMonitoring/GetTankLiveDataByTank',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(arg),
            success: function(arg) {
                if(arg.success){
                    Msg.hide();
                    $('#txt-liquid-level').val(arg.record.LiquidLevel);
                    $('#txt-liquid-water').val(arg.record.LiquidWater);
                    $('#txt-liquid-temperature').val(arg.record.LiquidTemperature);
                    $('#txt-liquid-density').val(arg.record.LiquidDensity);
                    $('#txt-liquid-density15').val(arg.record.LiquidDensity15);
                    $('#txt-liquid-vol-obs').val(arg.record.LiquidVolObs);
                    $('#txt-liquid-vol-15').val(arg.record.LiquidVol15);
                    $('#txt-liquid-mass').val(arg.record.LiquidMass);
                    $('#txt-liquid-pressure').val(arg.record.LiquidPressure);
                    $('#txt-vapor-temperature').val(arg.record.VaporTemperature);
                    $('#txt-vapor-density').val(arg.record.VaporDensity);
                    $('#txt-vapor-vol-obs').val(arg.record.VaporVolObs);
                    $('#txt-vapor-vol-15').val(arg.record.VaporVol15);
                    $('#txt-vapor-mass').val(arg.record.VaporMass);
                    $('#txt-vapor-pressure').val(arg.record.VaporPressure);
                    $('#txt-liquid-volume-correction-factor').val(arg.record.LiquidVolumeCorrectionFactor);
                    $('#txt-liquid-density-multiplier').val(arg.record.LiquidDensityMultiplier);
                    $('#txt-vapor-pressure-factor').val(arg.record.VaporPressureFactor);
                    $('#txt-vapor-temperature-factor').val(arg.record.VaporTemperatureFactor);
                    $('#txt-total-obs-vol').val(arg.record.TotalObservedVolume);
                }
                else{
                    Msg.hide();
                    alert(arg.message);
                }
            }
        });
        //jika auto, read only tank live data fieldset
        $('#txt-liquid-level').attr('readonly', 'readonly');
        $('#txt-liquid-water').attr('readonly', 'readonly');
        $('#txt-liquid-temperature').attr('readonly', 'readonly');
        $('#txt-liquid-density').attr('readonly', 'readonly');
        $('#txt-liquid-density15').attr('readonly', 'readonly');
        $('#txt-liquid-vol-obs').attr('readonly', 'readonly');
        $('#txt-liquid-vol-15').attr('readonly', 'readonly');
        $('#txt-total-obs-vol').attr('readonly', 'readonly');
        $('#txt-liquid-mass').attr('readonly', 'readonly');
        $('#txt-liquid-pressure').attr('readonly', 'readonly');
        $('#txt-vapor-temperature').attr('readonly', 'readonly');
        $('#txt-vapor-density').attr('readonly', 'readonly');
        $('#txt-vapor-vol-obs').attr('readonly', 'readonly');
        $('#txt-vapor-vol-15').attr('readonly', 'readonly');
        $('#txt-vapor-mass').attr('readonly', 'readonly');
        $('#txt-vapor-pressure').attr('readonly', 'readonly');
        $('#txt-liquid-volume-correction-factor').attr('readonly', 'readonly');
        $('#txt-liquid-density-multiplier').attr('readonly', 'readonly');
        $('#txt-vapor-pressure-factor').attr('readonly', 'readonly');
        $('#txt-vapor-temperature-factor').attr('readonly', 'readonly');
    }
    else{
        $('#txt-liquid-level').removeAttr('readonly');
        $('#txt-liquid-water').removeAttr('readonly');
        $('#txt-liquid-temperature').removeAttr('readonly');
        $('#txt-liquid-density').removeAttr('readonly');
        $('#txt-liquid-density15').removeAttr('readonly');
        $('#txt-liquid-vol-obs').removeAttr('readonly');
        $('#txt-total-obs-vol').removeAttr('readonly');
        $('#txt-liquid-vol-15').removeAttr('readonly');
        $('#txt-liquid-mass').removeAttr('readonly');
        $('#txt-liquid-pressure').removeAttr('readonly');
        $('#txt-vapor-temperature').removeAttr('readonly');
        $('#txt-vapor-density').removeAttr('readonly');
        $('#txt-vapor-vol-obs').removeAttr('readonly');
        $('#txt-vapor-vol-15').removeAttr('readonly');
        $('#txt-vapor-mass').removeAttr('readonly');
        $('#txt-vapor-pressure').removeAttr('readonly');
        $('#txt-liquid-volume-correction-factor').removeAttr('readonly');
        $('#txt-liquid-density-multiplier').removeAttr('readonly');
        $('#txt-vapor-pressure-factor').removeAttr('readonly');
        $('#txt-vapor-temperature-factor').removeAttr('readonly');
    }
},
dataBind: function(arg) {
    C.FormView.prototype.dataBind.apply(this, arguments);
    $('#txt-timestamp').val(arg.get('TimestampText'));
    $('#txt-liquid-level').val(arg.get('LiquidLevelText'));
    $('#txt-liquid-temperature').val(arg.get('LiquidTemperatureText'));
    $('#txt-liquid-density').val(arg.get('LiquidDensityText'));
    $('#txt-liquid-density15').val(arg.get('LiquidDensity15Text'));
    $('#txt-liquid-vol-obs').val(arg.get('LiquidVolObsText'));
    $('#txt-liquid-vol-15').val(arg.get('LiquidVol15Text'));
    $('#txt-liquid-mass').val(arg.get('LiquidMassText'));
    $('#txt-vapor-temperature').val(arg.get('VaporTemperatureText'));
    $('#txt-vapor-density').val(arg.get('VaporDensityText'));
    $('#txt-vapor-vol-obs').val(arg.get('VaporVolObsText'));
    $('#txt-vapor-vol-15').val(arg.get('VaporVol15Text'));
    $('#txt-vapor-mass').val(arg.get('VaporMassText'));
    $('#txt-vapor-pressure').val(arg.get('VaporPressureText'));
    $('#txt-liquid-volume-correction-factor').val(arg.get('LiquidVolumeCorrectionFactorText'));
    $('#txt-liquid-density-multiplier').val(arg.get('LiquidDensityMultiplierText'));
    $('#txt-vapor-pressure-factor').val(arg.get('VaporPressureFactorText'));
    $('#txt-vapor-temperature-factor').val(arg.get('VaporTemperatureFactorText'));
    if(arg.get('CreatedTimestamp') != null)
        {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else
        {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
    if(arg.get('UpdatedTimestamp') != null)
        {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else
        {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
},
prepareForUpdate: function(arg) {
        this.dataBind(arg);
        $('#txt-tank-ticket-number-extension').hide();
        this.validator.resetForm();
        this.makeReadOnly();
        this.enableEditControl(false);
},
clearAuditTrail: function() {
    $('#lbl-insert-date').text('-');
    $('#lbl-insert-by').text('-');
    $('#lbl-last-updated-date').text('-');
    $('#lbl-last-updated-by').text('-');
},
enableEditControl: function(en) {
        if (en) {
            this.$('button.ui-datepicker-trigger').hide();
        }
        else {
            this.$('button.ui-datepicker-trigger').hide();
        }
    },
});