﻿FDM.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-meter-reading').jqGrid(
        {
            pager: 'tbl-meter-reading-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No','Truck','DO Number', 'Preset (L)', 'Actual (L)',
            'Start Filling', 'End Filling', 'Start Totalizer', 'End Totalizer', 'Density', 'Temperature', 'Filling Status', 'Flowrate (L/min)'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'RegistrationPlate', sortable: true, width: 120, hidden: false },
                { name: 'DeliveryOrderNumber', sortable: true, width: 120, hidden: false },
                { name: 'PresetText', index: 'Preset', sortable: true, width: 80, hidden: false },
                { name: 'ActualText', index: 'Actual', sortable: true, width: 80, hidden: false },
                { name: 'StartTimestampText', sortable: true, width: 150, hidden: false },
                { name: 'StopTimestampText', sortable: true, width: 150, hidden: false },
                { name: 'StartTotalizerText', index: 'StartTotalizer', sortable: true, width: 120, hidden: false },
                { name: 'StopTotalizerText', index: 'StopTotalizer', sortable: true, width: 120, hidden: false },
                { name: 'DensityText', index: 'Density', sortable: true, width: 80, hidden: false },
                { name: 'TemperatureText',index: 'Temperature',  sortable: true, width: 100, hidden: false },
                { name: 'FillingStatus', sortable: true, width: 100, hidden: false },
                { name: 'FlowrateText', index: 'Flowrate', sortable: true, width: 120, hidden: false },
		],
            viewrecords: true,
            sortorder: "desc",
            sortname: "StopTimestamp",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-meter-reading-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {    
            $('#tbl-meter-reading')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-meter-reading').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

