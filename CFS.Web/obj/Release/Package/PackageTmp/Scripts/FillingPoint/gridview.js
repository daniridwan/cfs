﻿FDM.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-filling-point').jqGrid(
        {
            pager: 'tbl-filling-point-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', 'Filling Point Name', 'Tank', 'Product', 'Company', 'Expired Date', 'Pump Name', 'Flowrate (m<sup>3</sup>/h)'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 18, sortable: false, hidden: false },
                { name: 'Name', width: 120, sortable: true },
                { name: 'TankName', width: 140, sortable: true },
                { name: 'ProductName', width: 120, sortable: true },
                { name: 'CompanyName', width: 160, sortable: true },
                { name: 'ExpiredDateText', index: 'ExpiredDate', width: 160, sortable: true },
                { name: 'PumpName', width: 130, sortable: false },
                { name: 'Flowrate', width: 130, sortable: false },
		],
            viewrecords: true,
            sortorder: "asc",
            sortname: "Name",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-filling-point-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {    
            $('#tbl-filling-point')[0].addJSONData(toInject);
            $('.nearexpired', '#tbl-filling-point').each(function (key, val) {
                $(val).parent().parent().addClass('nearexpired');
            });
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-filling-point').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

