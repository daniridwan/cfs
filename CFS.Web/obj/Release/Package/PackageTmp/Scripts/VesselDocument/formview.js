﻿CFS.View.FormView = C.FormView.extend(
{
initialize: function () {
    },
    events: function() {
        //jika butuh event tambahan, extend events yang udh defined di ancestor
        return _.extend(C.FormView.prototype.events,
        {
        /* put additional event here */
            "click #btn-add-vessel-tank": "onAddVesselTankClick",
            "click #btn-add-plb-document": "onAddPlbDocumentClick",
            "change #ddl-type": "onChangeType",
            "click #btn-arrival-date": "onClickArrivalDate",
            "click #btn-approve": "onClickApproval"
        });
    },

    render: function() {

    // don't forget to call ancestor's render()
    C.FormView.prototype.render.call(this);

    this.gridVesselTank = new CFS.View.GridVesselTank({ el: $('#tbl-vessel-tank') });
    this.dlgVesselTank = new CFS.View.DlgVesselTank({ el: $('#dlg-vessel-tank') });
    this.gridPlbDocument = new CFS.View.GridPlbDocument({ el: $('#tbl-plb-document') });
    this.dlgPlbDocument = new CFS.View.DlgPlbDocument({ el: $('#dlg-plb-document') });

    var dateopt = { altFormat: "d-mmm-yyyy",
            dateFormat: 'dd-MM-yy', showOn: 'button', selectOtherMonths: true, duration: 'fast', 
            buttonImage: g_baseUrl + '/Images/calendar.png', changeMonth: true, changeYear: true,
        };

    this.validator = this.$("form").validate({ ignore: null });

    $(".datepicker").datepicker({
        beforeShow: function (input, inst) {
            setTimeout(function () {
                inst.dpDiv.css({
                    top: $(".datepicker").offset().top + 35,
                    left: $(".datepicker").offset().left
                });
            }, 0);
        }
    }); 

    $('#txt-sppb-date').datepicker(_.extend(dateopt, {}));
    $('#txt-pib-date').datepicker(_.extend(dateopt, {}));

    this.gridVesselTank.render();
    this.dlgVesselTank.render();
    this.gridPlbDocument.render();
    this.dlgPlbDocument.render();

    this.dlgVesselTank.on('created', this.onVesselTankCreated, this);
    this.dlgVesselTank.on('updated', this.onVesselTankUpdated, this);
    this.gridVesselTank.on('GridView.RequestDelete', this.onDeleteVesselTankRequested, this);
    this.gridVesselTank.on('GridView.RequestView', this.onViewVesselTankRequested, this);
    this.gridVesselTank.on('GridView.PageRequest', this.onPageVesselTankRequested, this); // masih blm dipake

    this.dlgPlbDocument.on('created', this.onPlbDocumentCreated, this);
    this.dlgPlbDocument.on('updated', this.onPlbDocumentUpdated, this);
    this.gridPlbDocument.on('GridView.RequestDelete', this.onDeletePlbDocumentRequested, this);
    this.gridPlbDocument.on('GridView.RequestView', this.onViewPlbDocumentRequested, this);
    this.gridPlbDocument.on('GridView.PageRequest', this.onPagePlbDocumentRequested, this); // masih blm dipake
    //ini mustinya tergantung permission
    this.enableEdit(true);

    $('#txt-arrival-date').datetimepicker({
        controlType: 'select',
        dateFormat: "dd-MM-yy",
        timeFormat: "HH:mm",
    });

    //$("#txt-arrival-date").css('pointer-events', 'none');
},
onEditClick: function(ev){
    C.FormView.prototype.onEditClick.call(this, ev);  
    console.log(this.model.attributes.Status);
    if (this.model.attributes.Status === 'Approved') {
        this.enableEditControl(false);
        }
    else {
        this.enableEditControl(true);
    }
    },
onSaveClick: function(ev) {
    // ini override onSaveClick di ancestor
    var $this = this;
        if(this.validator.form()==false)
        {
            this.renderClientMessage();
            $(window).scrollTop($('#pnl-form').offset().top);
            this.$('.client-msg').show();
            return;
        }
        else
        {
            this.$('.client-msg').hide();
        }

    // di sini mustinya ada validate
    // ...
    var toSave = this.$('.the-form').formHash();
    toSave.VesselTank = CFS.Model.VesselTankList.toJSON();
    toSave.PlbDocument = CFS.Model.PlbDocumentList.toJSON();

    if (this.model) //update
    {
        this.showThrobber(true);
        this.model.save(toSave,
            {
                success: function(model, response) {
                    if (response.success) {
                        $this.showThrobber(false);
                        $this.model.set(response.record);
                        $this.dataBind($this.model);
                        $this.makeReadOnly();
                        $this.renderServerMessage(response);
                        $this.trigger('FormView.Updated', $this.model);
                        $this.enableEditControl(false);
                    }
                    else {
                        $this.renderServerMessage(response);
                    }
                },
                error: function(model, response) {
                    $this.showThrobber(false);
                    C.Util.log(response);
                    if (window.console) console.log(response);
                }
            });
    }
    else //insert
    {
        var objSave = new CFS.Model.VesselDocument();
        delete (toSave.VesselDocumentId);
        Msg.show('Saving..');
        objSave.save(toSave,
    	    {
    	        success: function(model, response) {
    	            if (response.success) {
    	                $this.showThrobber(false);
    	                $this.model = objSave;
    	                $this.model.set(response.record);
    	                $this.dataBind($this.model);
    	                $this.makeReadOnly();
    	                $this.renderServerMessage(response);
    	                $this.trigger('FormView.Updated', $this.model);
                        $this.enableEditControl(false);
    	            }
    	            else {
    	                $this.renderServerMessage(response);
    	            }
    	        },
    	        error: function(model, response) {
    	            $this.showThrobber(false);
    	            if (window.console) console.log(response);
    	        }
    	    });
    }
},
prepareForInsert: function() {
    C.FormView.prototype.prepareForInsert.apply(this);
    this.validator.resetForm();
    this.clearAuditTrail();
    this.enableEditControl(true);
    this.PLBType(false);

    //reset child
    CFS.Model.VesselTankList.reset();
    this.gridVesselTank.dataBind(CFS.Model.VesselTankList.getFormattedArray());
    CFS.Model.PlbDocumentList.reset();
    this.gridPlbDocument.dataBind(CFS.Model.PlbDocumentList.getFormattedArray());
},
onClickArrivalDate: function (ev) {        
    $('#txt-arrival-date').datetimepicker('show');
    ev.preventDefault();
},
dataBind: function(arg) {
    C.FormView.prototype.dataBind.apply(this, arguments);
    $('#txt-pib-date').val(arg.get('PibDateText'));
    $('#txt-sppb-date').val(arg.get('SppbDateText'));
    $('#txt-arrival-date').val(arg.get('ArrivalDateText'));
    if(arg.get('CreatedTimestamp') != null)
        {
            var CreatedTimestamp = C.parseIsoDateTime(arg.get('CreatedTimestamp'));
            $('#lbl-insert-date').text(CreatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-insert-by').text(arg.get('CreatedByName'));
        }
        else
        {
            $('#lbl-insert-date').text('-');
            $('#lbl-insert-by').text('-');
        }
    if(arg.get('UpdatedTimestamp') != null)
        {
            var UpdatedTimestamp = C.parseIsoDateTime(arg.get('UpdatedTimestamp'));
            $('#lbl-last-updated-date').text(UpdatedTimestamp.format('d-mmm-yyyy HH:MM:ss'));
            $('#lbl-last-updated-by').text(arg.get('UpdatedByName'));
        }
        else
        {
            $('#lbl-last-updated-date').text('-');
            $('#lbl-last-updated-by').text('-');
        }
},
prepareForUpdate: function(arg) {
    this.dataBind(arg);
    this.validator.resetForm();
    this.makeReadOnly();
    this.enableEditControl(false);
    if (arg.attributes.Type === 'Import PLB') {
        this.PLBType(true);
    }
    else {
        this.PLBType(false);
    }
    //
    if (arg.attributes.Status === 'Approved') {
        this.PLBStatus(true);
    }

    CFS.Model.VesselTankList.reset(arg.attributes.VesselTank);
    this.gridVesselTank.dataBind(CFS.Model.VesselTankList.getFormattedArray());

    CFS.Model.PlbDocumentList.reset(arg.attributes.PlbDocument);
    this.gridPlbDocument.dataBind(CFS.Model.PlbDocumentList.getFormattedArray());
},
clearAuditTrail: function() {
    $('#lbl-insert-date').text('-');
    $('#lbl-insert-by').text('-');
    $('#lbl-last-updated-date').text('-');
    $('#lbl-last-updated-by').text('-');
},
onChangeType: function (arg) {
    var type = $("#ddl-type").val();
    var status = $("#txt-status").val();
    if (type === 'Import PLB') {
        this.PLBType(true);
    }
    else {
        this.PLBType(false);
    }
},
PLBStatus: function (status) {
    if (status) {
        this.$('#plb-document').show(); //dokumen detail plb
    }
    else {
        this.$('#plb-document').hide();
    }
},
PLBType: function (status) {
    if (status) {
        this.$('.sppb').hide(); //column sppb no and date
    }
    else {
        this.$('.sppb').show();
    }
},
enableEditControl: function(en) {
    if (en) {
        this.$('button.ui-datepicker-trigger').show();
        $('#btn-add-vessel-tank').show();
        $('#btn-add-plb-document').hide();
        this.gridVesselTank.showControl(true);
        this.gridPlbDocument.showControl(false);
        this.$('.btn-datetimepicker').show();

        this.$('#txt-vessel-no').removeAttr('readonly');
        this.$('#ddl-type').removeAttr('disabled');
        this.$('#txt-arrival-date').removeAttr('readonly');
        this.$('#txt-bl-no').removeAttr('readonly');
        this.$('#txt-bl-quantity').removeAttr('readonly');
        this.$('#ddl-bl-uom').removeAttr('disabled');
        this.$('#txt-bl-cost').removeAttr('readonly');
        this.$('#txt-sppb-no').removeAttr('readonly');
        this.$('#txt-sppb-date').removeAttr('readonly');
        this.$('#txt-pib-no').removeAttr('readonly');
        this.$('#txt-pib-date').removeAttr('readonly');
        this.$('#txt-remarks').removeAttr('readonly');
        this.$('#ddl-active').attr('disabled');
    }
    else {
        this.$('button.ui-datepicker-trigger').hide();
        $('#btn-add-vessel-tank').hide();
        $('#btn-add-plb-document').hide();
        this.gridVesselTank.showControl(false);
        this.gridPlbDocument.showControl(false);
        this.$('.btn-datetimepicker').hide();

        this.$('#txt-vessel-no').attr('readonly', 'readonly');
        this.$('#ddl-type').attr('disabled', 'disabled');
        this.$('#txt-arrival-date').attr('readonly', 'readonly');
        this.$('#txt-bl-no').attr('readonly', 'readonly');
        this.$('#txt-bl-quantity').attr('readonly', 'readonly');
        this.$('#ddl-bl-uom').attr('disabled', 'disabled');
        this.$('#txt-bl-cost').attr('readonly', 'readonly');
        this.$('#txt-sppb-no').attr('readonly', 'readonly');
        this.$('#txt-sppb-date').attr('readonly', 'readonly');
        this.$('#txt-pib-no').attr('readonly', 'readonly');
        this.$('#txt-pib-date').attr('readonly', 'readonly');
        this.$('#txt-remarks').attr('readonly', 'readonly');
        this.$('#ddl-active').removeAttr("disabled");
    }
},
onAddVesselTankClick: function (ev) {
    this.dlgVesselTank.prepareForInsert();
    this.dlgVesselTank.show(true);
},
onAddPlbDocumentClick: function (ev) {
    this.dlgPlbDocument.prepareForInsert();
    this.dlgPlbDocument.show(true);
},
onVesselTankCreated: function (arg) {
    CFS.Model.VesselTankList.push(arg, {
        silent: true
    });
    var i = 1;
    var toInject = CFS.Model.VesselTankList.getFormattedArray();
    this.gridVesselTank.dataBind(toInject);
    this.dlgVesselTank.show(false);
},
onPlbDocumentCreated: function (arg) {
    CFS.Model.PlbDocumentList.push(arg, {
        silent: true
    });
    var i = 1;
    var toInject = CFS.Model.PlbDocumentList.getFormattedArray();
    this.gridPlbDocument.dataBind(toInject);
    this.dlgPlbDocument.show(false);
},
onVesselTankUpdated: function (arg) {
    var toInject = CFS.Model.VesselTankList.getFormattedArray();
    //alert(toInject);
    this.gridVesselTank.dataBind(toInject);
    this.dlgVesselTank.show(false);
},
onPlbDocumentUpdated: function (arg) {
    var toInject = CFS.Model.PlbDocumentList.getFormattedArray();
    //alert(toInject);
    this.gridPlbDocument.dataBind(toInject);
    this.dlgPlbDocument.show(false);
},
onDeleteVesselTankRequested: function (id) {
    var record = CFS.Model.VesselTankList.get(id);
    // deprecated in current backbone version
    //var record = NetLPG.Model.TruckCompartmentList.getByCid(id);
    //var Info = '' + record.get('MaxCapacity') + ', ';
    if (confirm('Are you sure want to delete Tank no  ' + record.get('idx'))) {
        CFS.Model.VesselTankList.remove(record);
        var toInject = CFS.Model.VesselTankList.getFormattedArray();
        this.gridVesselTank.dataBind(toInject);
    }
},
onDeletePlbDocumentRequested: function (id) {
    var record = CFS.Model.PlbDocumentList.get(id);
    // deprecated in current backbone version
    //var record = NetLPG.Model.TruckCompartmentList.getByCid(id);
    //var Info = '' + record.get('MaxCapacity') + ', ';
    if (confirm('Are you sure want to delete PLB Document no  ' + record.get('idx'))) {
        CFS.Model.PlbDocumentList.remove(record);
        var toInject = CFS.Model.PlbDocumentList.getFormattedArray();
        this.gridPlbDocument.dataBind(toInject);
    }
},
onViewVesselTankRequested: function (id) {
    var record = CFS.Model.VesselTankList.get(id);
    // deprecated in current backbone version
    //var record = NetLPG.Model.TruckCompartmentList.getByCid(id);
    this.dlgVesselTank.prepareForUpdate(record);
    this.dlgVesselTank.show(true);
},
onViewPlbDocumentRequested: function (id) {
    var record = CFS.Model.PlbDocumentList.get(id);
    // deprecated in current backbone version
    //var record = NetLPG.Model.TruckCompartmentList.getByCid(id);
    this.dlgPlbDocument.prepareForUpdate(record);
    this.dlgPlbDocument.show(true);
}
});