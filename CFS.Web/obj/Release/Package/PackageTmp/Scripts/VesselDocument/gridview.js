﻿CFS.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-vessel-document').jqGrid(
        {
            pager: 'tbl-vessel-document-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', '', 'Vessel No', 'Vessel Name','Type', 'Product', 'Company', 'Arrival Date', 'BL No', 'SPPB No', 'SPPB Date', 'PIB No', 'PIB Date', 'Status', 'Is Active', 'Remarks'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 18, sortable: false, hidden: false },
                { name: 'approve', width: 18, sortable: false, hidden: false },
                { name: 'VesselNo', width: 100, sortable: true },
                { name: 'VesselName', index: 'VesselId', width: 150, sortable: true },
                { name: 'Type', width: 80, sortable: true },
                { name: 'ProductName', index: 'ProductId', width: 145, sortable: true },
                { name: 'CompanyName', index: 'CompanyId', width: 155, sortable: true },
                { name: 'ArrivalDateText', index: 'ArrivalDate', width: 120, sortable: true },
                { name: 'BlNo', width: 150, sortable: true },
                { name: 'SppbNo', width: 150, sortable: true },
                { name: 'SppbDateText', index: 'SppbDate', width: 110, sortable: true },
                { name: 'PibNo', width: 150, sortable: true },
                { name: 'PibDateText', index: 'PibDate', width: 110, sortable: true },
                { name: 'Status', width: 90, sortable: true },
                { name: 'IsActiveText', index: 'IsActive', width: 90, sortable: true },
                { name: 'Remarks', width: 250, sortable: true },
		],
            viewrecords: true,
            sortorder: "asc",
            sortname: "VesselNo",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-vessel-document-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {            
            $('#tbl-vessel-document')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-vessel-document').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

