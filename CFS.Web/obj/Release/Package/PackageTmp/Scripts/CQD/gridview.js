﻿FDM.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-cqd').jqGrid(
        {
            pager: 'tbl-cqd-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', 'Tank', 'Operation Type', 'Tank Ticket Number Open', 'Tank Ticket Number Close', 'Start Timestamp', 'Stop Timestamp', 'Liquid Level Before(mm)', 'Liquid Level After(mm)', 
            'Liquid Temperature Before(°C)', 'Liquid Temperature After(°C)', 'Liquid Vol Obs Before(L)', 'Liquid Vol Obs After(L)', 'Liquid Density Before', 'Liquid Density After', 
            'Liquid Density 15 Before', 'Liquid Density 15 After', 'Liquid Vol Correct Factor Before', 'Liquid Vol Correct Factor After','Liquid Vol 15 Before', 'Liquid Vol 15 After', 
            'Liquid Density Multiplier Before', 'Liquid Density Multiplier After', 'Liquid Mass Before', 'Liquid Mass After'
            ],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'TankName', width: 50, sortable: true },
                { name: 'OperationType', width: 100, sortable: true },
                { name: 'TankTicketNumberOpen', width: 180, sortable: true },
                { name: 'TankTicketNumberClose', width: 180, sortable: true },
                { name: 'StartTimestampText', index: 'StartTimestamp', width: 140, sortable: true },
                { name: 'StopTimestampText', index: 'StopTimestamp', width: 140, sortable: true },
                { name: 'LiquidLevelBefore', index: 'LiquidLevelBefore', width: 150, sortable: true },
                { name: 'LiquidLevelAfter', index: 'LiquidLevelAfter', width: 150, sortable: true },
                { name: 'LiquidTemperatureBefore', index: 'LiquidTemperatureBefore', width: 190, sortable: true },
                { name: 'LiquidTemperatureAfter', index: 'LiquidTemperatureAfter', width: 180, sortable: true },
                { name: 'LiquidVolObsBefore', index: 'LiquidVolObsBefore', width: 160, sortable: true },
                { name: 'LiquidVolObsAfter', index: 'LiquidVolObsAfter', width: 160, sortable: true },
                { name: 'LiquidDensityBefore', index: 'LiquidDensityBefore', width: 160, sortable: true },
                { name: 'LiquidDensityAfter',index: 'LiquidDensityAfter',  width: 160, sortable: true },
                { name: 'LiquidDensity15Before', index: 'LiquidDensity15Before', width: 180, sortable: true },
                { name: 'LiquidDensity15After', index: 'LiquidDensity15After', width: 180, sortable: true },
                { name: 'LiquidVolumeCorrectionFactorBefore', index: 'LiquidVolumeCorrectionFactorBefore', width: 210, sortable: true },
                { name: 'LiquidVolumeCorrectionFactorAfter', index: 'LiquidVolumeCorrectionFactorAfter', width: 210, sortable: true },
                { name: 'LiquidVol15Before', index: 'LiquidVol15Before', width: 160, sortable: true },
                { name: 'LiquidVol15After', index: 'LiquidVol15After', width: 160, sortable: true },
                { name: 'LiquidDensityMultiplierBefore', index: 'LiquidDensityMultiplierBefore', width: 200, sortable: true },
                { name: 'LiquidDensityMultiplierAfter', index: 'LiquidDensityMultiplierAfter', width: 200, sortable: true },
                { name: 'LiquidMassBefore', index: 'LiquidMassBefore', width: 140, sortable: true },
                { name: 'LiquidMassAfter', index: 'LiquidMassAfter', width: 140, sortable: true },
		],
            viewrecords: true,
            sortorder: "desc",
            sortname: "StartTimestamp",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-cqd-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {    
            $('#tbl-cqd')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-cqd').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

