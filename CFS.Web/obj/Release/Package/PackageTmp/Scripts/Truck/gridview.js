﻿FDM.View.GridView = Backbone.View.extend(
{
    initialize: function (el) {
        //C.Util.log(el);
        $(window).on("resize", this.OnWindowResize);
    },
    render: function () {
        var $this = this;

        this.gridObj = $('#tbl-truck').jqGrid(
        {
            pager: 'tbl-truck-pager',
            datatype: function (postData) {
                if ($this.newFilter) {
                    $this.newFilter = false;
                    postData.pageIndex = 1;
                }
                postData.pageIndex -= 1; //convert from 'one based' to 'zero based'
                var param = $.extend({}, postData);
                if ($this.filter) {
                    $.extend(param, $this.filter);
                }
                $this.trigger('GridView.PageRequest', param);
            },

            jsonReader: { id: 0, repeatitems: false },
            rowNum: 15,
            rowList: [15, 30, 45],
            colNames: ['No', '', 'Registration Plate', 'Transporter', 'Max Capacity (Kg)', 'Safe Capacity (Kg)', 'UOM', 'Emergency Contact'],
            colModel:
		[
				{ name: 'No', sortable: false, width: 40, hidden: false },
                { name: 'view', width: 18, sortable: false, hidden: false },
                { name: 'RegistrationPlate', width: 140, sortable: true },
                { name: 'TruckTransporter', width: 165, sortable: true },
                { name: 'TankMaxCapacity', index: 'TankMaxCapacity', width: 140, sortable: true },
                { name: 'TankSafeCapacity', index: 'TankSafeCapacity', width: 140, sortable: true },
                { name: 'UOM', width: 80, sortable: true },
                { name: 'EmergencyContact', width: 160, sortable: false },
		],
            viewrecords: true,
            sortorder: "asc",
            sortname: "TruckId",
            height: 'auto',
            multiselect: false,
            shrinkToFit: false,
            width: $('#pnl-frame').width() - 2,
            altclass: 'even',
            altRows: true,
            pginput: true,
            prmNames: { rows: 'pageSize', page: 'pageIndex' },
        }).navGrid('#tbl-truck-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false,
            pginput: true,
            viewrecords: true
        });
    },
    dataBind: function (toInject) {
        try {            
            $('#tbl-truck')[0].addJSONData(toInject);
        }
        catch (err) {
            C.Util.log(err);
        }
    },
    setFilter: function (filter) {
        this.filter = filter;
        this.newFilter = true;
    },
    refresh: function () {
        this.gridObj.trigger('reloadGrid');
    },
    OnWindowResize : function()
    {
        $('#tbl-truck').setGridWidth($('#pnl-frame').width(),false).triggerHandler('resize');
    }
});

