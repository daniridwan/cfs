﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.APIModels
{
    [ActiveRecord("VISITOR")]
    public class Visitor : ActiveRecordBase
    {
        private int idVisitor;
        private string kartu;
        private string nama;
        private string asal;
        private string keterangan;
        private DateTime? tanggal;
        private DateTime? jamInput;
        private DateTime? masuk;
        private DateTime? keluar;
        private bool isAktif;

        [PrimaryKey(PrimaryKeyType.Identity, "IDVISITOR")]
        public int Id { get { return idVisitor; } set { idVisitor = value; } }
        [Property("KARTU")]
        public string Kartu { get { return kartu; } set { kartu = value; } }
        [Property("NAMA")]
        public string Nama { get { return nama; } set { nama = value; } }
        [Property("ASAL")]
        public string Asal { get { return asal; } set { asal = value; } }
        [Property("KETERANGAN")]
        public string Keterangan { get { return keterangan; } set { keterangan = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("TANGGAL")]
        public DateTime? Tanggal { get { return tanggal; } set { tanggal = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("JAMINPUT")]
        public DateTime? JamInput { get { return jamInput; } set { jamInput = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("MASUK")]
        public DateTime? Masuk { get { return masuk; } set { masuk = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("KELUAR")]
        public DateTime? Keluar { get { return keluar; } set { keluar = value; } }
        [Property("ISAKTIF")]
        public bool IsAktif { get { return isAktif; } set { isAktif = value; } }

        public Visitor()
        {
        }

        public static Visitor FindById(long id)
        {
            return (Visitor)FindByPrimaryKey(typeof(Visitor), id);
        }

        public static Visitor[] FindAlreadyGateOut()
        {
            DetachedCriteria dc = DetachedCriteria.For<Visitor>();
            dc.Add(Expression.IsNotNull("Masuk"));
            dc.Add(Expression.IsNotNull("Keluar"));
            dc.AddOrder(Order.Asc("JamInput"));
            dc.Add(Expression.Eq("IsAktif", false));
            return (Visitor[])FindAll(typeof(Visitor), dc);
        }

        public static Visitor[] FindAlreadyGateIn()
        {
            DetachedCriteria dc = DetachedCriteria.For<Visitor>();
            dc.Add(Expression.IsNotNull("JamInput"));
            dc.Add(Expression.IsNotNull("Masuk"));
            dc.Add(Expression.IsNull("Keluar"));
            dc.Add(Expression.Eq("IsAktif", true));
            dc.AddOrder(Order.Asc("JamInput"));
            return (Visitor[])FindAll(typeof(Visitor), dc);
        }
    }
}
