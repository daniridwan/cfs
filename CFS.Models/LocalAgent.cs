﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Local_Agent")]
    public class LocalAgent : ActiveRecordBase
    {
        public List<string> messages;
        private int id;
        private string agentName;

        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public int Id { get { return id; } set { id = value; } }
        [Property("Agent_Name")]
        public string AgentName { get { return agentName; } set { agentName = value; } }

        public LocalAgent()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static LocalAgent[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<LocalAgent>();
            dc.AddOrder(Order.Asc("AgentName"));
            return (LocalAgent[])FindAll(typeof(LocalAgent), dc);
        }
    }
}
