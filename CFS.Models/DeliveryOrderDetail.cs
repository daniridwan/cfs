﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Delivery_Order_Detail")]
    public class DeliveryOrderDetail : ActiveRecordBase
    {
        private List<string> messages;
        private long deliveryOrderDetailId;
        private long deliveryOrderId;
        private string deliveryOrderNumber;
        private string customerReferenceNumber;
        private long productId;
        private string tankName;
        private float quantity;
        private string uom;
        private string shipToAddress;
        private SimpleUser firstApprover;
        private string firstApproverResponse;
        private DateTime? firstApproverTimestamp;
        private SimpleUser secondApprover;
        private string secondApproverResponse;
        private DateTime? secondApproverTimestamp;
        private string rejectReason;
        private float actualQuantity;
        private string blanketDONumber;

        [PrimaryKey(PrimaryKeyType.Identity, "Delivery_Order_Detail_Id")]
        public long DeliveryOrderDetailId { get { return deliveryOrderDetailId; } set { deliveryOrderDetailId = value; } }
        [Property("Delivery_Order_Id")]
        public long DeliveryOrderId { get { return deliveryOrderId; } set { deliveryOrderId = value; } }
        [Property("Delivery_Order_Number")]
        public string DeliveryOrderNumber { get { return deliveryOrderNumber; } set { deliveryOrderNumber = value; } }
        [Property("Customer_Reference_Number")]
        public string CustomerReferenceNumber { get { return customerReferenceNumber; } set { customerReferenceNumber = value; } }
        [Property("Product_Id")]
        public long ProductId { get { return productId; } set { productId = value; } }
        [Property("Tank_Name")]
        public string TankName { get { return tankName; } set { tankName = value; } }
        [Property("Quantity")]
        public float Quantity { get { return quantity; } set { quantity = value; } }
        [Property("UOM")]
        public string UOM { get { return uom; } set { uom = value; } }
        [Property("Ship_To_Address")]
        public string ShipToAddress { get { return shipToAddress; } set { shipToAddress = value; } }

        [ScriptIgnore]
        [BelongsTo("First_Approver")]
        public SimpleUser FirstApprover { get { return firstApprover; } set { firstApprover = value; } }
        [Property("First_Approver_Response")]
        public string FirstApproverResponse { get { return firstApproverResponse; } set { firstApproverResponse = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("First_Approver_Timestamp")]
        public DateTime? FirstApproverTimestamp { get { return firstApproverTimestamp; } set { firstApproverTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Second_Approver")]
        public SimpleUser SecondApprover { get { return secondApprover; } set { secondApprover = value; } }
        [Property("Second_Approver_Response")]
        public string SecondApproverResponse { get { return secondApproverResponse; } set { secondApproverResponse = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Second_Approver_Timestamp")]
        public DateTime? SecondApproverTimestamp { get { return secondApproverTimestamp; } set { secondApproverTimestamp = value; } }
        [Property("Reject_Reason")]
        public string RejectReason { get { return rejectReason; } set { rejectReason = value; } }
        [Property("Actual_Quantity")]
        public float ActualQuantity { get { return actualQuantity; } set { actualQuantity = value; } }
        [Property("Blanket_DO_Number")]
        public string BlanketDONumber { get { return blanketDONumber; } set { blanketDONumber = value; } }

        public string FirstApproverByName { get { return firstApprover != null ? firstApprover.Name : string.Empty; } }
        public string SecondApproverByName { get { return secondApprover != null ? secondApprover.Name : string.Empty; } }

        public string ProductName
        {
            get
            {
                Product product = Product.FindById(ProductId);
                if (product != null)
                {
                    return product.ProductName;
                }
                else
                {
                    return "";
                }
            }
        }

        public DeliveryOrderDetail()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(DeliveryOrder), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(DeliveryOrderDetail));
            DetachedCriteria countCriteria = DetachedCriteria.For<DeliveryOrderDetail>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltCustomerReferenceNumber"] != null && param.SearchParam["fltCustomerReferenceNumber"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("CustomerReferenceNumber", param.SearchParam["fltCustomerReferenceNumber"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("CustomerReferenceNumber", param.SearchParam["fltCustomerReferenceNumber"], MatchMode.Anywhere));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<DeliveryOrderDetail>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static DeliveryOrderDetail FindById(Int64 id)
        {
            return (DeliveryOrderDetail)FindByPrimaryKey(typeof(DeliveryOrderDetail), id);
        }

        public static DeliveryOrderDetail[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<DeliveryOrderDetail>();
            dc.AddOrder(Order.Asc("DeliveryOrderId"));
            return (DeliveryOrderDetail[])FindAll(typeof(DeliveryOrderDetail), dc);
        }

        public static DeliveryOrderDetail FindByCustomerReferenceNumber(string customerReferenceNumber)
        {
            ICriterion[] query = { Expression.Eq("CustomerReferenceNumber", customerReferenceNumber) };
            return (DeliveryOrderDetail)FindOne(typeof(DeliveryOrderDetail), query);
        }
        
        public static DeliveryOrderDetail FindByDeliveryOrderNumber(string deliveryOrderNumber)
        {
            ICriterion[] query = { Expression.Eq("DeliveryOrderNumber", deliveryOrderNumber) };
            return (DeliveryOrderDetail)FindOne(typeof(DeliveryOrderDetail), query);
        }

        public static DeliveryOrderDetail[] FindByDeliveryOrderId(long deliveryOrderId)
        {
            DetachedCriteria dc = DetachedCriteria.For<DeliveryOrderDetail>();
            dc.Add(Expression.Eq("DeliveryOrderId", deliveryOrderId));
            return (DeliveryOrderDetail[])FindAll(typeof(DeliveryOrderDetail), dc);
        }

        public static DeliveryOrderDetail[] FindByDeliveryOrderIdAndConsignee(long deliveryOrderId, string consignee)
        {
            DetachedCriteria dc = DetachedCriteria.For<DeliveryOrderDetail>();
            dc.Add(Expression.Eq("DeliveryOrderId", deliveryOrderId));
            if (consignee == "")
            {
                dc.Add(Expression.IsNull("ShipToAddress"));
            }
            else
            {
                dc.Add(Expression.Eq("ShipToAddress", consignee));
            }
            return (DeliveryOrderDetail[])FindAll(typeof(DeliveryOrderDetail), dc);
        }

        public bool Validate()
        {
            bool result = true;
            return result;
        }
    }
}
