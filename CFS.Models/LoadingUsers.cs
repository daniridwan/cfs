﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;
using System.Web;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Loading_Users")]
    public class LoadingUsers : ActiveRecordBase
    {
        private List<string> messages;
        private int id;
        private string name;

        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public int Id { get { return id; } set { id = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }

        public LoadingUsers()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static LoadingUsers[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<LoadingUsers>();
            dc.AddOrder(Order.Asc("Name"));
            return (LoadingUsers[])FindAll(typeof(LoadingUsers), dc);
        }
    }
}
