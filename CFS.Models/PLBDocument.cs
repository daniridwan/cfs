﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("PLB_Document")]
    public class PlbDocument : ActiveRecordBase
    {
        private List<string> messages;
        private long id;
        private long vesselDocumentId;
        private string plbSppbNo;
        private DateTime? plbSppbDate;
        private float plbQuantity;
        private DateTime? approvedTimestamp;
        private SimpleUser approvedBy;

        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public long Id { get { return id; } set { id = value; } }
        [Property("Vessel_Document_Id")]
        public long VesselDocumentId { get { return vesselDocumentId; } set { vesselDocumentId = value; } }
        [Property("SPPB_No")]
        public string PlbSppbNo { get { return plbSppbNo; } set { plbSppbNo = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("SPPB_Date")]
        public DateTime? PlbSppbDate { get { return plbSppbDate; } set { plbSppbDate = value; } }
        [Property("Quantity")]
        public float PlbQuantity { get { return plbQuantity; } set { plbQuantity = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Approved_Timestamp")]
        public DateTime? ApprovedTimestamp { get { return approvedTimestamp; } set { approvedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Approved_By")]
        public SimpleUser ApprovedBy { get { return approvedBy; } set { approvedBy = value; } }

        public string ApprovedByName { get { return approvedBy != null ? approvedBy.Username : string.Empty; } }

        public PlbDocument()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(PlbDocument), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(PlbDocument));
            DetachedCriteria countCriteria = DetachedCriteria.For<PlbDocument>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<PlbDocument>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static PlbDocument[] FindByVesselDocumentId(long vesselDocumentId)
        {
            ICriterion[] query = { Expression.Eq("VesselDocumentId", vesselDocumentId) };
            return (PlbDocument[])FindAll(typeof(PlbDocument), query);
        }

        public static PlbDocument[] FindApprovedByVesselDocumentId(long vesselDocumentId)
        {
            DetachedCriteria dc = DetachedCriteria.For<PlbDocument>();
            dc.Add(Expression.IsNotNull("ApprovedBy"));
            return (PlbDocument[])FindAll(typeof(PlbDocument), dc);
        }

        public static PlbDocument FindById(long id)
        {
            return (PlbDocument)FindByPrimaryKey(typeof(PlbDocument), id);
        }
    }
}
