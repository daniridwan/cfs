﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Vessel_Document")]
    public class VesselDocument : ActiveRecordBase
    {
        public List<string> messages;
        private long vesselDocumentId;
        private string vesselNo;
        private long vesselId;
        private string type;
        private long productId;
        private DateTime? estimateDate; //baru
        private DateTime? arrivalDate;
        private string localAgent; //baru
        private string jetty; //baru
        private string loadingType; //baru
        private string fromPort; //baru
        private string toPort; //baru
        private string blNo;
        private float blQuantity;
        private string blUom;
        private float blCost; //baru
        private long? companyId;
        private string sppbNo;
        private DateTime? sppbDate;
        private string pibNo;
        private DateTime? pibDate;
        private string remarks;
        private string status; //baru
        private int isActive;
        private DateTime? approvalTimestamp;
        private SimpleUser approver;
        private string filename;

        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        private IList<VesselTank> vesselTank;
        private IList<PlbDocument> plbDocument;

        [PrimaryKey(PrimaryKeyType.Identity, "Vessel_Document_Id")]
        public long VesselDocumentId { get { return vesselDocumentId; } set { vesselDocumentId = value; } }
        [Property("Vessel_No")]
        public string VesselNo { get { return vesselNo; } set { vesselNo = value; } }
        [Property("Vessel_Id")]
        public long VesselId { get { return vesselId; } set { vesselId = value; } }
        [Property("Type")]
        public string Type { get { return type; } set { type = value; } }
        [Property("Product_Id")]
        public long ProductId { get { return productId; } set { productId = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Estimate_Date")]
        public DateTime? EstimateDate { get { return estimateDate; } set { estimateDate = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Arrival_Date")]
        public DateTime? ArrivalDate { get { return arrivalDate; } set { arrivalDate = value; } }
        [Property("BL_No")]
        public string BlNo { get { return blNo; } set { blNo = value; } }
        [Property("BL_Quantity")]
        public float BlQuantity { get { return blQuantity; } set { blQuantity = value; } }
        [Property("BL_UOM")]
        public string BlUom { get { return blUom; } set { blUom = value; } }
        [Property("BL_Cost")]
        public float BlCost { get { return blCost; } set { blCost = value; } }
        [Property("Company_Id")]
        public long? CompanyId { get { return companyId; } set { companyId = value; } }
        [Property("SPPB_No")]
        public string SppbNo { get { return sppbNo; } set { sppbNo = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("SPPB_Date")]
        public DateTime? SppbDate { get { return sppbDate; } set { sppbDate = value; } }
        [Property("PIB_No")]
        public string PibNo { get { return pibNo; } set { pibNo = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("PIB_Date")]
        public DateTime? PibDate { get { return pibDate; } set { pibDate = value; } }
        [Property("Remarks")]
        public string Remarks { get { return remarks; } set { remarks = value; } }
        [Property("Status")]
        public string Status { get { return status; } set { status = value; } }
        [Property("Is_Active")]
        public int IsActive { get { return isActive; } set { isActive = value; } }

        [Property("Local_Agent")]
        public string LocalAgent { get { return localAgent; } set { localAgent = value; } }
        [Property("Jetty")]
        public string Jetty { get { return jetty; } set { jetty = value; } }
        [Property("Loading_Type")]
        public string LoadingType { get { return loadingType; } set { loadingType = value; } }
        [Property("From_Port")]
        public string FromPort { get { return fromPort; } set { fromPort = value; } }
        [Property("To_Port")]
        public string ToPort { get { return toPort; } set { toPort = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Approval_Timestamp")]
        public DateTime? ApprovalTimestamp { get { return approvalTimestamp; } set { approvalTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Approver")]
        public SimpleUser Approver { get { return approver; } set { approver = value; } }
        [Property("Filename")]
        public string Filename { get { return filename; } set { filename = value; } }

        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        [HasMany(typeof(VesselTank), Table = "VesselTank", ColumnKey = "Vessel_Document_Id")]
        public IList<VesselTank> VesselTank { get { return vesselTank; } set { vesselTank = value; } }

        [HasMany(typeof(PlbDocument), Table = "PlbDocument", ColumnKey = "Vessel_Document_Id")]
        public IList<PlbDocument> PlbDocument { get { return plbDocument; } set { plbDocument = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }
        public string ApproverByName { get { return approver != null ? approver.Username : string.Empty; } }

        public string ProductName
        {
            get
            {
                Product product = Product.FindById(ProductId);
                if (product != null)
                {
                    return product.ProductName;
                }
                else
                {
                    return "";
                }
            }
        }

        public string CompanyName
        {
            get
            {
                Company company = Company.FindById(CompanyId);
                if (company != null)
                {
                    return company.CompanyName;
                }
                else
                {
                    return "";
                }
            }
        }

        public string VesselName
        {
            get
            {
                Vessel vessel = Vessel.FindById(VesselId);
                if (vessel != null)
                {
                    return vessel.VesselName;
                }
                else
                {
                    return "";
                }
            }
        }

        public string Ticket { get; set; }

        public VesselDocument()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(VesselDocument), new NHibernateDelegate(SelectPagingCallback), param);
        }

        public static PagingResult SelectPagingPLB(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(VesselDocument), new NHibernateDelegate(SelectPagingPLBCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(VesselDocument));
            DetachedCriteria countCriteria = DetachedCriteria.For<VesselDocument>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltVesselNo"] != null && param.SearchParam["fltVesselNo"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("VesselNo", param.SearchParam["fltVesselNo"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("VesselNo", param.SearchParam["fltVesselNo"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltVesselId"] != null && param.SearchParam["fltVesselId"].Length != 0)
                {
                    criteria.Add(Expression.Eq("VesselId", Convert.ToInt64(param.SearchParam["fltVesselId"])));
                    countCriteria.Add(Expression.Eq("VesselId", Convert.ToInt64(param.SearchParam["fltVesselId"])));
                }
                if (param.SearchParam["fltSppbNo"] != null && param.SearchParam["fltSppbNo"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("SppbNo", param.SearchParam["fltSppbNo"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("SppbNo", param.SearchParam["fltSppbNo"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltBlNo"] != null && param.SearchParam["fltBlNo"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("BlNo", param.SearchParam["fltBlNo"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("BlNo", param.SearchParam["fltBlNo"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltCompany"] != null && param.SearchParam["fltCompany"].Length != 0)
                {
                    criteria.Add(Expression.Eq("CompanyId", Convert.ToInt64(param.SearchParam["fltCompany"])));
                    countCriteria.Add(Expression.Eq("CompanyId", Convert.ToInt64(param.SearchParam["fltCompany"])));
                }
                if (param.SearchParam["fltStatus"] != null && param.SearchParam["fltStatus"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Status", param.SearchParam["fltStatus"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Status", param.SearchParam["fltStatus"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltIsActive"] != null && param.SearchParam["fltIsActive"].Length != 0)
                {
                    criteria.Add(Expression.Eq("IsActive", Convert.ToInt32(param.SearchParam["fltIsActive"])));
                    countCriteria.Add(Expression.Eq("IsActive", Convert.ToInt32(param.SearchParam["fltIsActive"])));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<VesselDocument>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        private static object SelectPagingPLBCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(VesselDocument));
            DetachedCriteria countCriteria = DetachedCriteria.For<VesselDocument>();
            
            //if(Users.GetCurrentUser().HasPermission("View PLB Only"))
            //{
                criteria.Add(Expression.Eq("Type", "Import PLB"));
                countCriteria.Add(Expression.Eq("Type", "Import PLB"));
            //}
            
            criteria.Add(Expression.Eq("Status", "Approved"));
            countCriteria.Add(Expression.Eq("Status", "Approved"));

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltVesselNo"] != null && param.SearchParam["fltVesselNo"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("VesselNo", param.SearchParam["fltVesselNo"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("VesselNo", param.SearchParam["fltVesselNo"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltBlNo"] != null && param.SearchParam["fltBlNo"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("BlNo", param.SearchParam["fltBlNo"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("BlNo", param.SearchParam["fltBlNo"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltCompany"] != null && param.SearchParam["fltCompany"].Length != 0)
                {
                    criteria.Add(Expression.Eq("CompanyId", Convert.ToInt64(param.SearchParam["fltCompany"])));
                    countCriteria.Add(Expression.Eq("CompanyId", Convert.ToInt64(param.SearchParam["fltCompany"])));
                }
                if (param.SearchParam["fltIsActive"] != null && param.SearchParam["fltIsActive"].Length != 0)
                {
                    criteria.Add(Expression.Eq("IsActive", Convert.ToInt32(param.SearchParam["fltIsActive"])));
                    countCriteria.Add(Expression.Eq("IsActive", Convert.ToInt32(param.SearchParam["fltIsActive"])));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<VesselDocument>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static VesselDocument[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<VesselDocument>();
            dc.AddOrder(Order.Asc("VesselNo"));
            return (VesselDocument[])FindAll(typeof(VesselDocument), dc);
        }

        public static VesselDocument[] FindAllPlb()
        {
            DetachedCriteria dc = DetachedCriteria.For<VesselDocument>();
            dc.Add(Expression.Eq("Type", "Import PLB"));
            dc.AddOrder(Order.Asc("VesselNo"));
            return (VesselDocument[])FindAll(typeof(VesselDocument), dc);
        }

        public static VesselDocument FindById(long id)
        {
            return (VesselDocument)FindByPrimaryKey(typeof(VesselDocument), id);
        }

        public static VesselDocument FindByVesselNo(string vesselNo)
        {
            ICriterion[] query = { Expression.Eq("VesselNo", vesselNo) };
            return (VesselDocument)FindOne(typeof(VesselDocument), query);
        }

        public static VesselDocument[] FindByCompanyAndProduct(long companyId, long productId)
        {
            DetachedCriteria dc = DetachedCriteria.For<VesselDocument>();
            dc.Add(Expression.Eq("CompanyId", companyId));
            dc.Add(Expression.Eq("ProductId", productId));
            dc.Add(Expression.Eq("IsActive", 1));
            dc.AddOrder(Order.Asc("VesselNo"));
            return (VesselDocument[])FindAll(typeof(VesselDocument), dc);
        }

        public bool Validate()
        {
            bool result = true;
            if (Type == "Import")
            {
                //if(SppbDate == null)
                //{
                //    messages.Add("SPPB Date required for Import type.");
                //    result = false;
                //}
                //if (PibDate == null)
                //{
                //    messages.Add("PIB Date required for Import type.");
                //    result = false;
                //}

                //if (blUom == null)
                //{
                //    messages.Add("BL UOM required for Import type.");
                //    result = false;
                //}
            }
            if (BlQuantity <= 0)
            {
                messages.Add("BL Quantity must be greater than 0.");
                result = false;
            }
            if (VesselTank == null)
            {
                messages.Add("Please add destination tank.");
                result = false;
            }
            else { 
                foreach (VesselTank vt in VesselTank)
                {
                    if (vt.Quantity <= 0)
                    {
                        messages.Add("Tank Quantity must be greater than 0");
                        result = false;
                    }
                }
            }
            return result;
        }

        public bool Validate(string status)
        {
            bool result = true;
            //if (status == "Scheduled") { 
            //    if(Filename == null)
            //    {
            //        messages.Add("Please upload inward manifest document.");
            //        result = false;
            //    }
            //}
            return result;
        }
    }
}
