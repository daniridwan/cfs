﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Port")]
    public class Port : ActiveRecordBase
    {
        public List<string> messages;
        private int id;
        private string portName;

        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public int Id { get { return id; } set { id = value; } }
        [Property("Port_Name")]
        public string PortName { get { return portName; } set { portName = value; } }

        public Port()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static Port[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Port>();
            dc.AddOrder(Order.Asc("PortName"));
            return (Port[])FindAll(typeof(Port), dc);
        }
    }
}
