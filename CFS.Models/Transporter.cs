﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Transporter")]
    public class Transporter : ActiveRecordBase
    {
        private List<string> messages;
        private int id;
        private string transporterCode;
        private string name;
        private string address;
        private string city;
        private string postalCode;

        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public int Id { get { return id; } set { id = value; } }
        [Property("Transporter_Code")]
        public string TransporterCode { get { return transporterCode; } set { transporterCode = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }
        [Property("Address")]
        public string Address { get { return address; } set { address = value; } }
        [Property("City")]
        public string City { get { return city; } set { city = value; } }
        [Property("Postal_Code")]
        public string PostalCode { get { return postalCode; } set { postalCode = value; } }

        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }

        public Transporter()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Transporter), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Transporter));
            DetachedCriteria countCriteria = DetachedCriteria.For<Transporter>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltTransporterCode"] != null && param.SearchParam["fltTransporterCode"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("TransporterCode", param.SearchParam["fltTransporterCode"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("TransporterCode", param.SearchParam["fltTransporterCode"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Transporter>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static Transporter[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Transporter>();
            dc.AddOrder(Order.Asc("TransporterCode"));
            return (Transporter[])FindAll(typeof(Transporter), dc);
        }

        public static Transporter FindById(int id)
        {
            return (Transporter)FindByPrimaryKey(typeof(Transporter), id);
        }

        public static Transporter FindByName(string name)
        {
            ICriterion[] query = { Expression.Eq("Name", name) };
            return (Transporter)FindOne(typeof(Transporter), query);
        }

        public static Transporter FindByCode(string code)
        {
            ICriterion[] query = { Expression.Eq("TransporterCode", code) };
            return (Transporter)FindOne(typeof(Transporter), query);
        }

        public bool Validate()
        {
            bool result = true;
            return result;
        }
    }
}
