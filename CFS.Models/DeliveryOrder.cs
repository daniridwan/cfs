﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Delivery_Order")]
    public class DeliveryOrder : ActiveRecordBase
    {
        private List<string> messages;
        private long deliveryOrderId;
        private string deliveryOrderNumber;
        private DateTime? deliveryDate;
        private long companyId;
        private string truck;
        private string driver;
        private string type;
        private string status;
        private string remarks;
        private int? isCancelAvailable;
        private string cancelReason;
        private DateTime? cancelRequestTimestamp;
        private string cancelResponse;
        private string cancelResponseNotes;
        private SimpleUser cancelApprover;
        private DateTime? cancelResponseTimestamp;

        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        private IList<DeliveryOrderDetail> deliveryOrderDetail;

        [PrimaryKey(PrimaryKeyType.Identity, "Delivery_Order_Id")]
        public long DeliveryOrderId { get { return deliveryOrderId; } set { deliveryOrderId = value; } }
        [Property("Delivery_Order_Number")]
        public string DeliveryOrderNumber { get { return deliveryOrderNumber; } set { deliveryOrderNumber = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Delivery_Date")]
        public DateTime? DeliveryDate { get { return deliveryDate; } set { deliveryDate = value; } }
        [Property("Company_Id")]
        public long CompanyId { get { return companyId; } set { companyId = value; } }
        [Property("Truck")]
        public string Truck { get { return truck; } set { truck = value; } }
        [Property("Driver")]
        public string Driver { get { return driver; } set { driver = value; } }
        [Property("Type")]
        public string Type { get { return type; } set { type = value; } }
        [Property("Status")]
        public string Status { get { return status; } set { status = value; } }
        [Property("Remarks")]
        public string Remarks { get { return remarks; } set { remarks = value; } }
        [Property("Is_Cancel_Available")] //null = blm pernah req, 0 = wait approval, 1 =    not available
        public int? IsCancelAvailable { get { return isCancelAvailable; } set { isCancelAvailable = value; } }
        [Property("Cancel_Reason")]
        public string CancelReason { get { return cancelReason; } set { cancelReason = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Cancel_Request_Timestamp")]
        public DateTime? CancelRequestTimestamp { get { return cancelRequestTimestamp; } set { cancelRequestTimestamp = value; } }
        [Property("Cancel_Response")]
        public string CancelResponse { get { return cancelResponse; } set { cancelResponse = value; } }
        [Property("Cancel_Response_Notes")]
        public string CancelResponseNotes { get { return cancelResponseNotes; } set { cancelResponseNotes = value; } }
        [BelongsTo("Cancel_Approver")]
        public SimpleUser CancelApprover { get { return cancelApprover; } set { cancelApprover = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Cancel_Response_Timestamp")]
        public DateTime? CancelResponseTimestamp { get { return cancelResponseTimestamp; } set { cancelResponseTimestamp = value; } }

        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        //relationmapping
        [HasMany(typeof(DeliveryOrderDetail), Table = "Delivery_Order_Detail", ColumnKey = "Delivery_Order_Id")]
        public IList<DeliveryOrderDetail> DeliveryOrderDetail { get { return deliveryOrderDetail; } set { deliveryOrderDetail = value; } }
        //helper
        public string CreatedByName { get { return createdBy != null ? createdBy.Name : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Name : string.Empty; } }
        public string CreatedByUsername { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByUsername { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }
        public string CancelApproverByName { get { return cancelApprover != null ? cancelApprover.Name : string.Empty; } }

        public string CompanyName
        {
            get
            {
                Company company = Company.FindById(CompanyId);
                if (company != null)
                {
                    return company.CompanyName;
                }
                else
                {
                    return "";
                }
            }
        }

        public string ConsigneeString
        {
            get
            {
                IList<string> list = new List<string>();
                foreach (DeliveryOrderDetail dod in deliveryOrderDetail)
                {
                    if (!list.Contains(dod.ShipToAddress))
                    {
                        list.Add(dod.ShipToAddress);
                    }
                }
                return string.Join(",", list);
            }
        }

        public string CustomerRefString
        {
            get
            {
                IList<string> list = new List<string>();
                foreach (DeliveryOrderDetail dod in deliveryOrderDetail)
                {
                    if (!list.Contains(dod.CustomerReferenceNumber))
                    {
                        list.Add(dod.CustomerReferenceNumber);
                    }
                }
                return string.Join(",", list);
            }
        }

        public DeliveryOrder()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(DeliveryOrder), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(DeliveryOrder));
            DetachedCriteria countCriteria = DetachedCriteria.For<DeliveryOrder>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltDeliveryOrderNumber"] != null && param.SearchParam["fltDeliveryOrderNumber"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("DeliveryOrderNumber", param.SearchParam["fltDeliveryOrderNumber"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("DeliveryOrderNumber", param.SearchParam["fltDeliveryOrderNumber"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltType"] != null && param.SearchParam["fltType"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Type", param.SearchParam["fltType"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Type", param.SearchParam["fltType"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltCompany"] != null && param.SearchParam["fltCompany"].Length != 0)
                {
                    criteria.Add(Expression.Eq("CompanyId", Convert.ToInt64(param.SearchParam["fltCompany"])));
                    countCriteria.Add(Expression.Eq("CompanyId", Convert.ToInt64(param.SearchParam["fltCompany"])));
                }
                if (param.SearchParam["fltStatus"] != null && param.SearchParam["fltStatus"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Status", param.SearchParam["fltStatus"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Status", param.SearchParam["fltStatus"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltStatusDashboard"] != null && param.SearchParam["fltStatusDashboard"].Length != 0)
                {
                    criteria.Add(Expression.Or(Expression.Eq("Status", "In Progress"), Expression.Eq("Status", "Submitted")));
                    countCriteria.Add(Expression.Or(Expression.Eq("Status", "In Progress"), Expression.Eq("Status", "Submitted")));
                }
                if (param.SearchParam["fltCustomerReferenceNumber"] != null && param.SearchParam["fltCustomerReferenceNumber"].Length != 0)
                {
                    criteria.CreateAlias("DeliveryOrderDetail", "dod", NHibernate.SqlCommand.JoinType.InnerJoin)
                    .Add(Restrictions.InsensitiveLike("dod.CustomerReferenceNumber", param.SearchParam["fltCustomerReferenceNumber"], MatchMode.Anywhere));
                    countCriteria.CreateAlias("DeliveryOrderDetail", "dod", NHibernate.SqlCommand.JoinType.InnerJoin)
                    .Add(Expression.InsensitiveLike("dod.CustomerReferenceNumber", param.SearchParam["fltCustomerReferenceNumber"], MatchMode.Anywhere));
                }
                //flt timestamp from
                if (param.SearchParam["fltDeliveryDateFrom"] != null && param.SearchParam["fltDeliveryDateFrom"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltDeliveryDateFrom"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Ge("DeliveryDate", startPeriod));
                    countCriteria.Add(Expression.Ge("DeliveryDate", startPeriod));
                }
                //flt timestamp to
                if (param.SearchParam["fltDeliveryDateTo"] != null && param.SearchParam["fltDeliveryDateTo"].Length != 0)
                {
                    DateTime endPeriod = DateTime.ParseExact(param.SearchParam["fltDeliveryDateTo"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Le("DeliveryDate", endPeriod));
                    countCriteria.Add(Expression.Le("DeliveryDate", endPeriod));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<DeliveryOrder>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static PagingResult SelectPagingCustomer(PagingQuery param, long companyId)
        {
            param.CompanyId = companyId;
            return (PagingResult)Execute(typeof(DeliveryOrder), new NHibernateDelegate(SelectPagingCustomerCallback), param);
        }

        private static object SelectPagingCustomerCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(DeliveryOrder));
            DetachedCriteria countCriteria = DetachedCriteria.For<DeliveryOrder>();

            //for customer only
            criteria.Add(Expression.Eq("CompanyId", param.CompanyId));
            countCriteria.Add(Expression.Eq("CompanyId", param.CompanyId));

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltDeliveryOrderNumber"] != null && param.SearchParam["fltDeliveryOrderNumber"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("DeliveryOrderNumber", param.SearchParam["fltDeliveryOrderNumber"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("DeliveryOrderNumber", param.SearchParam["fltDeliveryOrderNumber"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltType"] != null && param.SearchParam["fltType"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Type", param.SearchParam["fltType"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Type", param.SearchParam["fltType"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltStatus"] != null && param.SearchParam["fltStatus"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Status", param.SearchParam["fltStatus"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Status", param.SearchParam["fltStatus"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltStatusDashboard"] != null && param.SearchParam["fltStatusDashboard"].Length != 0)
                {
                    criteria.Add(Expression.Or(Expression.Eq("Status", "In Progress"), Expression.Eq("Status", "Submitted")));
                    countCriteria.Add(Expression.Or(Expression.Eq("Status", "In Progress"), Expression.Eq("Status", "Submitted")));
                }
                //flt timestamp from
                if (param.SearchParam["fltDeliveryDateFrom"] != null && param.SearchParam["fltDeliveryDateFrom"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltDeliveryDateFrom"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Ge("DeliveryDate", startPeriod));
                    countCriteria.Add(Expression.Ge("DeliveryDate", startPeriod));
                }
                //flt timestamp to
                if (param.SearchParam["fltDeliveryDateTo"] != null && param.SearchParam["fltDeliveryDateTo"].Length != 0)
                {
                    DateTime endPeriod = DateTime.ParseExact(param.SearchParam["fltDeliveryDateTo"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Le("DeliveryDate", endPeriod));
                    countCriteria.Add(Expression.Le("DeliveryDate", endPeriod));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<DeliveryOrder>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static DeliveryOrder FindById(Int64 id)
        {
            return (DeliveryOrder)FindByPrimaryKey(typeof(DeliveryOrder), id);
        }

        public static DeliveryOrder[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<DeliveryOrder>();
            dc.AddOrder(Order.Asc("DeliveryOrderNumber"));
            return (DeliveryOrder[])FindAll(typeof(DeliveryOrder), dc);
        }

        public static DeliveryOrder FindByShipmentNumber(string shipmentNumber)
        {
            DetachedCriteria dc = DetachedCriteria.For<DeliveryOrder>();
            dc.Add(Expression.Eq("DeliveryOrderNumber", shipmentNumber));
            dc.AddOrder(Order.Desc("DeliveryDate"));
            return (DeliveryOrder)FindOne(typeof(DeliveryOrder), dc);
        }

        public static DeliveryOrder[] FindByStatus(string status)
        {
            DetachedCriteria dc = DetachedCriteria.For<DeliveryOrder>();
            dc.Add(Expression.Eq("Status", status));
            dc.AddOrder(Order.Desc("DeliveryDate"));
            return (DeliveryOrder[])FindAll(typeof(DeliveryOrder), dc);
        }

        public bool Validate()
        {
            bool result = true;
            if (DeliveryOrderDetail == null)
            {
                messages.Add("Please add Delivery Order Detail");
                result = false;
            }
            if (Status == null || Status == string.Empty)
            {
                TimeSpan diffDate = (DeliveryDate - DateTime.Today).Value;
                if (diffDate.Days < 0)
                {
                    messages.Add("Cant request backdate.");
                    result = false;
                }
            }
            if (Status == "Submitted")
            {
                bool hasNull = false;
                foreach (DeliveryOrderDetail d in DeliveryOrderDetail)
                {
                    if (d.FirstApproverResponse == null)
                    {
                        hasNull = true;
                    }
                }
                if (hasNull)
                {
                    messages.Add("Please choose response for all request detail.");
                    result = false;
                }
            }
            if (Status == "In Progress")
            {
                bool hasNull = false;
                foreach (DeliveryOrderDetail d in DeliveryOrderDetail)
                {
                    if (d.SecondApproverResponse == null)
                    {
                        hasNull = true;
                    }
                }
                if (hasNull)
                {
                    messages.Add("Please choose response for all request detail.");
                    result = false;
                }
            }
            return result;
        }
    }
}
