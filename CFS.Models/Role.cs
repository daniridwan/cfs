﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Mapping;

namespace CFS.Models
{
    [ActiveRecord("Role")]
    public class Role : ActiveRecordBase
    {
        private List<string> messages;
        private int roleId;
        private string name;
        private IList<Permission> rolePermissions;

        private string permissionIdStr;
        private string permissionNameStr;

        [PrimaryKey(PrimaryKeyType.Identity, "Role_Id")]
        public int RoleId { get { return roleId; } set { roleId = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }

        [JsonIgnore]
        [ScriptIgnore]
        [HasAndBelongsToMany(typeof(Permission), Table = "Role_Permission", ColumnKey = "Role_Id", ColumnRef = "Permission_Id")]
        public IList<Permission> RolePermissions { get { return rolePermissions; } set { rolePermissions = value; } }

        public string PermissionIdStr
        {
            get
            {
                if (permissionIdStr == null || permissionIdStr.Length == 0)
                {
                    StringBuilder sb = new StringBuilder();
                    bool first = true;
                    foreach (Permission p in rolePermissions)
                    {
                        if (first) first = false;
                        else sb.Append(",");
                        sb.AppendFormat("{0}", p.PermissionId);
                    }
                    permissionIdStr = sb.ToString();
                }
                return permissionIdStr;
            }
            set { permissionIdStr = value; }
        }

        public string PermissionNameStr
        {
            get
            {
                if (permissionNameStr == null || permissionNameStr.Length == 0)
                {
                    StringBuilder sb = new StringBuilder();
                    bool first = true;
                    foreach (Permission p in rolePermissions)
                    {
                        if (first) first = false;
                        else sb.Append(",");
                        sb.AppendFormat("{0}", CFS.Models.Permission.FindById(p.PermissionId).Name.ToString());
                    }
                    permissionNameStr = sb.ToString();
                }
                return permissionNameStr;
            }
            set { permissionNameStr = value; }
        }

        public string[] PermissionName { get { return GetPermissionNames(); } }
        public string[] GetPermissionNames()
        {
            List<string> arr = new List<string>();
            foreach (CFS.Models.Permission p in rolePermissions)
            {
                arr.Add(CFS.Models.Permission.FindById(p.PermissionId).Name.ToString());
            }
            return arr.ToArray();
        }

        public RolePermission rolePermission { get; set; }

        public Role()
        {
            rolePermissions = new List<Permission>();
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static Role FindById(int id)
        {
            return (Role)FindByPrimaryKey(typeof(Role), id);
        }

        public static Role FindByName(string name)
        {
            ICriterion[] query = { Expression.Eq("Name", name) };
            return (Role)FindOne(typeof(Role), query);
        }

        public static Role[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Role>();
            dc.AddOrder(Order.Asc("Name"));
            return (Role[])FindAll(typeof(Role), dc);
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Role), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Role));
            DetachedCriteria countCriteria = DetachedCriteria.For<Role>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Asc)
                    criteria.AddOrder(Order.Asc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Desc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Role>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }
        public bool Validate(string saveType)
        {
            bool valid = true;
            if (saveType == "delete")
            {
                return valid;
            }
            if (saveType == "insert")
            {
                valid = true;
            }
            else if (saveType == "update")
            {
                valid = true;
            }

            if ((Name == null) || (Name == string.Empty))
            {
                messages.Add("Please Fill Role Name Field.");
                valid = false;
            }
            else if (Name.Length > 256)
            {
                messages.Add("Role Name Must be filled Less Than 256 Character");
                valid = false;
            }

            return valid;
        }
    }
}
