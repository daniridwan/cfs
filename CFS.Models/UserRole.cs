﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Mapping;

namespace CFS.Models
{
    [ActiveRecord("User_Role")]
    public class UserRole : ActiveRecordBase
    {
        private List<string> messages;
        private long id;
        private long userId;
        private long roleId;

        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public long Id { get { return id; } set { id = value; } }
        [Property("User_Id")]
        public long UserId { get { return userId; } set { userId = value; } }
        [Property("Role_Id")]
        public long RoleId { get { return roleId; } set { roleId = value; } }

        public static UserRole[] FindByUserId(long userid)
        {
            ICriterion[] query = { Expression.Eq("UserId", userid) };
            return (UserRole[])FindAll(typeof(UserRole), query);
        }

        public static UserRole FindByRoleId(int roleid)
        {
            ICriterion[] query = { Expression.Eq("RoleId", roleid) };
            return (UserRole)FindOne(typeof(UserRole), query);
        }
    }
}
