﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("View_Tank_Percentage")]
    public class ViewTankPercentage : ActiveRecordBase
    {
        private List<string> messages;
        private int tankId;
        private float liquidLevel;
        private float tankHeight;
        private float liquidPercent;
        private float vaporPercent;

        [PrimaryKey(PrimaryKeyType.Assigned, "Tank_Id")]
        public int TankId { get { return tankId; } set { tankId = value; } }
        [Property("Liquid_Level")]
        public float LiquidLevel { get { return liquidLevel; } set { liquidLevel = value; } }
        [Property("Tank_Height")]
        public float TankHeight { get { return tankHeight; } set { tankHeight = value; } }
        [Property("Liquid_Percent")]
        public float LiquidPercent { get { return liquidPercent; } set { liquidPercent = value; } }
        [Property("Vapor_Percent")]
        public float VaporPercent { get { return vaporPercent; } set { vaporPercent = value; } }

        public ViewTankPercentage()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static ViewTankPercentage[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewTankPercentage>();
            dc.AddOrder(Order.Asc("TankId"));
            return (ViewTankPercentage[])FindAll(typeof(ViewTankPercentage), dc);
        }
        public static ViewTankPercentage FindById(int id)
        {
            return (ViewTankPercentage)FindByPrimaryKey(typeof(ViewTankPercentage), id);
        }
    }
}