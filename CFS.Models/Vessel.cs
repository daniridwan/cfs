﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Vessel")]
    public class Vessel : ActiveRecordBase
    {
        public List<string> messages;
        private long vesselId;
        private string vesselName;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        [PrimaryKey(PrimaryKeyType.Identity, "Vessel_Id")]
        public long VesselId { get { return vesselId; } set { vesselId = value; } }
        [Property("Vessel_Name")]
        public string VesselName { get { return vesselName ; } set { vesselName = value; } }

        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Name : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Name : string.Empty; } }

        public Vessel()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Company), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Vessel));
            DetachedCriteria countCriteria = DetachedCriteria.For<Vessel>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltVesselName"] != null && param.SearchParam["fltVesselName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("VesselName", param.SearchParam["fltVesselName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("VesselName", param.SearchParam["fltVesselName"], MatchMode.Anywhere));
                }
            }
            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Company>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static Vessel[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Vessel>();
            dc.AddOrder(Order.Asc("VesselName"));
            return (Vessel[])FindAll(typeof(Vessel), dc);
        }

        public static Vessel FindById(long id)
        {
            return (Vessel)FindByPrimaryKey(typeof(Vessel), id);
        }

        public bool Validate()
        {
            bool result = true;
            return result;
        }
    }
}
