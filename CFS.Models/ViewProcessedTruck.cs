﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("View_Processed_Truck", Mutable = false)]
    public class ViewProcessedTruck : ActiveRecordBase
    {
        private List<string> messages;
        private int planningId;
        private int truckLoadingId;
        private string deliveryOrderNumber;
        private float preset;
        private string uom;
        private Product productId;
        private string fillingPointName;
        private string fillingPointGroup;
        private string tankName;
        private double actual;
        private double initialWeight;
        private double finalWeight;
        private long vesselDocumentId;
        private int shipmentStatus;

        [PrimaryKey(PrimaryKeyType.Assigned, "Planning_Id")]
        public int PlanningId { get { return planningId; } set { planningId = value; } }
        [Property("Truck_Loading_Id")]
        public int TruckLoadingId { get { return truckLoadingId; } set { truckLoadingId = value; } }
        [Property("Delivery_Order_Number")]
        public string DeliveryOrderNumber { get { return deliveryOrderNumber; } set { deliveryOrderNumber = value; } }
        [Property("Preset")]
        public float Preset { get { return preset; } set { preset = value; } }
        [ScriptIgnore]
        [BelongsTo("Product_Id")]
        public Product ProductId { get { return productId; } set { productId = value; } }
        [Property("UOM")]
        public string UOM { get { return uom; } set { uom = value; } }
        [Property("Filling_Point_Name")]
        public string FillingPointName { get { return fillingPointName; } set { fillingPointName = value; } }
        [Property("Filling_Point_Group")]
        public string FillingPointGroup { get { return fillingPointGroup; } set { fillingPointGroup = value; } }
        [Property("Tank_Name")]
        public string TankName { get { return tankName; } set { tankName = value; } }
        [Property("Actual")]
        public double Actual { get { return actual; } set { actual = value; } }
        [Property("Initial_Weight")]
        public double InitialWeight { get { return initialWeight; } set { initialWeight = value; } }
        [Property("Final_Weight")]
        public double FinalWeight { get { return finalWeight; } set { finalWeight = value; } }
        [Property("Vessel_Document_Id")]
        public long VesselDocumentId { get { return vesselDocumentId; } set { vesselDocumentId = value; } }
        [Property("Shipment_Status")]
        public int ShipmentStatus { get { return shipmentStatus; } set { shipmentStatus = value; } }

        public ViewProcessedTruck()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(ViewProcessedTruck), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(ViewProcessedTruck));
            DetachedCriteria countCriteria = DetachedCriteria.For<ViewProcessedTruck>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<ViewProcessedTruck>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static ViewProcessedTruck[] FindByShipmentStatus(int id, string fillingPointGroup)
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewProcessedTruck>();
            dc.Add(Expression.Eq("ShipmentStatus", id));
            dc.Add(Expression.Eq("FillingPointGroup", fillingPointGroup));
            dc.AddOrder(Order.Asc("PlanningId"));
            return (ViewProcessedTruck[])FindAll(typeof(ViewProcessedTruck), dc);
        }
    }
}
