﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("View_Truck_Tracking")]
    public class ViewTruckTracking : ActiveRecordBase
    {
        private List<string> messages;
        private int truckId;
        private int truckLoadingId;
        private string registrationPlate;
        private string deliveryOrderNumber;
        private string status;

        [PrimaryKey(PrimaryKeyType.Assigned, "Truck_Loading_Id")]
        public int TruckLoadingId { get { return truckLoadingId; } set { truckLoadingId = value; } }
        [Property("Truck_Id")]
        public int TruckId { get { return truckId; } set { truckId = value; } }
        [Property("Registration_Plate")]
        public string RegistrationPlate { get { return registrationPlate; } set { registrationPlate = value; } }
        [Property("Delivery_Order_Number")]
        public string DeliveryOrderNumber { get { return deliveryOrderNumber; } set { deliveryOrderNumber = value; } }
        [Property("Status")]
        public string Status { get { return status; } set { status = value; } }

        public ViewTruckTracking()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Truck), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(ViewTruckTracking));
            DetachedCriteria countCriteria = DetachedCriteria.For<ViewTruckTracking>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<ViewTruckTracking>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static ViewTruckTracking[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewTruckTracking>();
            dc.AddOrder(Order.Asc("Status"));
            return (ViewTruckTracking[])FindAll(typeof(ViewTruckTracking), dc);
        }

        public static ViewTruckTracking[] FindByStatus(string status)
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewTruckTracking>();
            dc.Add(Expression.Eq("Status", status));
            return (ViewTruckTracking[])FindAll(typeof(ViewTruckTracking), dc);
        }
    }
}
