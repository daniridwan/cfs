﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Tank")]
    public class Tank : ActiveRecordBase
    {
        public List<string> messages;
        private long tankId;
        private string name;
        private long productId;
        private long companyId;
        private DateTime? startDate;
        private DateTime? expirationDate;
        private float measurementLevel;
        private int isPlb;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        [PrimaryKey(PrimaryKeyType.Identity, "Tank_Id")]
        public long TankId { get { return tankId; } set { tankId = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }
        [Property("Product_Id")]
        public long ProductId { get { return productId; } set { productId = value; } }
        [Property("Company_Id")]
        public long CompanyId { get { return companyId; } set { companyId = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Start_Date")]
        public DateTime? StartDate { get { return startDate; } set { startDate = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Expiration_Date")]
        public DateTime? ExpirationDate { get { return expirationDate; } set { expirationDate = value; } }
        [Property("Measurement_Level")]
        public float MeasurementLevel { get { return measurementLevel; } set { measurementLevel = value; } }
        [Property("Is_PLB")]
        public int IsPlb { get { return isPlb; } set { isPlb = value; } }

        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }

        public string ProductName
        {
            get
            {
                Product product = Product.FindById(ProductId);
                if (product != null)
                {
                    return product.ProductName;
                }
                else
                {
                    return "";
                }
            }
        }

        public string CompanyName
        {
            get
            {
                Company company = Company.FindById(CompanyId);
                if (company != null)
                {
                    return company.CompanyName;
                }
                else
                {
                    return "";
                }
            }
        }

        public Tank()
        {
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Tank), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Tank));
            DetachedCriteria countCriteria = DetachedCriteria.For<Tank>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltCompany"] != null && param.SearchParam["fltCompany"].Length != 0)
                {
                    criteria.Add(Expression.Eq("CompanyId", Convert.ToInt64(param.SearchParam["fltCompany"])));
                    countCriteria.Add(Expression.Eq("CompanyId", Convert.ToInt64(param.SearchParam["fltCompany"])));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Tank>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static Tank[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Tank>();
            dc.AddOrder(Order.Asc("Name"));
            return (Tank[])FindAll(typeof(Tank), dc);
        }

        public static Tank FindById(long id)
        {
            return (Tank)FindByPrimaryKey(typeof(Tank), id);
        }

        public static Tank FindByName(string name)
        {
            ICriterion[] query = { Expression.Eq("Name", name) };
            return (Tank)FindOne(typeof(Tank), query);
        }

        public static Tank[] FindByCompany(long companyId)
        {
            DetachedCriteria dc = DetachedCriteria.For<Tank>();
            dc.Add(Expression.Eq("CompanyId", companyId));
            dc.AddOrder(Order.Asc("Name"));
            return (Tank[])FindAll(typeof(Tank), dc);
        }

        public bool Validate()
        {
            bool result = true;
            return result;
        }

    }
}
