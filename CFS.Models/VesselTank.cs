﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Vessel_Tank")]
    public class VesselTank : ActiveRecordBase
    {
        private List<string> messages;
        private long vesselTankId;
        private long vesselDocumentId;
        private long tankId;
        private float quantity;
        private float spillage;

        [PrimaryKey(PrimaryKeyType.Identity, "Vessel_Tank_Id")]
        public long VesselTankId { get { return vesselTankId; } set { vesselTankId = value; } }
        [Property("Vessel_Document_Id")]
        public long VesselDocumentId { get { return vesselDocumentId; } set { vesselDocumentId = value; } }
        [Property("Tank_Id")]
        public long TankId { get { return tankId; } set { tankId = value; } }
        [Property("Quantity")]
        public float Quantity { get { return quantity; } set { quantity = value; } }
        [Property("Spillage")]
        public float Spillage { get { return spillage; } set { spillage = value; } }

        public string TankName
        {
            get
            {
                Tank tank = Tank.FindById(TankId);
                if (tank != null)
                {
                    return tank.Name;
                }
                else
                {
                    return "";
                }
            }
        }

        public VesselTank()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(VesselTank), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(VesselTank));
            DetachedCriteria countCriteria = DetachedCriteria.For<VesselTank>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<VesselTank>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }
    }
}
