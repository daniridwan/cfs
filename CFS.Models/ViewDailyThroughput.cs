﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("View_Daily_Throughput", Mutable = false)]
    public class ViewDailyThroughput : ActiveRecordBase
    {
        private List<string> messages;
        private DateTime? throughputDate;
        private float throughput;

        
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [PrimaryKey(PrimaryKeyType.Identity, "Throughput_Date")]
        public DateTime? ThroughputDate { get { return throughputDate; } set { throughputDate = value; } }
        [Property("Throughput")]
        public float Throughput { get { return throughput; } set { throughput = value; } }

        public ViewDailyThroughput()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(ViewDailyThroughput), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(ViewDailyThroughput));
            DetachedCriteria countCriteria = DetachedCriteria.For<ViewDailyThroughput>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                //flt timestamp from
                if (param.SearchParam["fltTimestampFrom"] != null && param.SearchParam["fltTimestampFrom"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampFrom"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Gt("ThroughputDate", startPeriod));
                    countCriteria.Add(Expression.Gt("ThroughputDate", startPeriod));
                }
                //flt timestamp to
                if (param.SearchParam["fltTimestampTo"] != null && param.SearchParam["fltTimestampTo"].Length != 0)
                {
                    DateTime endPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampTo"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Le("ThroughputDate", endPeriod));
                    countCriteria.Add(Expression.Le("ThroughputDate", endPeriod));
                }
                if (param.SearchParam["fltDate"] != null && param.SearchParam["fltDate"].Length != 0)
                {
                    DateTime date = DateTime.ParseExact(param.SearchParam["fltDate"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Eq("ThroughputDate", date));
                    countCriteria.Add(Expression.Eq("ThroughputDate", date));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<ViewDailyThroughput>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static ViewDailyThroughput[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewDailyThroughput>();
            dc.AddOrder(Order.Asc("ThroughputDate"));
            return (ViewDailyThroughput[])FindAll(typeof(ViewDailyThroughput), dc);
        }

        public static ViewDailyThroughput[] FindbyDate(DateTime throughputDate)
        {
            DateTime throughputDate2 = throughputDate.AddDays(7);
            ICriterion[] query = { Expression.Between("ThroughputDate", throughputDate, throughputDate2) };
            return (ViewDailyThroughput[])FindAll(typeof(ViewDailyThroughput), query);
        }

        public static ViewDailyThroughput[] FindCurrentMonthThroughput()
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewDailyThroughput>();
            dc.Add(Expression.Ge("ThroughputDate", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0)));
            dc.Add(Expression.Le("ThroughputDate", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month), 23, 59, 59)));
            dc.AddOrder(Order.Asc("ThroughputDate"));
            return (ViewDailyThroughput[])FindAll(typeof(ViewDailyThroughput), dc);
        }
    }
}
