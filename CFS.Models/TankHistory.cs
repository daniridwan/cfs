﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Tank_History")]
    public class TankHistory : ActiveRecordBase
    {
        public List<string> messages;
        private long tankHistoryId;
        private long tankId;
        private string name;
        private long productId;
        private long companyId;
        private DateTime? startDate;
        private DateTime? expirationDate;
        private float measurementLevel;
        private int isPlb;
        private DateTime? timestamp;

        [PrimaryKey(PrimaryKeyType.Identity, "Tank_History_Id")]
        public long TankHistoryId { get { return tankHistoryId; } set { tankHistoryId = value; } }
        [Property("Tank_Id")]
        public long TankId { get { return tankId; } set { tankId = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }
        [Property("Product_Id")]
        public long ProductId { get { return productId; } set { productId = value; } }
        [Property("Company_Id")]
        public long CompanyId { get { return companyId; } set { companyId = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Start_Date")]
        public DateTime? StartDate { get { return startDate; } set { startDate = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Expiration_Date")]
        public DateTime? ExpirationDate { get { return expirationDate; } set { expirationDate = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Measurement_Level")]
        public float MeasurementLevel { get { return measurementLevel; } set { measurementLevel = value; } }
        [Property("Is_PLB")]
        public int IsPlb { get { return isPlb; } set { isPlb = value; } }
        [Property("Timestamp")]
        public DateTime? Timestamp { get { return timestamp; } set { timestamp = value; } }

        public TankHistory()
        {
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(TankHistory), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(TankHistory));
            DetachedCriteria countCriteria = DetachedCriteria.For<TankHistory>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<TankHistory>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static TankHistory[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<TankHistory>();
            dc.AddOrder(Order.Asc("Name"));
            return (TankHistory[])FindAll(typeof(TankHistory), dc);
        }

        public static TankHistory FindById(long id)
        {
            return (TankHistory)FindByPrimaryKey(typeof(TankHistory), id);
        }

        public static TankHistory[] FindByTankId(long id)
        {
            DetachedCriteria dc = DetachedCriteria.For<TankHistory>();
            dc.Add(Expression.Eq("TankId", id));
            dc.AddOrder(Order.Asc("Name"));
            return (TankHistory[])FindAll(typeof(TankHistory), dc);
        }

        public bool Validate()
        {
            bool result = true;
            return result;
        }

    }
}
