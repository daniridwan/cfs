﻿using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;
using System.Globalization;
using System.Linq;

namespace CFS.Models.PDF
{
    public class DeliveryOrderPdfRenderer
    {
        private Font fontTitle;
        private Font fontSubTitle;
        private Font fontSubTitleNormal;
        private Font fontSubTitleLink;
        private Font fontFieldLabel;
        private Font fontFieldMediumLabel;
        private Font fontFieldLargeLabel;
        private Font fontFieldValue;
        private Font fontFieldMediumValue;
        private Font fontFieldLargeValue;
        private Font fontTableHeader;
        private Font fontTableValue;
        private Font fontFooter;

        private const string formatDate = "MMM/dd/yyyy";
        private const string formatApprovalDate = "MMM/dd/yyyy HH:mm:ss";

        private string mode = "";
        private string uom = "";
        private bool isLocal = false;
        private bool isImport = false;
        private List<TruckLoadingPlanning> listLocal = new List<TruckLoadingPlanning>();
        private List<TruckLoadingPlanning> listImport = new List<TruckLoadingPlanning>();

        public DeliveryOrderPdfRenderer()
        {
            fontTitle = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
            fontSubTitle = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
            fontSubTitleNormal = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
            fontSubTitleLink = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
            fontFieldLabel = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
            fontFieldMediumLabel = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
            fontFieldLargeLabel = new Font(Font.FontFamily.HELVETICA, 16, Font.NORMAL);
            fontFieldValue = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
            fontFieldMediumValue = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
            fontFieldLargeValue = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
            fontTableHeader = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
            fontTableValue = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
            fontFooter = new Font(Font.FontFamily.HELVETICA, 4, Font.NORMAL);
        }

        public byte[] Render(CFS.Models.TruckLoading data)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.A5);
            doc.SetMargins(15, 8, 15, 15);
            using (MemoryStream ms = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                //writer.PageEvent = new PDFFooter();
                doc.Open();
                doc.AddTitle(string.Format("Delivery Order # {0}", data.TruckRegistrationPlate));
                doc.AddCreator("CFS");

                //get mode & uom
                if (data.TruckLoadingPlanning[0].FillingPointGroup == "Chemical") //get first item to determine chemical / fuel
                {
                    mode = "Timbang";
                    uom = "Kg";
                }
                else
                {
                    mode = "Flow Meter";
                    uom = "KL";
                }

                List<TruckLoadingPlanning> tlpConsignee = new List<TruckLoadingPlanning>();
                List<string> listConsignee = new List<string>();
                foreach (TruckLoadingPlanning tlp in data.TruckLoadingPlanning)
                {
                    //list addresses
                    DeliveryOrderDetail dod = DeliveryOrderDetail.FindByDeliveryOrderNumber(tlp.DeliveryOrderNumber);
                    if(!listConsignee.Contains(dod.ShipToAddress))
                    {
                        listConsignee.Add(dod.ShipToAddress);
                    }
                }

                DeliveryOrder deliveryOrder = DeliveryOrder.FindByShipmentNumber(data.DeliveryOrderNumber);
                for (int i = 0;i < listConsignee.Count; i++)
                {
                    isImport = false;
                    isLocal = false;
                    listLocal.Clear();
                    listImport.Clear();
                    string consignee = "";
                    if (listConsignee[i] != null)
                    {
                        consignee = listConsignee[i].ToString();
                    }
                    DeliveryOrderDetail[] dod = DeliveryOrderDetail.FindByDeliveryOrderIdAndConsignee(deliveryOrder.DeliveryOrderId, consignee);
                    // 2000 -1
                    foreach (DeliveryOrderDetail d in dod)
                    {
                        TruckLoadingPlanning tlp = TruckLoadingPlanning.FindByDeliveryOrderNumber(d.DeliveryOrderNumber);
                        VesselDocument vd = VesselDocument.FindById(tlp.VesselDocumentId);
                        //list local / import
                        if (vd.Type == "Local")
                        {
                            isLocal = true;
                            listLocal.Add(tlp);
                        }
                        else
                        {
                            isImport = true;
                            listImport.Add(tlp);
                        }
                    }

                    if (isLocal) //render local terlebih dahulu
                    {
                        string currentMode = "LOKAL";
                        //border document
                        PdfContentByte content = writer.DirectContent;
                        content.Stroke();

                        PdfPTable headerTitle = new PdfPTable(3); // 3 kolom
                        headerTitle.WidthPercentage = 100;
                        headerTitle.SetWidths(new float[] { 0.3f, 1f, 1f });

                        PdfPTable tableContent = new PdfPTable(9);
                        tableContent.WidthPercentage = 100;
                        tableContent.SpacingBefore = 3;
                        tableContent.SpacingAfter = 3;
                        tableContent.SetWidths(new float[] { 0.47f, 0.1f, 0.7f, 0.4f, 0.1f, 0.7f, 0.4f, 0.1f, 1f });

                        PdfPTable tableCompartment = new PdfPTable(6);
                        tableCompartment.SetWidths(new float[] { 0.45f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f });

                        PdfPTable tableSignature = new PdfPTable(1);

                        PdfPTable tableNotes = new PdfPTable(1);
                        tableNotes.WidthPercentage = 100;
                        tableNotes.DefaultCell.Border = Rectangle.NO_BORDER;

                        PdfPTable outerTable = new PdfPTable(2);
                        outerTable.WidthPercentage = 100;
                        outerTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        outerTable.SetWidths(new float[] { 0.87f, 0.13f });
                        outerTable.AddCell(tableCompartment);
                        outerTable.AddCell(tableSignature);

                        RenderHeader(headerTitle, currentMode);
                        RenderContent(tableContent, data, content, currentMode, listLocal, consignee);
                        RenderCompartment(tableCompartment, data, listLocal);
                        RenderSignature(tableSignature);
                        RenderNotes(tableNotes);

                        doc.Add(headerTitle);
                        doc.Add(tableContent);
                        doc.Add(outerTable);
                        doc.Add(tableNotes);
                        doc.NewPage();
                    }

                    if (isImport) //render local terlebih dahulu
                    {
                        string currentMode = "IMPORT";
                        //border document
                        PdfContentByte content = writer.DirectContent;
                        content.Stroke();

                        PdfPTable headerTitle = new PdfPTable(3); // 3 kolom
                        headerTitle.WidthPercentage = 100;
                        headerTitle.SetWidths(new float[] { 0.3f, 1f, 1f });

                        PdfPTable tableContent = new PdfPTable(9);
                        tableContent.WidthPercentage = 100;
                        tableContent.SpacingBefore = 3;
                        tableContent.SpacingAfter = 3;
                        tableContent.SetWidths(new float[] { 0.47f, 0.1f, 0.7f, 0.4f, 0.1f, 0.7f, 0.4f, 0.1f, 1f });

                        PdfPTable tableCompartment = new PdfPTable(6);
                        tableCompartment.SetWidths(new float[] { 0.45f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f });

                        PdfPTable tableSignature = new PdfPTable(1);

                        PdfPTable tableNotes = new PdfPTable(1);
                        tableNotes.WidthPercentage = 100;
                        tableNotes.DefaultCell.Border = Rectangle.NO_BORDER;

                        PdfPTable outerTable = new PdfPTable(2);
                        outerTable.WidthPercentage = 100;
                        outerTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        outerTable.SetWidths(new float[] { 0.87f, 0.13f });
                        outerTable.AddCell(tableCompartment);
                        outerTable.AddCell(tableSignature);

                        RenderHeader(headerTitle, currentMode);
                        RenderContent(tableContent, data, content, currentMode, listImport, consignee);
                        RenderCompartment(tableCompartment, data, listImport);
                        RenderSignature(tableSignature);
                        RenderNotes(tableNotes);

                        doc.Add(headerTitle);
                        doc.Add(tableContent);
                        doc.Add(outerTable);
                        doc.Add(tableNotes);
                        doc.NewPage();
                    }
                }

                doc.Close();
                return ms.ToArray();
            }
        }

        private void RenderHeader(PdfPTable headerTitle, string currentMode)
        {
            //baris 1 & 2
            PdfPCell imgCell = new PdfPCell();
            imgCell.Rowspan = 4;
            imgCell.Colspan = 1;
            imgCell.PaddingTop = 5;
            imgCell.PaddingLeft = 3;
            imgCell.PaddingRight = 3;
            imgCell.Border = Rectangle.NO_BORDER;
            imgCell.BorderWidth = 0;
            Image logo = Image.GetInstance(PrepareLogo(), BaseColor.WHITE);
            logo.ScalePercent(45);
            imgCell.AddElement(logo);
            headerTitle.AddCell(imgCell);

            PdfPCell ContentCompany = new PdfPCell();
            ContentCompany.Border = Rectangle.NO_BORDER;
            ContentCompany.Colspan = 2;
            ContentCompany.PaddingTop = 5;
            ContentCompany.HorizontalAlignment = Element.ALIGN_LEFT;
            //header
            Paragraph tCompany = new Paragraph("PT. Redeco Petrolin Utama", fontSubTitle);
            tCompany.Leading = 2;
            tCompany.SpacingBefore = 0;
            tCompany.SpacingAfter = 0;
            tCompany.Alignment = Element.ALIGN_LEFT;
            ContentCompany.AddElement(tCompany);
            headerTitle.AddCell(ContentCompany);

            PdfPCell ContentSubHeader = new PdfPCell();
            ContentSubHeader.Border = Rectangle.NO_BORDER;
            ContentSubHeader.Colspan = 2;
            ContentSubHeader.PaddingTop = 0;
            ContentSubHeader.HorizontalAlignment = Element.ALIGN_LEFT;
            //subheader
            Paragraph tSubheader = new Paragraph("Terminal : Desa Mangunreja, Merak, Banten, Indonesia Phone: +62 254 5750074, Fax: +62 254 5750075", fontSubTitleNormal);
            tSubheader.SpacingBefore = 0;
            tSubheader.SpacingAfter = 0;
            tSubheader.Alignment = Element.ALIGN_LEFT;
            ContentSubHeader.AddElement(tSubheader);
            headerTitle.AddCell(ContentSubHeader);

            PdfPCell ContentSubHeader1 = new PdfPCell();
            ContentSubHeader1.Border = Rectangle.NO_BORDER;
            ContentSubHeader1.Colspan = 2;
            ContentSubHeader1.PaddingTop = 0;
            ContentSubHeader1.HorizontalAlignment = Element.ALIGN_LEFT;
            //subheader
            Paragraph tSubheader1 = new Paragraph("Kantor : Cyber 2 Tower, 30th floor, Jl. HR Rasuna Said Blok X5/13, Jakarta 12950, Indonesia, Phone: +62 21 252 0325, www.redeco-rpu.com", fontSubTitleNormal);
            tSubheader1.SpacingBefore = 0;
            tSubheader1.SpacingAfter = 0;
            tSubheader1.Alignment = Element.ALIGN_LEFT;
            ContentSubHeader1.AddElement(tSubheader1);
            headerTitle.AddCell(ContentSubHeader1);

            PdfPCell ContentTitle = new PdfPCell();
            ContentTitle.Border = Rectangle.NO_BORDER;
            ContentTitle.Colspan = 3;
            ContentTitle.PaddingTop = 5;
            ContentTitle.HorizontalAlignment = Element.ALIGN_MIDDLE;
            //header
            Paragraph tHeader = new Paragraph(string.Format("DELIVERY ORDER (D.O.) {0}", currentMode), fontTitle);
            tHeader.SpacingBefore = 0;
            tHeader.SpacingAfter = 0;
            tHeader.Alignment = Element.ALIGN_CENTER;
            ContentTitle.AddElement(tHeader);
            headerTitle.AddCell(ContentTitle);

            PdfPCell ContentSecondaryTitle = new PdfPCell();
            ContentSecondaryTitle.Border = Rectangle.NO_BORDER;
            ContentSecondaryTitle.Colspan = 3;
            ContentSecondaryTitle.PaddingTop = 0;
            ContentSecondaryTitle.VerticalAlignment = Element.ALIGN_MIDDLE;
            //header
            Paragraph tSecondaryTitle = new Paragraph("Surat Jalan", fontSubTitle);
            tSecondaryTitle.SpacingBefore = 0;
            tSecondaryTitle.SpacingAfter = 5;
            tSecondaryTitle.Alignment = Element.ALIGN_CENTER;
            ContentSecondaryTitle.AddElement(tSecondaryTitle);
            headerTitle.AddCell(ContentSecondaryTitle);
        }

        private void RenderContent(PdfPTable tableContent, TruckLoading data, PdfContentByte cb, string currentMode, List<TruckLoadingPlanning> listPlanning, string recipient)
        {
            //baris 1
            PdfPCell ContentRefCustomer = new PdfPCell();
            ContentRefCustomer.Border = Rectangle.NO_BORDER;
            ContentRefCustomer.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentRefCustomer.FixedHeight = 15f;
            Paragraph tRefCustomer = new Paragraph("Ref. Pelanggan", fontFieldMediumLabel);
            tRefCustomer.Alignment = Element.ALIGN_LEFT;
            ContentRefCustomer.AddElement(tRefCustomer);
            tableContent.AddCell(ContentRefCustomer);

            //:
            PdfPCell ContentSeparator = new PdfPCell();
            ContentSeparator.Border = Rectangle.NO_BORDER;
            ContentSeparator.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentSeparator.HorizontalAlignment = Element.ALIGN_CENTER;
            Paragraph tLabelSeparator = new Paragraph(":", fontFieldLabel);
            tLabelSeparator.Alignment = Element.ALIGN_LEFT;
            ContentSeparator.AddElement(tLabelSeparator);
            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentRefCustomer1 = new PdfPCell();
            ContentRefCustomer1.Border = Rectangle.NO_BORDER;
            ContentRefCustomer1.VerticalAlignment = Element.ALIGN_MIDDLE;
            List<string> listCustRef = new List<string>();
            List<string> listPibNo = new List<string>();
            List<string> listPibDate = new List<string>();
            foreach (TruckLoadingPlanning plan in listPlanning)
            {
                DeliveryOrderDetail detail = DeliveryOrderDetail.FindByDeliveryOrderNumber(plan.DeliveryOrderNumber);
                VesselDocument vesselDoc = VesselDocument.FindById(plan.VesselDocumentId);
                listCustRef.Add(detail.CustomerReferenceNumber);
                listPibNo.Add(vesselDoc.PibNo);
                string pibDate = vesselDoc.PibDate.HasValue ? vesselDoc.PibDate.Value.ToString("dd/MM/yyyy") : "";
                listPibDate.Add(pibDate.ToString());
            }
            Paragraph tRefCustomer1 = new Paragraph(string.Join(",", listCustRef), fontFieldMediumValue);
            tRefCustomer1.Alignment = Element.ALIGN_LEFT;
            ContentRefCustomer1.AddElement(tRefCustomer1);
            tableContent.AddCell(ContentRefCustomer1);

            PdfPCell ContentRefDate = new PdfPCell();
            ContentRefDate.Border = Rectangle.NO_BORDER;
            ContentRefDate.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tRefDate = new Paragraph("Tgl", fontFieldMediumLabel);
            tRefDate.Alignment = Element.ALIGN_CENTER;
            ContentRefDate.AddElement(tRefDate);
            tableContent.AddCell(ContentRefDate);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentRefDate1 = new PdfPCell();
            ContentRefDate1.Border = Rectangle.NO_BORDER;
            ContentRefDate1.VerticalAlignment = Element.ALIGN_MIDDLE;
            DeliveryOrder dO = DeliveryOrder.FindByShipmentNumber(data.DeliveryOrderNumber);
            string deliveryDate = dO.DeliveryDate.HasValue ? dO.DeliveryDate.Value.ToString("dd/MM/yyyy") : "";
            Paragraph tRefDate1 = new Paragraph(deliveryDate, fontFieldMediumValue);
            tRefDate1.Alignment = Element.ALIGN_LEFT;
            ContentRefDate1.AddElement(tRefDate1);
            tableContent.AddCell(ContentRefDate1);

            PdfPCell ContentSerieNo = new PdfPCell();
            ContentSerieNo.Border = Rectangle.NO_BORDER;
            ContentSerieNo.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tSerieNo = new Paragraph("No Serie", fontFieldMediumLabel);
            tSerieNo.Alignment = Element.ALIGN_CENTER;
            ContentSerieNo.AddElement(tSerieNo);
            tableContent.AddCell(ContentSerieNo);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentSerieNo1 = new PdfPCell();
            ContentSerieNo1.Border = Rectangle.NO_BORDER;
            ContentSerieNo1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tSerieNo1 = new Paragraph(data.DeliveryOrderNumber, fontFieldMediumValue);
            tSerieNo1.Alignment = Element.ALIGN_LEFT;
            ContentSerieNo1.AddElement(tSerieNo1);
            tableContent.AddCell(ContentSerieNo1);

            //baris 2
            PdfPCell ContentPib = new PdfPCell();
            ContentPib.Border = Rectangle.NO_BORDER;
            ContentPib.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentPib.FixedHeight = 15f;
            Paragraph tPib = new Paragraph("No. PIB", fontFieldMediumLabel);
            tPib.Alignment = Element.ALIGN_LEFT;
            ContentPib.AddElement(tPib);
            tableContent.AddCell(ContentPib);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentPib1 = new PdfPCell();
            ContentPib1.Border = Rectangle.NO_BORDER;
            ContentPib1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tPib1 = new Paragraph("");
            if (currentMode == "LOKAL") {
                tPib1 = new Paragraph("", fontFieldMediumValue);
            }
            else
            {
                tPib1 = new Paragraph(string.Join(",", listPibNo.Distinct()), fontFieldMediumValue);
            }
            tPib.Alignment = Element.ALIGN_LEFT;
            ContentPib1.AddElement(tPib1);
            tableContent.AddCell(ContentPib1);

            PdfPCell ContentPibDate = new PdfPCell();
            ContentPibDate.Border = Rectangle.NO_BORDER;
            ContentPibDate.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tPibDate = new Paragraph("Tgl", fontFieldMediumLabel);
            tPibDate.Alignment = Element.ALIGN_CENTER;
            ContentPibDate.AddElement(tPibDate);
            tableContent.AddCell(ContentPibDate);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentPibDate1 = new PdfPCell();
            ContentPibDate1.Border = Rectangle.NO_BORDER;
            ContentPibDate1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tPibDate1 = new Paragraph("");
            if (currentMode == "LOKAL")
            {
                tPibDate1 = new Paragraph("", fontFieldMediumValue);
            }
            else
            {
                tPibDate1 = new Paragraph(string.Join(",", listPibDate), fontFieldMediumValue);
            }
            tPibDate1.Alignment = Element.ALIGN_LEFT;
            ContentPibDate1.AddElement(tPibDate1);
            tableContent.AddCell(ContentPibDate1);

            PdfPCell ContentKapal = new PdfPCell();
            ContentKapal.Border = Rectangle.NO_BORDER;
            ContentKapal.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tKapal = new Paragraph("Ex Kapal", fontFieldMediumLabel);
            tKapal.Alignment = Element.ALIGN_CENTER;
            ContentKapal.AddElement(tKapal);
            tableContent.AddCell(ContentKapal);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentKapal1 = new PdfPCell();
            ContentKapal1.Border = Rectangle.NO_BORDER;
            ContentKapal1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tKapal1 = new Paragraph(data.VesselNameString, fontFieldMediumValue);
            tKapal1.Alignment = Element.ALIGN_LEFT;
            ContentKapal1.AddElement(tKapal1);
            tableContent.AddCell(ContentKapal1);

            //baris 3
            PdfPCell ContentQueue = new PdfPCell();
            ContentQueue.Border = Rectangle.NO_BORDER;
            ContentQueue.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentQueue.FixedHeight = 15f;
            Paragraph tQueue = new Paragraph("No. Antrian", fontFieldMediumLabel);
            tQueue.Alignment = Element.ALIGN_LEFT;
            ContentQueue.AddElement(tQueue);
            tableContent.AddCell(ContentQueue);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentQueue1 = new PdfPCell();
            ContentQueue1.Border = Rectangle.NO_BORDER;
            ContentQueue1.VerticalAlignment = Element.ALIGN_MIDDLE;
            ViewQueueTruck[] queue = ViewQueueTruck.FindByTruckLoadingId(data.TruckLoadingId);
            Paragraph tQueue1 = new Paragraph(queue[0].QueueNumber, fontFieldMediumValue);
            tQueue1.Alignment = Element.ALIGN_LEFT;
            ContentQueue1.AddElement(tQueue1);
            tableContent.AddCell(ContentQueue1);

            PdfPCell ContentLane = new PdfPCell();
            ContentLane.Border = Rectangle.NO_BORDER;
            ContentLane.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tLane = new Paragraph("No. Lajur", fontFieldMediumLabel);
            tLane.Alignment = Element.ALIGN_LEFT;
            ContentLane.AddElement(tLane);
            tableContent.AddCell(ContentLane);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentLane1 = new PdfPCell();
            ContentLane1.Border = Rectangle.NO_BORDER;
            ContentLane1.VerticalAlignment = Element.ALIGN_MIDDLE;
            string[] fp = listPlanning.Select(x => x.FillingPointName).ToArray();
            Paragraph tLane1 = new Paragraph(string.Join(",", fp.Distinct()), fontFieldMediumValue);
            tLane1.Alignment = Element.ALIGN_LEFT;
            ContentLane1.AddElement(tLane1);
            tableContent.AddCell(ContentLane1);

            //barcode
            Barcode128 code128 = new Barcode128();
            code128.CodeType = Barcode.CODE128;
            code128.Code = data.DeliveryOrderNumber.ToString();
            //code128.Font = null; //without text
            iTextSharp.text.Image image128 = code128.CreateImageWithBarcode(cb, null, null);
            image128.Border = Rectangle.NO_BORDER;
            image128.WidthPercentage = 100;
            image128.ScaleAbsolute(110f, 110f);
            image128.Alignment = Element.ALIGN_CENTER;
            PdfPCell barcodeCell = new PdfPCell();
            barcodeCell.Rowspan = 3;
            barcodeCell.Colspan = 3;
            barcodeCell.Border = Rectangle.NO_BORDER;
            barcodeCell.BorderWidth = 1;
            barcodeCell.AddElement(image128);
            tableContent.AddCell(barcodeCell);

            PdfPCell ContentRegPlate = new PdfPCell();
            ContentRegPlate.Border = Rectangle.NO_BORDER;
            ContentRegPlate.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentRegPlate.FixedHeight = 15f;
            Paragraph tRegPlate = new Paragraph("No. Polisi", fontFieldMediumLabel);
            tRegPlate.Alignment = Element.ALIGN_LEFT;
            ContentRegPlate.AddElement(tRegPlate);
            tableContent.AddCell(ContentRegPlate);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentRegPlate1 = new PdfPCell();
            ContentRegPlate1.Border = Rectangle.NO_BORDER;
            ContentRegPlate1.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentRegPlate1.Colspan = 4;
            Paragraph tRegPlate1 = new Paragraph(data.TruckRegistrationPlate, fontFieldMediumValue);
            tRegPlate1.Alignment = Element.ALIGN_LEFT;
            ContentRegPlate1.AddElement(tRegPlate1);
            tableContent.AddCell(ContentRegPlate1);

            PdfPCell ContentDriver = new PdfPCell();
            ContentDriver.Border = Rectangle.NO_BORDER;
            ContentDriver.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentDriver.FixedHeight = 15f;
            Paragraph tDriver = new Paragraph("Pengemudi", fontFieldMediumLabel);
            tDriver.Alignment = Element.ALIGN_LEFT;
            ContentDriver.AddElement(tDriver);
            tableContent.AddCell(ContentDriver);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentDriver1 = new PdfPCell();
            ContentDriver1.Border = Rectangle.NO_BORDER;
            ContentDriver1.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentDriver1.Colspan = 4;
            Driver d = Driver.FindById(data.DriverId);
            Paragraph tDriver1 = new Paragraph(d.Name, fontFieldMediumValue);
            tDriver1.Alignment = Element.ALIGN_LEFT;
            ContentDriver1.AddElement(tDriver1);
            tableContent.AddCell(ContentDriver1);

            PdfPCell ContentTransporter = new PdfPCell();
            ContentTransporter.Border = Rectangle.NO_BORDER;
            ContentTransporter.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentTransporter.FixedHeight = 15f;
            Paragraph tTransporter = new Paragraph("Transporter", fontFieldMediumLabel);
            tTransporter.Alignment = Element.ALIGN_LEFT;
            ContentTransporter.AddElement(tTransporter);
            tableContent.AddCell(ContentTransporter);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentTransporter1 = new PdfPCell();
            ContentTransporter1.Border = Rectangle.NO_BORDER;
            ContentTransporter1.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentTransporter1.Colspan = 7;
            Truck t = Truck.FindByRegistrationPlate(data.TruckRegistrationPlate);
            Paragraph tTransporter1 = new Paragraph(t.TruckTransporter, fontFieldMediumValue);
            tTransporter1.Alignment = Element.ALIGN_LEFT;
            ContentTransporter1.AddElement(tTransporter1);
            tableContent.AddCell(ContentTransporter1);

            PdfPCell ContentCustomer = new PdfPCell();
            ContentCustomer.Border = Rectangle.NO_BORDER;
            ContentCustomer.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentCustomer.FixedHeight = 15f;
            Paragraph tCustomer = new Paragraph("Pelanggan", fontFieldMediumLabel);
            tCustomer.Alignment = Element.ALIGN_LEFT;
            ContentCustomer.AddElement(tCustomer);
            tableContent.AddCell(ContentCustomer);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentCustomer1 = new PdfPCell();
            ContentCustomer1.Border = Rectangle.NO_BORDER;
            ContentCustomer1.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentCustomer1.Colspan = 7;
            Paragraph tCustomer1 = new Paragraph(dO.CompanyName, fontFieldMediumValue);
            tCustomer1.Alignment = Element.ALIGN_LEFT;
            ContentCustomer1.AddElement(tCustomer1);
            tableContent.AddCell(ContentCustomer1);

            PdfPCell ContentRecipient = new PdfPCell();
            ContentRecipient.Border = Rectangle.NO_BORDER;
            ContentRecipient.FixedHeight = 15f;
            ContentRecipient.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tRecipient = new Paragraph("Penerima", fontFieldMediumLabel);
            tRecipient.Alignment = Element.ALIGN_LEFT;
            ContentRecipient.AddElement(tRecipient);
            tableContent.AddCell(ContentRecipient);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentRecipient1 = new PdfPCell();
            ContentRecipient1.Border = Rectangle.NO_BORDER;
            ContentRecipient1.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentRecipient1.Colspan = 7;
            Paragraph tRecipient1 = new Paragraph(recipient, fontFieldMediumValue);
            tRecipient1.Alignment = Element.ALIGN_LEFT;
            ContentRecipient1.AddElement(tRecipient1);
            tableContent.AddCell(ContentRecipient1);

            PdfPCell ContentSeal = new PdfPCell();
            ContentSeal.Border = Rectangle.NO_BORDER;
            ContentSeal.FixedHeight = 15f;
            ContentSeal.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tSeal = new Paragraph("Segel", fontFieldMediumLabel);
            tSeal.Alignment = Element.ALIGN_LEFT;
            ContentSeal.AddElement(tSeal);
            tableContent.AddCell(ContentSeal);

            tableContent.AddCell(ContentSeparator);

            PdfPCell ContentSeal1 = new PdfPCell();
            ContentSeal1.Border = Rectangle.NO_BORDER;
            ContentSeal1.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentSeal1.Colspan = 7;
            Paragraph tSeal1 = new Paragraph(data.SealString, fontFieldMediumValue);
            tSeal1.Alignment = Element.ALIGN_LEFT;
            ContentSeal1.AddElement(tSeal1);
            tableContent.AddCell(ContentSeal1);
        }

        private void RenderCompartment(PdfPTable tableCompartment, TruckLoading data, List<TruckLoadingPlanning> listPlanning)
        {
            PdfPCell ContentCompartment = new PdfPCell();
            ContentCompartment.Border = Rectangle.BOX;
            ContentCompartment.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentCompartment.HorizontalAlignment = Element.ALIGN_CENTER;
            ContentCompartment.FixedHeight = 20f;
            Paragraph tCompartment = new Paragraph("Komp.", fontFieldMediumLabel);
            tCompartment.Alignment = Element.ALIGN_CENTER;
            ContentCompartment.AddElement(tCompartment);
            tableCompartment.AddCell(ContentCompartment);

            PdfPCell ContentMaterial = new PdfPCell();
            ContentMaterial.Border = Rectangle.BOX;
            ContentMaterial.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentMaterial.HorizontalAlignment = Element.ALIGN_CENTER;
            ContentMaterial.Colspan = 2;
            Paragraph tMaterial = new Paragraph("Nama Barang", fontFieldMediumLabel);
            tMaterial.Alignment = Element.ALIGN_CENTER;
            ContentMaterial.AddElement(tMaterial);
            tableCompartment.AddCell(ContentMaterial);

            PdfPCell ContentInitialWeight = new PdfPCell();
            ContentInitialWeight.Border = Rectangle.BOX;
            ContentInitialWeight.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentInitialWeight.HorizontalAlignment = Element.ALIGN_CENTER;
            ContentInitialWeight.Colspan = 1;
            Paragraph tInitialWeight = new Paragraph("Berat Kosong", fontFieldMediumLabel);
            tInitialWeight.Alignment = Element.ALIGN_CENTER;
            ContentInitialWeight.AddElement(tInitialWeight);
            tableCompartment.AddCell(ContentInitialWeight);

            PdfPCell ContentFinalWeight = new PdfPCell();
            ContentFinalWeight.Border = Rectangle.BOX;
            ContentFinalWeight.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentFinalWeight.HorizontalAlignment = Element.ALIGN_CENTER;
            ContentFinalWeight.Colspan = 1;
            Paragraph tFinalWeight = new Paragraph("Berat Isi", fontFieldMediumLabel);
            tFinalWeight.Alignment = Element.ALIGN_CENTER;
            ContentFinalWeight.AddElement(tFinalWeight);
            tableCompartment.AddCell(ContentFinalWeight);

            PdfPCell ContentQuantity = new PdfPCell();
            ContentQuantity.Border = Rectangle.BOX;
            ContentQuantity.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentQuantity.HorizontalAlignment = Element.ALIGN_CENTER;
            Paragraph tQuantity = new Paragraph();
            if(mode == "Timbang")
            {
                tQuantity = new Paragraph(string.Format("Kuantitas ({0})", uom), fontFieldMediumLabel);
            }
            else
            {
                tQuantity = new Paragraph(string.Format("Kuantitas ({0})", uom), fontFieldMediumLabel);
            }
            tQuantity.Alignment = Element.ALIGN_CENTER;
            ContentQuantity.AddElement(tQuantity);
            tableCompartment.AddCell(ContentQuantity);

            //foreach (TruckLoadingPlanning plan in data.TruckLoadingPlanning)
            for(int i = 1; i<=5; i++)
            {
                PdfPCell ContentComp1 = new PdfPCell();
                ContentComp1.Border = Rectangle.BOX;
                ContentComp1.FixedHeight = 20f;
                ContentComp1.VerticalAlignment = Element.ALIGN_MIDDLE;
                ContentComp1.HorizontalAlignment = Element.ALIGN_CENTER;
                Paragraph tLabelComp1 = new Paragraph(i.ToString(), fontFieldMediumLabel);
                tLabelComp1.Alignment = Element.ALIGN_CENTER;
                ContentComp1.AddElement(tLabelComp1);
                tableCompartment.AddCell(ContentComp1);

                PdfPCell ContentMaterial1 = new PdfPCell();
                ContentMaterial1.Border = Rectangle.BOX;
                ContentMaterial1.Colspan = 2;
                ContentMaterial1.VerticalAlignment = Element.ALIGN_MIDDLE;
                ContentMaterial1.HorizontalAlignment = Element.ALIGN_CENTER;
                Paragraph tLabelMaterial1 = new Paragraph();
                if (listPlanning.Count >= i)
                {
                    tLabelMaterial1 = new Paragraph(string.Format("{0} ({1})",listPlanning[i-1].ProductName, listPlanning[i-1].TankName), fontFieldMediumLabel);
                }
                else
                {
                    tLabelMaterial1 = new Paragraph("");
                }
                tLabelMaterial1.Alignment = Element.ALIGN_LEFT;
                ContentMaterial1.AddElement(tLabelMaterial1);
                tableCompartment.AddCell(ContentMaterial1);

                PdfPCell ContentInitialWeight1 = new PdfPCell();
                ContentInitialWeight1.Border = Rectangle.BOX;
                ContentInitialWeight1.VerticalAlignment = Element.ALIGN_MIDDLE;
                ContentInitialWeight1.HorizontalAlignment = Element.ALIGN_CENTER;
                Paragraph tLabelInitialWeight1 = new Paragraph();
                if (listPlanning.Count >= i)
                {
                    if (mode == "Timbang")
                    {
                        tLabelInitialWeight1 = new Paragraph(string.Format("{0:F0}",listPlanning[i - 1].InitialWeight), fontFieldLargeValue);
                    }
                    else
                    {
                        tLabelInitialWeight1 = new Paragraph("", fontFieldLargeValue);
                    }
                }
                else
                {
                    tLabelInitialWeight1 = new Paragraph("", fontFieldLargeValue);
                }
                tLabelInitialWeight1.Alignment = Element.ALIGN_RIGHT;
                ContentInitialWeight1.AddElement(tLabelInitialWeight1);
                tableCompartment.AddCell(ContentInitialWeight1);

                PdfPCell ContentFinalWeight1 = new PdfPCell();
                ContentFinalWeight1.Border = Rectangle.BOX;
                ContentFinalWeight1.VerticalAlignment = Element.ALIGN_MIDDLE;
                ContentFinalWeight1.HorizontalAlignment = Element.ALIGN_CENTER;
                Paragraph tLabelFinalWeight1 = new Paragraph();
                if (listPlanning.Count >= i)
                {
                    if (mode == "Timbang")
                    {
                        tLabelFinalWeight1 = new Paragraph(string.Format("{0:F0}", listPlanning[i - 1].FinalWeight), fontFieldLargeValue);
                    }
                    else
                    {
                        tLabelFinalWeight1 = new Paragraph("", fontFieldLargeValue);
                    }
                }
                else
                {
                    tLabelFinalWeight1 = new Paragraph("", fontFieldLargeValue);
                }
                tLabelFinalWeight1.Alignment = Element.ALIGN_RIGHT;
                ContentFinalWeight1.AddElement(tLabelFinalWeight1);
                tableCompartment.AddCell(ContentFinalWeight1);

                PdfPCell ContentQuantity1 = new PdfPCell();
                ContentQuantity1.Border = Rectangle.BOX;
                ContentQuantity1.VerticalAlignment = Element.ALIGN_MIDDLE;
                ContentQuantity1.HorizontalAlignment = Element.ALIGN_CENTER;
                Paragraph tLabelQuantity1 = new Paragraph();
                if (listPlanning.Count >= i)
                {
                    if(mode == "Timbang")
                    { 
                        tLabelQuantity1 = new Paragraph(string.Format("{0:F0}", listPlanning[i-1].FinalWeight - listPlanning[i-1].InitialWeight), fontFieldLargeValue);
                    }
                    else
                    {
                        tLabelQuantity1 = new Paragraph(listPlanning[i - 1].Actual.ToString("0.###"), fontFieldLargeValue);
                    }
                }
                else
                {
                    tLabelQuantity1 = new Paragraph("", fontFieldLargeValue);
                }
                tLabelQuantity1.Alignment = Element.ALIGN_RIGHT;
                ContentQuantity1.AddElement(tLabelQuantity1);
                tableCompartment.AddCell(ContentQuantity1);
            }

            PdfPCell ContentTotal = new PdfPCell();
            ContentTotal.Border = Rectangle.BOX;
            ContentTotal.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentTotal.HorizontalAlignment = Element.ALIGN_CENTER;
            ContentTotal.FixedHeight = 20f;
            ContentTotal.Colspan = 5;
            Paragraph tTotal = new Paragraph("Total Kuantitas", fontFieldMediumLabel);
            tTotal.Alignment = Element.ALIGN_RIGHT;
            ContentTotal.AddElement(tTotal);
            tableCompartment.AddCell(ContentTotal);

            double total = 0;
            foreach(TruckLoadingPlanning plan in listPlanning)
            {
                if(mode == "Timbang")
                {
                    total = total + (plan.FinalWeight - plan.InitialWeight);
                }
                else
                {
                    total = total + plan.Actual;
                }
            }
            PdfPCell ContentTotalValue = new PdfPCell();
            ContentTotalValue.Border = Rectangle.BOX;
            ContentTotalValue.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentTotalValue.HorizontalAlignment = Element.ALIGN_CENTER;
            Paragraph tTotalValue = new Paragraph();
            if (mode == "Timbang")
            {
                tTotalValue = new Paragraph(string.Format("{0:0.#}", total), fontFieldLargeValue);
            }
            else
            {
                tTotalValue = new Paragraph(string.Format("{0:0.###}", total), fontFieldLargeValue);
            }
            
            tTotalValue.Alignment = Element.ALIGN_RIGHT;
            ContentTotalValue.AddElement(tTotalValue);
            tableCompartment.AddCell(ContentTotalValue);

            PdfPCell ContentPrintDate = new PdfPCell();
            ContentPrintDate.Border = Rectangle.BOX;
            ContentPrintDate.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentPrintDate.HorizontalAlignment = Element.ALIGN_CENTER;
            ContentPrintDate.FixedHeight = 20f;
            Paragraph tPrintDate = new Paragraph("Tgl. Cetak :", fontFieldMediumLabel);
            tPrintDate.Alignment = Element.ALIGN_LEFT;
            ContentPrintDate.AddElement(tPrintDate);
            tableCompartment.AddCell(ContentPrintDate);

            string gateInDate = data.GateInTimestamp.HasValue ? data.GateInTimestamp.Value.ToString("dd MMMM yyyy") : "";
            string gateInTime = data.GateInTimestamp.HasValue ? data.GateInTimestamp.Value.ToString("HH:mm") : "";
            string loadedTime = data.LoadedTimestamp.HasValue ? data.LoadedTimestamp.Value.ToString("HH:mm") : "";

            PdfPCell ContentPrintDateValue = new PdfPCell();
            ContentPrintDateValue.Border = Rectangle.BOX;
            ContentPrintDateValue.Colspan = 5;
            ContentPrintDateValue.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentPrintDateValue.HorizontalAlignment = Element.ALIGN_CENTER;
            Paragraph tPrintDateValue = new Paragraph(string.Format(gateInDate), fontFieldMediumValue);
            tPrintDateValue.Alignment = Element.ALIGN_CENTER ;
            ContentPrintDateValue.AddElement(tPrintDateValue);
            tableCompartment.AddCell(ContentPrintDateValue);

            PdfPCell ContentGateIn = new PdfPCell();
            ContentGateIn.Border = Rectangle.BOX;
            ContentGateIn.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentGateIn.HorizontalAlignment = Element.ALIGN_CENTER;
            ContentGateIn.FixedHeight = 20f;
            Paragraph tGateIn = new Paragraph("Jam Masuk :", fontFieldMediumLabel);
            tGateIn.Alignment = Element.ALIGN_LEFT;
            ContentGateIn.AddElement(tGateIn);
            tableCompartment.AddCell(ContentGateIn);

            PdfPCell ContentGateInValue = new PdfPCell();
            ContentGateInValue.Border = Rectangle.BOX;
            ContentGateInValue.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentGateInValue.HorizontalAlignment = Element.ALIGN_CENTER;
            Paragraph tGateInValue = new Paragraph(gateInTime, fontFieldMediumValue);
            tGateInValue.Alignment = Element.ALIGN_CENTER;
            ContentGateInValue.AddElement(tGateInValue);
            tableCompartment.AddCell(ContentGateInValue);

            PdfPCell ContentGateOut = new PdfPCell();
            ContentGateOut.Border = Rectangle.BOX;
            ContentGateOut.Colspan = 3;
            ContentGateOut.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentGateOut.HorizontalAlignment = Element.ALIGN_CENTER;
            Paragraph tGateOut = new Paragraph("Jam Keluar :", fontFieldMediumLabel);
            tGateOut.Alignment = Element.ALIGN_RIGHT;
            ContentGateOut.AddElement(tGateOut);
            tableCompartment.AddCell(ContentGateOut);

            PdfPCell ContentGateOutValue = new PdfPCell();
            ContentGateOutValue.Border = Rectangle.BOX;
            ContentGateOutValue.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentGateOutValue.HorizontalAlignment = Element.ALIGN_CENTER;
            Paragraph tGateOutValue = new Paragraph(loadedTime, fontFieldMediumValue);
            tGateOutValue.Alignment = Element.ALIGN_CENTER;
            ContentGateOutValue.AddElement(tGateOutValue);
            tableCompartment.AddCell(ContentGateOutValue);

            PdfPCell ContentNotes = new PdfPCell();
            ContentNotes.Border = Rectangle.BOX;
            ContentNotes.Colspan = 6;
            ContentNotes.VerticalAlignment = Element.ALIGN_TOP;
            ContentNotes.HorizontalAlignment = Element.ALIGN_LEFT;
            Paragraph tNotes = new Paragraph("Catatan : ", fontFieldMediumLabel);
            tNotes.Alignment = Element.ALIGN_LEFT;
            ContentNotes.AddElement(tNotes);
            tableCompartment.AddCell(ContentNotes);
        }

        private void RenderSignature(PdfPTable tableSignature)
        {
            PdfPCell ContentSignOfficer = new PdfPCell();
            ContentSignOfficer.Border = Rectangle.BOX;
            ContentSignOfficer.FixedHeight = 20f;
            ContentSignOfficer.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentSignOfficer.HorizontalAlignment = Element.ALIGN_CENTER;
            Paragraph tSignOfficer = new Paragraph("Petugas", fontFieldMediumLabel);
            tSignOfficer.Alignment = Element.ALIGN_LEFT;
            ContentSignOfficer.AddElement(tSignOfficer);
            tableSignature.AddCell(ContentSignOfficer);

            PdfPCell ContentSignCell = new PdfPCell();
            ContentSignCell.Border = Rectangle.BOX;
            ContentSignCell.FixedHeight = 60f;
            Paragraph tSignCell = new Paragraph("", fontFieldMediumLabel);
            tSignCell.Alignment = Element.ALIGN_LEFT;
            ContentSignCell.AddElement(tSignCell);
            tableSignature.AddCell(ContentSignCell);

            PdfPCell ContentSignBoss = new PdfPCell();
            ContentSignBoss.Border = Rectangle.BOX;
            ContentSignBoss.FixedHeight = 20f;
            ContentSignBoss.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentSignBoss.HorizontalAlignment = Element.ALIGN_CENTER;
            Paragraph tSignBoss = new Paragraph("Atasan", fontFieldMediumLabel);
            tSignBoss.Alignment = Element.ALIGN_LEFT;
            ContentSignBoss.AddElement(tSignBoss);
            tableSignature.AddCell(ContentSignBoss);

            tableSignature.AddCell(ContentSignCell);

            PdfPCell ContentSignDriver = new PdfPCell();
            ContentSignDriver.Border = Rectangle.BOX;
            ContentSignDriver.FixedHeight = 20f;
            ContentSignDriver.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentSignDriver.HorizontalAlignment = Element.ALIGN_CENTER;
            Paragraph tSignDriver = new Paragraph("Pengemudi", fontFieldMediumLabel);
            tSignDriver.Alignment = Element.ALIGN_LEFT;
            ContentSignDriver.AddElement(tSignDriver);
            tableSignature.AddCell(ContentSignDriver);

            tableSignature.AddCell(ContentSignCell);
        }

        private void RenderNotes(PdfPTable tableNotes)
        {
            PdfPCell ContentNotes = new PdfPCell();
            ContentNotes.Border = Rectangle.NO_BORDER;
            ContentNotes.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentNotes.HorizontalAlignment = Element.ALIGN_CENTER;
            Paragraph tNotes = new Paragraph("Putih: Pemilik Barang - Hijau: Transporter - Kuning: Bea Cukai - Merah: Terminal PT. RPU", fontFieldMediumValue);
            tNotes.Alignment = Element.ALIGN_RIGHT;
            ContentNotes.AddElement(tNotes);
            tableNotes.AddCell(ContentNotes);
        }

        private System.Drawing.Image PrepareLogo()
        {
            string logoPath = HttpContext.Current.Server.MapPath("~/Images/RPU Logo.png");
            System.Drawing.Image imgLogo = System.Drawing.Image.FromFile(logoPath);
            return imgLogo;
        }
    }
}
