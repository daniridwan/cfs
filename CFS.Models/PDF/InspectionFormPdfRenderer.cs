﻿using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;
using System.Globalization;

namespace CFS.Models.PDF
{
    public class InspectionFormPdfRenderer
    {
        private Font fontTitle;
        private Font fontSubTitle;
        private Font fontFieldLabel;
        private Font fontFieldMediumLabel;
        private Font fontFieldLargeLabel;
        private Font fontFieldValue;
        private Font fontFieldMediumValue;
        private Font fontFieldLargeValue;
        private Font fontTableHeader;
        private Font fontTableValue;
        private Font fontFooter;

        private const string formatDate = "MMM/dd/yyyy";
        private const string formatApprovalDate = "MMM/dd/yyyy HH:mm:ss";

        public InspectionFormPdfRenderer()
        {
            fontTitle = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
            fontSubTitle = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
            fontFieldLabel = new Font(Font.FontFamily.HELVETICA, 7.5f, Font.NORMAL); //
            fontFieldMediumLabel = new Font(Font.FontFamily.HELVETICA, 7.5f, Font.NORMAL); //
            fontFieldLargeLabel = new Font(Font.FontFamily.HELVETICA, 16, Font.NORMAL);
            fontFieldValue = new Font(Font.FontFamily.HELVETICA, 7.5f, Font.BOLD); //
            fontFieldMediumValue = new Font(Font.FontFamily.HELVETICA, 7.5f, Font.BOLD);
            fontFieldLargeValue = new Font(Font.FontFamily.HELVETICA, 7.5f, Font.BOLD); 
            fontTableHeader = new Font(Font.FontFamily.HELVETICA, 7.5f, Font.BOLD); //
            fontTableValue = new Font(Font.FontFamily.HELVETICA, 7.5f, Font.NORMAL); //
            fontFooter = new Font(Font.FontFamily.HELVETICA, 6.3f, Font.NORMAL);
        }

        public byte[] Render(CFS.Models.TruckLoading data)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.A5);
            //doc.SetMargins(25, 8, 25, 25);
            doc.SetMargins(7, 7, 7, 7);
            using (MemoryStream ms = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                //writer.PageEvent = new PDFFooter();
                doc.Open();
                doc.AddTitle(string.Format("Form Inspeksi # {0}", data.TruckRegistrationPlate));
                doc.AddCreator("CFS");

                //border document
                PdfContentByte content = writer.DirectContent;
                //Rectangle rectangle = new Rectangle(doc.PageSize);
                //rectangle.Left += doc.LeftMargin;
                //rectangle.Right -= doc.RightMargin;
                //rectangle.Top -= doc.TopMargin;
                //rectangle.Bottom += doc.BottomMargin;
                //content.SetColorStroke(BaseColor.BLACK);
                //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                content.Stroke();

                PdfPTable headerTitle = new PdfPTable(3); // 3 kolom
                headerTitle.WidthPercentage = 100;
                headerTitle.SetWidths(new float[] { 0.5f, 1f, 1f });

                PdfPTable tableHeader = new PdfPTable(7); // 3 kolom
                tableHeader.WidthPercentage = 100;
                tableHeader.SetWidths(new float[] { 1f, 1f, 0.7f, 1f, 1f, 0.4f, 1f });

                PdfPTable tableContent = new PdfPTable(5);
                tableContent.WidthPercentage = 100;
                tableContent.SpacingBefore = 0;
                tableContent.SpacingAfter = 0;
                tableContent.SetWidths(new float[] { 0.3f, 3f, 0.7f, 0.9f, 0.9f});

                PdfPTable tableNotes = new PdfPTable(4);
                tableNotes.WidthPercentage = 100;
                tableNotes.SpacingBefore = 0;
                tableNotes.SpacingAfter = 0;
                tableNotes.SetWidths(new float[] { 0.5f, 0.3f, 1.5f, 0.42f });

                PdfPTable tableFooter = new PdfPTable(1);
                tableFooter.WidthPercentage = 100;

                RenderHeader(headerTitle);
                RenderTableHeader(tableHeader, data, content);
                RenderTableContent(tableContent);
                RenderTableNotes(tableNotes);
                RenderTableFooter(tableFooter);
                doc.Add(headerTitle);
                doc.Add(tableHeader);
                doc.Add(tableContent);
                doc.Add(tableNotes);
                doc.Add(tableFooter);

                doc.Close();
                return ms.ToArray();
            }
        }

        private void RenderHeader(PdfPTable headerTitle)
        {
            //baris 1 & 2
            PdfPCell imgCell = new PdfPCell();
            imgCell.Rowspan = 2;
            imgCell.Colspan = 1;
            imgCell.PaddingLeft = 5;
            imgCell.PaddingRight = 5;
            imgCell.Border = Rectangle.NO_BORDER;
            imgCell.BorderWidth = 0;
            Image logo = Image.GetInstance(PrepareLogo(), BaseColor.WHITE);
            logo.ScalePercent(45);
            imgCell.AddElement(logo);
            headerTitle.AddCell(imgCell);

            PdfPCell ContentCompany = new PdfPCell();
            ContentCompany.Border = Rectangle.NO_BORDER;
            ContentCompany.Colspan = 2;
            ContentCompany.PaddingTop = 5;
            ContentCompany.VerticalAlignment = Element.ALIGN_MIDDLE;
            //header
            Paragraph tCompany = new Paragraph("PT. Redeco Petrolin Utama", fontSubTitle);
            tCompany.SpacingBefore = 0;
            tCompany.SpacingAfter = 0;
            tCompany.Alignment = Element.ALIGN_CENTER;
            ContentCompany.AddElement(tCompany);
            headerTitle.AddCell(ContentCompany);

            PdfPCell ContentTitle = new PdfPCell();
            ContentTitle.Border = Rectangle.NO_BORDER;
            ContentTitle.Colspan = 2;
            ContentTitle.PaddingTop = 0;
            ContentTitle.VerticalAlignment = Element.ALIGN_MIDDLE;
            //header
            Paragraph tHeader = new Paragraph("INSPEKSI LORRY MASUK TERMINAL", fontTitle);
            tHeader.SpacingBefore = 0;
            tHeader.SpacingAfter = 0;
            tHeader.Alignment = Element.ALIGN_CENTER;
            ContentTitle.AddElement(tHeader);
            headerTitle.AddCell(ContentTitle);
        }

        private void RenderTableHeader(PdfPTable tableHeader, TruckLoading data, PdfContentByte cb)
        {
            PdfPCell ContentRefCustomer = new PdfPCell();
            ContentRefCustomer.Border = Rectangle.NO_BORDER;
            ContentRefCustomer.PaddingTop = 5;
            ContentRefCustomer.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tRefCustomer = new Paragraph("Ref. Pelanggan :", fontFieldLabel);
            tRefCustomer.SpacingBefore = 3;
            tRefCustomer.SpacingAfter = 3;
            tRefCustomer.Alignment = Element.ALIGN_LEFT;
            ContentRefCustomer.AddElement(tRefCustomer);
            tableHeader.AddCell(ContentRefCustomer);

            PdfPCell ContentRefCustomer1 = new PdfPCell();
            ContentRefCustomer1.Border = Rectangle.NO_BORDER;
            ContentRefCustomer1.PaddingTop = 5;
            ContentRefCustomer1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tRefCustomer1 = new Paragraph(data.DeliveryOrderNumber, fontFieldMediumValue);
            tRefCustomer1.SpacingBefore = 3;
            tRefCustomer1.SpacingAfter = 3;
            tRefCustomer1.Alignment = Element.ALIGN_LEFT;
            ContentRefCustomer1.AddElement(tRefCustomer1);
            tableHeader.AddCell(ContentRefCustomer1);

            PdfPCell ContentDate = new PdfPCell();
            ContentDate.Border = Rectangle.NO_BORDER;
            ContentDate.PaddingTop = 5;
            ContentDate.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tDate = new Paragraph("Tgl :", fontFieldLabel);
            tDate.SpacingBefore = 3;
            tDate.SpacingAfter = 3;
            tDate.Alignment = Element.ALIGN_LEFT;
            ContentDate.AddElement(tDate);
            tableHeader.AddCell(ContentDate);

            string administrationDate = data.AdministrationTimestamp.HasValue ? data.AdministrationTimestamp.Value.ToString("dd-MMM-yyyy") : "";
            string administrationTime = data.AdministrationTimestamp.HasValue ? data.AdministrationTimestamp.Value.ToString("HH:mm:ss") : "";
            PdfPCell ContentDate1 = new PdfPCell();
            ContentDate1.Border = Rectangle.NO_BORDER;
            ContentDate1.PaddingTop = 5;
            ContentDate1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tDate1 = new Paragraph(administrationDate, fontFieldMediumValue);
            tDate1.SpacingBefore = 3;
            tDate1.SpacingAfter = 3;
            tDate1.Alignment = Element.ALIGN_LEFT;
            ContentDate1.AddElement(tDate1);
            tableHeader.AddCell(ContentDate1);

            PdfPCell ContentInspection = new PdfPCell();
            ContentInspection.Border = Rectangle.NO_BORDER;
            ContentInspection.PaddingTop = 5;
            ContentInspection.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tInspection = new Paragraph("Hasil Inspeksi :", fontFieldLabel);
            tInspection.SpacingBefore = 3;
            tInspection.SpacingAfter = 3;
            tInspection.Alignment = Element.ALIGN_LEFT;
            ContentInspection.AddElement(tInspection);
            tableHeader.AddCell(ContentInspection);

            PdfPCell ContentBoxOk = new PdfPCell();
            ContentBoxOk.Border = Rectangle.BOX;
            ContentBoxOk.PaddingTop = 5;
            ContentBoxOk.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tBoxOk = new Paragraph("", fontFieldValue);
            tBoxOk.SpacingBefore = 3;
            tBoxOk.SpacingAfter = 3;
            tBoxOk.Alignment = Element.ALIGN_LEFT;
            ContentBoxOk.AddElement(tBoxOk);
            tableHeader.AddCell(ContentBoxOk);

            PdfPCell ContentOk = new PdfPCell();
            ContentOk.Border = Rectangle.NO_BORDER;
            ContentOk.PaddingTop = 5;
            ContentOk.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tOk = new Paragraph("BOLEH MASUK", fontFieldValue);
            tOk.SpacingBefore = 3;
            tOk.SpacingAfter = 3;
            tOk.Alignment = Element.ALIGN_LEFT;
            ContentOk.AddElement(tOk);
            tableHeader.AddCell(ContentOk);
            
            //baris 2
            PdfPCell ContentQueue = new PdfPCell();
            ContentQueue.Border = Rectangle.NO_BORDER;
            ContentQueue.PaddingTop = 5;
            ContentQueue.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tQueue = new Paragraph("No. Antrian :", fontFieldLabel);
            tQueue.SpacingBefore = 3;
            tQueue.SpacingAfter = 3;
            tQueue.Alignment = Element.ALIGN_LEFT;
            ContentQueue.AddElement(tQueue);
            tableHeader.AddCell(ContentQueue);

            PdfPCell ContentQueue1 = new PdfPCell();
            ContentQueue1.Border = Rectangle.NO_BORDER;
            ContentQueue1.PaddingTop = 5;
            ContentQueue1.VerticalAlignment = Element.ALIGN_MIDDLE;
            ViewQueueTruck[] queue = ViewQueueTruck.FindByTruckLoadingId(data.TruckLoadingId);
            Paragraph tQueue1 = new Paragraph(queue[0].QueueNumber, fontFieldMediumValue);
            tQueue1.SpacingBefore = 3;
            tQueue1.SpacingAfter = 3;
            tQueue1.Alignment = Element.ALIGN_LEFT;
            ContentQueue1.AddElement(tQueue1);
            tableHeader.AddCell(ContentQueue1);

            PdfPCell ContentLane = new PdfPCell();
            ContentLane.Border = Rectangle.NO_BORDER;
            ContentLane.PaddingTop = 5;
            ContentLane.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tLane = new Paragraph("No. Lajur :", fontFieldLabel);
            tLane.SpacingBefore = 3;
            tLane.SpacingAfter = 3;
            tLane.Alignment = Element.ALIGN_LEFT;
            ContentLane.AddElement(tLane);
            tableHeader.AddCell(ContentLane);

            PdfPCell ContentLane1 = new PdfPCell();
            ContentLane1.Border = Rectangle.NO_BORDER;
            ContentLane1.PaddingTop = 5;
            ContentLane1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tLane1 = new Paragraph(data.FillingPointString, fontFieldMediumValue);
            tLane1.SpacingBefore = 3;
            tLane1.SpacingAfter = 3;
            tRefCustomer1.Alignment = Element.ALIGN_LEFT;
            ContentLane1.AddElement(tLane1);
            tableHeader.AddCell(ContentLane1);

            PdfPCell ContentInspection1 = new PdfPCell();
            ContentInspection1.Border = Rectangle.NO_BORDER;
            ContentInspection1.PaddingTop = 5;
            ContentInspection1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tInspection1 = new Paragraph("", fontFieldValue);
            tInspection1.SpacingBefore = 3;
            tInspection1.SpacingAfter = 3;
            tInspection1.Alignment = Element.ALIGN_LEFT;
            ContentInspection1.AddElement(tInspection1);
            tableHeader.AddCell(ContentInspection1);

            PdfPCell ContentBoxNotOk = new PdfPCell();
            ContentBoxNotOk.Border = Rectangle.BOX;
            ContentBoxNotOk.PaddingTop = 5;
            ContentBoxNotOk.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tBoxNotOk = new Paragraph("", fontFieldValue);
            tBoxNotOk.SpacingBefore = 3;
            tBoxNotOk.SpacingAfter = 3;
            tBoxNotOk.Alignment = Element.ALIGN_LEFT;
            ContentBoxNotOk.AddElement(tBoxNotOk);
            tableHeader.AddCell(ContentBoxNotOk);

            PdfPCell ContentNotOk = new PdfPCell();
            ContentNotOk.Border = Rectangle.NO_BORDER;
            ContentNotOk.PaddingTop = 5;
            ContentNotOk.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNotOk = new Paragraph("DITOLAK", fontFieldValue);
            tNotOk.SpacingBefore = 3;
            tNotOk.SpacingAfter = 3;
            tNotOk.Alignment = Element.ALIGN_LEFT;
            ContentNotOk.AddElement(tNotOk);
            tableHeader.AddCell(ContentNotOk);

            //baris3
            PdfPCell ContentDate2 = new PdfPCell();
            ContentDate2.Border = Rectangle.NO_BORDER;
            ContentDate2.PaddingTop = 5;
            ContentDate2.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tDate2 = new Paragraph("Tgl. Cetak :", fontFieldLabel);
            tDate2.SpacingBefore = 3;
            tDate2.SpacingAfter = 3;
            tDate2.Alignment = Element.ALIGN_LEFT;
            ContentDate2.AddElement(tDate2);
            tableHeader.AddCell(ContentDate2);

            PdfPCell ContentDate3 = new PdfPCell();
            ContentDate3.Border = Rectangle.NO_BORDER;
            ContentDate3.PaddingTop = 5;
            //ContentDate3.Colspan = 2;
            ContentDate3.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tDate3 = new Paragraph(administrationDate, fontFieldMediumValue);
            tDate3.SpacingBefore = 3;
            tDate3.SpacingAfter = 3;
            tRefCustomer1.Alignment = Element.ALIGN_LEFT;
            ContentDate3.AddElement(tDate3);
            tableHeader.AddCell(ContentDate3);

            PdfPCell ContentTime = new PdfPCell();
            ContentTime.Border = Rectangle.NO_BORDER;
            ContentTime.PaddingTop = 5;
            ContentTime.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTime = new Paragraph("Jam :", fontFieldLabel);
            tTime.SpacingBefore = 3;
            tTime.SpacingAfter = 3;
            tTime.Alignment = Element.ALIGN_LEFT;
            ContentTime.AddElement(tTime);
            tableHeader.AddCell(ContentTime);

            PdfPCell ContentTime1 = new PdfPCell();
            ContentTime1.Border = Rectangle.NO_BORDER;
            ContentTime1.PaddingTop = 5;
            ContentTime1.Colspan = 2;
            ContentTime1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTime1 = new Paragraph(administrationTime, fontFieldMediumValue);
            tTime1.SpacingBefore = 3;
            tTime1.SpacingAfter = 3;
            tTime1.Alignment = Element.ALIGN_LEFT;
            ContentTime1.AddElement(tTime1);
            tableHeader.AddCell(ContentTime1);

            PdfPCell ContentOtherBox = new PdfPCell();
            ContentOtherBox.Border = Rectangle.BOX;
            ContentOtherBox.PaddingTop = 5;
            ContentOtherBox.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tOtherBox = new Paragraph("", fontSubTitle);
            tOtherBox.SpacingBefore = 3;
            tOtherBox.SpacingAfter = 3;
            tOtherBox.Alignment = Element.ALIGN_LEFT;
            ContentOtherBox.AddElement(tOtherBox);
            tableHeader.AddCell(ContentOtherBox);

            PdfPCell ContentOther = new PdfPCell();
            ContentOther.Border = Rectangle.NO_BORDER;
            ContentOther.PaddingTop = 5;
            ContentOther.VerticalAlignment = Element.ALIGN_BOTTOM;
            Paragraph tOther = new Paragraph("...........", fontSubTitle);
            tOther.SpacingBefore = 3;
            tOther.SpacingAfter = 3;
            tOther.Alignment = Element.ALIGN_LEFT;
            ContentOther.AddElement(tOther);
            tableHeader.AddCell(ContentOther);

            PdfPCell ContentRegPlate = new PdfPCell();
            ContentRegPlate.Border = Rectangle.NO_BORDER;
            ContentRegPlate.PaddingTop = 5;
            ContentRegPlate.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tRegPlate = new Paragraph("No. Polisi :", fontFieldLabel);
            tRegPlate.SpacingBefore = 3;
            tRegPlate.SpacingAfter = 3;
            tRegPlate.Alignment = Element.ALIGN_LEFT;
            ContentRegPlate.AddElement(tRegPlate);
            tableHeader.AddCell(ContentRegPlate);

            PdfPCell ContentRegPlate1 = new PdfPCell();
            ContentRegPlate1.Border = Rectangle.NO_BORDER;
            ContentRegPlate1.PaddingTop = 5;
            ContentRegPlate1.Colspan = 3;
            ContentRegPlate1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tRegPlate1 = new Paragraph(data.TruckRegistrationPlate, fontFieldMediumValue);
            tRegPlate1.SpacingBefore = 3;
            tRegPlate1.SpacingAfter = 3;
            tRegPlate1.Alignment = Element.ALIGN_LEFT;
            ContentRegPlate1.AddElement(tRegPlate1);
            tableHeader.AddCell(ContentRegPlate1);

            //barcode
            Barcode128 code128 = new Barcode128();
            code128.CodeType = Barcode.CODE128;
            code128.Code = data.DeliveryOrderNumber.ToString();
            //code128.Font = null; //without text
            iTextSharp.text.Image image128 = code128.CreateImageWithBarcode(cb, null, null);
            image128.Border = Rectangle.NO_BORDER;
            image128.WidthPercentage = 100;
            image128.ScaleAbsolute(95f, 95f);
            image128.Alignment = Element.ALIGN_CENTER;
            PdfPCell barcodeCell = new PdfPCell();
            barcodeCell.Rowspan = 3;
            barcodeCell.Colspan = 3;
            barcodeCell.Border = Rectangle.NO_BORDER;
            barcodeCell.BorderWidth = 1;
            barcodeCell.AddElement(image128);
            tableHeader.AddCell(barcodeCell);

            //baris 5
            PdfPCell ContentDriver = new PdfPCell();
            ContentDriver.Border = Rectangle.NO_BORDER;
            ContentDriver.PaddingTop = 5;
            ContentDriver.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tDriver = new Paragraph("Pengemudi : ", fontFieldLabel);
            tDriver.SpacingBefore = 5;
            tDriver.SpacingAfter = 5;
            tDriver.Alignment = Element.ALIGN_LEFT;
            ContentDriver.AddElement(tDriver);
            tableHeader.AddCell(ContentDriver);

            PdfPCell ContentDriver1 = new PdfPCell();
            ContentDriver1.Border = Rectangle.NO_BORDER;
            ContentDriver1.PaddingTop = 5;
            ContentDriver1.Colspan = 3;
            ContentDriver1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Driver d = Driver.FindById(data.DriverId);
            Paragraph tDriver1 = new Paragraph(d.Name, fontFieldMediumValue);
            tDriver1.SpacingBefore = 5;
            tDriver1.SpacingAfter = 5;
            tDriver1.Alignment = Element.ALIGN_LEFT;
            ContentDriver1.AddElement(tDriver1);
            tableHeader.AddCell(ContentDriver1);

            //baris 6
            PdfPCell ContentTransporter = new PdfPCell();
            ContentTransporter.Border = Rectangle.NO_BORDER;
            ContentTransporter.PaddingTop = 5;
            ContentTransporter.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTransporter = new Paragraph("Transporter : ", fontFieldLabel);
            tTransporter.SpacingBefore = 5;
            tTransporter.SpacingAfter = 5;
            tTransporter.Alignment = Element.ALIGN_LEFT;
            ContentTransporter.AddElement(tTransporter);
            tableHeader.AddCell(ContentTransporter);

            PdfPCell ContentTransporter1 = new PdfPCell();
            ContentTransporter1.Border = Rectangle.NO_BORDER;
            ContentTransporter1.PaddingTop = 5;
            ContentTransporter1.Colspan = 3;
            ContentTransporter1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Truck t = Truck.FindByRegistrationPlate(data.TruckRegistrationPlate);
            Paragraph tTransporter1 = new Paragraph(t.TruckTransporter, fontFieldMediumValue);
            tTransporter1.SpacingBefore = 5;
            tTransporter1.SpacingAfter = 5;
            tTransporter1.Alignment = Element.ALIGN_LEFT;
            ContentTransporter1.AddElement(tTransporter1);
            tableHeader.AddCell(ContentTransporter1);
        }

        private void RenderTableContent(PdfPTable tableContent)
        {
            //baris header
            PdfPCell ContentNo = new PdfPCell();
            ContentNo.Border = Rectangle.BOX;
            ContentNo.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentNo.HorizontalAlignment = Element.ALIGN_MIDDLE;
            ContentNo.PaddingTop = 3;
            Paragraph tNo = new Paragraph("No.", fontFieldMediumLabel);
            tNo.SpacingBefore = 4;
            tNo.SpacingAfter = 4;
            tNo.Alignment = Element.ALIGN_CENTER;
            ContentNo.AddElement(tNo);
            tableContent.AddCell(ContentNo);

            PdfPCell ContentTitle = new PdfPCell();
            ContentTitle.Border = Rectangle.BOX;
            ContentTitle.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle = new Paragraph("Nama Barang", fontFieldMediumLabel);
            tTitle.SpacingBefore = 4;
            tTitle.SpacingAfter = 4;
            tTitle.Alignment = Element.ALIGN_CENTER;
            ContentTitle.AddElement(tTitle);
            tableContent.AddCell(ContentTitle);

            PdfPCell ContentOk = new PdfPCell();
            ContentOk.Border = Rectangle.BOX;
            ContentOk.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tOk = new Paragraph("Ada/OK", fontFieldMediumLabel);
            tOk.SpacingBefore = 4;
            tOk.SpacingAfter = 4;
            tOk.Alignment = Element.ALIGN_CENTER;
            ContentOk.AddElement(tOk);
            tableContent.AddCell(ContentOk);

            PdfPCell ContentNotOk = new PdfPCell();
            ContentNotOk.Border = Rectangle.BOX;
            ContentNotOk.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNotOk = new Paragraph("Nihil/Tidak OK", fontFieldMediumLabel);
            tNotOk.SpacingBefore = 4;
            tNotOk.SpacingAfter = 4;
            tNotOk.Alignment = Element.ALIGN_CENTER;
            ContentNotOk.AddElement(tNotOk);
            tableContent.AddCell(ContentNotOk);

            PdfPCell ContentInspector = new PdfPCell();
            ContentInspector.Border = Rectangle.BOX;
            ContentInspector.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tInspector = new Paragraph("Inspektor", fontFieldMediumLabel);
            tInspector.SpacingBefore = 4;
            tInspector.SpacingAfter = 4;
            tInspector.Alignment = Element.ALIGN_CENTER;
            ContentInspector.AddElement(tInspector);
            tableContent.AddCell(ContentInspector);

            //baris 1
            PdfPCell ContentNo1 = new PdfPCell();
            ContentNo1.Border = Rectangle.BOX;
            ContentNo1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo1 = new Paragraph("1", fontFieldMediumLabel);
            tNo1.SpacingBefore = 4;
            tNo1.SpacingAfter = 4;
            tNo1.Alignment = Element.ALIGN_CENTER;
            ContentNo1.AddElement(tNo1);
            tableContent.AddCell(ContentNo1);

            PdfPCell ContentTitle1 = new PdfPCell();
            ContentTitle1.Border = Rectangle.BOX;
            ContentTitle1.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle1 = new Paragraph("Perlengkapan APD (Alat Pelindung Diri)", fontFieldMediumLabel);
            tTitle1.SpacingBefore = 4;
            tTitle1.SpacingAfter = 4;
            tTitle1.Alignment = Element.ALIGN_LEFT;
            ContentTitle1.AddElement(tTitle1);
            tableContent.AddCell(ContentTitle1);

            PdfPCell ContentOk1 = new PdfPCell();
            ContentOk1.Border = Rectangle.BOX;
            Paragraph tOk1 = new Paragraph("", fontFieldMediumLabel);
            tOk1.SpacingBefore = 4;
            tOk1.SpacingAfter = 4;
            tOk1.Alignment = Element.ALIGN_CENTER;
            ContentOk1.AddElement(tOk1);
            tableContent.AddCell(ContentOk1);

            PdfPCell ContentNotOk1 = new PdfPCell();
            ContentNotOk1.Border = Rectangle.BOX;
            Paragraph tNotOk1 = new Paragraph("", fontFieldMediumLabel);
            tNotOk1.SpacingBefore = 4;
            tNotOk1.SpacingAfter = 4;
            tNotOk1.Alignment = Element.ALIGN_CENTER;
            ContentNotOk1.AddElement(tNotOk1);
            tableContent.AddCell(ContentNotOk1);

            PdfPCell ContentInspector1 = new PdfPCell();
            ContentInspector1.Border = Rectangle.BOX;
            ContentInspector1.Rowspan = 7;
            Paragraph tInspector1 = new Paragraph("", fontFieldMediumLabel);
            tInspector1.SpacingBefore = 4;
            tInspector1.SpacingAfter = 4;
            tInspector1.Alignment = Element.ALIGN_CENTER;
            ContentInspector1.AddElement(tInspector1);
            tableContent.AddCell(ContentInspector1);

            //baris 1a
            PdfPCell ContentNo1a = new PdfPCell();
            ContentNo1a.Border = Rectangle.BOX;
            Paragraph tNo1a = new Paragraph("", fontFieldMediumLabel);
            tNo1a.SpacingBefore = 4;
            tNo1a.SpacingAfter = 4;
            tNo1a.Alignment = Element.ALIGN_CENTER;
            ContentNo1a.AddElement(tNo1a);
            tableContent.AddCell(ContentNo1a);

            PdfPCell ContentTitle1a = new PdfPCell();
            ContentTitle1a.Border = Rectangle.BOX;
            ContentTitle1a.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle1a = new Paragraph("1.a. Safety Helm (Helm)", fontFieldMediumLabel);
            tTitle1a.SpacingBefore = 4;
            tTitle1a.SpacingAfter = 4;
            tTitle1a.Alignment = Element.ALIGN_LEFT;
            ContentTitle1a.AddElement(tTitle1a);
            tableContent.AddCell(ContentTitle1a);

            PdfPCell ContentOk1a = new PdfPCell();
            ContentOk1a.Border = Rectangle.BOX;
            Paragraph tOk1a = new Paragraph("", fontFieldMediumLabel);
            tOk1a.SpacingBefore = 4;
            tOk1a.SpacingAfter = 4;
            tOk1a.Alignment = Element.ALIGN_CENTER;
            ContentOk1a.AddElement(tOk1a);
            tableContent.AddCell(ContentOk1a);

            PdfPCell ContentNotOk1a = new PdfPCell();
            ContentNotOk1a.Border = Rectangle.BOX;
            Paragraph tNotOk1a = new Paragraph("", fontFieldMediumLabel);
            tNotOk1a.SpacingBefore = 4;
            tNotOk1a.SpacingAfter = 4;
            tNotOk1a.Alignment = Element.ALIGN_CENTER;
            ContentNotOk1a.AddElement(tNotOk1a);
            tableContent.AddCell(ContentNotOk1a);

            //baris 1b
            PdfPCell ContentNo1b = new PdfPCell();
            ContentNo1b.Border = Rectangle.BOX;
            Paragraph tNo1b = new Paragraph("", fontFieldMediumLabel);
            tNo1b.SpacingBefore = 4;
            tNo1b.SpacingAfter = 4;
            tNo1b.Alignment = Element.ALIGN_CENTER;
            ContentNo1b.AddElement(tNo1b);
            tableContent.AddCell(ContentNo1b);

            PdfPCell ContentTitle1b = new PdfPCell();
            ContentTitle1b.Border = Rectangle.BOX;
            ContentTitle1b.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle1b = new Paragraph("1.b. Safety Shoes (Sepatu Safety)", fontFieldMediumLabel);
            tTitle1b.SpacingBefore = 4;
            tTitle1b.SpacingAfter = 4;
            tTitle1b.Alignment = Element.ALIGN_LEFT;
            ContentTitle1b.AddElement(tTitle1b);
            tableContent.AddCell(ContentTitle1b);

            PdfPCell ContentOk1b = new PdfPCell();
            ContentOk1b.Border = Rectangle.BOX;
            ContentOk1b.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tOk1b = new Paragraph("", fontFieldMediumLabel);
            tOk1b.SpacingBefore = 4;
            tOk1b.SpacingAfter = 4;
            tOk1b.Alignment = Element.ALIGN_CENTER;
            ContentOk1b.AddElement(tOk1b);
            tableContent.AddCell(ContentOk1b);

            PdfPCell ContentNotOk1b = new PdfPCell();
            ContentNotOk1b.Border = Rectangle.BOX;
            ContentNotOk1b.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNotOk1b = new Paragraph("", fontFieldMediumLabel);
            tNotOk1b.SpacingBefore = 4;
            tNotOk1b.SpacingAfter = 4;
            tNotOk1b.Alignment = Element.ALIGN_CENTER;
            ContentNotOk1b.AddElement(tNotOk1b);
            tableContent.AddCell(ContentNotOk1b);

            //baris 1c
            PdfPCell ContentNo1c = new PdfPCell();
            ContentNo1c.Border = Rectangle.BOX;
            Paragraph tNo1c = new Paragraph("", fontFieldMediumLabel);
            tNo1c.SpacingBefore = 4;
            tNo1c.SpacingAfter = 4;
            tNo1c.Alignment = Element.ALIGN_CENTER;
            ContentNo1c.AddElement(tNo1c);
            tableContent.AddCell(ContentNo1c);

            PdfPCell ContentTitle1c = new PdfPCell();
            ContentTitle1c.Border = Rectangle.BOX;
            ContentTitle1c.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle1c = new Paragraph("1.c. Sarung Tangan", fontFieldMediumLabel);
            tTitle1c.SpacingBefore = 4;
            tTitle1c.SpacingAfter = 4;
            tTitle1c.Alignment = Element.ALIGN_LEFT;
            ContentTitle1c.AddElement(tTitle1c);
            tableContent.AddCell(ContentTitle1c);

            PdfPCell ContentOk1c = new PdfPCell();
            ContentOk1c.Border = Rectangle.BOX;
            Paragraph tOk1c = new Paragraph("", fontFieldMediumLabel);
            tOk1c.SpacingBefore = 4;
            tOk1c.SpacingAfter = 4;
            tOk1c.Alignment = Element.ALIGN_CENTER;
            ContentOk1c.AddElement(tOk1c);
            tableContent.AddCell(ContentOk1c);

            PdfPCell ContentNotOk1c = new PdfPCell();
            ContentNotOk1c.Border = Rectangle.BOX;
            Paragraph tNotOk1c = new Paragraph("", fontFieldMediumLabel);
            tNotOk1c.SpacingBefore = 4;
            tNotOk1c.SpacingAfter = 4;
            tNotOk1c.Alignment = Element.ALIGN_CENTER;
            ContentNotOk1c.AddElement(tNotOk1c);
            tableContent.AddCell(ContentNotOk1c);

            //baris 1d
            PdfPCell ContentNo1d = new PdfPCell();
            ContentNo1d.Border = Rectangle.BOX;
            Paragraph tNo1d = new Paragraph("", fontFieldMediumLabel);
            tNo1d.SpacingBefore = 4;
            tNo1d.SpacingAfter = 4;
            tNo1d.Alignment = Element.ALIGN_CENTER;
            ContentNo1d.AddElement(tNo1d);
            tableContent.AddCell(ContentNo1d);

            PdfPCell ContentTitle1d = new PdfPCell();
            ContentTitle1d.Border = Rectangle.BOX;
            ContentTitle1d.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle1d = new Paragraph("1.d. Safety Goggle (Kacamata Safety)", fontFieldMediumLabel);
            tTitle1d.SpacingBefore = 4;
            tTitle1d.SpacingAfter = 4;
            tTitle1d.Alignment = Element.ALIGN_LEFT;
            ContentTitle1d.AddElement(tTitle1d);
            tableContent.AddCell(ContentTitle1d);

            PdfPCell ContentOk1d = new PdfPCell();
            ContentOk1d.Border = Rectangle.BOX;
            Paragraph tOk1d = new Paragraph("", fontFieldMediumLabel);
            tOk1d.SpacingBefore = 4;
            tOk1d.SpacingAfter = 4;
            tOk1d.Alignment = Element.ALIGN_CENTER;
            ContentOk1d.AddElement(tOk1d);
            tableContent.AddCell(ContentOk1d);

            PdfPCell ContentNotOk1d = new PdfPCell();
            ContentNotOk1d.Border = Rectangle.BOX;
            Paragraph tNotOk1d = new Paragraph("", fontFieldMediumLabel);
            tNotOk1d.SpacingBefore = 4;
            tNotOk1d.SpacingAfter = 4;
            tNotOk1d.Alignment = Element.ALIGN_CENTER;
            ContentNotOk1d.AddElement(tNotOk1d);
            tableContent.AddCell(ContentNotOk1d);

            //no 2
            PdfPCell ContentNo2 = new PdfPCell();
            ContentNo2.Border = Rectangle.BOX;
            ContentNo2.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo2 = new Paragraph("2", fontFieldMediumLabel);
            tNo2.SpacingBefore = 4;
            tNo2.SpacingAfter = 4;
            tNo2.Alignment = Element.ALIGN_CENTER;
            ContentNo2.AddElement(tNo2);
            tableContent.AddCell(ContentNo2);

            PdfPCell ContentTitle2 = new PdfPCell();
            ContentTitle2.Border = Rectangle.BOX;
            ContentTitle2.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle2 = new Paragraph("APAR (Alat Pemadam Api Ringan) layak pakai", fontFieldMediumLabel);
            tTitle2.SpacingBefore = 4;
            tTitle2.SpacingAfter = 4;
            tTitle2.Alignment = Element.ALIGN_LEFT;
            ContentTitle2.AddElement(tTitle2);
            tableContent.AddCell(ContentTitle2);

            PdfPCell ContentOk2 = new PdfPCell();
            ContentOk2.Border = Rectangle.BOX;
            Paragraph tOk2 = new Paragraph("", fontFieldMediumLabel);
            tOk2.SpacingBefore = 4;
            tOk2.SpacingAfter = 4;
            tOk2.Alignment = Element.ALIGN_CENTER;
            ContentOk2.AddElement(tOk2);
            tableContent.AddCell(ContentOk2);

            PdfPCell ContentNotOk2 = new PdfPCell();
            ContentNotOk2.Border = Rectangle.BOX;
            Paragraph tNotOk2 = new Paragraph("", fontFieldMediumLabel);
            tNotOk2.SpacingBefore = 4;
            tNotOk2.SpacingAfter = 4;
            tNotOk2.Alignment = Element.ALIGN_CENTER;
            ContentNotOk2.AddElement(tNotOk2);
            tableContent.AddCell(ContentNotOk2);

            //no3 mulai rowspan
            PdfPCell ContentNo3 = new PdfPCell();
            ContentNo3.Border = Rectangle.BOX;
            ContentNo3.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo3 = new Paragraph("3", fontFieldMediumLabel);
            tNo3.SpacingBefore = 4;
            tNo3.SpacingAfter = 4;
            tNo3.Alignment = Element.ALIGN_CENTER;
            ContentNo3.AddElement(tNo3);
            tableContent.AddCell(ContentNo3);

            PdfPCell ContentTitle3 = new PdfPCell();
            ContentTitle3.Border = Rectangle.BOX;
            ContentTitle3.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle3 = new Paragraph("Saringan Knalpot", fontFieldMediumLabel);
            tTitle3.SpacingBefore = 4;
            tTitle3.SpacingAfter = 4;
            tTitle3.Alignment = Element.ALIGN_LEFT;
            ContentTitle3.AddElement(tTitle3);
            tableContent.AddCell(ContentTitle3);

            PdfPCell ContentOk3 = new PdfPCell();
            ContentOk3.Border = Rectangle.BOX;
            Paragraph tOk3 = new Paragraph("", fontFieldMediumLabel);
            tOk3.SpacingBefore = 4;
            tOk3.SpacingAfter = 4;
            tOk3.Alignment = Element.ALIGN_CENTER;
            ContentOk3.AddElement(tOk3);
            tableContent.AddCell(ContentOk3);

            PdfPCell ContentNotOk3 = new PdfPCell();
            ContentNotOk3.Border = Rectangle.BOX;
            Paragraph tNotOk3 = new Paragraph("", fontFieldMediumLabel);
            tNotOk3.SpacingBefore = 4;
            tNotOk3.SpacingAfter = 4;
            tNotOk3.Alignment = Element.ALIGN_CENTER;
            ContentNotOk3.AddElement(tNotOk3);
            tableContent.AddCell(ContentNotOk3);

            //no4
            PdfPCell ContentNo4 = new PdfPCell();
            ContentNo4.Border = Rectangle.BOX;
            ContentNo4.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo4 = new Paragraph("4", fontFieldMediumLabel);
            tNo4.SpacingBefore = 4;
            tNo4.SpacingAfter = 4;
            tNo4.Alignment = Element.ALIGN_CENTER;
            ContentNo4.AddElement(tNo4);
            tableContent.AddCell(ContentNo4);

            PdfPCell ContentTitle4 = new PdfPCell();
            ContentTitle4.Border = Rectangle.BOX;
            ContentTitle4.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle4 = new Paragraph("Ban Kendaraan - depan kondisi baik", fontFieldMediumLabel);
            tTitle4.SpacingBefore = 4;
            tTitle4.SpacingAfter = 4;
            tTitle4.Alignment = Element.ALIGN_LEFT;
            ContentTitle4.AddElement(tTitle4);
            tableContent.AddCell(ContentTitle4);

            PdfPCell ContentOk4 = new PdfPCell();
            ContentOk4.Border = Rectangle.BOX;
            Paragraph tOk4 = new Paragraph("", fontFieldMediumLabel);
            tOk4.SpacingBefore = 4;
            tOk4.SpacingAfter = 4;
            tOk4.Alignment = Element.ALIGN_CENTER;
            ContentOk4.AddElement(tOk4);
            tableContent.AddCell(ContentOk4);

            PdfPCell ContentNotOk4 = new PdfPCell();
            ContentNotOk4.Border = Rectangle.BOX;
            Paragraph tNotOk4 = new Paragraph("", fontFieldMediumLabel);
            tNotOk4.SpacingBefore = 4;
            tNotOk4.SpacingAfter = 4;
            tNotOk4.Alignment = Element.ALIGN_CENTER;
            ContentNotOk4.AddElement(tNotOk4);
            tableContent.AddCell(ContentNotOk4);

            PdfPCell ContentPengemudi = new PdfPCell();
            ContentPengemudi.Border = Rectangle.BOX;
            ContentPengemudi.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tPengemudi = new Paragraph("Pengemudi", fontFieldMediumLabel);
            tPengemudi.SpacingBefore = 4;
            tPengemudi.SpacingAfter = 4;
            tPengemudi.Alignment = Element.ALIGN_CENTER;
            ContentPengemudi.AddElement(tPengemudi);
            tableContent.AddCell(ContentPengemudi);

            //no5
            PdfPCell ContentNo5 = new PdfPCell();
            ContentNo5.Border = Rectangle.BOX;
            ContentNo5.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo5 = new Paragraph("5", fontFieldMediumLabel);
            tNo5.SpacingBefore = 4;
            tNo5.SpacingAfter = 4;
            tNo5.Alignment = Element.ALIGN_CENTER;
            ContentNo5.AddElement(tNo5);
            tableContent.AddCell(ContentNo5);

            PdfPCell ContentTitle5 = new PdfPCell();
            ContentTitle5.Border = Rectangle.BOX;
            ContentTitle5.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle5 = new Paragraph("Ban Kendaraan - tengah kondisi baik", fontFieldMediumLabel);
            tTitle5.SpacingBefore = 4;
            tTitle5.SpacingAfter = 4;
            tTitle5.Alignment = Element.ALIGN_LEFT;
            ContentTitle5.AddElement(tTitle5);
            tableContent.AddCell(ContentTitle5);

            PdfPCell ContentOk5 = new PdfPCell();
            ContentOk5.Border = Rectangle.BOX;
            Paragraph tOk5 = new Paragraph("", fontFieldMediumLabel);
            tOk5.SpacingBefore = 4;
            tOk5.SpacingAfter = 4;
            tOk5.Alignment = Element.ALIGN_CENTER;
            ContentOk5.AddElement(tOk5);
            tableContent.AddCell(ContentOk5);

            PdfPCell ContentNotOk5 = new PdfPCell();
            ContentNotOk5.Border = Rectangle.BOX;
            Paragraph tNotOk5 = new Paragraph("", fontFieldMediumLabel);
            tNotOk5.SpacingBefore = 4;
            tNotOk5.SpacingAfter = 4;
            tNotOk5.Alignment = Element.ALIGN_CENTER;
            ContentNotOk5.AddElement(tNotOk5);
            tableContent.AddCell(ContentNotOk5);

            PdfPCell ContentPengemudi1 = new PdfPCell();
            ContentPengemudi1.Border = Rectangle.BOX;
            ContentPengemudi1.Rowspan = 7;
            Paragraph tPengemudi1 = new Paragraph("", fontFieldMediumLabel);
            tPengemudi1.SpacingBefore = 4;
            tPengemudi1.SpacingAfter = 4;
            tPengemudi1.Alignment = Element.ALIGN_CENTER;
            ContentPengemudi1.AddElement(tPengemudi1);
            tableContent.AddCell(ContentPengemudi1);

            //no6
            PdfPCell ContentNo6 = new PdfPCell();
            ContentNo6.Border = Rectangle.BOX;
            ContentNo6.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo6 = new Paragraph("6", fontFieldMediumLabel);
            tNo6.SpacingBefore = 4;
            tNo6.SpacingAfter = 4;
            tNo6.Alignment = Element.ALIGN_CENTER;
            ContentNo6.AddElement(tNo6);
            tableContent.AddCell(ContentNo6);

            PdfPCell ContentTitle6 = new PdfPCell();
            ContentTitle6.Border = Rectangle.BOX;
            ContentTitle6.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle6 = new Paragraph("Ban Kendaraan - belakang kondisi baik", fontFieldMediumLabel);
            tTitle6.SpacingBefore = 4;
            tTitle6.SpacingAfter = 4;
            tTitle6.Alignment = Element.ALIGN_LEFT;
            ContentTitle6.AddElement(tTitle6);
            tableContent.AddCell(ContentTitle6);

            PdfPCell ContentOk6 = new PdfPCell();
            ContentOk6.Border = Rectangle.BOX;
            Paragraph tOk6 = new Paragraph("", fontFieldMediumLabel);
            tOk6.SpacingBefore = 4;
            tOk6.SpacingAfter = 4;
            tOk6.Alignment = Element.ALIGN_CENTER;
            ContentOk6.AddElement(tOk6);
            tableContent.AddCell(ContentOk6);

            PdfPCell ContentNotOk6 = new PdfPCell();
            ContentNotOk6.Border = Rectangle.BOX;
            Paragraph tNotOk6 = new Paragraph("", fontFieldMediumLabel);
            tNotOk6.SpacingBefore = 4;
            tNotOk6.SpacingAfter = 4;
            tNotOk6.Alignment = Element.ALIGN_CENTER;
            ContentNotOk6.AddElement(tNotOk6);
            tableContent.AddCell(ContentNotOk6);

            //no7
            PdfPCell ContentNo7 = new PdfPCell();
            ContentNo7.Border = Rectangle.BOX;
            ContentNo7.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo7 = new Paragraph("7", fontFieldMediumLabel);
            tNo7.SpacingBefore = 4;
            tNo7.SpacingAfter = 4;
            tNo7.Alignment = Element.ALIGN_CENTER;
            ContentNo7.AddElement(tNo7);
            tableContent.AddCell(ContentNo7);

            PdfPCell ContentTitle7 = new PdfPCell();
            ContentTitle7.Border = Rectangle.BOX;
            ContentTitle7.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle7 = new Paragraph("Aki (battery) ada penutup", fontFieldMediumLabel);
            tTitle7.SpacingBefore = 4;
            tTitle7.SpacingAfter = 4;
            tTitle7.Alignment = Element.ALIGN_LEFT;
            ContentTitle7.AddElement(tTitle7);
            tableContent.AddCell(ContentTitle7);

            PdfPCell ContentOk7 = new PdfPCell();
            ContentOk7.Border = Rectangle.BOX;
            Paragraph tOk7 = new Paragraph("", fontFieldMediumLabel);
            tOk7.SpacingBefore = 4;
            tOk7.SpacingAfter = 4;
            tOk7.Alignment = Element.ALIGN_CENTER;
            ContentOk7.AddElement(tOk7);
            tableContent.AddCell(ContentOk7);

            PdfPCell ContentNotOk7 = new PdfPCell();
            ContentNotOk7.Border = Rectangle.BOX;
            Paragraph tNotOk7 = new Paragraph("", fontFieldMediumLabel);
            tNotOk7.SpacingBefore = 4;
            tNotOk7.SpacingAfter = 4;
            tNotOk7.Alignment = Element.ALIGN_CENTER;
            ContentNotOk7.AddElement(tNotOk7);
            tableContent.AddCell(ContentNotOk7);

            //no 8
            PdfPCell ContentNo8 = new PdfPCell();
            ContentNo8.Border = Rectangle.BOX;
            ContentNo8.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo8 = new Paragraph("8", fontFieldMediumLabel);
            tNo8.SpacingBefore = 4;
            tNo8.SpacingAfter = 4;
            tNo8.Alignment = Element.ALIGN_CENTER;
            ContentNo8.AddElement(tNo8);
            tableContent.AddCell(ContentNo8);

            PdfPCell ContentTitle8 = new PdfPCell();
            ContentTitle8.Border = Rectangle.BOX;
            ContentTitle8.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentTitle8.NoWrap = false;
            Paragraph tTitle8 = new Paragraph("Rem (Brake) & Rem Tangan (Hand Brake) kerja dengan baik", fontFieldMediumLabel);
            tTitle8.SpacingBefore = 4;
            tTitle8.SpacingAfter = 4;
            tTitle8.Alignment = Element.ALIGN_LEFT;
            ContentTitle8.AddElement(tTitle8);
            tableContent.AddCell(ContentTitle8);

            PdfPCell ContentOk8 = new PdfPCell();
            ContentOk8.Border = Rectangle.BOX;
            Paragraph tOk8 = new Paragraph("", fontFieldMediumLabel);
            tOk8.SpacingBefore = 4;
            tOk8.SpacingAfter = 4;
            tOk8.Alignment = Element.ALIGN_CENTER;
            ContentOk8.AddElement(tOk8);
            tableContent.AddCell(ContentOk8);

            PdfPCell ContentNotOk8 = new PdfPCell();
            ContentNotOk8.Border = Rectangle.BOX;
            Paragraph tNotOk8 = new Paragraph("", fontFieldMediumLabel);
            tNotOk8.SpacingBefore = 4;
            tNotOk8.SpacingAfter = 4;
            tNotOk8.Alignment = Element.ALIGN_CENTER;
            ContentNotOk8.AddElement(tNotOk8);
            tableContent.AddCell(ContentNotOk8);

            //baris 9
            PdfPCell ContentNo9 = new PdfPCell();
            ContentNo9.Border = Rectangle.BOX;
            ContentNo9.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo9 = new Paragraph("9", fontFieldMediumLabel);
            tNo9.SpacingBefore = 4;
            tNo9.SpacingAfter = 4;
            tNo9.Alignment = Element.ALIGN_CENTER;
            ContentNo9.AddElement(tNo9);
            tableContent.AddCell(ContentNo9);

            PdfPCell ContentTitle9 = new PdfPCell();
            ContentTitle9.Border = Rectangle.BOX;
            ContentTitle9.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle9 = new Paragraph("", fontFieldMediumLabel);
            tTitle9.SpacingBefore = 4;
            tTitle9.SpacingAfter = 4;
            tTitle9.Alignment = Element.ALIGN_LEFT;
            ContentTitle9.AddElement(tTitle9);
            tableContent.AddCell(ContentTitle9);

            PdfPCell ContentOk9 = new PdfPCell();
            ContentOk9.Border = Rectangle.BOX;
            Paragraph tOk9 = new Paragraph("", fontFieldMediumLabel);
            tOk9.SpacingBefore = 4;
            tOk9.SpacingAfter = 4;
            tOk9.Alignment = Element.ALIGN_CENTER;
            ContentOk9.AddElement(tOk9);
            tableContent.AddCell(ContentOk9);

            PdfPCell ContentNotOk9 = new PdfPCell();
            ContentNotOk9.Border = Rectangle.BOX;
            Paragraph tNotOk9 = new Paragraph("", fontFieldMediumLabel);
            tNotOk9.SpacingBefore = 4;
            tNotOk9.SpacingAfter = 4;
            tNotOk9.Alignment = Element.ALIGN_CENTER;
            ContentNotOk9.AddElement(tNotOk9);
            tableContent.AddCell(ContentNotOk9);

            //no10
            PdfPCell ContentNo10 = new PdfPCell();
            ContentNo10.Border = Rectangle.BOX;
            ContentNo10.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo10 = new Paragraph("10", fontFieldMediumLabel);
            tNo10.SpacingBefore = 4;
            tNo10.SpacingAfter = 4;
            tNo10.Alignment = Element.ALIGN_CENTER;
            ContentNo10.AddElement(tNo10);
            tableContent.AddCell(ContentNo10);

            PdfPCell ContentTitle10 = new PdfPCell();
            ContentTitle10.Border = Rectangle.BOX;
            ContentTitle10.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle10 = new Paragraph("", fontFieldMediumLabel);
            tTitle10.SpacingBefore = 4;
            tTitle10.SpacingAfter = 4;
            tTitle10.Alignment = Element.ALIGN_LEFT;
            ContentTitle10.AddElement(tTitle10);
            tableContent.AddCell(ContentTitle10);

            PdfPCell ContentOk10 = new PdfPCell();
            ContentOk10.Border = Rectangle.BOX;
            Paragraph tOk10 = new Paragraph("", fontFieldMediumLabel);
            tOk10.SpacingBefore = 4;
            tOk10.SpacingAfter = 4;
            tOk10.Alignment = Element.ALIGN_CENTER;
            ContentOk10.AddElement(tOk10);
            tableContent.AddCell(ContentOk10);

            PdfPCell ContentNotOk10 = new PdfPCell();
            ContentNotOk10.Border = Rectangle.BOX;
            Paragraph tNotOk10 = new Paragraph("", fontFieldMediumLabel);
            tNotOk10.SpacingBefore = 4;
            tNotOk10.SpacingAfter = 4;
            tNotOk10.Alignment = Element.ALIGN_CENTER;
            ContentNotOk10.AddElement(tNotOk10);
            tableContent.AddCell(ContentNotOk10);
        }

        private void RenderTableNotes(PdfPTable tableNotes)
        {
            PdfPCell ContentNotes = new PdfPCell();
            ContentNotes.Border = Rectangle.BOX;
            ContentNotes.Colspan = 2;
            ContentNotes.Rowspan = 2;
            ContentNotes.NoWrap = false;
            Paragraph tNotes = new Paragraph("Handphone, Camera Foto/ Video, Korek Api, Rokok, Rokok Elektrik, HT (Handy Talky), Benda Tajam harus dititipkan di Loket-1.", fontFieldLabel);
            tNotes.Alignment = Element.ALIGN_LEFT;
            ContentNotes.AddElement(tNotes);
            tableNotes.AddCell(ContentNotes);

            PdfPCell ContentGuarantee = new PdfPCell();
            ContentGuarantee.Border = Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            ContentGuarantee.NoWrap = false;
            Paragraph tGuarantee = new Paragraph("Pernyataan Jaminan Layak Muat (Cleanliness)", fontFieldValue);
            tGuarantee.Alignment = Element.ALIGN_CENTER;
            ContentGuarantee.AddElement(tGuarantee);
            tableNotes.AddCell(ContentGuarantee);

            PdfPCell ContentCatatan = new PdfPCell();
            ContentCatatan.Border = Rectangle.BOX;
            Paragraph tCatatan = new Paragraph("Catatan", fontFieldLabel);
            tCatatan.Alignment = Element.ALIGN_CENTER;
            ContentCatatan.AddElement(tCatatan);
            tableNotes.AddCell(ContentCatatan);

            PdfPCell ContentGuaranteeValue = new PdfPCell();
            ContentGuaranteeValue.Border = Rectangle.BOTTOM_BORDER;
            ContentGuaranteeValue.Rowspan = 9;
            ContentGuaranteeValue.NoWrap = false;
            Paragraph tGuaranteeValue = new Paragraph(@"Pelanggan dan/atau Transporter yang ditunjuk oleh Pelanggan PT.Redeco Petrolin Utama (penyewa storage tank) dengan ini menjamin dan bertanggungjawab penuh terhadap kelayakan kendaraan yang dilengkapi dengan tanki pengangkut produk, yang akan dipakai untuk mengambil dan memuat produk, termasuk namun tidak terbatas pada kebersihan, tidak akan terkontaminasi / tercemar oleh produk lain  yang diangkut sebelumnya dan / atau zat - zat lain yang dapat menimbulkan kontaminasi terhadap produk yang akan dimuat, atau pencemaran akibat karat, kebocoran akibat karat atau lainnya, pencemaran yang dapat menimbulkan perubahan warna dan / atau kejernihan produk yang dimuat, atau yang dapat menyebabkan pengentalan, terbentuknya suspensi, emulsi atau penggumpalan, valve yang bocor atau tidak tertutup dengan rapat, manhole yang tidak dapat atau tidak tertutup dengan rapat sehingga menimbulkan pencemaran terhadap produk yang diangkut. Pelanggan dan / atau Transporter membebaskan PT. Redeco Petrolin dari segala macam tuntutan sehubungan dengan hal - hal tersebut di atas. Demikian pernyataan ini ditandatangani oleh Pengemudi untuk dan atas nama Transporter / Pelanggan.", fontFooter);
            tGuaranteeValue.Alignment = Element.ALIGN_LEFT;
            ContentGuaranteeValue.AddElement(tGuaranteeValue);
            tableNotes.AddCell(ContentGuaranteeValue);

            PdfPCell ContentCatatan1 = new PdfPCell();
            ContentCatatan1.Border = Rectangle.BOX;
            ContentCatatan1.Rowspan = 9;
            Paragraph tCatatan1 = new Paragraph("", fontFieldMediumLabel);
            tCatatan1.SpacingBefore = 5;
            tCatatan1.SpacingAfter = 5;
            tCatatan1.Alignment = Element.ALIGN_CENTER;
            ContentCatatan1.AddElement(tCatatan1);
            tableNotes.AddCell(ContentCatatan1);

            PdfPCell ContentNotesSeal = new PdfPCell();
            ContentNotesSeal.Border = Rectangle.BOX;
            ContentNotesSeal.Colspan = 2;
            ContentNotesSeal.NoWrap = false;
            Paragraph tNotesSeal = new Paragraph("Kebutuhan Segel", fontFieldValue);
            tNotesSeal.Alignment = Element.ALIGN_CENTER;
            ContentNotesSeal.AddElement(tNotesSeal);
            tableNotes.AddCell(ContentNotesSeal);

            PdfPCell ContentNotesSeal1 = new PdfPCell();
            ContentNotesSeal1.Border = Rectangle.BOX;
            ContentNotesSeal1.NoWrap = false;
            Paragraph tNotesSeal1 = new Paragraph("Kompartemen-1", fontFieldLabel);
            tNotesSeal1.Alignment = Element.ALIGN_LEFT;
            ContentNotesSeal1.AddElement(tNotesSeal1);
            tableNotes.AddCell(ContentNotesSeal1);

            PdfPCell ContentNotesSeal1a = new PdfPCell();
            ContentNotesSeal1a.Border = Rectangle.BOX;
            ContentNotesSeal1a.NoWrap = false;
            Paragraph tNotesSeal1a = new Paragraph("", fontFieldValue);
            tNotesSeal1a.Alignment = Element.ALIGN_LEFT;
            ContentNotesSeal1a.AddElement(tNotesSeal1a);
            tableNotes.AddCell(ContentNotesSeal1a);

            PdfPCell ContentNotesSeal2 = new PdfPCell();
            ContentNotesSeal2.Border = Rectangle.BOX;
            ContentNotesSeal2.NoWrap = false;
            Paragraph tNotesSeal2 = new Paragraph("Kompartemen-2", fontFieldLabel);
            tNotesSeal2.Alignment = Element.ALIGN_LEFT;
            ContentNotesSeal2.AddElement(tNotesSeal2);
            tableNotes.AddCell(ContentNotesSeal2);

            PdfPCell ContentNotesSeal2a = new PdfPCell();
            ContentNotesSeal2a.Border = Rectangle.BOX;
            ContentNotesSeal2a.NoWrap = false;
            Paragraph tNotesSeal2a = new Paragraph("", fontFieldValue);
            tNotesSeal2a.Alignment = Element.ALIGN_LEFT;
            ContentNotesSeal2a.AddElement(tNotesSeal2a);
            tableNotes.AddCell(ContentNotesSeal2a);

            PdfPCell ContentNotesSeal3 = new PdfPCell();
            ContentNotesSeal3.Border = Rectangle.BOX;
            ContentNotesSeal3.NoWrap = false;
            Paragraph tNotesSeal3 = new Paragraph("Kompartemen-3", fontFieldLabel);
            tNotesSeal3.Alignment = Element.ALIGN_LEFT;
            ContentNotesSeal3.AddElement(tNotesSeal3);
            tableNotes.AddCell(ContentNotesSeal3);

            PdfPCell ContentNotesSeal3a = new PdfPCell();
            ContentNotesSeal3a.Border = Rectangle.BOX;
            ContentNotesSeal3a.NoWrap = false;
            Paragraph tNotesSeal3a = new Paragraph("", fontFieldValue);
            tNotesSeal3a.Alignment = Element.ALIGN_LEFT;
            ContentNotesSeal3a.AddElement(tNotesSeal3a);
            tableNotes.AddCell(ContentNotesSeal3a);

            PdfPCell ContentNotesSeal4 = new PdfPCell();
            ContentNotesSeal4.Border = Rectangle.BOX;
            ContentNotesSeal4.NoWrap = false;
            Paragraph tNotesSeal4 = new Paragraph("Kompartemen-4", fontFieldLabel);
            tNotesSeal4.Alignment = Element.ALIGN_LEFT;
            ContentNotesSeal4.AddElement(tNotesSeal4);
            tableNotes.AddCell(ContentNotesSeal4);

            PdfPCell ContentNotesSeal4a = new PdfPCell();
            ContentNotesSeal4a.Border = Rectangle.BOX;
            ContentNotesSeal4a.NoWrap = false;
            Paragraph tNotesSeal4a = new Paragraph("", fontFieldValue);
            tNotesSeal4a.Alignment = Element.ALIGN_LEFT;
            ContentNotesSeal4a.AddElement(tNotesSeal4a);
            tableNotes.AddCell(ContentNotesSeal4a);

            PdfPCell ContentNotesSeal5 = new PdfPCell();
            ContentNotesSeal5.Border = Rectangle.BOX;
            ContentNotesSeal5.NoWrap = false;
            Paragraph tNotesSeal5 = new Paragraph("Kompartemen-5", fontFieldLabel);
            tNotesSeal5.Alignment = Element.ALIGN_LEFT;
            ContentNotesSeal5.AddElement(tNotesSeal5);
            tableNotes.AddCell(ContentNotesSeal5);

            PdfPCell ContentNotesSeal5a = new PdfPCell();
            ContentNotesSeal5a.Border = Rectangle.BOX;
            ContentNotesSeal5a.NoWrap = false;
            Paragraph tNotesSeal5a = new Paragraph("", fontFieldValue);
            tNotesSeal5a.Alignment = Element.ALIGN_LEFT;
            ContentNotesSeal5a.AddElement(tNotesSeal5a);
            tableNotes.AddCell(ContentNotesSeal5a);

            PdfPCell ContentNotesSealOther = new PdfPCell();
            ContentNotesSealOther.Border = Rectangle.BOX;
            ContentNotesSealOther.NoWrap = false;
            Paragraph tNotesSealOther = new Paragraph("Lain-lain", fontFieldLabel);
            tNotesSealOther.Alignment = Element.ALIGN_LEFT;
            ContentNotesSealOther.AddElement(tNotesSealOther);
            tableNotes.AddCell(ContentNotesSealOther);

            PdfPCell ContentNotesSealOthera = new PdfPCell();
            ContentNotesSealOthera.Border = Rectangle.BOX;
            ContentNotesSealOthera.NoWrap = false;
            Paragraph tNotesSealOthera = new Paragraph("", fontFieldValue);
            tNotesSealOthera.Alignment = Element.ALIGN_LEFT;
            ContentNotesSealOthera.AddElement(tNotesSealOthera);
            tableNotes.AddCell(ContentNotesSealOthera);

            PdfPCell ContentNotesSealTotal = new PdfPCell();
            ContentNotesSealTotal.Border = Rectangle.BOX;
            ContentNotesSealTotal.NoWrap = false;
            Paragraph tNotesSealTotal = new Paragraph("Total", fontFieldLabel);
            tNotesSealTotal.Alignment = Element.ALIGN_LEFT;
            ContentNotesSealTotal.AddElement(tNotesSealTotal);
            tableNotes.AddCell(ContentNotesSealTotal);

            PdfPCell ContentNotesSealTotala = new PdfPCell();
            ContentNotesSealTotala.Border = Rectangle.BOX;
            ContentNotesSealTotala.NoWrap = false;
            Paragraph tNotesSealTotala = new Paragraph("", fontFieldValue);
            tNotesSealTotala.Alignment = Element.ALIGN_LEFT;
            ContentNotesSealTotala.AddElement(tNotesSealTotala);
            tableNotes.AddCell(ContentNotesSealTotala);
        }

        private void RenderTableFooter(PdfPTable tableFooter)
        {
            PdfPCell ContentFooter = new PdfPCell();
            ContentFooter.Border = Rectangle.NO_BORDER;
            Paragraph tFooter = new Paragraph("Putih: diserahkan ke Loket 2 - Merah: Transporter - Kuning: Inspektor", fontFieldValue);
            tFooter.Alignment = Element.ALIGN_RIGHT;
            ContentFooter.AddElement(tFooter);
            tableFooter.AddCell(ContentFooter);
        }

        private System.Drawing.Image PrepareLogo()
        {
            string logoPath = HttpContext.Current.Server.MapPath("~/Images/RPU Logo.png");
            System.Drawing.Image imgLogo = System.Drawing.Image.FromFile(logoPath);
            return imgLogo;
        }
    }
}
