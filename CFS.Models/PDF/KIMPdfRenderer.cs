﻿using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;
using System.Globalization;

namespace CFS.Models.PDF
{
    public class KIMPdfRenderer
    {
        private Font fontTitle;
        private Font fontSubTitle;
        private Font fontFieldLabel;
        private Font fontFieldMediumLabel;
        private Font fontFieldLargeLabel;
        private Font fontFieldValue;
        private Font fontFieldMediumValue;
        private Font fontFieldLargeValue;
        private Font fontTableHeader;
        private Font fontTableValue;
        private Font fontFooter;

        private const string formatDate = "MMM/dd/yyyy";
        private const string formatApprovalDate = "MMM/dd/yyyy HH:mm:ss";

        public KIMPdfRenderer()
        {
            fontTitle = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
            fontSubTitle = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
            fontFieldLabel = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL);
            fontFieldMediumLabel = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL);
            fontFieldLargeLabel = new Font(Font.FontFamily.HELVETICA, 16, Font.NORMAL);
            fontFieldValue = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
            fontFieldMediumValue = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
            fontFieldLargeValue = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
            fontTableHeader = new Font(Font.FontFamily.HELVETICA, 7.5f, Font.BOLD);
            fontTableValue = new Font(Font.FontFamily.HELVETICA, 7.5f, Font.NORMAL);
            fontFooter = new Font(Font.FontFamily.HELVETICA, 4, Font.NORMAL);
        }

        public byte[] Render(CFS.Models.Driver data)
        {
            iTextSharp.text.Rectangle rect = new iTextSharp.text.Rectangle(260.0f, 160.0f);
            iTextSharp.text.Document doc = new iTextSharp.text.Document(rect);
            doc.SetMargins(4, 4, 4, 4);
            using (MemoryStream ms = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                //writer.PageEvent = new PDFFooter();
                doc.Open();
                doc.AddTitle(string.Format("KIM # {0}", data.BadgeNumber));
                doc.AddCreator("CFS");

                PdfPTable mainTable = new PdfPTable(2); // 5 kolom
                mainTable.WidthPercentage = 100;
                mainTable.SetWidths(new float[] { 0.4f, 0.6f });

                PdfPTable backTable = new PdfPTable(1);
                backTable.WidthPercentage = 100;

                //border document
                PdfContentByte content = writer.DirectContent;
                Rectangle rectangle = new Rectangle(doc.PageSize);
                rectangle.Left += doc.LeftMargin;
                rectangle.Right -= doc.RightMargin;
                rectangle.Top -= doc.TopMargin;
                rectangle.Bottom += doc.BottomMargin;
                content.SetColorStroke(BaseColor.BLACK);
                content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                content.Stroke();

                //RenderHeader(mainTable);
                RenderFront(mainTable, data, content);
                RenderBack(backTable);
                
                doc.Add(mainTable);
                doc.NewPage();
                doc.Add(backTable);

                doc.Close();
                return ms.ToArray();
            }
        }

        private void RenderFront(PdfPTable mainTable, Driver data, PdfContentByte cb)
        {
            //picture
            PdfPCell imgCell = new PdfPCell();
            imgCell.Rowspan = 5;
            imgCell.FixedHeight = 132f;
            imgCell.Border = Rectangle.NO_BORDER;
            imgCell.BorderWidth = 1;
            imgCell.PaddingTop = 15;
            Image photo = Image.GetInstance(PrepareDriverPhoto(data), BaseColor.WHITE);
            photo.WidthPercentage = 100;
            imgCell.AddElement(photo);
            mainTable.AddCell(imgCell);

            PdfPCell Title = new PdfPCell();
            Title.Border = Rectangle.NO_BORDER;
            Title.PaddingTop = 15;
            Title.BorderWidth = 1;
            Paragraph tTitle = new Paragraph("KARTU IJIN MENGEMUDI", fontSubTitle);
            tTitle.SpacingBefore = 0;
            tTitle.SpacingAfter = 0;
            tTitle.Alignment = Element.ALIGN_CENTER;
            Title.AddElement(tTitle);
            mainTable.AddCell(Title);

            PdfPCell Company = new PdfPCell();
            Company.Border = Rectangle.NO_BORDER;
            Company.PaddingTop = 2;
            Paragraph tCompany = new Paragraph("PT. Redeco Petrolin Utama", fontTableHeader);
            tCompany.SpacingBefore = 0;
            tCompany.SpacingAfter = 0;
            tCompany.Alignment = Element.ALIGN_CENTER;
            Company.AddElement(tCompany);
            mainTable.AddCell(Company);

            //barcode
            Barcode128 code128 = new Barcode128();
            code128.CodeType = Barcode.CODE128;
            code128.Code = data.BadgeNumber;
            //code128.Font = null; //without text
            iTextSharp.text.Image image128 = code128.CreateImageWithBarcode(cb, null, null);
            image128.Border = Rectangle.NO_BORDER;
            image128.WidthPercentage = 75;
            image128.Alignment = Element.ALIGN_CENTER;
            PdfPCell barcodeCell = new PdfPCell();
            barcodeCell.Border = Rectangle.NO_BORDER;
            barcodeCell.BorderWidth = 1;
            barcodeCell.AddElement(image128);
            mainTable.AddCell(barcodeCell);

            //label expired
            PdfPCell LabelExpired = new PdfPCell();
            LabelExpired.Border = Rectangle.NO_BORDER;
            LabelExpired.Colspan = 1;
            LabelExpired.NoWrap = false;
            string expiryDate = data.DriverKimExpiryDate.HasValue ? data.DriverKimExpiryDate.Value.ToString("dd/MM/yyyy") : "";
            Paragraph tLabelExpired = new Paragraph(string.Format("Berlaku s/d :    {0}", expiryDate), fontTableHeader);
            tLabelExpired.SpacingBefore = 0;
            tLabelExpired.SpacingAfter = 0;
            tLabelExpired.Alignment = Element.ALIGN_LEFT;
            LabelExpired.AddElement(tLabelExpired);
            mainTable.AddCell(LabelExpired);

            //label name
            PdfPCell LabelName = new PdfPCell();
            LabelName.Border = Rectangle.NO_BORDER;
            LabelName.Colspan = 1;
            Paragraph tLabelName = new Paragraph(string.Format(data.Name.ToUpper()), fontTableHeader);
            tLabelName.SpacingBefore = 5;
            tLabelName.SpacingAfter = 5;
            tLabelName.Alignment = Element.ALIGN_LEFT;
            LabelName.AddElement(tLabelName);
            mainTable.AddCell(LabelName);
        }

        private void RenderBack(PdfPTable backTable)
        {
            PdfPCell LabelExpired = new PdfPCell();
            LabelExpired.Padding = 3;
            LabelExpired.Border = Rectangle.NO_BORDER;
            LabelExpired.NoWrap = false;
            Paragraph tLabelExpired = new Paragraph("Pengemudi Lorry harus membawa KIM (Kartu Ijin Mengemudi) yang diterbitkan oleh PT Redeco Petrolin Utama dan yang masih berlaku.", fontTableValue);
            tLabelExpired.SpacingBefore = 0;
            tLabelExpired.SpacingAfter = 0;
            tLabelExpired.Alignment = Element.ALIGN_JUSTIFIED;
            LabelExpired.AddElement(tLabelExpired);
            backTable.AddCell(LabelExpired);

            PdfPCell LabelExpired1 = new PdfPCell();
            LabelExpired1.Padding = 3;
            LabelExpired1.Border = Rectangle.NO_BORDER;
            LabelExpired1.NoWrap = false;
            Paragraph tLabelExpired1 = new Paragraph("Masa berlaku KIM 1(satu) tahun.", fontTableValue);
            tLabelExpired1.SpacingBefore = 0;
            tLabelExpired1.SpacingAfter = 0;
            tLabelExpired1.Alignment = Element.ALIGN_JUSTIFIED;
            LabelExpired1.AddElement(tLabelExpired1);
            backTable.AddCell(LabelExpired1);

            PdfPCell LabelExpired2 = new PdfPCell();
            LabelExpired2.Padding = 3;
            LabelExpired2.Border = Rectangle.NO_BORDER;
            LabelExpired2.NoWrap = false;
            Paragraph tLabelExpired2 = new Paragraph("Untuk aplikasi KIM baru, perpanjangan KIM dan bila KIM hilang, hubungi petugas di Loket-1, membawa SIM asli dan KTP yang masih berlaku serta Surat Pengantar dari Transporter / Perusahaan.", fontTableValue);
            tLabelExpired2.SpacingBefore = 0;
            tLabelExpired2.SpacingAfter = 0;
            tLabelExpired2.Alignment = Element.ALIGN_JUSTIFIED;
            LabelExpired2.AddElement(tLabelExpired2);
            backTable.AddCell(LabelExpired2);

            PdfPCell LabelExpired3 = new PdfPCell();
            LabelExpired3.Padding = 3;
            LabelExpired3.Border = Rectangle.NO_BORDER;
            LabelExpired3.NoWrap = false;
            Paragraph tLabelExpired3 = new Paragraph("Bila menemukan kartu ini, hubungi 0254 575 0074", fontTableValue);
            tLabelExpired3.SpacingBefore = 0;
            tLabelExpired3.SpacingAfter = 0;
            tLabelExpired3.Alignment = Element.ALIGN_JUSTIFIED;
            LabelExpired3.AddElement(tLabelExpired3);
            backTable.AddCell(LabelExpired3);
        }

        private System.Drawing.Image PrepareLogo()
        {
            string logoPath = HttpContext.Current.Server.MapPath("~/Images/RPU Logo.png");
            System.Drawing.Image imgLogo = System.Drawing.Image.FromFile(logoPath);
            return imgLogo;
        }

        private System.Drawing.Image PrepareDriverPhoto(Driver data)
        {
            string photoPath = HttpContext.Current.Server.MapPath(string.Format("~/Images/DriverPhoto/{0}.jpg", data.BadgeNumber));
            System.Drawing.Image imgLogo = System.Drawing.Image.FromFile(photoPath);
            return imgLogo;
        }

        //private void RenderBarcode(PdfPTable mainTable, Driver data, PdfContentByte cb)
        //{
        //    Barcode128 code128 = new Barcode128();
        //    code128.CodeType = Barcode.CODE128;
        //    code128.Code = data.BadgeNumber;
        //    iTextSharp.text.Image image128 = code128.CreateImageWithBarcode(cb, null, null);
        //    image128.Border = Rectangle.NO_BORDER;
        //    mainTable.AddCell(image128);
        //}
    }
}
