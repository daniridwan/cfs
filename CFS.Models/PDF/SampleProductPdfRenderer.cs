﻿using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;
using System.Globalization;

namespace CFS.Models.PDF
{
    public class SampleProductPdfRenderer
    {
        private Font fontTitle;
        private Font fontSubTitle;
        private Font fontSubTitleNormal;
        private Font fontSubTitleLink;
        private Font fontFieldLabel;
        private Font fontFieldMediumLabel;
        private Font fontFieldLargeLabel;
        private Font fontFieldValue;
        private Font fontFieldMediumValue;
        private Font fontFieldLargeValue;
        private Font fontTableHeader;
        private Font fontTableValue;
        private Font fontFooter;

        private const string formatDate = "MMM/dd/yyyy";
        private const string formatApprovalDate = "MMM/dd/yyyy HH:mm:ss";

        public SampleProductPdfRenderer()
        {
            fontTitle = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
            fontSubTitle = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
            fontFieldLabel = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
            fontFieldMediumLabel = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL);
            fontFieldLargeLabel = new Font(Font.FontFamily.HELVETICA, 16, Font.NORMAL);
            fontFieldValue = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
            fontFieldMediumValue = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
            fontFieldLargeValue = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
            fontTableHeader = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
            fontTableValue = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
            fontFooter = new Font(Font.FontFamily.HELVETICA, 6.7f, Font.NORMAL);
        }

        public byte[] Render(CFS.Models.TruckLoading data)
        {
            iTextSharp.text.Rectangle rect = new iTextSharp.text.Rectangle(260.0f, 140.0f);
            iTextSharp.text.Document doc = new iTextSharp.text.Document(rect);
            doc.SetMargins(8, 8, 8, 16);
            using (MemoryStream ms = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                //writer.PageEvent = new PDFFooter();
                doc.Open();
                doc.AddTitle(string.Format("Product Sample # {0}", data.DeliveryOrderNumber));
                doc.AddCreator("CFS");

                foreach(TruckLoadingPlanning plan in data.TruckLoadingPlanning) 
                {
                    //border document
                    PdfContentByte content = writer.DirectContent;
                    //Rectangle rectangle = new Rectangle(doc.PageSize);
                    //rectangle.Left += doc.LeftMargin;
                    //rectangle.Right -= doc.RightMargin;
                    //rectangle.Top -= doc.TopMargin;
                    //rectangle.Bottom += doc.BottomMargin;
                    //content.SetColorStroke(BaseColor.BLACK);
                    //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                    content.Stroke();

                    PdfPTable headerTable = new PdfPTable(3);
                    headerTable.SetWidths(new float[] { 0.5f, 1f, 1f });

                    PdfPTable contentTable = new PdfPTable(6);
                    contentTable.WidthPercentage = 100;
                    contentTable.SetWidths(new float[] { 0.9f, 0.1f ,1.9f, 0.6f, 0.1f, 1f });

                    RenderHeader(headerTable);
                    RenderContent(contentTable, data, plan, content);
                    doc.Add(headerTable);
                    doc.Add(contentTable);
                    doc.NewPage();
                }
                doc.Close();
                return ms.ToArray();
            }
        }

        private void RenderHeader(PdfPTable headerTable)
        {
            PdfPCell imgCell = new PdfPCell();
            imgCell.HorizontalAlignment = Element.ALIGN_CENTER;
            imgCell.Rowspan = 2;
            imgCell.PaddingTop = 2;
            imgCell.PaddingLeft = 3;
            imgCell.PaddingRight = 3;
            imgCell.Border = Rectangle.NO_BORDER;
            imgCell.BorderWidth = 0;
            Image logo = Image.GetInstance(PrepareLogo(), BaseColor.WHITE);
            logo.ScalePercent(30);
            imgCell.AddElement(logo);
            headerTable.AddCell(imgCell);

            PdfPCell ContentCompany = new PdfPCell();
            ContentCompany.Border = Rectangle.NO_BORDER;
            ContentCompany.Colspan = 3;
            ContentCompany.PaddingTop = 2;
            ContentCompany.VerticalAlignment = Element.ALIGN_MIDDLE;
            //header
            Paragraph tCompany = new Paragraph("PT. Redeco Petrolin Utama", fontSubTitle);
            tCompany.SpacingBefore = 0;
            tCompany.SpacingAfter = 0;
            tCompany.Alignment = Element.ALIGN_CENTER;
            ContentCompany.AddElement(tCompany);
            headerTable.AddCell(ContentCompany);

            PdfPCell ContentTitle = new PdfPCell();
            ContentTitle.Border = Rectangle.NO_BORDER;
            ContentTitle.Colspan = 3;
            ContentTitle.PaddingTop = 0;
            ContentTitle.VerticalAlignment = Element.ALIGN_MIDDLE;
            //header
            Paragraph tHeader = new Paragraph("SAMPLE PRODUK", fontTitle);
            tHeader.SpacingBefore = 0;
            tHeader.SpacingAfter = 0;
            tHeader.Alignment = Element.ALIGN_CENTER;
            ContentTitle.AddElement(tHeader);
            headerTable.AddCell(ContentTitle);
        }

        private void RenderContent(PdfPTable contentTable, TruckLoading data, TruckLoadingPlanning plan, PdfContentByte cb)
        {
            //baris 1
            PdfPCell ContentLabel1 = new PdfPCell();
            ContentLabel1.Border = Rectangle.NO_BORDER;
            Paragraph tLabel1 = new Paragraph("No Antrian", fontFieldLabel);
            tLabel1.SpacingBefore = 0;
            tLabel1.SpacingAfter = 0;
            tLabel1.Alignment = Element.ALIGN_LEFT;
            ContentLabel1.AddElement(tLabel1);
            contentTable.AddCell(ContentLabel1);

            //:
            PdfPCell ContentSeparator = new PdfPCell();
            ContentSeparator.Border = Rectangle.NO_BORDER;
            Paragraph tLabelSeparator = new Paragraph(":", fontFieldLabel);
            tLabelSeparator.SpacingBefore = 0;
            tLabelSeparator.SpacingAfter = 0;
            tLabelSeparator.Alignment = Element.ALIGN_LEFT;
            ContentSeparator.AddElement(tLabelSeparator);
            contentTable.AddCell(ContentSeparator);

            PdfPCell ContentQueue = new PdfPCell();
            ContentQueue.Border = Rectangle.NO_BORDER;
            ViewQueueTruck[] queue = ViewQueueTruck.FindByTruckLoadingId(data.TruckLoadingId);
            Paragraph tLabel = new Paragraph(queue[0].QueueNumber, fontFieldValue);
            tLabel.SpacingBefore = 0;
            tLabel.SpacingAfter = 0;
            tLabel.Alignment = Element.ALIGN_LEFT;
            ContentQueue.AddElement(tLabel);
            contentTable.AddCell(ContentQueue);

            PdfPCell ContentLabel2 = new PdfPCell();
            ContentLabel2.Border = Rectangle.NO_BORDER;
            Paragraph tLabel2 = new Paragraph("No Lajur", fontFieldLabel);
            tLabel2.SpacingBefore = 0;
            tLabel2.SpacingAfter = 0;
            tLabel2.Alignment = Element.ALIGN_LEFT;
            ContentLabel2.AddElement(tLabel2);
            contentTable.AddCell(ContentLabel2);

            contentTable.AddCell(ContentSeparator);

            PdfPCell ContentLane = new PdfPCell();
            ContentLane.Border = Rectangle.NO_BORDER;
            Paragraph tLabelLane = new Paragraph(plan.FillingPointName, fontFieldValue);
            tLabelLane.SpacingBefore = 0;
            tLabelLane.SpacingAfter = 0;
            tLabelLane.Alignment = Element.ALIGN_LEFT;
            ContentLane.AddElement(tLabelLane);
            contentTable.AddCell(ContentLane);

            PdfPCell ContentDate = new PdfPCell();
            ContentDate.Border = Rectangle.NO_BORDER;
            ContentDate.Colspan = 1;
            Paragraph tLabelDate = new Paragraph("Tanggal", fontFieldLabel);
            tLabelDate.SpacingBefore = 0;
            tLabelDate.SpacingAfter = 0;
            tLabelDate.Alignment = Element.ALIGN_LEFT;
            ContentDate.AddElement(tLabelDate);
            contentTable.AddCell(ContentDate);

            contentTable.AddCell(ContentSeparator);

            string administrationDate = data.AdministrationTimestamp.HasValue ? data.AdministrationTimestamp.Value.ToString("dd/MM/yyyy") : "";
            PdfPCell ContentDate1 = new PdfPCell();
            ContentDate1.Border = Rectangle.NO_BORDER;
            ContentDate1.Colspan = 5;
            Paragraph tLabelDate1 = new Paragraph(administrationDate, fontFieldValue);
            tLabelDate1.SpacingBefore = 0;
            tLabelDate1.SpacingAfter = 0;
            tLabelDate1.Alignment = Element.ALIGN_LEFT;
            ContentDate1.AddElement(tLabelDate1);
            contentTable.AddCell(ContentDate1);

            PdfPCell ContentMaterial = new PdfPCell();
            ContentMaterial.Border = Rectangle.NO_BORDER;
            ContentMaterial.VerticalAlignment = Element.ALIGN_LEFT;
            Paragraph tLabelMaterial = new Paragraph("Nama Barang", fontFieldLabel);
            tLabelMaterial.SpacingBefore = 0;
            tLabelMaterial.SpacingAfter = 0;
            tLabelMaterial.Alignment = Element.ALIGN_LEFT;
            ContentMaterial.AddElement(tLabelMaterial);
            contentTable.AddCell(ContentMaterial);

            contentTable.AddCell(ContentSeparator);

            PdfPCell ContentMaterial1 = new PdfPCell();
            ContentMaterial1.Border = Rectangle.NO_BORDER;
            ContentMaterial1.Colspan = 5;
            Paragraph tLabelMaterial1 = new Paragraph(plan.ProductName, fontFieldValue);
            tLabelMaterial1.SpacingBefore = 0;
            tLabelMaterial1.SpacingAfter = 0;
            tLabelMaterial1.Alignment = Element.ALIGN_LEFT;
            ContentMaterial1.AddElement(tLabelMaterial1);
            contentTable.AddCell(ContentMaterial1);

            PdfPCell ContentTruck = new PdfPCell();
            ContentTruck.Border = Rectangle.NO_BORDER;
            ContentTruck.VerticalAlignment = Element.ALIGN_LEFT;
            Paragraph tLabelTruck = new Paragraph("No. Truk", fontFieldLabel);
            tLabelTruck.SpacingBefore = 0;
            tLabelTruck.SpacingAfter = 0;
            tLabelTruck.Alignment = Element.ALIGN_LEFT;
            ContentTruck.AddElement(tLabelTruck);
            contentTable.AddCell(ContentTruck);

            contentTable.AddCell(ContentSeparator);

            PdfPCell ContentTruck1 = new PdfPCell();
            ContentTruck1.Border = Rectangle.NO_BORDER;
            ContentTruck1.Colspan = 5;
            Paragraph tLabelTruck1 = new Paragraph(data.TruckRegistrationPlate, fontFieldValue);
            tLabelTruck1.SpacingBefore = 0;
            tLabelTruck1.SpacingAfter = 0;
            tLabelTruck1.Alignment = Element.ALIGN_LEFT;
            ContentTruck1.AddElement(tLabelTruck1);
            contentTable.AddCell(ContentTruck1);

            PdfPCell ContentNotes = new PdfPCell();
            ContentNotes.Border = Rectangle.NO_BORDER;
            ContentNotes.VerticalAlignment = Element.ALIGN_LEFT;
            ContentNotes.Colspan = 3;
            Paragraph tLabelNotes = new Paragraph("Untuk informasi lengkap harap scan BARCODE", fontFieldLabel);
            tLabelNotes.SpacingBefore = 0;
            tLabelNotes.SpacingAfter = 0;
            tLabelNotes.Alignment = Element.ALIGN_LEFT;
            ContentNotes.AddElement(tLabelNotes);
            contentTable.AddCell(ContentNotes);

            //barcode
            Barcode128 code128 = new Barcode128();
            code128.CodeType = Barcode.CODE128;
            code128.Code = plan.DeliveryOrderNumber.ToString();
            //code128.Font = null; //without text
            iTextSharp.text.Image image128 = code128.CreateImageWithBarcode(cb, null, null);
            image128.Border = Rectangle.NO_BORDER;
            image128.WidthPercentage = 100;
            //image128.ScaleAbsolute(100f, 100f);
            PdfPCell barcodeCell = new PdfPCell();
            barcodeCell.Colspan = 3;
            barcodeCell.Border = Rectangle.NO_BORDER;
            barcodeCell.BorderWidth = 1;
            barcodeCell.AddElement(image128);
            contentTable.AddCell(barcodeCell);
        }

        private System.Drawing.Image PrepareLogo()
        {
            string logoPath = HttpContext.Current.Server.MapPath("~/Images/RPU Logo.png");
            System.Drawing.Image imgLogo = System.Drawing.Image.FromFile(logoPath);
            return imgLogo;
        }
    }
}
