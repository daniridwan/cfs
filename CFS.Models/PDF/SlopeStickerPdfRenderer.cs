﻿using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;
using System.Globalization;

namespace CFS.Models.PDF
{
    public class SlopeStickerPdfRenderer
    {
        private Font fontTitle;
        private Font fontSubTitle;
        private Font fontFieldLabel;
        private Font fontFieldMediumLabel;
        private Font fontFieldLargeLabel;
        private Font fontFieldValue;
        private Font fontFieldMediumValue;
        private Font fontFieldLargeValue;
        private Font fontTableHeader;
        private Font fontTableValue;
        private Font fontFooter;

        private const string formatDate = "MMM/dd/yyyy";
        private const string formatApprovalDate = "MMM/dd/yyyy HH:mm:ss";

        public SlopeStickerPdfRenderer()
        {
            fontTitle = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
            fontSubTitle = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
            fontFieldLabel = new Font(Font.FontFamily.HELVETICA, 6.5f, Font.NORMAL);
            fontFieldMediumLabel = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL);
            fontFieldLargeLabel = new Font(Font.FontFamily.HELVETICA, 16, Font.NORMAL);
            fontFieldValue = new Font(Font.FontFamily.HELVETICA, 6.2f, Font.BOLD);
            fontFieldMediumValue = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
            fontFieldLargeValue = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
            fontTableHeader = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
            fontTableValue = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
            fontFooter = new Font(Font.FontFamily.HELVETICA, 4, Font.NORMAL);
        }

        public byte[] Render()
        {
            iTextSharp.text.Rectangle rect = new iTextSharp.text.Rectangle(260.0f, 335.0f); //270, 350
            iTextSharp.text.Document doc = new iTextSharp.text.Document(rect);
            doc.SetMargins(8, 8, 8, 16);
            using (MemoryStream ms = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                writer.PageEvent = new PDFFooter();
                doc.Open();
                doc.AddTitle("Sticker Slope");
                doc.AddCreator("CFS");

                PdfContentByte content = writer.DirectContent;
                content.Stroke();

                PdfPTable mainTable = new PdfPTable(2); // 7 kolom
                mainTable.SpacingBefore = 2;
                mainTable.WidthPercentage = 100;
                mainTable.SetWidths(new float[] { 0.75f, 0.25f });

                RenderTable(mainTable);

                doc.Add(mainTable);
                doc.Close();
                return ms.ToArray();
            }
        }

        private void RenderTable(PdfPTable mainTable)
        {
            PdfPCell ContentTitle = new PdfPCell();
            ContentTitle.Border = Rectangle.NO_BORDER;
            ContentTitle.Colspan = 2;
            ContentTitle.PaddingTop = 0;
            //header
            Paragraph tHeader = new Paragraph("Marking / Labelling Drum", fontTitle);
            tHeader.SpacingBefore = 0;
            tHeader.SpacingAfter = 0;
            tHeader.Alignment = Element.ALIGN_CENTER;
            ContentTitle.AddElement(tHeader);
            mainTable.AddCell(ContentTitle);

            //Date
            PdfPCell ContentDate = new PdfPCell();
            ContentDate.Border = Rectangle.NO_BORDER;
            ContentDate.PaddingTop = 0;
            Paragraph tDate = new Paragraph("Tanggal :", fontTitle);
            tDate.SpacingBefore = 0;
            tDate.SpacingAfter = 0;
            tDate.Alignment = Element.ALIGN_CENTER;
            ContentDate.AddElement(tDate);
            mainTable.AddCell(ContentDate);

            PdfPCell EmptyCell = new PdfPCell();
            mainTable.AddCell(EmptyCell);

            PdfPCell ContentProduct = new PdfPCell();
            ContentProduct.Border = Rectangle.NO_BORDER;
            ContentProduct.PaddingTop = 0;
            Paragraph tProduct = new Paragraph("Produk :", fontTitle);
            tProduct.SpacingBefore = 0;
            tProduct.SpacingAfter = 0;
            tProduct.Alignment = Element.ALIGN_CENTER;
            ContentProduct.AddElement(tProduct);
            mainTable.AddCell(ContentProduct);

            mainTable.AddCell(EmptyCell);

            //customer
            PdfPCell ContentCustomer = new PdfPCell();
            ContentCustomer.Border = Rectangle.NO_BORDER;
            ContentCustomer.PaddingTop = 0;
            Paragraph tCustomer = new Paragraph("Customer :", fontTitle);
            tCustomer.SpacingBefore = 0;
            tCustomer.SpacingAfter = 0;
            tCustomer.Alignment = Element.ALIGN_CENTER;
            ContentCustomer.AddElement(tCustomer);
            mainTable.AddCell(ContentCustomer);

            mainTable.AddCell(EmptyCell);

            //tipe
            PdfPCell ContentType = new PdfPCell();
            ContentType.Border = Rectangle.NO_BORDER;
            ContentType.PaddingTop = 0;
            Paragraph tType = new Paragraph("Tipe :", fontTitle);
            tType.SpacingBefore = 0;
            tType.SpacingAfter = 0;
            tType.Alignment = Element.ALIGN_CENTER;
            ContentType.AddElement(tType);
            mainTable.AddCell(ContentType);

            mainTable.AddCell(EmptyCell);
        }
    }
}
