﻿using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;
using System.Globalization;

namespace CFS.Models.PDF
{
    public class LoadingInfoPdfRenderer
    {
        private Font fontTitle;
        private Font fontSubTitle;
        private Font fontFieldLabel;
        private Font fontFieldMediumLabel;
        private Font fontFieldLargeLabel;
        private Font fontFieldValue;
        private Font fontFieldMediumValue;
        private Font fontFieldLargeValue;
        private Font fontTableHeader;
        private Font fontTableValue;
        private Font fontFooter;

        private const string formatDate = "MMM/dd/yyyy";
        private const string formatApprovalDate = "MMM/dd/yyyy HH:mm:ss";

        private string mode = "";
        private string uom = "";

        public LoadingInfoPdfRenderer()
        {
            fontTitle = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
            fontSubTitle = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
            fontFieldLabel = new Font(Font.FontFamily.HELVETICA, 6.5f, Font.NORMAL);
            fontFieldMediumLabel = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL);
            fontFieldLargeLabel = new Font(Font.FontFamily.HELVETICA, 16, Font.NORMAL);
            fontFieldValue = new Font(Font.FontFamily.HELVETICA, 6.2f, Font.BOLD);
            fontFieldMediumValue = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
            fontFieldLargeValue = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
            fontTableHeader = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
            fontTableValue = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
            fontFooter = new Font(Font.FontFamily.HELVETICA, 4, Font.NORMAL);
        }

        public byte[] Render(CFS.Models.TruckLoading data)
        {
            iTextSharp.text.Rectangle rect = new iTextSharp.text.Rectangle(260.0f, 335.0f); //270, 350
            iTextSharp.text.Document doc = new iTextSharp.text.Document(rect);
            doc.SetMargins(8, 8, 8, 16);
            using (MemoryStream ms = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                writer.PageEvent = new PDFFooter();
                doc.Open();
                doc.AddTitle(string.Format("Sticker Pendaftaran # {0}", data.TruckRegistrationPlate));
                doc.AddCreator("CFS");

                //get mode & uom
                if (data.TruckLoadingPlanning[0].FillingPointGroup == "Chemical") //get first item to determine chemical / fuel
                {
                    mode = "Timbang";
                    uom = "Kg";
                }
                else
                {
                    mode = "Flow Meter";
                    uom = "KL";
                }

                int total = 0;
                if(mode == "Timbang")
                {
                    total = data.TruckLoadingPlanning.Count + 2;
                }
                else
                {
                    total = data.TruckLoadingPlanning.Count + 1;
                }
                
                for (int i=0;i< total; i++) { 
                    //border document
                    PdfContentByte content = writer.DirectContent;
                    //Rectangle rectangle = new Rectangle(doc.PageSize);
                    //rectangle.Left += doc.LeftMargin;
                    //rectangle.Right -= doc.RightMargin;
                    //rectangle.Top -= doc.TopMargin;
                    //rectangle.Bottom += doc.BottomMargin;
                    //content.SetColorStroke(BaseColor.BLACK);
                    //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                    content.Stroke();

                    PdfPTable mainTable = new PdfPTable(7); // 7 kolom
                    mainTable.SpacingBefore = 2;
                    mainTable.WidthPercentage = 100;
                    mainTable.SetWidths(new float[] { 0.79f, 0.1f, 1.35f, 0.65f, 0.81f, 0.1f, 1.04f });

                    PdfPTable contentTable = new PdfPTable(6);
                    contentTable.SpacingBefore = 2;
                    contentTable.WidthPercentage = 100;
                    contentTable.SetWidths(new float[] { 0.30f, 1.7f, 0.75f, 0.7f, 0.57f, 1.25f });

                    PdfPTable footerTable = new PdfPTable(6);
                    footerTable.SpacingBefore = 2;
                    footerTable.WidthPercentage = 100;
                    footerTable.SetWidths(new float[] { 0.8f, 0.95f, 0.9f, 1f, 0.5f, 1.4f });

                    PdfPTable signatureTable = new PdfPTable(3);
                    signatureTable.SpacingBefore = 2;
                    signatureTable.WidthPercentage = 100;

                    RenderHeader(mainTable);
                    RenderTableHeader(mainTable, data, content);
                    RenderTableContent(contentTable, data, content);
                    RenderTableFooter(footerTable, data);
                    RenderTableSignature(signatureTable);

                    doc.Add(mainTable);
                    doc.Add(contentTable);
                    doc.Add(footerTable);
                    doc.Add(signatureTable);
                    doc.NewPage();
                }
                doc.Close();
                return ms.ToArray();
            }
        }

        private void RenderHeader(PdfPTable mainTable)
        {
            //logo
            PdfPCell imgCell = new PdfPCell();
            imgCell.Rowspan = 2;
            imgCell.Colspan = 1;
            imgCell.PaddingLeft = 5;
            imgCell.PaddingRight = 5;
            imgCell.Border = Rectangle.NO_BORDER;
            imgCell.BorderWidth = 0;
            Image logo = Image.GetInstance(PrepareLogo(), BaseColor.WHITE);
            logo.ScalePercent(25);
            imgCell.AddElement(logo);
            mainTable.AddCell(imgCell);

            PdfPCell ContentCompany = new PdfPCell();
            ContentCompany.Border = Rectangle.NO_BORDER;
            ContentCompany.Colspan = 6;
            ContentCompany.PaddingTop = 0;
            //header
            Paragraph tCompany = new Paragraph("PT. Redeco Petrolin Utama", fontSubTitle);
            tCompany.SpacingBefore = 0;
            tCompany.SpacingAfter = 0;
            tCompany.Alignment = Element.ALIGN_CENTER;
            ContentCompany.AddElement(tCompany);
            mainTable.AddCell(ContentCompany);

            PdfPCell ContentTitle = new PdfPCell();
            ContentTitle.Border = Rectangle.NO_BORDER;
            ContentTitle.Colspan = 6;
            ContentTitle.PaddingTop = 0;
            //header
            Paragraph tHeader = new Paragraph("STICKER PENDAFTARAN", fontTitle);
            tHeader.SpacingBefore = 0;
            tHeader.SpacingAfter = 0;
            tHeader.Alignment = Element.ALIGN_CENTER;
            ContentTitle.AddElement(tHeader);
            mainTable.AddCell(ContentTitle);
        }

        private void RenderTableHeader(PdfPTable mainTable, TruckLoading data, PdfContentByte cb)
        {
            //baris 1
            PdfPCell ContentLabel1 = new PdfPCell();
            ContentLabel1.Border = Rectangle.NO_BORDER;
            Paragraph tLabel1 = new Paragraph("No Antrian", fontFieldLabel);
            tLabel1.SpacingBefore = 0;
            tLabel1.SpacingAfter = 0;
            tLabel1.Alignment = Element.ALIGN_LEFT;
            ContentLabel1.AddElement(tLabel1);
            mainTable.AddCell(ContentLabel1);

            //:
            PdfPCell ContentSeparator = new PdfPCell();
            ContentSeparator.Border = Rectangle.NO_BORDER;
            Paragraph tLabelSeparator = new Paragraph(":", fontFieldLabel);
            tLabelSeparator.SpacingBefore = 0;
            tLabelSeparator.SpacingAfter = 0;
            tLabelSeparator.Alignment = Element.ALIGN_LEFT;
            ContentSeparator.AddElement(tLabelSeparator);
            mainTable.AddCell(ContentSeparator);

            PdfPCell ContentQueue = new PdfPCell();
            ContentQueue.Border = Rectangle.NO_BORDER;
            ViewQueueTruck[] queue = ViewQueueTruck.FindByTruckLoadingId(data.TruckLoadingId);
            Paragraph tLabel = new Paragraph(queue[0].QueueNumber, fontFieldValue);
            tLabel.SpacingBefore = 0;
            tLabel.SpacingAfter = 0;
            tLabel.Alignment = Element.ALIGN_LEFT;
            ContentQueue.AddElement(tLabel);
            mainTable.AddCell(ContentQueue);

            //barcode
            Barcode128 code128 = new Barcode128();
            code128.CodeType = Barcode.CODE128;
            code128.Code = data.DeliveryOrderNumber.ToString();
            //code128.Font = null; //without text
            iTextSharp.text.Image image128 = code128.CreateImageWithBarcode(cb, null, null);
            image128.Border = Rectangle.NO_BORDER;
            image128.WidthPercentage = 100;
            image128.ScaleAbsoluteHeight(10f);
            image128.Alignment = Element.ALIGN_CENTER;
            PdfPCell barcodeCell = new PdfPCell();
            barcodeCell.Rowspan = 3;
            barcodeCell.Colspan = 4;
            barcodeCell.Border = Rectangle.NO_BORDER;
            barcodeCell.BorderWidth = 1;
            barcodeCell.AddElement(image128);
            mainTable.AddCell(barcodeCell);

            //baris 2
            PdfPCell ContentLabel2 = new PdfPCell();
            ContentLabel2.Border = Rectangle.NO_BORDER;
            Paragraph tLabel2 = new Paragraph("No Lajur", fontFieldLabel);
            tLabel2.SpacingBefore = 0;
            tLabel2.SpacingAfter = 0;
            tLabel2.Alignment = Element.ALIGN_LEFT;
            ContentLabel2.AddElement(tLabel2);
            mainTable.AddCell(ContentLabel2);

            //:
            mainTable.AddCell(ContentSeparator);

            PdfPCell ContentLane = new PdfPCell();
            ContentLane.Border = Rectangle.NO_BORDER;
            Paragraph tLabelLane = new Paragraph(data.FillingPointString, fontFieldValue);
            tLabelLane.SpacingBefore = 0;
            tLabelLane.SpacingAfter = 0;
            tLabelLane.Alignment = Element.ALIGN_LEFT;
            ContentLane.AddElement(tLabelLane);
            mainTable.AddCell(ContentLane);

            //baris 3
            PdfPCell ContentLabel3 = new PdfPCell();
            ContentLabel3.Border = Rectangle.NO_BORDER;
            Paragraph tLabel3 = new Paragraph("Pengemudi", fontFieldLabel);
            tLabel3.SpacingBefore = 0;
            tLabel3.SpacingAfter = 0;
            tLabel3.Alignment = Element.ALIGN_LEFT;
            ContentLabel3.AddElement(tLabel3);
            mainTable.AddCell(ContentLabel3);

            mainTable.AddCell(ContentSeparator);

            PdfPCell ContentDriver = new PdfPCell();
            ContentDriver.Border = Rectangle.NO_BORDER;
            //ContentDriver.Colspan = 5;
            //get driver
            Driver d = Driver.FindById(data.DriverId);
            Paragraph tLabelDriver = new Paragraph(d.Name.ToUpper(), fontFieldValue);
            tLabelDriver.SpacingBefore = 0;
            tLabelDriver.SpacingAfter = 0;
            tLabelDriver.Alignment = Element.ALIGN_LEFT;
            ContentDriver.AddElement(tLabelDriver);
            mainTable.AddCell(ContentDriver);

            //baris 4
            PdfPCell ContentLabel4 = new PdfPCell();
            ContentLabel4.Border = Rectangle.NO_BORDER;
            Paragraph tLabel4 = new Paragraph("Transporter", fontFieldLabel);
            tLabel4.SpacingBefore = 0;
            tLabel4.SpacingAfter = 0;
            tLabel4.Alignment = Element.ALIGN_LEFT;
            ContentLabel4.AddElement(tLabel4);
            mainTable.AddCell(ContentLabel4);

            mainTable.AddCell(ContentSeparator);

            PdfPCell ContentTransporter = new PdfPCell();
            ContentTransporter.Border = Rectangle.NO_BORDER;
            ContentTransporter.Colspan = 5;
            Truck t = Truck.FindByRegistrationPlate(data.TruckRegistrationPlate);
            Paragraph tLabelTransporter = new Paragraph(t.TruckTransporter, fontFieldValue);
            tLabelTransporter.SpacingBefore = 0;
            tLabelTransporter.SpacingAfter = 0;
            tLabelTransporter.Alignment = Element.ALIGN_LEFT;
            ContentTransporter.AddElement(tLabelTransporter);
            mainTable.AddCell(ContentTransporter);

            //baris 4
            PdfPCell ContentLabel5 = new PdfPCell();
            ContentLabel5.Border = Rectangle.NO_BORDER;
            ContentLabel5.Colspan = 1;
            Paragraph tLabel5 = new Paragraph("Pelanggan", fontFieldLabel);
            tLabel5.SpacingBefore = 0;
            tLabel5.SpacingAfter = 0;
            tLabel5.Alignment = Element.ALIGN_LEFT;
            ContentLabel5.AddElement(tLabel5);
            mainTable.AddCell(ContentLabel5);

            mainTable.AddCell(ContentSeparator);

            PdfPCell ContentCustomer = new PdfPCell();
            ContentCustomer.Border = Rectangle.NO_BORDER;
            ContentCustomer.Colspan = 2;
            //get customer
            DeliveryOrder c = DeliveryOrder.FindByShipmentNumber(data.DeliveryOrderNumber); 
            Paragraph tLabelCustomer = new Paragraph(c.CompanyName, fontFieldValue);
            tLabelCustomer.SpacingBefore = 0;
            tLabelCustomer.SpacingAfter = 0;
            tLabelCustomer.Alignment = Element.ALIGN_LEFT;
            ContentCustomer.AddElement(tLabelCustomer);
            mainTable.AddCell(ContentCustomer);

            PdfPCell ContentLabel6 = new PdfPCell();
            ContentLabel6.Border = Rectangle.NO_BORDER;
            ContentLabel6.Colspan = 1;
            Paragraph tLabel6 = new Paragraph("No Polisi", fontFieldLabel);
            tLabel6.SpacingBefore = 0;
            tLabel6.SpacingAfter = 0;
            tLabel6.Alignment = Element.ALIGN_LEFT;
            ContentLabel6.AddElement(tLabel6);
            mainTable.AddCell(ContentLabel6);

            mainTable.AddCell(ContentSeparator);

            PdfPCell ContentRegPlate = new PdfPCell();
            ContentRegPlate.Border = Rectangle.NO_BORDER;
            Paragraph tRegPlate = new Paragraph(data.TruckRegistrationPlate, fontFieldValue);
            tRegPlate.SpacingBefore = 0;
            tRegPlate.SpacingAfter = 0;
            tRegPlate.Alignment = Element.ALIGN_LEFT;
            ContentRegPlate.AddElement(tRegPlate);
            mainTable.AddCell(ContentRegPlate);

            //baris 5
            //PdfPCell ContentLabel7 = new PdfPCell();
            //ContentLabel7.Border = Rectangle.NO_BORDER;
            //ContentLabel7.Colspan = 1;
            //Paragraph tLabel7 = new Paragraph("Tujuan", fontFieldLabel);
            //tLabel7.SpacingBefore = 0;
            //tLabel7.SpacingAfter = 0;
            //tLabel7.Alignment = Element.ALIGN_LEFT;
            //ContentLabel7.AddElement(tLabel7);
            //mainTable.AddCell(ContentLabel7);

            //mainTable.AddCell(ContentSeparator);

            //PdfPCell ContentConsignee = new PdfPCell();
            //ContentConsignee.Border = Rectangle.NO_BORDER;
            //ContentConsignee.Colspan = 5;
            ////get consignee
            //DeliveryOrder dO = DeliveryOrder.FindByShipmentNumber(data.DeliveryOrderNumber);
            //Paragraph tLabelConsignee = new Paragraph(dO.ConsigneeString, fontFieldValue);
            //tLabelConsignee.SpacingBefore = 0;
            //tLabelConsignee.SpacingAfter = 0;
            //tLabelConsignee.Alignment = Element.ALIGN_LEFT;
            //ContentConsignee.AddElement(tLabelConsignee);
            //mainTable.AddCell(ContentConsignee);

            //baris mode
            PdfPCell ContentSpecialReq = new PdfPCell();
            ContentSpecialReq.Border = Rectangle.NO_BORDER;
            Paragraph tSpecialReq = new Paragraph("Mode", fontFieldLabel);
            tSpecialReq.Alignment = Element.ALIGN_LEFT;
            ContentSpecialReq.AddElement(tSpecialReq);
            mainTable.AddCell(ContentSpecialReq);

            mainTable.AddCell(ContentSeparator);

            PdfPCell ContentModeValue = new PdfPCell();
            ContentModeValue.Border = Rectangle.NO_BORDER;
            ContentModeValue.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentModeValue.Colspan = 2;
            Paragraph tLabelModeValue = new Paragraph(mode, fontFieldValue);
            tLabelModeValue.Alignment = Element.ALIGN_LEFT;
            ContentModeValue.AddElement(tLabelModeValue);
            mainTable.AddCell(ContentModeValue);

            PdfPCell ContentUOM = new PdfPCell();
            ContentUOM.Border = Rectangle.NO_BORDER;
            ContentUOM.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tUOM = new Paragraph("Satuan", fontFieldLabel);
            tUOM.Alignment = Element.ALIGN_LEFT;
            ContentUOM.AddElement(tUOM);
            mainTable.AddCell(ContentUOM);

            mainTable.AddCell(ContentSeparator);

            PdfPCell ContentUOMValue = new PdfPCell();
            ContentUOMValue.Border = Rectangle.NO_BORDER;
            ContentUOMValue.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tValueUOM = new Paragraph(uom, fontFieldValue);
            tValueUOM.Alignment = Element.ALIGN_LEFT;
            ContentUOMValue.AddElement(tValueUOM);
            mainTable.AddCell(ContentUOMValue);
        }

        private void RenderTableContent(PdfPTable contentTable, TruckLoading data, PdfContentByte cb)
        {
            //table header
            PdfPCell ContentComp = new PdfPCell();
            ContentComp.Border = Rectangle.BOX;
            ContentComp.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tLabelComp = new Paragraph("No", fontFieldValue);
            tLabelComp.SpacingBefore = 0;
            tLabelComp.SpacingAfter = 0;
            tLabelComp.Alignment = Element.ALIGN_CENTER;
            ContentComp.AddElement(tLabelComp);
            contentTable.AddCell(ContentComp);

            PdfPCell ContentMaterial = new PdfPCell();
            ContentMaterial.Border = Rectangle.BOX;
            ContentMaterial.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tLabelMaterial = new Paragraph("Nama Barang", fontFieldValue);
            tLabelMaterial.SpacingBefore = 0;
            tLabelMaterial.SpacingAfter = 0;
            tLabelMaterial.Alignment = Element.ALIGN_CENTER;
            ContentMaterial.AddElement(tLabelMaterial);
            contentTable.AddCell(ContentMaterial);

            PdfPCell ContentLane = new PdfPCell();
            ContentLane.Border = Rectangle.BOX;
            ContentLane.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tLabelLane = new Paragraph("No. Lajur", fontFieldValue);
            tLabelLane.SpacingBefore = 0;
            tLabelLane.SpacingAfter = 0;
            tLabelLane.Alignment = Element.ALIGN_CENTER;
            ContentLane.AddElement(tLabelLane);
            contentTable.AddCell(ContentLane);

            PdfPCell ContentSource = new PdfPCell();
            ContentSource.Border = Rectangle.BOX;
            ContentSource.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tLabelSource = new Paragraph("Sumber Tanki", fontFieldValue);
            tLabelSource.SpacingBefore = 0;
            tLabelSource.SpacingAfter = 0;
            tLabelSource.Alignment = Element.ALIGN_CENTER;
            ContentSource.AddElement(tLabelSource);
            contentTable.AddCell(ContentSource);

            PdfPCell ContentQuantity = new PdfPCell();
            ContentQuantity.Border = Rectangle.BOX;
            ContentQuantity.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tLabelQuantity = new Paragraph(string.Format("Order ({0})", uom), fontFieldValue);
            tLabelQuantity.SpacingBefore = 0;
            tLabelQuantity.SpacingAfter = 0;
            tLabelQuantity.Alignment = Element.ALIGN_CENTER;
            ContentQuantity.AddElement(tLabelQuantity);
            contentTable.AddCell(ContentQuantity);

            PdfPCell ContentActual = new PdfPCell();
            ContentActual.Border = Rectangle.BOX;
            ContentActual.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tLabelActual = new Paragraph();
            if(mode == "Timbang")
            {
                tLabelActual = new Paragraph("Barcode", fontFieldValue);
            }
            else
            {
                tLabelActual = new Paragraph(string.Format("Aktual ({0})", uom), fontFieldValue);
            }
            tLabelActual.SpacingBefore = 0;
            tLabelActual.SpacingAfter = 0;
            tLabelActual.Alignment = Element.ALIGN_CENTER;
            ContentActual.AddElement(tLabelActual);
            contentTable.AddCell(ContentActual);

            //isi table
            //foreach(TruckLoadingPlanning plan in data.TruckLoadingPlanning) //replaced to max 5 product

            for(int i = 1; i<=8; i++)
            {
                PdfPCell ContentComp1 = new PdfPCell();
                ContentComp1.Border = Rectangle.BOX;
                Paragraph tLabelComp1 = new Paragraph(i.ToString(), fontFieldValue);
                tLabelComp1.SpacingBefore = 0;
                tLabelComp1.SpacingAfter = 0;
                tLabelComp1.Alignment = Element.ALIGN_LEFT;
                ContentComp1.AddElement(tLabelComp1);
                ContentComp1.FixedHeight = 15f; //apply to all row 
                contentTable.AddCell(ContentComp1);

                PdfPCell ContentMaterial1 = new PdfPCell();
                ContentMaterial1.Border = Rectangle.BOX;
                Paragraph tLabelMaterial1 = new Paragraph();
                if (data.TruckLoadingPlanning.Count >= i)
                {
                    tLabelMaterial1 = new Paragraph(data.TruckLoadingPlanning[i-1].ProductName, fontFieldValue);
                }
                else
                {
                    tLabelMaterial1 = new Paragraph("", fontFieldValue);
                }
                tLabelMaterial1.SpacingBefore = 0;
                tLabelMaterial1.SpacingAfter = 0;
                tLabelMaterial1.Alignment = Element.ALIGN_LEFT;
                ContentMaterial1.AddElement(tLabelMaterial1);
                contentTable.AddCell(ContentMaterial1);

                PdfPCell ContentLane1 = new PdfPCell();
                ContentLane1.Border = Rectangle.BOX;
                Paragraph tLabelLane1 = new Paragraph();
                if (data.TruckLoadingPlanning.Count >= i)
                {
                    tLabelLane1 = new Paragraph(data.TruckLoadingPlanning[i-1].FillingPointName, fontFieldValue);
                }
                else
                {
                    tLabelLane1 = new Paragraph("", fontFieldValue);
                }
                tLabelLane1.SpacingBefore = 0;
                tLabelLane1.SpacingAfter = 0;
                tLabelLane1.Alignment = Element.ALIGN_LEFT;
                ContentLane1.AddElement(tLabelLane1);
                contentTable.AddCell(ContentLane1);

                PdfPCell ContentSource1 = new PdfPCell();
                ContentSource1.Border = Rectangle.BOX;
                Paragraph tLabelSource1 = new Paragraph();
                if (data.TruckLoadingPlanning.Count >= i)
                {
                    tLabelSource1 = new Paragraph(data.TruckLoadingPlanning[i-1].TankName, fontFieldValue);
                }
                else
                {
                    tLabelSource1 = new Paragraph("", fontFieldValue);

                }
                tLabelSource1.SpacingBefore = 0;
                tLabelSource1.SpacingAfter = 0;
                tLabelSource1.Alignment = Element.ALIGN_LEFT;
                ContentSource1.AddElement(tLabelSource1);
                contentTable.AddCell(ContentSource1);

                PdfPCell ContentQuantity1 = new PdfPCell();
                ContentQuantity1.Border = Rectangle.BOX;
                Paragraph tLabelQuantity1 = new Paragraph();
                if (data.TruckLoadingPlanning.Count >= i)
                {
                    tLabelQuantity1 = new Paragraph(data.TruckLoadingPlanning[i-1].ConfirmedQuantity.ToString("0.#"), fontFieldValue);
                }
                else
                {
                    tLabelQuantity1 = new Paragraph("", fontFieldValue);
                }
                tLabelQuantity1.SpacingBefore = 0;
                tLabelQuantity1.SpacingAfter = 0;
                tLabelQuantity1.Alignment = Element.ALIGN_LEFT;
                ContentQuantity1.AddElement(tLabelQuantity1);
                contentTable.AddCell(ContentQuantity1);

                if(mode == "Timbang") 
                {
                    if (data.TruckLoadingPlanning.Count >= i)
                    {
                        //barcode
                        Barcode128 code128 = new Barcode128();
                        code128.CodeType = Barcode.CODE128;
                        code128.Code = data.TruckLoadingPlanning[i-1].DeliveryOrderNumber.ToString();
                        code128.Font = null; //without text
                        iTextSharp.text.Image image128 = code128.CreateImageWithBarcode(cb, null, null);
                        image128.Border = Rectangle.NO_BORDER;
                        image128.WidthPercentage = 100;
                        image128.Alignment = Element.ALIGN_CENTER;
                        PdfPCell barcodeCell = new PdfPCell();
                        barcodeCell.Border = Rectangle.RECTANGLE;
                        barcodeCell.BorderWidth = 1;
                        barcodeCell.AddElement(image128);
                        contentTable.AddCell(barcodeCell);
                    }
                    else //empty cell
                    {
                        PdfPCell EmptyCell = new PdfPCell();
                        contentTable.AddCell(EmptyCell);
                    }
                }
                else 
                { 
                    PdfPCell ContentActual1 = new PdfPCell();
                    ContentActual1.Border = Rectangle.BOX;
                    Paragraph tLabelActual1 = new Paragraph("", fontFieldValue);
                    tLabelActual1.SpacingBefore = 0;
                    tLabelActual1.SpacingAfter = 0;
                    tLabelActual1.Alignment = Element.ALIGN_LEFT;
                    ContentActual1.AddElement(tLabelActual1);
                    contentTable.AddCell(ContentActual1);
                }
            }
        }

        private void RenderTableFooter(PdfPTable footerTable, TruckLoading data)
        {
            PdfPCell ContentNotes = new PdfPCell();
            ContentNotes.Border = Rectangle.NO_BORDER;
            ContentNotes.Colspan = 6;
            Paragraph tLabelNotes = new Paragraph("Telah melihat fisik, visual dan menyaksikan pengambilan sampel produk.", fontFieldLabel);
            tLabelNotes.SpacingBefore = 0;
            tLabelNotes.SpacingAfter = 0;
            tLabelNotes.Alignment = Element.ALIGN_LEFT;
            ContentNotes.AddElement(tLabelNotes);
            footerTable.AddCell(ContentNotes);

            //baris 2
            PdfPCell ContentDate = new PdfPCell();
            ContentDate.Border = Rectangle.NO_BORDER;
            ContentDate.Colspan = 1;
            Paragraph tLabelDate = new Paragraph("Tanggal :", fontFieldLabel);
            tLabelDate.SpacingBefore = 0;
            tLabelDate.SpacingAfter = 0;
            tLabelDate.Alignment = Element.ALIGN_LEFT;
            ContentDate.AddElement(tLabelDate);
            footerTable.AddCell(ContentDate);

            string administrationDate = data.AdministrationTimestamp.HasValue ? data.AdministrationTimestamp.Value.ToString("dd/MM/yyyy") : "";

            PdfPCell ContentDate1 = new PdfPCell();
            ContentDate1.Border = Rectangle.NO_BORDER;
            ContentDate1.Colspan = 1;
            Paragraph tLabelDate1 = new Paragraph(administrationDate, fontFieldValue);
            tLabelDate1.SpacingBefore = 0;
            tLabelDate1.SpacingAfter = 0;
            tLabelDate1.Alignment = Element.ALIGN_LEFT;
            ContentDate1.AddElement(tLabelDate1);
            footerTable.AddCell(ContentDate1);

            PdfPCell ContentTime = new PdfPCell();
            ContentTime.Border = Rectangle.NO_BORDER;
            ContentTime.Colspan = 3;
            Paragraph tLabelTime = new Paragraph("Jam Selesai Pengisian : ", fontFieldLabel);
            tLabelTime.SpacingBefore = 0;
            tLabelTime.SpacingAfter = 0;
            tLabelTime.Alignment = Element.ALIGN_LEFT;
            ContentTime.AddElement(tLabelTime);
            footerTable.AddCell(ContentTime);

            PdfPCell ContentTime1 = new PdfPCell();
            ContentTime1.Border = Rectangle.NO_BORDER;
            ContentTime1.Colspan = 1;
            Paragraph tLabelTime1 = new Paragraph("", fontFieldValue);
            tLabelTime1.SpacingBefore = 0;
            tLabelTime1.SpacingAfter = 0;
            tLabelTime1.Alignment = Element.ALIGN_LEFT;
            ContentTime1.AddElement(tLabelTime1);
            footerTable.AddCell(ContentTime1);
        }

        private void RenderTableSignature(PdfPTable signatureTable)
        {
            PdfPCell ContentSignOperatorHeader = new PdfPCell();
            ContentSignOperatorHeader.Border = Rectangle.BOX;
            Paragraph tSignOperatorHeader = new Paragraph("Operator CFS", fontFieldValue);
            tSignOperatorHeader.SpacingBefore = 0;
            tSignOperatorHeader.SpacingAfter = 0;
            tSignOperatorHeader.Alignment = Element.ALIGN_CENTER;
            ContentSignOperatorHeader.AddElement(tSignOperatorHeader);
            signatureTable.AddCell(ContentSignOperatorHeader);

            PdfPCell ContentSignDriverHeader = new PdfPCell();
            ContentSignDriverHeader.Border = Rectangle.BOX;
            Paragraph tSignDriverHeader = new Paragraph("Pengemudi", fontFieldValue);
            tSignDriverHeader.SpacingBefore = 0;
            tSignDriverHeader.SpacingAfter = 0;
            tSignDriverHeader.Alignment = Element.ALIGN_CENTER;
            ContentSignDriverHeader.AddElement(tSignDriverHeader);
            signatureTable.AddCell(ContentSignDriverHeader);

            PdfPCell ContentWeightOperatorHeader = new PdfPCell();
            ContentSignDriverHeader.Border = Rectangle.BOX;
            Paragraph tSignWeightOperatorHeader = new Paragraph("Operator Timbang", fontFieldValue);
            tSignWeightOperatorHeader.SpacingBefore = 0;
            tSignWeightOperatorHeader.SpacingAfter = 0;
            tSignWeightOperatorHeader.Alignment = Element.ALIGN_CENTER;
            ContentWeightOperatorHeader.AddElement(tSignWeightOperatorHeader);
            signatureTable.AddCell(ContentWeightOperatorHeader);

            PdfPCell ContentSign = new PdfPCell();
            ContentSign.Border = Rectangle.BOX;
            Paragraph tSignOperator = new Paragraph("", fontSubTitle);
            tSignOperator.SpacingBefore = 25;
            tSignOperator.SpacingAfter = 25;
            tSignOperator.Alignment = Element.ALIGN_CENTER;
            ContentSign.AddElement(tSignOperator);
            
            signatureTable.AddCell(ContentSign);
            signatureTable.AddCell(ContentSign);
            signatureTable.AddCell(ContentSign);
        }

        private System.Drawing.Image PrepareLogo()
        {
            string logoPath = HttpContext.Current.Server.MapPath("~/Images/RPU Logo.png");
            System.Drawing.Image imgLogo = System.Drawing.Image.FromFile(logoPath);
            return imgLogo;
        }
    }
}
