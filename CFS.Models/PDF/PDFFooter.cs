﻿using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;

namespace CFS.Models.PDF
{
    public class PDFFooter : PdfPageEventHelper
    {
        // write on top of document
        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            base.OnOpenDocument(writer, document);
            //PdfPTable tabFot = new PdfPTable(new float[] { 1F });
            //tabFot.SpacingAfter = 10F;
            //PdfPCell cell;
            //tabFot.TotalWidth = 300F;
            //cell = new PdfPCell(new Phrase("Header"));
            //tabFot.AddCell(cell);
            //tabFot.WriteSelectedRows(0, -1, 150, document.Top, writer.DirectContent);
        }

        // write on start of each page
        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);
        }

        // write on end of each page
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);
            PdfPTable tabFot = new PdfPTable((new float[] { 1F }));
            PdfPCell cell;
            tabFot.WidthPercentage = 100;
            tabFot.TotalWidth = PageSize.A6.Width;
            Font fontFooter = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL);
            cell = new PdfPCell(new Phrase(String.Format("Generated by CFS {0}", DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss")), fontFooter));
            cell.Border = Rectangle.NO_BORDER;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            tabFot.AddCell(cell);
            tabFot.WriteSelectedRows(0, -1, 8, document.Bottom, writer.DirectContent);
        }

        //write on close of document
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
        }
    }
}
