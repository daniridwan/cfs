﻿using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;
using System.Globalization;

namespace CFS.Models.PDF
{
    public class SealListPdfRenderer
    {
        private Font fontTitle;
        private Font fontSubTitle;
        private Font fontFieldLabel;
        private Font fontFieldMediumLabel;
        private Font fontFieldLargeLabel;
        private Font fontFieldValue;
        private Font fontFieldMediumValue;
        private Font fontFieldLargeValue;
        private Font fontTableHeader;
        private Font fontTableValue;
        private Font fontFooter;
        private Font fontFooterBold;

        private const string formatDate = "MMM/dd/yyyy";
        private const string formatApprovalDate = "MMM/dd/yyyy HH:mm:ss";

        private string mode = "";
        private string uom = "";

        public SealListPdfRenderer()
        {
            fontTitle = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
            fontSubTitle = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
            fontFieldLabel = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
            fontFieldMediumLabel = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL);
            fontFieldLargeLabel = new Font(Font.FontFamily.HELVETICA, 16, Font.NORMAL);
            fontFieldValue = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
            fontFieldMediumValue = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
            fontFieldLargeValue = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
            fontTableHeader = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
            fontTableValue = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
            fontFooter = new Font(Font.FontFamily.HELVETICA, 6.7f, Font.NORMAL);
            fontFooterBold = new Font(Font.FontFamily.HELVETICA, 6.7f, Font.BOLD);
        }

        public byte[] Render(CFS.Models.TruckLoading data)
        {
            iTextSharp.text.Rectangle rect = new iTextSharp.text.Rectangle(260.0f, 335.0f);
            iTextSharp.text.Document doc = new iTextSharp.text.Document(rect);
            doc.SetMargins(8, 8, 8, 16);
            using (MemoryStream ms = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                writer.PageEvent = new PDFFooter();
                doc.Open();
                doc.AddTitle(string.Format("Daftar Segel # {0}", data.TruckRegistrationPlate));
                doc.AddCreator("CFS");

                //get mode & uom
                if (data.TruckLoadingPlanning[0].FillingPointGroup == "Chemical") //get first item to determine chemical / fuel
                {
                    mode = "Timbang";
                    uom = "Kg";
                }
                else
                {
                    mode = "Flow Meter";
                    uom = "KL";
                }

                int total = 0;
                if (mode == "Timbang")
                {
                    total = data.TruckLoadingPlanning.Count + 1;
                }
                else
                {
                    total = data.TruckLoadingPlanning.Count + 1;
                }

                for (int i = 0; i < total; i++)
                {
                    //border document
                    PdfContentByte content = writer.DirectContent;
                    //Rectangle rectangle = new Rectangle(doc.PageSize);
                    //rectangle.Left += doc.LeftMargin;
                    //rectangle.Right -= doc.RightMargin;
                    //rectangle.Top -= doc.TopMargin;
                    //rectangle.Bottom += doc.BottomMargin;
                    //content.SetColorStroke(BaseColor.BLACK);
                    //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                    content.Stroke();

                    PdfPTable headerTable = new PdfPTable(4); // 4 kolom
                    headerTable.WidthPercentage = 100;
                    headerTable.SetWidths(new float[] { 0.45f, 0.1f, 1f, 1f });
                    headerTable.SpacingBefore = 3;
                    headerTable.SpacingAfter = 8;

                    PdfPTable sealTable = new PdfPTable(6);
                    sealTable.WidthPercentage = 100;
                    sealTable.SetWidths(new float[] { 0.2f, 1f, 0.2f, 0.2f, 1f, 0.2f });
                    sealTable.SpacingAfter = 8;

                    PdfPTable notesTable = new PdfPTable(4);
                    notesTable.SpacingAfter = 8;
                    notesTable.SpacingAfter = 8;
                    notesTable.WidthPercentage = 100;

                    PdfPTable signatureTable = new PdfPTable(3);
                    signatureTable.WidthPercentage = 100;

                    RenderHeader(headerTable, data, content);
                    RenderSealTable(sealTable, data);
                    RenderNotes(notesTable, data);
                    RenderSignature(signatureTable);

                    doc.Add(headerTable);
                    doc.Add(sealTable);
                    doc.Add(notesTable);
                    doc.Add(signatureTable);
                    doc.NewPage();
                }
                doc.Close();
                return ms.ToArray();
            }
        }

        private void RenderHeader(PdfPTable headerTable, TruckLoading data, PdfContentByte cb)
        {
            PdfPCell imgCell = new PdfPCell();
            imgCell.Border = Rectangle.NO_BORDER;
            imgCell.BorderWidth = 0;
            imgCell.Rowspan = 2;
            Image logo = Image.GetInstance(PrepareLogo(), BaseColor.WHITE);
            logo.ScalePercent(35);
            imgCell.AddElement(logo);
            headerTable.AddCell(imgCell);

            //title company
            PdfPCell ContentCompany = new PdfPCell();
            ContentCompany.Border = Rectangle.NO_BORDER;
            ContentCompany.Colspan = 3;
            ContentCompany.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tCompany = new Paragraph("PT. Redeco Petrolin Utama", fontSubTitle);
            tCompany.Alignment = Element.ALIGN_CENTER;
            ContentCompany.AddElement(tCompany);
            headerTable.AddCell(ContentCompany);

            //title daftar segel
            PdfPCell ContentTitle = new PdfPCell();
            ContentTitle.Border = Rectangle.NO_BORDER;
            ContentTitle.Colspan = 3;
            ContentTitle.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTitle = new Paragraph("STICKER DAFTAR SEGEL", fontTitle);
            tTitle.Alignment = Element.ALIGN_CENTER;
            ContentTitle.AddElement(tTitle);
            headerTable.AddCell(ContentTitle);

            //baris tabel header
            PdfPCell ContentQueue = new PdfPCell();
            ContentQueue.Border = Rectangle.NO_BORDER;
            ContentQueue.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentQueue.FixedHeight = 15f;
            Paragraph tQueue = new Paragraph("No. Antrian", fontFieldLabel);
            tQueue.SpacingBefore = 0;
            tQueue.Alignment = Element.ALIGN_LEFT;
            ContentQueue.AddElement(tQueue);
            headerTable.AddCell(ContentQueue);

            PdfPCell ContentSeparator = new PdfPCell();
            ContentSeparator.Border = Rectangle.NO_BORDER;
            Paragraph tLabelSeparator = new Paragraph(":", fontFieldLabel);
            tLabelSeparator.SpacingBefore = 0;
            tLabelSeparator.SpacingAfter = 0;
            tLabelSeparator.Alignment = Element.ALIGN_LEFT;
            ContentSeparator.AddElement(tLabelSeparator);
            headerTable.AddCell(ContentSeparator);

            PdfPCell ContentQueueValue = new PdfPCell();
            ContentQueueValue.Border = Rectangle.NO_BORDER;
            ContentQueueValue.VerticalAlignment = Element.ALIGN_MIDDLE;
            ViewQueueTruck[] queue = ViewQueueTruck.FindByTruckLoadingId(data.TruckLoadingId);
            Paragraph tQueueValue = new Paragraph(queue[0].QueueNumber, fontFieldValue);
            tQueueValue.Alignment = Element.ALIGN_LEFT;
            ContentQueueValue.AddElement(tQueueValue);
            headerTable.AddCell(ContentQueueValue);

            //barcode, rowspan 2
            Barcode128 code128 = new Barcode128();
            code128.CodeType = Barcode.CODE128;
            code128.Code = data.DeliveryOrderNumber.ToString();
            //code128.Font = null; //without text
            iTextSharp.text.Image image128 = code128.CreateImageWithBarcode(cb, null, null);
            image128.Border = Rectangle.NO_BORDER;
            image128.WidthPercentage = 100;
            image128.ScaleAbsolute(80f, 80f);
            image128.Alignment = Element.ALIGN_CENTER;
            PdfPCell barcodeCell = new PdfPCell();
            barcodeCell.Rowspan = 3;
            barcodeCell.Border = Rectangle.NO_BORDER;
            barcodeCell.BorderWidth = 1;
            barcodeCell.AddElement(image128);
            headerTable.AddCell(barcodeCell);

            PdfPCell ContentRegPlate = new PdfPCell();
            ContentRegPlate.Border = Rectangle.NO_BORDER;
            ContentRegPlate.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentRegPlate.FixedHeight = 15f;
            Paragraph tRegPlate = new Paragraph("No. Polisi", fontFieldLabel);
            tRegPlate.Alignment = Element.ALIGN_LEFT;
            ContentRegPlate.AddElement(tRegPlate);
            headerTable.AddCell(ContentRegPlate);

            headerTable.AddCell(ContentSeparator);

            PdfPCell ContentRegPlateValue = new PdfPCell();
            ContentRegPlateValue.Border = Rectangle.NO_BORDER;
            ContentRegPlateValue.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tRegPlateValue = new Paragraph(data.TruckRegistrationPlate, fontFieldValue);
            tRegPlateValue.SpacingBefore = 0;
            tRegPlateValue.Alignment = Element.ALIGN_LEFT;
            ContentRegPlateValue.AddElement(tRegPlateValue);
            headerTable.AddCell(ContentRegPlateValue);

            PdfPCell ContentLane = new PdfPCell();
            ContentLane.Border = Rectangle.NO_BORDER;
            ContentLane.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentLane.FixedHeight = 15f;
            Paragraph tLane = new Paragraph("No. Lajur", fontFieldLabel);
            tLane.Alignment = Element.ALIGN_LEFT;
            ContentLane.AddElement(tLane);
            headerTable.AddCell(ContentLane);

            headerTable.AddCell(ContentSeparator);

            PdfPCell ContentLaneValue = new PdfPCell();
            ContentLaneValue.Border = Rectangle.NO_BORDER;
            ContentLaneValue.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tLaneValue = new Paragraph(data.FillingPointString, fontFieldValue);
            tLaneValue.SpacingBefore = 0;
            tLaneValue.Alignment = Element.ALIGN_LEFT;
            ContentLaneValue.AddElement(tLaneValue);
            headerTable.AddCell(ContentLaneValue);
        }

        private void RenderSealTable(PdfPTable sealTable, TruckLoading data)
        {
            //table segel
            PdfPCell ContentNoLeft = new PdfPCell();
            ContentNoLeft.PaddingTop = 3;
            ContentNoLeft.PaddingBottom = 3;
            ContentNoLeft.Border = Rectangle.BOX;
            ContentNoLeft.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNoLeft = new Paragraph("No", fontFieldLabel);
            tNoLeft.Alignment = Element.ALIGN_CENTER;
            ContentNoLeft.AddElement(tNoLeft);
            sealTable.AddCell(ContentNoLeft);

            PdfPCell ContentSealNoLeft = new PdfPCell();
            ContentSealNoLeft.Border = Rectangle.BOX;
            ContentSealNoLeft.Colspan = 2;
            ContentSealNoLeft.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNoSealLeft = new Paragraph("Nomor Segel", fontFieldLabel);
            tNoSealLeft.Alignment = Element.ALIGN_CENTER;
            ContentSealNoLeft.AddElement(tNoSealLeft);
            sealTable.AddCell(ContentSealNoLeft);

            PdfPCell ContentNoRight = new PdfPCell();
            ContentNoRight.Border = Rectangle.BOX;
            ContentNoRight.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNoRight = new Paragraph("No", fontFieldLabel);
            tNoRight.Alignment = Element.ALIGN_CENTER;
            ContentNoRight.AddElement(tNoRight);
            sealTable.AddCell(ContentNoRight);

            PdfPCell ContentSealNoRight = new PdfPCell();
            ContentSealNoRight.Border = Rectangle.BOX;
            ContentSealNoRight.Colspan = 2;
            ContentSealNoRight.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNoSealRight = new Paragraph("Nomor Segel", fontFieldLabel);
            tNoSealRight.Alignment = Element.ALIGN_CENTER;
            ContentSealNoRight.AddElement(tNoSealRight);
            sealTable.AddCell(ContentSealNoRight);

            //baris 1
            PdfPCell ContentNo1Left = new PdfPCell();
            ContentNo1Left.Border = Rectangle.BOX;
            ContentNo1Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentNo1Left.PaddingTop = 3;
            ContentNo1Left.PaddingBottom = 3;
            Paragraph tNo1Left = new Paragraph("1", fontFieldLabel);
            tNo1Left.Alignment = Element.ALIGN_CENTER;
            ContentNo1Left.AddElement(tNo1Left);
            sealTable.AddCell(ContentNo1Left);

            PdfPCell ContentSealNo1Left = new PdfPCell();
            ContentSealNo1Left.Border = Rectangle.BOX;
            ContentSealNo1Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            string seal1 = "";
            if (data.TruckLoadingSecuritySeal.Count >= 1)
            {
                seal1 = data.TruckLoadingSecuritySeal[0].SealNumber.ToString();
            }
            Paragraph tNoSeal1Left = new Paragraph(seal1, fontFieldValue);
            tNoSeal1Left.Alignment = Element.ALIGN_CENTER;
            ContentSealNo1Left.AddElement(tNoSeal1Left);
            sealTable.AddCell(ContentSealNo1Left);

            PdfPCell ContentChecklist1Left = new PdfPCell();
            ContentChecklist1Left.Border = Rectangle.BOX;
            ContentChecklist1Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tChecklist1Left = new Paragraph("", fontFieldValue);
            tChecklist1Left.Alignment = Element.ALIGN_CENTER;
            ContentChecklist1Left.AddElement(tChecklist1Left);
            sealTable.AddCell(ContentChecklist1Left);

            PdfPCell ContentNo1Right = new PdfPCell();
            ContentNo1Right.Border = Rectangle.BOX;
            ContentNo1Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo1Right = new Paragraph("7", fontFieldLabel);
            tNo1Right.Alignment = Element.ALIGN_CENTER;
            ContentNo1Right.AddElement(tNo1Right);
            sealTable.AddCell(ContentNo1Right);

            PdfPCell ContentSealNo1Right = new PdfPCell();
            ContentSealNo1Right.Border = Rectangle.BOX;
            ContentSealNo1Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            string seal7 = "";
            if (data.TruckLoadingSecuritySeal.Count >= 7)
            {
                seal7 = data.TruckLoadingSecuritySeal[6].SealNumber.ToString();
            }
            Paragraph tNoSeal1Right = new Paragraph(seal7, fontFieldValue);
            tNoSeal1Right.Alignment = Element.ALIGN_CENTER;
            ContentSealNo1Right.AddElement(tNoSeal1Right);
            sealTable.AddCell(ContentSealNo1Right);

            PdfPCell ContentChecklist1Right = new PdfPCell();
            ContentChecklist1Right.Border = Rectangle.BOX;
            ContentChecklist1Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tChecklist1Right = new Paragraph("", fontFieldValue);
            tChecklist1Right.Alignment = Element.ALIGN_CENTER;
            ContentChecklist1Right.AddElement(tChecklist1Right);
            sealTable.AddCell(ContentChecklist1Right);

            //baris 2
            PdfPCell ContentNo2Left = new PdfPCell();
            ContentNo2Left.Border = Rectangle.BOX;
            ContentNo2Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentNo2Left.PaddingTop = 3;
            ContentNo2Left.PaddingBottom = 3;
            Paragraph tNo2Left = new Paragraph("2", fontFieldLabel);
            tNo2Left.Alignment = Element.ALIGN_CENTER;
            ContentNo2Left.AddElement(tNo2Left);
            sealTable.AddCell(ContentNo2Left);

            PdfPCell ContentSealNo2Left = new PdfPCell();
            ContentSealNo2Left.Border = Rectangle.BOX;
            ContentSealNo2Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            string seal2 = "";
            if (data.TruckLoadingSecuritySeal.Count >= 2)
            {
                seal2 = data.TruckLoadingSecuritySeal[1].SealNumber.ToString();
            }
            Paragraph tNoSeal2Left = new Paragraph(seal2, fontFieldValue);
            tNoSeal2Left.Alignment = Element.ALIGN_CENTER;
            ContentSealNo2Left.AddElement(tNoSeal2Left);
            sealTable.AddCell(ContentSealNo2Left);

            PdfPCell ContentChecklist2Left = new PdfPCell();
            ContentChecklist2Left.Border = Rectangle.BOX;
            ContentChecklist2Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tChecklist2Left = new Paragraph("", fontFieldValue);
            tChecklist2Left.Alignment = Element.ALIGN_CENTER;
            ContentChecklist2Left.AddElement(tChecklist2Left);
            sealTable.AddCell(ContentChecklist2Left);

            PdfPCell ContentNo2Right = new PdfPCell();
            ContentNo2Right.Border = Rectangle.BOX;
            ContentNo2Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo2Right = new Paragraph("8", fontFieldLabel);
            tNo2Right.Alignment = Element.ALIGN_CENTER;
            ContentNo2Right.AddElement(tNo2Right);
            sealTable.AddCell(ContentNo2Right);

            PdfPCell ContentSealNo2Right = new PdfPCell();
            ContentSealNo2Right.Border = Rectangle.BOX;
            ContentSealNo2Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            string seal8 = "";
            if (data.TruckLoadingSecuritySeal.Count >= 8)
            {
                seal8 = data.TruckLoadingSecuritySeal[7].SealNumber.ToString();
            }
            Paragraph tNoSeal2Right = new Paragraph(seal8, fontFieldValue);
            tNoSeal2Right.Alignment = Element.ALIGN_CENTER;
            ContentSealNo2Right.AddElement(tNoSeal2Right);
            sealTable.AddCell(ContentSealNo2Right);

            PdfPCell ContentChecklist2Right = new PdfPCell();
            ContentChecklist2Right.Border = Rectangle.BOX;
            ContentChecklist2Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tChecklist2Right = new Paragraph("", fontFieldValue);
            tChecklist2Right.Alignment = Element.ALIGN_CENTER;
            ContentChecklist2Right.AddElement(tChecklist2Right);
            sealTable.AddCell(ContentChecklist2Right);

            //baris 3
            PdfPCell ContentNo3Left = new PdfPCell();
            ContentNo3Left.Border = Rectangle.BOX;
            ContentNo3Left.PaddingTop = 3;
            ContentNo3Left.PaddingBottom = 3;
            ContentNo3Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo3Left = new Paragraph("3", fontFieldLabel);
            tNo3Left.Alignment = Element.ALIGN_CENTER;
            ContentNo3Left.AddElement(tNo3Left);
            sealTable.AddCell(ContentNo3Left);

            PdfPCell ContentSealNo3Left = new PdfPCell();
            ContentSealNo3Left.Border = Rectangle.BOX;
            ContentSealNo3Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            string seal3 = "";
            if (data.TruckLoadingSecuritySeal.Count >= 3)
            {
                seal3 = data.TruckLoadingSecuritySeal[2].SealNumber.ToString();
            }
            Paragraph tNoSeal3Left = new Paragraph(seal3, fontFieldValue);
            tNoSeal3Left.Alignment = Element.ALIGN_CENTER;
            ContentSealNo3Left.AddElement(tNoSeal3Left);
            sealTable.AddCell(ContentSealNo3Left);

            PdfPCell ContentChecklist3Left = new PdfPCell();
            ContentChecklist3Left.Border = Rectangle.BOX;
            ContentChecklist3Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tChecklist3Left = new Paragraph("", fontFieldValue);
            tChecklist3Left.Alignment = Element.ALIGN_CENTER;
            ContentChecklist3Left.AddElement(tChecklist3Left);
            sealTable.AddCell(ContentChecklist3Left);

            PdfPCell ContentNo3Right = new PdfPCell();
            ContentNo3Right.Border = Rectangle.BOX;
            ContentNo3Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo3Right = new Paragraph("9", fontFieldLabel);
            tNo3Right.Alignment = Element.ALIGN_CENTER;
            ContentNo3Right.AddElement(tNo3Right);
            sealTable.AddCell(ContentNo3Right);

            PdfPCell ContentSealNo3Right = new PdfPCell();
            ContentSealNo3Right.Border = Rectangle.BOX;
            ContentSealNo3Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            string seal9 = "";
            if (data.TruckLoadingSecuritySeal.Count >= 9)
            {
                seal9 = data.TruckLoadingSecuritySeal[8].SealNumber.ToString();
            }
            Paragraph tNoSeal3Right = new Paragraph(seal9, fontFieldValue);
            tNoSeal3Right.Alignment = Element.ALIGN_CENTER;
            ContentSealNo3Right.AddElement(tNoSeal3Right);
            sealTable.AddCell(ContentSealNo3Right);

            PdfPCell ContentChecklist3Right = new PdfPCell();
            ContentChecklist3Right.Border = Rectangle.BOX;
            ContentChecklist3Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tChecklist3Right = new Paragraph("", fontFieldValue);
            tChecklist3Right.Alignment = Element.ALIGN_CENTER;
            ContentChecklist3Right.AddElement(tChecklist3Right);
            sealTable.AddCell(ContentChecklist3Right);

            //baris 4
            PdfPCell ContentNo4Left = new PdfPCell();
            ContentNo4Left.Border = Rectangle.BOX;
            ContentNo4Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentNo4Left.PaddingTop = 3;
            ContentNo4Left.PaddingBottom = 3;
            Paragraph tNo4Left = new Paragraph("4", fontFieldLabel);
            tNo4Left.Alignment = Element.ALIGN_CENTER;
            ContentNo4Left.AddElement(tNo4Left);
            sealTable.AddCell(ContentNo4Left);

            PdfPCell ContentSealNo4Left = new PdfPCell();
            ContentSealNo4Left.Border = Rectangle.BOX;
            ContentSealNo4Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            string seal4 = "";
            if (data.TruckLoadingSecuritySeal.Count >= 4)
            {
                seal4 = data.TruckLoadingSecuritySeal[3].SealNumber.ToString();
            }
            Paragraph tNoSeal4Left = new Paragraph(seal4, fontFieldValue);
            tNoSeal4Left.Alignment = Element.ALIGN_CENTER;
            ContentSealNo4Left.AddElement(tNoSeal4Left);
            sealTable.AddCell(ContentSealNo4Left);

            PdfPCell ContentChecklist4Left = new PdfPCell();
            ContentChecklist4Left.Border = Rectangle.BOX;
            ContentChecklist4Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tChecklist4Left = new Paragraph("", fontFieldValue);
            tChecklist4Left.Alignment = Element.ALIGN_CENTER;
            ContentChecklist4Left.AddElement(tChecklist4Left);
            sealTable.AddCell(ContentChecklist4Left);

            PdfPCell ContentNo4Right = new PdfPCell();
            ContentNo4Right.Border = Rectangle.BOX;
            ContentNo4Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo4Right = new Paragraph("10", fontFieldLabel);
            tNo4Right.Alignment = Element.ALIGN_CENTER;
            ContentNo4Right.AddElement(tNo4Right);
            sealTable.AddCell(ContentNo4Right);

            PdfPCell ContentSealNo4Right = new PdfPCell();
            ContentSealNo4Right.Border = Rectangle.BOX;
            ContentSealNo4Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            string seal10 = "";
            if (data.TruckLoadingSecuritySeal.Count >= 10)
            {
                seal10 = data.TruckLoadingSecuritySeal[9].SealNumber.ToString();
            }
            Paragraph tNoSeal4Right = new Paragraph(seal10, fontFieldValue);
            tNoSeal4Right.Alignment = Element.ALIGN_CENTER;
            ContentSealNo4Right.AddElement(tNoSeal4Right);
            sealTable.AddCell(ContentSealNo4Right);

            PdfPCell ContentChecklist4Right = new PdfPCell();
            ContentChecklist4Right.Border = Rectangle.BOX;
            ContentChecklist4Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tChecklist4Right = new Paragraph("", fontFieldValue);
            tChecklist4Right.Alignment = Element.ALIGN_CENTER;
            ContentChecklist4Right.AddElement(tChecklist4Right);
            sealTable.AddCell(ContentChecklist4Right);

            //baris 5
            PdfPCell ContentNo5Left = new PdfPCell();
            ContentNo5Left.Border = Rectangle.BOX;
            ContentNo5Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentNo5Left.PaddingTop = 3;
            ContentNo5Left.PaddingBottom = 3;
            Paragraph tNo5Left = new Paragraph("5", fontFieldLabel);
            tNo5Left.Alignment = Element.ALIGN_CENTER;
            ContentNo5Left.AddElement(tNo5Left);
            sealTable.AddCell(ContentNo5Left);

            PdfPCell ContentSealNo5Left = new PdfPCell();
            ContentSealNo5Left.Border = Rectangle.BOX;
            ContentSealNo5Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            string seal5 = "";
            if (data.TruckLoadingSecuritySeal.Count >= 5)
            {
                seal5 = data.TruckLoadingSecuritySeal[4].SealNumber.ToString();
            }
            Paragraph tNoSeal5Left = new Paragraph(seal5, fontFieldValue);
            tNoSeal5Left.Alignment = Element.ALIGN_CENTER;
            ContentSealNo5Left.AddElement(tNoSeal5Left);
            sealTable.AddCell(ContentSealNo5Left);

            PdfPCell ContentChecklist5Left = new PdfPCell();
            ContentChecklist5Left.Border = Rectangle.BOX;
            ContentChecklist5Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tChecklist5Left = new Paragraph("", fontFieldValue);
            tChecklist5Left.Alignment = Element.ALIGN_CENTER;
            ContentChecklist5Left.AddElement(tChecklist5Left);
            sealTable.AddCell(ContentChecklist5Left);

            PdfPCell ContentNo5Right = new PdfPCell();
            ContentNo5Right.Border = Rectangle.BOX;
            ContentNo5Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo5Right = new Paragraph("11", fontFieldLabel);
            tNo5Right.Alignment = Element.ALIGN_CENTER;
            ContentNo5Right.AddElement(tNo5Right);
            sealTable.AddCell(ContentNo5Right);

            PdfPCell ContentSealNo5Right = new PdfPCell();
            ContentSealNo5Right.Border = Rectangle.BOX;
            ContentSealNo5Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            string seal11 = "";
            if (data.TruckLoadingSecuritySeal.Count >= 11)
            {
                seal11 = data.TruckLoadingSecuritySeal[10].SealNumber.ToString();
            }
            Paragraph tNoSeal5Right = new Paragraph(seal11, fontFieldValue);
            tNoSeal5Right.Alignment = Element.ALIGN_CENTER;
            ContentSealNo5Right.AddElement(tNoSeal5Right);
            sealTable.AddCell(ContentSealNo5Right);

            PdfPCell ContentChecklist5Right = new PdfPCell();
            ContentChecklist5Right.Border = Rectangle.BOX;
            ContentChecklist5Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tChecklist5Right = new Paragraph("", fontFieldValue);
            tChecklist5Right.Alignment = Element.ALIGN_CENTER;
            ContentChecklist5Right.AddElement(tChecklist5Right);
            sealTable.AddCell(ContentChecklist5Right);

            //baris 6
            PdfPCell ContentNo6Left = new PdfPCell();
            ContentNo6Left.Border = Rectangle.BOX;
            ContentNo6Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentNo6Left.PaddingTop = 3;
            ContentNo6Left.PaddingBottom = 3;
            Paragraph tNo6Left = new Paragraph("6", fontFieldLabel);
            tNo6Left.Alignment = Element.ALIGN_CENTER;
            ContentNo6Left.AddElement(tNo6Left);
            sealTable.AddCell(ContentNo6Left);

            PdfPCell ContentSealNo6Left = new PdfPCell();
            ContentSealNo6Left.Border = Rectangle.BOX;
            ContentSealNo6Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            string seal6 = "";
            if (data.TruckLoadingSecuritySeal.Count >= 6)
            {
                seal6 = data.TruckLoadingSecuritySeal[5].SealNumber.ToString();
            }
            Paragraph tNoSeal6Left = new Paragraph(seal6, fontFieldValue);
            tNoSeal6Left.Alignment = Element.ALIGN_CENTER;
            ContentSealNo6Left.AddElement(tNoSeal6Left);
            sealTable.AddCell(ContentSealNo6Left);

            PdfPCell ContentChecklist6Left = new PdfPCell();
            ContentChecklist6Left.Border = Rectangle.BOX;
            ContentChecklist6Left.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tChecklist6Left = new Paragraph("", fontFieldValue);
            tChecklist6Left.Alignment = Element.ALIGN_CENTER;
            ContentChecklist6Left.AddElement(tChecklist6Left);
            sealTable.AddCell(ContentChecklist6Left);

            PdfPCell ContentNo6Right = new PdfPCell();
            ContentNo6Right.Border = Rectangle.BOX;
            ContentNo6Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tNo6Right = new Paragraph("12", fontFieldLabel);
            tNo6Right.Alignment = Element.ALIGN_CENTER;
            ContentNo6Right.AddElement(tNo6Right);
            sealTable.AddCell(ContentNo6Right);

            PdfPCell ContentSealNo6Right = new PdfPCell();
            ContentSealNo6Right.Border = Rectangle.BOX;
            ContentSealNo6Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            string seal12 = "";
            if (data.TruckLoadingSecuritySeal.Count >= 12)
            {
                seal12 = data.TruckLoadingSecuritySeal[11].SealNumber.ToString();
            }
            Paragraph tNoSeal6Right = new Paragraph(seal12, fontFieldValue);
            tNoSeal6Right.Alignment = Element.ALIGN_CENTER;
            ContentSealNo6Right.AddElement(tNoSeal6Right);
            sealTable.AddCell(ContentSealNo6Right);

            PdfPCell ContentChecklist6Right = new PdfPCell();
            ContentChecklist6Right.Border = Rectangle.BOX;
            ContentChecklist6Right.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tChecklist6Right = new Paragraph("", fontFieldValue);
            tChecklist6Right.Alignment = Element.ALIGN_CENTER;
            ContentChecklist6Right.AddElement(tChecklist6Right);
            sealTable.AddCell(ContentChecklist6Right);
        }

        private void RenderNotes (PdfPTable notesTable, TruckLoading data)
        {
            PdfPCell ContentNotes = new PdfPCell();
            ContentNotes.FixedHeight = 15f;
            ContentNotes.Border = Rectangle.NO_BORDER;
            ContentNotes.Colspan = 4;
            ContentNotes.VerticalAlignment = Element.ALIGN_LEFT;
            Paragraph tNotes = new Paragraph("Telah melihat dan menyaksikan pemasangan SEGEL dengan baik dan benar", fontFooter);
            tNotes.Alignment = Element.ALIGN_LEFT;
            ContentNotes.AddElement(tNotes);
            notesTable.AddCell(ContentNotes);

            PdfPCell ContentDate = new PdfPCell();
            ContentDate.Border = Rectangle.NO_BORDER;
            ContentDate.Colspan = 2;
            ContentDate.FixedHeight = 15f;
            ContentDate.VerticalAlignment = Element.ALIGN_MIDDLE;
            string administrationDate = data.AdministrationTimestamp.HasValue ? data.AdministrationTimestamp.Value.ToString("dd/MM/yyyy") : "";
            Paragraph tDate = new Paragraph(string.Format("Tanggal : {0}", administrationDate), fontFooter);
            tDate.Alignment = Element.ALIGN_LEFT;
            ContentDate.AddElement(tDate);
            notesTable.AddCell(ContentDate);

            PdfPCell ContentTime = new PdfPCell();
            ContentTime.Border = Rectangle.NO_BORDER;
            ContentTime.Colspan = 2;
            ContentTime.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tTime = new Paragraph("Jam Selesai Inspeksi :", fontFooter);
            tTime.Alignment = Element.ALIGN_LEFT;
            ContentTime.AddElement(tTime);
            notesTable.AddCell(ContentTime);
        }

        private void RenderSignature(PdfPTable signatureTable)
        {
            PdfPCell ContentInspektor = new PdfPCell();
            ContentInspektor.Border = Rectangle.BOX;
            ContentInspektor.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tInspektor = new Paragraph("Inspektor", fontFieldValue);
            tInspektor.Alignment = Element.ALIGN_CENTER;
            ContentInspektor.AddElement(tInspektor);
            signatureTable.AddCell(ContentInspektor);

            PdfPCell ContentDriver = new PdfPCell();
            ContentDriver.Border = Rectangle.BOX;
            ContentDriver.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tDriver = new Paragraph("Pengemudi", fontFieldValue);
            tDriver.Alignment = Element.ALIGN_CENTER;
            ContentDriver.AddElement(tDriver);
            signatureTable.AddCell(ContentDriver);

            PdfPCell ContentIlsk = new PdfPCell();
            ContentIlsk.Border = Rectangle.BOX;
            ContentIlsk.VerticalAlignment = Element.ALIGN_MIDDLE;
            Paragraph tIlsk = new Paragraph("Inspektor ILSK", fontFieldValue);
            tIlsk.Alignment = Element.ALIGN_CENTER;
            ContentIlsk.AddElement(tIlsk);
            signatureTable.AddCell(ContentIlsk);

            PdfPCell ContentEmpty = new PdfPCell();
            ContentEmpty.Border = Rectangle.BOX;
            ContentEmpty.VerticalAlignment = Element.ALIGN_MIDDLE;
            ContentEmpty.PaddingTop = 25;
            ContentEmpty.PaddingBottom = 25;

            signatureTable.AddCell(ContentEmpty);
            signatureTable.AddCell(ContentEmpty);
            signatureTable.AddCell(ContentEmpty);
        }

        private System.Drawing.Image PrepareLogo()
        {
            string logoPath = HttpContext.Current.Server.MapPath("~/Images/RPU Logo.png");
            System.Drawing.Image imgLogo = System.Drawing.Image.FromFile(logoPath);
            return imgLogo;
        }
    }
}
