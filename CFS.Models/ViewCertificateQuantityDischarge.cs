﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("View_Certificate_Quantity_Discharge", Mutable = false)]
    public class ViewCertificateQuantityDischarge : ActiveRecordBase
    {
        private List<string> messages;
        private string tankName;
        private int? tankTicketOpenId;
        private int? tankTicketCloseId;
        private string operationType;
        private string tankTicketNumberOpen;
        private string tankTicketNumberClose;
        private DateTime? startTimestamp;
        private DateTime? stopTimestamp;
        private double liquidLevelBefore;
        private double liquidLevelAfter;
        private double liquidTemperatureBefore;
        private double liquidTemperatureAfter;
        private double liquidVolObsBefore;
        private double liquidVolObsAfter;
        private double liquidDensityBefore;
        private double liquidDensityAfter;
        private double liquidDensity15Before;
        private double liquidDensity15After;
        private float liquidVolumeCorrectionFactorBefore;
        private float liquidVolumeCorrectionFactorAfter;
        private double liquidVol15Before;
        private double liquidVol15After;
        private float liquidDensityMultiplierBefore;
        private float liquidDensityMultiplierAfter;
        private double liquidMassBefore;
        private double liquidMassAfter;
        private double nettReceiving;

        [Property("Tank_Name")]
        public string TankName { get { return tankName; } set { tankName = value; } }
        [Property("Operation_Type")]
        public string OperationType { get { return operationType; } set { operationType = value; } }
        [PrimaryKey(PrimaryKeyType.Assigned, "Tank_Ticket_Open_Id")]
        public int? TankTicketOpenId { get { return tankTicketOpenId; } set { tankTicketOpenId = value; } }
        [Property("Tank_Ticket_Close_Id")]
        public int? TankTicketCloseId { get { return tankTicketCloseId; } set { tankTicketCloseId = value; } }
        [Property("Tank_Ticket_Number_Open")]
        public string TankTicketNumberOpen { get { return tankTicketNumberOpen; } set { tankTicketNumberOpen = value; } }
        [Property("Tank_Ticket_Number_Close")]
        public string TankTicketNumberClose { get { return tankTicketNumberClose; } set { tankTicketNumberClose = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Start_Timestamp")]
        public DateTime? StartTimestamp { get { return startTimestamp; } set { startTimestamp = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Stop_Timestamp")]
        public DateTime? StopTimestamp { get { return stopTimestamp; } set { stopTimestamp = value; } }
        [Property("Liquid_Level_Before")]
        public double LiquidLevelBefore { get { return liquidLevelBefore; } set { liquidLevelBefore = value; } }
        [Property("Liquid_Level_After")]
        public double LiquidLevelAfter { get { return liquidLevelAfter; } set { liquidLevelAfter = value; } }
        [Property("Liquid_Temperature_Before")]
        public double LiquidTemperatureBefore { get { return liquidTemperatureBefore; } set { liquidTemperatureBefore = value; } }
        [Property("Liquid_Temperature_After")]
        public double LiquidTemperatureAfter { get { return liquidTemperatureAfter; } set { liquidTemperatureAfter = value; } }
        [Property("Liquid_VolObs_Before")]
        public double LiquidVolObsBefore { get { return liquidVolObsBefore; } set { liquidVolObsBefore = value; } }
        [Property("Liquid_VolObs_After")]
        public double LiquidVolObsAfter { get { return liquidVolObsAfter; } set { liquidVolObsAfter = value; } }
        [Property("Liquid_Density_Before")]
        public double LiquidDensityBefore { get { return liquidDensityBefore; } set { liquidDensityBefore = value; } }
        [Property("Liquid_Density_After")]
        public double LiquidDensityAfter { get { return liquidDensityAfter; } set { liquidDensityAfter = value; } }
        [Property("Liquid_Density15_Before")]
        public double LiquidDensity15Before { get { return liquidDensity15Before; } set { liquidDensity15Before = value; } }
        [Property("Liquid_Density15_After")]
        public double LiquidDensity15After { get { return liquidDensity15After; } set { liquidDensity15After = value; } }
        [Property("Liquid_Volume_Correction_Factor_Before")]
        public float LiquidVolumeCorrectionFactorBefore { get { return liquidVolumeCorrectionFactorBefore; } set { liquidVolumeCorrectionFactorBefore = value; } }
        [Property("Liquid_Volume_Correction_Factor_After")]
        public float LiquidVolumeCorrectionFactorAfter { get { return liquidVolumeCorrectionFactorAfter; } set { liquidVolumeCorrectionFactorAfter = value; } }
        [Property("Liquid_Vol15_Before")]
        public double LiquidVol15Before { get { return liquidVol15Before; } set { liquidVol15Before = value; } }
        [Property("Liquid_Vol15_After")]
        public double LiquidVol15After { get { return liquidVol15After; } set { liquidVol15After = value; } }
        [Property("Liquid_Density_Multiplier_Before")]
        public float LiquidDensityMultiplierBefore { get { return liquidDensityMultiplierBefore; } set { liquidDensityMultiplierBefore = value; } }
        [Property("Liquid_Density_Multiplier_After")]
        public float LiquidDensityMultiplierAfter { get { return liquidDensityMultiplierAfter; } set { liquidDensityMultiplierAfter = value; } }
        [Property("Liquid_Mass_Before")]
        public double LiquidMassBefore { get { return liquidMassBefore; } set { liquidMassBefore = value; } }
        [Property("Liquid_Mass_After")]
        public double LiquidMassAfter { get { return liquidMassAfter; } set { liquidMassAfter = value; } }
        [Property("Nett_Receiving")]
        public double NettReceiving { get { return nettReceiving; } set { nettReceiving = value; } }

        public ViewCertificateQuantityDischarge()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(ViewCertificateQuantityDischarge), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(ViewCertificateQuantityDischarge));
            DetachedCriteria countCriteria = DetachedCriteria.For<ViewCertificateQuantityDischarge>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                //flt timestamp from
                if (param.SearchParam["fltTimestampFrom"] != null && param.SearchParam["fltTimestampFrom"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampFrom"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Gt("StartTimestamp", startPeriod));
                    countCriteria.Add(Expression.Gt("StartTimestamp", startPeriod));
                }
                //flt timestamp to
                if (param.SearchParam["fltTimestampTo"] != null && param.SearchParam["fltTimestampTo"].Length != 0)
                {
                    DateTime endPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampTo"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Le("StopTimestamp", endPeriod));
                    countCriteria.Add(Expression.Le("StopTimestamp", endPeriod));
                }
            }


            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<ViewCertificateQuantityDischarge>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static ViewCertificateQuantityDischarge[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewCertificateQuantityDischarge>();
            dc.AddOrder(Order.Asc("TankTicketOpenId"));
            return (ViewCertificateQuantityDischarge[])FindAll(typeof(ViewCertificateQuantityDischarge), dc);
        }
        public static ViewCertificateQuantityDischarge FindByTankTicketNumber(string tankTicketClose)
        {
            ICriterion[] query = { Expression.Eq("TankTicketNumberClose", tankTicketClose) };
            return (ViewCertificateQuantityDischarge)FindOne(typeof(ViewCertificateQuantityDischarge), query);
        }
    }
}
