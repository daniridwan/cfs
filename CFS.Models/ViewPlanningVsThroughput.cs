﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("View_Planning_Vs_Throughput")]
    public class ViewPlanningVsThroughput : ActiveRecordBase
    {
        private List<string> messages;
        private DateTime? date;
        private float planning;
        private float throughput;
        private float percentage;

        [JsonConverter(typeof(IsoDateTimeConverter))]
        [PrimaryKey(PrimaryKeyType.Identity, "Date")]
        public DateTime? Date { get { return date; } set { date = value; } }
        [Property("Planning")]
        public float Planning { get { return planning; } set { planning = value; } }
        [Property("Throughput")]
        public float Throughput { get { return throughput; } set { throughput = value; } }
        [Property("Percentage")]
        public float Percentage { get { return percentage; } set { percentage = value; } }

        public ViewPlanningVsThroughput()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static ViewPlanningVsThroughput[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewPlanningVsThroughput>();
            dc.AddOrder(Order.Asc("Date"));
            return (ViewPlanningVsThroughput[])FindAll(typeof(ViewPlanningVsThroughput), dc);
        }
    }
}
