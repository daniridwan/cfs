﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("System_Validation")]
    public class SystemValidation : ActiveRecordBase
    {
        private List<string> messages;
        private int id;
        private string name;
        private string sysValue;
        private bool isActive;
        private string description;

        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public int Id { get { return id; } set { id = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }
        [Property("Value")]
        public string Value { get { return sysValue; } set { sysValue = value; } }
        [Property("Is_Active")]
        public bool IsActive { get { return isActive; } set { isActive = value; } }
        [Property("Description")]
        public string Description { get { return description; } set { description = value; } }

        public SystemValidation()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(SystemValidation), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(SystemValidation));
            DetachedCriteria countCriteria = DetachedCriteria.For<SystemValidation>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<SystemValidation>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public bool Validate()
        {
            bool valid = true;
            return valid; ;
        }

        public static SystemValidation FindById(int id)
        {
            return (SystemValidation)FindByPrimaryKey(typeof(SystemValidation), id);
        }

        public static SystemValidation FindByKey(string name)
        {
            ICriterion[] query = { Expression.Eq("Name", name) };
            return (SystemValidation)FindOne(typeof(SystemValidation), query);
        }
        public static SystemValidation[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<SystemValidation>();
            dc.AddOrder(Order.Asc("Name"));
            return (SystemValidation[])FindAll(typeof(SystemValidation), dc);
        }
    }
}
