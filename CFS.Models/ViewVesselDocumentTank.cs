﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("View_Vessel_Document_Tank", Mutable = false)]
    public class ViewVesselDocumentTank : ActiveRecordBase
    {
        private List<string> messages;
        private long vesselTankId;
        private long vesselDocumentId;
        private long vesselId;
        private string vesselName;
        private string vesselNo;
        private string type;
        private long productId;
        private DateTime? arrivalDate;
        private string blNo;
        private float blQuantity;
        private string blUom;
        private long companyId;
        private string sppbNo;
        private DateTime? sppbDate;
        private string pibNo;
        private DateTime? pibDate;
        private string remarks;
        private int isActive;
        private long tankId;
        private float spillage;
        private string name;
        private float quantity;

        [PrimaryKey(PrimaryKeyType.Assigned, "Vessel_Tank_Id")]
        public long VesselTankId { get { return vesselTankId; } set { vesselTankId = value; } }
        [Property("Vessel_Document_Id")]
        public long VesselDocumentId { get { return vesselDocumentId; } set { vesselDocumentId = value; } }
        [Property("Vessel_No")]
        public string VesselNo { get { return vesselNo; } set { vesselNo = value; } }
        [Property("Vessel_Id")]
        public long VesselId { get { return vesselId; } set { vesselId = value; } }
        [Property("Vessel_Name")]
        public string VesselName { get { return vesselName; } set { vesselName = value; } }
        [Property("Type")]
        public string Type { get { return type; } set { type = value; } }
        [Property("Product_Id")]
        public long ProductId { get { return productId; } set { productId = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Arrival_Date")]
        public DateTime? ArrivalDate { get { return arrivalDate; } set { arrivalDate = value; } }
        [Property("BL_No")]
        public string BlNo { get { return blNo; } set { blNo = value; } }
        [Property("BL_Quantity")]
        public float BlQuantity { get { return blQuantity; } set { blQuantity = value; } }
        [Property("BL_UOM")]
        public string BlUom { get { return blUom; } set { blUom = value; } }
        [Property("Company_Id")]
        public long CompanyId { get { return companyId; } set { companyId = value; } }
        [Property("SPPB_No")]
        public string SppbNo { get { return sppbNo; } set { sppbNo = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("SPPB_Date")]
        public DateTime? SppbDate { get { return sppbDate; } set { sppbDate = value; } }
        [Property("PIB_No")]
        public string PibNo { get { return pibNo; } set { pibNo = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("PIB_Date")]
        public DateTime? PibDate { get { return pibDate; } set { pibDate = value; } }
        [Property("Remarks")]
        public string Remarks { get { return remarks; } set { remarks = value; } }
        [Property("Is_Active")]
        public int IsActive { get { return isActive; } set { isActive = value; } }
        [Property("Tank_Id")]
        public long TankId { get { return tankId; } set { tankId = value; } }
        [Property("Spillage")]
        public float Spillage { get { return spillage; } set { spillage = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }
        [Property("Quantity")]
        public float Quantity { get { return quantity; } set { quantity = value; } }

        public ViewVesselDocumentTank()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(ViewVesselDocumentTank), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(ViewVesselDocumentTank));
            DetachedCriteria countCriteria = DetachedCriteria.For<ViewVesselDocumentTank>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltBlNo"] != null && param.SearchParam["fltBlNo"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("BlNo", param.SearchParam["fltBlNo"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("BlNo", param.SearchParam["fltBlNo"], MatchMode.Anywhere));
                }

                //add filter bl plb 
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<ViewVesselDocumentTank>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static ViewVesselDocumentTank[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewVesselDocumentTank>();
            dc.AddOrder(Order.Asc("VesselNo"));
            return (ViewVesselDocumentTank[])FindAll(typeof(ViewVesselDocumentTank), dc);
        }

        public static ViewVesselDocumentTank FindById(long id)
        {
            return (ViewVesselDocumentTank)FindByPrimaryKey(typeof(ViewVesselDocumentTank), id);
        }

        public static ViewVesselDocumentTank FindByVesselNo(string vesselNo)
        {
            ICriterion[] query = { Expression.Eq("VesselNo", vesselNo) };
            return (ViewVesselDocumentTank)FindOne(typeof(ViewVesselDocumentTank), query);
        }

        public static ViewVesselDocumentTank[] FindByCompanyAndProduct(long companyId, long productId)
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewVesselDocumentTank>();
            dc.Add(Expression.Eq("CompanyId", companyId));
            dc.Add(Expression.Eq("ProductId", productId));
            dc.Add(Expression.Eq("IsActive", 1));
            dc.AddOrder(Order.Asc("VesselNo"));
            return (ViewVesselDocumentTank[])FindAll(typeof(ViewVesselDocumentTank), dc);
        }
    }
}
