﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Web.Script.Serialization;
using NHibernate.Mapping;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace CFS.Models
{
    [ActiveRecord("Users")]
    public class Users : ActiveRecordBase
    {
        private List<string> messages;
        private long userId;
        private string userName;
        private string password;
        private string name;
        private string email;
        private string phoneNumber;
        private long companyId;
        private int isActive;
        private int isAlreadyChangePassword;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        private IList<Role> user_role;

        //helper
        private string roleIdStr;
        private string roleNameStr;
        private string oldPassword;
        private string newPassword;
        private string confirmPassword;

        [PrimaryKey(PrimaryKeyType.Identity, "User_Id")]
        public long UserId { get { return userId; } set { userId = value; } }
        [Property("Username")]
        public string Username { get { return userName; } set { userName = value; } }
        [JsonIgnore]
        [Property("Password")]
        public string Password { get { return password; } set { password = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }
        [Property("Email")]
        public string Email { get { return email; } set { email = value; } }
        [Property("Phone_Number")]
        public string PhoneNumber { get { return phoneNumber; } set { phoneNumber = value; } }
        [Property("Is_Active")]
        public int IsActive { get { return isActive; } set { isActive = value; } }
        [Property("Is_Already_Change_Password")]
        public int IsAlreadyChangePassword { get { return isAlreadyChangePassword; } set { isAlreadyChangePassword = value; } }

        [JsonIgnore]
        [ScriptIgnore]
        [HasAndBelongsToMany(typeof(Role), Table = "User_Role", ColumnKey = "User_Id", ColumnRef = "Role_Id")]
        public IList<Role> User_Roles { get { return user_role; } set { user_role = value; } }

        //Audit Trail
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }
        [JsonIgnore]
        public string OldPassword { get { return oldPassword; } set { oldPassword = value; } }
        [JsonIgnore]
        public string NewPassword { get { return newPassword; } set { newPassword = value; } }
        [JsonIgnore]
        public string ConfirmPassword { get { return confirmPassword; } set { confirmPassword = value; } }

        public string RoleIdStr
        {
            get
            {
                if (roleIdStr == null || roleIdStr.Length == 0)
                {
                    StringBuilder sb = new StringBuilder();
                    bool first = true;
                    foreach (Role r in user_role)
                    {
                        if (first) first = false;
                        else sb.Append(",");
                        sb.AppendFormat("{0}", r.RoleId);
                    }
                    roleIdStr = sb.ToString();
                }
                return roleIdStr;
            }
            set { roleIdStr = value; }
        }

        public string RoleNameStr
        {
            get
            {
                if (roleNameStr == null || roleNameStr.Length == 0)
                {
                    StringBuilder sb = new StringBuilder();
                    bool first = true;
                    foreach (Role r in user_role)
                    {
                        if (first) first = false;
                        else sb.Append(",");
                        sb.AppendFormat("{0}", CFS.Models.Role.FindById(r.RoleId).Name.ToString());
                    }
                    roleNameStr = sb.ToString();
                }
                return roleNameStr;
            }
            set { roleNameStr = value; }
        }

        public string[] RoleName { get { return GetRoleNames(); } }
        public string[] GetRoleNames()
        {
            List<string> arr = new List<string>();
            foreach (Role r in user_role)
            {
                arr.Add(CFS.Models.Role.FindById(r.RoleId).Name.ToString());
            }
            return arr.ToArray();
        }

        public UserRole userrole { get; set; }

        public bool HasPermission(string Permission)
        {
            bool toReturn = false;
            //Permission[] menu = Permission.FindAll();
            foreach (Role d in user_role)
            {
                foreach (Permission r in d.RolePermissions)
                {

                    if (r.Name == Permission)
                    {
                        toReturn = true;
                        break;
                    }
                }
                if (toReturn)
                {
                    break;
                }
            }

            return toReturn;
        }

        public Users()
        {
            user_role = new List<Role>();
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Users), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Users));
            DetachedCriteria countCriteria = DetachedCriteria.For<Users>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltUsername"] != null && param.SearchParam["fltUsername"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Username", param.SearchParam["fltUsername"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Username", param.SearchParam["fltUsername"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltEmail"] != null && param.SearchParam["fltEmail"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Email", param.SearchParam["fltEmail"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Email", param.SearchParam["fltEmail"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltRoles"] != null && param.SearchParam["fltRoles"].Length != 0)
                {
                    int id = int.Parse(param.SearchParam["fltRoles"]);
                    criteria.CreateAlias("Roles.RoleId", "id", NHibernate.SqlCommand.JoinType.InnerJoin)
                    .Add(Restrictions.Eq("Roles.RoleId", id));
                    countCriteria.CreateAlias("Roles.RoleId", "id", NHibernate.SqlCommand.JoinType.InnerJoin)
                    .Add(Expression.Eq("Roles.RoleId", id));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Asc)
                    criteria.AddOrder(Order.Asc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Desc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Users>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static Users FindById(long UserId)
        {
            return (Users)FindByPrimaryKey(typeof(Users), UserId, false);
        }

        public static Users[] FindByCompany(long companyId)
        {
            DetachedCriteria dc = DetachedCriteria.For<Users>();
            dc.Add(Expression.Eq("CompanyId", companyId));
            dc.AddOrder(Order.Asc("Name"));
            return (Users[])FindAll(typeof(Users), dc);
        }

        //public static Users FindByFingerprintId(string fingerprintId)
        //{
        //    ICriterion[] query = { Expression.Eq("IdentificationNumber", fingerprintId) };
        //    return (Users)FindOne(typeof(Users), query);
        //}

        public static Users FindByUsername(string username)
        {
            ICriterion[] query = { Expression.Eq("Username", username) };
            return (Users)FindOne(typeof(Users), query);
        }

        public static Users GetCurrentUser()
        {
            Users Users = Users.FindByUsername(HttpContext.Current.User.Identity.Name);
            return Users;
        }

        public static bool Login(string username, string password)
        {
            CFS.Models.Users user = CFS.Models.Users.FindByUsername(username);
            if (user != null)
            {
                string hashed = StringToMD5Hash(password);
                if (hashed == user.Password)
                {
                    //user.SaveAndFlush();
                    return true;
                }
                else
                {
                    //user.SaveAndFlush();
                    return false;
                }
            }
            else return false;
        }

        public static string StringToMD5Hash(string inputString)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] encryptedBytes = md5.ComputeHash(Encoding.ASCII.GetBytes(inputString));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < encryptedBytes.Length; i++)
            {
                sb.AppendFormat("{0:x2}", encryptedBytes[i]);
            }
            return sb.ToString();
        }

        public bool Validate(bool type)
        {
            bool result = true;
            return result;
        }

        public bool Validate(string saveType)
        {
            bool result = true;
            Regex phoneNumberRegex = new Regex("^[0-9]+$");
            if (saveType == "delete")
            {
                return result;
            }
            if (saveType == "insert")
            {
                if (Username.Length < 5)
                {
                    messages.Add("Username at least 5 character");
                    result = false;
                }
                if (Email != null && Email != "")
                {
                    //instead of using regex, use system.mail to validate
                    try
                    {
                        MailAddress m = new MailAddress(Email);
                    }
                    catch (FormatException)
                    {
                        messages.Add("Invalid email address format.");
                        return false;
                    }
                }
                if (PhoneNumber != null && PhoneNumber != "")
                {
                    if (!phoneNumberRegex.IsMatch(PhoneNumber))
                    {
                        messages.Add("Invalid phone number format.");
                        return false;
                    }
                }
                return result;
            }
            else if (saveType == "update")
            {
                if (Email != null && Email != "")
                {
                    //instead of using regex, use system.mail to validate
                    try
                    {
                        MailAddress m = new MailAddress(Email);
                    }
                    catch (FormatException)
                    {
                        messages.Add("Invalid email address format.");
                        return false;
                    }
                }
                if (PhoneNumber != null && PhoneNumber != "")
                {
                    if (!phoneNumberRegex.IsMatch(PhoneNumber))
                    {
                        messages.Add("Invalid phone number format.");
                        return false;
                    }
                }
                return result;
            }
            else if (saveType == "changePassword")
            {
                if (NewPassword.Length < 8)
                {
                    messages.Add("New Password length at least 8 character");
                    result = false;
                }
                OldPassword = StringToMD5Hash(OldPassword);
                NewPassword = StringToMD5Hash(NewPassword);
                ConfirmPassword = StringToMD5Hash(ConfirmPassword);
                if (OldPassword != Password)
                {
                    messages.Add("Invalid current password");
                    result = false;
                }
                if (NewPassword != ConfirmPassword) //harusnya dipindah ke client side
                {
                    messages.Add("New & Confirm Password doesn't match.");
                    result = false;
                }

                return result;
            }
            return result;
        }

        public void ResetPassword(string password)
        {
            Password = StringToMD5Hash(password);
            Save();
        }
    }
}