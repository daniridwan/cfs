﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Blanket_Delivery_Order")]
    public class BlanketDeliveryOrder : ActiveRecordBase
    {
        private List<string> messages;
        private long id;
        private string blanketNumber;
        private string customerReferenceNumber;
        private long companyId;
        private long productId;
        private float quantity;
        private string uom;
        private DateTime? startDate;
        private DateTime? endDate;

        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public long Id { get { return id; } set { id = value; } }
        [Property("Blanket_Number")]
        public string BlanketNumber { get { return blanketNumber; } set { blanketNumber = value; } }
        [Property("Customer_Reference_Number")]
        public string CustomerReferenceNumber { get { return customerReferenceNumber; } set { customerReferenceNumber = value; } }
        [Property("Company_Id")]
        public long CompanyId { get { return companyId; } set { companyId = value; } }
        [Property("Product_Id")]
        public long ProductId { get { return productId; } set { productId = value; } }
        [Property("Quantity")]
        public float Quantity { get { return quantity; } set { quantity = value; } }
        [Property("UOM")]
        public string UOM { get { return uom; } set { uom = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Start_Date")]
        public DateTime? StartDate { get { return startDate; } set { startDate = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("End_Date")]
        public DateTime? EndDate { get { return endDate; } set { endDate = value; } }

        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        //helper
        public string CreatedByName { get { return createdBy != null ? createdBy.Name : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Name : string.Empty; } }
        public string CreatedByUsername { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByUsername { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }

        public string CompanyName
        {
            get
            {
                Company company = Company.FindById(CompanyId);
                if (company != null)
                {
                    return company.CompanyName;
                }
                else
                {
                    return "";
                }
            }
        }

        public BlanketDeliveryOrder()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(DeliveryOrder), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(DeliveryOrder));
            DetachedCriteria countCriteria = DetachedCriteria.For<DeliveryOrder>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltBlanketNumber"] != null && param.SearchParam["fltBlanketNumber"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("BlanketNumber", param.SearchParam["fltBlanketNumber"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("BlanketNumber", param.SearchParam["fltBlanketNumber"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltCompany"] != null && param.SearchParam["fltCompany"].Length != 0)
                {
                    criteria.Add(Expression.Eq("CompanyId", Convert.ToInt64(param.SearchParam["fltCompany"])));
                    countCriteria.Add(Expression.Eq("CompanyId", Convert.ToInt64(param.SearchParam["fltCompany"])));
                }
                if (param.SearchParam["fltCustomerReferenceNumber"] != null && param.SearchParam["fltCustomerReferenceNumber"].Length != 0)
                {
                    criteria.CreateAlias("DeliveryOrderDetail", "dod", NHibernate.SqlCommand.JoinType.InnerJoin)
                    .Add(Restrictions.InsensitiveLike("dod.CustomerReferenceNumber", param.SearchParam["fltCustomerReferenceNumber"], MatchMode.Anywhere));
                    countCriteria.CreateAlias("DeliveryOrderDetail", "dod", NHibernate.SqlCommand.JoinType.InnerJoin)
                    .Add(Expression.InsensitiveLike("dod.CustomerReferenceNumber", param.SearchParam["fltCustomerReferenceNumber"], MatchMode.Anywhere));
                }
                //flt timestamp from
                if (param.SearchParam["fltStartDate"] != null && param.SearchParam["fltStartDate"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltStartDate"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Ge("StartDate", startPeriod));
                    countCriteria.Add(Expression.Ge("StartDate", startPeriod));
                }
                //flt timestamp to
                if (param.SearchParam["fltEndDate"] != null && param.SearchParam["fltEndDate"].Length != 0)
                {
                    DateTime endPeriod = DateTime.ParseExact(param.SearchParam["fltEndDate"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Le("EndDate", endPeriod));
                    countCriteria.Add(Expression.Le("EndDate", endPeriod));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<DeliveryOrder>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

    }
}
