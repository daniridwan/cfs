﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;

namespace CFS.Models
{
    public class PagingQuery
    {
        public enum Direction
        {
            Asc,
            Desc
        }

        public int PageIndex;
        public int PageSize;
        public NameValueCollection SearchParam;
        public string SortColumn;
        public Direction SortDirection;
        public long CompanyId;
    }
}
