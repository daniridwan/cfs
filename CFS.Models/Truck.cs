﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Truck")]
    public class Truck : ActiveRecordBase
    {
        private List<string> messages;
        private int truckId;
        private string registrationPlate;
        private string truckType;
        private string tankType;
        private string chassisNumber;
        private string truckTransporter;
        private double truckTareWeight;
        private DateTime? truckKeurTimestamp;
        private DateTime? truckKipTimestamp;
        private DateTime? truckStnkTimestamp;
        private string truckStnkNumber;
        private double tankMaxCapacity;
        private double tankSafeCapacity;
        private string uom;
        private string truckBrand;
        private string emergencyContact;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;
        private IList<TruckCompartment> truckCompartment;

        [PrimaryKey(PrimaryKeyType.Identity, "Truck_Id")]
        public int TruckId { get { return truckId; } set { truckId = value; } }
        [Property("Registration_Plate")]
        public string RegistrationPlate { get { return registrationPlate; } set { registrationPlate = value; } }
        [Property("Truck_Type")]
        public string TruckType { get { return truckType; } set { truckType = value; } }
        [Property("Tank_Type")]
        public string TankType { get { return tankType; } set { tankType = value; } }
        [Property("Chassis_Number")]
        public string ChassisNumber { get { return chassisNumber; } set { chassisNumber = value; } }
        [Property("Truck_Transporter")]
        public string TruckTransporter { get { return truckTransporter; } set { truckTransporter = value; } }
        [Property("Truck_Tare_Weight")]
        public double TruckTareWeight { get { return truckTareWeight; } set { truckTareWeight = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Truck_Stnk_Timestamp")]
        public DateTime? TruckStnkTimestamp { get { return truckStnkTimestamp; } set { truckStnkTimestamp = value; } }
        [Property("Truck_Stnk_Number")]
        public string TruckStnkNumber { get { return truckStnkNumber; } set { truckStnkNumber = value; } }
        [Property("Tank_Max_Capacity")]
        public double TankMaxCapacity { get { return tankMaxCapacity; } set { tankMaxCapacity = value; } }
        [Property("Tank_Safe_Capacity")]
        public double TankSafeCapacity { get { return tankSafeCapacity; } set { tankSafeCapacity = value; } }
        [Property("UOM")]
        public string UOM { get { return uom; } set { uom = value; } }
        [Property("Truck_Brand")]
        public string TruckBrand { get { return truckBrand; } set { truckBrand = value; } }
        [Property("Emergency_Contact")]
        public string EmergencyContact { get { return emergencyContact; } set { emergencyContact = value; } }

        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }

        //relationmapping
        [HasMany(typeof(TruckCompartment), Table = "Truck_Compartment", ColumnKey = "Truck_Id")]
        public IList<TruckCompartment> TruckCompartment { get { return truckCompartment; } set { truckCompartment = value; } }

        public Truck()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Truck), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Truck));
            DetachedCriteria countCriteria = DetachedCriteria.For<Truck>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltRegistrationPlate"] != null && param.SearchParam["fltRegistrationPlate"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("RegistrationPlate", param.SearchParam["fltRegistrationPlate"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("RegistrationPlate", param.SearchParam["fltRegistrationPlate"], MatchMode.Anywhere));
                }
                //if (param.SearchParam["fltRegistrationNumber"] != null && param.SearchParam["fltRegistrationNumber"].Length != 0)
                //{
                //    criteria.Add(Expression.InsensitiveLike("RegistrationNumber", param.SearchParam["fltRegistrationNumber"], MatchMode.Anywhere));
                //    countCriteria.Add(Expression.InsensitiveLike("RegistrationNumber", param.SearchParam["fltRegistrationNumber"], MatchMode.Anywhere));
                //}
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Truck>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static Truck[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Truck>();
            dc.AddOrder(Order.Asc("TruckId"));
            return (Truck[])FindAll(typeof(Truck), dc);
        }

        public static Truck FindById(int id)
        {
            return (Truck)FindByPrimaryKey(typeof(Truck), id);
        }

        public static Truck FindByRegistrationPlate(string registrationPlate)
        {
            ICriterion[] query = { Expression.Eq("RegistrationPlate", registrationPlate) };
            return (Truck)FindOne(typeof(Truck), query);
        }

        //public static Truck FindByIdentificationNumber(string identificationNumber)
        //{
        //    ICriterion[] query = { Expression.Eq("IdentificationNumber", identificationNumber) };
        //    return (Truck)FindOne(typeof(Truck), query);
        //}

        public bool Validate()
        {
            bool result = true;
            //total di compartment
            double totalTankMaxCapacity = 0;
            double totalTankSafeCapacity = 0;
            if (TruckCompartment == null || TruckCompartment.Count == 0)
            {
                messages.Add("Isi data kompartemen truk terlebih dahulu.");
                result = false;
            }
            else
            {
                foreach (TruckCompartment tc in TruckCompartment)
                {
                    totalTankMaxCapacity = totalTankMaxCapacity + tc.MaxCapacity;
                    totalTankSafeCapacity = totalTankSafeCapacity + tc.SafeCapacity;
                }
                //validasi tank max capacity
                if ((totalTankMaxCapacity != TankMaxCapacity) || (totalTankMaxCapacity > TankMaxCapacity))
                {
                    messages.Add("Total Tank Max Capacity tidak sesuai");
                    result = false;
                }
                //validasi tank safe capacity
                if ((totalTankSafeCapacity != TankSafeCapacity) || (totalTankSafeCapacity > TankSafeCapacity))
                {
                    messages.Add("Total Tank Safe Capacity tidak sesuai");
                    result = false;
                }
                //validasi tank max tidak boleh lebih besar dari tank safe
                if ((TankMaxCapacity > TankSafeCapacity) || (totalTankMaxCapacity > totalTankSafeCapacity))
                {
                    messages.Add("Total Tank Max Capacity tidak boleh lebih besar dari Total Tank Safe Capacity ");
                    result = false;
                }
            }

            return result;
        }
    }
}
