﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;


namespace CFS.Models
{
    [ActiveRecord("View_Queue_Truck")]
    public class ViewQueueTruck : ActiveRecordBase
    {
        private List<string> messages;
        private string queueNumber;
        private int truckLoadingId;
        private int truckId;
        private string registrationPlate;
        private string deliveryOrderNumber;
        private DateTime? administrationTimestamp;
        private string status;

        [PrimaryKey(PrimaryKeyType.Identity, "Truck_Loading_Id")]
        public int TruckLoadingId { get { return truckLoadingId; } set { truckLoadingId = value; } }
        [Property("Queue_Number")]
        public string QueueNumber { get { return queueNumber; } set { queueNumber = value; } }
        [Property("Truck_Id")]
        public int TruckId { get { return truckId; } set { truckId = value; } }
        [Property("Registration_Plate")]
        public string RegistrationPlate { get { return registrationPlate; } set { registrationPlate = value; } }
        [Property("Delivery_Order_Number")]
        public string DeliveryOrderNumber { get { return deliveryOrderNumber; } set { deliveryOrderNumber = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Administration_Timestamp")]
        public DateTime? AdministrationTimestamp { get { return administrationTimestamp; } set { administrationTimestamp = value; } }
        [Property("Status")]
        public string Status { get { return status; } set { status = value; } }

        public ViewQueueTruck()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(ViewQueueTruck), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(ViewQueueTruck));
            DetachedCriteria countCriteria = DetachedCriteria.For<ViewQueueTruck>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<ViewQueueTruck>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static PagingResult SelectPagingGateIn(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(ViewQueueTruck), new NHibernateDelegate(SelectPagingCallbackGateIn), param);
        }

        private static object SelectPagingCallbackGateIn(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(ViewQueueTruck));
            DetachedCriteria countCriteria = DetachedCriteria.For<ViewQueueTruck>();
            //find Status GateIn
            criteria.Add(Expression.InsensitiveLike("Status", "Administration"));
            countCriteria.Add(Expression.InsensitiveLike("Status", "Administration"));

            //add param key here
            if (param.SearchParam.HasKeys())
            {
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<ViewQueueTruck>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static ViewQueueTruck[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewQueueTruck>();
            dc.AddOrder(Order.Asc("QueueNumber"));
            return (ViewQueueTruck[])FindAll(typeof(ViewQueueTruck), dc);
        }

        public static ViewQueueTruck[] FindQueue()
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewQueueTruck>();
            dc.Add(Expression.Eq("Status", "Gate In"));
            //dc.Add(Expression.Not(Expression.In("Status", new string[] { "Loading" })));
            //dc.Add(Expression.Not(Expression.In("Status", new string[] { "Complete" })));
            dc.AddOrder(Order.Asc("GateInTimestamp"));
            return (ViewQueueTruck[])FindAll(typeof(ViewQueueTruck), dc);
        }

        public static ViewQueueTruck FindByTruckId(string key)
        {
            ICriterion[] query = { Expression.Eq("TruckId", key) };
            return (ViewQueueTruck)FindOne(typeof(ViewQueueTruck), query);
        }

        public static ViewQueueTruck[] FindByTruckLoadingId(int truckLoadId)
        {
            ICriterion[] query = { Expression.Eq("TruckLoadingId", truckLoadId) };
            return (ViewQueueTruck[])FindAll(typeof(ViewQueueTruck), query);
        }
    }
}
