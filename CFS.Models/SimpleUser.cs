﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Web;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [Castle.ActiveRecord.ActiveRecord("Users", Mutable = false)]
    public class SimpleUser : ActiveRecordBase
    {
        private long userId;
        private string username;
        private string name;
        private IList<Role> role;

        [PrimaryKey(PrimaryKeyType.Identity, "User_Id")]
        public long UserId { get { return userId; } set { userId = value; } }
        [Property("Username")]
        public string Username { get { return username; } set { username = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }

        [ScriptIgnore]
        [JsonIgnore]
        [HasAndBelongsToMany(typeof(Role), Table = "USER_ROLE", ColumnKey = "USER_ID", ColumnRef = "ROLE_ID")]
        public IList<Role> Roles { get { return role; } set { role = value; } }

        public static SimpleUser CreateFromUser(Users u)
        {
            if (u == null)
            {
                return null;
            }
            else
            {
                return (SimpleUser)FindByPrimaryKey(typeof(SimpleUser), u.UserId);
            }

        }

        public static SimpleUser FindById(int id)
        {
            return (SimpleUser)FindByPrimaryKey(typeof(SimpleUser), id);
        }
    }
}
