﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Pump")]
    public class Pump : ActiveRecordBase
    {
        private List<string> messages;
        private int id;
        private string name;
        private float flowrate;
        private string merk;
        private string tipe;
        private string jenis;
        private float motor;
        private string explosionCode;
        private string kelas;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;


        [PrimaryKey(PrimaryKeyType.Identity, "Pump_Id")]
        public int PumpId { get { return id; } set { id = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }
        [Property("Flowrate")]
        public float Flowrate { get { return flowrate; } set { flowrate = value; } }
        [Property("Merk")]
        public string Merk {  get { return merk; } set { merk = value; } }
        [Property("Tipe")]
        public string Tipe { get { return tipe; } set { tipe = value; } }
        [Property("Jenis")]
        public string Jenis { get { return jenis; } set { jenis = value; } }
        [Property("Motor")]
        public float Motor { get { return motor; } set { motor = value; } }
        [Property("Explosion_Code")]
        public string ExplosionCode { get { return explosionCode; } set { explosionCode = value; } }
        [Property("Kelas")]
        public string Kelas { get { return kelas; } set { kelas = value; } }

        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public Pump()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static Pump FindById(int id)
        {
            return (Pump)FindByPrimaryKey(typeof(Pump), id);
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Pump), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Pump));
            DetachedCriteria countCriteria = DetachedCriteria.For<Pump>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Pump>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static Pump[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Pump>();
            dc.AddOrder(Order.Asc("Name"));
            return (Pump[])FindAll(typeof(Pump), dc);
        }

        public bool Validate()
        {
            bool result = true;
            return result;
        }
    }
}
