﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Product")]
    public class Product : ActiveRecordBase
    {
        public List<string> messages;
        private long productId;
        private string productCode;
        private string productName;
        private string productType;
        private float productDensity;
        private string uom;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        private IList tanks = new ArrayList();

        [PrimaryKey(PrimaryKeyType.Identity, "Product_Id")]
        public long ProductId { get { return productId; } set { productId = value; } }
        [Property("Product_Code")]
        public string ProductCode { get { return productCode; } set { productCode = value; } }
        [Property("Product_Name")]
        public string ProductName { get { return productName; } set { productName = value; } }
        [Property("Product_Type")]
        public string ProductType { get { return productType; } set { productType = value; } }
        [Property("Product_Density")]
        public float ProductDensity { get { return productDensity; } set { productDensity = value; } }
        [Property("UOM")]
        public string UOM { get { return uom; } set { uom = value; } }

        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }

        public Product()
        {
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Product), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Product));
            DetachedCriteria countCriteria = DetachedCriteria.For<Product>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltProductName"] != null && param.SearchParam["fltProductName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("ProductName", param.SearchParam["fltProductName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("ProductName", param.SearchParam["fltProductName"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltProductCode"] != null && param.SearchParam["fltProductCode"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("ProductCode", param.SearchParam["fltProductCode"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("ProductCode", param.SearchParam["fltProductCode"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltProductType"] != null && param.SearchParam["fltProductType"].Length != 0)
                {
                    criteria.Add(Expression.Eq("ProductType", param.SearchParam["fltProductType"]));
                    countCriteria.Add(Expression.Eq("ProductType", param.SearchParam["fltProductType"]));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Product>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static Product[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Product>();
            dc.AddOrder(Order.Asc("ProductCode"));
            return (Product[])FindAll(typeof(Product), dc);
        }

        public static Product FindById(long id)
        {
            return (Product)FindByPrimaryKey(typeof(Product), id);
        }

        public static Product FindByName(string name)
        {
            ICriterion[] query = { Expression.Eq("ProductName", name) };
            return (Product)FindOne(typeof(Product), query);
        }

        public static Product FindByProductCode(string code)
        {
            ICriterion[] query = { Expression.Eq("ProductCode", code) };
            return (Product)FindOne(typeof(Product), query);
        }

        public bool Validate()
        {
            bool result = true;
            return result;
        }

    }
}
