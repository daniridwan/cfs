﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;
using System.Web;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CFS.Models
{
    [ActiveRecord("Customer")]
    public class Customer : ActiveRecordBase
    {
        private List<string> messages;
        private int customerId;
        private string customerCode;
        private string customerName;
        private string customerAddress;
        private string customerPhone;
        private string customerCity;
        private string primaryContactNumber;
        private string secondaryContactNumber;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        //helper
        private string salesOrganization;
        private string distributionChannel;

        [PrimaryKey(PrimaryKeyType.Identity, "Customer_Id")]
        public int CustomerId { get { return customerId; } set { customerId = value; } }
        [Property("Customer_Code")]
        public string CustomerCode { get { return customerCode; } set { customerCode = value; } }
        [Property("Customer_Name")]
        public string CustomerName { get { return customerName; } set { customerName = value; } }
        [Property("Customer_Address")]
        public string CustomerAddress { get { return customerAddress; } set { customerAddress = value; } }
        [Property("Customer_Phone")]
        public string CustomerPhone { get { return customerPhone; } set { customerPhone = value; } }
        [Property("Customer_City")]
        public string CustomerCity { get { return customerCity; } set { customerCity = value; } }
        [Property("Primary_Contact_Number")]
        public string PrimaryContactNumber { get { return primaryContactNumber; } set { primaryContactNumber = value; } }
        [Property("Secondary_Contact_Number")]
        public string SecondaryContactNumber { get { return secondaryContactNumber; } set { secondaryContactNumber = value; } }
        
        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }
        //helper
        public String SalesOrganization { get { return salesOrganization; } set { salesOrganization = value; } }
        public String DistributionChannel { get { return distributionChannel; } set { distributionChannel = value; } }

        public Customer()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Customer), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Customer));
            DetachedCriteria countCriteria = DetachedCriteria.For<Customer>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltShipToCode"] != null && param.SearchParam["fltShipToCode"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("ShipToCode", param.SearchParam["fltShipToCode"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("ShipToCode", param.SearchParam["fltShipToCode"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltShipToName"] != null && param.SearchParam["fltShipToName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("ShipToName", param.SearchParam["fltShipToName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("ShipToName", param.SearchParam["fltShipToName"], MatchMode.Anywhere));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Customer>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public bool Validate()
        {
            bool result = true;
            return result;
        }

        public static Customer[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Customer>();
            dc.AddOrder(Order.Asc("CustomerName"));
            return (Customer[])FindAll(typeof(Customer), dc);
        }

        public static Customer FindById(int id)
        {
            return (Customer)FindByPrimaryKey(typeof(Customer), id);
        }

        public static Customer FindByCode(string customerCode)
        {
            ICriterion[] query = { Expression.Eq("CustomerCode", customerCode) };
            return (Customer)FindOne(typeof(Customer), query);
        }
        public static Customer FindByShipToName(string customerName)
        {
            ICriterion[] query = { Expression.Eq("CustomerName", customerName) };
            return (Customer)FindOne(typeof(Customer), query);
        }
    }
}
