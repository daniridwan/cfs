﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Driver_Notes")]
    public class DriverNotes : ActiveRecordBase
    {
        private List<string> messages;
        private int driverNotesId;
        private int driverId;
        private DateTime? date;
        private string notes;

        [PrimaryKey(PrimaryKeyType.Identity, "Driver_Notes_Id")]
        public int DriverNotesId { get { return driverNotesId; } set { driverNotesId = value; } }
        [Property("Driver_Id")]
        public int DriverId { get { return driverId; } set { driverId = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Date")]
        public DateTime? Date { get { return date; } set { date = value; } }
        [Property("Notes")]
        public string Notes { get { return notes; } set { notes = value; } }

        public DriverNotes()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static DriverNotes FindById(int id)
        {
            return (DriverNotes)FindByPrimaryKey(typeof(DeliveryOrderDetail), id);
        }

        public static DriverNotes[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<DriverNotes>();
            dc.AddOrder(Order.Asc("DriverNotesId"));
            return (DriverNotes[])FindAll(typeof(DriverNotes), dc);
        }
    }
}
