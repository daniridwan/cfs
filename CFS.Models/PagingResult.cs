﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace CFS.Models
{
    public class PagingResult
    {
        [JsonProperty("success")]
        public bool Success;
        [JsonProperty("message")]
        public string Message;
        [JsonProperty("page")]
        public int PageIndex;
        [JsonProperty("totalRecords")]
        public Int64 TotalRecords;
        [JsonProperty("records")]
        public IList Records;

        public PagingResult()
        {
            PageIndex = 0;
            TotalRecords = 0;
            Message = "OK";
            Success = true;
        }
    }
}
