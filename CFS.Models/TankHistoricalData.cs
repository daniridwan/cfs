﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Tank_Historical_Data")]
    public class TankHistoricalData : ActiveRecordBase
    {
        private List<string> messages;
        private int id;
        private int tankId;
        private float liquidLevel;
        private float liquidWater;
        private float liquidTemperature;
        private float liquidDensity;
        private float liquidDensity15;
        private float liquidVolObs;
        private float liquidVol15;
        private float liquidMass;
        private float liquidPressure;
        private float tankHeight;
        private float tankVolume;
        private float liquidVolumeCorrectionFactor;
        private float liquidDensityMultiplier;
        private float totalObservedVolume;
        private DateTime? timestamp;

        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public int Id { get { return id; } set { id = value; } }
        [Property("Tank_Id")]
        public int TankId { get { return tankId; } set { tankId = value; } }
        [Property("Liquid_Level")]
        public float LiquidLevel { get { return liquidLevel; } set { liquidLevel = value; } }
        [Property("Liquid_Water")]
        public float LiquidWater { get { return liquidWater; } set { liquidWater = value; } }
        [Property("Liquid_Temperature")]
        public float LiquidTemperature { get { return liquidTemperature; } set { liquidTemperature = value; } }
        [Property("Liquid_Density")]
        public float LiquidDensity { get { return liquidDensity; } set { liquidDensity = value; } }
        [Property("Liquid_Density15")]
        public float LiquidDensity15 { get { return liquidDensity15; } set { liquidDensity15 = value; } }
        [Property("Liquid_VolObs")]
        public float LiquidVolObs { get { return liquidVolObs; } set { liquidVolObs = value; } }
        [Property("Liquid_Vol15")]
        public float LiquidVol15 { get { return liquidVol15; } set { liquidVol15 = value; } }
        [Property("Liquid_Mass")]
        public float LiquidMass { get { return liquidMass; } set { liquidMass = value; } }
        [Property("Liquid_Pressure")]
        public float LiquidPressure { get { return liquidPressure; } set { liquidPressure = value; } }
        [Property("Tank_Height")]
        public float TankHeight { get { return tankHeight; } set { tankHeight = value; } }
        [Property("Tank_Volume")]
        public float TankVolume { get { return tankVolume; } set { tankVolume = value; } }
        [Property("Liquid_Volume_Correction_Factor")]
        public float LiquidVolumeCorrectionFactor { get { return liquidVolumeCorrectionFactor; } set { liquidVolumeCorrectionFactor = value; } }
        [Property("Liquid_Density_Multiplier")]
        public float LiquidDensityMultiplier { get { return liquidDensityMultiplier; } set { liquidDensityMultiplier = value; } }
        [Property("Total_Observed_Volume")]
        public float TotalObservedVolume { get { return totalObservedVolume; } set { totalObservedVolume = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Timestamp")]
        public DateTime? Timestamp { get { return timestamp; } set { timestamp = value; } }

        //[JsonIgnore]
        //[OneToOne]
        //public Tank Tank { get { return tank; } set { tank = value; } }
        public string TankName
        {
            get
            {
                Tank tank = Tank.FindById(TankId);
                if (tank != null)
                {
                    return tank.Name;
                }
                else
                {
                    return "";
                }
            }
        }

        public TankHistoricalData()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(TankHistoricalData), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(TankHistoricalData));
            DetachedCriteria countCriteria = DetachedCriteria.For<TankHistoricalData>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {

                //flt tank name
                if (param.SearchParam["fltTank"] != null && param.SearchParam["fltTank"].Length != 0)
                {
                    int tank = int.Parse(param.SearchParam["fltTank"]);
                    criteria.Add(Expression.Eq("TankId", tank));
                    countCriteria.Add(Expression.Eq("TankId", tank));
                }
                //flt timestamp from
                if (param.SearchParam["fltTimestampFrom"] != null && param.SearchParam["fltTimestampFrom"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampFrom"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Gt("Timestamp", startPeriod));
                    countCriteria.Add(Expression.Gt("Timestamp", startPeriod));
                }
                //flt timestamp to
                if (param.SearchParam["fltTimestampTo"] != null && param.SearchParam["fltTimestampTo"].Length != 0)
                {
                    DateTime endPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampTo"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Le("Timestamp", endPeriod));
                    countCriteria.Add(Expression.Le("Timestamp", endPeriod));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<TankHistoricalData>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static TankHistoricalData FindById(int id)
        {
            return (TankHistoricalData)FindByPrimaryKey(typeof(TankHistoricalData), id);
        }

        public static TankHistoricalData[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<TankHistoricalData>();
            dc.AddOrder(Order.Asc("Timestamp"));
            return (TankHistoricalData[])FindAll(typeof(TankHistoricalData), dc);
        }
    }
}

