﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Driver")]
    public class Driver : ActiveRecordBase
    {
        private List<string> messages;
        private int driverId;
        private string badgeNumber;
        private string identificationNumber;
        private string name;
        private string placeOfBirth;
        private DateTime? dateOfBirth;
        private string driverPhoto;
        private string driverKtpNumber;
        private string driverSimNumber;
        private string driverSimType;
        private DateTime? driverSimExpiryDate;
        private DateTime? driverKimExpiryDate;
        private string driverCompany;
        private string driverAddress;
        private string driverPhoneNumber;
        private string driverMobilePhoneNumber;
        private bool isActive;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        private IList<DriverNotes> driverNotes;

        [PrimaryKey(PrimaryKeyType.Identity, "Driver_Id")]
        public int DriverId { get { return driverId; } set { driverId = value; } }
        [Property("Badge_Number")]
        public string BadgeNumber { get { return badgeNumber; } set { badgeNumber = value; } }
        [Property("Identification_Number")]
        public string IdentificationNumber { get { return identificationNumber; } set { identificationNumber = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }
        [Property("Place_Of_Birth")]
        public string PlaceOfBirth { get { return placeOfBirth; } set { placeOfBirth = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Date_Of_Birth")]
        public DateTime? DateOfBirth { get { return dateOfBirth; } set { dateOfBirth = value; } }
        [Property("Driver_Photo")]
        public string DriverPhoto { get { return driverPhoto; } set { driverPhoto = value; } }
        [Property("Driver_Ktp_Number")]
        public string DriverKtpNumber { get { return driverKtpNumber; } set { driverKtpNumber = value; } }
        [Property("Driver_Sim_Number")]
        public string DriverSimNumber { get { return driverSimNumber; } set { driverSimNumber = value; } }
        [Property("Driver_Sim_Type")]
        public string DriverSimType { get { return driverSimType; } set { driverSimType = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Driver_Sim_Expiry_Date")]
        public DateTime? DriverSimExpiryDate { get { return driverSimExpiryDate; } set { driverSimExpiryDate = value; } } 
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Driver_Kim_Expiry_Date")]
        public DateTime? DriverKimExpiryDate { get { return driverKimExpiryDate; } set { driverKimExpiryDate = value; } }
        [Property("Driver_Company")]
        public string DriverCompany { get { return driverCompany; } set { driverCompany = value; } }
        [Property("Driver_Address")]
        public string DriverAddress { get { return driverAddress; } set { driverAddress = value; } }
        [Property("Driver_Phone_Number")]
        public string DriverPhoneNumber { get { return driverPhoneNumber; } set { driverPhoneNumber = value; } }
        [Property("Driver_Mobile_Phone_Number")]
        public string DriverMobilePhoneNumber { get { return driverMobilePhoneNumber; } set { driverMobilePhoneNumber = value; } }
        [Property("Is_Active")]
        public bool IsActive { get { return isActive; } set { isActive = value; } }

        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }

        //relationmapping
        [HasMany(typeof(DriverNotes), Table = "Driver_Notes", ColumnKey = "Driver_Id")]
        public IList<DriverNotes> DriverNotes { get { return driverNotes; } set { driverNotes = value; } }

        public bool HasPhoto{
            get{
                bool result = false;
                if (DriverPhoto != null)
                {
                    result = true;
                }
                return result;
            }
        }

        public Driver()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Driver), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Driver));
            DetachedCriteria countCriteria = DetachedCriteria.For<Driver>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltKIMNumber"] != null && param.SearchParam["fltKIMNumber"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("BadgeNumber", param.SearchParam["fltKIMNumber"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("BadgeNumber", param.SearchParam["fltKIMNumber"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltKTPNumber"] != null && param.SearchParam["fltKTPNumber"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("DriverKtpNumber", param.SearchParam["fltKTPNumber"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("DriverKtpNumber", param.SearchParam["fltKTPNumber"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltStatus"] != null && param.SearchParam["fltStatus"].Length != 0)
                {
                    int isActive = int.Parse(param.SearchParam["fltStatus"]);
                    bool status = false;
                    if (isActive == 1)
                    {
                        status = true;
                    }
                    criteria.Add(Expression.Eq("IsActive", status));
                    countCriteria.Add(Expression.Eq("IsActive", status));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Driver>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static Driver[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Driver>();
            dc.AddOrder(Order.Asc("DriverId"));
            return (Driver[])FindAll(typeof(Driver), dc);
        }

        public static Driver[] FindAllActive()
        {
            DetachedCriteria dc = DetachedCriteria.For<Driver>();
            dc.Add(Expression.Eq("IsActive", true));
            dc.Add(Expression.Ge("DriverKimExpiryDate", DateTime.Now));
            dc.AddOrder(Order.Asc("DriverId"));
            return (Driver[])FindAll(typeof(Driver), dc);
        }

        public static Driver FindById(int id)
        {
            return (Driver)FindByPrimaryKey(typeof(Driver), id);
        }

        public static Driver FindByKey(string id)
        {
            ICriterion[] query = { Expression.Eq("IdentificationNumber", id) };
            return (Driver)FindOne(typeof(Driver), query);
        }

        public static PagingResult GetTotalRegistered(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Driver), new NHibernateDelegate(TotalDriver), param);
        }

        private static object TotalDriver(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria<Driver>();

            if (param.SearchParam.HasKeys())
            {
            }
            criteria.SetProjection(Projections.ProjectionList()
                                       .Add(Projections.Count("DriverId")));
            IList records = criteria.List();

            PagingResult pr = new PagingResult();
            pr.Records = records;

            return pr;
        }

        public bool Validate()
        {
            bool result = true;
            Regex regex = new Regex("^[0-9]+$"); //number only
            if (!regex.IsMatch(DriverSimNumber))
            {
                messages.Add("No Sim hanya dapat diisi dengan angka");
                result = false;
            }
            if (!regex.IsMatch(DriverKtpNumber))
            {
                messages.Add("KTP hanya dapat diisi dengan angka");
                result = false;
            }
            if (DriverSimNumber.Length < 12) {
                messages.Add("SIM minimal 14 digit");
                result = false;
            }
            if (DriverKtpNumber.Length < 16)
            {
                messages.Add("KTP minimal 16 digit");
                result = false;
            }

            return result;
        }
    }
}
