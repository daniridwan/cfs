﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Company")]
    public class Company : ActiveRecordBase
    {
        public List<string> messages;
        private long companyId;
        private string companyName;
        private string address;
        private string email;
        private string phoneNumber;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        [PrimaryKey(PrimaryKeyType.Identity, "Company_Id")]
        public long CompanyId { get { return companyId; } set { companyId = value; } }
        [Property("Company_Name")]
        public string CompanyName { get { return companyName; } set { companyName = value; } }
        [Property("Address")]
        public string Address { get { return address; } set { address = value; } }
        [Property("Email")]
        public string Email { get { return email; } set { email = value; } }
        [Property("Phone_Number")]
        public string PhoneNumber { get { return phoneNumber; } set { phoneNumber = value; } }

        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Name : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Name : string.Empty; } }

        public Company()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Company), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Company));
            DetachedCriteria countCriteria = DetachedCriteria.For<Company>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltCompanyName"] != null && param.SearchParam["fltCompanyName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("CompanyName", param.SearchParam["fltCompanyName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("CompanyName", param.SearchParam["fltCompanyName"], MatchMode.Anywhere));
                }
            }
            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Company>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static Company[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Company>();
            dc.AddOrder(Order.Asc("CompanyName"));
            return (Company[])FindAll(typeof(Company), dc);
        }

        public static Company FindById(long id)
        {
            return (Company)FindByPrimaryKey(typeof(Company), id);
        }

        public static Company FindById(long? id)
        {
            return (Company)FindByPrimaryKey(typeof(Company), id);
        }

        public bool Validate()
        {
            bool result = true;
            return result;
        }
    }
}
