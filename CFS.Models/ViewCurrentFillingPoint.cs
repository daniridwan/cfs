﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("View_Current_Filling_Point", Mutable = false)]
    public class ViewCurrentFillingPoint : ActiveRecordBase
    {
        private List<string> messages;
        private int fillingPointId;
        private string fillingPointName;
        private int tankId;
        private string tankName;
        private long productId;
        private string productName;
        private long companyId;
        private string companyName;
        private int pumpId;
        private string pumpName;
        private float flowrate;

        [PrimaryKey(PrimaryKeyType.Assigned, "Filling_Point_Id")]
        public int FillingPointId { get { return fillingPointId; } set { fillingPointId = value; } }
        [Property("Filling_Point_Name")]
        public string FillingPointName { get { return fillingPointName; } set { fillingPointName = value; } }
        [Property("Tank_Id")]
        public int TankId { get { return tankId; } set { tankId = value; } }
        [Property("Tank_Name")]
        public string TankName { get { return tankName; } set { tankName = value; } }
        [Property("Product_Id")]
        public long ProductId { get { return productId; } set { productId = value; } }
        [Property("Product_Name")]
        public string ProductName { get { return productName; } set { productName = value; } }
        [Property("Company_Id")]
        public long CompanyId { get { return companyId; } set { companyId = value; } }
        [Property("Company_Name")]
        public string CompanyName { get { return companyName; } set { companyName = value; } }
        [Property("Pump_Id")]
        public int PumpId { get { return pumpId; } set { pumpId = value; } }
        [Property("Pump_Name")]
        public string PumpName { get { return pumpName; } set { pumpName = value; } }
        [Property("Flowrate")]
        public float Flowrate { get { return flowrate; } set { flowrate = value; } }

        public ViewCurrentFillingPoint()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static ViewCurrentFillingPoint[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewCurrentFillingPoint>();
            dc.AddOrder(Order.Asc("FillingPointId"));
            return (ViewCurrentFillingPoint[])FindAll(typeof(ViewCurrentFillingPoint), dc);
        }

        public static ViewCurrentFillingPoint[] FindFillingPointByTankAndProduct(long productId, string tankName)
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewCurrentFillingPoint>();
            dc.Add(Expression.Eq("ProductId", productId));
            dc.Add(Expression.Eq("TankName", tankName));
            dc.AddOrder(Order.Asc("FillingPointId"));
            return (ViewCurrentFillingPoint[])FindAll(typeof(ViewCurrentFillingPoint), dc);
        }

        public static ViewCurrentFillingPoint[] FindFillingPointByCompanyAndProduct(long companyId, long productId)
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewCurrentFillingPoint>();
            dc.Add(Expression.Eq("ProductId", productId));
            dc.Add(Expression.Eq("CompanyId", companyId));
            dc.AddOrder(Order.Asc("FillingPointId"));
            return (ViewCurrentFillingPoint[])FindAll(typeof(ViewCurrentFillingPoint), dc);
        }
    }
}
