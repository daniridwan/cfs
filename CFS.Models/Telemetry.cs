﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CFS.Models
{
    public class Telemetry
    {
        public string title { get; set; } // notification title
        public string icon { get; set; } // example : glyphicon glyphicon-home
        public string text { get; set; } // insert message here
        public string status { get; set; } //SUCCESS / FAIL / REMINDER
        public string url { get; set; }
        public DateTime? dateTime { get; set; }
    }
}
