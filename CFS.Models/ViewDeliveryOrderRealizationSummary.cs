﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("View_Delivery_Order_Realization_Summary_2", Mutable = false)]
    public class ViewDeliveryOrderRealizationSummary : ActiveRecordBase
    {
        private List<string> messages;
        //private int planningId;
        private string deliveryOrderNumber;
        //private string transactionNumber;
        private string registrationPlate;
        //private string companyName;
        private string driverName;
        private string product;
        //private string customerReferenceNumber;
        private DateTime? administrationTimestamp;
        private DateTime? gateInTimestamp;
        private DateTime? gateOutTimestamp;
        //private DateTime? goodIssueTimestamp;
        private float preset;
        private float confirmedQuantity;
        private float actual;
        //private string uom;
        //private string fillingPointGroup;
        //private string fillingPointName;
        //private string tankName;
        //private float initialWeight;
        //private float finalWeight;
        //private float netWeight;
        //private long vesselDocumentId;
        //private string vesselNo;
        //private string blNo;
        private string unit;
        private string administrationUser;
        private string loadedUser;
        private double tankSafeCapacity;
        private string loadedCFSUser;
        private string pumpName;
        private string flowrate;
        //private float productDensity;
        //private string remarks;
        //private float blCost;
        //private string sppbNo;
        //private string sppbPlb;
        //private string type;
        //private float blQuantity;
        //private double actual2;
        //private double correctionFactor;
        //private double flowmeterQuantity;

        //[PrimaryKey(PrimaryKeyType.Assigned, "Planning_Id")]
        //public int PlanningId { get { return planningId; } set { planningId = value; } }
        [PrimaryKey(PrimaryKeyType.Assigned, "Delivery_Order_Number")]
        public string DeliveryOrderNumber { get { return deliveryOrderNumber; } set { deliveryOrderNumber = value; } }
        //[Property("Transaction_Number")]
        //public string TransactionNumber { get { return transactionNumber; } set { transactionNumber = value; } }
        [Property("Registration_Plate")]
        public string RegistrationPlate { get { return registrationPlate; } set { registrationPlate = value; } }
        [Property("Driver_Name")]
        public string DriverName { get { return driverName; } set { driverName = value; } }
        [Property("Product")]
        public string Product { get { return product; } set { product = value; } }
        //[Property("Customer_Reference_Number")]
        //public string CustomerReferenceNumber { get { return customerReferenceNumber; } set { customerReferenceNumber = value; } }
        [Property("Preset")]
        public float Preset { get { return preset; } set { preset = value; } }
        [Property("Confirmed_Quantity")]
        public float ConfirmedQuantity { get { return confirmedQuantity; } set { confirmedQuantity = value; } }
        [Property("Actual")]
        public float Actual { get { return actual; } set { actual = value; } }
        [Property("Unit")]
        public string Unit { get { return unit; } set { unit = value; } }
        //[Property("Company_Name")]
        //public string CompanyName { get { return companyName; } set { companyName = value; } }
        //[Property("UOM")]
        //public string UOM { get { return uom; } set { uom = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Administration_Timestamp")]
        public DateTime? AdministrationTimestamp { get { return administrationTimestamp; } set { administrationTimestamp = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Gate_In_Timestamp")]
        public DateTime? GateInTimestamp { get { return gateInTimestamp; } set { gateInTimestamp = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Gate_Out_Timestamp")]
        public DateTime? GateOutTimestamp { get { return gateOutTimestamp; } set { gateOutTimestamp = value; } }
        //[JsonConverter(typeof(IsoDateTimeConverter))]
        //[Property("Good_Issue_Timestamp")]
        //public DateTime? GoodIssueTimestamp { get { return goodIssueTimestamp; } set { goodIssueTimestamp = value; } }
        //[Property("Filling_Point_Group")]
        //public string FillingPointGroup { get { return fillingPointGroup; } set { fillingPointGroup = value; } }
        //[Property("Filling_Point_Name")]
        //public string FillingPointName { get { return fillingPointName; } set { fillingPointName = value; } }
        //[Property("Tank_Name")]
        //public string TankName { get { return tankName; } set { tankName = value; } }
        //[Property("Initial_Weight")]
        //public float InitialWeight { get { return initialWeight; } set { initialWeight = value; } }
        //[Property("Final_Weight")]
        //public float FinalWeight { get { return finalWeight; } set { finalWeight = value; } }
        //[Property("Net_Weight")]
        //public float NetWeight { get { return netWeight; } set { netWeight = value; } }
        //[Property("Vessel_Document_Id")]
        //public long VesselDocumentId { get { return vesselDocumentId; } set { vesselDocumentId = value; } }
        //[Property("Vessel_No")]
        //public string VesselNo { get { return vesselNo; } set { vesselNo = value; } }
        //[Property("BL_No")]
        //public string BlNo { get { return blNo; } set { blNo = value; } }
        [Property("Administration_User")]
        public string AdministrationUser { get { return administrationUser; } set { administrationUser = value; } }
        [Property("Loaded_User")]
        public string LoadedUser { get { return loadedUser; } set { loadedUser = value; } }
        [Property("Tank_Safe_Capacity")]
        public double TankSafeCapacity { get { return tankSafeCapacity; } set { tankSafeCapacity = value; } }
        [Property("Loaded_CFS_User")]
        public string LoadedCFSUser { get { return loadedCFSUser; } set { loadedCFSUser = value; } }
        [Property("Pump_Name")]
        public string PumpName { get { return pumpName; } set { pumpName = value; } }
        [Property("Flowrate")]
        public string Flowrate { get { return flowrate; } set { flowrate = value; } }
        //[Property("Product_Density")]
        //public float ProductDensity { get { return productDensity; } set { productDensity = value; } }
        //[Property("Remarks")]
        //public string Remarks { get { return remarks; } set { remarks = value; } }
        //[Property("BL_Cost")]
        //public float BlCost { get { return blCost; } set { blCost = value; } }
        //[Property("SPPB_No")]
        //public string SppbNo { get { return sppbNo; } set { sppbNo = value; } }
        //[Property("SPPB_PLB")]
        //public string SppbPlb { get { return sppbPlb; } set { sppbPlb = value; } }
        //[Property("Type")]
        //public string Type { get { return type; } set { type = value; } }
        //[Property("BL_Quantity")]
        //public float BlQuantity { get { return blQuantity; } set { blQuantity = value; } }
        //[Property("Actual_2")]
        //public double Actual2 { get { return actual2; } set { actual2 = value; } }
        //[Property("Correction_Factor")]
        //public double CorrectionFactor { get { return correctionFactor; } set { correctionFactor = value; } }
        //[Property("Flowmeter_Quantity")]
        //public double FlowmeterQuantity { get { return flowmeterQuantity; } set { flowmeterQuantity = value; } }

        ////public float Quantity
        ////{
        ////    get
        ////    {
        ////        if (UOM == "Kg")
        ////        {
        ////            return NetWeight;
        ////        }
        ////        else if (UOM == "KL")
        ////        {
        ////            return Actual;
        ////        }
        ////        else
        ////        {
        ////            return 0;
        ////        }
        ////    }
        ////}

        //public string Sppb
        //{
        //    get
        //    {
        //        if (Type == "Import PLB")
        //        {
        //            return SppbPlb;
        //        }
        //        else if(Type == "Import")
        //        {
        //            return SppbNo;
        //        }
        //        else
        //        {
        //            return "";
        //        }
        //    }
        //}

        //public float UnitPrice
        //{
        //    get
        //    {
        //        if (UOM == "Kg" && BlCost >= 0)
        //        {
        //            float unitPrice = BlCost / BlQuantity;
        //            return unitPrice * netWeight;
        //            //return NetWeight;
        //        }
        //        else if (UOM == "KL" && BlCost >= 0)
        //        {
        //            float unitPrice = BlCost / BlQuantity;
        //            return unitPrice * Actual;
        //            //return Actual;
        //        }
        //        else
        //        {
        //            return 0;
        //        }
        //    }
        //}

        public ViewDeliveryOrderRealizationSummary()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(ViewDeliveryOrderRealizationSummary), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(ViewDeliveryOrderRealizationSummary));
            DetachedCriteria countCriteria = DetachedCriteria.For<ViewDeliveryOrderRealizationSummary>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltDeliveryOrderNumber"] != null && param.SearchParam["fltDeliveryOrderNumber"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("DeliveryOrderNumber", param.SearchParam["fltDeliveryOrderNumber"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("DeliveryOrderNumber", param.SearchParam["fltDeliveryOrderNumber"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltRegistrationPlate"] != null && param.SearchParam["fltRegistrationPlate"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("RegistrationPlate", param.SearchParam["fltRegistrationPlate"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("RegistrationPlate", param.SearchParam["fltRegistrationPlate"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltProductName"] != null && param.SearchParam["fltProductName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("ProductName", param.SearchParam["fltProductName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("ProductName", param.SearchParam["fltProductName"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltCompanyName"] != null && param.SearchParam["fltCompanyName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("CompanyName", param.SearchParam["fltCompanyName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("CompanyName", param.SearchParam["fltCompanyName"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltFillingPointGroup"] != null && param.SearchParam["fltFillingPointGroup"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("FillingPointGroup", param.SearchParam["fltFillingPointGroup"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("FillingPointGroup", param.SearchParam["fltFillingPointGroup"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltBlNo"] != null && param.SearchParam["fltBlNo"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("BlNo", param.SearchParam["fltBlNo"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("BlNo", param.SearchParam["fltBlNo"], MatchMode.Anywhere));
                }
                //flt timestamp from
                if (param.SearchParam["fltTimestampFrom"] != null && param.SearchParam["fltTimestampFrom"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampFrom"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Gt("GateOutTimestamp", startPeriod));
                    countCriteria.Add(Expression.Gt("GateOutTimestamp", startPeriod));
                }
                //flt timestamp to
                if (param.SearchParam["fltTimestampTo"] != null && param.SearchParam["fltTimestampTo"].Length != 0)
                {
                    DateTime endPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampTo"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Le("GateOutTimestamp", endPeriod));
                    countCriteria.Add(Expression.Le("GateOutTimestamp", endPeriod));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<ViewDeliveryOrderRealizationSummary>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }
    }
}
