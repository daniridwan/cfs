﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Filling_Point")]
    public class FillingPoint : ActiveRecordBase
    {
        private List<string> messages;
        private int fillingPointId;
        private string name;
        private Tank tankId;
        private float correctionFactor;
        private int pumpId;
        private DateTime? expiredDate;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        private long idHelper;

        [PrimaryKey(PrimaryKeyType.Identity, "Filling_Point_Id")]
        public int FillingPointId { get { return fillingPointId; } set { fillingPointId = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }
        [ScriptIgnore]
        [BelongsTo("Tank_Id")]
        public Tank TankId { get { return tankId; } set { tankId = value; } }
        [Property("Correction_Factor")]
        public float CorrectionFactor { get { return correctionFactor; } set { correctionFactor = value; } }
        [Property("Pump_Id")]
        public int PumpId { get { return pumpId; } set { pumpId = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Expired_Date")]
        public DateTime? ExpiredDate { get { return expiredDate; } set { expiredDate = value; } }

        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }

        public string TankName { get { return tankId != null ? tankId.Name : string.Empty; } }
        public long TankIdHelper { get { return tankId != null ? tankId.TankId : 0; } }
        public string ProductName { get { return tankId != null ? tankId.ProductName : string.Empty; } }
        public string CompanyName { get { return tankId != null ? tankId.CompanyName : string.Empty; } }

        public long IdHelper { get { return idHelper; } set { idHelper = value; } }

        public string PumpName
        {
            get
            {
                Pump pump = Pump.FindById(PumpId);
                if (pump != null)
                {
                    return pump.Name;
                }
                else
                {
                    return "";
                }
            }
        }

        public float Flowrate
        {
            get
            {
                Pump pump = Pump.FindById(PumpId);
                if (pump != null)
                {
                    return pump.Flowrate;
                }
                else
                {
                    return 0;
                }
            }
        }

        public FillingPoint()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(FillingPoint), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(FillingPoint));
            DetachedCriteria countCriteria = DetachedCriteria.For<FillingPoint>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<FillingPoint>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static FillingPoint FindById(int id)
        {
            return (FillingPoint)FindByPrimaryKey(typeof(FillingPoint), id);
        }

        public static FillingPoint[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<FillingPoint>();
            dc.AddOrder(Order.Asc("FillingPointId"));
            return (FillingPoint[])FindAll(typeof(FillingPoint), dc);
        }

        public static FillingPoint FindByName(string name)
        {
            ICriterion[] query = { Expression.Eq("Name", name) };
            return (FillingPoint)FindOne(typeof(FillingPoint), query);
        }

        public static FillingPoint[] FindActiveByGroup(string group)
        {
            DetachedCriteria dc = DetachedCriteria.For<FillingPoint>();
            dc.Add(Expression.Eq("FillingPointGroup", group));
            dc.AddOrder(Order.Asc("Name"));
            return (FillingPoint[])FindAll(typeof(FillingPoint), dc);
        }

        public static FillingPoint[] FindActiveByGroupAndCode(string group, string code)
        {
            DetachedCriteria dc = DetachedCriteria.For<FillingPoint>();
            dc.Add(Expression.Eq("FillingPointGroup", group));
            dc.Add(Expression.Eq("ProductCode", code));
            dc.AddOrder(Order.Asc("Name"));
            return (FillingPoint[])FindAll(typeof(FillingPoint), dc);
        }

        public static FillingPoint[] FindActiveByCode(string code)
        {
            DetachedCriteria dc = DetachedCriteria.For<FillingPoint>();
            dc.Add(Expression.Eq("ProductCode", code));
            dc.AddOrder(Order.Asc("Name"));
            return (FillingPoint[])FindAll(typeof(FillingPoint), dc);
        }

        public bool Validate()
        {
            bool result = true;
            //validasi nama tdk boleh duplikat
            //if (fillingPoint != null || FillingPointId != 0)
            //{
            //    messages.Add("Nama Filling Point harus unik / tidak boleh sama");
            //    result = false;
            //}
            float tryParseHandler;
            if(float.TryParse(CorrectionFactor.ToString(),out tryParseHandler)){
                if (tryParseHandler == 0)
                {
                    messages.Add("Invalid Correction Factor");
                    result = false;
                }
                //if(CorrectionFactor >= 1)
                //{
                //    messages.Add("Invalid Format for Correction Factor, please use . instead of ,");
                //    result = false;
                //}
            }

            return result;
        }
    }
}
