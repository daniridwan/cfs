﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Truck_Loading_Planning")]
    public class TruckLoadingPlanning : ActiveRecordBase
    {
        private List<string> messages;
        private int planningId;
        private int truckLoadingId;
        private string deliveryOrderNumber;
        private double preset;
        private string uom;
        private Product productId;
        private string fillingPointName;
        private string fillingPointGroup;
        private string tankName;
        private double actual;
        private double initialWeight;
        private double finalWeight;
        private long vesselDocumentId;
        private string pumpName;
        private float flowrate;
        private float productDensity;
        private long plbDocumentId;
        private double confirmedQuantity;
        private double actual2;
        private double correctionFactor;
        private double flowmeterQuantity;

        [PrimaryKey(PrimaryKeyType.Identity, "Planning_Id")]
        public int PlanningId { get { return planningId; } set { planningId = value; } }
        [Property("Truck_Loading_Id")]
        public int TruckLoadingId { get { return truckLoadingId; } set { truckLoadingId = value; } }
        [Property("Delivery_Order_Number")]
        public string DeliveryOrderNumber { get { return deliveryOrderNumber; } set { deliveryOrderNumber = value; } }
        [Property("Preset")]
        public double Preset { get { return preset; } set { preset = value; } }
        [ScriptIgnore]
        [BelongsTo("Product_Id")]
        public Product ProductId { get { return productId; } set { productId = value; } }
        [Property("UOM")]
        public string UOM { get { return uom; } set { uom = value; } }
        [Property("Filling_Point_Name")]
        public string FillingPointName { get { return fillingPointName; } set { fillingPointName = value; } }
        [Property("Filling_Point_Group")]
        public string FillingPointGroup { get { return fillingPointGroup; } set { fillingPointGroup = value; } }
        [Property("Tank_Name")]
        public string TankName { get { return tankName; } set { tankName = value; } }
        [Property("Confirmed_Quantity")]
        public double ConfirmedQuantity { get { return confirmedQuantity; } set { confirmedQuantity = value; } }
        [Property("Actual")]
        public double Actual { get { return actual; } set { actual = value; } }
        [Property("Initial_Weight")]
        public double InitialWeight { get { return initialWeight; } set { initialWeight = value; } }
        [Property("Final_Weight")]
        public double FinalWeight { get { return finalWeight; } set { finalWeight = value; } }
        [Property("Vessel_Document_Id")]
        public long VesselDocumentId { get { return vesselDocumentId; } set { vesselDocumentId = value; } }
        [Property("Pump_Name")]
        public string PumpName { get { return pumpName; } set { pumpName = value; } }
        [Property("Flowrate")]
        public float Flowrate { get { return flowrate; } set { flowrate = value; } }
        [Property("Product_Density")]
        public float ProductDensity { get { return productDensity; } set { productDensity = value; } }
        [Property("PLB_Document_Id")]
        public long PlbDocumentId { get { return plbDocumentId; } set { plbDocumentId = value; } }
        [Property("Actual_2")]
        public double Actual2 { get { return actual2; } set { actual2 = value; } }
        [Property("Correction_Factor")]
        public double CorrectionFactor { get { return correctionFactor; } set { correctionFactor = value; } }
        [Property("Flowmeter_Quantity")]
        public double FlowmeterQuantity { get { return flowmeterQuantity; } set { flowmeterQuantity = value; } }

        public string ProductName { get { return productId != null ? productId.ProductName : string.Empty; } }
        public string ProductType { get { return productId != null ? productId.ProductType : string.Empty; } }

        //public string VesselNo { get { return vesselDocumentId != null ? vesselDocumentId.VesselNo : string.Empty; } }

        public string VesselNo
        {
            get
            {
                if (vesselDocumentId != 0)
                {
                    VesselDocument vesselDocument = VesselDocument.FindById(VesselDocumentId);
                    if (vesselDocument != null)
                    {
                        return vesselDocument.VesselNo;
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        public string PlbSppbNo
        {
            get
            {
                if (plbDocumentId != 0)
                {
                    PlbDocument plbDocument = PlbDocument.FindById(PlbDocumentId);
                    if (plbDocument != null)
                    {
                        return plbDocument.PlbSppbNo;
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        public TruckLoadingPlanning()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(TruckLoadingPlanning), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(TruckLoadingPlanning));
            DetachedCriteria countCriteria = DetachedCriteria.For<TruckLoadingPlanning>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<TruckLoadingPlanning>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static TruckLoadingPlanning FindById(int id)
        {
            return (TruckLoadingPlanning)FindByPrimaryKey(typeof(TruckLoadingPlanning), id);
        }

        public static TruckLoadingPlanning[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<TruckLoadingPlanning>();
            dc.AddOrder(Order.Asc("PlanningId"));
            return (TruckLoadingPlanning[])FindAll(typeof(TruckLoadingPlanning), dc);
        }

        public static TruckLoadingPlanning[] FindByTruckLoadingId(int truckLoadId)
        {
            ICriterion[] query = { Expression.Eq("TruckLoadingId", truckLoadId) };
            return (TruckLoadingPlanning[])FindAll(typeof(TruckLoadingPlanning), query);
        }

        public static TruckLoadingPlanning FindByTruckLoadId(int truckLoadingId)
        {
            ICriterion[] query = { Expression.Eq("TruckLoadingId", truckLoadingId) };
            return (TruckLoadingPlanning)FindOne(typeof(TruckLoadingPlanning), query);
        }
        public static TruckLoadingPlanning FindByDeliveryOrderNumber(string deliveryOrderNumber)
        {
            ICriterion[] query = { Expression.Eq("DeliveryOrderNumber", deliveryOrderNumber) };
            return (TruckLoadingPlanning)FindOne(typeof(TruckLoadingPlanning), query);
        }

        public bool Validate(string saveType)
        {
            bool result = true;
            if (saveType == "Filling" || saveType == "Weighting")
            {
            }
            return result;
        }
    }
}
