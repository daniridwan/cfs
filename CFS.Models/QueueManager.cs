﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CFS.Models
{
    public class QueueManager
    {
        public static CFS.Models.FillingPointGroupCandidate DefineQueue(List<CFS.Models.FillingPointGroupCandidate> ListOfFillingPointGroup, int numberofbatch)
        {
            double QueueFactor = 25;
            double AvailableFPFactor = 1;
            //  double TodaysMeterReadingFactor = 5;
            foreach (CFS.Models.FillingPointGroupCandidate fpgroup in ListOfFillingPointGroup)
            {
                fpgroup.Score.Clear();
                #region Assign Score for Queue Multiplier
                double queuescore = ((3 - (double)fpgroup.QueueCount) / 5) * QueueFactor;

                fpgroup.Score.Add("queue", queuescore);
                #endregion

                #region Assign Score for Available FP Multiplier
                int availableFP = fpgroup.ActiveFillingPointCount;
                double availablefpscore = 0;
                switch (numberofbatch)
                {
                    case 1:
                        switch (availableFP)
                        {
                            case 0: availablefpscore = -10000; break;
                            case 1: availablefpscore = AvailableFPFactor * 3; break;
                            case 2: availablefpscore = AvailableFPFactor * 2; break;
                            case 3: availablefpscore = AvailableFPFactor * 1; break;
                            case 4: availablefpscore = AvailableFPFactor * 0; break;
                            case 5: availablefpscore = AvailableFPFactor * 0; break;
                        }
                        break;
                    case 2:
                        switch (availableFP)
                        {
                            case 0: availablefpscore = -10000; break;
                            case 1: availablefpscore = AvailableFPFactor * 1; break;
                            case 2: availablefpscore = AvailableFPFactor * 3; break;
                            case 3: availablefpscore = AvailableFPFactor * 2; break;
                            case 4: availablefpscore = AvailableFPFactor * 0; break;
                            case 5: availablefpscore = AvailableFPFactor * 0; break;
                        }
                        break;
                    case 3:
                        switch (availableFP)
                        {
                            case 0: availablefpscore = -10000; break;
                            case 1: availablefpscore = AvailableFPFactor * 1; break;
                            case 2: availablefpscore = AvailableFPFactor * 17; break;
                            case 3: availablefpscore = AvailableFPFactor * 18; break;
                            case 4: availablefpscore = AvailableFPFactor * 0; break;
                            case 5: availablefpscore = AvailableFPFactor * 0; break;
                        }
                        break;
                    case 4:
                        switch (availableFP)
                        {
                            case 0: availablefpscore = -10000; break;
                            case 1: availablefpscore = AvailableFPFactor * 1; break;
                            case 2: availablefpscore = AvailableFPFactor * 17; break;
                            case 3: availablefpscore = AvailableFPFactor * 18; break;
                            case 4: availablefpscore = AvailableFPFactor * 19; break;
                            case 5: availablefpscore = AvailableFPFactor * 20; break;
                        }
                        break;
                    case 5:
                        switch (availableFP)
                        {
                            case 0: availablefpscore = -10000; break;
                            case 1: availablefpscore = AvailableFPFactor * 1; break;
                            case 2: availablefpscore = AvailableFPFactor * 17; break;
                            case 3: availablefpscore = AvailableFPFactor * 18; break;
                            case 4: availablefpscore = AvailableFPFactor * 19; break;
                            case 5: availablefpscore = AvailableFPFactor * 20; break;
                        }
                        break;
                    default:
                        availablefpscore = 0;
                        break;
                }
                fpgroup.Score.Add("availablefp", availablefpscore);
                #endregion
            }

            ListOfFillingPointGroup.Sort(delegate(CFS.Models.FillingPointGroupCandidate fpg1, CFS.Models.FillingPointGroupCandidate fpg2)
            {
                return fpg1.BatchTotal.CompareTo(fpg2.BatchTotal);
            });
            double currentpreset = 0;
            int presetscore = ListOfFillingPointGroup.Count;
            double todaysmeterreadingscore = 0;
            foreach (CFS.Models.FillingPointGroupCandidate fpg in ListOfFillingPointGroup)
            {
                if (currentpreset != fpg.BatchTotal)
                {
                    currentpreset = fpg.BatchTotal;
                    presetscore -= 1;
                }
                todaysmeterreadingscore = 0;//(presetscore / ListOfFillingPointGroup.Count) * TodaysMeterReadingFactor;
                fpg.Score.Add("meterreading", todaysmeterreadingscore);
            }

            ListOfFillingPointGroup.Sort(delegate(CFS.Models.FillingPointGroupCandidate fpg1, CFS.Models.FillingPointGroupCandidate fpg2)
            {
                return fpg1.TotalScore.CompareTo(fpg2.TotalScore);
            });

            CFS.Models.FillingPointGroupCandidate selectedfillingpointgroup = null;
            if (ListOfFillingPointGroup.Count > 0)
            {
                selectedfillingpointgroup = ListOfFillingPointGroup[ListOfFillingPointGroup.Count - 1];
                if (selectedfillingpointgroup.ActiveFillingPointCount == 0)
                    selectedfillingpointgroup = null;
            }
            return selectedfillingpointgroup;
        }
    }
}
