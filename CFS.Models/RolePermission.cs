﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Mapping;

namespace CFS.Models
{
    [ActiveRecord("Role_Permission")]
    public class RolePermission : ActiveRecordBase
    {
        private List<string> messages;
        private int id;
        private int roleId;
        private int permissionId;

        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public int Id { get { return id; } set { id = value; } }
        [Property("Role_Id")]
        public int RoleId { get { return roleId; } set { roleId = value; } }
        [Property("Permission_Id")]
        public int PermissionId { get { return permissionId; } set { permissionId = value; } }

        #region Client Side Helper

        //private string permissionName;
        //public string PermissionName { get { return PermissionId != null ? permissionId.Name : permissionName; } set { permissionName = value; } }
        //private int permissionID;
        //public int PermissionID { get { return PermissionId != null ? permissionId.PermissionId : permissionID; } set { permissionID = value; } }

        #endregion

        public RolePermission()
        {
            messages = new List<string>();
        }

        public static RolePermission[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<RolePermission>();
            return (RolePermission[])FindAll(typeof(RolePermission), dc);
        }

        public static RolePermission[] FindByPermission(int id)
        {
            DetachedCriteria dc = DetachedCriteria.For<RolePermission>();
            dc.Add(Expression.Eq("PermissionId", id));
            return (RolePermission[])FindAll(typeof(RolePermission), dc);
        }

        public static RolePermission FindByRolePermission(int role, int permission)
        {
            DetachedCriteria dc = DetachedCriteria.For<RolePermission>();
            dc.Add(Expression.Eq("PermissionId", permission));
            dc.Add(Expression.Eq("RoleId", role));
            ////dc.AddOrder(Order.Asc("USERNAME"));

            return (RolePermission)FindOne(typeof(RolePermission), dc);
        }

        public static RolePermission[] FindByRoleId(int id)
        {
            DetachedCriteria dc = DetachedCriteria.For<RolePermission>();
            dc.Add(Expression.Eq("RoleId", id));
            return (RolePermission[])FindAll(typeof(RolePermission), dc);
        }

        public static RolePermission FindById(int id)
        {
            return (RolePermission)FindByPrimaryKey(typeof(RolePermission), id);
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(RolePermission), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Role));
            DetachedCriteria countCriteria = DetachedCriteria.For<Role>();

            //add param key here
            //if (param.SearchParam.HasKeys())
            //{
            //    if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
            //    {
            //        criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
            //        countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
            //    }
            //}

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Asc)
                    criteria.AddOrder(Order.Asc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Desc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Role>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }
    }
}
