﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Notification")]
    public class Notification : ActiveRecordBase
    {
        private List<string> messages;
        private int id;
        private string title;
        private string text;
        private string icon;
        private string status;
        private string url;
        private DateTime? timestamp;

        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public int Id { get { return id; } set { id = value; } }
        [Property("Title")]
        public string Title { get { return title; } set { title = value; } }
        [Property("Text")]
        public string Text { get { return text; } set { text = value; } }
        [Property("Icon")]
        public string Icon { get { return icon; } set { icon = value; } }
        [Property("Status")]
        public string Status { get { return status; } set { status = value; } }
        [Property("Url")]
        public string Url { get { return url; } set { url = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Timestamp")]
        public DateTime? Timestamp { get { return timestamp; } set { timestamp = value; } }

        public Notification()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Notification), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Notification));
            DetachedCriteria countCriteria = DetachedCriteria.For<Notification>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltTitle"] != null && param.SearchParam["fltTitle"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Title", param.SearchParam["fltTitle"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Title", param.SearchParam["fltTitle"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltStatus"] != null && param.SearchParam["fltStatus"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Status", param.SearchParam["fltStatus"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Status", param.SearchParam["fltStatus"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltTimestampFrom"] != null && param.SearchParam["fltTimestampFrom"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampFrom"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Gt("Timestamp", startPeriod));
                    countCriteria.Add(Expression.Gt("Timestamp", startPeriod));
                }
                //flt timestamp to
                if (param.SearchParam["fltTimestampTo"] != null && param.SearchParam["fltTimestampTo"].Length != 0)
                {
                    DateTime endPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampTo"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Le("Timestamp", endPeriod));
                    countCriteria.Add(Expression.Le("Timestamp", endPeriod));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Notification>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static Notification[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Notification>();
            dc.AddOrder(Order.Desc("Id"));
            return (Notification[])FindAll(typeof(Notification), dc);
        }

        public static Notification[] Find3Latest()
        {
            DetachedCriteria dc = DetachedCriteria.For<Notification>();
            dc.SetMaxResults(3);
            dc.AddOrder(Order.Desc("Timestamp"));
            return (Notification[])FindAll(typeof(Notification), dc);
        }
    }
}
