﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Truck_Loading_Record")]
    public class TruckLoadingRecord : ActiveRecordBase
    {
        private List<string> messages;
        private int recordId;
        private int truckLoadingId;
        private string deliveryOrderNumber;
        private string productCode;
        private string productName;
        private string fillingPointName;
        private string fillingPointGroup;
        private double preset;
        private double actual;
        private int batch;
        private DateTime? startTimestamp;
        private double startTotalizer;
        private DateTime? stopTimestamp;
        private double stopTotalizer;
        private double density;
        private double temperature;
        private int fillingStatus;
        private int fillingStopFlag;
        private double keyedDensity;
        private double keyedTemperature;

        [PrimaryKey(PrimaryKeyType.Identity, "Record_Id")]
        public int RecordId { get { return recordId; } set { recordId = value; } }
        [Property("Truck_Loading_Id")]
        public int TruckLoadingId { get { return truckLoadingId; } set { truckLoadingId = value; } }
        [Property("Delivery_Order_Number")]
        public string DeliveryOrderNumber { get { return deliveryOrderNumber; } set { deliveryOrderNumber = value; } }
        [Property("Product_Code")]
        public string ProductCode { get { return productCode; } set { productCode = value; } }
        [Property("Product_Name")]
        public string ProductName { get { return productName; } set { productName = value; } }
        [Property("Filling_Point_Name")]
        public string FillingPointName { get { return fillingPointName; } set { fillingPointName = value; } }
        [Property("Filling_Point_Group")]
        public string FillingPointGroup { get { return fillingPointGroup; } set { fillingPointGroup = value; } }
        [Property("Preset")]
        public double Preset { get { return preset; } set { preset = value; } }
        [Property("Actual")]
        public double Actual { get { return actual; } set { actual = value; } }
        [Property("Batch")]
        public int Batch { get { return batch; } set { batch = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Start_Timestamp")]
        public DateTime? StartTimestamp { get { return startTimestamp; } set { startTimestamp = value; } }
        [Property("Start_Totalizer")]
        public double StartTotalizer { get { return startTotalizer; } set { startTotalizer = value; } }
        [Property("Stop_Timestamp")]
        public DateTime? StopTimestamp { get { return stopTimestamp; } set { stopTimestamp = value; } }
        [Property("Stop_Totalizer")]
        public double StopTotalizer { get { return stopTotalizer; } set { stopTotalizer = value; } }
        [Property("Density")]
        public double Density { get { return density; } set { density = value; } }
        [Property("Temperature")]
        public double Temperature { get { return temperature; } set { temperature = value; } }
        [Property("Filling_Status")]
        public int FillingStatus { get { return fillingStatus; } set { fillingStatus = value; } }
        [Property("Filling_Stop_Flag")]
        public int FillingStopFlag { get { return fillingStopFlag; } set { fillingStopFlag = value; } }
        [Property("Keyed_Density")]
        public double KeyedDensity { get { return keyedDensity; } set { keyedDensity = value; } }
        [Property("Keyed_Temperature")]
        public double KeyedTemperature { get { return keyedTemperature; } set { keyedTemperature = value; } }

        public TruckLoadingRecord()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(TruckLoadingRecord), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(TruckLoadingRecord));
            DetachedCriteria countCriteria = DetachedCriteria.For<TruckLoadingRecord>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                }
            }


            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<TruckLoadingRecord>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static TruckLoadingRecord[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<TruckLoadingRecord>();
            dc.AddOrder(Order.Asc("RecordId"));
            return (TruckLoadingRecord[])FindAll(typeof(TruckLoadingRecord), dc);
        }

        public static TruckLoadingRecord[] FindByTruckLoadingId(int truckLoadingId)
        {
            ICriterion[] query = { Expression.Eq("TruckLoadingId", truckLoadingId) };
            return (TruckLoadingRecord[])FindAll(typeof(TruckLoadingRecord), query);
        }

        public static TruckLoadingRecord FindByLONumber(string loadingOrderNumber)
        {
            ICriterion[] query = { Expression.Eq("LoadingOrderNumber", loadingOrderNumber) };
            return (TruckLoadingRecord)FindFirst(typeof(TruckLoadingRecord), query);
        }
    }
}
