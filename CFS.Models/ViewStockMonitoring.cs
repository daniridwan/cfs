﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("View_Stock_Monitoring", Mutable = false)]
    public class ViewStockMonitoring : ActiveRecordBase
    {
        private List<string> messages;
        private long row;
        private int? soundingId;
        private long? tankId;
        private string name;
        private long companyId;
        private string companyName;
        private DateTime? soundingDate;
        private float soundingLevel;
        private float temperature;
        private float productTankVolume;
        private float productPipeVolume;
        private float productSlopeVolume;
        private long? vesselTankId;
        private float quantity;
        private float spillage;
        private DateTime? arrivalDate;
        private string blNo;
        private string remarks;
        private DateTime? deliveryDate;
        private float totalDelivery;
        public long? deliveryTankId;
        public string deliveryTankName;

        [PrimaryKey(PrimaryKeyType.Assigned, "Row")]
        public long Row { get { return row; } set { row = value; } }
        [Property("Sounding_Id")]
        public int? SoundingId { get { return soundingId; } set { soundingId = value; } }
        [Property("Tank_Id")]
        public long? TankId { get { return tankId; } set { tankId = value; } }
        [Property("Name")] //tank name
        public string Name { get { return name; } set { name = value; } }
        [Property("Company_Id")]
        public long CompanyId { get { return companyId; } set { companyId = value; } }
        [Property("Company_Name")]
        public string CompanyName { get { return companyName; } set { companyName = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Sounding_Date")]
        public DateTime? SoundingDate { get { return soundingDate; } set { soundingDate = value; } }
        [Property("Sounding_Level")]
        public float SoundingLevel { get { return soundingLevel; } set { soundingLevel = value; } }
        [Property("Temperature")]
        public float Temperature { get { return temperature; } set { temperature = value; } }
        [Property("Product_Tank_Volume")]
        public float ProductTankVolume { get { return productTankVolume; } set { productTankVolume = value; } }
        [Property("Product_Pipe_Volume")]
        public float ProductPipeVolume { get { return productPipeVolume; } set { productPipeVolume = value; } }
        [Property("Product_Slope_Volume")]
        public float ProductSlopeVolume { get { return productSlopeVolume; } set { productSlopeVolume = value; } }
        [Property("Vessel_Tank_Id")]
        public long? VesselTankId { get { return vesselTankId; } set { vesselTankId = value; } }
        [Property("Quantity")]
        public float Quantity { get { return quantity; } set { quantity = value; } }
        [Property("Spillage")]
        public float Spillage { get { return spillage; } set { spillage = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Arrival_Date")]
        public DateTime? ArrivalDate { get { return arrivalDate; } set { arrivalDate = value; } }
        [Property("BL_No")]
        public string BlNo { get { return blNo; } set { blNo = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Delivery_Date")]
        public DateTime? DeliveryDate { get { return deliveryDate; } set { deliveryDate = value; } }
        [Property("Total_Delivery")]
        public float TotalDelivery { get { return totalDelivery; } set { totalDelivery = value; } }
        [Property("Delivery_Tank_Id")]
        public long? DeliveryTankId { get { return deliveryTankId; } set { deliveryTankId = value; } }
        [Property("Delivery_Tank_Name")]
        public string DeliveryTankName { get { return deliveryTankName; } set { deliveryTankName = value; } }

        public ViewStockMonitoring()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(ViewStockMonitoring), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(ViewStockMonitoring));
            DetachedCriteria countCriteria = DetachedCriteria.For<ViewStockMonitoring>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltTank"] != null && param.SearchParam["fltTank"].Length != 0)
                {

                    criteria.Add(Expression.Eq("TankId", Convert.ToInt64(param.SearchParam["fltTank"])));
                    countCriteria.Add(Expression.Eq("TankId", Convert.ToInt64(param.SearchParam["fltTank"])));
                }
                
                //flt timestamp from
                if (param.SearchParam["fltStartPeriod"] != null && param.SearchParam["fltPeriod"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltPeriod"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Ge("SoundingDate", startPeriod));
                    countCriteria.Add(Expression.Ge("SoundingDate", startPeriod));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<ViewStockMonitoring>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static ViewStockMonitoring FindByTankAndDeliveryDate(long tankId, DateTime date)
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewStockMonitoring>();
            dc.Add(Expression.Eq("DeliveryTankId", tankId));
            dc.Add(Expression.Eq("DeliveryDate", date));
            return (ViewStockMonitoring)FindOne(typeof(ViewStockMonitoring), dc);
        }

        public static ViewStockMonitoring FindByTankAndReceivingDate(long tankId, DateTime date)
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewStockMonitoring>();
            dc.Add(Expression.Eq("VesselTankId", tankId));
            dc.Add(Expression.Eq("ArrivalDate", date));
            return (ViewStockMonitoring)FindOne(typeof(ViewStockMonitoring), dc);
        }

        public static ViewStockMonitoring FindByTankAndSoundingDate(long tankId, DateTime date)
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewStockMonitoring>();
            dc.Add(Expression.Eq("TankId", tankId));
            dc.Add(Expression.Eq("SoundingDate", date));
            return (ViewStockMonitoring)FindOne(typeof(ViewStockMonitoring), dc);
        }

        public static ViewStockMonitoring FindLatestSounding(long tankId, DateTime date)
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewStockMonitoring>();
            dc.Add(Expression.Lt("SoundingDate", date));
            dc.Add(Expression.Eq("TankId", tankId));
            dc.AddOrder(Order.Desc("SoundingDate"));
            return (ViewStockMonitoring)FindFirst(typeof(ViewStockMonitoring), dc);
        }
    }
}
