﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Sounding")]
    public class Sounding : ActiveRecordBase
    {
        private List<string> messages;
        private int id;
        private long tankId;
        private long productId;
        private long companyId;
        private DateTime? soundingDate;
        private float soundingLevel;
        private float temperature;
        private float productTankVolume;
        private float productPipeVolume;
        private float productSlopeVolume;
        private string type;
        private string remarks;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;


        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public int Id { get { return id; } set { id = value; } }
        [Property("Tank_Id")]
        public long TankId { get { return tankId; } set { tankId = value; } }
        [Property("Product_Id")]
        public long ProductId { get { return productId; } set { productId = value; } }
        [Property("Company_Id")]
        public long CompanyId { get { return companyId; } set { companyId = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Sounding_Date")]
        public DateTime? SoundingDate { get { return soundingDate; } set { soundingDate = value; } }
        [Property("Sounding_Level")]
        public float SoundingLevel { get { return soundingLevel; } set { soundingLevel = value; } }
        [Property("Temperature")]
        public float Temperature { get { return temperature; } set { temperature = value; } }
        [Property("Product_Tank_Volume")]
        public float ProductTankVolume { get { return productTankVolume; } set { productTankVolume = value; } }
        [Property("Product_Pipe_Volume")]
        public float ProductPipeVolume { get { return productPipeVolume; } set { productPipeVolume = value; } }
        [Property("Product_Slope_Volume")]
        public float ProductSlopeVolume { get { return productSlopeVolume; } set { productSlopeVolume = value; } }
        [Property("Type")]
        public string Type { get { return type; } set { type = value; } }
        [Property("Remarks")]
        public string Remarks { get { return remarks; } set { remarks = value; } }

        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Name : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Name : string.Empty; } }

        public string ProductName
        {
            get
            {
                Product product = Product.FindById(ProductId);
                if (product != null)
                {
                    return product.ProductName;
                }
                else
                {
                    return "";
                }
            }
        }

        public string CompanyName
        {
            get
            {
                Company company = Company.FindById(CompanyId);
                if (company != null)
                {
                    return company.CompanyName;
                }
                else
                {
                    return "";
                }
            }
        }

        public string TankName
        {
            get
            {
                Tank tank = Tank.FindById(TankId);
                if (tank != null)
                {
                    return tank.Name;
                }
                else
                {
                    return "";
                }
            }
        }

        public Sounding()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static Sounding FindById(int id)
        {
            return (Sounding)FindByPrimaryKey(typeof(Sounding), id);
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Sounding), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Sounding));
            DetachedCriteria countCriteria = DetachedCriteria.For<Sounding>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltTankId"] != null && param.SearchParam["fltTankId"].Length != 0)
                {
                    int tank = int.Parse(param.SearchParam["fltTankId"]);
                    criteria.Add(Expression.Eq("TankId", tank));
                    countCriteria.Add(Expression.Eq("TankId", tank));
                }
                //flt timestamp from
                if (param.SearchParam["fltSoundingDate"] != null && param.SearchParam["fltSoundingDate"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltSoundingDate"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Eq("SoundingDate", startPeriod));
                    countCriteria.Add(Expression.Eq("SoundingDate", startPeriod));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Sounding>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static Pump[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Sounding>();
            dc.AddOrder(Order.Asc("Name"));
            return (Pump[])FindAll(typeof(Sounding), dc);
        }

        public bool Validate()
        {
            bool result = true;
            //if (ReceivingQuantity >= 0)
            //{
            //    if(BlNo == "")
            //    {
            //        messages.Add("BL No required");
            //        result = false;
            //    }
            //}
            return result;
        }
    }
}
