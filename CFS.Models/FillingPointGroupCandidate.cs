﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using NHibernate;

namespace CFS.Models
{
    [ActiveRecord("Filling_Point_Group", Mutable = false)]
    public class FillingPointGroupCandidate : ActiveRecordBase
    {
        private string fillingPointGroup;
        private int activeFillingPointCount;
        private int queueCount;
        private int batchTotal;
        private Dictionary<string, double> score;
        private List<FillingPoint> fillingPoints;

        [PrimaryKey(PrimaryKeyType.Assigned, "Filling_Point_Group")]
        public string FillingPointGroup { get { return fillingPointGroup; } set { fillingPointGroup = value; } }
        [Property("Active_Count")]
        public int ActiveFillingPointCount { get { return activeFillingPointCount; } set { activeFillingPointCount = value; } }
        [Property("Queue_Count")]
        public int QueueCount { get { return queueCount; } set { queueCount = value; } }
        [Property("Batch")]
        public int BatchTotal { get { return batchTotal; } set { batchTotal = value; } }

        public Dictionary<string, double> Score { get { return score; } set { score = value; } }
        public double TotalScore
        {
            get
            {
                double total = 0;
                foreach (KeyValuePair<string, double> args in Score)
                {
                    total += args.Value;
                }
                return total;
            }
        }

        public List<FillingPoint> FillingPoints { get { return fillingPoints; } set { fillingPoints = value; } }

        public FillingPointGroupCandidate()
        {
            score = new Dictionary<string, double>();
            fillingPoints = new List<FillingPoint>();
        }

        private static string TPL_QUERY = @"
        select x3.Filling_Point_Group, x3.Active_Count, x2.Queue_Count, x2.Batch
        from
        (
            select Filling_Point_Group,
            sum(case when Is_Active = 1 then 1 else null end) Active_Count
            from Filling_Point where
            Is_Active = 1
            group by Filling_Point_Group
        ) x3
        left outer join
        (
            select Filling_Point_Group, count(Truck_Id) queue_count, sum(Batch) Batch
            from
            (
            select tlp.Filling_Point_Group, tl.Truck_Id, sum(tlp.Batch) batch
            from Truck_Loading tl
            inner join Truck_Loading_Planning tlp on (tlp.Truck_Loading_Id = tl.Truck_Loading_Id)
            where 1 =1
            and tl.Gate_In_Timestamp >= '{0}'
            and tl.Gate_In_Timestamp < '{1}'
            group by tlp.Filling_Point_Group, tl.Truck_Id
            ) x1
            group by filling_point_group
        ) x2 on (x2.Filling_Point_Group = x3.Filling_Point_Group)
        ";

        public static IList<FillingPointGroupCandidate> GetCandidate(DateTime timepoint)
        {
            string startDate = timepoint.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            string endDate = timepoint.AddDays(1).ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

            string strQ = string.Format(TPL_QUERY, startDate, endDate);
            IList<FillingPointGroupCandidate> result = (IList<FillingPointGroupCandidate>)ActiveRecordMediator<FillingPointGroupCandidate>.Execute(
            delegate(ISession session, object instance)
            {
                ISQLQuery q = session.CreateSQLQuery(strQ);
                q.AddEntity("FillingPointGroupCandidate", typeof(FillingPointGroupCandidate));
                return q.List<FillingPointGroupCandidate>();
            }, null);

            return result;
        }

        public void SetFillingPoints(FillingPoint[] member)
        {
            fillingPoints.Clear();
            foreach (FillingPoint fp in member)
            {
                fillingPoints.Add(fp);
            }
        }
    }
}
