﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;
using CFS.FDFlow;

namespace CFS.Models
{
    [ActiveRecord("Truck_Loading")]
    public class TruckLoading : ActiveRecordBase
    {
        private List<string> messages;
        private int truckLoadingId;
        private int truckId;
        private int driverId;
        private DateTime? administrationTimestamp;
        private string administrationLocation;
        private string administrationUser;
        private string administrationMode;
        private string administrationDescription;
        private DateTime? gateInTimestamp;
        private string gateInLocation;
        private string gateInUser;
        private string gateInMode;
        private string gateInDescription;
        private DateTime? loadedTimestamp;
        private string loadedLocation;
        private string loadedUser;
        private string loadedCFSUser;
        private string loadedMode;
        private string loadedDescription;
        private DateTime? gateOutTimestamp;
        private string gateOutLocation;
        private string gateOutUser;
        private string gateOutMode;
        private string gateOutDescription;
        //do
        private string deliveryOrderNumber;
        private int shipmentStatus;
        private int isGoodIssueSucceed;
        private string goodIssueMessage;
        private DateTime? goodIssueTimestamp;
        private string remarks;
        private int queueNumber;

        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        private IList<TruckLoadingPlanning> truckLoadingPlanning;
        private IList<TruckLoadingSecuritySeal> truckLoadingSecuritySeal;
        //private IList<TruckLoadingRecord> truckLoadingRecord;

        [PrimaryKey(PrimaryKeyType.Identity, "Truck_Loading_Id")]
        public int TruckLoadingId { get { return truckLoadingId; } set { truckLoadingId = value; } }
        [Property("Truck_Id")]
        public int TruckId { get { return truckId; } set { truckId = value; } }
        [Property("Driver_Id")]
        public int DriverId { get { return driverId; } set { driverId = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        //Administration
        [Property("Administration_Timestamp")]
        public DateTime? AdministrationTimestamp { get { return administrationTimestamp; } set { administrationTimestamp = value; } }
        [Property("Administration_Location")]
        public string AdministrationLocation { get { return administrationLocation; } set { administrationLocation = value; } }
        [Property("Administration_User")]
        public string AdministrationUser { get { return administrationUser; } set { administrationUser = value; } }
        [Property("Administration_Mode")]
        public string AdministrationMode { get { return administrationMode; } set { administrationMode = value; } }
        [Property("Administration_Description")]
        public string AdministrationDescription { get { return administrationDescription; } set { administrationDescription = value; } }
        //Gate in
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Gate_In_Timestamp")]
        public DateTime? GateInTimestamp { get { return gateInTimestamp; } set { gateInTimestamp = value; } }
        [Property("Gate_In_Location")]
        public string GateInLocation { get { return gateInLocation; } set { gateInLocation = value; } }
        [Property("Gate_In_User")]
        public string GateInUser { get { return gateInUser; } set { gateInUser = value; } }
        [Property("Gate_In_Mode")]
        public string GateInMode { get { return gateInMode; } set { gateInMode = value; } }
        [Property("Gate_In_Description")]
        public string GateInDescription { get { return gateInDescription; } set { gateInDescription = value; } }
        //Loaded
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Loaded_Timestamp")]
        public DateTime? LoadedTimestamp { get { return loadedTimestamp; } set { loadedTimestamp = value; } }
        [Property("Loaded_Location")]
        public string LoadedLocation { get { return loadedLocation; } set { loadedLocation = value; } }
        [Property("Loaded_User")]
        public string LoadedUser { get { return loadedUser; } set { loadedUser = value; } }
        [Property("Loaded_CFS_User")]
        public string LoadedCFSUser { get { return loadedCFSUser; } set { loadedCFSUser = value; } }
        [Property("Loaded_Mode")]
        public string LoadedMode { get { return loadedMode; } set { loadedMode = value; } }
        [Property("Loaded_Description")]
        public string LoadedDescription { get { return loadedDescription; } set { loadedDescription = value; } }
        //Gate Out
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Gate_Out_Timestamp")]
        public DateTime? GateOutTimestamp { get { return gateOutTimestamp; } set { gateOutTimestamp = value; } }
        [Property("Gate_Out_Location")]
        public string GateOutLocation { get { return gateOutLocation; } set { gateOutLocation = value; } }
        [Property("Gate_Out_User")]
        public string GateOutUser { get { return gateOutUser; } set { gateOutUser = value; } }
        [Property("Gate_Out_Mode")]
        public string GateOutMode { get { return gateOutMode; } set { gateOutMode = value; } }
        [Property("Gate_Out_Description")]
        public string GateOutDescription { get { return gateOutDescription; } set { gateOutDescription = value; } }
        //do
        [Property("Delivery_Order_Number")]
        public string DeliveryOrderNumber { get { return deliveryOrderNumber; } set { deliveryOrderNumber = value; } }
        [Property("Shipment_Status")]
        public int ShipmentStatus { get { return shipmentStatus; } set { shipmentStatus = value; } }
        [Property("Is_Good_Issue_Succeed")]
        public int IsGoodIssueSucceed { get { return isGoodIssueSucceed; } set { isGoodIssueSucceed = value; } }
        [Property("Good_Issue_Message")]
        public string GoodIssueMessage { get { return goodIssueMessage; } set { goodIssueMessage = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Good_Issue_Timestamp")]
        public DateTime? GoodIssueTimestamp { get { return goodIssueTimestamp; } set { goodIssueTimestamp = value; } }

        [Property("Remarks")]
        public string Remarks { get { return remarks; } set { remarks = value; } }
        [Property("Queue_Number")]
        public int QueueNumber { get { return queueNumber; } set { queueNumber = value; } }

        //Audit Trail
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }

        //relationmapping
        [HasMany(typeof(TruckLoadingPlanning), Table = "Truck_Loading_Planning", ColumnKey = "Truck_Loading_Id")]
        public IList<TruckLoadingPlanning> TruckLoadingPlanning { get { return truckLoadingPlanning; } set { truckLoadingPlanning = value; } }
        [HasMany(typeof(TruckLoadingSecuritySeal), Table = "Truck_Loading_Security_Seal", ColumnKey = "Truck_Loading_Id")]
        public IList<TruckLoadingSecuritySeal> TruckLoadingSecuritySeal { get { return truckLoadingSecuritySeal; } set { truckLoadingSecuritySeal = value; } }
        //[HasMany(typeof(TruckLoadingRecord), Table = "Truck_Loading_Record", ColumnKey = "Truck_Loading_Id")]
        //public IList<TruckLoadingRecord> TruckLoadingRecord { get { return truckLoadingRecord; } set { truckLoadingRecord = value; } }

        public string TruckRegistrationPlate
        {
            get
            {
                Truck truck = Truck.FindById(TruckId);
                if (truck != null)
                {
                    return truck.RegistrationPlate;
                }
                else
                {
                    return "";
                }
            }
        }

        //public long CompanyId
        //{
        //    get
        //    {
        //        DeliveryOrder DO = DeliveryOrder.FindByShipmentNumber(DeliveryOrderNumber);
        //        if (DO != null)
        //        {
        //            return DO.CompanyId;
        //        }
        //        else
        //        {
        //            return 0;
        //        }
        //    }
        //}

        public string TotalPreset
        {
            get
            {
                double preset = 0;
                if (truckLoadingPlanning != null)
                {
                    foreach (TruckLoadingPlanning tp in truckLoadingPlanning)
                    {
                        preset += tp.Preset;
                    }
                }

                return preset.ToString();
            }
        }

        public string SealString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                IList<string> seal = new List<string>();
                bool first = true;
                foreach (TruckLoadingSecuritySeal tlss in truckLoadingSecuritySeal)
                {

                    if (first)
                        first = false;
                    else sb.Append(",");
                    if (!seal.Contains(tlss.SealNumber.ToString()))
                    {
                        sb.Append(tlss.SealNumber.ToString());
                    }
                }
                if (sb.ToString() == ",")
                {
                    return "";
                }
                else
                {
                    return sb.ToString();
                }
            }
        }

        public string FillingPointString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                IList<string> fp = new List<string>();
                bool first = true;
                foreach (TruckLoadingPlanning tp in truckLoadingPlanning)
                {
                    fp.Add(tp.FillingPointName);
                    //if (first)
                    //    first = false;
                    //else sb.Append(",");
                    //if (!sb.Contains(tp.FillingPointName))
                    //{
                    //    sb.Append(tp.FillingPointName);
                    //}
                }
                string s = string.Join(",", fp.Distinct());
                return s;
                //if (sb.ToString() == ",")
                //{
                //    return "";
                //}
                //else
                //{
                //    return sb.ToString();
                //}
            }
        }

        public bool IsFinished {
            get
            {
                bool result = false;
                foreach (TruckLoadingPlanning tp in TruckLoadingPlanning)
                {
                    if(tp.FillingPointGroup == "Chemical")
                    {
                        if(tp.FinalWeight > 0 && tp.InitialWeight > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                            break;
                        }
                    }
                    else
                    {
                        if(tp.Actual > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                            break;
                        }
                    }
                }
                return result;
            }
        }

        public string PIBDateString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                IList<string> li = new List<string>();
                bool first = true;
                foreach (TruckLoadingPlanning tp in truckLoadingPlanning)
                {
                    if (first)
                        first = false;
                    else sb.Append(",");
                    VesselDocument v = VesselDocument.FindById(tp.VesselDocumentId);
                    string pibDate = v.PibDate.HasValue ? v.PibDate.Value.ToString("dd/MM/yyyy") : "";
                    if (!li.Contains(pibDate))
                    {
                        sb.Append(pibDate);
                    }
                }
                if(sb.ToString() == ",")
                {
                    return "";
                }
                else { 
                    return sb.ToString();
                }
            }
        }

        public string PIBNoString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                IList<string> li = new List<string>();
                bool first = true;
                foreach (TruckLoadingPlanning tp in truckLoadingPlanning)
                {
                    if (first)
                        first = false;
                    else sb.Append(",");
                    VesselDocument v = VesselDocument.FindById(tp.VesselDocumentId);
                    if (!li.Contains(v.PibNo))
                    {
                        sb.Append(v.PibNo);
                    }
                }
                if (sb.ToString() == ",")
                {
                    return "";
                }
                else
                {
                    return sb.ToString();
                }
            }
        }

        public string NoKapalString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                IList<string> li = new List<string>();
                bool first = true;
                foreach (TruckLoadingPlanning tp in truckLoadingPlanning)
                {
                    if (first)
                        first = false;
                    else sb.Append(",");
                    VesselDocument v = VesselDocument.FindById(tp.VesselDocumentId);
                    if (!li.Contains(v.VesselNo))
                    {
                        sb.Append(v.VesselNo);
                    }
                }
                if (sb.ToString() == ",")
                {
                    return "";
                }
                else
                {
                    return sb.ToString();
                }
            }
        }

        public string VesselNameString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                IList<string> li = new List<string>();
                foreach (TruckLoadingPlanning tp in truckLoadingPlanning)
                {
                    VesselDocument v = VesselDocument.FindById(tp.VesselDocumentId);
                    if (!li.Contains(v.VesselName))
                    {
                        //sb.Append(v.VesselName);
                        li.Add(v.VesselName);
                    }
                }
                return string.Join(",", li);
            }
        }

        public TruckLoading()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(TruckLoading), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(TruckLoading));
            DetachedCriteria countCriteria = DetachedCriteria.For<TruckLoading>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltRegistrationPlate"] != null && param.SearchParam["fltRegistrationPlate"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("TruckRegistrationPlate", param.SearchParam["fltRegistrationPlate"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("TruckRegistrationPlate", param.SearchParam["fltRegistrationPlate"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltDONumber"] != null && param.SearchParam["fltDONumber"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("DeliveryOrderNumber", param.SearchParam["fltDONumber"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("DeliveryOrderNumber", param.SearchParam["fltDONumber"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltShipmentStatus"] != null && param.SearchParam["fltShipmentStatus"].Length != 0)
                {
                    int isActive = int.Parse(param.SearchParam["fltShipmentStatus"]);
                    criteria.Add(Expression.Eq("ShipmentStatus", isActive));
                    countCriteria.Add(Expression.Eq("ShipmentStatus", isActive));
                }
                //flt timestamp from
                if (param.SearchParam["fltTimestampFrom"] != null && param.SearchParam["fltTimestampFrom"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampFrom"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Gt("GateInTimestamp", startPeriod));
                    countCriteria.Add(Expression.Gt("GateInTimestamp", startPeriod));
                }
                //flt timestamp to
                if (param.SearchParam["fltTimestampTo"] != null && param.SearchParam["fltTimestampTo"].Length != 0)
                {
                    DateTime endPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampTo"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Le("GateInTimestamp", endPeriod));
                    countCriteria.Add(Expression.Le("GateInTimestamp", endPeriod));
                }
                //if (param.SearchParam["fltCompany"] != null && param.SearchParam["fltCompany"].Length != 0)
                //{
                //    //criteria.Add(Expression.Eq("CompanyId", Convert.ToInt64(param.SearchParam["fltCompany"])));
                //    //countCriteria.Add(Expression.Eq("CompanyId", Convert.ToInt64(param.SearchParam["fltCompany"])));
                //    criteria.CreateAlias("Company", "c", NHibernate.SqlCommand.JoinType.InnerJoin)
                //    .Add(Restrictions.InsensitiveLike("c.CompanyId", param.SearchParam["fltCompany"], MatchMode.Anywhere));
                //    countCriteria.CreateAlias("Company", "c", NHibernate.SqlCommand.JoinType.InnerJoin)
                //    .Add(Expression.InsensitiveLike("c.CompanyId", param.SearchParam["fltCompany"], MatchMode.Anywhere));
                //}
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<TruckLoading>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static TruckLoading FindById(int id)
        {
            return (TruckLoading)FindByPrimaryKey(typeof(TruckLoading), id);
        }

        public static TruckLoading[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<TruckLoading>();
            dc.AddOrder(Order.Asc("TruckLoadingId"));
            return (TruckLoading[])FindAll(typeof(TruckLoading), dc);
        }

        public static TruckLoading FindByTruckId(string key)
        {
            ICriterion[] query = { Expression.Eq("TruckId", key) };
            return (TruckLoading)FindOne(typeof(TruckLoading), query);
        }

        public static TruckLoading FindByDeliveryOrdeNumber(string number)
        {
            ICriterion[] query = { Expression.Eq("DeliveryOrderNumber", number) };
            return (TruckLoading)FindOne(typeof(TruckLoading), query);
        }

        public static TruckLoading[] FindByShipmentStatus(int id)
        {
            DetachedCriteria dc = DetachedCriteria.For<TruckLoading>();
            dc.Add(Expression.Eq("ShipmentStatus", id));
            dc.AddOrder(Order.Asc("AdministrationTimestamp"));
            return (TruckLoading[])FindAll(typeof(TruckLoading), dc);
        }

        public static TruckLoading[] FindToGoodIssue()
        {
            DetachedCriteria dc = DetachedCriteria.For<TruckLoading>();
            dc.Add(Expression.Eq("ShipmentStatus", 3));
            dc.Add(Expression.Eq("IsGoodIssueSucceed", 0));
            dc.AddOrder(Order.Asc("AdministrationTimestamp"));
            return (TruckLoading[])FindAll(typeof(TruckLoading), dc);
        }

        public bool Validate(string saveType)
        {
            bool result = true;
            #region administration validation
            if (saveType == "Administration")
            {
                #region DO
                //double totalPreset = 0;
                if (TruckLoadingPlanning == null)
                {
                    messages.Add("Please input DO");
                    result = false;
                }
                else
                {
                    foreach (TruckLoadingPlanning plan in TruckLoadingPlanning)
                    {
                        if (plan.VesselDocumentId == 0)
                        {
                            messages.Add("Please select Vessel Document for each product");
                            result = false;
                        }
                        if (plan.FillingPointName == string.Empty || plan.FillingPointName == null)
                        {
                            messages.Add("Please select Filling Point for each product");
                            result = false;
                        }
                        VesselDocument vd = VesselDocument.FindById(plan.VesselDocumentId);
                        if(vd.Type == "Import" && vd.SppbDate == null && vd.PibDate == null)
                        {
                            messages.Add(string.Format("Vessel Document for BL {0} has no valid PIB & SPPB Date", vd.BlNo));
                            result = false;
                        }
                    }
                }
                if (truckLoadingSecuritySeal == null)
                {
                    messages.Add("Please input seal");
                    result = false;
                }
                
                //Truck truck = Truck.FindById(TruckId);
                //if (totalPreset > truck.TankMaxCapacity)
                //{
                //    messages.Add("Delivery Order melebihi kapasitas truk.");
                //    result = false;
                //}
                //if (totalPreset == 0)
                //{
                //    messages.Add("Truk belum memiliki muatan.");
                //    result = false;
                //}
                #endregion
                #region validation STNK
                CFS.Models.SystemValidation sysValSTNK = CFS.Models.SystemValidation.FindByKey("STNK_Validation");
                if (sysValSTNK.IsActive)
                {
                    CFS.Models.Truck origTruck = CFS.Models.Truck.FindById(TruckId);
                    if (origTruck.TruckStnkTimestamp < DateTime.Now)
                    {
                        result = false;
                        messages.Add(string.Format("Truck STNK Expired. ({0:dd-MMM-yyyy})", origTruck.TruckStnkTimestamp));
                    }
                }
                #endregion
                #region validation Driver
                CFS.Models.Driver origDriver = CFS.Models.Driver.FindById(DriverId);
                CFS.Models.SystemValidation sysValSIM = CFS.Models.SystemValidation.FindByKey("SIM_Validation");
                CFS.Models.SystemValidation sysValPhoto = CFS.Models.SystemValidation.FindByKey("Photo_Validation");
                if (sysValSIM.IsActive)
                {
                    if (origDriver.DriverSimExpiryDate < DateTime.Now)
                    {
                        result = false;
                        messages.Add(string.Format("Driver License Expired. ({0:dd-MMM-yyyy})", origDriver.DriverSimExpiryDate));
                    }
                }
                if (sysValPhoto.IsActive) 
                {
                    if (!origDriver.HasPhoto) {
                        result = false;
                        messages.Add("Driver belum safety induction / foto KIM");
                    }
                }
                if (origDriver.DriverKimExpiryDate < DateTime.Now)
                {
                    result = false;
                    messages.Add("KIM Expired");
                }
                #endregion
            }
            #endregion

            #region gate in validation
            else if (saveType == "Gate In")
            {
                #region validation Truck Already Gate In
                if (GateInTimestamp != null)
                {
                    messages.Add("Truk sudah gate in.");
                    result = false;
                }
                #endregion
            }
            #endregion
            #region gate out validation
            else if (saveType == "Gate Out")
            {
                foreach (TruckLoadingPlanning plan in TruckLoadingPlanning)
                {
                    if (plan.FillingPointGroup == "Chemical")
                    {
                        if (plan.InitialWeight <= 0 || plan.FinalWeight <= 0)
                        {
                            messages.Add("Truk belum menimbang kosong / isi, selesaikan transaksi terlebih dahulu.");
                            result = false;
                        }
                    }
                    else
                    {
                        if (plan.Actual <= 0)
                        {
                            messages.Add("Truk belum melakukan pengisian, selesaikan transaksi terlebih dahulu.");
                            result = false;
                        }
                    }
                }
            }
            #endregion
            #region filling validation
            else if (saveType == "Filling" || saveType == "Weighting" || saveType == "Update")
            {
            }
            #endregion
            return result;
        }
    }
}
