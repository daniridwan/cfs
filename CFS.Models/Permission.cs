﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Mapping;


namespace CFS.Models
{
    [ActiveRecord("Permission")]
    public class Permission : ActiveRecordBase
    {
        private List<string> messages;
        private int permissionId;
        private string name;

        [PrimaryKey(PrimaryKeyType.Identity, "Permission_Id")]
        public int PermissionId { get { return permissionId; } set { permissionId = value; } }
        [Property("Name")]
        public string Name { get { return name; } set { name = value; } }

        public Permission()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static Permission[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Permission>();
            dc.AddOrder(Order.Asc("Name"));
            return (Permission[])FindAll(typeof(Permission), dc);
        }

        public static Permission FindById(int id)
        {
            return (Permission)FindByPrimaryKey(typeof(Permission), id);
        }

        public static Permission FindByName(string name)
        {
            ICriterion[] query = { Expression.Eq("Name", name) };
            return (Permission)FindOne(typeof(Permission), query);
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(Permission), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(Role));
            DetachedCriteria countCriteria = DetachedCriteria.For<Role>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Asc)
                    criteria.AddOrder(Order.Asc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Desc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<Role>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public bool Validate()
        {
            bool result = true;
            return result;
        }
    }
}
