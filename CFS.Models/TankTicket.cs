﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Tank_Ticket")]
    public class TankTicket : ActiveRecordBase
    {
        private List<string> messages;
        private int tankTicketId;
        private string tankTicketNumber;
        private string tankName;
        private DateTime? timestamp;
        private string operationType;
        private string operationStatus;
        private string measurementType;
        private string productCode;
        private double liquidLevel;
        private double liquidWater;
        private double liquidTemperature;
        private double liquidDensity;
        private double liquidDensity15;
        private double liquidVolObs;
        private double liquidVol15;
        private double liquidMass;
        private float liquidPressure;
        private double vaporTemperature;
        private double vaporDensity;
        private double vaporVolObs;
        private double vaporVol15;
        private double vaporMass;
        private double vaporPressure;
        private float liquidVolumeCorrectionFactor;
        private float liquidDensityMultiplier;
        private float vaporPressureFactor;
        private float vaporTemperatureFactor;
        private float totalObservedVolume;
        private int? pairId;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        [PrimaryKey(PrimaryKeyType.Identity, "Tank_Ticket_Id")]
        public int TankTicketId { get { return tankTicketId; } set { tankTicketId = value; } }
        [Property("Tank_Ticket_Number")]
        public string TankTicketNumber { get { return tankTicketNumber; } set { tankTicketNumber = value; } }
        [Property("Tank_Name")]
        public string TankName { get { return tankName; } set { tankName = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Timestamp")]
        public DateTime? Timestamp { get { return timestamp; } set { timestamp = value; } }
        [Property("Operation_Type")]
        public string OperationType { get { return operationType; } set { operationType = value; } }
        [Property("Operation_Status")]
        public string OperationStatus { get { return operationStatus; } set { operationStatus = value; } }
        [Property("Measurement_Type")]
        public string MeasurementType { get { return measurementType; } set { measurementType = value; } }
        [Property("Product_Code")]
        public string ProductCode { get { return productCode; } set { productCode = value; } }
        [Property("Liquid_Level")]
        public double LiquidLevel { get { return liquidLevel; } set { liquidLevel = value; } }
        [Property("Liquid_Water")]
        public double LiquidWater { get { return liquidWater; } set { liquidWater = value; } }
        [Property("Liquid_Temperature")]
        public double LiquidTemperature { get { return liquidTemperature; } set { liquidTemperature = value; } }
        [Property("Liquid_Density")]
        public double LiquidDensity { get { return liquidDensity; } set { liquidDensity = value; } }
        [Property("Liquid_Density15")]
        public double LiquidDensity15 { get { return liquidDensity15; } set { liquidDensity15 = value; } }
        [Property("Liquid_VolObs")]
        public double LiquidVolObs { get { return liquidVolObs; } set { liquidVolObs = value; } }
        [Property("Liquid_Vol15")]
        public double LiquidVol15 { get { return liquidVol15; } set { liquidVol15 = value; } }
        [Property("Liquid_Mass")]
        public double LiquidMass { get { return liquidMass; } set { liquidMass = value; } }
        [Property("Liquid_Pressure")]
        public float LiquidPressure { get { return liquidPressure; } set { liquidPressure = value; } }
        [Property("Liquid_Volume_Correction_Factor")]
        public float LiquidVolumeCorrectionFactor { get { return liquidVolumeCorrectionFactor; } set { liquidVolumeCorrectionFactor = value; } }
        [Property("Liquid_Density_Multiplier")]
        public float LiquidDensityMultiplier { get { return liquidDensityMultiplier; } set { liquidDensityMultiplier = value; } }
        [Property("Total_Observed_Volume")]
        public float TotalObservedVolume { get { return totalObservedVolume; } set { totalObservedVolume = value; } }
        [Property("Pair_Id")]
        public int? PairId { get { return pairId; } set { pairId = value; } }
        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }

        public string ProductName
        {
            get
            {
                Product product = Product.FindByProductCode(ProductCode);
                if (product != null)
                {
                    return product.ProductName;
                }
                else
                {
                    return "";
                }
            }
        }

        public TankTicket()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(TankTicket), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(TankTicket));
            DetachedCriteria countCriteria = DetachedCriteria.For<TankTicket>();

            //find operation type not in PSC
            criteria.Add(Expression.Not(Expression.In("OperationType", new string[] { "PSC" })));
            countCriteria.Add(Expression.Not(Expression.In("OperationType", new string[] { "PSC" })));

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                //flt tank ticket number
                if (param.SearchParam["fltTankTicketNumber"] != null && param.SearchParam["fltTankTicketNumber"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("TankTicketNumber", param.SearchParam["fltTankTicketNumber"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("TankTicketNumber", param.SearchParam["fltTankTicketNumber"], MatchMode.Anywhere));
                }
                //flt tank name
                if (param.SearchParam["fltTank"] != null && param.SearchParam["fltTank"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("TankName", param.SearchParam["fltTank"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("TankName", param.SearchParam["fltTank"], MatchMode.Anywhere));
                }
                //flt operation type
                if (param.SearchParam["fltOperationType"] != null && param.SearchParam["fltOperationType"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("OperationType", param.SearchParam["fltOperationType"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("OperationType", param.SearchParam["fltOperationType"], MatchMode.Anywhere));
                }
                //flt operation status
                if (param.SearchParam["fltOperationStatus"] != null && param.SearchParam["fltOperationStatus"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("OperationStatus", param.SearchParam["fltOperationStatus"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("OperationStatus", param.SearchParam["fltOperationStatus"], MatchMode.Anywhere));
                }
                //flt timestamp from
                if (param.SearchParam["fltTimestampFrom"] != null && param.SearchParam["fltTimestampFrom"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampFrom"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Gt("Timestamp", startPeriod));
                    countCriteria.Add(Expression.Gt("Timestamp", startPeriod));
                }
                //flt timestamp to
                if (param.SearchParam["fltTimestampTo"] != null && param.SearchParam["fltTimestampTo"].Length != 0)
                {
                    DateTime endPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampTo"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Le("Timestamp", endPeriod));
                    countCriteria.Add(Expression.Le("Timestamp", endPeriod));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<TankTicket>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static PagingResult SelectPagingPSC(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(TankTicket), new NHibernateDelegate(SelectPagingCallbackPSC), param);
        }

        private static object SelectPagingCallbackPSC(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(TankTicket));
            DetachedCriteria countCriteria = DetachedCriteria.For<TankTicket>();
            //find operation type PSC
            criteria.Add(Expression.InsensitiveLike("OperationType", "PSC"));
            countCriteria.Add(Expression.InsensitiveLike("OperationType", "PSC"));

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                //flt tank ticket number
                if (param.SearchParam["fltTankTicketNumber"] != null && param.SearchParam["fltTankTicketNumber"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("TankTicketNumber", param.SearchParam["fltTankTicketNumber"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("TankTicketNumber", param.SearchParam["fltTankTicketNumber"], MatchMode.Anywhere));
                }
                //flt tank name
                if (param.SearchParam["fltTank"] != null && param.SearchParam["fltTank"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("TankName", param.SearchParam["fltTank"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("TankName", param.SearchParam["fltTank"], MatchMode.Anywhere));
                }
                //flt timestamp from
                if (param.SearchParam["fltTimestampFrom"] != null && param.SearchParam["fltTimestampFrom"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampFrom"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Gt("Timestamp", startPeriod));
                    countCriteria.Add(Expression.Gt("Timestamp", startPeriod));
                }
                //flt timestamp to
                if (param.SearchParam["fltTimestampTo"] != null && param.SearchParam["fltTimestampTo"].Length != 0)
                {
                    DateTime endPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampTo"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Le("Timestamp", endPeriod));
                    countCriteria.Add(Expression.Le("Timestamp", endPeriod));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<TankTicket>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public bool Validate()
        {
            bool result = true;
            return result;
        }

        public static TankTicket[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<Tank>();
            dc.AddOrder(Order.Asc("TankTicketId"));
            return (TankTicket[])FindAll(typeof(TankTicket), dc);
        }

        public static TankTicket FindById(int id)
        {
            return (TankTicket)FindByPrimaryKey(typeof(TankTicket), id);
        }

        public static TankTicket[] FindLatestTankTicketById(string tankName)
        {
            DetachedCriteria dc = DetachedCriteria.For<TankTicket>();
            dc.Add(Expression.Eq("TankName", tankName));
            dc.Add(Expression.Not(Expression.In("OperationType", new string[] { "PSC" }))); //exclude PSC
            dc.AddOrder(Order.Desc("Timestamp"));
            dc.SetMaxResults(1);
            return (TankTicket[])FindAll(typeof(TankTicket), dc);

        }

        public static TankTicket FindTicketPairOpen(string tankName, string operationType)
        {
            DetachedCriteria dc = DetachedCriteria.For<TankTicket>();
            dc.Add(Expression.Eq("TankName", tankName));
            dc.Add(Expression.Eq("OperationType", operationType));
            dc.Add(Expression.Eq("OperationStatus", "Open"));
            dc.AddOrder(Order.Desc("Timestamp"));
            dc.SetMaxResults(1);
            return (TankTicket)FindFirst(typeof(TankTicket), dc);
        }

        public static TankTicket[] FindSalesReservationByTank(DateTime date, string tankName)
        {
            DetachedCriteria dc = DetachedCriteria.For<TankTicket>();
            dc.Add(Expression.And(Expression.Ge("Timestamp", date.Date), Expression.Lt("Timestamp", date.Date.AddDays(1))));
            dc.Add(Expression.Eq("TankName", tankName));
            dc.Add(Expression.Or(Expression.Eq("OperationType", "Sales"), Expression.Eq("OperationType", "Receiving Sales")));
            dc.AddOrder(Order.Desc("Timestamp"));
            return (TankTicket[])FindAll(typeof(TankTicket), dc);
        }

        public static TankTicket[] FindSalesReservationByProduct(DateTime date, string productCode)
        {
            DetachedCriteria dc = DetachedCriteria.For<TankTicket>();
            dc.Add(Expression.And(Expression.Ge("Timestamp", date.Date), Expression.Lt("Timestamp", date.Date.AddDays(1))));
            dc.Add(Expression.Eq("ProductCode", productCode));
            dc.Add(Expression.Or(Expression.Eq("OperationType", "Sales"), Expression.Eq("OperationType", "Receiving Sales")));
            dc.AddOrder(Order.Desc("Timestamp"));
            return (TankTicket[])FindAll(typeof(TankTicket), dc);
        }
    }
}
