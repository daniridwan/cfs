﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;
using System.Web;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CFS.Models
{
    [ActiveRecord("Distribution_Planning")]
    public class DistributionPlanning : ActiveRecordBase
    {
        private List<string> messages;
        private int id;
        private DateTime? date;
        private float planning;
        private DateTime? createdTimestamp;
        private SimpleUser createdBy;
        private DateTime? updatedTimestamp;
        private SimpleUser updatedBy;

        [PrimaryKey(PrimaryKeyType.Identity, "Id")]
        public int Id { get { return id; } set { id = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Date")]
        public DateTime? Date { get { return date; } set { date = value; } }
        [Property("Planning")]
        public float Planning { get { return planning; } set { planning = value; } }
        //Audit Trail
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Created_Timestamp")]
        public DateTime? CreatedTimestamp { get { return createdTimestamp; } set { createdTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Created_By")]
        public SimpleUser CreatedBy { get { return createdBy; } set { createdBy = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Updated_Timestamp")]
        public DateTime? UpdatedTimestamp { get { return updatedTimestamp; } set { updatedTimestamp = value; } }
        [ScriptIgnore]
        [BelongsTo("Updated_By")]
        public SimpleUser UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public string CreatedByName { get { return createdBy != null ? createdBy.Username : string.Empty; } }
        public string UpdatedByName { get { return updatedBy != null ? updatedBy.Username : string.Empty; } }

        public DistributionPlanning()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(DistributionPlanning), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(DistributionPlanning));
            DetachedCriteria countCriteria = DetachedCriteria.For<DistributionPlanning>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltDate"] != null && param.SearchParam["fltDate"].Length != 0)
                {
                    DateTime date = DateTime.ParseExact(param.SearchParam["fltDate"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Eq("Date", date));
                    countCriteria.Add(Expression.Eq("Date", date));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<DistributionPlanning>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public bool Validate()
        {
            bool result = true;
            return result;
        }

        public static DistributionPlanning[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<DistributionPlanning>();
            dc.AddOrder(Order.Asc("Date"));

            return (DistributionPlanning[])FindAll(typeof(DistributionPlanning), dc);
        }

        public static DistributionPlanning FindById(int id)
        {
            return (DistributionPlanning)FindByPrimaryKey(typeof(DistributionPlanning), id);
        }

        public static DistributionPlanning FindByDate(string date)
        {
            ICriterion[] query = { Expression.Eq("Date", date) };
            return (DistributionPlanning)FindOne(typeof(DistributionPlanning), query);
        }
    }
}
