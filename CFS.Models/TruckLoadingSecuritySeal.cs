﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("Truck_Loading_Security_Seal")]
    public class TruckLoadingSecuritySeal : ActiveRecordBase
    {
        private List<string> messages;
        private int securitySealId;
        private int truckLoadingId;
        private int sealNumber;
        private DateTime? sealTimestamp;
        private string sealDescription;

        [PrimaryKey(PrimaryKeyType.Identity, "Security_Seal_Id")]
        public int SecuritySealId { get { return securitySealId; } set { securitySealId = value; } }
        [Property("Truck_Loading_Id")]
        public int TruckLoadingId { get { return truckLoadingId; } set { truckLoadingId = value; } }
        [Property("Seal_Number")]
        public int SealNumber { get { return sealNumber; } set { sealNumber = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Seal_Timestamp")]
        public DateTime? SealTimestamp { get { return sealTimestamp; } set { sealTimestamp = value; } }
        [Property("Seal_Description")]
        public string SealDescription { get { return sealDescription; } set { sealDescription = value; } }

        public TruckLoadingSecuritySeal()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(TruckLoadingSecuritySeal), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(TruckLoadingSecuritySeal));
            DetachedCriteria countCriteria = DetachedCriteria.For<TruckLoadingSecuritySeal>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltName"] != null && param.SearchParam["fltName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("Name", param.SearchParam["fltName"], MatchMode.Anywhere));
                }
            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<TruckLoadingSecuritySeal>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }
        public static TruckLoadingSecuritySeal[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<TruckLoadingSecuritySeal>();
            dc.AddOrder(Order.Asc("SecuritySealId"));
            return (TruckLoadingSecuritySeal[])FindAll(typeof(TruckLoadingSecuritySeal), dc);
        }
        public static TruckLoadingSecuritySeal[] FindByTruckLoadingId(int truckLoadId)
        {
            ICriterion[] query = { Expression.Eq("TruckLoadingId", truckLoadId) };
            return (TruckLoadingSecuritySeal[])FindAll(typeof(TruckLoadingSecuritySeal), query);
        }
        public static TruckLoadingSecuritySeal FindByTruckLoadId(int truckLoadingId)
        {
            ICriterion[] query = { Expression.Eq("TruckLoadingId", truckLoadingId) };
            return (TruckLoadingSecuritySeal)FindOne(typeof(TruckLoadingSecuritySeal), query);
        }

    }
}
