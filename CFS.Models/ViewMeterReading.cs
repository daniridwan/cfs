﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("View_Meter_Reading")]
    public class ViewMeterReading : ActiveRecordBase
    {
        private List<string> messages;
        private int truckLoadingId;
        private int recordId;
        private int truckId;
        private string registrationPlate;
        private string deliveryOrderNumber;
        private double preset;
        private double actual;
        private string productCode;
        private string productName;
        private string actualFillingPointName;
        private DateTime? startTimestamp;
        private double startTotalizer;
        private DateTime? stopTimestamp;
        private double stopTotalizer;
        private double density;
        private double temperature;
        private int fillingStatus;
        private int sealNumber;
        private DateTime? sealTimestamp;
        private float flowrate;

        [PrimaryKey(PrimaryKeyType.Identity, "Record_Id")]
        public int RecordId { get { return recordId; } set { recordId = value; } }
        [Property("Truck_Loading_Id")]
        public int TruckLoadingId { get { return truckLoadingId; } set { truckLoadingId = value; } }
        [Property("Truck_Id")]
        public int TruckId { get { return truckId; } set { truckId = value; } }
        [Property("Registration_Plate")]
        public string RegistrationPlate { get { return registrationPlate; } set { registrationPlate = value; } }
        [Property("Delivery_Order_Number")]
        public string DeliveryOrderNumber { get { return deliveryOrderNumber; } set { deliveryOrderNumber = value; } }
        [Property("Preset")]
        public double Preset { get { return preset; } set { preset = value; } }
        [Property("Actual")]
        public double Actual { get { return actual; } set { actual = value; } }
        [Property("Product_Code")]
        public string ProductCode { get { return productCode; } set { productCode = value; } }
        [Property("Product_Name")]
        public string ProductName { get { return productName; } set { productName = value; } }
        [Property("Actual_Filling_Point_Name")]
        public string ActualFillingPointName { get { return actualFillingPointName; } set { actualFillingPointName = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Start_Timestamp")]
        public DateTime? StartTimestamp { get { return startTimestamp; } set { startTimestamp = value; } }
        [Property("Start_Totalizer")]
        public double StartTotalizer { get { return startTotalizer; } set { startTotalizer = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Stop_Timestamp")]
        public DateTime? StopTimestamp { get { return stopTimestamp; } set { stopTimestamp = value; } }
        [Property("Stop_Totalizer")]
        public double StopTotalizer { get { return stopTotalizer; } set { stopTotalizer = value; } }
        [Property("Density")]
        public double Density { get { return density; } set { density = value; } }
        [Property("Temperature")]
        public double Temperature { get { return temperature; } set { temperature = value; } }
        [Property("Filling_Status")]
        public int FillingStatus { get { return fillingStatus; } set { fillingStatus = value; } }
        [Property("Flowrate")]
        public float Flowrate { get { return flowrate; } set { flowrate = value; } }

        public ViewMeterReading()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static PagingResult SelectPaging(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(ViewMeterReading), new NHibernateDelegate(SelectPagingCallback), param);
        }

        private static object SelectPagingCallback(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria(typeof(ViewMeterReading));
            DetachedCriteria countCriteria = DetachedCriteria.For<ViewMeterReading>();

            //add param key here
            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltFillingPointName"] != null && param.SearchParam["fltFillingPointName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("ActualFillingPointName", param.SearchParam["fltFillingPointName"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("ActualFillingPointName", param.SearchParam["fltFillingPointName"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltDeliveryOrderNumber"] != null && param.SearchParam["fltDeliveryOrderNumber"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("DeliveryOrderNumber", param.SearchParam["fltDeliveryOrderNumber"], MatchMode.Anywhere));
                    countCriteria.Add(Expression.InsensitiveLike("DeliveryOrderNumber", param.SearchParam["fltDeliveryOrderNumber"], MatchMode.Anywhere));
                }
                //flt timestamp from
                if (param.SearchParam["fltTimestampFrom"] != null && param.SearchParam["fltTimestampFrom"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampFrom"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Gt("StartTimestamp", startPeriod));
                    countCriteria.Add(Expression.Gt("StartTimestamp", startPeriod));
                }
                //flt timestamp to
                if (param.SearchParam["fltTimestampTo"] != null && param.SearchParam["fltTimestampTo"].Length != 0)
                {
                    DateTime endPeriod = DateTime.ParseExact(param.SearchParam["fltTimestampTo"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    criteria.Add(Expression.Le("StartTimestamp", endPeriod));
                    countCriteria.Add(Expression.Le("StartTimestamp", endPeriod));
                }

            }

            if (param.SortColumn != null && param.SortColumn.Length != 0)
            {
                if (param.SortDirection == PagingQuery.Direction.Desc)
                    criteria.AddOrder(Order.Desc(param.SortColumn));
                else
                    criteria.AddOrder(Order.Asc(param.SortColumn));
            }

            if (param.PageSize != 0)
            {
                criteria.SetFirstResult(param.PageIndex * param.PageSize);
                criteria.SetMaxResults(param.PageSize);
            }

            IList records = criteria.List();

            int count = ActiveRecordMediator<ViewMeterReading>.Count(countCriteria);

            PagingResult pr = new PagingResult();
            pr.PageIndex = param.PageIndex;
            pr.Records = records;
            pr.TotalRecords = count;
            return pr;
        }

        public static ViewMeterReading[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewMeterReading>();
            dc.AddOrder(Order.Asc("TruckLoadingId"));
            return (ViewMeterReading[])FindAll(typeof(ViewMeterReading), dc);
        }

        public static ViewMeterReading FindById(int id)
        {
            return (ViewMeterReading)FindByPrimaryKey(typeof(ViewMeterReading), id);
        }

        public static ViewMeterReading FindByDeliveryOrderNumber(string spaNumber)
        {
            ICriterion[] query = { Expression.Eq("DeliveryOrderNumber", spaNumber) };
            return (ViewMeterReading)FindOne(typeof(ViewMeterReading), query);
        }
        public static ViewMeterReading FindByFillingPointName(string fillingPointName)
        {
            ICriterion[] query = { Expression.Eq("ActualFillingPointName", fillingPointName) };
            return (ViewMeterReading)FindOne(typeof(ViewMeterReading), query);
        }

        public static PagingResult getThruputByPeriodandProduct(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(ViewMeterReading), new NHibernateDelegate(sumPreset), param);
        }

        private static object sumPreset(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;
            ICriteria criteria = session.CreateCriteria<ViewMeterReading>();

            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltFillingPointName"] != null && param.SearchParam["fltFillingPointName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("ActualFillingPointName", param.SearchParam["fltFillingPointName"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltProductName"] != null && param.SearchParam["fltProductName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("ProductName", param.SearchParam["fltProductName"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltProductCode"] != null && param.SearchParam["fltProductCode"].Length != 0)
                {
                    criteria.Add(Expression.Eq("ProductCode", param.SearchParam["fltProductCode"]));
                }

                if (param.SearchParam["fltStartPeriod"] != null && param.SearchParam["fltStartPeriod"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltStartPeriod"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime endPeriod = DateTime.MinValue;

                    if (param.SearchParam["fltEndPeriod"] != null && param.SearchParam["fltEndPeriod"].Length != 0)
                    {
                        endPeriod = DateTime.ParseExact(param.SearchParam["fltEndPeriod"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    }

                    if (endPeriod != DateTime.MinValue)
                    {
                        endPeriod = endPeriod.AddDays(1);
                        criteria.Add(Expression.Ge("StopTimeStamp", startPeriod));
                        criteria.Add(Expression.Le("StopTimeStamp", endPeriod));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq("StopTimeStamp", startPeriod));
                    }
                }
            }

            criteria.SetProjection(Projections.ProjectionList()
                                       .Add(Projections.Sum("Preset"), "Preset"));
            // .Add(Projections.Sum("Actual"), "Actual"));

            IList records = criteria.List();

            PagingResult pr = new PagingResult();
            pr.Records = records;

            return pr;
        }

        public static PagingResult getActualByPeriodandProduct(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(ViewMeterReading), new NHibernateDelegate(sumActual), param);
        }

        private static object sumActual(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;


            ICriteria criteria = session.CreateCriteria<ViewMeterReading>();
            //ICriteria countCriteria = session.CreateCriteria<TruckLoadingRecord>();

            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltFillingPointName"] != null && param.SearchParam["fltFillingPointName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("ActualFillingPointName", param.SearchParam["fltFillingPointName"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltProductName"] != null && param.SearchParam["fltProductName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("ProductName", param.SearchParam["fltProductName"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltProductCode"] != null && param.SearchParam["fltProductCode"].Length != 0)
                {
                    criteria.Add(Expression.Eq("ProductCode", param.SearchParam["fltProductCode"]));
                }

                if (param.SearchParam["fltStartPeriod"] != null && param.SearchParam["fltStartPeriod"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltStartPeriod"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime endPeriod = DateTime.MinValue;

                    if (param.SearchParam["fltEndPeriod"] != null && param.SearchParam["fltEndPeriod"].Length != 0)
                    {
                        endPeriod = DateTime.ParseExact(param.SearchParam["fltEndPeriod"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    }

                    if (endPeriod != DateTime.MinValue)
                    {
                        endPeriod = endPeriod.AddDays(1);
                        criteria.Add(Expression.Ge("StopTimeStamp", startPeriod));
                        criteria.Add(Expression.Le("StopTimeStamp", endPeriod));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq("StopTimeStamp", startPeriod));
                    }
                }
            }

            criteria.SetProjection(Projections.ProjectionList()
                                       .Add(Projections.Sum("Actual"), "Actual"));
            // .Add(Projections.Sum("Actual"), "Actual"));
            IList records = criteria.List();

            PagingResult pr = new PagingResult();
            pr.Records = records;

            return pr;
        }

        public static PagingResult getAverageFlowrate(PagingQuery param)
        {
            return (PagingResult)Execute(typeof(ViewMeterReading), new NHibernateDelegate(AverageFlowrate), param);
        }

        private static object AverageFlowrate(ISession session, object instance)
        {
            PagingQuery param = (PagingQuery)instance;

            ICriteria criteria = session.CreateCriteria<ViewMeterReading>();
            //ICriteria countCriteria = session.CreateCriteria<TruckLoadingRecord>();

            if (param.SearchParam.HasKeys())
            {
                if (param.SearchParam["fltFillingPoint"] != null && param.SearchParam["fltFillingPoint"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("ActualFillingPointName", param.SearchParam["fltFillingPoint"], MatchMode.Anywhere));
                    //countCriteria.Add(Expression.InsensitiveLike("FillingPointName", param.SearchParam["fltFillingPoint"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltProductName"] != null && param.SearchParam["fltProductName"].Length != 0)
                {
                    criteria.Add(Expression.InsensitiveLike("ProductName", param.SearchParam["fltProductName"], MatchMode.Anywhere));
                    //countCriteria.Add(Expression.InsensitiveLike("ProductName", param.SearchParam["fltProductName"], MatchMode.Anywhere));
                }
                if (param.SearchParam["fltProductCode"] != null && param.SearchParam["fltProductCode"].Length != 0)
                {
                    criteria.Add(Expression.Eq("ProductCode", param.SearchParam["fltProductCode"]));
                    //countCriteria.Add(Expression.InsensitiveLike("ProductName", param.SearchParam["fltProductName"], MatchMode.Anywhere));
                }

                if (param.SearchParam["fltStartPeriod"] != null && param.SearchParam["fltStartPeriod"].Length != 0)
                {
                    DateTime startPeriod = DateTime.ParseExact(param.SearchParam["fltStartPeriod"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime endPeriod = DateTime.MinValue;

                    if (param.SearchParam["fltEndPeriod"] != null && param.SearchParam["fltEndPeriod"].Length != 0)
                    {
                        endPeriod = DateTime.ParseExact(param.SearchParam["fltEndPeriod"], "s", System.Globalization.CultureInfo.InvariantCulture);
                    }

                    if (endPeriod != DateTime.MinValue)
                    {
                        endPeriod = endPeriod.AddDays(1);
                        criteria.Add(Expression.Ge("StopTimeStamp", startPeriod));
                        criteria.Add(Expression.Le("StopTimeStamp", endPeriod));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq("StopTimeStamp", startPeriod));
                    }
                }
            }

            criteria.SetProjection(Projections.ProjectionList()
                                       .Add(Projections.Avg("Flowrate")));
            // .Add(Projections.Sum("Actual"), "Actual"));

            IList records = criteria.List();

            PagingResult pr = new PagingResult();
            pr.Records = records;

            return pr;
        }
    }
}
