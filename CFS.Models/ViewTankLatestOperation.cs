﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NHibernate;
using NHibernate.Criterion;
using System.Text.RegularExpressions;
using NHibernate.Mapping;
using System.Web.Script.Serialization;

namespace CFS.Models
{
    [ActiveRecord("View_Tank_Latest_Operation")]
    public class ViewTankLatestOperation : ActiveRecordBase
    {
        private List<string> messages;
        private int tankTicketId;
        private string tankTicketNumber;
        private string tankName;
        private DateTime? timestamp;
        private string operationType;
        private string operationStatus;
        private string measurementType;
        private string productCode;
        private float liquidLevel;
        private float liquidThreshold;
        private bool isActive;

        [PrimaryKey(PrimaryKeyType.Identity, "Tank_Ticket_Id")]
        public int TankTicketId { get { return tankTicketId; } set { tankTicketId = value; } }
        [Property("Tank_Ticket_Number")]
        public string TankTicketNumber { get { return tankTicketNumber; } set { tankTicketNumber = value; } }
        [Property("Tank_Name")]
        public string TankName { get { return tankName; } set { tankName = value; } }
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Property("Timestamp")]
        public DateTime? Timestamp { get { return timestamp; } set { timestamp = value; } }
        [Property("Operation_Type")]
        public string OperationType { get { return operationType; } set { operationType = value; } }
        [Property("Operation_Status")]
        public string OperationStatus { get { return operationStatus; } set { operationStatus = value; } }
        [Property("Measurement_Type")]
        public string MeasurementType { get { return measurementType; } set { measurementType = value; } }
        [Property("Product_Code")]
        public string ProductCode { get { return productCode; } set { productCode = value; } }
        [Property("Liquid_Level")]
        public float LiquidLevel { get { return liquidLevel; } set { liquidLevel = value; } }
        [Property("Liquid_Threshold")]
        public float LiquidThreshold { get { return liquidThreshold; } set { liquidThreshold = value; } }
        [Property("Is_Active")]
        public bool IsActive { get { return isActive; } set { isActive = value; } }

        public string ProductName
        {
            get
            {
                Product product = Product.FindByProductCode(ProductCode);
                if (product != null)
                {
                    return product.ProductName;
                }
                else
                {
                    return "";
                }
            }
        }

        public string IsActiveText
        {
            get
            {
                if (IsActive)
                {
                    return "Active";
                }
                else
                {
                    return "Inactive";
                }
            }
        }

        public ViewTankLatestOperation()
        {
            messages = new List<string>();
        }

        public List<string> GetErrorMessages()
        {
            return messages;
        }

        public static ViewTankLatestOperation[] FindAll()
        {
            DetachedCriteria dc = DetachedCriteria.For<ViewTankLatestOperation>();
            dc.AddOrder(Order.Asc("TankName"));
            return (ViewTankLatestOperation[])FindAll(typeof(ViewTankLatestOperation), dc);
        }

        public static ViewTankLatestOperation FindByName(string tankName)
        {
            ICriterion[] query = { Expression.Eq("TankName", tankName) };
            return (ViewTankLatestOperation)FindOne(typeof(ViewTankLatestOperation), query);
        }
    }
}
