﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace CFS.Biostar
{
    public class BS_Program
    {
        private BS2SimpleDeviceInfo deviceInfo = new BS2SimpleDeviceInfo();

        private UInt32 deviceID = 0;
        private IntPtr sdkContext = IntPtr.Zero;
        private BS2ErrorCode result;
        private List<string> msg = new List<string>();

        public BS2SimpleDeviceInfo DeviceInfo { get { return deviceInfo; } set { deviceInfo = value; } }
        public UInt32 DeviceID { get { return deviceID; } set { deviceID = value; } }
        public IntPtr SdkContext { get { return sdkContext; } set { sdkContext = value; } }
        public BS2ErrorCode Result { get { return result; } set { result = value; } }
        public List<string> Msg { get { return msg; } set { msg = value; } }

        public void init()
        {
            deviceID = 0;
            sdkContext = API.BS2_AllocateContext();

            if (sdkContext == IntPtr.Zero)
            {
                msg.Add("Can't allocate sdk context.");
            }
            result = (BS2ErrorCode)API.BS2_Initialize(sdkContext);
            if (result != BS2ErrorCode.BS_SDK_SUCCESS)
            {
                msg.Add(string.Format("SDK initialization failed with : {0}", result));
                API.BS2_ReleaseContext(sdkContext);
                return;
            }
        }

        public void release()
        {
            if (sdkContext != IntPtr.Zero)
            {
                API.BS2_ReleaseContext(sdkContext);
                sdkContext = IntPtr.Zero;
            }
        }

        public bool ConnectToDevice(string deviceIpAddress)
        {
            UInt16 port = (UInt16)BS2Envirionment.BS2_TCP_DEVICE_PORT_DEFAULT;

            result = (BS2ErrorCode)API.BS2_ConnectDeviceViaIP(sdkContext, deviceIpAddress, port, out deviceID);

            //result = (BS2ErrorCode)API.BS2_ConnectDevice(sdkContext,deviceID);

            if (result != BS2ErrorCode.BS_SDK_SUCCESS)
            {
                msg.Add(string.Format("Can't connect to device(errorCode : {0}).", result));
                return false;
            }
            else
            {
                BS2ErrorCode resultDeviceInfo = (BS2ErrorCode)API.BS2_GetDeviceInfo(sdkContext, deviceID, out deviceInfo);

                msg.Add(string.Format("Successfully connected to the device[{0}].", deviceID));
                return true;
            }
        }

        public void DisconnectToDevice()
        {
            if (deviceID != 0)
            {
                API.BS2_DisconnectDevice(sdkContext, deviceID);
                deviceID = 0;
            }
        }

        public BS2ErrorCode computeCRC(BS2SmartCardData smartCard, out UInt16 hdrCRC, out UInt16 cardCRC)
        {
            BS2ErrorCode result = BS2ErrorCode.BS_SDK_SUCCESS;
            IntPtr smartCardObj = Marshal.AllocHGlobal(Marshal.SizeOf(smartCard));
            int cardTypeOffset = (int)Marshal.OffsetOf(typeof(BS2SmartCardHeader), "cardType");
            int cardCRCOffset = (int)Marshal.OffsetOf(typeof(BS2SmartCardHeader), "cardCRC");
            IntPtr cardDataObj = smartCardObj + cardTypeOffset;

            Marshal.StructureToPtr(smartCard, smartCardObj, false);

            cardCRC = 0xFFFF;
            hdrCRC = 0xFFFF;

            result = (BS2ErrorCode)API.BS2_ComputeCRC16CCITT(cardDataObj, (UInt32)(Marshal.SizeOf(typeof(BS2SmartCardData)) - cardTypeOffset), ref cardCRC);
            if (result == BS2ErrorCode.BS_SDK_SUCCESS)
            {
                Marshal.WriteInt16(smartCardObj, cardCRCOffset, (Int16)cardCRC);
                IntPtr cardCrcObj = smartCardObj + cardCRCOffset;
                result = (BS2ErrorCode)API.BS2_ComputeCRC16CCITT(cardCrcObj, (UInt32)(Marshal.SizeOf(typeof(BS2SmartCardHeader)) - cardCRCOffset), ref hdrCRC);
            }

            Marshal.FreeHGlobal(smartCardObj);
            return result;
        }
    }
}
